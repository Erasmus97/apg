Dear Shareholders of the EARNEST Partners Fixed Income Trust:



Enclosed for your review is the annual report of the EARNEST Partners Fixed Income Trust (the Fund) for the fiscal year ended March the fiscal year, the Funds total return exceeded that of its benchmark, the Barclays Capital Aggregate Bond Index (BCABI),* by basis points, with the Fund having a return of and the BCABI having a return of the Lipper Intermediate Investment Grade Debt Funds category as of March the Fund ranked out of out of out of and out of for the one year, three year, five year, and ten year periods, respectively.** Thus, the Fund beat at least of its peer group in this category for the fiscal year ended March on a total return basis.



The fiscal year continued the trends we saw for much of the fiscal year.With government intervention around the globe remaining robust, risk takers were willing to drift further down the risk spectrum this past fiscal year.The confidence in risk assets stems from the implicit and sometimes explicit backing of central banks and governments around the world.While we question and do not invest on implicit guarantees, the financial markets are certainly comfortable with it today.Domestically, the Federal Reserve, which was wrapping up their first buy program at the end of the fiscal year, instituted a new buy program (Quantitative Easing Part II) to further stimulate the economy midway through the most recent fiscal year as economic growth and improvements in employment have been slow to take-hold.Their buy program continues to remove supply from the market, which has supported all asset prices.While the fiscal picture domestically continued to deteriorate during the fiscal year, internationally, the European Union grappled with fiscal problems as well.Greece and Ireland received direct support from the European Union while Portugal will most likely be bailed-out early in the fiscal year.The question remains as to whether the bailouts are sufficient to cover the needs of these countries or will a full fledged debt restructuring be necessary to put the countries back-on solid footing.



As we close the fiscal year, it is staggering for many to learn that US interest rates are actually lower year over year (even though the Federal government has borrowed trillions of dollars during this period of time) and that the US dollar is stronger today against a basket of currencies than it was at end of the fiscal year (despite the Federal Reserve increasing its balance sheet by trillions of dollars).Lower interest rates have provided an assist to all borrowers though it has become apparent that just because someone has the ability to borrow at low interest rates does not mean they want to do so.A quick look at the Federal Reserves data shows that banks are sitting on over a trillion dollars earning essentially nothing each day.As the good credits do not want to borrow and the banks do not want to lend to riskier credits, the demand for high quality, short-term investments remains strong.



The tailwind that risk assets have received over the past fiscal year lifted the Funds returns relative to the benchmark.Our emphasis on protecting capital through the downturn has continued to benefit returns as all of our assets have been able to participate in the tightening of credit spreads.In particular, the Fund benefited from its holdings in commercial mortgage-backed, corporate, and agency mortgage-backed securities this past fiscal year.



Looking forward, we continue to see attractive opportunities for investment.The margin for error though has changed dramatically in the last two fiscal years as the compensation for risk taking has declined precipitously.Risk management is always important and a key element to the Funds strategy, but in todays environment, we argue that it is even more critical.Interest rates are low and risk spreads are tight, which means that any capital impairments will be almost impossible to earn back.As we look at investments going forward, we will continue to implement the same philosophy and process that carried us safely through the volatility of the past fiscal years.We continue to look at many high quality sectors supported by real assets or explicit government guarantees.We believe these types of securities are the foundation of the Funds time tested process and its future success.




Very Truly Yours,
