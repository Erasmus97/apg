Dear Fellow Shareholder, 
We thank you for your continued investment with the Metropolitan West Funds. Steady inflows to the Funds since our September 2010 Semi-Annual Report have increased total assets to more
than $17 billion, with the flagship Total Return Bond Fund at nearly $13 billion in aggregate across its share classes. Both Fund leadership and the overall investment team have enjoyed a period of stability and continuity that has resulted in the
realization of good performance, particularly as we have moved away from the financial crisis of 2008. Over the long-term, the consistency of the team has afforded a measured approach to market cycles and a far-reaching perspective, providing the
basis for solid returns relative to the various Fund benchmarks, an imperative for active investment management. 
The March 31, 2011 Annual Report for the Metropolitan West Funds covers the following: 
 
Metropolitan West Ultra Short Bond Fund
M-Class (MWUSX), I-Class (MWUIX)
Metropolitan West Low Duration Bond Fund
M-Class (MWLDX), I-Class (MWLIX),
Administrative-Class (MWLNX)
Metropolitan West Intermediate Bond Fund
M-Class (MWIMX), I-Class (MWIIX)
Metropolitan West Total Return Bond Fund
M-Class (MWTRX), I-Class (MWTIX),
Administrative-Class (MWTNX)
Metropolitan West High Yield Bond Fund
M-Class (MWHYX), I-Class (MWHIX)
Metropolitan West Strategic Income Fund
M-Class (MWSTX), I-Class (MWSIX)
Metropolitan West AlphaTrak 500 Fund
M-Class (MWATX)
Economic Review and Market Environment 
As GDP has
grudgingly but steadily edged higher over the past two-plus years, among the ongoing risks has been a potential for external shocks to disrupt the fragile recovery, while spreading volatility across the global and, particularly, U.S. capital
markets. So, with the start to 2011 full of political upheaval in the Middle East and North Africa and a tragically devastating earthquake, tsunami, and nuclear disaster in Japan a reasonable expectation would have had investors bracing for the
worst in terms of a sustained flight-to-safety and, in accompaniment, a fevered selloff in the risk markets. While oil prices climbed higher and held at over $100 as conflagration in Libya intensified, other indicators either failed to appear or
receded rapidly if they did. U.S. Treasury rates, to wit, rose over the quarter and the VIX indicator of implied equity market volatility did spike in mid March but quickly fell back to levels consistent with or lower than most of 2010. In a show of
resilience (perhaps a testament to liquidity), investment grade and high yield risk premiums actually declined, the latter delivering decidedly positive returns – nearly 4% in the quarter and more than 7% in the past six months. In contrast,
the U.S. Treasury market fell 0.2% to kick off 2011, after sliding 2.6% in the fourth quarter of 2010. The fall represented an unwinding of the rally that had followed Ben Bernanke’s speech last August in Jackson Hole, Wyoming signaling the
Fed’s commitment to provide additional stimulus as needed to sustain economic recovery and lift employment. 
On the economic front, recognizing that the effects of higher oil prices and trading partner dislocations will impact the data on a lagged basis, the contemporaneous readings appeared to
confirm an ongoing recovery of modest pace. Principally, fourth quarter GDP growth was revised upward, settling in near 3% and, with more than 200 thousand additions to non-farm payrolls in March, the unemployment rate fell to 8.8% (from nearly
10% last November). Household finances look to be recovering, owing much to equity market appreciation that has nearly doubled the S&amp;P 500 from March 2009 lows. Optimism from such developments would appear to account for a breakthrough in
consumer outlays, punctuated by a significant increase in auto sales and outlook. Still, while economic positives are accumulating slowly, evidence of sluggishness – and thus risk – remains present. Most notably, the housing market has
struggled to generate sustained momentum, especially since the expiration of the homebuyer credit last year. Case-Shiller data points to year-over-year declines in an overwhelming proportion of major markets, while even the up-in-market metropolitan
areas had paltry gains. 
Pacing the fixed income market over a reporting period along with high yield were
commercial mortgage-backed securities (CMBS), which gained 3%.The investment grade corporate market, though not as rewarding as high yield, did post significant excess performance over U.S. Treasury issues though was not able to completely overcome
rising rates to post positive returns over the past six months. Although jarred somewhat by the U.S. Treasury’s unexpected announcement to initiate an unwind of its mortgage holdings, agency MBS finished with a rally as buying for mortgage real
estate investment trusts (REIT) amplified strong technicals, producing a 0.6% return in the first quarter and 0.8% for the six months. Non-agency MBS, too, benefitted from good market demand and scant new supply to deliver price appreciation to this
still-attractive part of the market. Overall, on the return contributions from the spread sectors of the fixed income market, the Barclays Aggregate index, while posting 0.9% for the past six months, considerably outperformed the 2.8% fall in the
U.S. Treasury index. 
 
Annual
Report March 2011 / 1 
The Economy and Market Ahead 
The Economy and Market Ahead 
Market focus now shifts toward Fed action and slated end of QE2 heading into the latter half of 2011. Questions abound as
to how the market might respond and the investor base that may come to fill the spot the Fed vacates. To the first, recent history has underscored the forward-looking nature of the markets. For example, the end of agency MBS buying at the close of
the Fed’s QE1 program, confirmed an adjustment well ahead of the termination. While history is not certain to repeat, the year-to-date move up in rates, particularly against the events of the first quarter, suggests the process is unfolding.
Regarding who steps up, a faith in the market mechanism means that the only pertinent question may be the first: At what yield level? As it pertains to the more traditional execution of monetary policy, via the Fed Funds rate, the zero interest rate
policy (ZIRP) is likely to hold until early 2012, as evidence of more buoyant growth, e.g. increasing CPI, declining unemployment, housing revival, accumulates. However, a caveat exists in that it has become increasingly apparent that the consensus
for unfettered and ongoing easing by the Fed has been breaking down for a while, so that any unexpected and sustained acceleration in economic indicators, especially if validated by the bond market, will increase calls for an expeditious move away
from the ZIRP. 
 1
Data sources for the discussion above include Bloomberg, Barclays, and Merrill Lynch. 
Fund Results 
General Performance Commentary 
In a change from the last several reports in which the relative performance was earned across the Funds largely through
the sector allocation and security selection strategies, over the past six months positive incremental performance also came from a defensive duration position - about a half-year short the index through the quarter - as rates rose across the curve,
but most notably 100 basis points in the 5- to 7-year stretch of the U.S. Treasury curve. While the contribution from interest rate positioning was welcome, it did not come at the expense of bottom-up sector allocation and security selection
effects, which again were positive. Exposure to non-agency MBS and financial issuers, particularly money center banks, accounted for much of the value added as those areas continue to normalize from the deep distress of 2008. Opportunistic
adjustments to the agency MBS exposure during the period also provided a measure of outperformance. Causing a slight drag to performance was an underweight to the lower quality segment of the CMBS market, which continues to see performance led by
riskier lower-rated subordinated tranches included in the index (and not held in the Funds). In the High Yield Fund, a defensive interest rate duration was additive over the past six months, and security selection, notably in utilities and
financials, contributed to favorable performance versus the index. This was achieved with comparatively higher quality than the index, which bore a higher allocation to the lowest quality cohorts than the Fund. 
Following back-to-back years of solid index performance and outstanding relative return experience, expectations for
fixed income alpha in 2011 are more in line with historical precedent rather than the recovery years of 2009 and 2010. While opportunity remains, the lower relative return outlook informs caution across the value-added dimensions. As a result, risk
will be monitored on a continuous basis, with trimming of spread exposure on further gains. To the extent that periodic volatility picks up, the investment team – consistent with its value orientation – will look to take advantage of
misalignments between price and fundamental value. 
The performance data quoted represents past performance
and does not guarantee future results. Current performance may be lower or higher. Due to recent favorable market conditions, some of the Funds have experienced an unusually high-performance which may not be sustainable or repeated in the future.
Performance data current to the most recent month-end may be obtained at www.mwamllc.com. The investment return and principal value of an investment will fluctuate so that shares, when redeemed, may be worth more or less than their original cost.

The net expense ratio reflects a contractual agreement by the adviser to reduce its fees and/or absorb
expenses to limit the Fund’s total annual operating expenses until July 31, 2011. Without fee waivers, returns would have been lower. For additional expense ratio information, please refer to the Financial Highlights section of the report.

 
 
2 / Annual Report March
2011 
Metropolitan West Ultra Short Bond Fund 
Metropolitan West Ultra Short Bond Fund 
M-Class (MWUSX), I-Class (MWUIX) 
 
For MWUSX, the total expense ratio is 0.75% and the net expense ratio is 0.53%. For MWUIX, the total expense ratio is
0.59% and the net expense ratio is 0.37%. 
Metropolitan West Low Duration Bond Fund 
M-Class (MWLDX), I-Class (MWLIX), Administrative-Class (MWLNX) 
 
For MWLDX, the total expense ratio is 0.63% and the net expense ratio is 0.61%. For MWLIX, the total expense ratio is 0.44% and the net expense ratio is 0.42%. For MWLNX, the total expense
ratio is 0.83% and the net expense ratio is 0.81%. 
Metropolitan West Intermediate Bond Fund 
M-Class (MWIMX), I-Class (MWIIX) 
 
For MWIMX, the total expense ratio is 0.77% and the net expense ratio is 0.66%. For MWIIX, the total expense ratio is
0.56% and the net expense ratio is 0.45%. 
Metropolitan West Total Return Bond Fund 
M-Class (MWTRX), I-Class (MWTIX), Administrative-Class (MWTNX) 
 
For MWTRX, the total expense ratio is 0.66% and the net expense ratio is 0.66%. For MWTIX, the total expense ratio is 0.45% and the net expense ratio is 0.45%. For MWTNX, the total expense
ratio is 0.86% and the net expense ratio is 0.86%. 
 
Annual
Report March 2011 / 3 
Metropolitan West High Yield Bond Fund 
Metropolitan West High Yield Bond Fund 
M-Class (MWHYX), I-Class (MWHIX) 
 
For MWHYX, the total expense ratio is 0.87% and the net expense ratio is 0.80%. For MWHIX, the total expense ratio is
0.62% and the net expense ratio is 0.55%. 
Metropolitan West Strategic Income Fund 
M-Class (MWSTX), I-Class (MWSIX) 
 
For MWSTX, the total expense ratio is 1.71% and the net expense ratio is 1.71%. For MWSIX, the total expense ratio is
1.46% and the net expense ratio is 1.46%. 
Metropolitan West AlphaTrak 500 Fund 
(MWATX) 
 
For MWATX, the total expense ratio is 1.02% and the net expense ratio is 0.93%. 
A Consistent Long-Term Value Orientation 
We remain
committed to an investment approach that emphasizes a long-term perspective and an understanding that market pricing can and does come disconnected to fundamental value on a persistent basis. By maintaining discipline, we seek to continue adding
value to client portfolios in a measured, risk-controlled manner. Diversification is a cornerstone to the portfolio construction, not only in the traditional manner of allocating across maturities, sectors and individual securities, but also in the
decisions – duration, yield curve positioning, sector allocation and security selection – that drive performance through time. While market conditions change constantly, our process is steadfast and vigilant to opportunities to bring
value. 
 
4 / Annual Report March
2011 
Again, we thank you for your continued support of the Metropolitan West
Funds and look forward to the ongoing opportunity to meet your investment objectives. 
Again, we thank you for your continued support of the Metropolitan West
Funds and look forward to the ongoing opportunity to meet your investment objectives. 
Sincerely, 


