Dear Shareholder,



Enclosed you will find the annual report for USA MUTUALS. The USA MUTUALS management team and Trustees of the Funds recognize the importance of thorough and consistent communication with our shareholders. In this annual report, we include all the required quantitative information, such as financial statements, detailed footnotes, performance reports, Fund holdings and manager insights.



Vice Fund



The last year has been eventful for the Vice Fund. We have seen dividends and dividend growth come back in style. The Fund has focused even more on what historically has driven past stock returns and found vice companies that meet those criteria. For the one year period ending March the Vice Fund had a total return of while the S&amp;P Index returned The long term broad stock market returns have been about total return for nearly years. This has led many to favor equities as the key to higher returns. In reality, dividends produced over half of the total returns for stocks. P/E expansion has also played a role in stock returns, as people became fonder of equities they paid more for each dollar of earnings produced. Earnings growth has also played a significant role in driving returns. There are three ways to grow EPS:











Actual top-line revenue driven growth price increases, higher volume, general economic growth.












Cost cutting, improved efficiency, gaining economies of scale can also improve earnings even without revenue growth.












Reducing the number of shares and dividing income by fewer shares also drives EPS.






My goal is to find stocks that embody these characteristics in abundance and typically this comes back to finding companies with huge cash flows, which vice companies have historically had. Keep in mind, our goal is not to find stocks of a prurient nature. Our goal is to find companies that demonstrate characteristics we just discussed.



Dividends the market (as measured by the S&amp;P Index) is at dividend yield. Tobacco currently yields Alcohol about Defense Gaming does not pay a dividend but it gives us some emerging market exposure well discuss below.



Earnings/EPS growth has also been strong and has remained so:











Revenue growth comes from annual price hikes and expanding market share. In the case of all our target industries, they have been seeing solid exposure to the industrialization of China, India, Africa, Eastern Europe, Russia, and Latin America. These are countries where growing affluence means people will likely trade up in alcohol or tobacco quality. People in Asia love to gamble and the new casinos in Macau already do more business than Las Vegas. Rising defense budgets overseas also means the prospect of more government orders for aerospace and defense materials.












Cost savings These companies have actively sought to acquire others in their industries. For example, Molson and Coors merged, Inbev bought Anheuser Busch and Raytheon bought Applied Signal. This can enable duplicate overhead costs to be cut and for production facilities to run at a higher rate of utilization. Thus, income should be higher for the combined company than simply adding the results of two separate ones.












Share repurchases have been a mainstay of these companies as well. Most of our companies have a history of buying back shares to drive EPS further.




























P/E expansion There is an over-riding aversion to vice stocks. People may think they are going out of business (smokers are aging, older people dont drink beer, the government will cut the defense budget, there is no disposable cash for gambling). As a result, many of these companies have been trading at discounts to the markets P/E. As the myths surrounding many of these companies are exposed, valuations have moved higher. One of the big drivers for the Funds performance last year was P/E expansion in the tobacco area. We have been expanding our positions in defense to take advantage of low valuations there, looking to take advantage of P/E expansion in that group.



The unique nature of vice stocks can create huge cash flows for investors. These are not products that change radically such as computers, software, or medical tech. Cigarettes, beer, spare parts for jets and hotel rooms just do not need the same level of research and development (R&amp;D) and advertising. Nor do they become obsolete every six months.



Demand is not likely to change with the economy people continue to smoke and drink, governments maintain their militaries, and people still seek entertainment. Moreover, many of these items may be small luxuries to their users and are not something they have to agonize over to purchase.



Upstarts are not likely to enter the market its too expensive. This allows the current players to boost prices and operate in an oligopoly manner. To become a defense contractor requires years of service, evidence of financial stability, and the ability to finance large multi-year projects. A new casino costs billion these days. New cigarettes and alcohol need to push something else off the shelves to gain access to a store, plus they need to win over distributors. The current players can still acquire upstarts that manage to achieve success.



In areas where demand is flat, the companies can consolidate and grow by cutting costs. Thus, capital spending declines too while cash flows rise. All this cash flow can be used to fund dividends, repurchases, and more acquisitions and the cycle can repeat as described above.



Vice stocks can offer the best of both worlds to investors. Vice stocks can grow in good times and have been defensive in bear markets. High dividends may act as a break on pushing these stocks down in bear markets. The high cash flows allow the companies to purchase more shares if the prices fall, which in turn should boost EPS and allows the dividends per share to rise even if total outlay for dividends is flat. More consolidation can occur in bear markets and thus there are normally buyers for these stocks. The end result is the returns have the potential to keep coming in good times or bad times. Dividends are paid and shareholders earn cash returns.



As we moved through and into we have sought to rotate more into low P/E stocks like were finding in the defense sector, and out of high P/E stocks like were seeing in gaming. For example, Wynn Macau is still a fine company. It is one of the less leveraged casino companies in Las Vegas and has new properties to compete against older properties that need to be updated. It has growth potential with its Chinese properties that have continued to see more visitors and increased gambling profits. The issue for us is that it pays a very minor dividend and was trading at forward earnings. That is hardly cheap. We realized profits on Wynn Macau and reallocated the proceeds into defense stocks yielding more than and trading at about earnings.



Early can be largely summed up with Lorillard. Lorillard was going to hurt performance in and early as the market worried about potential negative action on menthol by the FDA in the spring. The stock at the time was in the and fell steadily through January into the As the company is mostly dependent on selling menthol cigarettes, we understood that fear would drive this stock down.
























The over-riding issue was that Lorillard was still very cheap in my view. The company was gaining market share, trading at a steep discount to the group, had very little debt, and was boosting its dividend by On our cost, the stock was yielding over In the market, it was yielding between which again was a discount to its group. Plus, I still believe it may eventually get bought out as part of the consolidation of the industry at a higher price. Thus, the potential upside outweighed the short-term risk of holding the full position. Therefore, I held the investment during the down time and endured the fear of potential problems with menthol.



As the initial reports from the FDA subcommittee came in, it was clear that the subcommittee wanted to get tougher with menthol, but it did not want to ban it. The whole thing is getting more study and in my view will probably involve additional labeling when this is all done. In the meantime, lifting the subcommittee risk drove the stock back to and Lorillards continued strong results, repurchases, and dividends have now pushed the stock above



The Vice Funds outperformance is due largely to three issues (mentioned above): P/E expansion among the tobacco and gaming sectors, high dividend growth with the stocks trading up and then the sizable appreciation in Lorillard. In a stock market that searched for earnings growth - gaming in Macau stood out as an area that attracted many investors who continued to buy the stocks. As these companies completed their debt offerings for new projects, this risk diminished and the stocks were well-rewarded by investors as the P/E ratios increased. Tobacco companies also saw their P/E ratios expand and generate appreciation. The same was not true of many other S&amp;P Index stocks where earnings rose, but the P/E ratio did not. These two Vice Fund sectors saw both earnings growth and P/E expansion, which helped the outperformance. In addition, nearly every stock we hold saw a dividend increase. With the attention paid to low interest rates, investors have focused more heavily on dividends for income. Companies with growing dividends saw greater appreciation than the S&amp;P Index as a whole.



In terms of portfolio strategy during the period, I have begun writing covered calls on some positions. Covered calls are a lower risk way to boost total return for the shareholders. It allows us to generate income from some stocks in the portfolio that do not pay a dividend or pay a very nominal one. So far the call writing has centered on defense stocks and some of the alcohol names.



In terms of the overall weightings in the portfolio, we ended with tobacco at alcohol at defense at gaming is and cash is The cheapest sector is by far defense where P/E ratios are less than and dividends are often greater than We also see more consolidation happening there and have added some smaller companies that appear to be good buy-out candidates, like Applied Signal was for us in Mantech International and SAIC Inc. were bought with that thought in mind. Even in the event they are not acquired, they trade at only EPS and are heavily focused on fast-growing areas of defense such as cyber warfare and intelligence gathering.



Gaming remains the most expensive sector and we have been taking profits there. Moreover, while there has been growth in the foreign areas for gaming, we are up against the limit on foreign stock ownership and would have to reduce our alcohol positions to add more. Given the valuations and lack of dividends versus alcohol, I am opting to stick with alcohol over gaming at this time. The foreign gaming stocks are valued at forward earnings. I sold the most expensive one, Melco Crown in Domestic gaming still breaks down into highly leveraged casino companies that have under-spent on maintenance for years and the slot-machine makers that represent pent up demand as that maintenance spending is done. We continue to focus on the slot machine makers for our domestic exposure.
























Generation Wave Growth Fund



Generation Wave Growth Fund (Generation Wave) results in reflect some sizeable portfolio turnover. Coming into the year there were significant investments in large banks such as Bank of America and Citicorp and positions that emphasized improving credit quality for individuals and leveraged companies in general. All of these positions were sold during the year and we realized profits overall. The top performers were the companies that buy defaulted loans and work out payment plans with individuals. For the one year period ended March Generation Wave had a total return of for the year, while the S&amp;P Index returned for the same period.



We further sold off positions that had less to do with the boomer theme such as Japanese securities, lithium batteries, and Chinese computer gaming companies. We have also sought to help returns by eliminating investments in ETFs and closed-end funds that charge us fees. These past areas have been replaced with stocks trading in three basic areas: medical/healthcare (preferably stocks with dividends), financial stocks focusing on savings and investment and a stay-at-home focus for people looking to save more money and turn inward.



Three themes dominated the moves made in early for GenerationWave: selling more of the remaining ETFs and closed-end funds, exchanging some of the big pharma names into new drug companies that have greater room to boost dividends and rely less on patent-expiring drugs, and identifying and adding more turn-around companies. The remaining ETFs and closed-end funds were in silver and junk bonds. Both have had a tremendous rally as investors bought precious metals on inflation fears and reached for yield with Treasuries at low levels. At this point we have shares of the iShares Silver Trust left and both junk bond funds are completely sold. In my view, poor credits can offer good value at cents on the dollar and reward investors with higher than average yield and capital appreciation. The same cannot be said when junk credits are trading at cents on the dollar. That is the level of price change those securities have seen since and profits have been realized for several quarters with the final positions eliminated in The iShares Silver Trust position was reduced, but we still hold shares. Inflation fears justify keeping some of this position, and there are issues with realizing gains in investment companies like this ETF for the mutual fund. Within the mutual fund rule parameters, we will likely look to reduce this position further this year.



One of the largest drags on portfolio performance has been the large pharmaceutical companies such as BristolMyers Squibb, AstraZeneca, etc. These stocks have been owned for: high yield, cheap valuations, strong cash flows, and low leverage to enable them to maneuver through patent expirations. While we still strongly believe many of these stocks should produce solid returns going forward, the rapid gains in the market as a whole have been leaving them behind as the market worries about patent expiration.



We looked at the industry completely and did a full cash flow analysis and came away with the conclusion that the entire group was very cheap and some logical swaps existed. As a result, we cut back on Eli Lilly, Pfizer, Bristol Myers, and AstraZeneca and added new names like Roche, Abbott Labs, and Johnson &amp; Johnson. In each case, the new additions were yielding about vs. for the positions sold. More importantly, the new additions have been posting double-digit sales growth and have very minor exposure to patent expiration in the near term in terms of percentage of total sales. And finally, the new additions were only spending about of free cash flow on dividends and thus have room to potentially grow their dividends faster than the reduced positions. Moreover, while the reduced positions had a higher current yield, they no longer had a meaningful edge in P/E ratios. They were trading for forward EPS vs. forward EPS for























the companies with better growth potential. The total return potential looked more compelling to make these swaps and the new names were of the portfolio at the end of They have since risen to of the total portfolio.



Turnaround names continue to be our primary focus. I have talked in the past about looking for companies trading near lows after some bad news that continue to have solid businesses with high barriers to entry to help protect them and help stage a recovery. These are the types of companies that have good prospects for getting acquired as we are currently seeing with our positions in NASDAQ OMX Group and NYSE Euronext. We believed that there would be a renewed focus on investing by the Boomer generation and thought the stock exchanges had businesses that would be very tough to duplicate and gather market share. We have also seen solid performance in our turnaround stocks that focused on people staying at home more. Comcast (cable TV), Myers Industries (flower pots), Kroger (grocery stores) and Lifetime Brands (kitchen wares) were all bought when they were hitting new lows and have recovered nicely.



A new name that I have added of late is Aegean Marine. This is a company that refuels cruise ships (Boomer connection), tankers, and cargo ships at sea. For years, this has been a very steady business that earned per ton reselling fuel. It has continually added new boats and now operates in seventeen ports up from five a few years ago and has become a dominant player in the industry.



In the summer of as oil prices began to rise, customers sought to hoard cash by not filling up their tanks completely and only bought enough fuel to get to the next port. Major ports such as Singapore and Amsterdam suddenly saw the volume of fuel sold drop noticeably, which crushed pricing. In addition, smaller operators still run some single-hulled tankers (all of Aegean Marines boats are double-hulled and very new). These single-hulls will be phased out worldwide by and many markets have already banned them. Therefore, in their dying days, the owners can only operate in a few ports and have to generate business at any price and did. The result was pricing on reselling fuel dropped to per ton driven by oversupply in the normally huge markets for refueling.



Aegean Marine stock fell from the to less than The replacement value of its boats, cash on hand, and working capital should give it a fair value of in our view. Moreover, the value of its boats should rise as the mom-n-pop competitors are forced out of business by the ban on single hulled boats. Mom-n-pops may also be forced out because they may not be able to afford to buy fuel when oil is above and extend weeks of credit to customers. Their working capital build essentially drains them of cash. Aegean Marine has over billion in liquidity to withstand this. I bought a large position in Aegean Marine at less than for the Fund. .



What is happening is Aegean Marine moved several of its boats out of the crowded ports to new places. During those moves, it cost the company money and they werent selling fuel so volumes were lower. The less crowded ports did not see fuel spread collapse to the same degree and already spreads have been rising again and are Aegean Marine is at breakeven at about and every in spread should be worth about cents in EPS. Just returning to the low-end of the normal spread would have it earning per share and growing if they add volumes from new boats and with mom-n-pops potentially leaving the industry. Aegean Marines focus on many ports makes it easy for large-scale customers operating on many routes to deal with Aegean Marine over a collection of smaller players. Moreover, it signed an exclusive deal with Panama and expects to sign deals with two more ports. All of that should help it improve pricing as well.























We thank you for entrusting your investments to us. We take our role as manager of your assets very seriously, and hope to have the opportunity to continue to earn your trust and confidence for many years to come.



Thank you.



Sincerely,
