Dear Shareowner,

In the U.S. economy moved forward on a slow path to recovery. But with the
memory of a deep recession still lingering, businesses and consumers remained
cautious about both investing and spending. While business fundamentals showed
signs of improvement, there was still a reluctance to hire, and high
unemployment remained a problem throughout the year. Wary investors, concerned
about risk, gravitated towards cash and bonds for most of until better
economic news in the final few months of the year caused a slight shift in
investor sentiment back towards stocks, thus lifting equity returns.

As we enter Pioneer remains generally optimistic about the prospects for
economic recovery. The recovery process may occur more slowly than many would
like, and may be accompanied by short-term market swings. But our investment
professionals are finding good opportunities to invest in both equities and
bonds.

At Pioneer, we have long advocated the benefits of staying diversified and
investing for the long term. The strategy has generally performed well for many
investors. For instance, bond markets certainly rewarded investors for most of
while equity markets barely budged, even though equity valuations were
inexpensive relative to bonds and compared with historic levels -- conditions
which represented potentially good value for long-term investors. Ultimately,
many of those long-term investors were rewarded when the equity markets finally
rallied over the last few months of

Pioneer has not changed the basic approach to investing that we have used for
more than years. We remain focused on company fundamentals and risk
management. Our investment process is based on careful research into individual
companies, quantitative analysis, and active portfolio management. This
three-pillared process, which we apply to each of our portfolios, is supported
by an integrated team approach and is designed to carefully balance risk and
reward. Our experienced professionals devote themselves to the careful research
needed to identify investment opportunities in markets around the world.

Our advice, as always, is to work closely with a trusted financial advisor to
discuss your goals and work together to develop an investment strategy that
meets


Pioneer Classic Balanced Fund | Semiannual Report |


your individual needs. There is no single best strategy that works for every
investor.

We invite you to learn more about Pioneer and our time-tested approach to
investing by consulting with your financial advisor or visiting us online at
www.pioneerinvestments.com. We greatly appreciate your trust in us and we thank
you for investing with Pioneer.


Sincerely,

Dear Shareowner,

In the U.S. economy moved forward on a slow path to recovery. But with
the memory of a deep recession still lingering, businesses and consumers
remained cautious about both investing and spending. While business
fundamentals showed signs of improvement, there was still a reluctance to hire,
and high unemployment remained a problem throughout the year. Wary investors,
concerned about risk, gravitated towards cash and bonds for most of until
better economic news in the final few months of the year caused a slight shift
in investor sentiment back towards stocks, thus lifting equity returns.

As we enter Pioneer remains generally optimistic about the prospects for
economic recovery. The recovery process may occur more slowly than many would
like, and may be accompanied by short-term market swings. But our investment
professionals are finding good opportunities to invest in both equities and
bonds.

At Pioneer, we have long advocated the benefits of staying diversified and
investing for the long term. The strategy has generally performed well for many
investors. For instance, bond markets certainly rewarded investors for most of
while equity markets barely budged, even though equity valuations were
inexpensive relative to bonds and compared with historic levels -- conditions
which represented potentially good value for long-term investors. Ultimately,
many of those long-term investors were rewarded when the equity markets finally
rallied over the last few months of

Pioneer has not changed the basic approach to investing that we have used for
more than years. We remain focused on company fundamentals and risk
management. Our investment process is based on careful research into individual
companies, quantitative analysis, and active portfolio management. This
three-pillared process, which we apply to each of our portfolios, is supported
by an integrated team approach and is designed to carefully balance risk and
reward. Our experienced professionals devote themselves to the careful research
needed to identify investment opportunities in markets around the world.

Our advice, as always, is to work closely with a trusted financial advisor to
discuss your goals and work together to develop an investment strategy that
meets


Pioneer Government Income Fund | Semiannual Report |


your individual needs. There is no single best strategy that works for every
investor.

We invite you to learn more about Pioneer and our time-tested approach to
investing by consulting with your financial advisor or visiting us online at
www.pioneerinvestments.com. We greatly appreciate your trust in us and we thank
you for investing with Pioneer.

Sincerely,

Dear Shareowner,

In the U.S. economy moved forward on a slow path to recovery. But with
the memory of a deep recession still lingering, businesses and consumers
remained cautious about both investing and spending. While business
fundamentals showed signs of improvement, there was still a reluctance to hire,
and high unemployment remained a problem throughout the year. Wary investors,
concerned about risk, gravitated towards cash and bonds for most of until
better economic news in the final few months of the year caused a slight shift
in investor sentiment back towards stocks, thus lifting equity returns.

As we enter Pioneer remains generally optimistic about the prospects for
economic recovery. The recovery process may occur more slowly than many would
like, and may be accompanied by short-term market swings. But our investment
professionals are finding good opportunities to invest in both equities and
bonds.

At Pioneer, we have long advocated the benefits of staying diversified and
investing for the long term. The strategy has generally performed well for many
investors. For instance, bond markets certainly rewarded investors for most of
while equity markets barely budged, even though equity valuations were
inexpensive relative to bonds and compared with historic levels -- conditions
which represented potentially good value for long-term investors. Ultimately,
many of those long-term investors were rewarded when the equity markets finally
rallied over the last few months of

Pioneer has not changed the basic approach to investing that we have used for
more than years. We remain focused on company fundamentals and risk
management. Our investment process is based on careful research into individual
companies, quantitative analysis, and active portfolio management. This
three-pillared process, which we apply to each of our portfolios, is supported
by an integrated team approach and is designed to carefully balance risk and
reward. Our experienced professionals devote themselves to the careful research
needed to identify investment opportunities in markets around the world.

Our advice, as always, is to work closely with a trusted financial advisor to
discuss your goals and work together to develop an investment strategy that
meets


Pioneer Treasury Reserves Fund | Semiannual Report |


your individual needs. There is no single best strategy that works for every
investor.

We invite you to learn more about Pioneer and our time-tested approach to
investing by consulting with your financial advisor or visiting us online at
www.pioneerinvestments.com. We greatly appreciate your trust in us and we thank
you for investing with Pioneer.

Sincerely,
