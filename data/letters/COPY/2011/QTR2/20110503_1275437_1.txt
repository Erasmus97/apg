Dear Fellow Shareholders,



We are pleased to report that the Veracity Small Cap Value Fund (the Fund) achieved both a strong absolute and relative return for the recently ended fiscal year. The Veracity Small Cap Fund Class R returned versus the Russell Value Index (the Funds benchmark) return of for the year ended February Funds performance was broad based and our optimism on the markets has been rewarded over the last two years.



Specifically, for the fiscal year, the Funds performance was led by stock selection in Financials, Producer Durables, Autos &amp; Transports, and Energy.Consumer Discretionary and Utilities were the most sizable detractors due to stock selection.Our weighting in Financials and Producer Durables added positively to performance, while cash was our biggest detractor.Higher momentum, higher price/book and higher earnings growth were all favorable style attributes.



Stock picking within Banks, S&amp;Ls and REITs, as well as avoiding some of the worse performing insurance stocks were the key drivers of the Funds outperformance in Financials.Within Producer Durables, we owned a number of Aerospace stocks that outperformed as well, including Baldor Electric (BEZ) and ADC Telecom (ADCT) which were acquired at premiums.Tenneco (TEN) was our best stock in Auto and Transports.Kodiak Oil &amp; Gas (KOG), Solutia (SOA) and Quaker Chemical (KWR) were also large contributors to performance.



Utilities were fairly minor detractors.In Utilities, our Telecom holdings underperformed their peers. Larger detractors included Mueller Water Products (MWA), Comtech Telecommunications (CMTL) and Hornbeck Offshore Services (HOS).



As we look forward, we remain positive on both the market and the economy.We believe the seeds of a self-sustaining economic recovery are being sown.In particular, we are encouraged by increased capital expenditures from larger companies over the last three quarters.Moreover, small business confidence, while not robust, is also beginning to improve.We are also seeing signs of continued improvement in the employment picture, a lynch-pin in the economys future.



While the economy has seen more certainty of late, this has also been partly reflected in the stock market which has had a stellar six months of performance.That said, we believe there is still further upside for the following reasons













Valuations are still reasonable.The market is not as cheap as it used to be, but it is still cheap based on historical metrics.
















Risk appetites are increasing as evidenced by the strong demand for the recent Facebook deal with Goldman Sachs.




































There is a lot of money sitting in fixed income instruments earning very little, while the stock market has produced strong returns over the last years.Combined with increased risk appetites we expect to see this money start to migrate toward equities.
















We are beginning to see equity mutual fund flows turn positive, supporting our thesis of migration toward equities






Certainly, recent events cloud our outlook some.The first is the crisis in the Middle East which has led to a sharp spike in energy costs.The second is the terrible tragedy in Japan resulting from the earthquake and tsunami.In addition to these two exogenous events, the market faces the end of (the Quantitative Easing of the U.S. money supply by the Federal Reserve) in June.



In looking at these three concerns, the least threat, in our opinion, is the Japanese disaster.While the human toll is incalculable, we think the overall impact on global growth will be manageable and transitory.As a large, developed country with a well-built infrastructure, Japan will be able to recover from this catastrophe.From a global perspective, it may even increase growth in some sectors.There will be an earnings impact for companies, but we believe that the market will look through any Japan-induced earnings blips.Our bigger concern with Japan is the timing of those earnings blips in conjunction with the end of both of which are likely to hit at the same time.Should the market correlate slower growth with instead of a temporary pause from Japan, it could weigh on stocks.



The more significant of these new uncertainties is the spike in energy costs arising from the unrest in the Middle East.Our concern here derives from both the psychological as well as the financial impact of rising prices.We have read some interesting research from Empirical Research Partners that makes us feel a little better about the situation.Empirical points out that much of the consumer recovery has been centered in the top decile of income where energy prices make up a much smaller percentage of expenditures than for middle income individuals.In the end, the financial impact could be muted.However, the psychological risk to confidence is real and remains something that bears watching.



It has been encouraging that the market continues to rally in the face of these macro events.We think that this points to the healing of confidence and confirms a sustainable economic recovery.There has been a recent host of positive macro-economic data pointing to improving employment, increasing cap-ex low inflation and better equity fund flows. As a result, we remain optimistic on the economy.



We believe the market is turning its attention from bigger picture issues to individual company fundamentals.As we said last quarter, we will continue to look for companies in the value universe which offer prospects for both top and bottom line growth.We retain a positive bias toward the market although we are weary of the risks we spoke about above.Overall, we expect























this to be a good year for the market, but not with returns like we have witnessed over the last two years.



We thank you for the trust you have placed in us, and we will continue to diligently apply our philosophy of buying undervalued stocks with a catalyst in an effort to add alpha for the shareholders of the Fund.



Sincerely,
