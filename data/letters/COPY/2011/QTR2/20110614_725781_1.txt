Dear Shareholders:



Enclosed is important information about your Fund and its performance.



Ive always believed that companies have an obligation to communicate regularly with their
clients, and I believe that obligation is especially critical in the investment industry.



Our website invesco.com/us offers timely market updates and commentary from many of our
portfolio managers and other investment professionals, as well as quarterly messages from me. At
invesco.com/us, you also can obtain information about your account at any hour of the day or night.
I invite you to visit and explore the tools and information we offer.



Invescos commitment to investment excellence



Invescos acquisition of Morgan Stanleys retail asset management business, including Van
Kampen Investments, broadened our range of investment products available to you. As a strong
organization with a single focus investment management Invesco today offers investment
capabilities and products to meet the needs of virtually any investor. In addition to traditional
mutual funds, we manage a broad range of other solutions, including single-country, regional and
global investments spanning major equity, fixed income and alternative asset classes.



Investment excellence is our goal across our product line. Let me explain what that means. All
of our funds are managed by specialized teams of investment professionals. Each team has a discrete
investment perspective and philosophy, and all follow disciplined, repeatable processes governed by
strong risk oversight. Our investment-centric culture provides an environment that seeks to reduce
distractions, allowing our fund managers to concentrate on what they do best manage your money.



The importance of a broad product line and investment management expertise is obvious given
the markets weve experienced over the last two to three years. Weve seen that investment
strategies can outperform or underperform their benchmark indexes for a variety of reasons,
including where we are in the market cycle, and whether prevailing economic conditions are
favorable or unfavorable for that strategy. Thats why no investment strategy can guarantee
top-tier performance at all times. What investors can expect, and what Invesco offers, are funds
that are managed according to their stated investment objectives and strategies, with robust risk
oversight using consistent, repeatable investment processes that dont change as short-term
external conditions change investments managed for the long term. This disciplined approach
cant guarantee a profit; no investment can do that, since all involve some measure of risk. But it
can ensure that your money is managed the way we said it would be.



This adherence to stated investment objectives and strategies allows your financial advisor to
build a diversified portfolio that meets your individual risk tolerance and financial goals. It
also means that when your goals change, your financial advisor will be able to find an appropriate
investment option to meet your needs.



Invescos commitment to you



Invescos commitment to you remains stronger than ever. Its one of the reasons weve grown to
become one of the worlds leading asset managers.



If you have questions about your account, please contact one of our client service
representatives at If you have a general Invesco-related question or comment for me,
I invite you to email me directly at phil@invesco.com.



I want to thank you for placing your trust in us. All of us at Invesco look forward to serving
your investment management needs for many years to come. Thank you for investing with us.



Sincerely,

Dear Fellow Shareholders:


With behind us, now is a good time to review our portfolios and ensure that we are adhering to
a long-term, diversified investment strategy, which Ive mentioned in previous letters. The year
was notable for a number of reasons, but Im sure most of us are grateful for a return to more
stable markets and growing signs that the worst of the economic crisis is behind us.

Your Board continued to oversee the Invesco Funds with a strong sense of responsibility for
your savings and a deep appreciation for your continued trust. As always, we worked throughout
to manage costs and ensure Invesco continued to place investor interests first.

Im pleased to report that the latest report from Morningstar affirmed the work weve done and
included a number of positive comments regarding your Boards oversight of the Invesco Funds.

As background, Morningstar is a leading independent provider of investment research in North
America, Europe, Australia and Asia. Morningstar stated, A fund boards duty is to represent the
interests of fund shareholders, ensuring that the funds that it oversees charge reasonable fees and
are run by capable advisors with a sound investment process.

Morningstar maintained your Fund
Boards A grade for Board Quality, praising the Board for taking meaningful steps in recent
years to act in fund shareholders These steps included becoming much more
proactive and vocal in overseeing how Invesco votes the funds shareholders proxies and requiring
each fund trustee to invest more than one years board compensation in Invesco funds, further
aligning our interests with those of our shareholders. Morningstar also cited the work Ive done to
make myself more available to fund shareholders via email.



I am also pleased that Morningstar recognized the effort and the Fund Boards efforts over the
past several years to work together with management at Invesco to enhance performance and sharpen
the focus on investors.



As always, youre welcome to contact me at bruce@brucecrockett.com with any questions or
concerns you have. We look forward to representing and serving you.



Sincerely,

Dear Shareholders:



Enclosed is important information about your Fund and its performance.



Ive always believed that companies have an obligation to communicate regularly with their
clients, and I believe that obligation is especially critical in the investment industry.


Our website invesco.com/us offers timely market updates and commentary from many of our
portfolio managers and other investment professionals, as well as quarterly messages from me. At
invesco.com/us, you also can obtain information about your account at any hour of the day or night.
I invite you to visit and explore the tools and information we offer.


Invescos commitment to investment excellence



Invescos acquisition of Morgan Stanleys retail asset management business, including Van
Kampen Investments, broadened our range of investment products available to you. As a strong
organization with a single focus investment management Invesco today offers investment
capabilities and products to meet the needs of virtually any investor. In addition to traditional
mutual funds, we manage a broad range of other solutions, including single-country, regional and
global investments spanning major equity, fixed income and alternative asset classes.



Investment excellence is our goal across our product line. Let me explain what that means. All
of our funds are managed by specialized teams of investment professionals. Each team has a discrete
investment perspective and philosophy, and all follow disciplined, repeatable processes governed by
strong risk oversight. Our investment-centric culture provides an environment that seeks to reduce
distractions, allowing our fund managers to concentrate on what they do best manage your money.


The importance of a broad product line and investment management expertise is obvious given
the markets weve experienced over the last two to three years. Weve seen that investment
strategies can outperform or underperform their benchmark indexes for a variety of reasons,
including where we are in the market cycle, and whether prevailing economic conditions are
favorable or unfavorable for that strategy. Thats why no investment strategy can guarantee
top-tier performance at all times. What investors can expect, and what Invesco offers, are funds
that are managed according to their stated investment objectives and strategies, with robust risk
oversight using consistent, repeatable investment processes that dont change as short-term
external conditions change investments managed for the long term. This disciplined approach
cant guarantee a profit; no investment can do that, since all involve some measure of risk. But it
can ensure that your money is managed the way we said it would be.


This adherence to stated investment objectives and strategies allows your financial advisor to
build a diversified portfolio that meets your individual risk tolerance and financial goals. It
also means that when your goals change, your financial advisor will be able to find an appropriate
investment option to meet your needs.


Invescos commitment to you



Invescos commitment to you remains stronger than ever. Its one of the reasons weve grown to
become one of the worlds leading asset managers.



If you have questions about your account, please contact one of our client service
representatives at If you have a general Invesco-related question or comment for me,
I invite you to email me directly at phil@invesco.com.


I want to thank you for placing your trust in us. All of us at Invesco look forward to serving
your investment management needs for many years to come. Thank you for investing with us.


Sincerely,

Dear Fellow Shareholders:



With behind us, now is a good time to review our portfolios and ensure that we are adhering to
a long-term, diversified investment strategy, which Ive mentioned in previous letters. The year
was notable for a number of reasons, but Im sure most of us are grateful for a return to more
stable markets and growing signs that the worst of the economic crisis is behind us.



Your Board continued to oversee the Invesco Funds with a strong sense of responsibility for
your savings and a deep appreciation for your continued trust. As always, we worked throughout
to manage costs and ensure Invesco continued to place investor interests first.


Im pleased to report that the latest report from Morningstar affirmed the work weve done and
included a number of positive comments regarding your Boards oversight of the Invesco Funds.


As background, Morningstar is a leading independent provider of investment research in North
America, Europe, Australia and Asia. Morningstar stated, A fund boards duty is to represent the
interests of fund shareholders, ensuring that the funds that it oversees charge reasonable fees and
are run by capable advisors with a sound investment process.
Morningstar maintained your Fund
Boards A grade for Board Quality, praising the Board for taking meaningful steps in recent
years to act in fund shareholders These steps included becoming much more
proactive and vocal in overseeing how Invesco votes the funds shareholders proxies and requiring
each fund trustee to invest more than one years board compensation in Invesco funds, further
aligning our interests with those of our shareholders. Morningstar also cited the work Ive done to
make myself more available to fund shareholders via email.


I am also pleased that Morningstar recognized the effort and the Fund Boards efforts over the
past several years to work together with management at Invesco to enhance performance and sharpen
the focus on investors.


As always, youre welcome to contact me at bruce@brucecrockett.com with any questions or
concerns you have. We look forward to representing and serving you.


Sincerely,

Dear Shareholders:



Enclosed is important information about your Fund and its performance.



Ive always believed that companies have an obligation to communicate regularly with their clients, and I believe
that obligation is especially critical in the investment industry.



Our website invesco.com/us offers timely market updates and commentary from many of our portfolio managers
and other investment professionals, as well as quarterly messages from me. At invesco.com/us, you also can obtain
information about your account at any hour of the day or night. I invite you to visit and explore the tools and
information we offer.



Invescos commitment to investment excellence



Invescos acquisition of Morgan Stanleys retail asset management business, including Van Kampen Investments,
broadened our range of investment products available to you. As a strong organization with a single focus investment
management Invesco today offers investment capabilities and products to meet the needs of virtually any investor. In
addition to traditional mutual funds, we manage a broad range of other solutions, including single-country, regional
and global investments spanning major equity, fixed income and alternative asset classes.



Investment excellence is our goal across our product line. Let me explain what that means. All of our funds are
managed by specialized teams of investment professionals. Each team has a discrete investment perspective and
philosophy, and all follow disciplined, repeatable processes governed by strong risk oversight. Our investment-centric
culture provides an environment that seeks to reduce distractions, allowing our fund managers to concentrate on what
they do best manage your money.



The importance of a broad product line and investment management expertise is obvious given the markets weve
experienced over the last two to three years. Weve seen that investment strategies can outperform or underperform their
benchmark indexes for a variety of reasons, including where we are in the market cycle, and whether prevailing economic
conditions are favorable or unfavorable for that strategy. Thats why no investment strategy can guarantee top-tier
performance at all times. What investors can expect, and what Invesco offers, are funds that are managed according to
their stated investment objectives and strategies, with robust risk oversight using consistent, repeatable investment
processes that dont change as short-term external conditions change investments managed for the long term. This
disciplined approach cant guarantee a profit; no investment can do that, since all involve some measure of risk. But
it can ensure that your money is managed the way we said it would be.



This adherence to stated investment objectives and strategies allows your financial advisor to build a diversified
portfolio that meets your individual risk tolerance and financial goals. It also means that when your goals change,
your financial advisor will be able to find an appropriate investment option to meet your needs.



Invescos commitment to you



Invescos commitment to you remains stronger than ever. Its one of the reasons weve grown to become one of the
worlds leading asset managers.



If you have questions about your account, please contact one of our client service representatives at
If you have a general Invesco-related question or comment for me, I invite you to email me directly at phil@invesco.com.



I want to thank you for placing your trust in us. All of us at Invesco look forward to serving your investment
management needs for many years to come. Thank you for investing with us.



Sincerely,

Dear Fellow Shareholders:



With behind us, now is a good time to review our portfolios and ensure that we are adhering to a long-term,
diversified investment strategy, which Ive mentioned in previous letters. The year was notable for a number of
reasons, but Im sure most of us are grateful for a return to more stable markets and growing signs that the worst of
the economic crisis is behind us.



Your Board continued to oversee the Invesco Funds with a strong sense of responsibility for your savings and a
deep appreciation for your continued trust. As always, we worked throughout to manage costs and ensure Invesco
continued to place investor interests first.



Im pleased to report that the latest report from Morningstar affirmed the work weve done and included a number
of positive comments regarding your Boards oversight of the Invesco Funds. As background, Morningstar is a leading
independent provider of investment research in North America, Europe, Australia and Asia. Morningstar stated, A fund
boards duty is to represent the interests of fund shareholders, ensuring that the funds that it oversees charge
reasonable fees and are run by capable advisors with a sound investment process.



Morningstar maintained your Fund Boards A grade for Board Quality, praising the Board for taking meaningful
steps in recent years to act in fund shareholders These steps included becoming much more proactive and
vocal in overseeing how Invesco votes the funds shareholders proxies and requiring each fund trustee to invest more
than one years board compensation in Invesco funds, further aligning our interests with those of our shareholders.
Morningstar also cited the work Ive done to make myself more available to fund shareholders via email.



I am also pleased that Morningstar recognized the effort and the Fund Boards efforts over the past several years
to work together with management at Invesco to enhance performance and sharpen the focus on investors.



As always, youre welcome to contact me at bruce@brucecrockett.com with any questions or concerns you have. We look
forward to representing and serving you.



Sincerely,

Dear Shareholders:



Enclosed is important information about your Fund and its performance.



Ive always believed that companies have an obligation to communicate regularly with their
clients, and I believe that obligation is especially critical in the investment industry.



Our website invesco.com/us offers timely market updates and commentary from many of our
portfolio managers and other investment professionals, as well as quarterly messages from me. At
invesco.com/us, you also can obtain information about your account at any hour of the day or night.
I invite you to visit and explore the tools and information we offer.



Invescos commitment to investment excellence



Invescos acquisition of Morgan Stanleys retail asset management business, including Van
Kampen Investments, broadened our range of investment products available to you. As a strong
organization with a single focus investment management Invesco today offers investment
capabilities and products to meet the needs of virtually any investor. In addition to traditional
mutual funds, we manage a broad range of other solutions, including single-country, regional and
global investments spanning major equity, fixed income and alternative asset classes.



Investment excellence is our goal across our product line. Let me explain what that means. All
of our funds are managed by specialized teams of investment professionals. Each team has a discrete
investment perspective and philosophy, and all follow disciplined, repeatable processes governed by
strong risk oversight. Our investment-centric culture provides an environment that seeks to reduce
distractions, allowing our fund managers to concentrate on what they do best manage your money.



The importance of a broad product line and investment management expertise is obvious given
the markets weve experienced over the last two to three years. Weve seen that investment
strategies can outperform or underperform their benchmark indexes for a variety of reasons,
including where we are in the market cycle, and whether prevailing economic conditions are
favorable or unfavorable for that strategy. Thats why no investment strategy can guarantee
top-tier performance at all times. What investors can expect, and what Invesco offers, are funds
that are managed according to their stated investment objectives and strategies, with robust risk
oversight using consistent, repeatable investment processes that dont change as short-term
external conditions change investments managed for the long term. This disciplined approach
cant guarantee a profit; no investment can do that, since all involve some measure of risk. But it
can ensure that your money is managed the way we said it would be.



This adherence to stated investment objectives and strategies allows your financial advisor to
build a diversified portfolio that meets your individual risk tolerance and financial goals. It
also means that when your goals change, your financial advisor will be able to find an appropriate
investment option to meet your needs.



Invescos commitment to you



Invescos commitment to you remains stronger than ever. Its one of the reasons weve grown to
become one of the worlds leading asset managers.



If you have questions about your account, please contact one of our client service
representatives at If you have a general Invesco-related question or comment for me,
I invite you to email me directly at phil@invesco.com.



I want to thank you for placing your trust in us. All of us at Invesco look forward to serving
your investment management needs for many years to come. Thank you for investing with us.



Sincerely,

Dear Fellow Shareholders:



With behind us, now is a good time to review our portfolios and ensure that we are adhering to
a long-term, diversified investment strategy, which Ive mentioned in previous letters. The year
was notable for a number of reasons, but Im sure most of us are grateful for a return to more
stable markets and growing signs that the worst of the economic crisis is behind us.



Your Board continued to oversee the Invesco Funds with a strong sense of responsibility for
your savings and a deep appreciation for your continued trust. As always, we worked throughout
to manage costs and ensure Invesco continued to place investor interests first.



Im pleased to report that the latest report from Morningstar affirmed the work weve done and
included a number of positive comments regarding your Boards oversight of the Invesco Funds.



As background, Morningstar is a leading independent provider of investment research in North
America, Europe, Australia and Asia. Morningstar stated, A fund boards duty is to represent the
interests of fund shareholders, ensuring that the funds that it oversees charge reasonable fees and
are run by capable advisors with a sound investment process.



Morningstar maintained your Fund Boards A grade for Board Quality, praising the Board for
taking meaningful steps in recent years to act in fund shareholders These
steps included becoming much more proactive and vocal in overseeing how Invesco votes the funds
shareholders proxies and requiring each fund trustee to invest more than one years board
compensation in Invesco funds, further aligning our interests with those of our shareholders.
Morningstar also cited the work Ive done to make myself more available to fund shareholders via
email.



I am also pleased that Morningstar recognized the effort and the Fund Boards efforts over the
past several years to work together with management at Invesco to enhance performance and sharpen
the focus on investors.



As always, youre welcome to contact me at bruce@brucecrockett.com with any questions or
concerns you have. We look forward to representing and serving you.



Sincerely,
