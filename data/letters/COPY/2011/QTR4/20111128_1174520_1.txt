Dear Shareowner,

During the first three quarters of the U.S. economy struggled to gain
solid footing. The economy went through a soft patch in the first half, and the
second half, so far, has been highlighted by the U.S. government's battle over
the debt ceiling and Standard & Poor's downgrade of the U.S. Treasury's credit
rating from the top rating of "AAA" for the first time in history. After
rallying nicely in the first half, U.S. equity markets reacted sharply this
summer to the political stalemate and the downgrade. There has been continued
pressure on equities due to concerns about the growing European sovereign-debt
crisis and its potential impact on the global economy.

Despite legitimate reasons for concern, we believe there are also reasons for
optimism that the U.S. economy will continue to exhibit low to flat growth and
not head into a severe recession. Corporations continue to post solid earnings
and, for the most part, are maintaining their positive earnings outlooks. They
also have strong balance sheets with improved net leverage and high cash levels.
Auto production has rebounded following the Japanese supply chain interruptions
caused by the earthquake and tsunami last spring. Retail sales growth
year-over-year has remained strong despite low consumer confidence. And despite
high unemployment in the U.S., private sector employment has grown consistently
since February There are certainly risks to our outlook, including
possible contagion from the European sovereign-debt and banking crisis, the
fiscal drag from federal and state budget cuts in the U.S., as well as potential
"negative feedback loops" from capital market volatility. But broadly speaking,
we think this subpar economic recovery is consistent with recoveries from other
"balance sheet"-caused recessions.

The difficult recovery process has been accompanied by wide market swings. While
this is a challenging environment, our investment professionals continue to
focus on finding good opportunities to invest in both equity and bond markets
using the same disciplined approach Pioneer has used since Our approach is
to identify undervalued individual securities with the greatest potential for
success, carefully weighing risk against reward. Our teams of investment
professionals continually monitor and analyze the relative valuations of
different sectors and securities globally to help build portfolios that we
believe can help you achieve your investment goals.

Pioneer Absolute Return Credit Fund | Semiannual Report |


At Pioneer, we have long advocated the benefits of staying diversified and
investing for the long term. The strategy has generally performed well for many
investors. Our advice, as always, is to work closely with a trusted financial
advisor to discuss your goals and work together to develop an investment
strategy that meets your individual needs. There is no single best strategy that
works for every investor.

We invite you to learn more about Pioneer and our time-tested approach to
investing by consulting with your financial advisor or visiting us online at
us.pioneerinvestments.com. We greatly appreciate your trust in us and we thank
you for investing with Pioneer.

Sincerely,

Dear Shareowner,

During the first three quarters of the U.S. economy struggled to gain
solid footing. The economy went through a soft patch in the first half, and the
second half, so far, has been highlighted by the U.S. government's battle over
the debt ceiling and Standard & Poor's downgrade of the U.S. Treasury's credit
rating from the top rating of "AAA" for the first time in history. After
rallying nicely in the first half, U.S. equity markets reacted sharply this
summer to the political stalemate and the downgrade. There has been continued
pressure on equities due to concerns about the growing European sovereign-debt
crisis and its potential impact on the global economy.

Despite legitimate reasons for concern, we believe there are also reasons for
optimism that the U.S. economy will continue to exhibit low to flat growth and
not head into a severe recession. Corporations continue to post solid earnings
and, for the most part, are maintaining their positive earnings outlooks. They
also have strong balance sheets with improved net leverage and high cash
levels. Auto production has rebounded following the Japanese supply chain
interruptions caused by the earthquake and tsunami last spring. Retail sales
growth year-over-year has remained strong despite low consumer confidence. And
despite high unemployment in the U.S., private sector employment has grown
consistently since February There are certainly risks to our outlook,
including possible contagion from the European sovereign-debt and banking
crisis, the fiscal drag from federal and state budget cuts in the U.S., as well
as potential "negative feedback loops" from capital market volatility. But
broadly speaking, we think this subpar economic recovery is consistent with
recoveries from other "balance sheet"-caused recessions.

The difficult recovery process has been accompanied by wide market swings.
While this is a challenging environment, our investment professionals continue
to focus on finding good opportunities to invest in both equity and bond
markets using the same disciplined approach Pioneer has used since Our
approach is to identify undervalued individual securities with the greatest
potential for success, carefully weighing risk against reward. Our teams of
investment professionals continually monitor and analyze the relative
valuations of different sectors and securities globally to help build
portfolios that we believe can help you achieve your investment goals.


Pioneer Fundamental Growth Fund | Semiannual Report |


At Pioneer, we have long advocated the benefits of staying diversified and
investing for the long term. The strategy has generally performed well for many
investors. Our advice, as always, is to work closely with a trusted financial
advisor to discuss your goals and work together to develop an investment
strategy that meets your individual needs. There is no single best strategy
that works for every investor.

We invite you to learn more about Pioneer and our time-tested approach to
investing by consulting with your financial advisor or visiting us online at
us.pioneerinvestments.com. We greatly appreciate your trust in us and we thank
you for investing with Pioneer.

Sincerely,

Dear Shareowner,

During the first three quarters of the U.S. economy struggled to gain
solid footing. The economy went through a soft patch in the first half, and the
second half, so far, has been highlighted by the U.S. government's battle over
the debt ceiling and Standard & Poor's downgrade of the U.S. Treasury's credit
rating from the top rating of "AAA" for the first time in history. After
rallying nicely in the first half, U.S. equity markets reacted sharply this
summer to the political stalemate and the downgrade. There has been continued
pressure on equities due to concerns about the growing European sovereign-debt
crisis and its potential impact on the global economy.

Despite legitimate reasons for concern, we believe there are also reasons for
optimism that the U.S. economy will continue to exhibit low to flat growth and
not head into a severe recession. Corporations continue to post solid earnings
and, for the most part, are maintaining their positive earnings outlooks. They
also have strong balance sheets with improved net leverage and high cash
levels. Auto production has rebounded following the Japanese supply chain
interruptions caused by the earthquake and tsunami last spring. Retail sales
growth year-over-year has remained strong despite low consumer confidence. And
despite high unemployment in the U.S., private sector employment has grown
consistently since February There are certainly risks to our outlook,
including possible contagion from the European sovereign-debt and banking
crisis, the fiscal drag from federal and state budget cuts in the U.S., as well
as potential "negative feedback loops" from capital market volatility. But
broadly speaking, we think this subpar economic recovery is consistent with
recoveries from other "balance sheet"-caused recessions.

The difficult recovery process has been accompanied by wide market swings.
While this is a challenging environment, our investment professionals continue
to focus on finding good opportunities to invest in both equity and bond
markets using the same disciplined approach Pioneer has used since Our
approach is to identify undervalued individual securities with the greatest
potential for success, carefully weighing risk against reward. Our teams of
investment professionals continually monitor and analyze the relative
valuations of different sectors and securities globally to help build
portfolios that we believe can help you achieve your investment goals.


Pioneer Multi-Asset Floating Rate Fund | Semiannual Report |


At Pioneer, we have long advocated the benefits of staying diversified and
investing for the long term. The strategy has generally performed well for many
investors. Our advice, as always, is to work closely with a trusted financial
advisor to discuss your goals and work together to develop an investment
strategy that meets your individual needs. There is no single best strategy
that works for every investor.

We invite you to learn more about Pioneer and our time-tested approach to
investing by consulting with your financial advisor or visiting us online at .
We greatly appreciate your trust in us and we thank you for investing with
Pioneer.

Sincerely,
