Dear Fellow Shareholders:



Performance highlights (September

The Reynolds Blue Chip Growth Fund has a overall rating from Morningstar. This is Morningstars highest overall mutual fund rating. The performance of the Reynolds Blue Chip Growth Fund (the Fund or the Blue Chip Fund) in the twelve months ended September was The performance of the Standard &amp; Poors during that same period was The annualized average total returns of the Reynolds Blue Chip Growth Fund for the and periods through September were + respectively. The annualized average total returns for the Funds benchmark, the Standard &amp; Poors for the and periods through September were and respectively.






























The Reynolds Blue Chip Growth Fund has an Overall Morningstar Rating of Stars. This is out of funds in Morningstars Large Growth Category which is the Funds Morningstar Category. The Overall Morningstar Rating for a fund is derived from a weighted-average of the performance figures associated with its three-, five-, and ten -year (if applicable) ratings, based on risk-adjusted returns. For each fund with at least a three-year history, Morningstar calculates a Morningstar Rating based on a Morningstar Risk-Adjusted Return measure that accounts for variation in a funds monthly performance (including the effects of sales charges, loads, and redemption fees), placing more emphasis on downward variations and rewarding consistent performance. The top of funds in each category receive stars, the next receive stars, the next receive stars, the next receive stars and the bottom receive star. Past performance is no guarantee of future results.














The Standard &amp; Poors Index consists of selected common stocks, most of which are listed on the New York Stock Exchange. The Standard &amp; Poors Ratings Group designates the stocks to be included in the Index on a statistical basis. A particular stocks weighting in the Index is based on its relative total market value (i.e. its market price per share times the number of shares outstanding.) Stocks may be added or deleted from the Index from time to time.






Investment Strategy



Ideally I would always invest in stocks for the long-term and have low turnover. However, it is very important to closely analyze intermediate-term potential economic cycles and resulting stock market problems or positives. In October I became increasingly concerned regarding several potential economic problems. I believed that one of these potential problems was high housing prices, and the increasingly speculative financial instruments that had evolved in this segment of the economy. As a result, I began implementing a strong defensive investment strategy for the Reynolds Blue Chip Growth Fund by selling equities held in the Fund and raising the cash position. The beginning of the implementation of this defensive investment strategy coincided within a few days of the intermediate top of the stock market in October I began purchasing equities for the Blue Chip Fund in March as I believed that the prices of many high


























quality equities had declined to attractive long-term buying ranges and the massive amount of stimulus that was being implemented worldwide would be a positive. The beginning of the implementation of this more normal investment strategy also coincided within a few days of the intermediate bottom of the stock market.



The U.S. Economy



The U.S. economic recovery that started in has continued so far in The U.S. economy is growing, but at a lower than ideal rate. For example, growth of or better is needed to begin meaningfully reducing unemployment which is at In the last two months the economy has been growing faster than was forecast a few months ago.U.S. Gross Domestic Product (GDP) is estimated to have expanded at an inflation-adjusted annual rate of in the quarter ended September helped by stronger than expected consumer spending and business investment. Americans spent more on services such as health care, restaurants and finance. This is the most robust quarterly performance in a year and eased anxiety that the recovery may be stalling. U.S. GDP is forecast to expand at a rate in the quarter ended December GDP increased at a rate in the quarter ended June at a rate in the quarter ended March at a rate in the quarter ended December at a rate in the quarter ended September at a in the quarter ended June at a rate in the quarter ended March at a rate in the quarter ended December and at a rate in the quarter ended September Prior to that, the economy decreased for four consecutive quarters. For the next six months the outlook remains for good but less than ideal growth accompanied by high unemployment, modest inflation pressures and no change in the Federal Reserve policy on the federal funds rate. The economy will continue to be characterized by large fiscal deficits. The Fed completed the second round of economic stimulus in June The Fed recently began Operation Twist by purchasing longer term maturity Treasury Bonds and selling shorter term maturity Treasury Bonds. This tends to moderately lower longer term interest rates. This should add a moderate amount to economic growth.



There are some current and potential economic and investment negatives at the present time including: hiring continues cautious and unemployment at is still high, more than two and one half years after the economy started growing; oil prices including higher gasoline prices continue to negatively affect economic growth, although prices have recently declined; rates are low, but mortgage credit is still somewhat tight; federal, state and localgovernment spending remains weak; inflation, while modest, continues to creep upward; the U.S. deficit needs to be reduced; there are major economic problems with some individual Euro-zone countries such as Greece, Italy, Ireland, Portugal, and Spain which may spread to other countries; the European Commission recently lowered its Euro-zone forecast for to from the Euro-zone economy may slip into a recession in the near term; Euro-zone problems and uncertainties may continue for some time; and economic problems in Europe may negatively affect U.S. economic growth.




Some current and potential economic and investment positives are: the Conference Boards confidence gauge and the Thomson/Reuters/University of Michigan index of consumer sentiment recently improved; consumer spending, which accounts for about of the economy, has recently increased; manufacturing demand and output are showing gains; capital spending on equipment and software has been increasing; employment and payrolls are increasing although at a modest rate; there is modest growth in commercial and residential construction; near term energy costs including gasoline have recently decreased; there has recently been a modest increase in new home sales; productivity has recently increased; there has been an increase in the willingness of companies to commit capital as evidenced by the increase in merger and acquisition activity; the economy has grown in the last eight quarters through the September quarter and growth should continue in the near term; growth in the service sector is broadening; businesses have been able to use the credit markets to strengthen their balance sheets; short-term interest rates remain low helped by a low Fed Funds rate; long-term Treasury rates have fallen in response to the problems in Europe; many companies reported revenue growth, not just earnings growth from cutting expenses in the most recent quarters; European leaders agreed to expand a bailout fund; the European Central Bank (ECB) recently cut its benchmark interest rate by to to boost growth; and the current valuation of stocks is at the low end of historical ranges.




The U.S. economic recovery continues to be affected by a number of secular factors that are altering the pace and composition of growth. The economy in is being affected by greater prudence and less speculation in lending, high unemployment and less leverage for consumer spending. GDP increased in after decreasing in calendar after no change in calendar and increasing in in in in in in in and in GDP is forecast to increase in and in



U.S. inflation numbers have been helped in the last few years by such factors as: global competition; advances in technology resulting in increasing productivity; and technology innovations that are helping to lower production and distribution costs.Inflation, as measured by the Consumer Price Index, increased in after decreasing in and increasing in in




























in in in in in in and in U.S. inflation is forecast to increase in and in



The World Economy



The global economic recovery that started in has continued in Growth is slowing, but outside of Europe, most economies do not appear to be contracting. The expansion today, except for the Euro-zone, appears to be more sustainable than it was a year ago because domestic demand is better in many economies and accommodative financial conditions remain largely in place in most countries. Recovery is strongest in Asia with China having the strongest growth. However, Chinas growth rate has recently slowed to Other Asian countries are having good growth rates as well. Recovery is also occurring in Latin America and Brazil. Some developing countries in the world have been growing faster than the U.S. in the last few years. Their economies continued to grow faster than the U.S. during the most recent worldwide economic slowdown and their economies are continuing to grow faster. The biggest risk to Euro-zone economic growth is related to the potential sovereign government debt crises that have appeared.Europe needs significant fiscal retrenchment in the next few years which is slowing their economic growth.The Euro-zone may slip into a recession and real GDP may decrease over the next two quarters.




The World Economy increased in after decreasing in and is forecast to increase in and in Among advanced economies: Japan increased in after decreasing in and is forecast to decrease in and increase in the Euro-zone increased in after decreasing in and is forecast to increase in and in the UnitedKingdom increased in after decreasing in and is forecast to increase in and in Canada increased in after decreasing in and is forecast to increase in and in and Korea increased in after increasing in and is forecast to increase in and in




The biggest developing economies are many times referred to as the BRIC economies, which is short for Brazil, Russia, India, and China. Chinas population is approximately of the worlds total population of approximately seven billion. Many economists believe that China has a particularly good long-term outlook. In the second quarter of China overtook Japan and became the worlds second largest economy after the U.S. It is the worlds fastest growing major economy. China was one of the first countries to show a pickup in growth as the recession was ending in and it helped to lead the world out of recession. Chinas strong growth is resulting in policy makers withdrawing record fiscal and monetary stimulus. For example, bank lending has been tightened and interest rates have been raised several times. GDP increased in after increasing in and is forecast to increase in and in




Indias population is approximately of the worlds population. It is the worlds second fastest growing economy. Indias economy increased in after increasing in and is forecast to increase in and in




Brazil is Latin Americas biggest economy. Brazil emerged from its first recession since in the second quarter of increased in after decreasing in and is forecast to increase in and in Russias economy grew at in after decreasing in and is forecast to increase in and in




Many worldwide larger multinational companies should be well positioned to benefit long-term from worldwide growth. To the extent that some of these companies U.S. earnings are growing slower, this could be somewhat offset by their possible stronger foreign earnings. The long-term strategy of the Reynolds Blue Chip Growth Fund is to be structured to benefit from this worldwide growth by investing in many of these leading multinational growth companies.




The Blue Chip Fund is positioned to participate in long-term worldwide growth trends through investments in multinational U.S. headquartered companies. In addition, the Fund has investments in leading foreign headquartered companies, whose stocks or American Depositary Receipts (ADRs) trade in the United States. These ADRs are denominated in dollars and they must use GAAP (Generally Accepted Accounting Principles) accounting to qualify as an ADR. The Blue Chip Fund may hold up to of its assets in ADRs.




Opportunistic Investing in Companies of Various Sizes and Diversified Among Various Industries



The Reynolds Blue Chip Growth Fund usually invests in companies of various sizes as classified by their market capitalizations. A companys market capitalization is calculated by taking the number of shares the company has outstanding multiplied by its current market price.Other considerations in selecting companies for the Fund include revenue growth rates, product innovations, financial strength, managements knowledge and experience plus the overall economic and geopolitical environments and interest rates.The Funds investments are diversified among various industries.




The long-term strategy of the Reynolds Blue Chip Growth Fund is to emphasize investment in worldwide blue chip growth companies.These companies are defined as companies with a minimum market capitalization of billion. In the long-term these companies build value as their earnings grow. This growth in value should ultimately be recognized in higher stock prices for these companies.


























Industry as of September





















The Global Industry Classification Standard (GICS) was developed by and/or is the exclusive property of MSCI, Inc. and Standard &amp; Poors Financial Services LLC (S&amp;P). GICS is a service mark of MSCI and S&amp;P and has been licensed for use by U.S. Bancorp Fund Services, LLC.






Low Long-Term Interest Rates by Historical Standards are a Significant Positive for Stock Valuations




Long-term interest rates remain near historically low levels. Low long-term interest rates usually result in higher stock valuations for many reasons including:











Long-term borrowing costs of corporations are lower resulting in higher business confidence and profits.














Long-term borrowing costs of individuals are lower which increases consumer confidence and spending.














A companys stock is usually valued by placing a present value on that companys future stream of earnings and dividends. The present value is higher when interest and inflation rates are low.






Linked Money Market Fund



The First American Treasury Obligations Fund is a money market fund offered by an affiliate of our transfer agent, U.S. Bancorp Fund Services, LLC. This Fund is offered as a money market alternative to our shareholders. The First American Treasury Obligations Fund offers many free shareholder conveniences including automatic investment and withdrawal plans and check writing access to your funds and is linked to your holdings in the Reynolds Blue Chip Growth Fund.This Fund is also included on your quarterly statements.




Information about the Reynolds Blue Chip Growth Fund and the First American Treasury Obligations Fund



Reynoldsfunds.com website: You can access current information about your investment holdings via our website, reynoldsfunds.com. You must first request a personal identification number (PIN) by calling our shareholder service representatives at You will be able to view your account list, account detail (including balances), transaction history, distributions, and the current Reynolds Blue Chip Growth Fund net asset value. Additional information available (PIN number not needed) includes quarterly updates of the returns of the Blue Chip Fund, top ten holdings and industry percentages. Also, detailed statistics and graphs of past performances from a link to Morningstar for the Blue Chip Fund.



For automatic current daily net asset values:Call twenty-four hours a day, seven days a week and press any key then The updated current net asset value for the Blue Chip Fund is usually available each business day after P.M. (PST).



For the Reynolds Blue Chip Growth Fund shareholders to automatically access their current account information:Call (twenty-four hours a day, seven days a week), press any key then and enter your digit account number which appears at the top right of your statement.



To speak to a Fund representative regarding the current daily net asset value, current account information and any other questions:Call and press from A.M. to P.M. (PST).



Shareholder statement frequency:Consolidated statements summarizing the Blue Chip Fund and First American Treasury Obligations Fund accounts held by a shareholder are sent quarterly. In addition, individual Blue Chip Fund statements are sent whenever a transaction























occurs. These transactions are: statements are sent for the Blue Chip Fund or First American Treasury Obligations Fund when a shareholder purchases or redeems shares; Blue Chip Fund statements are sent twice a year if, and when, any ordinary income or capital gains are distributed.



Tax reporting:Individual forms, which summarize any dividend income and any long- or short-term capital gains, are sent annually to shareholders each January. The percentage of income earned from various government securities, if any, for the Blue Chip Fund and the First American Treasury Obligations Fund are also reported in January.



Minimum for regular and retirement accounts for additional investments for all accounts except for the Automatic Investment Plan, which is for regular and retirement plan accounts).



Retirement plans:All types are offered including Traditional IRA, Roth IRA, Coverdell Education Savings Account, SIMPLE IRA Plan, and SEP IRA.



Automatic Investment Plan:There is no charge to automatically debit your checking account to invest in the Blue Chip Fund or the First American Treasury Obligations Fund minimum for either of these Funds) at periodic intervals to make automatic purchases in either of these Funds. This is useful for dollar cost averaging for the Blue Chip Fund.



Systematic Withdrawal Plan:For shareholders with a minimum starting balance, there is no charge to automatically redeem shares minimum) in the Blue Chip Fund or the First American Treasury Obligations Fund as often as monthly and send a check to you or transfer funds to your bank account.



Free Check Writing:Free check writing minimum) is offered for accounts invested in the First American Treasury Obligations Fund.



Exchanges or regular redemptions between the Blue Chip Fund and the First American Treasury Obligations Fund:As often as desired no charge.



NASDAQ symbols:Reynolds Blue Chip Growth Fund RBCGX and First American Treasury Obligations Fund FATXX.



Portfolio Manager:Frederick Reynolds is the portfolio manager of the Reynolds Blue Chip Growth Fund.



The Reynolds Blue Chip Growth Fund and the First American Treasury Obligations Fund are No-Load:No front-end sales commissions or deferred sales charges (loads) are charged. Over of all mutual funds impose these marketing charges that are ultimately paid by the shareholder. These marketing charges are either: a front-end fee or load in which up to of a shareholders assets are deducted from the original investment (some funds even charge a fee when a shareholder reinvests capital gains or dividends); or a back-end penalty fee or load which is typically deducted from a shareholders account if a shareholder redeems within five years of the original investment. These fees reduce a shareholders return. The Blue Chip Fund and First American Treasury Obligations Fund are No-Load as they do not have these extra charges.



We appreciate your continued confidence in the Reynolds Blue Chip Growth Fund and would like to welcome our new shareholders. We look forward to strong results in the future.



Sincerely,
