Dear Hillman Fund Shareholder,




We are pleased to provide the annual report for the Hillman Funds for the fiscal year ended




We have enclosed the attached commentary to remind our shareholders of Hillman Capital Managements approach and to share some perspective on current economic conditions.




On behalf of the team at Hillman Capital Management, I thank you for your ongoing confidence.It is our hope that we may continue to serve you throughout the years to come.




Sincerely,
