Dear Shareholder,
Despite notable volatility in the U.S. stock market, Vanguard U.S. Value Fund ended the fiscal year with a modest gain. For the 12 months ended September 30, the fund returned 0.89%, a result that was more than 3 percentage points ahead of that of its benchmark, the Russell 3000 Value Index, and almost 5 percentage points above the average return of multi-cap value funds.
The fund’s advisor, Vanguard Quantitative Equity Group, uses computer models designed to find companies with strong earnings growth potential and undervalued stocks. The advisor’s strong stock selection in the financial, consumer discretionary, and health care sectors helped the fund to outperform its comparative standards for the year.
If you own shares of the fund in a taxable account, you may wish to review information on the fund’s after-tax returns that appears later in this report.
2
gridlock on display during the debt-ceiling debate. Vanguard’s confidence in the “full faith and credit” of the U.S. Treasury remains unshaken.)
The U.S. stock market’s second-half weakness sapped its first-half strength. The broad market returned 0.31% for the full 12 months. International stocks, which gained less at the start of the year and lost more at the end, returned –10.81% in U.S. dollars.
The yields of money market instruments hovered near zero, as they have since December 2008, when the Federal Reserve cut its target for short-term interest rates to between 0% and 0.25%. Toward the end of the period, the Fed indicated that it expected to maintain this exceptionally low target at least through mid-2013.
 
3
In the consumer discretionary sector, the fund benefited from owning stocks of high-end apparel makers and media companies. Despite meager growth in the U.S. economy and a lackluster job market, consumers have continued to spend on brand-name clothes and cable and satellite services.
The fund expense ratio shown is from the prospectus dated May 13, 2011, and represents estimated costs for the current fiscal year. For the fiscal year ended September 30, 2011, the fund’s expense ratio was 0.29%. The peer-group expense ratio is derived from data provided by Lipper Inc. and captures information through year-end 2010.
Peer group: Multi-Cap Value Funds.
4
Health care, considered a defensive sector because its companies provide essential services regardless of economic conditions, delivered the strongest return for the fiscal year. The fund’s holdings among managed health care providers and pharmaceutical companies boosted its return relative to the index. Since the passage of health care reform legislation last year, investors have been more confident in the financial standing of these types of companies.
The stock market has endured a lot of turbulence over the past ten years: the aftermath of the tech bubble, the financial crisis in 2008, and more recently, the U.S. and European debt dramas. The fund’s shortfall relative to its index and peers in
The figures shown represent past performance, which is not a guarantee of future results. (Current performance may be lower or higher than the performance data cited. For performance data current to the most recent month-end, visit our website at vanguard.com/performance.) Note, too, that both investment returns and principal value can fluctuate widely, so an investor’s shares, when sold, could be worth more or less than their original cost.
5
part reflects the fact that during much of this period, the quantitative strategies used by its current and previous advisors was out of favor. For example, you may recall that as markets recovered from the crushing losses of 2008, it was often the stocks of marginal, financially troubled companies that enjoyed the biggest gains, boosted by investors’ renewed appetite for risk. The advisor’s approach, which tends to favor the stocks of high-quality, reasonably valued companies, was out of step, at times, with the prevailing mood. Indeed, it was only in the past fiscal year that the tide turned and the types of stocks favored by your fund’s advisor enjoyed a resurgence.
We remain confident in the disciplined, quantitative investment process employed by the advisor, and we believe this approach can produce benchmark-beating results over the long term.
Maintaining a balanced portfolio is a prudent response to this never-ending, always unpredictable change in the markets. Vanguard U.S. Value Fund, with its broad diversification among value stocks, can play an important role in such a portfolio.
Thank you for entrusting your assets to Vanguard.
Sincerely,


Dear Shareholder,
In a market that turned volatile as the fiscal year progressed, Vanguard Capital Value Fund returned –10.00% for the 12 months ended September 30, 2011. By comparison, the fund’s benchmark, the Russell 3000 Value Index, returned about –2%, and the average return of its multi-cap value funds peers was about –4%.
Value holdings fared worse than growth during the period as value-oriented sectors such as banks and brokerages continued to grapple with the legacy of the financial crisis. The fund’s disappointing performance reflected both this investing landscape and the advisor’s subpar stock choices, especially in the financial, energy, and consumer discretionary sectors.
If you hold shares in a taxable account, you may wish to review the table and discussion on after-tax returns for the fund’s fiscal year, based on the highest tax bracket, later in this report.
2
U.S. credit rating, in large part because of the political gridlock on display during the debt-ceiling debate. Vanguard’s confidence in the “full faith and credit” of the U.S. Treasury remains unshaken.)
The U.S. stock market’s second-half weakness sapped its first-half strength. The broad market returned 0.31% for the full 12 months. International stocks, which gained less at the start of the year and lost more at the end, returned –10.81% in U.S. dollars.
The yields of money market instruments hovered near zero, as they have since December 2008, when the Federal Reserve cut its target for short-term interest rates to between 0% and 0.25%. Toward the end of the period, the Fed indicated that it expected to maintain this exceptionally low target at least through mid-2013.
 
3
Financial stocks, by far the portfolio’s largest allocation, declined about 23% as the European debt crisis plagued diversified financial services companies and banks. Companies that serve the housing and mortgage industry still struggled under the weight of the mortgage-related problems that triggered the financial crisis and the uncertain regulatory environment that ensued.
The energy sector was another trouble spot. Poor stock selection was the culprit as the fund’s energy stocks retreated
The fund expense ratio shown is from the prospectus dated January 28, 2011, and represents estimated costs for the current fiscal year. For the fiscal year ended September 30, 2011, the fund’s expense ratio was 0.58%. This increase from the estimated expense ratio reflects a performance-based investment advisory fee adjustment. When the performance adjustment is positive, the fund’s expenses increase; when it is negative, expenses decrease. The peer-group expense ratio is derived from data provided by Lipper Inc. and captures information through year-end 2010.
Peer group: Multi-Cap Value Funds.
4
nearly 18% compared with an increase of about 7% for the benchmark’s holdings. During the period, the advisor maintained a sizable commitment to coal producers, which were negatively affected by commodity prices and a downturn in the economic cycle.
Consumer discretionary stocks also floundered as fears of a double-dip recession threatened the economy. The potential for spending pullbacks haunted a variety of companies: automobiles, educational services, homebuilding, luxury goods, internet services, and travel. Again, the advisor’s stock choices were off the mark, particularly in educational services and travel.
Weakness was evident across the board. Subpar stock selection also hurt performance in information technology, telecommunication services, and materials. Bright spots were few and far between, although certain food companies boosted returns in consumer staples, a defensive sector that held up better than others during the downturn.
The figures shown represent past performance, which is not a guarantee of future results. (Current performance may be lower or higher than the performance data cited. For performance data current to the most recent month-end, visit our website at vanguard.com/performance.) Note, too, that both investment returns and principal value can fluctuate widely, so an investor’s shares, when sold, could be worth more or less than their original cost.
5
aggressive approach. For the period since its inception, as shown in the table accompanying this report, the fund posted an annualized return of 1.89%. Over the same period, the average annual return of its benchmark index was 3.12% and that of its peer group was 2.12%.
While the fund’s recent performance is an obvious disappointment, please be aware that both its peaks and its valleys have the potential to be more severe than those of the broad market. In 2010, Wellington Management added a manager with a complementary investment style and expanded the fund’s holdings in the expectation that the resulting less-concentrated portfolio would increase diversification and decrease volatility. But the potential for a bumpy ride still exists.
We remain highly confident in the abilities of Wellington Management, which has managed the Capital Value Fund since its inception and also manages or co-manages several other Vanguard funds. Along with Wellington’s experience and talent, you also benefit from the fund’s low investment costs, which allow you to keep a larger proportion of returns.
At Vanguard, we counsel you to focus on the long term rather than the short term and to create a balanced portfolio based on your unique goals, time horizon, and appetite for risk. This means diversifying across stocks, bonds, and money market funds as well as within these asset classes. If your investment needs include a more aggressive value fund, the Capital Value Fund can play an important supporting role in a broadly diversified portfolio.
As always, thank you for entrusting your assets to Vanguard.
Sincerely,


