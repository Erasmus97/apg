Dear Shareholder:

Global equity markets finished the third quarter in a significant downturn that intensified in late July and early August, as Congress
struggled to raise the debt ceiling and Standard &amp; Poor's announced it was lowering its long-term sovereign credit rating of the United
States. The historic downgrade, along with continued evidence of a slowing global economy, heightened investor anxiety and set off a wave of
volatility that hampered most major asset classes through the end of September. Financial markets are always unpredictable, of course, but
there are several time-tested investment principles that can help put the odds in your favor.

One of the basic tenets is to invest for the long term. Over time, riding out the markets' inevitable ups and downs has proven much more
effective than selling into panic or chasing the hottest trend. Even missing only a few of the markets' best days can significantly diminish investor returns. Patience also affords the benefits of compounding - of earning interest on additional income or reinvested dividends and
capital gains. There can be tax advantages and cost benefits to consider as well. While staying the course doesn't eliminate risk, it can considerably lessen the effect of short-term declines.

You can further manage your investing risk through diversification. And today, more than ever, geographic diversification should be taken
into account. Studies indicate that asset allocation is the single mostimportant determinant ofaportfolio's long-term success. The right mix
of stocks, bonds and cash - aligned to your particular risktolerance and investment objective - is very important. Age-appropriate rebalancing isalso an essential aspect of assetallocation. For younger investors, an emphasis on equities - which historically have been the
best-performing asset class over time - is encouraged. As investors near their specific goal, such as retirement orsending a child to college,
consideration may be given to replacing volatile assets (e.g. common stocks) with more-stable fixed investments (bonds or savings plans).

A third principle - investing regularly - can help lower the average cost of your purchases. Investing acertain amount of money each
month or quarter helps ensure you won't pay for all your shares at market highs. This strategy - known as dollar cost averaging - also reduces &quot;emotion&quot; from investing, helping shareholders avoid selling weak performers just prior to anupswing, or chasing a hot performer just
before a correction.

We invite you to contact us via the Internet, through our Investor Centers or by phone. It is our privilege to provide you the information you
need to make the investments that are right for you.

Sincerely,

Dear Shareholder:

Global equity markets finished the third quarter in a significant downturn that intensified in late July and early August, as Congress
struggled to raise the debt ceiling and Standard &amp; Poor's announced it was lowering its long-term sovereign credit rating of the United
States. The historic downgrade, along with continued evidence of a slowing global economy, heightened investor anxiety and set off a wave of
volatility that hampered most major asset classes through the end of September. Financial markets are always unpredictable, of course, but
there are several time-tested investment principles that can help put the odds in your favor.

One of the basic tenets is to invest for the long term. Over time, riding out the markets' inevitable ups and downs has proven much more
effective than selling into panic or chasing the hottest trend. Even missing only a few of the markets' best days can significantly diminish investor returns. Patience also affords the benefits of compounding - of earning interest on additional income or reinvested dividends and
capital gains. There can be tax advantages and cost benefits to consider as well. While staying the course doesn't eliminate risk, it can considerably lessen the effect of short-term declines.

You can further manage your investing risk through diversification. And today, more than ever, geographic diversification should be taken
into account. Studies indicate that asset allocation is the single mostimportant determinant ofaportfolio's long-term success. The right mix
of stocks, bonds and cash - aligned to your particular risktolerance and investment objective - is very important. Age-appropriate rebalancing isalso an essential aspect of assetallocation. For younger investors, an emphasis on equities - which historically have been the
best-performing asset class over time - is encouraged. As investors near their specific goal, such as retirement orsending a child to college,
consideration may be given to replacing volatile assets (e.g. common stocks) with more-stable fixed investments (bonds or savings plans).

A third principle - investing regularly - can help lower the average cost of your purchases. Investing acertain amount of money each
month or quarter helps ensure you won't pay for all your shares at market highs. This strategy - known as dollar cost averaging - also reduces &quot;emotion&quot; from investing, helping shareholders avoid selling weak performers just prior to anupswing, or chasing a hot performer just
before a correction.

We invite you to contact us via the Internet, through our Investor Centers or by phone. It is our privilege to provide you the information you
need to make the investments that are right for you.

Sincerely,

Dear Shareholder:

Global equity markets finished the third quarter in a significant downturn that intensified in late July and early August, as Congress
struggled to raise the debt ceiling and Standard &amp; Poor's announced it was lowering its long-term sovereign credit rating of the United
States. The historic downgrade, along with continued evidence of a slowing global economy, heightened investor anxiety and set off a wave of
volatility that hampered most major asset classes through the end of September. Financial markets are always unpredictable, of course, but
there are several time-tested investment principles that can help put the odds in your favor.

One of the basic tenets is to invest for the long term. Over time, riding out the markets' inevitable ups and downs has proven much more
effective than selling into panic or chasing the hottest trend. Even missing only a few of the markets' best days can significantly diminish investor returns. Patience also affords the benefits of compounding - of earning interest on additional income or reinvested dividends and
capital gains. There can be tax advantages and cost benefits to consider as well. While staying the course doesn't eliminate risk, it can considerably lessen the effect of short-term declines.

You can further manage your investing risk through diversification. And today, more than ever, geographic diversification should be taken
into account. Studies indicate that asset allocation is the single mostimportant determinant ofaportfolio's long-term success. The right mix
of stocks, bonds and cash - aligned to your particular risktolerance and investment objective - is very important. Age-appropriate rebalancing isalso an essential aspect of assetallocation. For younger investors, an emphasis on equities - which historically have been the
best-performing asset class over time - is encouraged. As investors near their specific goal, such as retirement orsending a child to college,
consideration may be given to replacing volatile assets (e.g. common stocks) with more-stable fixed investments (bonds or savings plans).

A third principle - investing regularly - can help lower the average cost of your purchases. Investing acertain amount of money each
month or quarter helps ensure you won't pay for all your shares at market highs. This strategy - known as dollar cost averaging - also reduces &quot;emotion&quot; from investing, helping shareholders avoid selling weak performers just prior to anupswing, or chasing a hot performer just
before a correction.

We invite you to contact us via the Internet, through our Investor Centers or by phone. It is our privilege to provide you the information you
need to make the investments that are right for you.

Sincerely,
