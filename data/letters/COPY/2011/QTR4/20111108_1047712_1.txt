Dear Shareholder: 
Last year, some welcome improvements in economic data appeared to indicate that the U.S. may finally be on course to economic recovery. 
Investors responded warmly to these signs by displaying their appetite for risk. The Standard &amp; Poor’s 500 (“S&amp;P 500” Index) shook off its summer malaise and embarked on an impressive
rally. Before long, however, investors’ enthusiasm was tempered by weaker global economic data and sovereign debt risks. Concerns about the credit downgrade of U.S.-issued debt exacerbated this negative sentiment among investors, and helped
trigger a major market downturn. In early August, each of the three major U.S. stock indices experienced their worst one-day performance since December 1, 2008. 
 
