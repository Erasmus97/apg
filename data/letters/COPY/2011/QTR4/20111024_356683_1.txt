Dear Shareholder: 
 
We hope you find the annual report for the Prudential Jennison Blend Fund, Inc. informative and useful. Because of ongoing market volatility, we understand that this is a difficult time to be an investor. While it
is impossible to predict what the future holds, we continue to believe a prudent response to uncertainty is to maintain a diversified portfolio, including stock and bond mutual funds consistent with your tolerance for risk, time horizon, and
financial goals. 
 
A diversified asset allocation offers two potential
advantages: It limits your exposure to any particular asset class; plus it provides a better opportunity to invest some of your assets in the right place at the right time. Your financial professional can help you create a diversified investment
plan that may include mutual funds covering all the basic asset classes and that reflects your personal investor profile and risk tolerance. Keep in mind that diversification and asset allocation strategies do not assure a profit or protect against
loss in declining markets. 
 
®
 
Thank you for choosing the Prudential Investments family of mutual funds. 
 
Sincerely, 


