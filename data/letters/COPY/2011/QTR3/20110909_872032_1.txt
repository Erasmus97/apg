Dear Fellow Shareholder:



The Management of Tax-Free Trust of Arizona has been privileged to serve you and our other shareholders since the Trusts inception in more than years ago.



One of the benefits of being in existence so long is that we have experienced a number of up and down economic cycles and endured a variety of changes and challenges. In short, we have generally seen it all before. Back in we produced a Thought for the Month entitled The Sky is Falling! It was intended to calm any fears that our then shareholders might have had as a result of newspaper and magazine articles which were predicting that the municipal bond market was about to crumble.



Here we are years later and similar articles have been appearing once again in both the financial and local press. While the facts and circumstances may be somewhat different, we believe that what we said back in still holds true for us today.



Certainly there is a degree of risk with virtually everything in life. However, we do not believe that the sky is falling. Furthermore, all of us associated with Tax-Free Trust of Arizona take very deliberate steps each and every day in pursuit of your Trusts objective of seeking to provide you with as high a level of double tax-free income as is consistent with preservation of capital.



The following are among those deliberate steps we take each day in managing your Trust:













In line with Tax-Free Trust of Arizonas prospectus, we may only purchase investment grade securities securities rated within the four highest credit ratings assigned by nationally recognized statistical rating organizations - or if unrated, determined by your investment team to be of comparable quality. In addition to credit characteristics, we also look at an issues maturity and sector (in order to enhance diversification and meet other requirements identified by your portfolio management team).
















We invest in an issue based on our initial research. Then we monitor the ongoing financial condition of the issuer. This may include speaking to financial officers affiliated with the issuer, reviewing economic changes impacting the issuer, and reviewing the issuers financial reports. The importance of knowing what we own is heightened during periods of market volatility.






NOT A PART OF THE ANNUAL REPORT

























The research conducted prior to investing in a bond, and ongoing credit monitoring, make it possible to evaluate potential risks associated with an individual bond and the adequacy of the compensation provided for that risk. Simply put, we seek to evaluate whether, as a bond investor, the Trust is adequately compensated for the risk associated with lending to a particular issuer.













We use a nationally prominent independent pricing service to price each and every single one of your Trusts portfolio holdings on a daily basis.
















In an effort to test the accuracy of our pricing, we regularly compare and confirm prices of our portfolio securities with a second pricing service.
















We continually seek to ensure that Tax-Free Trust of Arizonas net assets are invested in liquid securities.
















And, your Trusts portfolio holdings are published regularly. A detailed report is available quarterly, while your Trusts five largest portfolio holdings are listed as of each month-end. This information may be found on our website at www.aquilafunds.com.






We believe that following these various policies and procedures have served you and our other shareholders well over the past years.



And, you can rest assured that we regularly review and seek to enhance them as and when we believe appropriate.





Sincerely,
