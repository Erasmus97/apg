Dear Shareholders, 
In 2010, the global economy recorded another year of recovery from the financial and economic crises of 2008, but many of the factors that caused the downturn still weigh on the prospects for continued improvement.
In the U.S., ongoing weakness in housing values has put pressure on homeowners and mortgage lenders. Similarly, the strong earnings recovery for corporations and banks is only slowly being translated into increased hiring or more active lending.
Globally, deleveraging by private and public borrowers has inhibited economic growth and that process is far from complete. 
Encouragingly, constructive
actions are being taken by governments around the world to deal with economic issues. In the U.S., the recent passage of a stimulatory tax bill relieved some of the pressure on the Federal Reserve to promote economic expansion through quantitative
easing and offers the promise of sustained economic growth. A number of European governments are undertaking programs that could significantly reduce their budget deficits. Governments across the emerging markets are implementing various steps to
deal with global capital flows without undermining international trade and investment. 
The success of these government actions could determine whether
2011 brings further economic recovery and financial market progress. One risk associated with the extraordinary efforts to strengthen U.S. economic growth is that the debt of the U.S. government will continue to grow to unprecedented levels. Another
risk is that over time there could be inflationary pressures on asset values in the U.S. and abroad, because what happens in the U.S. impacts the rest of the world economy. Also, these various actions are being taken in a setting of heightened
global economic uncertainty, primarily about the supplies of energy and other critical commodities. In this challenging environment, your Nuveen investment team continues to seek sustainable investment opportunities and to remain alert to potential
risks in a recovery still facing many headwinds. On your behalf, we monitor their activities to assure they maintain their investment disciplines. 
As
you will note elsewhere in this report, on December 31, 2010, Nuveen Investments completed a strategic combination with FAF Advisors, Inc., the manager of the First American Funds. The combination adds highly respected and distinct investment
teams to meet the needs of investors and their advisors and is designed to benefit all fund shareholders by creating a fund organization with the potential for further economies of scale and the ability to draw from even greater talent and expertise
to meet those investor needs. 
As always, I encourage you to contact your financial consultant if you have any questions about your investment in a
Nuveen Fund. On behalf of the other members of your Fund Board, we look forward to continuing to earn your trust in the months and years ahead. 
Sincerely, 


