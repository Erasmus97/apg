DEAR FELLOW SHAREHOLDERS,



The Jensen Portfolio -- Class J Shares -- returned for the year ended May compared to a return of for the Standard &amp; Poors Index over this period. Please see pages through of this report for complete standardized performance information for the Portfolio.



Specific stock selection added value in the Information Technology and Financial sectors, while detracting from returns in the Consumer Staples and Health Care sectors. From a sector perspective, our overweighting in the Industrials sector added value. In addition, substantially all of our underperformance versus the benchmark can be attributed to our lack of representation in the Energy and Telecom Services sectors as both saw much more positive returns than the benchmark as a whole. We avoid these sectors because they produce virtually no companies with the long-term record of consistent business performance that Jensen requires, measured by consecutive years of or greater Return on Equity, as determined by the Investment Adviser.



Market Perspective



The S&amp;P Index has shown a return between the low of March and May Since that low over two fiscal years ago, the S&amp;P has risen from a level of to Despite the strong rebound, the index is still well below its previous peak level.



Much of the rebound through March was led by lower quality companies, as investors appeared to show little concern for world events or the issues plaguing the U.S. domestic economy. Over the last couple of months, however, we have seen increasing volatility in the markets and a corresponding rotation to quality companies such as those in The Jensen Portfolio. This is likely due to renewed concerns about the continuing Eurozone sovereign debt crisis, particularly in Greece, the Middle East unrest, particularly in Libya and Syria, and the domestic economy as uncertainty clouds the Federal Reserves future policies and doubt persists as to their effectiveness.



The stimulus-fed environment and low interest rates that have propelled the shares of lower quality businesses may continue for the remainder of albeit in different forms. But there now appears to be additional concerns expressed by investors. Risks to the economy have really not abated. The U.S. unemployment rate still exceeds the housing market remains quite sluggish and consumers continue to express doubt about the economy, reflected in less than robust spending. Credit remains difficult to obtain, despite low interest rates, as lenders worry about default risk and the impact of proposed regulatory constraints. All of these factors are again weighing heavily on investors.



The Effect at Jensen



Despite ongoing volatility, or perhaps because of it, we continue to find what we believe are attractive opportunities across the portfolio. That is particularly true for companies that are positioned for growth from overseas operations, especially in emerging markets.



Our top contributors to performance for the fiscal year were Oracle, Cognizant Technology Solutions, Praxair, United Technologies and T. Rowe Price. While these companies are in different sectors, their combination of strong future business prospects, technological prowess, innovation-focused strategies and attractive market prices made these businesses compelling. Each of them operates in a sector that we believe is well positioned for future growth.



Significant detractors from the Portfolios performance for the fiscal year included Microsoft and Sysco. Other significant detractors were Nike, our newest addition to the Portfolio, as well as Johnson &amp; Johnson and Clorox, both of which were sold. (Please see details below under Portfolio Additions and Eliminations). We believe that Nikes short-term performance is not representative of the long-term opportunity that this company represents.



Shares of Microsoft were essentially flat for the fiscal year due to concerns about growth in the PC markets and the explosive growth of tablet devices. Exiting a recession, businesses generally spend on Information Technology because of the measurable improvements in productivity and the ability to maintain lean staffing levels. Adoption of Windows has been slower than anticipated at the enterprise level as a result of the weak economy. Yet a silver lining remains. For many businesses still relying on the decade-old Windows XP, an upgrade is inevitable. Growth should also be driven by the strong product cycles of Microsofts server products and the launch of Office The robust free cash flow that these franchises provide enables Microsoft to continue developing areas such as online services, consumer products and its cloud computing venture, Azure, which we believe represents the next major growth area for the company.



Sysco, the global leader in food product distribution, continues to be impacted by concerns regarding consumer spending and more limited dining out. At the same time, the company continues to execute its strategy by maintaining a low cost structure, pursuing additional cost saving initiatives and assisting customers through a business transformation program designed to combat the sluggish economy. As the dominant company in its industry, Sysco has been gaining market share and utilizing its strong financial position to drive additional growth. Additional consumer confidence and clarity on the economy should benefit Sysco in the future.




















the Jensen Portfolio








Portfolio Additions and Eliminations



Becton Dickinson was added to the Portfolio in January Becton is a global medical device maker focused on the design and manufacture of needles and syringes for use in hospitals and other medical settings. The products are used for drug delivery, diabetes care and diagnostic fluid sampling. The company also provides tools that facilitate testing for diseases such as hospital-acquired infections and products that enable cellular analysis and pharmaceutical research. The companys primary competitive advantage is its manufacturing scale in the needle and syringe business. The company produces over billion syringes each year which translates into a nearly global market share. The ubiquitous nature of its products, and the fact that each is discarded after a single use, create an attractive recurring revenue stream. The company has consistently generated returns well in excess of its capital cost, has strong and growing free cash flows and a strong balance sheet.



In April we added Nike, the worlds leading footwear, sportswear and equipment supplier, both in terms of revenue and market capitalization. The company operates in more than countries and more than half of its revenues come from outside the U.S. Competitive advantages are numerous and include a premium, globally recognized brand name, powerful research and development abilities, large economies of scale across the business, diversified revenue sources and a strong marketing reputation. While the industry is competitive, we believe Nike is financially stronger than its competitors and is more profitable. Growth opportunities are present in nearly all markets and categories and huge global sports events such as the London Olympics in and the World Cup in Brazil should bode well for the company and its worldwide presence.



Johnson &amp; Johnson and Clorox were both sold primarily due to concerns about growth prospects within their respective businesses.



Johnson &amp; Johnson has been impacted by the likelihood of mandatory pricing concessions and the threat of pricing pressure as a result of the passage of healthcare reform in the U.S. last year. The company faces specific growth headwinds due to patent expirations and generic competition within its pharmaceutical franchise. While the companys drug pipeline has historically been positive, the U.S. Food and Drug Administration has become increasingly more demanding in terms of the approval process for new pharmaceuticals brought to market. The company has also been forced to focus on quality issues within its consumer franchise. This has diverted resources from other growth initiatives.



Clorox has been struggling against private label competition in key categories more than other Consumer Staples companies that we follow. Pricing power appears to have eroded in the face of such competition. Moreover, the company lacks real exposure to what are considered high growth emerging markets such as the BRIC countries of Brazil, Russia, India and China. Most of our portfolio companies are deeply and strategically exposed to these countries which likely represent the next engine for growth. We also believe that management execution at Clorox has been underwhelming, whether related to the delay of planned divestitures or the missed expectations around a key acquisition that resulted in a substantial writedown of intangible assets within the last few months.



The Jensen Outlook



Equity markets have climbed considerably over the past two years. Corporate America is lean and productive, as demonstrated by many companies strong business performance exiting the recession. Consumer strength is improving, marked by lower delinquency rates on lending, rising auto sales and increased borrowing, offset somewhat by a lack of increased spending. Nonetheless, substantial risks remain in the global economy, many of which we have noted above.




















the Jensen Portfolio








We believe that the easy money has already been made. We do not anticipate trouble-free sailing for the equity markets, but we do see opportunities for longer term investors in high-quality U.S.-based growth companies. Business performance for these firms has been steadily improving. Their balance sheets are strong, allowing them to reinvest for future growth. Yet the share prices of these businesses have not risen to the same degree as the overall market in recent quarters. We do not expect the market to rise at rates similar to when credit was flowing more freely and the relationship between risk and reward was off balance. However, we anticipate that quality businesses will continue to perform at high levels and believe that the market will reflect such performance.



The Jensen Portfolios companies benefit from a balance of revenues from inside the U.S. as well as outside. Growth in the U.S. may be slow and the economic recovery thus far has been choppy. The challenges suppressing the recovery appear stubborn and will likely persist for at least the near term. But the strong free cash flows from the domestic operations of our companies are expected to be effectively deployed to faster growing economies around the world, as well as toward acquisitions, or returned to shareholders as dividends and share buybacks. We believe these quality businesses are well positioned for the next stage of the economic recovery, however it plays out.



We invite you to seek additional information about The Jensen Portfolio at www.jenseninvestment.com where additional content, including updated holdings and performance information, is available. We take our investment responsibilities seriously and appreciate the trust you have placed in us. As always, we welcome your feedback.



Sincerely,
