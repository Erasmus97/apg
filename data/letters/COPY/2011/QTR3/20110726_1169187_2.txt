Dear Investor

High yield bonds generated strong gains during the past six months and our fiscal year ended May The overall environment for below investment-grade companies remained favorable due to moderating default expectations, abundant
liquidity, and improving corporate balance sheets. High yield bond prices moved higher and yields trended lower, although the market experienced a soft patch in May as the economic signals weakened and concerns about the European sovereign debt
crisis worsened. The companies in our market continue to perform well and remain fundamentally strong. However, we have become more cautious in the last six monthsour asset class has generated powerful gains in a relatively short
periodand we would not be surprised to see a short-term pullback.

Portfolio Performance



The Institutional High Yield Fund posted strong absolute and relative performance during the six months and year ended May Junk bonds were again among the best-performing fixed income asset classes. The portfolio performed in
line with the J.P. Morgan benchmark for the and periods. Credit selection was particularly good in financials, forest products, and information technology, but the retail, airlines, and housing sectors were weak.



The funds share price advanced over the past six months and for the year, closing the reporting period at Capital appreciation contributed about of the funds total return in the six-month
period and almost half of total return for months. In our previous shareholder letter (November we expected more muted results from the asset class: Overall, the market has performed better than we thought. The portfolio generated
of dividend income in the last six months and for the year. Although we strive to generate as much current income as is prudently possible, the funds rising share price and the low interest rate environment caused the
portfolios SEC yield to decline to at the end of May, near its lowest level ever. Our longer-term returns are shown in the table on page

Market Environment

The high yield market has been exceptionally strong for the past two and a half years, advancing more than in about in and nearly for the year to date. High yield companies have issued a record amount of new
securities, and investor demand remained solid. May was a record month for high yield bond deals, representing almost billionand the five-month total is already more than half of last years record
billion. Refinancing has represented the majority of new issue activity, which is strengthening corporate balance sheets and reducing the likelihood of defaults.

Investors were increasingly willing to buy lower-quality credits for their higher yield as they grew more convinced of the economic recovery. CCC rated securities were the best-performing segment of the asset class by a wide margin
during the last six months and year. According to J.P. Morgan data, CCC credits gained in the past six months, while BB rated issues advanced CCCs were up for the period, which is about one-third better than the
gain for BB issues in the index. At the same time, the market warmed to riskier leveraged buyout (LBO) offerings, allowing more-speculative companies to strengthen their balance sheets. LBO companies were the last candidates in our universe to take
advantage of refinancingin and only higher-quality companies were able to access the market for new money.

In a nutshell, the last six months remained an almost-perfect backdrop for high yield bonds. The macroeconomic environment improved, company fundamentals were strong, capital markets were open, demand was healthy, and the market
advanced significantly. The roughly gain over the last six months is impressive in the context of historical results. Investors should understand that we have enjoyed about two and a half years of strong performance, and we are likely to experience some
volatility after this kind of exceptional run. The J.P. Morgan benchmark has advanced more than since the end of As we write this letter, it seems that we are hitting a soft patch. Investors who heeded the long-held go away in
May adage enjoyed perfect timing. The environment has darkened to a degree, as there is growing concern about the economys health. Interest rates tumbled in April and May, and Treasuries have rallied on concerns about the strength of the
U.S. economy. As a result, we believe the high yield bond market faces both limited upside and downside potential in the near term.



A correction is naturalmarkets do not go up in a straight line. We believe we are experiencing a temporary slowdown, and in the second half of we expect more positive trends to emerge, including better employment creation and
stronger economic growth. Still, there are a few storm clouds on the horizon, such as the European sovereign debt crisis. That said, we believe that the environment for our companies is still good, and we are unlikely to experience another high
yield sell-off like Of course, we will carefully monitor the overall health of our companies, the economy, and the consumer situation.

The conundrum for fixed income investors is that yields on high-quality bonds are extremely low. The five-year Treasury, for example, yields just The yield advantage in the high yield market remains attractive versus Treasuries
and investment-grade corporate bonds. However, our asset class is beginning to look somewhat pricey. We believe the potential for earning double-digit returns fueled by capital appreciation is past.

Portfolio Review

The funds performance versus its benchmark in the last six months benefited from holdings in financials, forest products, wireless communications, and technology. The same groups were also top contributors for the year, along with
autos, which experienced some weakness in the last six months. Several of the funds best industry contributors generated returns of over the last six months, and virtually all industry groups posted double-digit gains for the
yearno industry group posted a loss in either period. The funds financial holdings were the biggest industry contributor by a large margin in both periods. Over the past two years, several large investment-grade financial companies were downgraded, increasing the sectors weighting in the benchmark. The portfolios industry allocations and largest issuers are shown in the tables on pages and

Harvesting Superb Gains in Financials

Five of our largest holdings at the end of the reporting period were financials, led by Ally Financial, formerly known as GMAC and at one time a wholly owned subsidiary and the finance
arm of General Motors. A bailout engineered when the company was on the brink of bankruptcy made the federal government a majority shareholder in December The one-time investment-grade company has
flourished thanks to the cash infusion, and it continues to make substantial progress financially. Now that the company has been resuscitated, its credit profile has climbed toward investment grade, and there are plans for an initial public offering
(IPO). We hold a large position in Ally bonds that have performed well, and we own a large and very lucrative position in its preferred securities yielding The preferred stock has rallied nearly in the past six months, and we earned the
healthy coupon, making it the portfolios best performer in this reporting period. However, there is some speculation that the preferred stock will be refinanced if and when the company does an IPO. (Please refer to the funds portfolio of
investments for a complete list of holdings and the amount each represents in the portfolio.)
Other strong financials performers included American International Group and CIT
Group. Both companies benefited from the improvement in the economic environment and could be upgraded to investment grade in the not-too-distant future. Nuveen Investments,
SLM Corporation (Sallie Mae), and E*Trade Financial were also among our top contributors for the and periods.

The best of the run for financials is probably behind us, and we have begun harvesting some of our gains. Six months ago, we had a allocation in financials, and since then we trimmed that exposure to about Many of the bonds now
trade more like investment-grade debt, which means their yields are no longer as enticing.

We have also harvested gains from our equity holdingscommon and preferred stock, warrants, and convertiblesthat had generated strong results. At the end of May, our equity exposure was The portfolios return from
equities over the past six months was more than and for the year we earned more than Following these strong results, we trimmed some of the position as the period came to a close. Although we have become somewhat concerned about volatility in the stock market, equities remain a key component in our investment strategy.



Higher Yields in Lower-Quality Credits

Subdued inflation pressures, highly accommodative Federal Reserve monetary policy, and signs of continuing economic weakness caused Treasury yields to decline over much of the past year. At the same time, corporate profits grew more
robustly than the economy as a whole, and defaults were virtually nonexistent. In this environment, the spread, or yield advantage high yield bonds have over comparable-maturity Treasuries, has narrowed significantly. At the end of May, the spread
was basis points percentage points), according to J.P. Morgan data. However, credit spreads within the high yield market varied widely. For example, the yield on BB rated bonds was basis points above comparable-maturity Treasuries,
while the advantage for CCC rated credits stood at basis points. Because of this disparity and our exhaustive credit research capabilities, we have been willing to increase our exposure to lower-rated, higher-yielding bonds.



First Data, a CCC rated credit, was the portfolios biggest purchase in the last six months and one of its best contributors. The company is the largest credit card transaction processor in the U.S., but
it had accumulated a large debt load. With the help of KKR, a global asset manager and LBO specialist, and in conjunction with several of the large bondholders and some very creative financial engineering, the company has turned itself around. It posted significantly improved earnings this year as consumer spending recovered. First Datas notes, with a coupon of gained nearly in the
six-month perioda combination of price appreciation and income payments.

Our CCC rated LBO investments were significant contributors over the past six months and year. We modestly increased our allocation in CCC rated credits to of the fund at the end of May, from about six months ago. One of our
best contributors in both the and periods was CDW, which works with corporations and the government to set up technology platforms. Companies that do not have sufficient technology resources
typically go to a contractor such as CDW to help with their technology needs. Another top contributor was Univision Communications. Univision is the leading Spanish-language broadcaster in the U.S. It
owns a world-class portfolio of TV and radio stations in several major markets and was involved in an aggressive LBO a few years ago.

Other Contributors and Detractors

We continued to generate good results from some of the largest and most notable companies in our space, such as Sprint Nextel and Ford Motor.
Both of these fallen angels (formerly investment-grade firms) performed exceptionally well. The wireless communications industry continues to generate steady cash flow and remains a core holding in the fund. The auto sector has been a strong
performer, although we have experienced some weakness recently, and Ford was among the funds best performers. It has made a remarkable turnaround in the three years since it looked like it might have to file for bankruptcy. The company is now
on the verge of being upgraded to investment grade, which means we will likely trim our holdings to realize gains. Across the entire portfolio, credit selection was the primary driver of our good results, far more than industry allocation decisions.

We have increased our exposure in energy to from six months ago because we like the prospects for some of the globally focused oil services and exploration and production firms. We remain under the benchmarks energy allocation. We are
confident that the group can perform well, and, like the wireless segment, it tends to have more defensive characteristics.

Our best and worst contributors over the past six months were hugely lopsided. Our top contributors generated basis points of return, which was about one-third of the performance for the period. On the other side, the bottom
contributors detracted only basis points. From an industry perspective, all sectors provided positive performance, but the retail, airlines, and housing sectors were weakest. However, across the credits that actually declined in the past six months, there is
very little in common.



Retailers are experiencing margin pressure, and investors have become concerned about the prospect of faltering consumer spending. For example, at J. Crew and
Gymboree, the topline results were solid but cotton costs spiked. Our holdings in the building and real estate segment totaled about of the fund. In both of these lagging industries, we were underweight compared
with the benchmark index. Airlines have had their profits squeezed by higher fuel costs. At the end of May, airline holdings accounted for just of the fund. Our modest cash position also hurt relative resultsthe allocation in
short-term holdings generated next to nothing. Overall, there was a lack of meaningful detractors in the portfolio, and there have been no defaults.

Outlook

We have become more cautious about the near-term prospects for high yield securities. The market has run a long way in a short periodour gain in the last six months is about what we have averaged annually over longer-term
periods. We have generated above-average returns in each of our last five six-month reporting periods. Therefore, we would not be surprised to see a pullback this summer.
Several of the forces that powered the bull market in junk bonds have
weakened. The market has benefited from falling interest rates, and there just is not much room for that to continue. Yields are already at historically low levels, and the funds SEC yield is only fractionally above its lowest level
ever. Next, the default rate is near its historical low and is expected to be just this year and through This positive is already priced into the market. Some of the enthusiasm for the companies in our universe is based on the potential for
IPOs, and if stocks continue to head lower, IPO activity is likely to diminishremoving a positive catalyst. Another factor is the recent new issuance. Underwriters sold a record amount of high yield bonds in May. It is good that many companies
were able to access attractive long-term financing at to but it is hard for us to get excited about B rated bonds yielding

Six months ago, we thought interest rates would move higher given the inflation trends and the economic strength at the end of We were wrong, but we still believe that higher interest rates are inevitablethe question is
when. The worst scenario for our market is if the economy slips into a double-dip recession. The odds of that happening seem low, but it is possible. The best scenario would be to economic growth, which is what we expect as the year
progresses. We recommend that investors ratchet down their expectations for this market for the coming year. Returns likely will be lower than the exceptional gains we have enjoyed over the past two and a half years.

As always, our goal is to deliver high current income and attractive total returns over time while seeking to cushion the volatility inherent in this market. We will continue our commitment to research and diversification, which we
believe is prudent for a fund that invests in a riskier area of the bond market.

Thank you for investing with T. Rowe Price.

Respectfully submitted,
