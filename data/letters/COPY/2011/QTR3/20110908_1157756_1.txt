Dear Shareholders:



It feels like more now than ever, there is a constant tug of war for the top news headline of the day.On Monday we might see distressing news coming from Greece and Portugal. Tuesdays paper might tell us that federal budget deficits are going to be even higher than expected. On Wednesday, Germany and France may be disagreeing on the best methods to prop up the weaker European economies. Back to the US on Thursday for stories about Congress bickering over the debt ceiling, while Fridays report of unemployment continues to remind us that the country is not out of the economic woods.



What is a common theme to the solutions being considered for these problems? Stalling. Playing for time. Kicking the can down the road. This last phrase has been used frequently over the past year to describe solutions that put a band-aid on a huge problem while never addressing, or even identifying, the root cause. You hope that by the time the crisis pops up again, a clear solution will be available or different means of addressing the problem will be developed, or at least that it will become someone elses problem. The situation in Europe with excessive and/or growing sovereign debt levels in Greece, Portugal, Ireland, Spain and Italy is not likely to be solved by loaning these countries more money. Budget deficits in the US wont be contained by borrowing more money. Extending unemployment benefits does nothing to address how jobs can be created. All of these situations require a number of tough choices.None will be easy, and all will be painful to some.Most solutions on the table appear to rely on the world economy growing its way out of the current troubles.



Todays equity markets are extremely volatile and are on what I would describe as the edge of high drama.Out of trading days in the first months of this year, the S&amp;P Index moved up or down or more on of those days, and had moves of plus or minus on over of those days.More importantly, the notion of risk has come into extreme focus, and is dominating the daily move in stock prices.This has in turn resulted in a condition of either risk on or risk off.This notion of risk has not proved to be contained in the stock market alone.Fixed income securities are witnessing an equal amount of volatility and uncertainty.This type of environment has made it increasingly difficult for an investment style like ours that focuses on fundamentals instead of reliance on very short-term oriented trading activities.



However, all is not dire in the investable universe.We continue to focus our efforts on high quality companies that can stand the test of time.Despite all of the uncertainties that persist, we are finding opportunities and have stuck to our strict investment discipline.In our equity portfolios, we continue to seek companies that have proven business models and do not depend on extraordinary government support.Our fixed income approach maintains a bias towards high quality, investment grade bonds.Currently, we are staying towards the short end of the interest rate curve and participate in appropriate opportunities when they present themselves.



Performance data quoted represents past performance; past performance does not guarantee future results.The investment return and principal value of an investment will fluctuate so that an investors shares, when redeemed, may be worth more or less than their original cost.Current performance of the Funds may be lower or higher than the performance quoted.Performance data current to the most recent month end may be obtained by calling or visiting www.countryinvestment.com.



COUNTRY GROWTH FUND

Inception Date (Class Y)



The average annualized total returns for the Fund for the year ended June are as follows:








Year




Year




Year

















Gross Expense (as of the prospectus dated



Net Expense Ratio: (as of the prospectus dated



The custodian has contractually agreed to waive fees through October returns assume all dividends and capital gains distributions were re-invested in the Fund and reflect fee waivers in effect.In the absence of fee waivers, returns would be reduced.



The COUNTRY Growth Fund performance over the past year has been quite strong as the stock market advanced from the lows reached in late and early much of this strength was seen in the last half of calendar For the past six months, the equity market has been in a trading range with more volatility being experienced.



We remain concerned about the economic environment and its potential impact on securities markets. Domestic growth has been quite anemic for this stage of a recovery and unemployment remains a major problem with rates in excess of There are no visible signs of improvement in housing, personal income or consumer spending. We are concerned that any external shocks could tip growth back to negative territory.






















The Fund remains well diversified across economic sectors, but we are emphasizing those areas most likely to perform satisfactorily in difficult environments. We are looking for signs that would allow us to take a less conservative approach to investing in stocks, but at this point we do not feel such a move would be in the best interests of our shareholders.



COUNTRY BOND FUND

Inception Date (Class Y)



The average annualized total returns for the Fund for the year ending June are as follows:







Year




Year




Year

















Gross Expense Ratio: (as of the prospectus dated



Net Expense Ratio: (as of the prospectus dated



The advisor and custodian have contractually agreed to waive fees and/or reimburse expenses through October returns assume all dividend and capital gain distributions were re-invested in the Fund and reflect fee waivers and/or expense reimbursements in effect.In the absence of fee waivers and/or expense reimbursements, returns would be reduced.



U.S. bonds, as represented by the Barclays Capital U.S. Aggregate Bond Index, have produced good returns for the trailing six and twelve months. Bond yields declined and prices climbed for much of the year as a sluggish economy, low inflation, and the European debt crisis spurred investor demand. Investors were hungry for yield which pushed bonds with the highest risk to the highest returns. Shorter-term Treasuries and mortgagebacked securities posted more modest gains. Our diversification has allowed the COUNTRY Bond Fund to avoid some of the extreme volatility of the past four years experienced by the individual sectors that make up the index. We currently have an overweight position in taxable municipal bonds (BABS). We feel this sector offers favorable risk/reward parameters versus corporate bonds. However, this sector has also experienced volatility after a well known analyst made a questionable dire forecast for the municipal market earlier this year.



We continue to believe fixed income investors should be careful about risk, given how low interest rates are. The income cushion available to offset any decline in price if rates do rise, is very thin. We could be in this low-interest rate environment for some time. But when the markets think that the potential for a Fed rate hike is sooner than later, rates could snap back with a vengeance. With this risk we are maintaining a conservative portfolio duration.








Sincerely
