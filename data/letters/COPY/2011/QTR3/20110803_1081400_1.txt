Dear Valued Shareholder, 
Wells Fargo Advantage
Funds 
The period was marked by continuing optimism as most economic data continued to point toward recovery; although a continued
sluggish employment market and a late-period weakening in the housing market remained worrisome. All major areas of the global stock market ended the period with double-digit gains. In an environment distinguished by continued low interest rates and
recovering credit markets, most bond market sectors posted gains. 
Solid economic reports helped support a market rally.

Economic numbers throughout the reporting period supported the case for a gradual recovery. Reported real gross domestic product (GDP) grew
by 3.1% in the fourth quarter of 2010. Although GDP growth slowed in the first quarter of 2011, it remained positive at 1.8%, supporting consensus opinion that a double-dip recession was unlikely. 
The unemployment rate began the period at 9.7% in October 2010 and ended the period modestly lower at 9.1% in May 2011. The stubbornly high unemployment
rate was the main sour note and the primary reason why most analysts believed that an economic recovery would be gradual. Persistent weakness in the housing market remained another source of concern, and by the end of the period, higher prices for
food and energy led to concerns about renewed inflation. 
Europe’s sovereign debt problems weighed on confidence.

Although European economic news had initially supported the moderately positive outlook, investors became concerned in
mid-2010 about the possibility that eurozone member Greece would default on its sovereign debt. Greece applied for a bailout from the European Union and the International Monetary Fund in April 2010, and Ireland followed in November. Portugal also
requested a bailout in early April 2011. Concerns about default led to worries about the debt burdens of other member states, such as Italy and Spain, and about the effect of possible defaults on European banks and pension funds. Despite the debt
worries, European stocks rallied during the reporting period, and the MSCI EAFE Index
Central banks continued to provide stimulus. 
Throughout the reporting period,
the Federal Open Market Committee kept its key interest rates at effectively zero in order to support the financial system. In November 2010 the Federal Reserve announced a second round of monetary 
 
Table of Contents
 

stimulus (popularly known as QE2) in which it planned to purchase $600 billion in long-term U.S. Treasuries by the end of the second quarter of 2011, as well as reinvest an additional $250
billion to $300 billion in Treasuries with the proceeds from earlier investments. 
The European Central Bank (ECB) began the period with a key
rate of 1.00% but in early April 2011 raised it to 1.25% as part of an attempt to keep inflation in check. The ECB continued to provide unlimited liquidity for banks and, in December 2010, announced the purchase of Irish and Portuguese bonds to help
reduce those countries’ borrowing costs. 
Global stock and bond markets rallied. 
Stocks sold off in the summer of 2010 because of concerns about sovereign debt, but they soon recovered on hopes of a continued economic recovery. For the
period, both domestic small-cap and large-cap stocks rallied as most investors seemed to operate on the belief that the economic recovery would continue. Within the small-cap space, growth stocks strongly outperformed value stocks, in part because
small-cap growth issues tend to have greater sensitivity to economic growth. Within the large-cap space, though, growth stocks and value stocks posted comparable returns. Both developed and emerging markets stocks posted gains for the period.

Within the fixed-income universe, investment-grade bonds continued to benefit from historically low interest rates, though returns were in the
single digits, as most of the total returns came from income rather than from capital gains. High-yield bonds posted stronger gains on improved economic conditions and investors’ apparent increased willingness to take on credit risk in exchange
for higher yields in a low-yielding environment. 
Many variables are at work in the market. 
The full effect of the credit crisis remains unknown. Elevated unemployment and debt defaults continue to pressure consumers and businesses alike.

Wells Fargo Advantage
Funds
Wells Fargo Advantage Funds
Sincerely, 


Dear Valued Shareholder, 
Wells Fargo Advantage Funds 
The equity market built upon the rally that began in 2009 by delivering broad-based gains against the backdrop of a subpar economic recovery. Yet, a variety
of global and domestic challenges kept the market from advancing in a straight line throughout the period, once again highlighting the value of a sound, well-diversified investment strategy. As always, we believe that such a strategy may enable
investors to balance risks and opportunities as they pursue long-term financial goals in a dynamic market environment. 
The
economic recovery continued but lost some steam. 
As we have often seen since the recession officially ended in mid-2009, the economic
recovery followed a decidedly uneven path during the period. In the fourth quarter of 2010, the recovery accelerated as U.S. gross domestic product (GDP) grew at an annualized rate of 3.1%, up from 2.6% in the previous quarter. In the first quarter
of 2011, however, GDP growth slowed to 1.8%, casting renewed doubts on the strength of the recovery. 
While a double-dip recession appears
unlikely at this point, the consensus outlook for the economy has dimmed somewhat. As of May 2011, disappointing economic data in several key areas led many analysts to scale back their forecasts for second-quarter and full-year GDP growth.

The labor and housing markets remained sluggish. 
The beleaguered labor and housing markets have continued to exert a powerful drag on economic growth. After improving slightly in the first quarter, the labor market faltered in late May as new claims for
jobless benefits increased by more than expected. Although many private-sector employers have been adding jobs, the pace of job creation has not been nearly brisk enough to lower the unemployment rate, which stood at 9.1% as of May. Housing has
arguably been even more of a trouble spot, with weak home sales and housing starts putting significant downward pressure on prices. 
Not
surprisingly, these and other issues—particularly high food and gas prices—have weighed heavily on consumer confidence and bear close watching in the months ahead 
The Federal Reserve stayed focused on economic stimulus. 
Despite recent
inflation concerns, U.S. core inflation, which excludes food and energy prices, has been fairly modest. As a result, the Federal Reserve (Fed) has been able to hold its target range for the federal funds rate—a proxy for short-term interest
rates—steady at 0% to 0.25%. 
On April 27, 2011, in its final statement of this reporting period, the Fed indicated that it intends to keep
short-term rates at exceptionally low levels for “an extended period” in order to promote a more robust economic expansion. It also stated that it plans to complete the second round of quantitative easing (QE2)—a stimulus program
designed to purchase $600 billion in longer-term Treasury securities by the end of June 2011. 
Table of Contents
 
The equity market rewarded investors. 
Aided by strong corporate earnings, QE2 seemed to be a catalyst for the equity market’s impressive performance during the period. While there were bouts
of volatility along the way, the market demonstrated surprising resilience in the face of numerous headwinds—including mixed economic news, government budget deficits, Europe’s sovereign debt crisis, geopolitical turmoil in the Middle
East, and natural disasters in both Japan and the U.S. 
The S&amp;P 500 Index
The market momentum carried over into April 2011 but reversed in May amid mounting evidence that the U.S. economic recovery had entered a soft patch.
However, the May decline was relatively mild given the scarcity of upbeat data reported during the month. 
A long-term
investment perspective is key. 
It remains to be seen how quickly the equity market can resume its upward trajectory. In any case, the
market’s dramatic rebound over the past two years from the severe downturn of 2008 to 2009 underscores the importance of maintaining a disciplined, long-term investment strategy through changing market cycles. By staying focused on your
long-term goals, you may be better positioned to both navigate falling markets and participate in rising markets. 
Wells Fargo Advantage Funds
Wells Fargo Advantage Funds
Sincerely, 


Dear Valued Shareholder, 
Wells Fargo Advantage Strategic Municipal Bond Fund
The global economic recovery moved toward expansion. 
The U.S. economic recovery that began in mid-2009 gained further momentum throughout the period, particularly toward the end of 2010. For example, advanced
estimates of gross domestic product (GDP) indicated that the economy grew at an annualized rate of 1.8% during the first quarter of 2011. While this reading is lower than the 3.1% in the fourth quarter of 2010, it continues the streak of seven
consecutive quarters of positive GDP growth. Although the path of recovery within the U.S. has been uneven at times and growth remains subpar compared with previous recoveries, the consensus among economists is that the economy will likely continue
to move toward a sustainable expansion during the second half of 2011. 
Jobs and housing remained troublesome. 
By the end of the period, the unemployment rate in the U.S. stood at 9.0%, down from 9.9% a year earlier but still notably higher than historical averages.
Unfortunately, the drop may be more attributable to a decline in the labor force than to a meaningful uptick in hiring. In fact, employers added just 1.1 million jobs during the entire 2010 calendar year, below the historical average of
1.4 million jobs created each year over the past 80 years, suggesting that the improving economy has yet to translate into widespread hiring. Meanwhile, the beleaguered housing market was an ongoing source of concern, despite some tentative
late-year signs of stabilization. That said, persistent weakness in the labor and housing markets bears close watching in the months ahead. 
Other economic data in the U.S. was more encouraging, reflecting greater confidence in the sustainability of the expansion on the part of both consumers and
businesses. Retail sales came in strong at certain points during the period, including the critical holiday shopping season, and industrial production and new orders have picked up. Although still reluctant to hire, businesses have gradually
increased spending in other areas, such as equipment and information technology. Core inflation, which excludes volatile food and energy prices, remained benign. 
The Federal Reserve continued to do its part. 
With inflation subdued, the
Federal Reserve (the Fed) held its target range for the federal funds rate—a proxy for short-term interest rates—steady at 0.00% to 0.25%. On April 27, 2011, in its final statement of the 12-month period, the Fed noted that the
economic recovery “is proceeding at a moderate pace,” while the 
Table of Contents
 

employment situation is “improving gradually.” With regard to inflation, the Fed noted that the rate of inflation increased but that “longer-term inflation expectations have
remained stable and measures of underlying inflation are still subdued.” As a result, the Fed indicated that it intends to keep short-term rates at historically low levels for as long as necessary to ensure a sustainable recovery
and expansion. 
U.S. Treasury yields declined for most of the period. 
Most sectors of the bond market performed well during 2010 and continued to post positive total returns through May 31, 2011, with interest
income—rather than price gains—accounting for the bulk of those returns. This part of the investment cycle is known as the income phase and is typically characterized by relatively stable short-term rates and relatively small movements in
bond yields. The current market environment is certainly holding true to a typical income phase, especially considering the Fed is maintaining an extraordinarily accommodative monetary policy. U.S. Treasuries continued to rally for much of the
period, pulling yields lower in nearly all corners of the fixed-income markets, including municipal bonds. 
In general, investors appeared
increasingly convinced that economic growth would remain tepid throughout 2010 and 2011, but an even more powerful effect on the markets was the disinflationary sentiment that became more firmly entrenched during the late summer of 2010. The core
consumer price index (CPI) declined to its lowest level on record, prompting the Fed to formally comment that inflation was well below levels that are consistent with its mandate. Consequently, the Fed mentioned during the fall that it was again
willing to use all available tools to stimulate growth and promote maximum employment and price stability. This spurred further rallies in the U.S. Treasury market and across several of the riskier areas of the domestic fixed-income markets.
Municipal bond markets followed the Treasury market for much of the period and continued to rally through the end of October 2010. 
After trailing taxable bonds in 2010, the municipal market has thus far outperformed in 2011. 
During the first half of the reporting period, municipal bonds rallied, with the Barclays Capital Municipal Bond Index
 
 
 
Table of Contents
 
 
 
 
If investors are
willing to assume a bit more credit risk by moving beyond Treasuries into a well-researched and diversified municipal portfolio, it is possible to realize better income than what is being offered by short-term Treasuries. 
 
 

After experiencing some market turmoil near the end of 2010—primarily the result of a heavy new issuance
calendar, especially from the State of California and a spike in Build America Bonds—the municipal markets began 2011 with exceptionally higher yields than were typically being offered through most of 2010. These higher yields created good
relative and absolute values in the tax-exempt markets compared with their taxable counterparts. This relative value relationship presented a compelling investment opportunity for municipal investors, which has been rewarded so far in 2011. Within
the lower-quality investment-grade credits and high yield, performance has been particularly strong on a year-to-date basis as investor risk appetites seemed to have increased with an improving outlook about the health of state and local
governments. Also, as would be expected in the current low rate environment, intermediate- and long-maturity bonds within both the taxable and municipal markets continue to benefit from strong demand. 
Through May 31, 2011, the investment-grade Barclays Capital Municipal Bond Index returned 4.06%, with the Barclays Capital High Yield
Bond Index
However, for the reporting period, the municipal bond index returned 3.18%, compared with 4.49% for the Treasury
index and 5.84% for the broad-based U.S. aggregate index. 
The municipal markets offer compelling valuations for diligent
investors. 
Considering the extremely low yields being offered by U.S. Treasuries, especially those notes with less than five-year maturities,
most of the municipal market continues to offer an attractive alternative. There is still a level of uncertainty as the U.S. economy continues to search for sustainable growth, and several states must continue to aggressively manage their budgets.
Nevertheless, if investors are willing to assume a bit more credit risk by moving beyond Treasuries into a well-researched and diversified municipal portfolio, it is possible to realize better income than what is being offered by short-term
Treasuries. 
Active management and credit analysis will drive fund construction and performance. 
Wells Fargo Advantage Funds
 
 
 
Table of Contents
 
Wells Fargo Advantage Strategic Municipal Bond
Fund
Wells Fargo Advantage Funds
Sincerely, 


Dear Valued Shareholder, 
Wells Fargo Advantage 
Solid economic reports helped support a market rally. 
Economic numbers
throughout the reporting period supported the case for a gradual recovery. Reported real gross domestic product (GDP) grew by 2.6% in the third quarter of 2010 and by 3.1% in the fourth quarter. Although GDP growth slowed in the first quarter of
2011, it remained solidly positive at 1.8%, supporting consensus opinion that a double-dip recession was unlikely. 
The unemployment rate began
the period at 9.5% in June 2010 and ended the period modestly lower at 9.1% in May 2011. The stubbornly high unemployment rate was the main sour note and the primary reason why most analysts believed that an economic recovery would be gradual.

Persistent weakness in the housing market remained another source of concern, and by the end of the period, higher prices for food and energy
led to concerns about renewed inflation. 
Europe’s sovereign debt problems weighed on confidence. 
Although European economic news had initially supported the moderately positive outlook, investors became concerned in mid-2010 about the
possibility that eurozone member Greece would default on its sovereign debt. Greece applied for a bailout from the European Union and the International Monetary Fund in April 2010, and Ireland followed in November. Portugal also requested a bailout
in early April 2011. Concerns about default led to worries about the debt burdens of other member states, such as Italy and Spain, and about the effect of possible defaults on European banks and pension funds. Despite the debt worries, European
stocks rallied during the reporting period, and the MSCI EAFE Index
Central banks continued to provide stimulus. 
Throughout the reporting period,
the Federal Open Market Committee kept its key interest rates at effectively zero in order to support the financial system. In November 2010, the Federal Reserve (Fed) announced a second round of monetary stimulus (QE2) in which it planned to
purchase $600 billion in long-term U.S. Treasuries by the end of the second quarter of 2011, as well as reinvest an additional $250 billion to $300 billion in Treasuries with the proceeds from earlier investments. 
The European Central Bank (ECB) began the period with a key rate of 1.00% but in early April raised it to 1.25% as part of an attempt to keep inflation in
check. The 
 
Table of Contents
 

ECB continued to provide unlimited liquidity for banks and, in December 2010, announced the purchase of Irish and Portuguese bonds to help reduce those countries’ borrowing costs.

 
Global stock and bond markets rallied. 
Stocks sold off in the summer of 2010 because of concerns about sovereign debt, but they soon recovered on hopes of a continued economic recovery. For the
period, both domestic small-cap and large-cap stocks rallied as most investors seemed to operate on the belief that the economic recovery would continue. Within the small-cap space, growth stocks strongly outperformed value stocks, in part because
small-cap growth issues tend to have greater sensitivity to economic growth. Growth stocks also outperformed within the large-cap space but by a smaller margin. Both developed and emerging markets stocks posted gains for the period. 
Within the fixed-income universe, investment-grade bonds continued to benefit from historically low interest rates, though returns were in the single
digits, as most of the total returns came from income rather than from capital gains. High-yield bonds posted double-digit gains on improved economic conditions and investors’ apparent increased willingness to take on credit risk in exchange
for higher yields in a low-yielding environment. 
Many variables are at work in the market. 
The full effect of the credit crisis remains unknown. Elevated unemployment and debt defaults continue to pressure consumers and businesses alike.

Wells Fargo Advantage
Funds
Wells Fargo Advantage Funds
Sincerely, 


