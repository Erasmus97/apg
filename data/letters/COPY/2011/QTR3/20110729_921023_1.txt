Dear Shareowner,

The U.S. economy is moving forward on a slow path to recovery. We believe the
theme for the economy in may be modest but positive growth. The private
sector is showing signs of slow but steady improvement, led by higher capital
investment, solid exports, improved consumption, and gradually rising demand
for consumer auto loans and commercial loans. At the same time, the risks to a
steady recovery remain substantial, including the continued delays in the
housing sector's recovery, rising oil prices, and the fiscal drag of U.S.
federal and state budget cuts. We are concerned about the long-term risk of
inflation in an environment of accommodative Fed policy, continued low nominal
and "real" interest rates and rising commodity prices.

The recovery process may occur more slowly than many would like, and will
almost certainly be accompanied by short-term market swings. But our investment
professionals are finding good opportunities to invest. Through the first
quarter of although bonds remained popular with investors, we believed
there was value in the equity market. In both equity and bond markets, we are
finding good opportunities to invest using the same disciplined approach we
have used at Pioneer since which is to focus on identifying undervalued
individual securities with the greatest potential for success, carefully
weighing risk against reward. Our teams of investment professionals continually
monitor and analyze the relative valuations of different sectors and securities
globally to help build portfolios that we believe can help you achieve your
investment goals.

At Pioneer, we have long advocated the benefits of staying diversified and
investing for the long term. The strategy has generally performed well for many
investors. For instance, bond markets certainly rewarded investors for most of
even though equity valuations seemed quite reasonable and were
inexpensive relative to bonds and compared with historic levels -- conditions
which represented potentially good value for long-term investors.

Our advice, as always, is to work closely with a trusted financial advisor to
discuss your goals and work together to develop an investment strategy that
meets your individual needs. There is no single best strategy that works for
every investor.

Pioneer Emerging Markets Fund | Semiannual Report |


We invite you to learn more about Pioneer and our time-tested approach to
investing by consulting with your financial advisor or visiting us online at
www.pioneerinvestments.com. We greatly appreciate your trust in us and we thank
you for investing with Pioneer.

Sincerely,
