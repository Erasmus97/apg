Dear Shareholder:
We are pleased to present the FBR Funds’ Annual Report for the twelve-month period ended October 31, 2010.
1
All ten of the Global Industry Classification
 Standard (GICS) sectors represented in the S&amp;P 500 Index posted positive returns
 for the period. Despite the historically high long-term unemployment rates, continued
 weakness in the housing market, and negative investor sentiment, the consumer discretionary
 sector lead the way outpacing the nine other sectors. Other top performing sectors
 included industrials, telecommunications and materials. The steady rise in commodity
 prices, especially precious metals, drove strong returns in material stocks. Not
 surprisingly, financials were the weakest performing sector as banks continue to
 struggle with numerous credit, regulatory and loan growth headwinds. Income-seeking
 investors, however, have been in a difficult place over the previous twelve-months
 with continuing low bond, bank CD and money market returns.
Overall, we believe the U.S. economy is
 healing and that better conditions for investing, employment and economic growth
 lie ahead. There is no doubt that this so called “great recession” has
 been very severe and will take time to heal. However, we see that companies are
 de-levering, de-risking and de-complicating their balance sheets and income statements.
 We see the consumer reducing debt and becoming more rational about the balance between
 spending and saving. We see governments and central banks globally looking to (or
 being forced to) be more efficient by reducing budget shortfalls and flooding the
 system with credit to ease liquidity issues. We see new products and services being
 invented or implemented that will create new industries and new jobs. We see well-run
 companies that are growing and improving profits despite the recession. We see companies
 in trouble that we believe will repair, recover and prosper. We see companies taking
 share from weakened competitors. In sum, we believe that attractive investment opportunities
 are available today despite the near-term “doom and gloom”. As stewards
 of your capital, we are working very hard to provide conservative exposure to these
 opportunities in accordance with our investment philosophy and process that drives
 all of our fund products.
During the year we accomplished two non-investment
 items that we believe make our products more efficient and easier to understand.
 First, during the fiscal year we merged the FBR Pegasus Small Cap Growth Fund into
 the FBR Small Cap Fund. The combined fund should provide a meaningful increase in assets
 which improves the cost efficiencies achieved by a larger shareholder base. More
 importantly, the merger allows Robert Barringer, portfolio manager of both small
 cap funds, to focus his time, attention and efforts on executing his investment
 process on a single small cap core strategy. We believe the FBR Small Cap
 Fund is a viable solution for any investor looking to gain diversified exposure
 to the small cap segment of the market. Second, we eliminated the “Pegasus” sub-brand
 from our product line
2
nomenclature. This was done to simplify
 and remove any confusion regarding the objective of each of our core equity products
 as they are increasingly being uncovered by professional and retail investors “screening” for strong performing funds.
As in prior years, what follows in this
 report is a discussion with each portfolio manager with respect to the performance
 of their fund(s) over the 2010 fiscal year. It also includes their thoughts on related
 industry or company conditions and their investment outlook. We believe the commentary
 from our fund managers is an important part of our communications to you. We encourage
 you to read them to gain a better understanding of the investment philosophy and
 process that drives our organization.
All of us at The FBR Funds want to thank
 you for your continued support, and we look forward to serving your investment needs
 in the years ahead. As always, we welcome your questions and comments. You can reach
 us via e-mail at fbrfundsinfo@fbr.com or toll free at 888.200.4710. If you would
 like more timely updates, www.fbrfunds.com provides quarterly performance data as
 well as other important information.
Sincerely,


