Dear MMA Praxis shareholder:



Although the soft housing market and high rate of unemployment continue to bring stress to many people, saw signs of a strengthening economy including improved performance of the equity markets.



Domestic equities, as measured by the Standard &amp; Poors Index, rose by percent for the one year period ending Dec. following a gain of percent for the prior year. The MMA Praxis Core Stock Fund Class A Shares (without load) underperformed its benchmark and rose percent. Over the same period the Class A Shares (without load) of the MMA Praxis Value Index Fund rose by percent and Class A Shares (without load) of the MMA Praxis Growth Index Fund rose by percent. The Class A Shares (without load) of the MMA Praxis Small Cap Fund rose by percent and outperformed its benchmark, the Russell which rose by percent. We are very pleased with this outperformance.



The international equity benchmark MSCI EAFE Index rose by percent for the one-year period ending Dec. The MMA Praxis International Fund Class A Shares (without load) outperformed its benchmark over the one year period and rose by percent. This outperformance is very encouraging after an underperformance in



We are pleased to report that the MMA Praxis Intermediate Income Fund continues to perform well against its benchmark, the Barclays Capital U.S. Aggregate Bond Index, which rose percent. The Class A Shares (without load) posted a gain of percent for the year ending Dec. following a return of percent for the year ending Dec. when the benchmark return was percent. The three-year annualized returns were percent and five-year annualized returns were percent.



In January we launched the MMA Praxis Genesis Portfolios, which are considered fund of funds. This is an investment approach that uses a strategy of holding a group of funds rather than investing directly in individual stocks, bonds or other securities. The Genesis Portfolios invest in a combination of the other MMA Praxis Mutual Funds to achieve the investment objective of the Portfolios. We offer three portfolios: the Conservative Allocation portfolio (without load) returned percent, the Balanced Allocation portfolio (without load) returned percent and the Growth Allocation portfolio (without load) rose percent.



On the following pages, you will find portfolio managers letters and performance review for each of the Funds as well as an update on our stewardship investing activities. The work we do on behalf of you, the shareholder, in company screening, shareholder advocacy and community development are reminders that shareholders can have significant impact by the way they invest their dollars. I encourage you to read these letters for further explanation of their investment strategies and perspective of
























Thank you for being an investor with MMA Praxis Mutual Funds. As a faith-based mutual fund family, we are committed to helping you integrate your investments with your values. As always, we are dedicated to being responsible stewards of the resources you entrust to us, and we are grateful for your confidence.








Sincerely,
