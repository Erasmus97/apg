Dear Investor

Wall Street and Main Street often seem worlds apart, but the tenor of the two could not have been more different for most of Main Street remained glum as high unemployment and the continuing housing slump made the economic
recovery seem like an abstraction to many Americans. Conversely, after working through anxiety resulting from the Greek debt crisis, the flash crash brought about by an electronic trading glitch in early May, and the sluggish economic
recovery, Wall Street celebrated strong profit growth in the second half of the year, driving the major indexes back to levels reached on the eve of the financial crisis in the fall of The mood was especially ebullient among investors in
mid-cap stocks, who propelled the S&amp;P MidCap Index to within a few percentage points of its all-time high by year-end. The Institutional Mid-Cap Equity Growth Fund kept pace with its surging benchmarks, posting a solid gain in this
environment.



The Institutional Mid-Cap Equity Growth Fund gained for the past six months and for the year ended December The fund outperformed all of its comparison indexes over the past year, while slightly trailing its
growth-oriented indexes over the past six months. The fund remained favorably ranked relative to its competitors over all time periods. (Based on cumulative total return, Lipper ranked the Institutional Mid-Cap Equity Growth Fund out of
out of out of and out of funds in the mid-cap growth funds category for the and periods ended December respectively. Results will vary for other time periods. Past performance
cannot guarantee future results.)
In our letter last year, we disputed the common notion that stock investors had suffered a lost decade, noting that mid-cap stocks and other asset classes had performed
considerably better than the large-cap indexes referenced by most market observers. Indeed, as indicated by the line chart on page the strong performance of the past year saw an investment in the fund made years ago recapture the all-time high it established before the Great Recession took hold in late
Over the past years, as seen on the Average Annual Compound Total Return table on page the fund has gained an average of per year. Annualized returns for the fund were and for the and periods ended
December respectively. Current performance may be higher or lower than the quoted past performance, which cannot guarantee future results. Share price, principal value, and return will vary, and you may have a gain
or loss when you sell your shares. To obtain the most recent month-end performance, please call The funds expense ratio was as of its fiscal year ended December

Market Environment

The stealth rally of reflected the highly bifurcated nature of the economic recovery. Many American companies enjoyed stellar profit growth, thanks in part to vitality in emerging export markets as well as deep cost cuts made
during the economic downturn. The nations manufacturing sector expanded throughout most of and the service sector was showing consistent gains by year-end. Companies did well with less but resisted adding workers even as they once again
ramped up production. As a result, productivity has skyrocketed, but the unemployment rate has hovered near an abysmal With jobs still hard to find, consumer sentiment gauges have remained at low levels, and personal spending has defied past
recession patterns, increasing only slowly as the economy began to grow again.



Americans have begun to realize that we are only in the early stages of a multiyear period of adjustment away from a false prosperity built on a housing bubble and easy credit. The housing market remains deep in the doldrums, with home
sales and construction stuck at multi-decade lows and housing prices continuing to fall in some regions. As a result, over of homeowners are underwater on their mortgages. Many others, whose mortgage profiles are more secure, remain deeply risk averse and burdened with the need to pay down debt in other forms.
We believe that the personal savings rate, which has increased from around on the eve of the financial crisis to in Novemberbelow its long-term average of roughly go as high as in the next several years.



Readjustment and newfound thrift in the private sector have yet to be matched by federal, state, and municipal governments, which are falling deeper into debt with each passing month. This glaring juxtaposition gave rise to the Tea
Party movement, a populist uprising against unsustainable public spending and bailouts. While this is not the forum to judge the Tea Partys remedies, the popular backlash that it has engendered against Washington is affecting the views of
lawmakers. Since the November election, the Obama administration appears to be moving toward the center, becoming more hospitable to business, similar to the tack taken by the Clinton administration after its own setback in It is ironic,
however, that the first bipartisan action following the recent election was the budget-busting tax compromise of early December.

While the economy improved and the market soared in monetary policy remained extremely accommodative. The Federal Reserve, fearing a rerun of the deflationary environment in the U.S. in the or in Japan in the last two
decades, kept overall monetary policy as loose as it had been at the depth of the financial crisis, with the federal funds rate still hovering near Worried that even this was not enough, in early November the Fed renewed its purchases of
long-term Treasuries in an effort to keep long-term interest rates low and eventually spur hiring. To date, the Feds Rube Goldberg construct has failed, with long-term rates moving higher and job growth remaining anemic. Instead, this bold and
risky experiment appears to have fostered huge capital flows to already overheating emerging markets. Inflation has picked up alarmingly in many developing nations, and higher prices for commodities such as food and gasoline may presage a broad rise
in consumer prices in the U.S. in the coming yearsan expensive price to pay for the boost in confidence the Fed provided investors this fall.

Portfolio Review

As we have noted in previous letters, we typically would expect to lag our benchmarks somewhat during momentum-driven rallies such as we experienced over the past six months. Our focus on valuation typically serves us well in down markets, and over the course of a market cycle, but can restrain our gains in bull phases. However, we managed largely to keep pace with our benchmarks in the
second half of the year, thanks in large part to favorable stock selection in the energy, health care, and consumer staples sectors. Unsurprisingly in such a strong market, most of the funds holdings had gains over the past six months and
year.



Some of our best contributors in came from the consumer sectors. Despite our muted expectations for overall consumer spending, we have continued to believe that strong brands and innovative managements will be able to prosper as
they seize market share from less-nimble competitors. Chipotle Mexican Grill continued to play this role with its unique blend of inexpensive but high-quality food. Whole
Foods has also continued to defy skeptics who thought that customers would spurn the chain following the recession. The company posted outstanding operating results as it moved firmly beyond its ill-timed acquisition of Wild
Oats. We have similar hopes for Canadas Shoppers Drug Mart, which we consider the best drugstore chain in North America. The companys stock suffered from concerns about a new reimbursement
regime in Ontario but has bounced back as investors have begun to realize its longer-term potential. Innovative used car retailer CarMax, as well as Coach, Starbucks, and Marriott were also top
contributors in the periods. (Please refer to the portfolio of investments for a complete list of fund holdings and the amount each represents in the portfolio.)
Information technology holdings were also generally good performers. Longtime shareholders know the fund has had a
large weighting in the sector over the last several years due to the attractive growth prospects we see in many of these companies. Strong performers included Juniper Networks, which is benefiting from
demand for network communications infrastructure equipment needed to carry the rapidly increasing volume of Internet traffic; JDS Uniphase, which also sells a wide array of communications devices and
test and measurement equipment; and Rovi, which facilitates on-demand digital entertainment. We eliminated our poorly performing position in Palm, which
was unable to gain traction against the iPhone before being acquired by Hewlett-Packard last spring.

Health care stocks generally lagged as investors worried about Obamacare and focused on companies more likely to benefit from economic recovery, but they provided some of our best returns for the period. In our semiannual
letter, we discussed the outstanding performances of equipment firm Edwards Lifesciences and drugmaker Valeant Pharmaceuticals, and both firms continued to
move higher in the second half of the year. We also benefited from the release of positive clinical trial results for an asthma medication being developed by biotechnology firm Theravance. We typically
hold a basket of promising biotechnology firms on the theory that the large payoffs in the winners will more than offset the losses in instances where we are wrong.

There has been a paradigm change in the U.S. energy industry over the last five years as advances in drilling technology have opened up vast new oil and gas reserves in shale formations around the country. We have benefited from this
dynamic through investments in a number of fast-growing, mid-size producers, including Range Resources, Continental Resources, and
SM Energy. We also had two gas holdings, CNX Gas and Atlas Energy, become acquisition targets during
Our top
contributor for the past six months was engineering and construction firm McDermott International, whose fortunes are tied to the offshore oil and gas industries. Other industrial holdings, such as
electrical equipment maker AMETEK, benefited from the continuing cyclical upturn in the global economy.
Finally, we had mixed results among our financial holdings, another relatively small segment of
the portfolio. After a long absence from the industry, we made several investments in commercial banks in the first half of Our investment thesis was twofold: The collapse of securitized lending would offer banks the chance to reclaim an important source of revenues, and credit
conditions could not get much worse. While we enjoyed good results from most of our purchases, fears over new bank regulations have recently dogged holdings such as TCF Financial. We sold our shares in
struggling Marshall &amp; Ilsley along with capital markets firm Janus Capital Group and real estate developer St.
Joe. On the other hand, we continued to buy options exchange CBOE Holdings, a strong and valuable franchise whose shares have been battered since its initial public offering in
June.

Investment Strategy and Outlook

In past letters, we have emphasized the importance of central banks intervening when credit becomes too easy and asset prices become inflateda role former Fed Chairman William McChesney Martin, Jr., likened to taking away
the punch bowl just when the party gets going. We worry that, far from following Martins advice, his successors are spiking the punch bowl through the addition of two potent spirits. The first is ZIRP, the Feds zero interest rate
policy; while it has greatly benefited the largest borrowersgovernments, banks, speculators, and large corporationsZIRP has inflicted significant hardship on many of the nations savers, particularly senior citizens who rely on
income from savings accounts and certificates of deposit. The second spirit is the Feds new round of quantitative easing, which we view as a polite term for printing money and monetary debasement. As noted above, much of the intoxication
from is occurring overseasin Asian property markets, for exampleas liquidity created by the program quickly flows to where returns are highest. The policy has led to resentment on the part of our trading partners, who have been
burdened with surging inflation and new asset bubbles, along with increased efforts to replace the U.S. dollar as the worlds reserve currency.

To be sure, the party on Wall Street in recent months has been less rollicking than it was at the height of the housing bubble. Mid-cap valuations have risen but remain reasonableand stocks in general appear attractive relative to
bonds. Growth shares only modestly outperformed value stocks over the past year and remain relatively attractive, particularly large-caps. Still, we sense that a bit of intoxication is beginning to creep back into our market. Momentum investing has mounted a comeback among a narrow group of high-growth
stocks, most notably in hot areas such as cloud computing and social media.



While the fund has benefited from this enthusiasm, we are intent on remaining the sober ones at the party, focused on companies with durable franchises that are likely to prosper over the long term. Thus, we are trimming positions in
which valuations have become stretched and investing in companies that will benefit as the global economy restructures. We remain particularly focused on the U.S. manufacturing renaissance, which we still view as the best way out of our
nations dependence on debt-financed consumption. We look forward to reporting to you on the results of our efforts in six months.

Respectfully submitted,
