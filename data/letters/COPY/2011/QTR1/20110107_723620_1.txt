Dear Fellow Shareholder,



The fixed income markets generally provided solid results in the fiscal year ended October While there were periods of heightened volatility, they were largely offset by robust risk appetite, as investors looked to generate incremental income in the low-interest-rate environment. Against this backdrop, non-Treasury securities outperformed their Treasury counterparts. Also supporting non-Treasuries were the continued economic expansion, benign inflation and strengthening corporate balance sheets.



Our Fixed Income Funds were generally positioned to benefit from the outperformance of non-Treasuries. In particular, exposure to the areas of the market that had lagged the most during the credit crisis enhanced the Funds' results during the reporting period.



Looking ahead, while the lengthiest recession since the Great Depression has finally been declared over, the U.S. economy still faces a number of headwinds. These include continued elevated unemployment and a housing market that is struggling to regain its footing. Despite these challenges, it is our belief that the economy has enough momentum to avoid a double-dip recession. That said, we expect to see growth that is more modest than previous periods exiting a severe recession.



One factor that we believe will support the economy going forward is continued low interest rates. With economic growth moderating during the second half of the reporting period, the Federal Reserve kept the Federal Funds rate at a historically low range of to In addition, during its September meeting, Federal Reserve Chairman Ben Bernanke indicated that the Fed was "prepared to provide additional accommodation if needed to support the economic recovery..." In November, after the reporting period had concluded, the Federal Reserve announced its intention to purchase billion in Treasuries by the end of June in an attempt to keep longer-term rates low and support the economy. Whether this plan has its desired effect, of course, remains to be seen.



Against a backdrop of moderately positive economic growth and mild inflation, we believe that non-Treasuries could continue to generate solid results, and our Fixed Income Funds are positioned accordingly. In addition, it is our belief that demand for non-Treasuries will remain strong given the low yields offered by Treasury securities.



In conclusion, as the last few years have shown, the financial markets can experience periods of unsettling volatility. Given uncertainties surrounding the economy and the Federal Reserve's policies, we believe there is a chance that the financial markets will experience higher volatility going forward. Should such volatility occur, we urge shareholders to maintain a long-term perspective.



Thank you for your continued support and trust. We look forward to continue serving your investment needs in the years to come.



Sincerely,
