DEAR SHAREHOLDER,

More than a full year into the recovery that officially began in the
U.S. economy has been growing, but unevenly. Corporate profits, consumer
spending and some manufacturing measures have been rising or holding steady,
while the employment and housing markets have remained stagnant. A soft patch
occurred in when a debt crisis struck Greece, and the potential for a
double-dip U.S. recession loomed. While slipping back into recession appears
less likely, the U.S. economy has yet to create much in the way of jobs or wean
itself off government involvement in everything from cars to banks to housing.
Still, by the year ended October investors looked at an economy where
stocks were the highest in two years, growth had persisted for five straight
quarters and inflation was virtually flat.

Trying to stimulate economic growth, but unable to lower interest rates below
their current levels of near zero, the Federal Reserve would like to avoid the
specter of deflation and so has resorted to the controversial practice of
printing money to buy government bonds, otherwise known as quantitative easing.
Proponents hope it will produce more than modest support for the economy, while
driving long-term interest rates even lower, thus encouraging businesses and
individuals to borrow. Critics contend that it could push down the dollar's
value, triggering commodity price rises, and set the stage for inflation above
the Fed's generally accepted range of about Markets reacted positively to
the prospect of this new bout of bond buying by the Fed, as stock prices grew
about from Labor Day until Halloween, compared with about a rise for
corporate bonds. Other analysts attributed the rally to the shift in the
political alignment in Washington based on the mid-term elections, which is
expected to slow down or even reverse recent overhauls of health care and
financial regulation, as well as more prominently address ways to reduce public
debt.

OUTLOOK
As the impact wanes from recent catalysts such as Congressional realignment,
quantitative easing and positive earnings and economic reports, markets are apt
to wake up to the reality of a continuing sluggish economy and political
stalemate in Washington. Some analysts are expecting an easing of monetary
policy globally, as the world responds to a weaker dollar and looser U.S.
financial conditions. Many are maintaining neutral allocations across major
asset classes, with an emphasis on growth outside the U.S. market, particularly
in emerging markets. Favored areas include commodities and growth stocks paying
a dividend. Equities are expected to offer the highest risk premia in the coming
months, and corporate paper is favored over government securities as the era of
extraordinary returns in credit may be coming to an end.

RYDEX|SGI ETFS
ETFs have continued to enjoy popularity in the current uncertain market
environment, and not just because of their well-known benefits of transparency,
convenience and low cost. ETFs incorporate some of the most innovative product
development and fastest asset growth in the financial industry. There was a
increase in assets under management in ETFs over the month period ended
September according to the Investment Company Institute. As of October
there were about exchange traded funds with about billion in
assets in the U.S. and more than trillion globally.

At Rydex|SGI, assets under management increased by during the past months,
and the firm closed leveraged or inverse ETFs as it rationalized its product
line-up. Today, the firm offers exchange traded products, including currency
funds, two leveraged or inverse products, nine sector funds, six pure style
funds, a mega cap fund and an equal weight S&P Index* fund, our best-known
ETF.

GUGGENHEIM PARTNERS
As you may know, Security Global Investors' (SGI) parent firm, Security Benefit
Corporation (Security Benefit), was acquired this year by a group of investors
led by Guggenheim Partners. Guggenheim Partners is a diversified financial
services firm with billion in assets under supervision and a well-known
name in the investment-management industry. We look forward to a long and
productive relationship with Guggenheim Partners, as we do with you, our
shareholders, and thank you for the trust you place in us.

Sincerely,

DEAR SHAREHOLDER,

More than a full year into the recovery that officially began in the
U.S. economy has been growing, but unevenly. Corporate profits, consumer
spending and some manufacturing measures have been rising or holding steady,
while the employment and housing markets have remained stagnant. A soft patch
occurred in when a debt crisis struck Greece, and the potential for a
double-dip U.S. recession loomed. While slipping back into recession appears
less likely, the U.S. economy has yet to create much in the way of jobs or wean
itself off government involvement in everything from cars to banks to housing.
Still, by the year ended October investors looked at an economy where
stocks were the highest in two years, growth had persisted for five straight
quarters and inflation was virtually flat.

Trying to stimulate economic growth, but unable to lower interest rates below
their current levels of near zero, the Federal Reserve would like to avoid the
specter of deflation and so has resorted to the controversial practice of
printing money to buy government bonds, otherwise known as quantitative easing.
Proponents hope it will produce more than modest support for the economy, while
driving long-term interest rates even lower, thus encouraging businesses and
individuals to borrow. Critics contend that it could push down the dollar's
value, triggering commodity price rises, and set the stage for inflation above
the Fed's generally accepted range of about Markets reacted positively to
the prospect of this new bout of bond buying by the Fed, as stock prices grew
about from Labor Day until Halloween, compared with about a rise for
corporate bonds. Other analysts attributed the rally to the shift in the
political alignment in Washington based on the mid-term elections, which is
expected to slow down or even reverse recent overhauls of health care and
financial regulation, as well as more prominently address ways to reduce public
debt.

OUTLOOK

As the impact wanes from recent catalysts such as Congressional realignment,
quantitative easing and positive earnings and economic reports, markets are apt
to wake up to the reality of a continuing sluggish economy and political
stalemate in Washington. Some analysts are expecting an easing of monetary
policy globally, as the world responds to a weaker dollar and looser U.S.
financial conditions. Many are maintaining neutral allocations across major
asset classes, with an emphasis on growth outside the U.S. market, particularly
in emerging markets. Favored areas include commodities and growth stocks paying
a dividend. Equities are expected to offer the highest risk premia in the coming
months, and corporate paper is favored over government securities as the era of
extraordinary returns in credit may be coming to an end.

RYDEX|SGI ETFS

ETFs have continued to enjoy popularity in the current uncertain market
environment, and not just because of their well-known benefits of transparency,
convenience and low cost. ETFs incorporate some of the most innovative product
development and fastest asset growth in the financial industry. There was a
increase in assets under management in ETFs over the month period ended
September according to the Investment Company Institute. As of October
there were about exchange traded funds with about billion in
assets in the U.S. and more than trillion globally.

At Rydex|SGI, assets under management increased by during the past months,
and the firm closed leveraged or inverse ETFs as it rationalized its product
line-up. Today, the firm offers exchange traded products, including currency
funds, two leveraged or inverse products, nine sector funds, six pure style
funds, a mega cap fund and an equal weight S&P Index* fund, our best-known
ETF.

GUGGENHEIM PARTNERS

As you may know, Security Global Investors' (SGI) parent firm, Security Benefit
Corporation (Security Benefit), was acquired this year by a group of investors
led by Guggenheim Partners. Guggenheim Partners is a diversified financial
services firm with billion in assets under supervision and a well-known
name in the investment-management industry. We look forward to a long and
productive relationship with Guggenheim Partners, as we do with you, our
shareholders, and thank you for the trust you place in us.

Sincerely,

DEAR SHAREHOLDER,

More than a full year into the recovery that officially began in the
U.S. economy has been growing, but unevenly. Corporate profits, consumer
spending and some manufacturing measures have been rising or holding steady,
while the employment and housing markets have remained stagnant. A soft patch
occurred in when a debt crisis struck Greece, and the potential for a
double-dip U.S. recession loomed. While slipping back into recession appears
less likely, the U.S. economy has yet to create much in the way of jobs or wean
itself off government involvement in everything from cars to banks to housing.
Still, by the year ended October investors looked at an economy where
stocks were the highest in two years, growth had persisted for five straight
quarters and inflation was virtually flat.

Trying to stimulate economic growth, but unable to lower interest rates below
their current levels of near zero, the Federal Reserve would like to avoid the
specter of deflation and so has resorted to the controversial practice of
printing money to buy government bonds, otherwise known as quantitative easing.
Proponents hope it will produce more than modest support for the economy, while
driving long-term interest rates even lower, thus encouraging businesses and
individuals to borrow. Critics contend that it could push down the dollar's
value, triggering commodity price rises, and set the stage for inflation above
the Fed's generally accepted range of about Markets reacted positively to
the prospect of this new bout of bond buying by the Fed, as stock prices grew
about from Labor Day until Halloween, compared with about a rise for
corporate bonds. Other analysts attributed the rally to the shift in the
political alignment in Washington based on the mid-term elections, which is
expected to slow down or even reverse recent overhauls of health care and
financial regulation, as well as more prominently address ways to reduce public
debt.

OUTLOOK

As the impact wanes from recent catalysts such as Congressional realignment,
quantitative easing and positive earnings and economic reports, markets are apt
to wake up to the reality of a continuing sluggish economy and political
stalemate in Washington. Some analysts are expecting an easing of monetary
policy globally, as the world responds to a weaker dollar and looser U.S.
financial conditions. Many are maintaining neutral allocations across major
asset classes, with an emphasis on growth outside the U.S. market, particularly
in emerging markets. Favored areas include commodities and growth stocks paying
a dividend. Equities are expected to offer the highest risk premia in the coming
months, and corporate paper is favored over government securities as the era of
extraordinary returns in credit may be coming to an end.

RYDEX|SGI ETFS

ETFs have continued to enjoy popularity in the current uncertain market
environment, and not just because of their well-known benefits of transparency,
convenience and low cost. ETFs incorporate some of the most innovative product
development and fastest asset growth in the financial industry. There was a
increase in assets under management in ETFs over the month period ended
September according to the Investment Company Institute. As of October
there were about exchange traded funds with about billion in
assets in the U.S. and more than trillion globally.

At Rydex|SGI, assets under management increased by during the past months,
and the firm closed leveraged or inverse ETFs as it rationalized its product
line-up. Today, the firm offers exchange traded products, including currency
funds, two leveraged or inverse products, nine sector funds, six pure style
funds, a mega cap fund and an equal weight S&P Index* fund, our best-known
ETF.



ANNUAL REPORT





LETTER TO OUR SHAREHOLDERS
- --------------------------------------------------------------------------------

GUGGENHEIM PARTNERS

As you may know, Security Global Investors' (SGI) parent firm, Security Benefit
Corporation (Security Benefit), was acquired this year by a group of investors
led by Guggenheim Partners. Guggenheim Partners is a diversified financial
services firm with billion in assets under supervision and a well-known
name in the investment-management industry. We look forward to a long and
productive relationship with Guggenheim Partners, as we do with you, our
shareholders, and thank you for the trust you place in us.

Sincerely,
