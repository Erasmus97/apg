Dear Fellow Shareholder:



Like a jigsaw puzzle piece, Tax-Free Fund For Utahs co-portfolio managers must decide whether any municipal bond under consideration for addition to the Funds portfolio has the potential to fit correctly into place. Otherwise, the addition might distort the overall picture.



Specifically, any bond under consideration must fit in terms of the principal amount, quality, maturity, liquidity and sector diversification.



For example, lets say the Fund has to invest. The portfolio manager must first find bonds available for purchase in the marketplace within that size range. Then, it must be decided whether to purchase one bond for the full amount of several offerings or some other combination. If the portfolio already possesses a significant holding in Issuer A, the portfolio manager may decide to purchase a smaller additional offering so as not to overweight the portfolio in that particular issuer.



The bonds available for purchase must also be looked at in terms of quality, maturity and sector diversification. As you know, the municipal bonds in Tax-Free Fund For Utah must be rated investment grade within the top four credit ratings assigned by a Nationally Recognized Statistical Rating Organization (NRSRO) like Moodys or Standard &amp; Poors or, if unrated, must be determined to be of comparable quality. The portfolio is also managed to have an intermediate maturity (as of December the Funds average maturity was years) and a reasonable degree of diversification among varying projects.



So, if both a transportation and a school bond are available with identical maturities and quality rating, the portfolio manager may decide for sector diversification purposes to purchase the transportation bond if the portfolio already contains a sufficient amount of school bonds.



NOT A PART OF THE SEMI-ANNUAL REPORT

























The last piece of the puzzle is an ongoing attempt to keep the overall portfolio functioning smoothly, such that when one bond is removed, another complementary one is sought in an effort to keep the picture just right.



Sincerely,
