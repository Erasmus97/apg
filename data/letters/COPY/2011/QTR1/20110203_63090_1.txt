Dear Shareholders: 
After an
extended rebound in the financial markets, uncertainty returned in early 2010 as investors began to question the durability of the recovery for global economies and markets. That uncertainty led to increased risk aversion, especially as investors
saw the eurozone struggle with the debt woes of many of its members. In September, the U.S. Federal Reserve Board’s promises to further loosen monetary policy helped assuage market fears and drive asset prices off their recent lows. A
combination of solid earnings and improving economic data gave an additional boost to investor sentiment. As we begin 2011, we are cautiously optimistic that economic growth will continue to improve and that the global economies will recover from
the shocks of the past few years. We expect the pace of recovery worldwide will be uneven and volatile. 
As always, we continue to be mindful
of the many challenges faced at the individual, national, and international levels. It is in times such as these that we want to remind investors of the merits of maintaining a long-term view, adhering to basic investing principles such as asset
allocation and diversification, and working closely with their advisors to research and identify investment opportunities. 
Respectfully,



