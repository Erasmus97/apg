Dear Fellow Shareholders*:



The total return for the Meehan Focus Fund (the Fund) for its fiscal year ended October was Funds net asset value (NAV) at October was per share (net of a per share income distribution).Over the past fiscal year the Funds return trailed the Standard and Poors Total Return Index** (S&amp;P and the NASDAQ Composite Index** (NASDAQ).However, the Fund has outperformed the S&amp;P and the NASDAQ over the past ten years and since inception.The Funds results for its fiscal year, for five years, and for ten years a
re shown below with comparable results for the S&amp;P and NASDAQ.








Fiscal Year

November -

October


Annualized Return

Five Years November

- October


Annualized Return

Ten Years November

- October



Meehan Focus Fund












S&amp;P Total Return Index **












NASDAQ**
















Past performance does not guarantee future results.Performance data quoted above represents past performance, and the investment return and principal value of an investment will fluctuate so that an investors share, when redeemed, may be worth more or less than their original cost.Current performance may be lower or higher than the performance data quoted.To obtain more current performance information, please call






The Meehan Focus Fund's total annual expenses are



______________________

*The views expressed herein are not meant as investment advice.Although some of the described portfolio holdings were viewed favorably as of the date of this letter, there is no guarantee that the Fund will continue to hold these securities in the future.Please consider the investment objectives, risks, charges, and expenses of the Fund before investing.Contact the Fund at for a prospectus, which contains this and other important information about the Fund.Read the prospectus carefully before investing.



** The S&amp;P Total Return Index is an unmanaged index of U.S. stocks and represents the broad performance of the U.S. stock market. The NASDAQ Composite Index measures all NASDAQ domestic and non-U.S. based common stocks listed on the NASDAQ Stock Market.Index performance does not include transaction costs or other fees, which will affect actual performance.


























The stock market has rebounded from its spring swoon as signs continue to point towards a steady, albeit slower than hoped for, economic recovery.The rally that began this past summer has extended into the holiday season, and the Funds NAV as of December is an increase of more than five percent since the close of the Funds fiscal year.



Despite their recent rally, we believe stocks are attractive at current valuations and see several reasons to be optimistic about the prospects for the markets and the economy as we look to a strong corporate earnings are forecast to be at record levels in and recent reports suggest consumer spending, critical to a sustainable recovery, is up in the all important holiday shopping season.The Federal Reserve has again signaled its commitment to keeping the recovery on track with a second round of quantitative easing set to pump an additional billion into the economy over the next six months.And lastly, we believe that the tax deal struck in Congress, while it only delays our day of fiscal reckoning, is likely to provide a near term boost to the economy.



As is usually the case, not all signs are positive.Although recent employment reports have shown indications that hiring is finally picking up, unemployment remains stubbornly high at hope that as the economic recovery moves forward companies will begin putting their record cash hoards to work by hiring more workers, further boosting the economy.Sovereign debt issues, like the recent Irish bailout, continue to haunt Europe, but the Euro-zone nations appear willing to take the necessary measures to avert a major meltdown.Back at home, states and municipalities are facing their own debt and spending crunch, forcing cutbacks that could blunt the recovery.Nevertheless, we are optimistic that the economy will overcome these obstacles.



Since our last report we have added three stocks to the Funds portfolio: Internet advertising and search engine heavyweight Google, global retailing giant Wal-Mart, and small cap Florida land and development company St. Joe.We also substantially increased our positions in ExxonMobil, Thermo Fisher Scientific, and Exelon.



We exited positions in two stocks over the past six months: Conoco Phillips and Monsanto.Conoco Phillips was sold at a solid gain after we concluded that competitor ExxonMobil possessed better long-term potential.As discussed in our last report, we sold Monsanto after generic competition and a farmer backlash against the high cost of genetically modified seeds reduced profits and dimmed its prospects.We also reduced our positions in Berkshire Hathaway, Nestle, and Wellpoint.Proceeds from these sales were redeployed to new and existing positions we believe offer better growth prospects going forward.



Portfolio Review



The attached Schedule of Investments identifies the stocks we owned and their market value as of October Fund held stocks, and nearly of the Funds assets were invested in companies.Our top holdings, which represented of the Funds portfolio, were as follows:






























Company


% of Fund












Diageo PLC ADR










American Express Co.










Procter &amp; Gamble










Berkshire Hathaway Inc. B










Automatic Data Processing, Inc.










Microsoft Corp.










ExxonMobil Corp.










Cisco Systems, Inc.










Johnson &amp; Johnson










Nestle SA Reg B ADR

















As of October nine of our top ten holdings showed gains, with Procter &amp; Gamble the lone exception.Our largest gains, in dollar terms, were in Berkshire Hathaway, Nestl, and General Electric.As of this writing, Procter &amp; Gamble shows a small gain and we believe that with its well known brands, history of innovation, and growing emerging markets presence, the company is well positioned to benefit as the economic recovery gains strength.



Solid gains from Google, Novartis, and Weight Watchers partially offset declines from home improvement retailer Lowes and several other Fund holdings.Lowes, our weakest performer over the past six months, is representative of our current focus on large capitalization, dividend paying stocks, which we believe offer the most attractive valuations in todays market.These stocks have trailed the overall market over the past year and have been a drag on the Funds performance.However, despite this recent underperformance, we believe that, over the long term, financially strong businesses with durable competitive advantages that are selling at a discount to their intrinsic value will outperform the broader market.



Brief Discussion of Three of Our Top Holdings








Average Cost

Per Share


October

Market Price

per Share


Percent

Increase

(Decrease)



ExxonMobil












Novartis AG ADR












United Parcel Service B

















































Price

Market Cap.




B












Forward P/E

Price / Sales

















Price / Book

Dividend Yield














ExxonMobil is the worlds largest integrated oil and gas company, operating in countries around the globe to explore for, produce, and refine petroleum and natural gas production totaled million barrels of oil and billion cubic feet of gas each day.ExxonMobil also owns refineries, more than any other company, and is one of the biggest global chemical producers.



We initiated our position in ExxonMobil late last year, purchasing shares of natural gas producer XTO Energy after ExxonMobil announced its intent to acquire the company in an all stock deal and while XTOs shares were trading at a discount to the acquisition price.We received ExxonMobil shares when the deal closed in June of this year.



ExxonMobils primary competitive advantage stems from its enormous scale and expertise.Because most large production and exploration opportunities are under the control of foreign governments, ExxonMobils reputation and capabilities provide an important competitive advantage that should enable it to continue replenishing reserves more successfully than its peers.



Revenues and profits for ExxonMobil and other energy firms are closely tied to oil and gas prices.After plunging from highs, prices have recovered and oil now trades at roughly per barrel.Meanwhile, natural gas prices have slumped due to lower demand from the slow economy and a supply glut. We believe prices for both oil and natural gas will rise over time as an improving global economy, particularly in emerging markets such as Asia, drives up demand.



ExxonMobil has proven itself to be skilled in allocating capital, most recently with its acquisition of XTO, which makes ExxonMobil the largest natural gas producer in the U.S.XTOs advanced drilling technologies should enable ExxonMobil to increase production in new and existing fields near large U.S. markets.This, in turn, should boost profits as utilities shift to using more natural gas for electricity generation.



ExxonMobils focus on shareholder returns is evidenced by its payment of billion in dividends and use of billion for share repurchases over the past five years.We expect more of the same due to high returns on equity and tremendous cash flows.ExxonMobils stock offers a dividend yield of and trades at a forward price/earnings multiple of a significant discount to the overall market.We are excited about the companys prospects and expect ExxonMobils shares to provide solid returns over time.















Price

Market Cap.





B









Forward P/E


Price / Sales




















Price / Book

Dividend Yield













Atlanta-based UPS is the largest package-delivery firm in the world.The companys ubiquitous brown trucks, airplanes, and other freight vehicles deliver million packages each day across over countries and territories worldwide.UPS also offers specialized transportation and logistics services.



We added UPS to the Funds portfolio several years ago, attracted by its efficiency, profitability, and dominant market position, which remain among its most attractive attributes today.UPS and its primary competitor, FedEx, dominate the U.S. delivery market, which accounted for of UPSs revenue.Consolidation among industry participants and high barriers to entry should continue to favor existing, large delivery firms such as UPS.



While FedEx has a huge worldwide operation, UPSs daily domestic delivery volume and market capitalization are more than two times larger.UPS is also more efficient, using the same network for overnight and standard ground shipments.In contrast, FedEx runs duplicate operations for the two different businesses.



UPSs legendary efforts to boost efficiency, including detailed studies of the best techniques for fastening delivery truck safety belts and walking to the door of a house, enable the company to produce higher returns and cash flows than its peers.UPS also invests heavily in efficiency enhancing technology.For example, use of complex algorithms to map drivers routes, combined with other initiatives to optimize sorting and delivery of packages, has resulted in million fewer miles driven each year.



Like other cyclical businesses, UPS suffered volume declines in due to the weak global economy; but, as the economy has rebounded, so has UPS.The company has seen particularly rapid growth in its international delivery operations, led by a rise in the second quarter in shipments from Asia where the companys new international air hub in Shanghai enables UPS to benefit from booming Asian economies.



Employees, officers, and directors own about of the companys outstanding shares, which contributes to the culture of efficiency and improvement.UPS has averaged returns on equity in the over the past five years, has billion in cash on its balance sheet, and continues to generate tremendous cash flows to support future share repurchases, capital expenditures, and dividends.The stock currently yields forward, we believe UPS will provide handsome rewards to long-term investors.





































Price

Market Cap.





B









Forward P/E


Price / Sales






















Price / Book

Dividend Yield













With a market cap of nearly billion, Swiss pharmaceutical company Novartis is one of the worlds largest pharmaceutical and health care companies, and a long-term Fund holding.Novartis operates in four main segments: branded pharmaceuticals of sales), generics of sales), diagnostic and vaccines of sales), and consumer health products



Like all major pharmaceutical companies Novartis faces patent expirations on some of its blockbuster drugs, such as hypertension drug Diovan and leukemia drug Gleevec.However, Novartis faces fewer expirations spread over a longer time period than many of its competitors.Further helping to offset looming expirations is one of the strongest drug pipelines in the industry, with drug projects in development.Several potential blockbusters have received, or are close to receiving, approval for use, including Gilenya for multiple sclerosis, Tasigna for leukemia, and MenB for meningitis.



In addition to its strength in branded pharmaceuticals, Novartiss Sandoz division is the worlds second largest generic drug maker.The generics market worldwide has been growing at roughly a year and is projected to continue that growth rate into driven by the needs of aging populations in the U.S. and Europe and a desire to control health care costs.We believe Sandoz is well positioned to benefit from these trends, which should help dampen the impact of Novartiss patent expirations.



Results through the first nine months of this year have been strong with sales up and net income up Novartiss nearly completed acquisition of eye care company Alcon should further diversify its product lineup, boost margins, and eliminate uncertainty about the deal that has dragged on its share price.



Novartis carries an AA+ credit rating and produces strong cash flows which it uses to support its substantial R&amp;D efforts and pay a very attractive dividend.Currently trading at a substantial discount to the broader market, we think Novartis offers patient investors an outstanding opportunity.



Sources for charts and text: Morningstar, Value Line, Standard and Poor's, Yahoo Finance, company reports, and Edgemoor Investment Advisors estimates.



























Conclusion




We appreciate your confidence in our management of the Fund and look forward to continued success.You can check the Funds NAV online at any time by typing in the Funds symbol (MEFOX) in most stock quotation services.Best wishes for a happy holiday season -- and please do not hesitate to contact us if you have any questions regarding the status of your investment in the Fund.












Sincerely,
