Dear Fellow Shareholder,


On behalf of Transamerica Partners Institutional Funds Group and Transamerica Institutional Asset
Allocation Funds, we would like to thank you for your continued support and confidence in our
products as we look forward to continuing to serve you and your financial adviser in the future. We
value the trust you have placed in us.


This annual report is provided to you with the intent of presenting a comprehensive review of the
investments of each of your funds. The Securities and Exchange Commission requires that annual and
semi-annual reports be sent to all shareholders, and we believe this report to be an important part
of the investment process. In addition to providing a comprehensive review, this report also
provides a discussion of accounting policies as well as matters presented to shareholders that may
have required their vote.


We believe it is important to recognize and understand current market conditions in order to
provide a context for reading this report. During the past twelve months, markets have generally
advanced, yet have exhibited periods of weakness in conjunction with investors concerns over the
health of the economy and the labor market, and periods of strength in conjunction with investors
optimism of recovery and improved economic data points. The equity markets exhibited weakness in
February, advanced into late April, were weak during June, July and August, and ended the period at
the highs of the year. The period ended with positive twelve month returns for both the broad
equity and bond markets. The U.S. Dollar has risen and fallen versus the British Pound and Euro
along with rising and falling levels of concern over foreign debt levels during the year. The U.S.
Dollar ended the period modestly stronger versus the Euro and the British Pound, and weaker versus
the Japanese Yen. Oil prices have remained volatile over the past year, hitting their lows in early
summer and rebounding to end higher at the end of the period. The Federal Reserve has kept the
federal funds rate to a range of in an effort to stimulate the economy. The unemployment
rate has been sluggish to recede, beginning the period at and ending the period at
Anticipation of economic recovery has led to strong gains for particular equity and fixed-income
sectors, including small-cap and mid-cap stocks, emerging market stocks, and high yield bonds.
Money market securities have lagged on a relative basis. For the twelve months ended
the Dow Jones Industrial Average returned the Standard Poors Index returned
and the Barclays Capital U.S. Aggregate Bond Index returned Please keep in mind it
is important to maintain a diversified portfolio as investment returns have historically been
difficult to predict.


In addition to your active involvement in the investment process, we firmly believe that a
financial adviser is a key resource to help you build a complete picture of your current and future
financial needs. Financial advisers are familiar with the markets history, including long-term
returns and volatility of various asset classes. With your financial adviser, you can develop an
investment program that incorporates factors such as your goals, your investment timeline, and your
risk tolerance.


Please contact your financial adviser if you have any questions about the contents of this report,
and thanks again for the confidence you have placed in us.



Sincerely,
