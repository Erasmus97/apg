Dear Shareholder: 
If 2009 was the year that we tried to sort out the aftermath of the global financial crisis and recession, then 2010 should be viewed as the year we began to slowly emerge from the crisis and embark on the
road to recovery. 
 
Dear Shareholder: 
If 2009 was the year that we tried to sort out the aftermath of the global financial crisis and recession, then 2010 should be viewed as the year we began to slowly emerge from the crisis and embark on the
road to recovery. 
 
Dear Shareholder: 
If 2009 was the year that we tried to sort out the aftermath of the global financial crisis and recession, then 2010 should be viewed as the year we began to slowly emerge from the crisis and embark on the
road to recovery. 
 
Dear Shareholder: 
If 2009 was the year that we tried to sort out the aftermath of the global financial crisis and recession, then 2010 should be viewed as the year we began to slowly emerge from the crisis and embark on the
road to recovery. 
 
Dear Shareholder:

If 2009 was the year that we tried to sort out the aftermath of the global financial crisis and recession,
then 2010 should be viewed as the year we began to slowly emerge from the crisis and embark on the road to recovery. 
 
Dear Shareholder: 
If 2009 was the year that we tried to sort out the aftermath of the global financial crisis and recession, then 2010 should be viewed as the year we began to slowly emerge from the crisis and embark on the
road to recovery. 
 
Dear Shareholder: 
If 2009 was the year that we tried to sort out the aftermath of the global financial crisis and recession, then 2010 should be viewed as the year we began to slowly emerge from the crisis and embark on the
road to recovery. 
 
Dear Shareholder: 
If 2009
was the year that we tried to sort out the aftermath of the global financial crisis and recession, then 2010 should be viewed as the year we began to slowly emerge from the crisis and embark on the road to recovery. 
Last year, some welcome improvements in economic data appeared to indicate that a modest recovery was occurring. Investors responded warmly to these
signs by displaying their appetite for risk. The equity markets rebounded sharply, and by November 18, 2009, the Standard &amp; Poor’s 500 Index (the “S&amp;P 500 Index”) had risen by nearly 67% from its 14-year low on
March 9, 2009. By the end of December 2009, the S&amp;P 500 Index had risen 26.5% for the year to close at 1,115, and this positive momentum carried over into early 2010. 
Quote 
“If 2009 was the year that we tried to sort out the aftermath of the global
financial crisis and recession, then 2010 should be viewed as the year we began to slowly emerge from the crisis and embark on the road to recovery.” 
However, investors’ upbeat mood didn’t last for long. A wave of discouraging U.S. economic data, compounded by sovereign debt issues in Europe, led to a major market correction in May 2010,
followed by heightened market volatility throughout most of the summer. Beginning in the latter half of the third quarter, however, the markets responded well to a wave of much anticipated news, including a strong September labor market report, the
U.S. mid-term elections, as well as the Federal Reserve’s (“the Fed”) announced launch of a second round of quantitative easing (“QE2”). 
These gains, however, should be viewed as tentative, as while the recovery continues, markets remain sensitive to risks such as high unemployment, the future direction of tax policy, as well as concerns
that Ireland’s fiscal issues will contribute to additional European financial stress. 
We believe, however, that the current economic
recovery offers some encouraging signs for investors, including positive gross domestic product growth and a strong conclusion to third quarter 2010 corporate earnings, as many companies reported some of their healthiest profit margins in years.

Investors buoyed by solid corporate earnings 
During the late spring and summer months, economic uncertainty and fears of deflation drove investors to the safety of U.S. Treasuries and gold. A recent run of positive news, however, including improved
economic data and better-than-expected corporate earnings, led to a surge late in the third quarter. As of the end of the 12- month period ended October 31, 2010, the Standard &amp; Poor’s 500 Index had reached a level of 1,183, a
16.5% increase from 12 months earlier. 
Although global stock indices reflected steady growth throughout most of the year, this trend has
recently been interrupted due to growing anxiety surrounding sovereign debt in Europe and inflationary concerns in China. However, as of the end of the 12-month reporting period, the MSCI EAFE Index (Europe, Australasia, and the Far East) had
returned 8.8% (gross), while the MSCI EM (Emerging Markets) Index had returned 23.9% (gross) for the same reporting period. 
Treasuries
move higher, pushing yields to historic lows 
Weak economic growth boosted the fixed income market throughout the year, as investors sought
safety in U.S. Treasuries and high-quality corporate bonds. In this environment, the Barclays Capital High Yield Index returned 19.4%, while the Barclays Capital Emerging Markets Index returned 18.3% for the 12-month period ended October 31,
2010. The Barclays Capital U.S. Aggregate Bond Index returned 8.0% for the same period. 
 
1 
Table of Contents
Investors continued to demonstrate their concern about the stability of the economic recovery, pushing bond
prices up and yields down. At one point, these concerns, combined with near-zero official policy rates and central bank bond purchases, drove 10-year yields to their lowest levels since January 2009. As of October 31, 2010, the yields on the
benchmark 10-year Treasury bond had dropped from 3.4% to 2.6%. Yields on the 30-year bond also declined, falling from 4.2% to 4.0% as of the end of the period, as did the two-year note, from 0.9% to 0.3%. 
Will QE2 promote stronger economic growth? 
In a much anticipated action, the Fed initiated a second round of quantitative easing designed to stimulate the economy. It plans to spend an additional $600 billion to buy a wide range of both short-term
and long-term U.S. Treasuries. In its statement, the Fed also indicated that it may extend the program if conditions warrant doing so, and promised to “employ its policies as needed.” Although this measure may potentially hold down both
short and long-term interest rates, it does increase the risk of higher inflation and rising interest rates down the road. Additionally, the flexibility that the Fed has afforded itself in implementing the program may increase uncertainty about
future monetary policy and the economy. 
Certainly, if the economy continues to improve going forward, the Fed may likely resume a more
balanced posture. However, due to the uncertainty of the impact of this plan, it still makes sense for investors to maintain a balanced portfolio, including a diversified approach to fixed income and other securities. 
www.jpmorganfunds.com
 
 
2 
Table of Contents
JPMorgan International Opportunities Plus Fund 
Twelve Months Ended October 31, 2010 (Unaudited) 


Dear Shareholder: 
If 2009 was the year that we tried to sort out the aftermath of the global financial crisis and recession, then 2010 should be viewed as the year we began to slowly emerge from the crisis and embark on the
road to recovery. 
 
Dear Shareholder: 
If 2009 was the year that we tried to sort out the aftermath of the global financial crisis and recession, then 2010 should be viewed as the year we began to slowly emerge from the crisis and embark on the
road to recovery. 
 
Dear Shareholder: 
If 2009 was the year that we tried to sort out the aftermath of the global financial crisis and recession, then 2010 should be viewed as the year we began to slowly emerge from the crisis and embark on the
road to recovery. 
 
