Dear Investor
With several years of gains already
behind us, investors could not have asked much more of the market in 2013.
Stocks recorded stellar gains and marched steadily higher as the year
progressed, with only minor pullbacks in the early summer and fall. Mid-cap
growth stocks fared even better than the market as a whole, though they lost
some momentum relative to large-caps as the year came to an end. The market’s
advance was notably broad, with all sectors in the Russell Midcap Growth Index
recording double-digit gains. Although our focus on buying reasonably valued
growth stocks can often result in our trailing the benchmarks in such rapid
advances, our fund participated fully in the rally and added to its strong
returns of the past several years.
