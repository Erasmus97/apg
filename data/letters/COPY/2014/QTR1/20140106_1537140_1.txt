Dear Fellow Shareholders,

In the following Management Discussion and Analysis, you will find details of performance results and the report of a good year for your fund.

What many of you want is the secret of how to buy stocks that will go higher. Here it is - pick various combinations of either two, three or four letters. Then look at a list of stock symbols, and when one of your combinations matches, buy it. I caution that this system may have worked well in but is not one we would ever recommend, and one that will not work in the future.

The Federal Reserve Board has chosen to continue its policy of quantitative easing longer than most observers imagined. Quantitative easing means that the Federal Reserve buys treasury debt and mortgage securities from banks. The money from these purchases goes into bank reserves that can be lent or kept as reserves at the Federal Reserve, which pays interest on them. The increase in the money supply has gone into financial markets and bank reserves. Little of it has been lent to companies or consumers, and the money thus far is not circulating in the economy.

As a consequence, stocks have had an easy time of it. Since on most days market participants have a very short-term outlook, there is little attention given to the longer term consequences of this policy. Unfortunately, those who have longer term responsibilities think about the consequences. As a result, corporations and consumers (except the wealthy beneficiaries of the explosion in asset prices) have not revived the economy by capital investment and spending to the extent the Federal Reserve expected. The uncertainties weigh heavily on their minds.

Although the policy has not worked, it is in force as of now. When the Fed Chair hinted in May and June that the Federal Reserve might taper their debt purchases, the markets, especially the bond markets, fell. Would they have fallen more if they actually did buy fewer bonds? My point in this discussion is that both the economy and the financial markets are subjects in a novel experiment, the results of which are unknowable.

To the extent that we are able, we try not to rely upon the Federal Reserves largesse. Our aim is still to try to make investments in companies that are compounding returns for the benefit of their shareholders. We continue to look for companies whose share prices we believe contain the right mix of risk and return.

Thanks to all for your continued confidence.

Sincerely,
