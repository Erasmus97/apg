Dear Fellow Shareholders:
 
Do you remember all of the fuss about Y2K? Strange as it seems
now, in 1999 a lot of people thought technology’s inability to cope with the century date change would trigger a global crisis.
But the tech concerns dominating my thoughts then weren’t about computer bugs. Instead, I was preoccupied with growth’s
dominance over value, epitomized by the NASDAQ Composite rising above 5000. In that run, momentum-driven tech stocks reached astounding
valuations. It was their dizzying valuations in the face of often-lacking earnings that had me scratching my head.
 
As it turns out, computers didn’t come crashing down—but
in time, some of the stocks behind them did. Investors who chased their performance got hurt, while fundamentally driven value
investors like us generally outperformed.
 
Now some investors think they see signs that markets are tracing
a similar trajectory. Equity returns were strong across the board in 2013, it’s true. But for the year, growth stocks—led
by biotech and internet companies—soundly outperformed value. This continued a multiyear trend of value trailing growth,
as seen in the chart below.
 
Russell 2000® Growth – Russell 2000® Value
 
