Dear fellow shareholders,
For the fourth quarter, FPA Perennial Fund was up 7.43%. The S&P and the Russell 2500 had strong gains as well of 10.51% and 8.66%, respectively. We are proud of our returns over longer periods with strong absolute and relative performance for 5, 10, and 15 years, as shown on the table below.
Fund/Index
MTD
YTD
1 Year
3 Years**
5 Years**
10 Years**
15 Years**
FPA Perennial
2.11
%
30.46
%
30.46
%
13.00
%
21.18
%
9.03
%
11.27
%
Russell 2500
2.54
%
36.80
%
36.80
%
16.28
%
21.77
%
9.81
%
9.67
%
S&P 500
2.53
%
32.39
%
32.39
%
16.18
%
17.94
%
7.41
%
4.68
%
Standout performers this year include Heartland Express (+50%) and Knight Transportation (+25%) both truckload carriers working to complete attractive acquisitions. Other winners in 2013 fit the description of light industrial or business services, with gains of +35-60% — suppliers of pumps, motors, valves, bar code printers, and industrial distributors.
We focus on owning high-quality businesses, firms with histories of earning high returns on capital with modest debt levels. These companies have leading market shares and top-tier management. They have demonstrable track records of wise allocation of the businesses' cash flow. In other words, they walk the talk.
Our approach to investing sometimes generates unusual looking portfolios. We make no attempt to manage portfolios which mimic the benchmark. We have no fear of tracking error. Our portfolios are always much more concentrated than the benchmark, with much of the weightings the result of owning only a dozen or so names. Five- to ten-year holding periods for individual securities are not unusual, as annual portfolio turnover has been very low, averaging about 15% over the past five years.
We believe the most important contributor to the long-term investment performance of the companies we own is growth in earnings, not changes in valuation (price/earnings ratio). Because growth is driven by earning high returns on capital and successful reinvestment of cash flow, it is necessary to own most of our portfolio securities for an extended period in order for this process to come to fruition.
Beyond the philosophical considerations, there are practical reasons we prefer low portfolio turnover. Trading has identifiable, real, and significant negative costs — "moving the price," broker commissions, and realizing taxable gains. In addition, excessive trading is a distraction from our highest priority focus, understanding the business operations of our portfolio companies.
Minimizing risk, especially in treacherous economic or stock market environments, is an important element of our investment process. We do this by emphasizing companies with relatively unleveraged balance sheets, operating in more predictable, less volatile sectors of the economy, with business models we can understand. Our companies' leading market shares and high operating margins help to reduce the volatility of their earnings. Diversified portfolios and modest relative valuations, typically lower than the market, also contribute to lower risk.
**  Annualized
A redemption fee of 2.00% will be imposed on redemptions within 90 days. Expense ratio calculated as of most recent prospectus is 1.02%. Past performance is no guarantee of future results and current performance may be higher or lower than the performance shown. This data represents past performance and investors should understand that investment returns and principal values fluctuate, so that when you redeem your investment it may be worth more or less than its original cost. Current month-end performance data may be obtained by calling toll-free, 1-800-982-4372.
Continued
A comparison of the FPA Perennial portfolio with the benchmark shows the portfolio companies to be financially stronger and more profitable yet selling at a lower valuation.
FPA Perennial
19.3
%
9.9
%
34
%
21.1
x
Russell 2500
10.1
%
7.9
%
45
%
27.5
x
Source: BNY Mellon
Many commentators have noted the outperformance of quality stocks in recent years, though with widely varying definitions of quality and generally without our 35-year track record.
Last year The Leuthold Group, a financial research firm based in Minneapolis, published a study of stock quality and investment performance for the most recent 30-year period. We found Leuthold's research defined quality in a manner very consistent with the investment strategy we have followed over many years. Leuthold's screens for quality emphasize stable growth of revenue and income, with high operating margins, unleveraged balance sheets, and dividend growth, qualities we have highlighted in shareholder letters over many years.
The performance part of the study provides a response to the question: "what does owning a portfolio of high quality companies actually do for us?" Although Leuthold's back-testing of their quality stock hypothesis should not be a basis for personal investment decisions, it is worth noting that the quality companies identified by Leuthold have tended to outperform in very difficult markets and generally match the market in less exacting environments.
Thank you for your continued support and trust.


