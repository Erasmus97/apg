Dear Fellow Shareholder, 
On behalf of
Transamerica Partners Funds Group and Transamerica Asset Allocation Funds, we would like to thank you for your continued support and confidence in our products as we look forward to continuing to serve you and your financial adviser in the future.
We value the trust you have placed in us. 
This annual report is provided to you with the intent of presenting a comprehensive review of the investments of each of
your funds. The Securities and Exchange Commission requires that annual and semi-annual reports be sent to all shareholders, and we believe this report to be an important part of the investment process. In addition to providing a comprehensive
review, this report also provides a discussion of accounting policies as well as matters presented to shareholders that may have required their vote. 
We believe it
is important to understand market conditions over the last year in order to provide a context for reading this report. As 2013 began, fear of the U.S. fiscal cliff had been holding markets down even as earnings were improving. However, in early
January, Congress resolved the fiscal cliff and as a result U.S. markets moved up strongly throughout the first quarter of 2013. 
Spring brought slower conditions in
Europe, which pushed international equities down. However, Japan’s new quantitative easing program launched in April helped them bounce back. In the U.S., economic conditions continued to improve as summer approached, prompting the Federal
Reserve to announce plans to taper purchases of mortgages and Treasury bonds. As a result, equities and bonds sold off in anticipation of higher rates, pushing yields on the 10-year Treasury from 1.70% in April to 2.98% in September. 
During the third quarter, the Federal Reserve’s message that “tapering is not tightening” seemed to satisfy markets as U.S. equities climbed higher on
improving earnings. The market was able to hold onto those gains despite the government shutdown in October and concerns about a possible default. In December, the Federal Reserve announced the first taper and while U.S. equity markets responded
favorably to the growth implications, bond markets pushed the 10-year Treasury yield further upward to end the year at 3.04%. 
®
In addition to your active involvement in the investment process, we firmly
believe that a financial adviser is a key resource to help you build a complete picture of your current and future financial needs. Financial advisers are familiar with the market’s history, including long-term returns and volatility of various
asset classes. With your financial adviser, you can develop an investment program that incorporates factors such as your goals, your investment timeline, and your risk tolerance. 
Please contact your financial adviser if you have any questions about the contents of this report, and thanks again for the confidence you have placed in us. 
Sincerely, 


