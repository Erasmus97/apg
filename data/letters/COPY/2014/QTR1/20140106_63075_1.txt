Dear Shareholders: 
The global economy is trending toward growth again despite risks created by the U.S. government’s gridlock. The eurozone has emerged from its 18-month-long
recession. 
However, unemployment in the region persists at historically high levels. The U.K. economy is on the rebound. China’s economic gauges
are improving and point toward expansion. And Japan’s aggressive program of monetary easing is showing signs of success. 
The U.S. Federal
Reserve’s expected tapering of its bond-buying stimulus program — telegraphed in the spring and delayed in September — has weighed on global markets. Emerging markets have borne much of the brunt, with currency values dropping and
nervous investors seeking safety elsewhere. The greatest 

threat to global economic recovery now appears to be related to the U.S. government’s impasse. While the tensions surrounding the 16-day government shutdown and potential U.S. debt default
have dissipated, another round of potential gridlock lies ahead early in 2014, with the next U.S. budget and debt ceiling deadlines. 
As always, managing risk in the face of uncertainty remains a top priority for investors. At MFS®, our uniquely collaborative investment
process employs integrated, global research and active risk management. Our global team of investment professionals shares ideas and evaluates opportunities across continents, investment disciplines and asset classes — all with a goal of
building better insights, and ultimately better results, for our clients. 
We are mindful of the many economic challenges investors face, and believe it
is more important than ever to maintain a long-term view and employ time-tested principles, such as asset allocation and diversification. We remain confident that our unique approach can serve investors well as they work with their financial
advisors to identify and pursue the most suitable opportunities. 
Respectfully, 


