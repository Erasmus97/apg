Dear Shareholder:




As it had in the first three quarters of the U.S. stock market rang up healthy returns again during the fourth quarter. The S&amp;P squeezed what is almost a normal annual return into the final three months of the year, swelling its total return for all of to a bit more than Signs that economic strength was building combined with a continuation of easy monetary policies from the Federal Reserve to power stock prices higher. Not even the Feds decision in December to begin cutting back on its bond buying as began slowed the markets momentum. Perhaps the year-end rally was in appreciation of the fact that the economy and Fed monetary policy appear to be starting to normalize; in any event, considering that U.S. stocks were closing in on the fifth anniversary of the market bottom, the strong market action was most impressive.




Foreign stock markets had a strong fourth-quarter showing as well, although for the most part returns were not in the same class as those in U.S. equities. Returns to stocks in the developed markets of the world outside the U.S. were roughly half those in the U.S. during Here too, a sense that the pace of economic activity was improving was instrumental in the year-end rally. In local currency terms, Japan and Germany had outsized returns for the quarter and the year, but U.S. dollar returns were trimmed in the case of Japan by the depreciating yen (which lost against the dollar for all of and enhanced in the case of Germany by the appreciating euro (which gained vis--vis the dollar). Emerging market returns were positive on average during the fourth quarter, but not enough to put this laggard asset category in the black for the year. Perhaps characteristic of trends, Greece was the best performing market in the emerging market class, with returns averaging more than




Yields went higher in the best credits during and were flat to lower in some of the worst. U.S. and German bond yields rose and basis points, respectively. By contrast, U.S. high yield bond rates declined about bps during and yields on peripheral sovereign debt in Europe declined by bps (Italy), by bps (Spain) and, in the extreme case, by more than bps (Greece). This improving confidence that the economic abyss envisioned for Europe in grew less and less likely as progressed was reflected in a sharp narrowing of yield spreads in Europe but more broadly as well.




The U.S. bond market had a small loss in the fourth quarter, as Treasury yields rose, with shorter maturity and lower quality securities generally performing better. Virtually all of the bond markets losses last year occurred in the second quarter when Federal Reserve Chairman Ben Bernanke introduced the idea of tapering bond purchases. But that was enough to produce the first full-year loss for the Barclays U.S. bond market aggregate since In the end, when the Fed finally announced in mid-December that it would begin to trim QE bond buying, bond prices retreated nominally, while stocks raced to new highs as the year ended.




Government shutdown and the approaching debt ceiling affected markets early in the fourth quarter, but a deal was struck to end the shutdown and suspend the debt ceiling. The special Paul Ryan/Patty Murray Congressional committee that was created to craft a budget that both sides could agree on delivered a budget outline that President Obama signed into law in the final week of December. The debt ceiling suspension set to expire in early February may be more problematic, although the October budget deal provides a template of sorts for extending the suspension of the debt ceiling, which was done twice in Given the failed strategy of government shutdown, Wright suspects further debt ceiling suspensions may be the easiest course for Congress until and unless the Congressional elections produce a material shift in the balance of power in Washington. While investors, consumers and businesses cannot ignore Washington politics, the


























Letter to Shareholders (Unaudited)
