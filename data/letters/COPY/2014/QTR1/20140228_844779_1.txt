Dear Shareholder 
Annual Report:
Fund Summary
About Fund Performance
Disclosure of Expenses
Financial Statements:
Schedule of Investments
Statement of Assets and Liabilities
Statement of Operations
Statements of Changes in Net Assets
Financial Highlights
Notes to Financial Statements
Report of Independent Registered Public Accounting Firm
Important Tax Information
Officers and Trustees
Additional Information
A World-Class Mutual Fund Family
 

Risk assets (such as equities) powered higher in 2013, for the most part unscathed by ongoing political and economic uncertainty. While monetary policy was the main driving force behind the rally in risk
assets, it was also the main culprit for the bouts of volatility during the year. 
Equity markets rallied right out of the gate in January with
cash pouring back in from the sidelines after a potential US fiscal crisis (i.e., the “fiscal cliff”) was averted with a last-minute tax deal. Key indicators signaling modest but broad-based improvements in the world’s major economies
and a calming in Europe’s debt troubles fostered an aura of comfort for investors. Global economic momentum slowed in February, however, and the pace of the rally moderated. In the months that followed, US stocks outperformed international
stocks, as the US showed greater stability than most other regions. Slow but positive growth was sufficient to support corporate earnings, while uncomfortably high unemployment reinforced expectations that the Federal Reserve would continue its
aggressive monetary stimulus program. International markets experienced higher levels of volatility given a resurgence of political instability in Italy, a banking crisis in Cyprus and a generally poor outlook for European economies. Emerging
markets significantly lagged the rest of the world as growth in these economies fell short of expectations. 
Financial markets were rattled in May
when Fed Chairman Bernanke mentioned the possibility of reducing — or “tapering” — the central bank’s asset purchase programs — comments that were widely misinterpreted as signaling an end to the Fed’s
zero-interest-rate policy. US Treasury yields rose sharply, triggering a steep sell-off across fixed income markets. (Bond prices move in the opposite direction of yields.) Equity prices also suffered as investors feared the implications of a
potential end of a program that had greatly supported the markets. Risk assets rebounded in late June, however, when the Fed’s tone turned more dovish, and improving economic indicators and better corporate earnings helped extend gains through
most of the summer. 
The fall was a surprisingly positive period for most asset classes as the Fed defied market expectations with its decision to
delay tapering. Easing of political tensions that had earlier surfaced in Egypt and Syria and the re-election of Angela Merkel as Chancellor of Germany also boosted investor sentiment. Higher volatility returned in late September when the US
Treasury Department warned that the national debt would soon breach its statutory maximum. The ensuing political brinksmanship led to a partial government shutdown, roiling global financial markets through the first half of October, but the rally
quickly resumed when politicians engineered a compromise to reopen the government and extend the debt ceiling, at least temporarily. 
The
remainder of the year was generally positive for stock markets in the developed world, although investors continued to grapple with uncertainty about when and how much the Fed would scale back on stimulus. On the one hand, persistent weak growth and
low inflation provided significant latitude for monetary policy decisions and investors were encouraged by dovish comments from Fed Chair-to-be Janet Yellen. On the other hand, US housing and manufacturing reports had begun to signal fundamental
improvement in the economy. The long-awaited taper announcement ultimately came in mid-December. The Fed reduced the amount of its monthly asset purchases, but at the same time, extended its time horizon for maintaining low short-term interest
rates. Markets reacted positively as this move signaled the Fed’s perception of real improvement in the economy and investors felt relief from the tenacious anxiety that had gripped them throughout the year. 
Accommodative monetary policy and the avoidance of major risks made 2013 a strong year for most equity markets. US stocks were the strongest performers for
the six- and 12-month periods ended December 31. In contrast, emerging markets were weighed down by uneven growth and structural imbalances. Rising US Treasury yields led to a rare annual loss in 2013 for Treasury bonds and other high-quality
fixed income sectors including tax-exempt municipals and investment grade corporate bonds. High yield bonds, to the contrary, generated gains driven by income-oriented investors seeking yield in the low-rate environment. Short-term interest rates
remained near zero, keeping yields on money market securities near historical lows. 
www.blackrock.com
Sincerely, 


