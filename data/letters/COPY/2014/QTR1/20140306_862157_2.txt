Dear Shareholder:



Having recently joined Deutsche Asset &amp; Wealth Management as president of the DWS funds and head of Fund Administration, I'd like to take this opportunity to introduce myself. I come with years of experience in asset management and the mutual fund industry. My job is to work closely with your fund board to ensure optimal oversight of the DWS funds' management and operations. I look forward to serving in this role on your behalf.



As for the economy, experts seem to agree that both the U.S. and global economies are recovering. Interest rates, while destined to rise to a level more in line with historical "normal" at some point, will likely remain relatively low for the foreseeable future. The stock markets continue to demonstrate strength as housing rebounds, American manufacturing strengthens, the U.S. budget deficit improves and unemployment continues to move lower. However, uncertainty persists regarding the pace of the recovery, the eventual tapering of government bond purchases, the potential for further political gridlock around the fiscal impasse and lingering effects of the financial crisis. All this uncertainty may well contribute to volatility in both the bond and stock markets.



It may help to remember that market fluctuations are not unusual. However, significant market swings may also reflect behavior that is driven more by investor emotion than any fundamental factors relating to the securities in question. If volatility is making you nervous, it may be time to review your investments. A trusted financial advisor can help you determine if a strategy change is appropriate and identify risk management strategies that serve your specific goals and situation.



Best regards,
