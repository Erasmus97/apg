Dear Fellow Shareholder,




As stewards of our customers savings, the management team and Directors of Davis Global Fund and Davis International Fund recognize the importance of candid, thorough, and regular communication with our shareholders.In our Annual and Semi-Annual Reports we include all of the required quantitative information such as audited financial statements, detailed footnotes, performance reports, fund holdings, and performance attribution.Also included is a list of positions opened and closed.




In addition, we produce a Manager Commentary for each Fund.In this commentary, we give a more qualitative perspective on fund performance, discuss our thoughts on individual holdings, and share our investment outlook.You may obtain a copy of the current Manager Commentary either on the Funds website at www.davisfunds.com or by calling




Co-portfolio manager of Davis Global Fund and Davis International Fund, Stephen Chen, will be departing our firm at the end of this year. On behalf of our colleagues, we thank him for his service and wish him the best. Danton Goei and Tania Pouschine will continue to serve as co-portfolio managers of the Funds.




We thank you for your continued trust.We will do our best to earn it in the years ahead.




Sincerely,
