Dear Fellow Shareholders:



We are pleased to present you with this annual report for Value Line Premier Growth Fund, Inc., The Value Line Fund, Inc., Value Line Income and Growth Fund, Inc., Value Line Larger Companies Fund, Inc. and Value Line Core Bond Fund (individually, a Fund and collectively, the Funds) for the months ended December We are especially excited to present this annual report in its new format, revised to be more informative, more useful and more reader-friendly.



The months ended December were rewarding ones for the equity and hybrid Value Line Funds but challenging ones for the fixed income Value Line Funds, as investors generally focused on the economic recovery despite persistent volatility. At the same time, the annual period was highlighted by several of the Funds being recognized for their long-term performance.











Value Line Premier Growth Fund, Inc. outpaced its peers for the three-, five- and ten-year periods ended December as noted by leading independent mutual fund advisory service Lipper (multi-cap growth category). Lipper also awarded its top Lipper Leader rating of to the Fund for Preservationi versus its peers as of December Additionally, the Fund earned an Overall four-star rating from in the mid-cap growth category among funds as of December based on risk-adjusted returns. Morningstar gave the Fund a Risk rating of Below Average.ii









The Value Line Fund, Inc. was named a Lipper for overall Preservation versus its peers as of December









Value Line Income and Growth Fund, Inc. outpaced its peers for the three-, five- and ten-year periods ended December as noted by Lipper (mixed-asset target allocation moderate category). The Fund also earned an Overall four- star rating from in the moderate allocation category among funds as of December based on risk- adjusted returns.iv The Fund, along with Value Line Premier Growth Fund, Inc., was featured in various national publications for its consistent performance over multiple time periods.






Also a highlight of the annual period was Value Line Core Bond Fund transitioning to a new, more efficient strategy. Value Line Core Bond Fund, having changed its strategy to be a broad-based intermediate-term investment grade bond fund in December enjoyed significantly increased assets with the merger of the Value Line U.S. Government Securities Fund, Inc. into the Fund in March The Fund has already begun to realize the benefits of a larger, more efficiently managed fund, and the investment adviser, EULAV Asset Management (the Adviser) permanently reduced the management fee in February



On the following pages, the Funds portfolio managers discuss the management of their respective Funds over the annual period. The discussions highlight key factors influencing recent performance of the Funds. You will also find a schedule of investments and financial statements for each of the Funds.



Before reviewing the performance of your individual mutual fund investment, we encourage you to take a brief look at the major factors affecting the financial markets over the months ended December especially given the newsworthy events of the year. With the exciting developments and performance results of the Funds during we also invite you to take this time to consider a broader diversification strategy by including additional Value Line Funds, which you can read about on the following pages, in your investment portfolio.



Economic Review



U.S. real Gross Domestic Product (GDP) was lackluster with growth in the first half of at less than in the first and second calendar quarters. The U.S. economy faced strong headwinds, including increases in the payroll tax and disruptions from the sequester budget cuts. Third quarter GDP, however, turned sharply upward, coming in at as boosted by higher consumer spending, increased business investment and rising inventories. Estimates for fourth quarter GDP suggest the U.S. economy may have ended the year with more momentum than had been anticipated.



Despite the growing economy, inflation remained modest. Consumer prices stayed in check, with the Consumer Price Index (CPI) rising just before seasonal adjustment. Limited wage growth and declining energy prices contributed to the relatively benign inflation scenario. The U.S. also saw moderate job growth, as reflected in a drop in unemployment from at the close of to at the close of The makeup of job growth, however, was somewhat disappointing, with hiring generally concentrated in sectors representative of low-wage jobs.



In recognition of the improving U.S. economy, the Federal Reserve (the Fed) had ongoingand well-publicizeddiscussions throughout the year about the possibility of reducing its monthly bond-buying program. Speculation about the timing and magnitude of the tapering had a great impact on both the equity and fixed income markets. Ultimately, Fed Chair Ben Bernanke kept the focus on key market data as the basis for the decision on tapering. As unemployment dropped close to the Feds stated target of the Fed finally announced in December that it would modestly reduce its monthly bond purchasesfrom billion to billionbeginning in January At the same time, the Fed reaffirmed its commitment to maintaining low short-term interest rates, with the targeted federal funds rate not likely to exceed At the end of the annual period, the appointment of Janet Yellen as new Fed Chair was seen by the financial markets as likely to not steer the Fed too far off the course set by Ben Bernanke.










































(continued)






Equity Market Review



U.S. equities, as measured by the S&amp;P posted robust double-digit gains for supported by a significantly stronger real estate market, steady growth in manufacturing and a modest drop in the national unemployment rate.



Stocks began the year strong upon the announcement of a partial bi-partisan deal regarding the federal budget, debt ceiling and government shutdownwhich drove a generally steady climb through May The S&amp;P Index subsequently dropped between the end of May and the end of June, as fears of over-bought conditions, an imminent end to the quantitative easing program by the Fed and worries over second quarter corporate earnings arose. The U.S. equity market snapped back to post solid gains after reasonably good earnings reports and what were perceived as dovish words by Fed members that eased investors concerns. A notable acceleration in market appreciation occurred in early October in response to the Feds surprise announcement in September that it would not yet begin tapering its asset purchases. This announcement combined with improving employment reports to drive the S&amp;P Index higher between early October and the end of December. Also boosting the U.S. equity markets gain at the end of the annual period was the Feds announcement, ending seven months of speculation, that it would finally but gradually begin to taper its asset purchases in January A particularly notable catalyst for the U.S. equity market during the annual period was the expansion of the price/earnings multiple investors were willing to pay, as the price/earnings multiple of the S&amp;P Index expanded from to by the end of December The S&amp;P Index posted new all-time closing highs in including a new closing high on the final day of trading. The last time the Index closed the year with a new high was in



All ten sectors of the S&amp;P Index posted positive double-digit absolute performance for the year, with the consumer discretionary, health care and industrials sectors leading the way. Telecommunication services and utilities, both traditionally considered defensive sectors, were the weakest sectors during the annual period.



Fixed Income Market Review



The broad U.S. fixed income market, as measured by the Barclays U.S. Aggregate Bond posted negative returns during the annual period. The U.S. fixed income market faced several headwinds, including speculation of the Fed tapering its bond-buying program and Congressional discord resulting in protracted budget disputes and a partial U.S. government shutdown for days. Economic indicators, while mixed, were generally improving during the year.



Against this backdrop, interest rates rose across the spectrum of maturities, but most dramatically in the intermediate segment of the yield curve. The rise in interest rates helped propel investor demand for investment grade and high yield corporate bonds at the expense of owning U.S. Treasuries. In turn, spread, or non-U.S. Treasury, sectors of the U.S. fixed income market were the best performers in the Barclays U.S. Aggregate Bond Index during the annual period. Generally speaking, lower quality bonds outpaced higher quality bonds, as investors sought yield amidst the underlying support of a growing economy. U.S. Treasuries posted negative returns overall.



* * *



We thank you for trusting us to be a part of your long-term, comprehensive investment strategy. We appreciate your confidence in the Value Line Funds and look forward to serving your investment needs in the years ahead just as we have been helping to secure generations financial futures for more than yearsbased on solid fundamentals, sound investment principles and the power of disciplined and rigorous analytics. If you have any questions or would like additional information on these or other Value Line Funds, we invite you to contact your investment representative or visit us at www.vlfunds.com.







Sincerely,
