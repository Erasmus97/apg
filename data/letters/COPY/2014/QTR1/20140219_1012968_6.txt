Dear Investor
Stocks in most developed markets rose
strongly, with some broad U.S. indexes reaching all-time highs. The U.S. and
Japan were among the best-performing stock markets in the world, while stocks in
emerging markets struggled. Large-cap growth shares fully participated in the
rally, edging out large-cap value for the year. An expansion in price/earnings
multiples drove stocks higher, as the S&amp;P 500 Index’s 32% return greatly
outpaced mid-single-digit earnings per share growth. Many themes central to our
thinking over the last several years were embraced by the market in 2013,
enhancing fund results.
