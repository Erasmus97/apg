Dear Shareholders, 
A market rally led by equities 
Despite threats of military action
in Syria, rumblings from Iran and an impending showdown over the debt ceiling here at home, the U.S. financial markets delivered positive results in the third quarter of 2013. Stocks outperformed bonds by a substantial margin. Still, robust growth
continued to elude the U.S. economy, which merely plodded along. New job growth was solid but not spectacular. Consumer spending remained steady, but the only obvious beneficiary was the auto industry. Gains in the housing market met some headwinds,
as mortgage rates rose and sales slipped somewhat. Nevertheless, the recovery in housing remained intact. 
The Federal Reserve
(the Fed) unsettled investors with a hint that it was ready to taper its purchase of U.S. Treasury and mortgage securities. However, its failure to take any action in a September meeting rallied stocks to new highs and brought bonds back into
positive territory for the quarter. Small-cap stocks were the U.S. market leaders. Growth outperformed value in a quarter dominated by the materials, industrials and consumer discretionary sectors. Developed markets led the global rally, driven by
strengthening economic conditions in the eurozone. Certain emerging stock markets, including China’s, bounced back with returns in line with those of the United States. India and Indonesia were exceptions to that trend, as fears of the
Fed’s tapering efforts rattled investors in both countries. 
Improved risk appetites boosted fixed income 
Following a weak second quarter, the fixed-income markets made up some ground in the third quarter of 2013. As yields fell, bond prices rallied enough to
push returns on non-Treasury sectors into positive territory. Risk appetites improved in response to continued liquidity from the Fed. Against this backdrop, U.S. high-yield and foreign bonds led the fixed-income markets, along with mortgage-backed
securities and emerging market bonds. The U.S. municipal bond market was the exception, as it slipped into negative territory in the final week of the period, pressured by heightened concerns over Puerto Rico, potential municipal bankruptcies and
continued fund redemptions. 
Stay on track with Columbia Management 
Backed by more than 100 years of experience, Columbia Management is one of the nation’s largest asset managers. At the heart of our success and, most importantly, that of our investors, are highly
talented industry professionals, brought together by a unique way of working. At Columbia Management, reaching our performance goals matters, and how we reach them matters just as much. 
Visit columbiamanagement.com for: 
 
 
Detailed up-to-date fund performance and portfolio information 
 
Quarterly fund commentaries 
 
Thank you for your continued support of the Columbia Funds. We look forward to serving your investment needs for many years to come. 
Best Regards, 


Dear Shareholders, 
A market rally led by equities 
Despite threats of military action
in Syria, rumblings from Iran and an impending showdown over the debt ceiling here at home, the U.S. financial markets delivered positive results in the third quarter of 2013. Stocks outperformed bonds by a substantial margin. Still, robust growth
continued to elude the U.S. economy, which merely plodded along. New job growth was solid but not spectacular. Consumer spending remained steady, but the only obvious beneficiary was the auto industry. Gains in the housing market met some headwinds,
as mortgage rates rose and sales slipped somewhat. Nevertheless, the recovery in housing remained intact. 
The Federal Reserve
(the Fed) unsettled investors with a hint that it was ready to taper its purchase of U.S. Treasury and mortgage securities. However, its failure to take any action in a September meeting rallied stocks to new highs and brought bonds back into
positive territory for the quarter. Small-cap stocks were the U.S. market leaders. Growth outperformed value in a quarter dominated by the materials, industrials and consumer discretionary sectors. Developed markets led the global rally, driven by
strengthening economic conditions in the eurozone. Certain emerging stock markets, including China’s, bounced back with returns in line with those of the United States. India and Indonesia were exceptions to that trend, as fears of the
Fed’s tapering efforts rattled investors in both countries. 
Improved risk appetites boosted fixed income 
Following a weak second quarter, the fixed-income markets made up some ground in the third quarter of 2013. As yields fell, bond prices rallied enough to
push returns on non-Treasury sectors into positive territory. Risk appetites improved in response to continued liquidity from the Fed. Against this backdrop, U.S. high-yield and foreign bonds led the fixed-income markets, along with mortgage-backed
securities and emerging market bonds. The U.S. municipal bond market was the exception, as it slipped into negative territory in the final week of the period, pressured by heightened concerns over Puerto Rico, potential municipal bankruptcies and
continued fund redemptions. 
Stay on track with Columbia Management 
Backed by more than 100 years of experience, Columbia Management is one of the nation’s largest asset managers. At the heart of our success and, most importantly, that of our investors, are highly
talented industry professionals, brought together by a unique way of working. At Columbia Management, reaching our performance goals matters, and how we reach them matters just as much. 
Visit columbiamanagement.com for: 
 
 
Detailed up-to-date fund performance and portfolio information 
 
Quarterly fund commentaries 
 
Thank you for your continued support of the Columbia Funds. We look forward to serving your investment needs for many years to come. 
Best Regards, 


Dear Shareholders, 
A market rally led by equities 
Despite threats of military action
in Syria, rumblings from Iran and an impending showdown over the debt ceiling here at home, the U.S. financial markets delivered positive results in the third quarter of 2013. Stocks outperformed bonds by a substantial margin. Still, robust growth
continued to elude the U.S. economy, which merely plodded along. New job growth was solid but not spectacular. Consumer spending remained steady, but the only obvious beneficiary was the auto industry. Gains in the housing market met some headwinds,
as mortgage rates rose and sales slipped somewhat. Nevertheless, the recovery in housing remained intact. 
The Federal Reserve
(the Fed) unsettled investors with a hint that it was ready to taper its purchase of U.S. Treasury and mortgage securities. However, its failure to take any action in a September meeting rallied stocks to new highs and brought bonds back into
positive territory for the quarter. Small-cap stocks were the U.S. market leaders. Growth outperformed value in a quarter dominated by the materials, industrials and consumer discretionary sectors. Developed markets led the global rally, driven by
strengthening economic conditions in the eurozone. Certain emerging stock markets, including China’s, bounced back with returns in line with those of the United States. India and Indonesia were exceptions to that trend, as fears of the
Fed’s tapering efforts rattled investors in both countries. 
Improved risk appetites boosted fixed income 
Following a weak second quarter, the fixed-income markets made up some ground in the third quarter of 2013. As yields fell, bond prices rallied enough to
push returns on non-Treasury sectors into positive territory. Risk appetites improved in response to continued liquidity from the Fed. Against this backdrop, U.S. high-yield and foreign bonds led the fixed-income markets, along with mortgage-backed
securities and emerging market bonds. The U.S. municipal bond market was the exception, as it slipped into negative territory in the final week of the period, pressured by heightened concerns over Puerto Rico, potential municipal bankruptcies and
continued fund redemptions. 
Stay on track with Columbia Management 
Backed by more than 100 years of experience, Columbia Management is one of the nation’s largest asset managers. At the heart of our success and, most importantly, that of our investors, are highly
talented industry professionals, brought together by a unique way of working. At Columbia Management, reaching our performance goals matters, and how we reach them matters just as much. 
Visit columbiamanagement.com for: 
 
 
Detailed up-to-date fund performance and portfolio information 
 
Quarterly fund commentaries 
 
Thank you for your continued support of the Columbia Funds. We look forward to serving your investment needs for many years to come. 
Best Regards, 


Dear Shareholders, 
A market rally led by equities 
Despite threats of military action
in Syria, rumblings from Iran and an impending showdown over the debt ceiling here at home, the U.S. financial markets delivered positive results in the third quarter of 2013. Stocks outperformed bonds by a substantial margin. Still, robust growth
continued to elude the U.S. economy, which merely plodded along. New job growth was solid but not spectacular. Consumer spending remained steady, but the only obvious beneficiary was the auto industry. Gains in the housing market met some headwinds,
as mortgage rates rose and sales slipped somewhat. Nevertheless, the recovery in housing remained intact. 
The Federal Reserve
(the Fed) unsettled investors with a hint that it was ready to taper its purchase of U.S. Treasury and mortgage securities. However, its failure to take any action in a September meeting rallied stocks to new highs and brought bonds back into
positive territory for the quarter. Small-cap stocks were the U.S. market leaders. Growth outperformed value in a quarter dominated by the materials, industrials and consumer discretionary sectors. Developed markets led the global rally, driven by
strengthening economic conditions in the eurozone. Certain emerging stock markets, including China’s, bounced back with returns in line with those of the United States. India and Indonesia were exceptions to that trend, as fears of the
Fed’s tapering efforts rattled investors in both countries. 
Improved risk appetites boosted fixed income 
Following a weak second quarter, the fixed-income markets made up some ground in the third quarter of 2013. As yields fell, bond prices rallied enough to
push returns on non-Treasury sectors into positive territory. Risk appetites improved in response to continued liquidity from the Fed. Against this backdrop, U.S. high-yield and foreign bonds led the fixed-income markets, along with mortgage-backed
securities and emerging market bonds. The U.S. municipal bond market was the exception, as it slipped into negative territory in the final week of the period, pressured by heightened concerns over Puerto Rico, potential municipal bankruptcies and
continued fund redemptions. 
Stay on track with Columbia Management 
Backed by more than 100 years of experience, Columbia Management is one of the nation’s largest asset managers. At the heart of our success and, most importantly, that of our investors, are highly
talented industry professionals, brought together by a unique way of working. At Columbia Management, reaching our performance goals matters, and how we reach them matters just as much. 
Visit columbiamanagement.com for: 
 
 
Detailed up-to-date fund performance and portfolio information 
 
Quarterly fund commentaries 
 
Thank you for your continued support of the Columbia Funds. We look forward to serving your investment needs for many years to come. 
Best Regards, 


Dear Shareholders, 
A market rally led by equities 
Despite threats of military action
in Syria, rumblings from Iran and an impending showdown over the debt ceiling here at home, the U.S. financial markets delivered positive results in the third quarter of 2013. Stocks outperformed bonds by a substantial margin. Still, robust growth
continued to elude the U.S. economy, which merely plodded along. New job growth was solid but not spectacular. Consumer spending remained steady, but the only obvious beneficiary was the auto industry. Gains in the housing market met some headwinds,
as mortgage rates rose and sales slipped somewhat. Nevertheless, the recovery in housing remained intact. 
The Federal Reserve
(the Fed) unsettled investors with a hint that it was ready to taper its purchase of U.S. Treasury and mortgage securities. However, its failure to take any action in a September meeting rallied stocks to new highs and brought bonds back into
positive territory for the quarter. Small-cap stocks were the U.S. market leaders. Growth outperformed value in a quarter dominated by the materials, industrials and consumer discretionary sectors. Developed markets led the global rally, driven by
strengthening economic conditions in the eurozone. Certain emerging stock markets, including China’s, bounced back with returns in line with those of the United States. India and Indonesia were exceptions to that trend, as fears of the
Fed’s tapering efforts rattled investors in both countries. 
Improved risk appetites boosted fixed income 
Following a weak second quarter, the fixed-income markets made up some ground in the third quarter of 2013. As yields fell, bond prices rallied enough to
push returns on non-Treasury sectors into positive territory. Risk appetites improved in response to continued liquidity from the Fed. Against this backdrop, U.S. high-yield and foreign bonds led the fixed-income markets, along with mortgage-backed
securities and emerging market bonds. The U.S. municipal bond market was the exception, as it slipped into negative territory in the final week of the period, pressured by heightened concerns over Puerto Rico, potential municipal bankruptcies and
continued fund redemptions. 
Stay on track with Columbia Management 
Backed by more than 100 years of experience, Columbia Management is one of the nation’s largest asset managers. At the heart of our success and, most importantly, that of our investors, are highly
talented industry professionals, brought together by a unique way of working. At Columbia Management, reaching our performance goals matters, and how we reach them matters just as much. 
Visit columbiamanagement.com for: 
 
 
Detailed up-to-date fund performance and portfolio information 
 
Quarterly fund commentaries 
 
Thank you for your continued support of the Columbia Funds. We look forward to serving your investment needs for many years to come. 
Best Regards, 


Dear Shareholders, 
A market rally led by equities 
Despite threats of military action
in Syria, rumblings from Iran and an impending showdown over the debt ceiling here at home, the U.S. financial markets delivered positive results in the third quarter of 2013. Stocks outperformed bonds by a substantial margin. Still, robust growth
continued to elude the U.S. economy, which merely plodded along. New job growth was solid but not spectacular. Consumer spending remained steady, but the only obvious beneficiary was the auto industry. Gains in the housing market met some headwinds,
as mortgage rates rose and sales slipped somewhat. Nevertheless, the recovery in housing remained intact. 
The Federal Reserve
(the Fed) unsettled investors with a hint that it was ready to taper its purchase of U.S. Treasury and mortgage securities. However, its failure to take any action in a September meeting rallied stocks to new highs and brought bonds back into
positive territory for the quarter. Small-cap stocks were the U.S. market leaders. Growth outperformed value in a quarter dominated by the materials, industrials and consumer discretionary sectors. Developed markets led the global rally, driven by
strengthening economic conditions in the eurozone. Certain emerging stock markets, including China’s, bounced back with returns in line with those of the United States. India and Indonesia were exceptions to that trend, as fears of the
Fed’s tapering efforts rattled investors in both countries. 
Improved risk appetites boosted fixed income 
Following a weak second quarter, the fixed-income markets made up some ground in the third quarter of 2013. As yields fell, bond prices rallied enough to
push returns on non-Treasury sectors into positive territory. Risk appetites improved in response to continued liquidity from the Fed. Against this backdrop, U.S. high-yield and foreign bonds led the fixed-income markets, along with mortgage-backed
securities and emerging market bonds. The U.S. municipal bond market was the exception, as it slipped into negative territory in the final week of the period, pressured by heightened concerns over Puerto Rico, potential municipal bankruptcies and
continued fund redemptions. 
Stay on track with Columbia Management 
Backed by more than 100 years of experience, Columbia Management is one of the nation’s largest asset managers. At the heart of our success and, most importantly, that of our investors, are highly
talented industry professionals, brought together by a unique way of working. At Columbia Management, reaching our performance goals matters, and how we reach them matters just as much. 
Visit columbiamanagement.com for: 
 
 
Detailed up-to-date fund performance and portfolio information 
 
Quarterly fund commentaries 
 
Thank you for your continued support of the Columbia Funds. We look forward to serving your investment needs for many years to come. 
Best Regards, 


Dear Shareholders, 
A market rally led by equities 
Despite threats of military action
in Syria, rumblings from Iran and an impending showdown over the debt ceiling here at home, the U.S. financial markets delivered positive results in the third quarter of 2013. Stocks outperformed bonds by a substantial margin. Still, robust growth
continued to elude the U.S. economy, which merely plodded along. New job growth was solid but not spectacular. Consumer spending remained steady, but the only obvious beneficiary was the auto industry. Gains in the housing market met some headwinds,
as mortgage rates rose and sales slipped somewhat. Nevertheless, the recovery in housing remained intact. 
The Federal Reserve
(the Fed) unsettled investors with a hint that it was ready to taper its purchase of U.S. Treasury and mortgage securities. However, its failure to take any action in a September meeting rallied stocks to new highs and brought bonds back into
positive territory for the quarter. Small-cap stocks were the U.S. market leaders. Growth outperformed value in a quarter dominated by the materials, industrials and consumer discretionary sectors. Developed markets led the global rally, driven by
strengthening economic conditions in the eurozone. Certain emerging stock markets, including China’s, bounced back with returns in line with those of the United States. India and Indonesia were exceptions to that trend, as fears of the
Fed’s tapering efforts rattled investors in both countries. 
Improved risk appetites boosted fixed income 
Following a weak second quarter, the fixed-income markets made up some ground in the third quarter of 2013. As yields fell, bond prices rallied enough to
push returns on non-Treasury sectors into positive territory. Risk appetites improved in response to continued liquidity from the Fed. Against this backdrop, U.S. high-yield and foreign bonds led the fixed-income markets, along with mortgage-backed
securities and emerging market bonds. The U.S. municipal bond market was the exception, as it slipped into negative territory in the final week of the period, pressured by heightened concerns over Puerto Rico, potential municipal bankruptcies and
continued fund redemptions. 
Stay on track with Columbia Management 
Backed by more than 100 years of experience, Columbia Management is one of the nation’s largest asset managers. At the heart of our success and, most importantly, that of our investors, are highly
talented industry professionals, brought together by a unique way of working. At Columbia Management, reaching our performance goals matters, and how we reach them matters just as much. 
Visit columbiamanagement.com for: 
 
 
Detailed up-to-date fund performance and portfolio information 
 
Quarterly fund commentaries 
 
Thank you for your continued support of the Columbia Funds. We look forward to serving your investment needs for many years to come. 
Best Regards, 


