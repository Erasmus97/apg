Dear Shareholders, 
After
significant growth in 2013, domestic and international equity markets have been less compelling during the first part of 2014. Concerns about deflation, political uncertainty in many places and the potential for more fragile economies to impact
other countries have produced uncertainty in the markets. 
Europe is beginning to emerge slowly from the recession in mid-2013, with improved GDP and employment
trends in some countries. However, Japan’s deflationary headwinds have resurfaced; and China shows signs of slowing from credit distress combined with declines in manufacturing and exports. Most recently, tensions between Russia and Ukraine may
continue to hold back stocks and support government bonds in the near term. 
Despite these headwinds, there are some encouraging signs of forward momentum in the
markets. In the U.S., the news is more positive with financial risks slowly receding, positive GDP trends, downward trending unemployment and stronger household finances and corporate spending. 
It is in such changeable markets that professional investment management is most important. Investment teams who have experienced challenging markets in the past
understand how their asset class can behave in rapidly changing times. Remaining committed to their investment disciplines during these times is a critical component to achieving long-term success. In fact, many strong investment track records are
established during challenging periods because experienced investment teams understand that volatile markets place a premium on companies and investment ideas that can weather the short-term volatility. By maintaining appropriate time horizons,
diversification and relying on practiced investment teams, we believe that investors can achieve their long-term investment objectives. 
As always, I encourage you
to communicate with your financial consultant if you have any questions about your investment in a Nuveen Fund. On behalf of the other members of the Nuveen Fund Board, we look forward to continuing to earn your trust in the months and years ahead.

Sincerely, 


