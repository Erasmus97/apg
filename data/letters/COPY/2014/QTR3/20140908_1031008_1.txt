Dear Azzad Funds Shareholder,
Enclosed is your copy of the 2014 Azzad Funds Annual Report. In it you will find a review of the performance of your investments for the past 12 months.
The Great Recession officially ended five years ago, and the subsequent recovery has been one of weak growth, subject to fits and starts and characterized by slow job creation, low aggregate demand and ubiquitous uncertainty. The tenuous nature of this recovery was underscored by the US economy’s 2.9% contraction in the first quarter of 2014.
The US economy, however, has now recovered all of the nearly nine million jobs lost during the recession. This is significant because the American consumer is the single biggest driver of the US economy, accounting for two-thirds of all economic activity. Improving employment prospects with higher wages could provide a solid boost to consumer spending. To date, however, wage growth has barely kept up with a low level of inflation. And there are now an additional two million people seeking jobs since the Great Recession began thanks to a growing workforce. Employment and wage growth remain key determinants for how soon the Federal Reserve’s monetary policy can shift from economic stimulus mode to a more normal state going forward.
Thank you for your continued trust and investment in the Azzad Funds.
Sincerely,


