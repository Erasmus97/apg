Dear Shareowner,

As we move past the midway point of U.S. economic growth is still expected
to exceed for the full calendar year, despite the fact that U.S. gross
domestic product (GDP) for the first quarter was revised down to in May.
The markets, in fact, almost completely ignored that bad news, as more timely
data -- especially labor market data -- pointed to continuing economic strength.
By mid-year, the number of people filing initial unemployment claims and the
number of job openings were at levels last seen during the boom years of
through and unemployment was down to just over Barring an external
shock, we think it's likely that the domestic economic expansion will continue.

A modestly improving European economy and continuing economic improvement in
Japan appear likely to result in improving global economic growth in the second
half of further supporting the U.S. economy. Some slack remains in the
labor markets and capacity utilization, which offers the potential for
continuing non-inflationary growth.

The Federal Reserve System (the Fed) is widely expected to end its QE
(quantitative easing) program by the end of this year, and to begin raising the
Federal funds rate in The timing and pace of Fed's actions remain
uncertain: Fed Chair Janet Yellen has continually stressed that Fed policy will
be sensitive to incoming economic data.

While the U.S. economy appears robust and the global economy seems to be
improving, there are still weaknesses and risks to the economic outlook. Risks
of an adverse shock also remain. Military conflicts and political tensions are
widespread, with particular concerns about recent developments in the Middle
East that could prove disruptive to the global oil supply. While most of the
widely recognized risks may already be "priced into" the market, we caution
against complacency and believe investors should continue to expect market
volatility.

At Pioneer, we have long advocated the benefits of staying diversified and
investing for the long term. And while diversification does not assure a profit
or protect against loss in a declining market, we believe there are still
opportunities for prudent investors to earn attractive returns. Our advice, as
always, is to work closely with a trusted financial advisor to discuss your
goals and work together to develop an investment strategy that meets your
individual needs, keeping in mind that there is no single best strategy that
works for every investor.

Pioneer Oak Ridge Large Cap Growth Fund | Semiannual Report |


Pioneer's investment teams have, since sought out attractive opportunities
in equity and bond markets, using in-depth research to identify undervalued
individual securities, and using thoughtful risk management to construct
portfolios which seek to balance potential risks and rewards in an ever-changing
world.

We encourage you to learn more about Pioneer and our time-tested approach to
investing by consulting with your financial advisor or visiting us online at
us.pioneerinvestments.com. We greatly appreciate your trust in us, and we thank
you for investing with Pioneer.

Sincerely,

Dear Shareowner,

As we move past the midway point of U.S. economic growth is still expected
to exceed for the full calendar year, despite the fact that U.S. gross
domestic product (GDP) for the first quarter was revised down to in May.
The markets, in fact, almost completely ignored that bad news, as more timely
data -- especially labor market data -- pointed to continuing economic strength.
By mid-year, the number of people filing initial unemployment claims and the
number of job openings were at levels last seen during the boom years of
through and unemployment was down to just over Barring an external
shock, we think it's likely that the domestic economic expansion will continue.

A modestly improving European economy and continuing economic improvement in
Japan appear likely to result in improving global economic growth in the second
half of further supporting the U.S. economy. Some slack remains in the
labor markets and capacity utilization, which offers the potential for
continuing non-inflationary growth.

The Federal Reserve System (the Fed) is widely expected to end its QE
(quantitative easing) program by the end of this year, and to begin raising the
Federal funds rate in The timing and pace of Fed's actions remain
uncertain: Fed Chair Janet Yellen has continually stressed that Fed policy will
be sensitive to incoming economic data.

While the U.S. economy appears robust and the global economy seems to be
improving, there are still weaknesses and risks to the economic outlook. Risks
of an adverse shock also remain. Military conflicts and political tensions are
widespread, with particular concerns about recent developments in the Middle
East that could prove disruptive to the global oil supply. While most of the
widely recognized risks may already be "priced into" the market, we caution
against complacency and believe investors should continue to expect market
volatility.

At Pioneer, we have long advocated the benefits of staying diversified and
investing for the long term. And while diversification does not assure a profit
or protect against loss in a declining market, we believe there are still
opportunities for prudent investors to earn attractive returns. Our advice, as
always, is to work closely with a trusted financial advisor to discuss your
goals

Pioneer Oak Ridge Small Cap Growth Fund | Semiannual Report |


and work together to develop an investment strategy that meets your individual
needs, keeping in mind that there is no single best strategy that works for
every investor.

Pioneer's investment teams have, since sought out attractive opportunities
in equity and bond markets, using in-depth research to identify undervalued
individual securities, and using thoughtful risk management to construct
portfolios which seek to balance potential risks and rewards in an ever-changing
world.

We encourage you to learn more about Pioneer and our time-tested approach to
investing by consulting with your financial advisor or visiting us online at
us.pioneerinvestments.com. We greatly appreciate your trust in us, and we thank
you for investing with Pioneer.

Sincerely,
