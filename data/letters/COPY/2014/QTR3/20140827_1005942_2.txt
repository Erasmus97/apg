Dear Fellow Shareholder:
After pulling back at the start of the year, equity markets resumed their steady advance through the halfway mark of 2014. In June, the S&P 500 Index posted its fifth consecutive month of gains. The bond market has also generated solid positive results across many sectors this year.
Underpinning these stable returns is growing evidence of an economy on the mend, including a strengthening jobs market, and accelerating consumer spending and business investment. Corporations are participating in this improvement, as reflected in stronger-than-expected earnings growth and heightened merger-and-acquisition activity.
One notable characteristic of this market environment is its historically low level of volatility — a relative calm that experience indicates does not last forever. As we move into the second half of the year, we also note that a range of geopolitical uncertainties could push risk levels higher.
We believe the market advance may offer an opportune moment for you to meet with your financial advisor, review your portfolio, and make informed and objective decisions about your financial goals.
As always, thank you for investing with Putnam.
Respectfully yours,


