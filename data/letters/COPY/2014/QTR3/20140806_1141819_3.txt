Dear Shareholders:


Shareholders



May


The Barrett Growth Fund (the Fund) gained for the fiscal year ended May a relative basis, the Fund trailed the S&amp;P Index, which posted a total return of and the Lipper Large-Cap Growth Fund Index which advanced over the same period.The Dow Jones Industrial Average brought up the rear with a price gain of







The Year in Review







The U.S. stock market broke through into new all-time levels over the past twelve months.Extremely low interest rates along with higher profits and dividends have been the magic formula propelling stocks over the past several years.If there is one factor, in our opinion, that continues to push stocks up it is the historically low level of interest rates.After many years of earning near zero returns on savings, individuals have been lured into the stock market where dividend yields average some and are likely to continue upward as companies continue to increase their dividends.As we mentioned in last years letter, for the first time since individual investors are putting more money into stock mutual funds than withdrawing it.At the same time that demand for stocks is increasing, large companies, in particular, continue to shrink the supply with stock repurchases.







There was also a period of greater speculation in selected areas of the stock market during the past year.While large, established companies have been very actively buying back stock, small startup companies, many of which are border line profitable, have gone public by offering shares for the first time.These faster growing smaller companies have attracted great interest by investors, in many cases leading to stock market valuations that stretch an investors imagination.Many large technology companies have also bought privately owned technology startup companies at prices that represent an unhealthy degree of optimism.As a result, valuations in selected sectors such as small retailers and restaurants as well as several internet related businesses became more reflective of market tops rather than opportunities by the end of the start of the New Year, the majority of these high flyers have experienced a significant correction in price with many declining over percent.







Corporate profit growth started to slow over the past year.One explanation for the slowing growth has been that the U.S. economic recovery from the recession of is the slowest since World War II.There have been many factors contributing to the slow recovery including weak employment markets, low wage growth, and tight lending restrictions in large financial institutions.The environment in Europe and Japan has been even more muted.And, the fastest growing major economy of all, China, is experiencing slower growth.In addition to the sluggish global economy, corporations are finding it more difficult to squeeze more and more costs out of their organizations.In fact, there has been a recent pickup in larger mergers and acquisitions, which is frequently a sign that companies need to combine operations in order to find additional savings.




































BARRETT






GROWTH FUND








Investment Outlook







We think there are four major factors that are having the most influence in the direction of the stock market.These factors include the global economic growth rate, interest rate levels and direction, the valuation of the market, and investor psychology.At the end of the calendar year it was our best judgment that two of these factors remained encouragingpositive, albeit sluggish, economic growth and low interest rates.In contrast to the positive factors, valuations and investor psychology were approaching levels that would normally coincide with a market approaching a top.As we referenced above, unbridled investor enthusiasm has changed as reflected in the sizable declines of many speculative stocks that led the advance last year.In the past three months, for example, the S&amp;P Index has advanced in price, whereas the Russell Index of smaller companies has declined in price.And, this decline is hardly representative of many plus declines in former high flyers.We view these declines as a healthy correction for the broader market.









Most economists and even Federal Reserve board members are suggesting that todays low interest rates will stay low well into next year, if not into that proves to be the case, the interest rate prop will remain an important tailwind for the stock market.Forecasts of economic growth by the same group of economists and central bankers have proven to be too optimistic recently as well as over the past few years.We continue to see a global economy recovery that is positive but disappointing to the bullish forecasters.Also of great importance to the markets direction are decisions made at the micro level where corporations, many flush with record cash, are going to have to decide whether to pay people more or continue to pay shareholders more.So far, corporations have focused more on shareholders, but the tide may be turning as


















Top Ten Holdings (Percent of Net Assets)*



Sector Weightings (Percent of Total Investments)*





Walt Disney Co.




%






Schlumberger Ltd.




%





Co.




%





Visa, Inc.




%





Berkshire Hathaway, Inc.




%





Mead Johnson Nutrition Co.




%





Automatic Data










Processing, Inc.




%





Costco Wholesale Corp.




%





AmerisourceBergen Corp.




%





Ecolab, Inc.




%





































































*Portfolio characteristics are as of May and are subject to change at any time.

































BARRETT






GROWTH FUND








more employees, particularly those earning minimum wages are more vocal in demanding higher compensation.







At the beginning of this calendar year, we became progressively more concerned about the speculation in selected market segments and the high valuations placed on some sectors.Much of that excess has abated, which we view as an important correction which should enable the broader market to work higher.We continue to see positive gains in the market, but less than the gains of the prior two years, as price gains more closely track slowing corporate profit growth.







The Portfolio







We have been quite pleased with the operating performance of the companies in the Fund.The Fund invests in companies of high quality as defined by very competitive market positions, high levels of profitability, multiple opportunities for growth, and shareholder driven managements.Over the past twelve months, such industry leaders as Schlumberger, Company, Walt Disney, Google, and Tiffany have contributed positively to the Funds performance.In addition to these well-known names, many less familiar companies such as Ecolab, AmerisourceBergen, Intuit, and Devon have been strong performers.We also have helped shareholder returns with the Funds position in Facebook, which was the single best performer during the period.The companies in the Fund, on average, bring cents of every dollar in revenue to the bottom line, and over the past three years have grown revenues by in a sluggish economy.The Funds investments in a few smaller companies, such as Atwood Oceanics, Verisk Analytics, Donaldson Company and Tetra Tech detracted from performance.A few of the larger companies, such as Costco, Accenture and JP Morgan also were drags on performance.







Over the past year, we have added several companies to the Fund that we expect to be important contributors going forward.Similar to our overall strategy of diversifying across industries and sectors, these companies operate in many different areas.For example, Gilead Sciences is becoming a leader in treating hepatitis C; Blackstone is a leader in private equity investing including significant real estate investments; and TJX Companies is growing off-price retailing in Europe.







We are enthusiastic about the prospects of the companies in the Fund.We expect most of the holdings in the Fund to increase their dividends at healthy rates going forward, materially in excess of inflation.We believe that many of the companies in the Fund will use the excess cash on their balance sheets to continue to repurchase their own stock.But most importantly, we believe the companies in the Fund are growing their operating businesses in new markets and have many years of growth ahead of them.



































BARRETT




GROWTH FUND









Thank you for your continued interest in the Fund.












Robert J. Milnamow


E. Wells Beck, CFA




Lead Portfolio Manager


Portfolio Manager
























Past performance is not a guarantee of future results.







The outlook, views, and opinions presented are those of the Adviser as of May are not intended to be a forecast of future events, a guarantee of future results, or investment advice.







Must be preceded or accompanied by a prospectus.







Mutual fund investing involves risk.Principal loss is possible. Foreign investments are subject to special risks not ordinarily associated with U.S. securities including currency fluctuations and social, economic and political uncertainties, which could increase volatility.These risks are magnified in emerging markets.The Fund may also invest in smaller and mid-capitalization companies, which involve a higher degree of risk and volatility than investments in larger, more established companies.The Fund may also invest in derivatives, such as options and futures, which can be illiquid, may disproportionately increase losses, and have a potentially large impact on Fund performance.







Diversification does not assure a profit or protect against a loss in a declining market.







The Russell Index is an index which measures the performance of approximately small-cap companies in the Russell Index, which is made up of of the biggest U.S. stocks. The Russell Index serves as a benchmark for small-cap stocks in the United States.







The S&amp;P Index is a capitalization weighted index of five hundred large capitalization stocks, which is designed to measure broad domestic securities markets.







The Lipper Large-Cap Growth Funds Index is an equally-weighted performance index, adjusted for capital gains distributions and income dividends, of the largest mutual funds within the Growth Funds category, as reported by Lipper.







The Dow Jones Industrial Average is a price-weighted average of significant stocks traded on the New York Stock Exchange and the Nasdaq.







An index is unmanaged and reflects the reinvestment of dividends and capital gains, but does not reflect the deduction of investment advisory fees.Investors cannot invest directly in an index.







Fund holdings and sector allocations are subject to change and should not be considered a recommendation to buy or sell any security.For a complete list of portfolio holdings, please refer the Schedule of Investments provided in this report.







The Barrett Growth Fund is distributed by Quasar Distributors, LLC.






































BARRETT






GROWTH FUND




Expense Example May (Unaudited)
