Dear Shareholder:

We are pleased to present this annual report for Dreyfus NewYork Tax Exempt Bond Fund, Inc., covering the period from June through May For information about how the fund performed during the reporting period, as well as general market perspectives, we provide a Discussion of Fund Performance on the pages that follow.

In the wake of heightened market volatility over much of municipal bonds generally stabilized over the first five months of enabling them to post positive total returns, on average, for the reporting period overall.Although concerns regarding a shift to a more moderately accommodative monetary policy initially roiled fixed income markets, investors later took the Federal Reserve Boards actions in stride. Moreover, investor demand rebounded while the supply of newly issued securities ebbed, and most states and municipalities saw improved credit conditions in the recovering U.S. economy.

We remain cautiously optimistic regarding the municipal bond markets prospects over the months ahead.We expect the domestic economy to continue to strengthen, which could support higher tax revenues for most states and municipalities.We also anticipate rising demand for a limited supply of securities as more income-oriented investors seek the tax advantages of municipal bonds. As always, we encourage you to discuss our observations with your financial advisor to assess their potential impact on your investments.

Thank you for your continued confidence and support.



J. Charles Cardona
President
The Dreyfus Corporation
June









DISCUSSION OF FUND PERFORMANCE

For the period of June through May as provided by Thomas Casey and David Belton, Portfolio Managers

Fund and Market Performance Overview

For the period ended May Dreyfus NewYork Tax Exempt Bond Fund achieved a total return of In comparison, the Barclays Municipal Bond Index (the Index), the funds benchmark index, which is composed of bonds issued nationally and not solely within New York, achieved a total return of for the same

Heightened market volatility over the reporting periods first half was followed by rallies as long-term interest rates moderated, investor demand rebounded, the supply of newly issued securities declined, and credit conditions improved.The fund lagged its benchmark, mainly due to weakness among Puerto Rico bonds owned by the fund.

The Funds Investment Approach

The fund seeks as high a level of current income exempt from federal, New York state, and New York city income taxes as is consistent with the preservation of capital. To pursue its goal, the fund normally invests substantially all of its assets in municipal bonds that provide income exempt from federal, New York state, and New York city personal income taxes.The dollar-weighted average maturity of the funds portfolio normally exceeds years, but the fund may invest without regard to maturity.The fund will invest at least of its assets in investment-grade municipal bonds or the unrated equivalent as determined by Dreyfus.The fund may invest up to of its assets in municipal bonds rated below investment grade (junk bonds) or the unrated equivalent as determined by Dreyfus.

We focus on identifying undervalued sectors and securities, and we minimize the use of interest rate forecasting. We select municipal bonds by using fundamental credit analysis to estimate the relative value and attractiveness of various sectors and securities and to exploit pricing inefficiencies in the municipal bond market. We actively trade among various sectors, such as pre-refunded, general obligation, and revenue, based on their apparent relative values.

The Fund






DISCUSSION OF FUND PERFORMANCE (continued)

Municipal Bonds Rebounded from Earlier Weakness

Like many other financial assets, municipal bonds generally lost value at the start of the reporting period in the wake of news that the Federal Reserve Board would soon back away from its quantitative easing program. The resulting market turbulence sparked a flight of capital from the municipal bond market, sending long-term bond prices lower and yields higher. Selling pressure was particularly severe among lower rated and longer term securities.

Municipal bonds generally stabilized in the fall, and the first five months of witnessed a market recovery amid weaker-than-expected economic data stemming from unusually harsh winter weather. Municipal bond prices also were supported by favorable supply-and-demand dynamics when investor demand recovered and less refinancing activity produced a reduced supply of newly issued securities.

The economic rebound resulted in better underlying credit conditions for NewYork and many other states as higher tax revenues enabled them to achieve budget surpluses and replenish reserves. However, credit concerns lingered with regard to municipal bonds issued by Puerto Rico, which are exempt from federal and NewYork state taxes but lost value after media reports detailed the U.S. territorys fiscal challenges.

Puerto Rico Bonds Undermined Relative Performance

The funds relative performance was hampered during the reporting period by overweighted exposure to Puerto Rico securities.We took advantage of periodic rebounds during to reduce the funds exposure to Puerto Rico bonds at relatively attractive prices.

The fund achieved better relative performance in other areas. An emphasis on NewYork revenue bonds proved effective, and the fund received especially positive contributions from bonds backed by hospitals, airports, educational facilities, and the states settlement of litigation with U.S. tobacco companies. An underweighted position in lower yielding escrowed bonds also buoyed relative results. The fund further benefited from its interest-rate strategies, as a relatively long average duration helped support seeking higher current income and enabled fuller participation in gains at the longer end of the markets maturity spectrum.








Maintaining a Constructive Investment Posture

We already have seen evidence of a stronger U.S. recovery in warmer spring weather, including a strengthening labor market and improved investor confidence. In addition, we believe that recently positive market trends have been driven, in part, by investors returning their focus to market and issuer fundamentals now that the Fed is tapering its quantitative easing program, which we expect to continue. Finally, we expect investor demand to intensify as investors seek to reinvest principal from maturing bonds in a limited number of newly issued tax-exempt securities.

In light of favorable fundamental and technical market conditions, we have redeployed proceeds from recent sales of escrowed securities and Puerto Rico bonds to longer term NewYork revenue bonds with strong income characteristics.We also have maintained a relatively long average duration in seeking to capture higher levels of current income and in anticipation of narrower yield differences along the markets maturity spectrum that merits the long duration positioning.

June

Bond funds are subject generally to interest rate, credit, liquidity, and market risks, to varying degrees, all of which are more fully described in the funds prospectus. Generally, all other factors being equal, bond prices are inversely related to interest-rate changes, and rate increases can cause price declines.

High yield bonds are subject to increased credit risk and are considered speculative in terms of the issuers perceived ability to continue making interest payments on a timely basis and to repay principal upon maturity.

For derivatives with a leveraging component, adverse changes in the value or level of the underlying asset can result in a loss that is much greater than the original investment in the derivative.













Total return includes reinvestment of dividends and any capital gains paid, and does not take into consideration the
