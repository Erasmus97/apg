Dear Shareholder:

We are pleased to present the Steward Funds Annual Report for the year ending April The Annual Report includes commentary on the Funds screening process, as well as performance reviews and current strategy from the portfolio managers.

Steward Funds Offer Diversification to Your Portfolio

The asset allocation decision is one of the most important decisions an investor will make and the Steward Funds make it possible to achieve a diverse asset allocation for your investment portfolio. The Funds offer fixed income, large-cap equity, small/mid-cap equity, international equity and global equity income for your investing solutions.

What are the Steward Funds Faith-Based Screens?

Did you know faith-based screens are applied to all of the investments within the Steward Funds family? The Steward Funds seek to avoid investment in pornography and abortion, as well as companies that are substantial producers of alcohol, gambling and tobacco. The Funds screening resource firm, CFS Consulting Services, LLC, has extensive experience in the faith-based investing business and brings a wealth of knowledge of screening for Christian-based investors.

The following details each of the Funds annual performance and strategies.

STEWARD LARGE CAP ENHANCED INDEX FUND

Fund Performance

For the year ended April the Steward Large Cap Enhanced Index Fund returned for the Individual class shares and for the Institutional class shares. The return for the S&amp;P Index for the same period was The S&amp;P Pure Index component returns were: for the S&amp;P Pure Growth Index and for the S&amp;P Pure Value Index.

Factors Affecting Performance

The Funds blended-index structure at April was allocated to the S&amp;P S&amp;P Pure Growth and S&amp;P Pure Value indices at and respectively. The Fund has maintained this allocation for the entire period. The anticipated reduction of central bank stimulus by the Fed was finally announced in December. The tapering of its monthly asset purchases by billion per meeting is now taking place. Economic growth weakness in the U.S. over the past few months has been attributed to severe winter weather across the country. On the political front, Russias occupation of the Crimea region
of Ukraine has added more uncertainty to the markets as that situation continues to develop. Forward looking earnings growth estimates for remain a solid despite some weakness from the first quarter of the year. Valuations of the S&amp;P continued to rise in the second half of the period hitting, new highs despite a mild pullback in January.

Growth and value style momentum continued to shift directions unable to sustain a clear trend. The Fund will continue its neutral style position in this environment. The more style focused S&amp;P Pure Growth and S&amp;P Pure Value indices, when combined, have outperformed their non-pure counterparts over the year, contributing to the Funds higher relative return. The higher weighted style focused companies such as Genworth Financial, Inc. Class A at and Netflix, Inc. at were the most significant factors.

Performance of the Fund can also be affected by the Funds faith-based values investment policies, which avoid investments in companies whose primary business is associated with alcohol, tobacco products, abortion, gambling and pornography. For the year ended April the faith-based values investment policies had a net positive performance impact on the Fund. Companies such as Philip Morris International Inc. (tobacco) and Pfizer Inc. (abortion/life ethics) substantially underperformed the benchmark with returns of and respectively.

Compensating for a specific restricted industry or company whose total return deviates dramatically from the overall index is extremely difficult regardless of its weight within that index.













TABLE OF CONTENTS



Current Strategy

The Fund is not a passively managed index Fund. Its strategy is to seek to enhance its performance over that of its primary benchmark index by changing the relative weighting in the Funds portfolio of growth versus value style securities in the index (style tilt) and utilizing computer-aided quantitative analysis of valuation, growth, dividend yield, industry and other factors to attempt to compensate for the exclusion of certain index securities due to the faith-based values investment policies. The Funds investments are allocated in an attempt to match the characteristics of a blend of its benchmark index
and varied weighting from time to time of two indices that are subcomponents of the benchmark: large cap growth index and a large cap value index.

STEWARD SMALL-MID CAP ENHANCED INDEX FUND

Fund Performance

For the year ended April the Steward Small Mid-Cap Enhanced Index Fund returned for the Individual class shares and for the Institutional class shares. The return for the S&amp;P Index for the same period was The S&amp;P Pure Index component returns were: for the S&amp;P Pure Growth Index and for the S&amp;P Pure Value Index.

Factors Affecting Performance

The Funds blended-index structure at April was allocated to the S&amp;P S&amp;P Pure Growth and S&amp;P Pure Value indices at and respectively. The Fund has maintained this allocation for the entire period. The anticipated reduction of central bank stimulus by the Fed was finally announced in December. The tapering of its monthly asset purchases by billion per meeting is now taking place. Economic growth weakness in the U.S. over the past few months has been attributed to severe winter weather across the country. On the political front, Russias occupation of the Crimea
region of Ukraine has added more uncertainty to the markets as that situation continues to develop. Forward looking earnings growth estimates for remain a solid despite some weakness from the first quarter of the year. Valuations of the S&amp;P continued to rise in the second half of the period hitting new highs despite a mild pullback in January.

Growth and value style momentum continued to shift directions, being unable to sustain a clear trend. The Fund will continue its neutral style position in this environment. The more style focused S&amp;P Pure Growth and S&amp;P Pure Value indices, when combined, outperformed their non-pure counterparts over the year, contributing to the Funds higher relative return. The higher weighted style focused companies such as Arkansas Best Corp. at and Keurig Green Mountain, Inc. at were the primary factors.

Performance of the Fund can also be affected by the Funds faith-based values investment policies, which avoid investments in companies whose primary business is associated with alcohol, tobacco products, abortion, gambling and pornography. For the year ended April the faith-based values investment policies had a slight positive performance impact on the Fund. Companies such as Community Health Systems, Inc. (abortion) and AMC Networks Inc. Class A (adult entertainment) significantly underperformed the benchmark with returns of and respectively.

Compensating for a specific industry or company whose total return deviates dramatically from the overall index is extremely difficult regardless of its weight within that index.

Current Strategy

The Fund is not a passively managed index Fund. Its strategy is to seek to enhance its performance over that of its primary benchmark index by changing the relative weighting in the Funds portfolio of growth versus value style securities in the index (style tilt) and utilizing computer-aided quantitative analysis of valuation, growth, dividend yield, industry and other factors to attempt to compensate for the exclusion of certain index securities due to the faith-based values investment policies. The Funds investments are allocated in an attempt to match the characteristics of a blend of its benchmark index
and varied weighting from time to time of two indices that are subcomponents of the benchmark: small-mid cap growth index and a small-mid cap value index.

STEWARD INTERNATIONAL ENHANCED INDEX FUND

Fund Performance

For the year ended April the Steward International Enhanced Index Fund returned for the Individual class shares and for the Institutional class shares. The return for the S&amp;P ADR Index was To represent the emerging markets component, the BLDRS Emerging Markets ADR Index Fund returned for the same period.

Factors Affecting Performance

For the year ended April the Funds dual-index structure was allocated between the S&amp;P ADR Index and BLDRS Emerging Markets ADR Index Fund at and respectively, representing a tilt towards emerging markets. The Fund













TABLE OF CONTENTS





has maintained this allocation for the entire month period. Recent economic data in Europe is showing an improving economy, with inflation forecasts being revised downward. This suggests that the European Central Bank will continue its accommodative policy into Disappointing economic reports in China may explain the new interest in reforming their foreign exchange policy linked to the U.S. Dollar. On the political front, Russias occupation of the Crimea region of Ukraine has added more uncertainty to the markets as that situation continues to develop. Valuations in the international equity markets has been mixed
with developed country markets generally following the U.S. lead of double digit gains while emerging market values lagged.

The emerging markets component of the Fund continued to underperform the international developed markets during the period. Emerging market companies such as Petroleo Brasileiro SA (Brazil) and China Mobil Ltd. (Hong Kong) were the primary factors with returns of and respectively.

Performance of the Fund can also be affected by the faith-based values investment policies, which avoid investments in companies whose primary business is associated with alcohol, tobacco products, abortion, gambling and pornography. For the year ended April the faith-based values investment policies had a net negative performance impact on the Fund. Companies such as AstraZeneca PLC (abortion/life ethics) and Novartis AG (abortion/life ethics) substantially outperformed the benchmark with returns of and respectively.

Compensating for a specific restricted industry or company whose total return deviates dramatically from the overall index is extremely difficult regardless of its weight within that Index.

Current Strategy

The Fund is not a passively managed index Fund. The Funds strategy is to seek to enhance its performance over that of its primary benchmark index by changing the relative weighting in the Funds portfolio of equity securities of developed market companies and of emerging market companies, and utilizing computer-aided quantitative analysis of valuation, growth, dividend yield, industry and other factors to attempt to compensate for the exclusion of certain index securities due to the Funds faith-based values investment policies. The Funds investments are allocated in an attempt to match the
characteristics of a blend of the primary benchmark with varied weighting from time to time of a secondary benchmark that includes only securities of issuers in emerging market countries.

STEWARD SELECT BOND FUND

Market Overview

Have you ever sat down in a public environment and simply watched the people pass by, trying to determine their story and wondering if your assumptions are true? From the end of January, it appears that the bond market has been doing just that content to mainly sit on the sidelines and watch what goes on around it, maintaining a tight trading range and waiting for more solid evidence of the path of future growth. Over the past few months, we have received some conflicting reports with weather-related distortions to the economic reports, rising geo-political tensions, and discussions on the next moves by
central banks around the world. With a market that usually makes some decent movements on headline news, the bond market has maintained a trading range on the US Treasury note between roughly regardless of the news hitting the tape. Is this going to change our outlook and cause us to alter our strategy going forward? The simple answer is No.

About one year ago, we made the decision to position the Steward Select Bond Fund with a shorter duration than its comparable index in anticipation of rates moving higher from their levels on the US Treasury note. Following Federal Reserve Chairman Bernankes announcement of impending tapering of the quantitative easing programs, we did see rates rise up, at an extremely quick pace, to at the end of In the beginning of rates fell from the level back to the trading range where we have remained since. We still feel rates will rise as we progress through the end of as forces
holding rates low, such as: Ukraine tensions, speculation on European Central Bank stimulus programs, low European sovereign yields and economic growth weaker than hoped for in the U.S. and abroad, will tend to diminish.

We anticipate that with outside forces holding rates low as we move through rates will begin to rise and our portfolio positioning and strategy will prove to be a strong positive contribution to the performance of the Fund. The Fed should continue on a consistent pace to eliminate all QE purchases by the end of this year leading to a possible rise in rates beginning around the middle of Growth in the U.S. is anticipated to grow much stronger in the and quarters than we saw in the first part of the year, which was due in part to weather considerations and uncertainties regarding Russia
and the Ukraine. The bond market should remain steady with rates moving higher beginning in the quarter of the year and providing opportunistic market movements to increase yield and total return for the Fund.

Fund Performance

For the year ended April the Steward Select Bond Fund returned for the Individual class shares and for the Institutional class shares. The Barclays Capital US Government/Credit Bond Index returned for the same period.













TABLE OF CONTENTS



Portfolio Positioning

Over the past year, the Steward Select Bond Fund has maintained an overweight to the corporate bond allocation in order to provide the Fund with an elevated level of income during an environment where higher rates were anticipated. As a result, an underweight to the US treasury sector was created as compared to the comparable index, although that was mitigated with an allocation to the government agency sector providing liquidity to the Fund. As stated previously, a shorter duration of years was established vs. years for the Barclays Capital US Government/Credit Bond Index, reducing the interest rate risk for the
Funds portfolio. This positioning was beneficial as rates were rising and will continue, in our opinion, to provide positive contributions to the Fund as rates expectantly increase over the coming quarters.

Current Strategy

Based on an outlook of rising rates, the Fund will maintain its portfolio structure of a shorter duration than the comparable index, while focusing on bonds in the steepest part of the curve with high-quality ratings and strong company cash flows. As rates begin to normalize in coming quarters, we will take the opportunity to extend duration slightly, picking up yield as rates are higher but also due to placement on the yield curve, allowing these bonds to roll down the curve providing positive contributions in both price and income to the Fund. As we have discussed many times, for fixed income securities, total return is a
combination of both price and income with our portfolio providing heightened levels of income as compared to the Barclays Capital US Government/Credit Bond Index. As always, we will continue to monitor the markets and make adjustments to our outlook and Fund construction as deemed appropriate in order to benefit from the current marketplace environment.

STEWARD GLOBAL EQUITY INCOME FUND

Fund Performance

For the year ended April the Steward Global Equity Income Fund returned for the Individual class shares and for the Institutional class shares. The return for the benchmark S&amp;P Index for the same period was The return for the global market as represented by the S&amp;P Global Index was The dividend yield on the portfolio as of the year end was

Factors Affecting Performance

Global central banks were a primary driver of equity markets during the period. The anticipated reduction of central bank stimulus by the Fed was finally announced in December. The tapering of its monthly asset purchases by billion per meeting is now taking place. Economic growth weakness in the U.S. over the past few months has been attributed to severe winter weather across the country. Recent data in Europe is showing an improving economy with inflation remaining extremely low. Disappointing economic reports in China may explain the new interest in reforming their foreign exchange policy linked to the U.S. Dollar. On the
political front, Russias occupation of the Crimea region of Ukraine has added more uncertainty to the markets as that situation continues to develop. Forward looking earnings growth estimates for remain a solid despite some weakness from the first quarter of the year.

Positive contributors on a company specific basis included Raytheon Co. at The company reported third quarter profits dropping from U.S. budget cuts. However it raised its full-year earnings forecast on gains in international sales. Caterpillar Inc. at topped Wall Street earnings estimates on a combination of strong sales of construction equipment and cost cutting. The company slashed overall operating costs after being buffeted by a sharp downturn in the global mining industry. Lockheed Martin Corp. at one of the largest U.S. defense companies, reported a increase in first quarter net profit and
raised its earnings per share forecast. While U.S. budget cuts continue to depress revenue, the company benefitted from long term protected defense contracts.

Relative negative performance within the portfolio was reflected in holdings such as Mattel Inc. at whose earnings disappointed as U.S. holiday toy sales dropped roughly The company continues to market some of the countrys best selling toys with significant growth coming from abroad. Shares of Ensco PLC at an offshore drilling contractor, have declined in recent months because of weakness in the overall sector. A low debt load, secure cash flows and seven new rigs under construction will drive Enscos growth going forward. The strategys quantitative stock selection screens along with the
methodologys validation process continue to perform as designed and are exceeding expectations.

Performance of the Fund can also be affected by the Funds faith-based values investment policies, which avoid investments in companies whose primary business is associated with alcohol, tobacco products, abortion, gambling and pornography. Compensating for a specific restricted industry or company whose total return deviates dramatically from the overall Index is extremely difficult regardless of its weight within that Index.













TABLE OF CONTENTS



Current Strategy

The Funds strategy pursues its objective through the investment in dividend paying stocks that have demonstrated above median yield and a favorable trend in dividend and earnings growth. In addition to domestic stocks, this strategy includes the ability to invest in international securities traded on U.S. market exchanges. As the international markets share of the worlds total market capitalization continues to grow, the ability to access these markets becomes increasingly important.

The benefits of dividend paying stocks include lower volatility versus non-dividend payers and the overall market. Dividends are an important indicator of corporate strength. Therefore companies are reluctant to change a policy that encourages disciplined management, since that could signal corporate distress. Unlike earnings, which can be affected by various accounting methods; dividends are transparent and cannot be manipulated. In addition, dividends have historically provided a major component of the stock markets total return. The strategy provides income with capital appreciation while lowering overall risk. This is
accomplished while adhering to the Funds faith-based values investment policies.

We Thank You for Your Continued Investment with the Steward Funds

Your business is important to us. The current total net assets of the Funds are million. This growth would not have been possible without the many referrals that we have received from our existing clients, as well as your continued investment. For more information on the Steward Funds, we invite you to visit our website at www.stewardmutualfunds.com or call us at We look forward to fulfilling your investment needs for many years to come.

Sincerely,
