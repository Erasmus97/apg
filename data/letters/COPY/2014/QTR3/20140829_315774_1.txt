Dear Fellow Shareholder,



Performance



Please find below recent performance results for each of the DF Dent Growth Funds compared against their respective benchmarks for periods ending June









DF Dent

Premier Growth



DF Dent

Midcap Growth



DF Dent

Small Cap Growth



Months















Fund



-







%



-





%



-





%



Benchmark



S&amp;P



Russell Midcap Growth



Russell Growth



Benchmark Performance



+





%



+





%



+





%



Fund vs Benchmark



-





%



-





%



-





%

















Months















Fund



+





%



+





%




N/A




Benchmark



+





%



+





%




N/A




Fund vs Benchmark



-





%



-





%




N/A


















Years















Fund



+





%




N/A





N/A




Benchmark



+





%




N/A





N/A




Fund vs Benchmark



+





%




N/A





N/A


















Since Inception















Fund



+





%*



+





%*



+





%



Benchmark



+





%*



+





%*



+





%



Fund vs Benchmark



+





%*



+





%*



-





%



* annualized





























Cumulative Since Inception















Fund



+





%



+





%



+





%



Benchmark



+





%



+





%



+





%



Fund vs Benchmark



+





%



+





%



-





%



Inception Date



















N/A- Periods which exceed the life of the particular fund.



Performance data quoted represents past performance and is no guarantee of future results. Current performance may be lower or higher than the performance data quoted. Investment return and principal value will fluctuate so that an investors shares, when redeemed, may be worth more or less than original cost. For the most recent month-end performance, please call As stated in the current prospectus, the DF Dent Premier Growth Funds and DF Dent Midcap Growth Funds annual operating expense ratios (gross) are



DENT GROWTHFUNDS























DF DENT PREMIER GROWTH FUND

A MESSAGE TO OUR SHAREHOLDERS (Unaudited)

Dear Fellow Shareholders:



Performance



For the period July through June the DF Dent Midcap Growth Fund (the Fund) experienced a total return of underperforming the total return of for the Russell Midcap Growth Index (the Index), which is the benchmark we have used for performance comparisons, by



Expense Ratio



Your Funds Adviser, D.F. Dent and Company, Inc. (the Adviser), has contractually agreed to maintain your Funds expense ratio (excluding all taxes, interest, portfolio transaction expenses, and extraordinary expenses) through October at a net on the first million of net assets and on net assets exceeding million by reimbursing expenses and waiving a portion of management fees. Per the prospectus, the Funds gross total operating expense ratio is



Concentration



The Funds concentration in its top holdings is as follows:







Top Holdings








% of Fund







%



Average Position Size of Top Per Holding







%







We believe that the current concentration in the top positions is appropriate at its current level and has the potential to enhance long-term performance.



Management Ownership of Fund



Employees, their families, and the Advisers retirement plan owned approximately of the Fund as of June There were only management purchases and no management redemptions during the fiscal year ended June



Portfolio Commentary



Despite the persistent strength of the major indices, the internal dynamics of the stock market have shifted this year. Unlike investors have favored more defensive strategies as evidenced by the strong preference for stocks with large capitalizations, low price earnings ratios and high dividend yields. These trends adversely impacted the investment results from your growth-oriented Fund.



After strong gains over the last several years, in late and early we trimmed some stocks in your Funds portfolio that had performed extremely well. This trimming was in reaction to significant multiple





DENT GROWTH FUNDS























DF DENT MIDCAP GROWTH FUND

A MESSAGE TO OUR SHAREHOLDERS (Unaudited)

Dear Fellow Shareholders:



Performance



The DF Dent Small Cap Growth Fund (the Fund) experienced a total return of for the period of November (the date of inception for the Fund), through June This represents an underperformance of relative to the total return of for the Russell Growth Index (the Index), which is the benchmark we have used for performance comparisons, for the same timeframe.



Expense Ratio



The estimated gross operating expense ratio for the Fund is currently which is high given the small size of Fund assets relative to certain fixed expenses. However, per the Funds prospectus, your Funds adviser, D.F. Dent and Company, Inc. (the Adviser), has contractually agreed to waive a portion of its fees and/or reimburse expenses so as to maintain your Funds expense ratio (excluding all taxes, interest, portfolio transaction expenses, and extraordinary expenses) at a net through at least October



Concentration



The Funds concentration in its top holdings is as follows:







Top Holdings








% of Fund







%



Average Position Size of Top Per Holding







%







We believe that the current concentration in the top positions is appropriate at its current level and has the potential to enhance long-term performance.



Management Ownership of Fund



Employees, their families, and the Advisers retirement plan owned approximately of the Fund as of June There were only management purchases and no management redemptions during the fiscal period ended June



Portfolio Commentary



Since the Funds inception in November most U.S. stock market indices have appreciated considerably. The movements upward, however, have not been uniform throughout the period or across the asset class, and the seemingly persistent strength in the market has somewhat masked a strong change in investor appetite. Of note, many of the leaders in have pivoted to laggards in the first half of Smaller capitalization stocks in general, and growth-oriented smaller capitalization stocks in particular, are examples of this dynamic, with the Russell Growth Index generating a total return of in and just a



DF DENT GROWTH FUNDS























DF DENT SMALL CAP GROWTH FUND

A MESSAGE TO OUR SHAREHOLDERS (Unaudited)

Dear Shareholder,



We are pleased to present the annual report for the Golden Large Cap Core Fund and the Golden Small Cap Core Fund (the Funds) for the period of July through June




Market Review




Third Quarter of




During the third quarter of the momentum from the strong first half of continued to push broad measures of the U.S. equity markets higher. The Russell Index returned the Russell Index of small cap stocks returned and the S&amp;P Index rose Global equity markets posted strong returns as well. The MSCI All-Country World Index rose the MSCI EAFE Index rose and the MSCI Emerging Markets Index ended the quarter up




In the U.S., economic data reported during the quarter continued to indicate a slow-growing economy. The positive data included: U.S. payroll reports that showed continued modest growth in the number of jobs, which pushed the unemployment rate down to very low measures of inflation; strengthening in the manufacturing and services sectors of the economy; and consumer confidence measures near post-recession highs as consumers benefited from improved labor markets and higher stock prices. Some of the weak economic data items of note included: a continuation of the weak core retail sales trend; second quarter GDP growth estimated at a persistently high trade deficit; and the first indications of cooling in the housing market.




Over the course of the quarter, many investors were concerned about the Federal Reserves (Fed) billion per month bond buying program. Consensus seemed to expect tapering to begin in September; though, the Fed surprised the market when it made no change to its policy.




Fourth Quarter of




The fourth quarter of saw U.S. equity markets move higher to cap off a year of impressive returns. Global equity markets trailed the U.S., though they also posted impressive calendar year returns and positive returns in the quarter. Emerging markets were the exception as the MSCI Emerging Markets Index ended the year in negative territory.




In the U.S., data released during the quarter continued to indicate economic growth. The positive data included: U.S. payroll reports that showed continued modest growth in the number of jobs, which pushed the unemployment rate down to very low measures of inflation; modest growth in the manufacturing and services sectors of the economy; and third quarter GDP growth estimated at Some of the weaker economic data items included: consumer confidence measures that retreated from their recent post-recession highs despite improved labor markets and higher stock prices; a continuation of the weak core retail sales trend; weak growth in personal incomes; and mixed data in the U.S. housing market.





























GOLDEN FUNDS

A MESSAGE TO OUR SHAREHOLDERS



JUNE


















On the fiscal policy front, a temporary budget deal was reached, and it provided some stability to the fiscal outlook. On the monetary policy front, the Fed surprised the market at its December meeting when it announced a reduction in its billion per month bond buying program. The reduction was set to begin in January, at which point the Fed would begin reducing its monthly asset purchases by billion.




First Quarter of




During the first quarter of most broad measures of the global equity markets posted modest positive returns. In the U.S., large cap stocks outpaced small caps. Outside the U.S., developed market small cap stocks generated strong returns, while emerging markets were down slightly.




In the U.S., most measures of the economy indicated continued slow growth. Manufacturing continued expanding slowly; retail sales reports were weak; unemployment hovered around inflation was low; and surveys of consumer confidence improved modestly over the quarter. While many economic reports fell short of consensus expectations, much of the weakness was attributed to worse-than-normal winter weather.




The Fed continued to reduce the scale of its quantitative easing program over the course of the quarter.




Second Quarter of




During the second quarter of most broad measures of the global equity markets posted positive returns. In the U.S., large cap stocks outpaced small caps. Outside the U.S., emerging market stocks generated the strongest returns, while developed market small caps slightly trailed larger cap stocks.




In the U.S., GDP was revised significantly lower to While the GDP revision was much larger than expected, most of the real-time measures of economic activity indicated that should return to the recent trend of slow growth. Manufacturing results indicated slow expansion; retail sales reports indicated weak growth; unemployment continued to improve to the current rate of inflation remained low; and surveys of consumer confidence improved modestly over the quarter.




The Fed continued to steadily reduce the scale of its quantitative easing program over the course of the quarter, but Fed officials emphasized that the near-zero interest rate policy will be maintained for a considerable time after the bond-buying program ends in October.




Golden Large Cap Core Fund




The Fund seeks to achieve long-term capital appreciation by investing in large capitalization domestic equities.The goal of the Fund is to construct an actively managed portfolio of fundamentally sound large-cap companies that we believe exhibit the likelihood of meeting or exceeding analysts earnings expectations.




For the one-year period ending June the Fund delivered a total return of The Funds return was ahead of the total return of the S&amp;P the Funds benchmark index, for the same period. The






























GOLDEN FUNDS

A MESSAGE TO OUR SHAREHOLDERS



JUNE



















market has steadily improved since early and rewarded investors that maintained their exposure to equities.




The Funds best sources of advantage relative to the S&amp;P over the last months were the Consumer Discretionary and Consumer Staples sectors, most notably, stock selection within those sectors. Within the Fund, the Consumer Discretionary sector was helped by a strong advance in the shares of apparel company Hanesbrands Inc. (HBI). The companys Innovate to Elevate strategy has successfully powered fundamental improvements through product innovation, supply chain efficiencies, and synergies from acquisitions. In the Consumer Staples sector, the Funds strength was led by Herbalife Ltd. (HLF) and CVS Caremark (CVS). Herbalife has been able to grow sales volume by promoting daily consumption programs and expanding its sales force. CVS Caremarks pharmacy segment has experienced growth in drug distribution that is at least partially attributable to the Affordable Care Act.




The Energy sector was the primary source of relative underperformance during the period. The weakness was based largely on our underweight exposure to oil producers in the rising crude oil environment that characterized the first half of In addition, shares of Valero Energy (VLO), an independent oil refiner that was recently added to the Fund, declined when the Commerce Department announced that it would begin to allow the export of unrefined crude oil. Stock selection in the Materials sector also detracted over the period. Three of the Funds four holdings in the sector underperformed, as compared to the return of the Materials sector in the benchmark, led by shares of Berry Plastics Group (BERY), which declined over our holding period.




Golden Small Cap Core Fund




The Fund seeks to achieve maximum long-term total return by investing in small capitalization domestic equities. The goal of the Fund is to construct an actively managed portfolio of fundamentally sound small-cap companies that we believe exhibit the likelihood of meeting or exceeding analysts earnings expectations.




For the one-year period ending June the Fund delivered a total return of The Funds return was ahead of the total return of the Russell the Funds benchmark index, for the same period. The market has steadily improved since early and rewarded investors that maintained their exposure to equities.




Overall, stock selection and sector weighting decisions added value relative to the benchmark for the twelve month period. The Fund benefitted most from stock selection within the Health Care and Industrials sectors.Top performers within these groups included generic pharmaceutical company Lannett (LCI), specialty hospital and rehabilitation clinic Select Medical Holdings (SEM), electrical weapons manufacturer Taser International (TASR), and check printer and business services company Deluxe Corp (DLX). Our overweight exposure to Industrials was also favorable as the sector benefited from a rebound in capital spending and a strong transportation industry.




The Funds weakest results relative to the benchmark came from the Financials sector. While an underweight exposure to this sector was beneficial, stock selection was weak within the Banks, Insurance, and Real






























GOLDEN FUNDS

A MESSAGE TO OUR SHAREHOLDERS



JUNE



















Estate industries as Susquehanna Bancshares (SUSQ), Employer Holdings (EIG), and American Residential Properties (ARPI) were notable detractors. Stock selection was also weak in the Information Technology sector where Angies List (ANGI) was the bottom performer.




Market Outlook

As we begin the third quarter of investors seem to have re-embraced risk. Emerging market stocks have recently rallied. Junk bond spreads over U.S. Treasury bonds have tightened to the lowest levels since Peripheral European sovereign bond yields are near historic low levels. To those with a bullish outlook, these conditions could indicate that investors have positioned themselves for a strengthening global economy. To those espousing a more bearish outlook, they could represent unwarranted optimism.




Global central bank policies are beginning to diverge. The U.S. Federal Reserve is on a path to end its quantitative easing policy by the end of the year. The Bank of Japan has expressed its intention to continue its aggressive monetary policies. The European Central Bank has recently become more accommodative and has hinted toward the possibility of even more aggressive policies should the inflation rate fall further.




The equity markets have generally benefitted from multiple expansion over the course of this market cycle. The table below shows the current consensus estimates for sales and earnings growth in (reporting season begins early in July) and for the full year.










Sales Growth Estimate




EPS Growth Estimate




Sales Growth Estimate




EPS Growth Estimate




S&amp;P




%



%



%



%


Russell




%



%



%



%


MSCI AC World




%



%



%



%


MSCI EM (Emerging Markets)




%



%



%



%


MSCI EAFE




%



%



%



%


Source: FactSet























The most optimistic expectations are for U.S. small caps and for international developed markets large cap stocks. As valuations increase, we believe it becomes even more important to focus on company fundamentals. As we select investments in the new quarter, we will seek to identify companies that can generate the earnings necessary to justify their valuations and meet investor expectations.




We value and appreciate our relationship with the Funds and thank you for your support.






























GOLDEN FUNDS

A MESSAGE TO OUR SHAREHOLDERS



JUNE



















Sincerely,
