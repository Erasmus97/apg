Dear Shareholder,
 
As the French say, the more things change,
the more they stay the same. Though the global economy is slowly sputtering back to life, the world remains enthralled by old
policy notions that led us to the brink of collapse in 2008. In many nations we continue to see political ideology trump
economic reality, generally at the expense of the citizenry.
 
The world needs some new thinking, and we may
get it from an unexpected source: the International Monetary Fund (the “IMF”). In recent reports, the IMF assessed
key risks facing the stability of the global financial system, as the advanced and emerging economies shift from “liquidity-driven”
to “growth-driven” markets. While markets are becoming more robust, the transition is far from complete, and the IMF
observed that “In far too many countries, improvements in financial markets have not translated into improvements in the
real economy — and in the lives of people.” Perhaps as the global economy stabilizes we will see new policy recommendations
for sustaining better economic outcomes.
 
In the meantime, we must strive to reach our
investment goals in the marketplace that now exists. It’s always true but bears repeating that you should invest to achieve
your long-term goals, not to beat the market today or this week or this month. Keep your eye on your goals and don’t let
yourself be distracted by attention-grabbing headlines or seemingly “hot” investment opportunities. And always thoroughly
discuss your situation with your financial advisor before making any changes to your plans or your investments.
 
Reaching investment goals is a long-term process,
a journey in which stability and continuity play important parts. On May 1, 2014, ING U.S. Investment Management changed its name
to Voya Investment Management. Our new name reminds us that a secure financial future is about more than just reaching a destination
— it’s about positive experiences along the way. It’s also about continuity: there will be no changes in terms
of investment processes or the services we provide to you, our clients. As part of the transition to our new name, we are building
upon our commitment to be a reliable partner committed to reliable investing.
 
We appreciate your continued confidence in us, and we look forward
to serving your investment needs in the future.
 
Sincerely,


Dear Shareholder,
 
As the French say, the more things change,
the more they stay the same. Though the global economy is slowly sputtering back to life, the world remains enthralled by old policy
notions that led us to the brink of collapse in 2008. In many nations we continue to see political ideology trump economic reality,
generally at the expense of the citizenry.
 
The world needs some new thinking, and we
may get it from an unexpected source: the International Monetary Fund (the “IMF”). In recent reports, the IMF assessed
key risks facing the stability of the global financial system, as the advanced and emerging economies shift from “liquidity-driven”
to “growth-driven” markets. While markets are becoming more robust, the transition is far from complete, and the IMF
observed that “In far too many countries, improvements in financial markets have not translated into improvements in the
real economy — and in the lives of people.” Perhaps as the global economy stabilizes we will see new policy recommendations
for sustaining better economic outcomes.
 
In the meantime, we must strive to reach
our investment goals in the marketplace that now exists. It’s always true but bears repeating that you should invest to achieve
your long-term goals, not to beat the market today or this week or this month. Keep your eye on your goals and don’t let
yourself be distracted by attention-grabbing headlines or seemingly “hot” investment opportunities. And always thoroughly
discuss your situation with your financial advisor before making any changes to your plans or your investments.
 
Reaching investment goals is a long-term
process, a journey in which stability and continuity play important parts. On May 1, 2014, ING U.S. Investment Management changed
its name to Voya Investment Management. Our new name reminds us that a secure financial future is about more than just reaching
a destination — it’s about positive experiences along the way. It’s also about continuity: there will be no changes
in terms of investment processes or the services we provide to you, our clients. As part of the transition to our new name, we
are building upon our commitment to be a reliable partner committed to reliable investing.
 
We appreciate your continued confidence
in us, and we look forward to serving your investment needs in the future.
 
Sincerely,


Dear Shareholder,
 
As the French say, the more things change, the more
they stay the same. Though the global economy is slowly sputtering back to life, the world remains enthralled by old policy notions
that led us to the brink of collapse in 2008. In many nations we continue to see political ideology trump economic reality, generally
at the expense of the citizenry.
 
The world needs some new thinking, and we may get it
from an unexpected source: the International Monetary Fund (the “IMF”). In recent reports, the IMF assessed key risks
facing the stability of the global financial system, as the advanced and emerging economies shift from “liquidity-driven”
to “growth-driven” markets. While markets are becoming more robust, the transition is far from complete, and the IMF
observed that “In far too many countries, improvements in financial markets have not translated into improvements in the
real economy — and in the lives of people.” Perhaps as the global economy stabilizes we will see new policy recommendations
for sustaining better economic outcomes.
 
In the meantime, we must strive to reach our investment
goals in the marketplace that now exists. It’s always true but bears repeating that you should invest to achieve your long-term
goals, not to beat the market today or this week or this month. Keep your eye on your goals and don’t let yourself be distracted
by attention-grabbing headlines or seemingly “hot” investment opportunities. And always thoroughly discuss your situation
with your financial advisor before making any changes to your plans or your investments.
 
Reaching investment goals is a long-term process, a
journey in which stability and continuity play important parts. On May 1, 2014, ING U.S. Investment Management changed its name
to Voya Investment Management. Our new name reminds us that a secure financial future is about more than just reaching a destination
— it’s about positive experiences along the way. It’s also about continuity: there will be no changes in terms
of investment processes or the services we provide to you, our clients. As part of the transition to our new name, we are building
upon our commitment to be a reliable partner committed to reliable investing.
 
We appreciate your continued confidence in us, and we
look forward to serving your investment needs in the future.
 
Sincerely,


