Dear Shareholder:

We are pleased to present this annual report for Dreyfus Intermediate Municipal Bond Fund, Inc., covering the period from June through May For information about how the fund performed during the reporting period, as well as general market perspectives, we provide a Discussion of Fund Performance on the pages that follow.

In the wake of heightened market volatility over much of municipal bonds generally stabilized over the first five months of enabling them to post positive total returns, on average, for the reporting period overall.Although concerns regarding a shift to a more moderately accommodative monetary policy initially roiled fixed income markets, investors later took the Federal Reserve Boards actions in stride. Moreover, investor demand rebounded while the supply of newly issued securities ebbed, and most states and municipalities saw improved credit conditions in the recovering U.S. economy.

We remain cautiously optimistic regarding the municipal bond markets prospects over the months ahead.We expect the domestic economy to continue to strengthen, which could support higher tax revenues for most states and municipalities.We also anticipate rising demand for a limited supply of securities as more income-oriented investors seek the tax advantages of municipal bonds. As always, we encourage you to discuss our observations with your financial advisor to assess their potential impact on your investments.

Thank you for your continued confidence and support.



J. Charles Cardona
President
The Dreyfus Corporation
June









DISCUSSION OF FUND PERFORMANCE

For the period from June through May as provided by Steven Harvey, Portfolio Manager

Fund and Market Performance Overview

For the period ended May Dreyfus Intermediate Municipal Bond Fund, Inc. achieved a total return of The Barclays Municipal Bond Index (the Index), the funds benchmark, achieved a total return of for the same

Heightened market volatility over the reporting periods first half was followed by rallies as long-term interest rates moderated, investor demand rebounded, the supply of newly issued securities declined, and credit conditions improved.The fund lagged its benchmark, mainly due to weakness among Puerto Rico bonds owned by the fund.

The Funds Investment Approach

The fund seeks the maximum amount of current income exempt from federal income tax as is consistent with the preservation of capital.To pursue its goal, the fund normally invests substantially all of its net assets in municipal bonds that provide income exempt from federal personal income tax.

The fund invests at least of its assets in municipal bonds rated A or higher, or the unrated equivalent as determined by The Dreyfus Corporation (Dreyfus).The fund may invest up to of its assets in municipal bonds rated below A, including bonds rated below investment grade (high yield or junk bonds) or the unrated equivalent as determined by Dreyfus. The dollar-weighted average maturity of the funds portfolio ranges between three and years.

We focus on identifying undervalued sectors and securities, and we minimize the use of interest rate forecasting. We select municipal bonds by using fundamental credit analysis to estimate the relative value and attractiveness of various sectors and securities and to exploit pricing inefficiencies in the municipal bond market. We actively trade among various sectors, such as pre-refunded, general obligation, and revenue, based on their apparent relative values.

The Fund






DISCUSSION OF FUND PERFORMANCE (continued)

Most Municipal Bonds Rebounded from Earlier Weakness

Like many other financial assets, municipal bonds generally lost value at the start of the reporting period in the wake of news that the Federal Reserve Board (the Fed) would soon back away from its quantitative easing program. The resulting market turbulence sparked a flight of capital from the municipal bond market, sending long-term bond prices lower and yields higher. Selling pressure was particularly severe among lower rated and longer term securities.

Municipal bonds generally stabilized in the fall, and the first five months of witnessed a market recovery amid weaker-than-expected economic data stemming from unusually harsh winter weather. Municipal bond prices also were supported by favorable supply-and-demand dynamics when investor demand recovered and less refinancing activity produced a reduced supply of newly issued securities.

The economic rebound resulted in better underlying credit conditions for most issuers as tax revenues increased, enabling many states to achieve budget surpluses and replenish reserves. However, credit concerns lingered with regard to two issuers: The City of Detroit filed for bankruptcy protection during the summer of and in September, municipal bonds issued by Puerto Rico lost value after media reports detailed the U.S. territorys fiscal challenges.

Puerto Rico Bonds Undermined Relative Performance

During the summer of we responded to pronounced market weakness by taking advantage of newly attractive valuations among lower rated and longer dated municipal bonds. As part of this repositioning, we reduced the funds holdings of AAA-rated securities and added to credits that, in our judgment, were punished more severely than warranted by their underlying fundamentals. New positions included Illinois general obligation bonds and revenue bonds backed by A-rated hospitals, airports, and the states settlement of litigation with U.S. tobacco companies. We also reduced the funds holdings of bonds with maturities of three years and less in favor of bonds with to maturities.These strategies supported the funds relative performance when the municipal bond market rallied over the reporting periods second half.

However, the benefits of a more constructive investment posture were more than offset by weakness among the funds holdings of Puerto Rico securities, which were hurt








by publicity surrounding the U.S. territorys economic challenges and unsustainable pension liabilities.We took advantage of periodic rebounds during to reduce the funds exposure to Puerto Rico bonds at relatively attractive prices.

Adapting to a Changing Market Environment

We already have seen evidence of a stronger U.S. recovery in warmer spring weather, including a strengthening labor market and improved investor confidence. In addition, we believe that recently positive market trends have been driven, in part, by investors returning their focus to market and issuer fundamentals now that the Fed is tapering its quantitative easing program, which we expect to continue.

We have redeployed proceeds from recent sales of Puerto Rico bonds to longer term securities with strong income characteristics. We also have maintained a relatively long average duration in order to capture higher levels of current income and in anticipation of narrower yield differences along the markets maturity spectrum.

June

Bond funds are subject generally to interest rate, credit, liquidity, and market risks, to varying degrees, all of which are more fully described in the funds prospectus. Generally, all other factors being equal, bond prices are inversely related to interest rate changes, and rate increases can cause price declines.

High yield bonds involve increased credit and liquidity risks compared with investment grade bonds and are considered speculative in terms of the issuers ability to pay interest and repay principal on a timely basis.

The use of derivatives involves risks different from, or possibly greater than, the risks associated with investing directly in the underlying assets. Derivatives can be highly volatile, illiquid and difficult to value, and there is the risk that changes in the value of a derivative held by the fund will not correlate with the underlying instruments or the funds other instruments.













Total return includes reinvestment of dividends and any capital gains paid. Past performance is no guarantee of future
