Dear Shareholder: 
The economic recovery strengthened over the past year and financial markets in the U.S. withstood periodic volatility to end the period with robust returns. Throughout the 12 months ended June 30, 2014,
major central banks continued to provide economic stimulus through closely aligned policies of historically low interest rates. During this period, the U.S. unemployment rate fell to 6.09% from 7.54%. While there were discrete sell-offs in U.S.
Treasury bonds, emerging market equities and U.S. small cap stocks, by the end of the period each of these market segments had largely recovered along with broader bond and equity markets. 
 
