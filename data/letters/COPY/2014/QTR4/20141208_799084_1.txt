Dear Shareholders,

We are pleased to present this annual update for the SunAmerica Equity Funds
for the months ended September It was a period wherein
international equity performance was driven largely by economic conditions and
monetary policies of global central banks, both of which diverged as the fiscal
year progressed, as well as by concerns surrounding geopolitical events.

As the annual period began in October generally solid economic data,
coupled with continued accommodative monetary policy from central banks around
the world, raised many investors' expectations for Despite liquidity
concerns in China and tepid economic growth in Europe, market participants were
emboldened by signs of expansionary traction in the U.S. and Japan, the world's
largest and third-largest economies, respectively. In the first quarter of
global equities continued to advance. Despite ongoing geopolitical
tensions surrounding the crisis in Ukraine, concerns about a Chinese growth
slowdown and unsettling economic and political developments in several other
emerging market countries, the global equity markets posted positive returns.
Investors appeared to take solace from comments out of the European Central
Bank ("ECB") and Chinese government suggesting that stimulus measures may be
ramped up. Continued evidence of a Eurozone recovery, solid U.S. corporate
earnings and robust merger and acquisition activity also aided bullish
sentiment. Global equities continued their rally in the second quarter of
as the ECB introduced a set of unconventional measures to fight disinflationary
forces and rekindle growth in the Eurozone. In addition, the People's Bank of
China ("PBOC") announced a cut in its reserve requirement ratio for major banks
in order to stimulate lending and support growth.

Global equities then returned a modest gain in the third quarter of for
the ninth consecutive quarter of positive returns. However, ongoing
geopolitical tensions in Ukraine and the Middle East, Portuguese banking woes,
Argentine government debt default, European economic malaise and the prospect
of an accelerated U.S. Federal Reserve (the "Fed") interest rate hike timeline
all conspired to stall the five-year-old global stock rally in September
In addition, China's property slump and poor GDP readings in Japan and the
Eurozone raised the specter of a slowdown in global economic growth.
Nevertheless, there were several positive developments during the third
calendar quarter. The ECB increased its accommodative monetary policy to spur
growth; the PBOC eased monetary conditions to reduce financial fragilities and
lower the risk of a recession; and the U.S. corporate earnings season painted a
generally encouraging picture. Even with these upbeat events, many market
participants found ample reason to reassess their risk appetites, especially
given the strong bull market in recent years.

Against this backdrop, international equities, as measured by the MSCI ACWI
ex-U.S. (Net)*, posted positive returns but lagged the U.S. equity market,
returning for the period ended September To compare,
U.S. equities, as measured by the S&P Index*, returned for the
same month period. Japanese equities, as measured by the MSCI Japan Index
(Net)*, posted a positive return of during the same annual period.

Amid these conditions, each of the portfolios in the SunAmerica Equity Funds
generated positive absolute returns during the annual period ended September
On the following pages, you will find detailed financial statements
and portfolio information for each of the SunAmerica Equity Funds.









We thank you for being a part of the SunAmerica Equity Funds. We value your
ongoing confidence in us and look forward to serving your investment needs in
the future. As always, if you have any questions regarding your investments,
please contact your financial advisor or get in touch with us directly at
or www.safunds.com.

Sincerely,
