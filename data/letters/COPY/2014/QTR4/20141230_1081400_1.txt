Dear Valued Shareholder: 
Wells Fargo Advantage Emerging Markets Local Bond Fund
Major central banks continued to
provide stimulus. 
%.
Economic growth was sluggish outside the U.S. 
The Fed has a dual mandate to maximize employment and keep prices stable, and economic activity during the period showed improvement on both these fronts. The
unemployment rate ticked lower over the course of the reporting period, reaching 5.8% as of October 2014. More than 200,000 jobs were added to payrolls each month between February and October 2014. On the inflation front, the personal consumption
expenditures price index rose from a deflationary danger zone toward the Fed’s longer-run objective of a 2% pace. 
sales-tax
With the U.S. economy the furthest along its business cycle, the level of interest rates in the U.S. and Europe began to
diverge—the 10-year U.S. Treasury yield was 2.35% at the end of October 2014 compared with 0.85% for 10-year German bund yields, 150 basis points higher. This is largely attributed to the Fed being closer to eventually increasing interest rates
while the ECB potentially needs to further increase rather than scale back its amount of accommodation. Further, U.S. economic activity is healthier than Europe’s. 
Within emerging markets, declining commodity prices contributed to weak growth in export-oriented countries. Russia was hurt by sanctions due to its involvement in the
Ukraine crisis. China faced slowing growth and peaceful but widespread political dissent in Hong Kong over the process for selecting political candidates. Meanwhile, Brazilian president Dilma Rousseff secured her re-election by a narrow margin, and
many investors expected Brazil’s disappointing economic policies to continue. 
 
 
 
Table of Contents
 
While yields have been at historically low levels, there is a risk of rising rates and negative bond returns. However,
fixed-income markets appear to have functioned well over the past year with sufficient liquidity and muted volatility. 
Don’t let
short-term uncertainty derail long-term investment goals. 
Wells Fargo Advantage Funds
Wells Fargo Advantage Funds
Sincerely, 


Dear Valued Shareholder: 
Wells Fargo Advantage International Bond Fund 
Major central banks continued to provide stimulus. 
%.
Economic growth was sluggish outside the U.S. 
The Fed has a dual mandate to maximize employment and keep prices stable, and economic activity during the period showed improvement on both these fronts. The
unemployment rate ticked lower over the course of the reporting period, reaching 5.8% as of October 2014. More than 200,000 jobs were added to payrolls each month between February and October 2014. On the inflation front, the personal consumption
expenditures price index rose from a deflationary danger zone toward the Fed’s longer-run objective of a 2% pace. 
Elsewhere, economic data continued to show
sluggish growth. The European Union’s gross domestic product rose 0.2% in the third quarter of 2014 from the preceding quarter. Within the eurozone’s report on economic activity, Germany managed to avoid a recession and Greece showed two
consecutive quarters of growth. The region’s unemployment rate remained high at 11.5% in September 2014. Japan’s economy faltered in reaction to a sales-tax increase despite Prime Minister Abe’s economic plans to promote growth. 
With the U.S. economy the furthest along its business cycle, the level of interest rates in the U.S. and Europe began to diverge—the 10-year U.S. Treasury yield
was 2.35% at the end of October 2014 compared with 0.85% for 10-year German bund yields, 150 basis points higher (100 basis points=1%). This is largely attributed to the Fed being closer to eventually increasing interest rates while the ECB
potentially needs to further increase rather than scale back its amount of accommodation. Furthermore, U.S. economic activity is healthier than Europe’s. 
Within emerging markets, declining commodity prices contributed to weak growth in export-oriented countries. Russia was hurt by sanctions due to its involvement in the
Ukraine crisis. China faced slowing growth and peaceful but 
widespread political dissent in Hong Kong over the process for selecting political candidates.
Meanwhile, Brazilian president Dilma Rousseff secured her reelection by a narrow margin, and many investors expected Brazil’s disappointing economic policies to continue.

Table of Contents
 
While yields have been at historically low levels, there is a risk of rising rates and negative bond returns. However,
fixed-income markets appear to have functioned well over the past year with sufficient liquidity and muted volatility. 
Don’t let
short-term uncertainty derail long-term investment goals. 
Wells Fargo Advantage Funds
Wells Fargo Advantage Funds
Sincerely, 


Dear Valued Shareholder: 
Wells Fargo Advantage Strategic Income Fund
Major central banks continued to provide
stimulus. 
%.
Economic growth was sluggish outside the U.S. 
The Fed has a dual mandate to maximize employment and keep prices stable, and economic activity during the period showed improvement on both these fronts. The
unemployment rate ticked lower over the course of the reporting period, reaching 5.8% as of October 2014. More than 200,000 jobs were added to payrolls each month between February and October 2014. On the inflation front, the personal consumption
expenditures price index rose from a deflationary danger zone toward the Fed’s longer-run objective of a 2% pace. 
Elsewhere, economic data continued to show
sluggish growth. The European Union’s gross domestic product rose 0.2% in the third quarter of 2014 from the preceding quarter. Within the eurozone’s report on economic activity, Germany managed to avoid a recession and Greece showed two
consecutive quarters of growth. The region’s unemployment rate remained high at 11.5% in September 2014. Japan’s economy faltered in reaction to a sales-tax increase despite Prime Minister Abe’s economic plans to promote growth. 
With the U.S. economy the furthest along its business cycle, the level of interest rates in the U.S. and Europe began to diverge—the 10-year U.S. Treasury yield
was 2.35% at the end of October 2014 compared with 0.85% for 10-year German bund yields, 150 basis points higher. This is largely attributed to the Fed being closer to eventually increasing interest rates while the ECB potentially needs to further
increase rather than scale back its amount of accommodation. Further, U.S. economic activity is healthier than Europe’s. 
Within emerging markets, declining
commodity prices contributed to weak growth in export-oriented countries. Russia was hurt by sanctions due to its involvement in the Ukraine crisis. China faced slowing growth and peaceful but widespread political dissent in Hong Kong over the
process for selecting political candidates. Meanwhile, Brazilian president Dilma Rousseff secured her reelection by a narrow margin, and many investors expected Brazil’s disappointing economic policies to continue.

Table of Contents
 
While yields have been at historically low levels, there is a risk of rising rates and negative bond returns. However,
fixed-income markets appear to have functioned well over the past year with sufficient liquidity and muted volatility. 
Don’t let
short-term uncertainty derail long-term investment goals. 
Wells Fargo Advantage Funds
Wells Fargo Advantage Funds
Sincerely, 


Dear Valued Shareholder: 
Wells Fargo Advantage Asia Pacific Fund
Central banks continued to provide liquidity to
the markets. 
Throughout the reporting period, the Bank of Japan (BOJ) continued to engage in significant quantitative easing with the goal of achieving
2% annual inflation. The Federal Open Market Committee (FOMC)—the Fed’s monetary policymaking body—provided further global liquidity by keeping its interest rate near zero. In January 2014, the FOMC began to reduce (or taper) its
bond-buying program, bringing it to an end in late October 2014. The FOMC’s guidance led investors to expect an interest-rate hike sometime in 2015. The prospect of higher U.S. interest rates caused the dollar to rise relative to most global
currencies and pressured international returns for U.S. dollar-based investors. 
Against the backdrop of a challenging geopolitical
situation, Asian markets collectively posted a modest gain. 
Geopolitical events were major obstacles throughout the reporting period. The ongoing
standoff between the West (the U.S., Europe, and their allies) and Russia over Ukraine began in late 2013 and continued throughout the reporting period. As of October 2014, most investors believed that the situation would not result in a war.
However, economic sanctions from the West against Russia and from Russia against the West contributed to slower growth in Europe, a key Russian trading partner. In the Middle East, the advance of Islamic militants in Iraq and Syria led to airstrikes
by the U.S. and its allies and fears of a prolonged regional war. 
We believe much of the return in Asian markets over the past 12 months was driven by factors such
as weaker economic growth in China and in Europe, which contains key end export markets for many of the goods produced by companies domiciled in Asia. Shifting views on the possible impact of the FOMC’s ending its quantitative easing program
also played a role. In our view, a number of election campaigns in Asian countries added to an overall choppy economic environment. Notable elections included India’s general election from April to May 2014, which resulted in the appointment of
a reform-minded prime minister, and Indonesia’s general election in July 2014, which resulted in the first president to come from outside of the country’s traditional elite. 
The MSCI All Country Asia Pacific Index (Net) ended the reporting period with a single-digit positive return, reflecting the market’s crosscurrents. Among
individual markets, the Japanese market lagged despite continued monetary easing from the BOJ. Prime Minister Shinzô Abe and his government continued

 
 
Table of Contents
 

their attempts to implement reforms and to spur growth through a quantitative stimulus package, but getting consumers to spend remained a struggle. The South Korean stock market also
underperformed as the country continued to struggle with sluggish economic growth. 
India was a top performer based on investor optimism that Prime Minister
Narendra Modi would be able to create the conditions for higher economic growth. China outperformed the index, likely due to is strong performance in the first half of 2014. Even though China had earlier shown signs of slowing economic growth,
investors initially hoped that a mini-stimulus package would produce a soft landing. Those hopes faded toward the end of the period as China produced lower-than-expected economic results. Indonesia also outperformed as the country successfully
transferred power from one popularly elected president to another, solidifying a break from its authoritarian past. 
Don’t let
short-term uncertainty derail long-term investment goals. 
Wells Fargo Advantage Funds 
Wells Fargo Advantage Funds
Sincerely, 


Dear Valued Shareholder: 
Wells Fargo Advantage Diversified International Fund
Major central banks continued to provide liquidity to the
markets. 
Throughout the reporting period, the Federal Open Market Committee (FOMC)—the Fed’s monetary policymaking body—kept its key
interest rate near zero. This tended to support global markets given the Fed’s role in providing investor liquidity. In January 2014, the FOMC began to reduce (or taper) its bond-buying program by $10 billion per month and brought the program
to an end in late October 2014. Although the FOMC kept its key interest rate near zero, its guidance led investors to expect an interest-rate hike sometime in 2015. 
In June 2014, the European Central Bank (ECB) announced a variety of measures aimed at encouraging lending, including making funds available to banks at low interest
rates and imposing a negative interest rate on bank deposits held at the central bank. In September, the ECB cut its key rate to a historic low of 0.05% and increased its negative interest rate. In Japan, the Bank of Japan (BoJ) maintained an
aggressive monetary program aimed at combating deflation by doubling the bank’s asset base over two years; on the last day of the reporting period, the BoJ surprised observers by increasing its asset purchases. 
The backdrop of a challenging geopolitical situation and slowing global growth caused significant volatility. 
Geopolitical events were major obstacles throughout the reporting period. The ongoing standoff between the West (the U.S., Europe, and their allies) and Russia over
Ukraine began in late 2013 and continued throughout the reporting period. As of October 2014, most investors believed that the situation would not result in a war. However, economic sanctions from the West against Russia and from Russia against the
West contributed to slower growth in Europe, a key Russian trading partner. In the Middle East, the advance of Islamic militants in Iraq and Syria led to airstrikes by the U.S. and its allies and fears of a prolonged regional war. 
Market volatility increased toward the end of the reporting period as renewed fears of slowing global growth took hold and investors began to price in expectations that
the FOMC was finally looking to raise short-tem interest rates. 
 
 
 
 
Table of Contents
 

Economic news out of Europe trended lower for most of 2014. Partly as a result of slower European growth, the euro fell versus the dollar during the period, pressuring returns for U.S.
dollar-denominated investors. Returns in Asia were dampened by a sluggish Japanese economy as well as a late-period decline in the yen. Among countries in the MSCI EAFE, most of the major markets—such as Japan, Germany, and France—ended
the period with losses. By contrast, U.S. economic news remained favorable; gross domestic product annualized growth was at least 3.5% in three of the past four quarters, pushing the U.S. stock market to a solid gain. 
Don’t let short-term uncertainty derail long-term investment goals. 
Wells Fargo Advantage Funds 
Wells Fargo Advantage Funds
Sincerely, 


Dear Valued Shareholder: 
Wells Fargo Advantage Emerging Markets Equity Fund
The Fed continued to provide
liquidity to the markets. 
Throughout the reporting period, the Federal Open Market Committee (FOMC)—the Fed’s monetary policymaking
body—kept its key interest rate near zero. This tended to support global markets given the Fed’s role in providing investor liquidity. In January 2014, the FOMC began to reduce (or taper) its bond-buying program by $10 billion per month
and brought the program to an end in late October 2014. Although the FOMC kept its key interest rate near zero, its guidance led investors to expect an interest-rate hike sometime in 2015. The prospect of higher U.S. interest rates caused the dollar
to rise relative to most global currencies and pressured emerging markets returns for U.S. dollar-based investors. 
Against the backdrop
of a challenging geopolitical situation, emerging markets displayed considerable volatility. 
Geopolitical events were major obstacles throughout the
reporting period. The ongoing standoff between the West (the U.S., Europe, and their allies) and Russia over Ukraine began in late 2013 and continued throughout the reporting period. As of October 2014, most investors believed that the situation
would not result in a war. However, economic sanctions from the West against Russia and from Russia against the West contributed to slower growth in Europe, a key Russian trading partner. In the Middle East, the advance of Islamic militants in Iraq
and Syria led to airstrikes by the U.S. and its allies and fears of a prolonged regional war. 
Emerging markets largely traded within a range over the past 12
months but displayed considerable volatility within that range. Much of the volatility was driven by factors such as weaker economic growth in China and in Europe, which contains key end export markets for many of the goods produced by companies
domiciled in emerging economies. Shifting views on the possible impact of the FOMC’s ending its quantitative easing program also played a role. A number of election campaigns in emerging countries added to an overall choppy economic
environment. Notable elections included India’s general election from April to May 2014, which resulted in the appointment of a reform-minded prime minister, and Brazil’s general election in October 2014, which resulted in the narrow
re-election of the incumbent. 
The MSCI Emerging Markets Index (Net) ended the reporting period with a barely positive return, reflecting the market’s
crosscurrents. Among individual markets, India was a top performer on investor optimism that Prime Minister Narendra Modi

 
 
Table of Contents
 

would be able to create the conditions for higher economic growth. China outperformed the index, in large part, because of strong performance in the first half of 2014. Even though China had
earlier shown signs of slowing economic growth, investors initially hoped that a mini-stimulus package would produce a soft landing. Those hopes faded toward the end of the period as China produced lower-than-expected economic results. Russia posted
sharply negative returns as a result of its standoff with the West; the tense situation also negatively affected eastern European markets, such as Hungary, Poland, and the Czech Republic. Brazil also underperformed, especially toward the end of the
reporting period as it became likely that the incumbent president, Dilma Rousseff, would win reelection. Many investors disliked aspects of her economic policies and had favored a change in office. 
Don’t let short-term uncertainty derail long-term investment goals. 
Wells Fargo Advantage Funds
Wells Fargo Advantage Funds
Sincerely, 


Dear Valued Shareholder: 
Wells Fargo Advantage Emerging Markets Equity Income Fund
The Fed continued to provide
liquidity to the markets. 
bond-buying
Against the backdrop of a challenging geopolitical situation, emerging markets displayed considerable volatility. 
Geopolitical events were major obstacles throughout the reporting period. The ongoing standoff between the West (the U.S., Europe, and their allies) and Russia over
Ukraine began in late 2013 and continued throughout the reporting period. As of October 2014, most investors believed that the situation would not result in a war. However, economic sanctions from the West against Russia and from Russia against the
West contributed to slower growth in Europe, a key Russian trading partner. In the Middle East, the advance of Islamic militants in Iraq and Syria led to airstrikes by the U.S. and its allies and fears of a prolonged regional war. 
Emerging markets largely traded within a range over the past 12 months but displayed considerable volatility within that range. Much of the volatility was driven by
factors such as weaker economic growth in China and in Europe, which contains key end export markets for many of the goods produced by companies domiciled in emerging economies. Shifting views on the possible impact of the FOMC’s ending its
quantitative easing program also played a role. A number of election campaigns in emerging countries added to an overall choppy economic environment. Notable elections included India’s general election from April to May 2014, which resulted in
the appointment of a reform-minded prime minister, and Brazil’s general election in October 2014, which resulted in the narrow reelection of the incumbent. 
The MSCI Emerging Markets Index (Net) ended the reporting period with a barely positive return, reflecting the market’s crosscurrents. Among individual markets,

 
 
 
 
Table of Contents
 

India was a top performer on investor optimism that Prime Minister Narendra Modi would be able to create the conditions for higher economic growth. China outperformed the index, in large part,
because of strong performance in the first half of 2014. Even though China had earlier shown signs of slowing economic growth, investors initially hoped that a mini-stimulus package would produce a soft landing. Those hopes faded toward the end of
the period as China produced lower-than-expected economic results. Russia posted sharply negative returns as a result of its standoff with the West; the tense situation also negatively affected eastern European markets, such as Hungary, Poland, and
the Czech Republic. Brazil also underperformed, especially toward the end of the reporting period as it became likely that the incumbent president, Dilma Rousseff, would win reelection. Many investors disliked aspects of her economic policies and
had favored a change in office. 
Don’t let short-term uncertainty derail long-term investment goals. 
Wells Fargo Advantage Funds 
Wells Fargo Advantage Funds
Sincerely, 


Dear Valued Shareholder: 
Wells Fargo Advantage Emerging Markets Equity 
The Fed continued to provide liquidity to the markets. 
bond-buying
Against the backdrop of a
challenging geopolitical situation, emerging markets displayed considerable volatility. 
Geopolitical events were major obstacles throughout the reporting
period. The ongoing standoff between the West (the U.S., Europe, and their allies) and Russia over Ukraine began in late 2013 and continued throughout the reporting period. As of October 2014, most investors believed that the situation would not
result in a war. However, economic sanctions from the West against Russia and from Russia against the West contributed to slower growth in Europe, a key Russian trading partner. In the Middle East, the advance of Islamic militants in Iraq and Syria
led to airstrikes by the U.S. and its allies and fears of a prolonged regional war. 
re-election
The MSCI Emerging Markets Index (Net) ended the reporting period with a barely positive return,
reflecting the market’s crosscurrents. Among individual markets, 
 
 
 
 
Table of Contents
 
re-election.
Don’t let short-term uncertainty derail long-term
investment goals. 
Wells Fargo Advantage Funds 
Wells Fargo Advantage Funds
Sincerely, 


Dear Valued Shareholder: 
Wells Fargo Advantage 
Major central banks continued to provide liquidity to the markets. 
Throughout the reporting period, the Federal Open Market Committee
(FOMC)—the Fed’s monetary policymaking body—kept its key interest rate near zero. This tended to support global markets given the Fed’s role in providing investor liquidity. In January 2014, the FOMC began to reduce (or taper)
its bond-buying program by $10 billion per month and brought the program to an end in late October 2014. Although the FOMC kept its key interest rate near zero, its guidance led investors to expect an interest-rate hike sometime in 2015. 
In June 2014, the European Central Bank (ECB) announced a variety of measures aimed at encouraging lending, including making funds available to banks at low interest
rates and imposing a negative interest rate on bank deposits held at the central bank. In September, the ECB cut its key rate to a historic low of 0.05% and increased its negative interest rate. In Japan, the Bank of Japan (BoJ) maintained an
aggressive monetary program aimed at combating deflation by doubling the bank’s asset base over two years; on the last day of the reporting period, the BoJ surprised observers by increasing its asset purchases. 
The backdrop of a challenging geopolitical situation and slowing global growth caused significant volatility. 
Geopolitical events were major obstacles throughout the reporting period. The ongoing standoff between the West (the U.S., Europe, and their allies) and Russia over
Ukraine began in late 2013 and continued throughout the reporting period. As of October 2014, most investors believed that the situation would not result in a war. However, economic sanctions from the West against Russia and from Russia against the
West contributed to slower growth in Europe, a key Russian trading partner. In the Middle East, the advance of Islamic militants in Iraq and Syria led to airstrikes by the U.S. and its allies and fears of a prolonged regional war. 
Market volatility increased toward the end of the reporting period as renewed fears of slowing global growth took hold and investors began to price in expectations that
the FOMC was finally looking to raise short-tem interest rates. Economic news out of Europe trended lower for most of 2014. Partly as a result of slower European growth, the euro fell versus the dollar during the period, pressuring returns for U.S.
dollar-denominated investors. Returns in Asia were dampened by a sluggish Japanese economy as well as a late-period decline in the yen. Among countries in the MSCI EAFE, most of the major markets—such as

 
 
 
Table of Contents
 

Japan, Germany, and France—ended the period with losses. By contrast, U.S. economic news remained favorable; gross domestic product annualized growth was at least 3.5% in three of the past
four quarters, pushing the U.S. stock market to a solid gain. 
Don’t let short-term uncertainty derail long-term investment goals.

Wells Fargo Advantage Funds 
Wells Fargo Advantage
Funds
Sincerely, 


Dear Valued Shareholder: 
Wells Fargo Advantage International 
Major central banks continued to provide liquidity to the markets. 
Throughout the reporting period, the Federal Open Market Committee
(FOMC)—the Fed’s monetary policymaking body—kept its key interest rate near zero. This tended to support global markets given the Fed’s role in providing investor liquidity. In January 2014, the FOMC began to reduce (or taper)
its bond-buying program by $10 billion per month and brought the program to an end in late October 2014. Although the FOMC kept its key interest rate near zero, its guidance led investors to expect an interest-rate hike sometime in 2015. 
In June 2014, the European Central Bank (ECB) announced a variety of measures aimed at encouraging lending, including making funds available to banks at low interest
rates and imposing a negative interest rate on bank deposits held at the central bank. In September, the ECB cut its key rate to a historic low of 0.05% and increased its negative interest rate. In Japan, the Bank of Japan (BoJ) maintained an
aggressive monetary program aimed at combating deflation by doubling the bank’s asset base over two years; on the last day of the reporting period, the BoJ surprised observers by increasing its asset purchases. 
The backdrop of a challenging geopolitical situation and slowing global growth caused significant volatility. 
Geopolitical events were major obstacles throughout the reporting period. The ongoing standoff between the West (the U.S., Europe, and their allies) and Russia over
Ukraine began in late 2013 and continued throughout the reporting period. As of October 2014, most investors believed that the situation would not result in a war. However, economic sanctions from the West against Russia and from Russia against the
West contributed to slower growth in Europe, a key Russian trading partner. In the Middle East, the advance of Islamic militants in Iraq and Syria led to airstrikes by the U.S. and its allies and fears of a prolonged regional war.

 
 
 
 
Table of Contents
 
Market volatility increased toward the end of the reporting period as renewed fears of slowing global growth took hold and
investors began to price in expectations that the FOMC was finally looking to raise short-term interest rates. Economic news out of Europe trended lower for most of 2014. Partly as a result of slower European growth, the euro fell versus the dollar
during the period, pressuring returns for U.S. dollar-denominated investors. Returns in Asia were dampened by a sluggish Japanese economy as well as a late-period decline in the yen. Among countries in the MSCI EAFE, most of the major
markets—such as Japan, Germany, and France—ended the period with losses. By contrast, U.S. economic news remained favorable; gross domestic product annualized growth was at least 3.5% in three of the past four quarters, pushing the U.S.
stock market to a solid gain. 
Don’t let short-term uncertainty derail long-term investment goals. 
Wells Fargo Advantage Funds 
Wells Fargo Advantage Funds
Sincerely, 


Dear Valued Shareholder: 
Wells Fargo Advantage Intrinsic World Equity Fund
Major central banks continued to provide liquidity to the markets.

bond-buying
In June 2014, the European Central Bank (ECB) announced a variety of measures aimed at encouraging lending, including making funds available to banks at low interest
rates and imposing a negative interest rate on bank deposits held at the central bank. In September, the ECB cut its key rate to a historic low of 0.05% and increased its negative interest rate. In Japan, the Bank of Japan (BoJ) maintained an
aggressive monetary program aimed at combating deflation by doubling the bank’s asset base over two years; on the last day of the reporting period, the BoJ surprised observers by increasing its asset purchases. 
The backdrop of a challenging geopolitical situation and slowing global growth caused significant volatility. 
Geopolitical events were major obstacles throughout the reporting period. The ongoing standoff between the West (the U.S., Europe, and their allies) and Russia over
Ukraine began in late 2013 and continued throughout the reporting period. As of October 2014, most investors believed that the situation would not result in a war. However, economic sanctions from the West against Russia and from Russia against the
West contributed to slower growth in Europe, a key Russian trading partner. In the Middle East, the advance of Islamic militants in Iraq and Syria led to airstrikes by the U.S. and its allies and fears of a prolonged regional war. 
Market volatility increased toward the end of the reporting period as renewed fears of slowing global growth took hold and investors began to price in expectations that
the FOMC was finally looking to raise short-term interest rates. 
 
 
 
 
Table of Contents
 

Economic news out of Europe trended lower for most of 2014. Partly as a result of slower European growth, the euro fell versus the dollar during the period, pressuring returns for U.S.
dollar-denominated investors. Returns in Asia were dampened by a sluggish Japanese economy as well as a late-period decline in the yen. Among countries in the MSCI EAFE, most of the major markets—such as Japan, Germany, and France—ended
the period with losses. By contrast, U.S. economic news remained favorable; gross domestic product annualized growth was at least 3.5% in three of the past four quarters, pushing the U.S. stock market to a solid gain. 
Don’t let short-term uncertainty derail long-term investment goals. 
Wells Fargo Advantage Funds 
Wells Fargo Advantage Funds
Sincerely, 


