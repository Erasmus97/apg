Dear Fellow
Shareholders,
 
We are
pleased to report that the SignalPoint Global Alpha Fund (“Fund”) has performed to our expectations for the fiscal
year ended September 30, 2014. While maintaining a high percentage of cash reserve throughout the year the Fund’s performance
correlated well with the MSCI World Index. Additionally, the cash reserve level closely matched SignalPoint’s Market Risk
Indicator (MRI) thereby maintaining a modestly defensive posture appropriate for perceived market risk.
 
Investment
Strategy 
 
The Fund’s
investment strategy is designed to provide broad exposure to global markets through owning regional and sector based exchange-traded
funds (ETFs). The equity exposure is counterbalanced with a reserve of cash for each region and sector ETF, with each ETF position
functioning as its own profit center. As any sector/regional ETF position rises in price, SignalPoint will typically sell off
a small percentage of that ETF position and reserve the cash proceeds for repurchasing shares at a later point in time when shares
are trading at a discount. Cash levels rise to a calculated maximum that roughly equates to our opinion of the level of market
risk.
 
Market
Risk Indicator (MRI) 
 
SignalPoint’s
proprietary Market Risk Indicator (MRI) is used as a barometer for gaining realistic insight into the stock market’s level
of risk. The MRI evaluates market valuations, speculative activity and investor sentiment and correlative activity. We also broadly
review the expansion/contraction of the number of available issues from which investors can choose. The MRI is a summary of these
measures that is scaled to produce our target maximum cash reserve for the Fund based upon our view of market risk.
 
Fiscal
Year 2014 Performance 
 
As fiscal
year 2014 began exposure to the various U.S. and international equity sector ETF positions had shifted slightly favoring the U.S.
holdings as these ETFs outperformed the international ETFs. The trend continued as the last twelve months progressed. As of September
30, 2014 the international ETFs now represents about 35% of total equity exposure or approximately 24% of the Fund. The U.S. ETFs
have risen to 65% of the overall equity exposure and 43% of the Fund. The remaining Fund holding is the cash reserves which has
risen to approximately 33% of Fund assets. The cash allocation has risen approximately 7% over the 12 month period, primarily
from trimming selected U.S. ETF holdings that had appreciated in price/share during the year.
 
The
ETF holdings reduced during the last twelve months included U.S. based Basic Materials, Utilities, Industrials, Financials, Energy,
Information Technology, Healthcare, Consumer Staples and Consumer Discretionary. International ETF reductions occurred in the
Asia (ex-Japan) and the Euro Economic Zone. By contrast, the only purchase or increase in an ETF holding occurred in the Latin
America ETF.
 
Appreciation
in the Fund’s ETF positions also closely tracked the global equity market’s growth throughout the period. As the Fund’s
equity values rose the cash reserves were kept relatively constant
 
4411-NLD-10/22/2014
