Dear Shareholders, 
Continued Economic Recovery 
The U.S. economy improved along with a
warm-up in the weather during the second quarter of 2014. Economic data generally beat expectations with positive news from major market sectors. Strong hiring brought unemployment down to 6.1%. Manufacturing activity remained robust. Personal
income growth was healthy with accelerated wage gains, an increased savings rate of 4.8% and a 35-year low in household debt as a percentage of income. After a slow start, the housing market showed renewed strength. Sales rose and pending sales, a
key indicator of future activity, rose strongly near the end of the quarter. Consumer spending was somewhat lackluster, but consumer confidence rose to a post-recession high. 
Against this backdrop, the Federal Reserve (the Fed) maintained a steady hand, while central banks outside of the United States took steps to stimulate growth. Investors were the beneficiaries of a stable
environment and accommodative monetary policy. Equity markets stabilized at high levels with the S&amp;P 500 Index setting a new record high above 1900. Every major asset class delivered gains for the period. All 10 sectors within the S&amp;P
500 Index delivered gains, led by energy, utilities and technology. Large- and mid-cap stocks outperformed small-cap stocks by a considerable margin. 
Interest rates moved lower, resulting in positive returns for most domestic fixed-income sectors. The Barclays U.S. Aggregate Bond Index returned 2.04%, while mortgage and securitized bonds,
investment-grade and high-yield corporate and municipal bonds returned around 2.50% for the second quarter of 2014. U.S. convertible bonds and Treasury Inflation Protected Securities generated somewhat stronger returns. Emerging-market bonds were
the period’s best fixed-income performers, with returns in line with U.S. equities. 
Stay on Track with Columbia Management

Backed by more than 100 years of experience, Columbia Management is one of the nation’s largest asset managers. At the heart of our
success and, most importantly, that of our investors, are highly talented industry professionals, brought together by a unique way of working. At Columbia Management, reaching our performance goals matters, and how we reach them matters just as
much. 
Visit columbiamanagement.com for: 
 
 
Detailed up-to-date fund performance and portfolio information 
 
Quarterly fund commentaries 
 
Thank you for your continued support of the Columbia Funds. We look forward to serving your investment needs for many years to come. 
Best Regards, 


Dear Shareholders, 
Continued Economic Recovery 
The U.S. economy improved along with a
warm-up in the weather during the second quarter of 2014. Economic data generally beat expectations with positive news from major market sectors. Strong hiring brought unemployment down to 6.1%. Manufacturing activity remained robust. Personal
income growth was healthy with accelerated wage gains, an increased savings rate of 4.8% and a 35-year low in household debt as a percentage of income. After a slow start, the housing market showed renewed strength. Sales rose and pending sales, a
key indicator of future activity, rose strongly near the end of the quarter. Consumer spending was somewhat lackluster, but consumer confidence rose to a post-recession high. 
Against this backdrop, the Federal Reserve (the Fed) maintained a steady hand, while central banks outside of the United States took steps to stimulate growth. Investors were the beneficiaries of a stable
environment and accommodative monetary policy. Equity markets stabilized at high levels with the S&amp;P 500 Index setting a new record high above 1900. Every major asset class delivered gains for the period. All 10 sectors within the S&amp;P 500
Index delivered gains, led by energy, utilities and technology. Large- and mid-cap stocks outperformed small-cap stocks by a considerable margin. 
Interest rates moved lower, resulting in positive returns for most domestic fixed-income sectors. The Barclays U.S. Aggregate Bond Index returned 2.04%, while mortgage and securitized bonds,
investment-grade and high-yield corporate and municipal bonds returned around 2.50% for the second quarter of 2014. U.S. convertible bonds and Treasury Inflation Protected Securities generated somewhat stronger returns. Emerging-market bonds were
the period’s best fixed-income performers, with returns in line with U.S. equities. 
Stay on Track with Columbia Management

Backed by more than 100 years of experience, Columbia Management is one of the nation’s largest asset managers. At the heart of our
success and, most importantly, that of our investors, are highly talented industry professionals, brought together by a unique way of working. At Columbia Management, reaching our performance goals matters, and how we reach them matters just as
much. 
Visit columbiamanagement.com for: 
 
 
Detailed up-to-date fund performance and portfolio information 
 
Quarterly fund commentaries 
 
Thank you for your continued support of the Columbia Funds. We look forward to serving your investment needs for many years to come. 
Best Regards, 


Dear Shareholders, 
Continued Economic Recovery 
The U.S. economy improved along with a
warm-up in the weather during the second quarter of 2014. Economic data generally beat expectations with positive news from major market sectors. Strong hiring brought unemployment down to 6.1%. Manufacturing activity remained robust. Personal
income growth was healthy with accelerated wage gains, an increased savings rate of 4.8% and a 35-year low in household debt as a percentage of income. After a slow start, the housing market showed renewed strength. Sales rose and pending sales, a
key indicator of future activity, rose strongly near the end of the quarter. Consumer spending was somewhat lackluster, but consumer confidence rose to a post-recession high. 
Against this backdrop, the Federal Reserve (the Fed) maintained a steady hand, while central banks outside of the United States took steps to stimulate growth. Investors were the beneficiaries of a stable
environment and accommodative monetary policy. Equity markets stabilized at high levels with the S&amp;P 500 Index setting a new record high above 1900. Every major asset class delivered gains for the period. All 10 sectors within the S&amp;P 500
Index delivered gains, led by energy, utilities and technology. Large- and mid-cap stocks outperformed small-cap stocks by a considerable margin. 
Interest rates moved lower, resulting in positive returns for most domestic fixed-income sectors. The Barclays U.S. Aggregate Bond Index returned 2.04%, while mortgage and securitized bonds,
investment-grade and high-yield corporate and municipal bonds returned around 2.50% for the second quarter of 2014. U.S. convertible bonds and Treasury Inflation Protected Securities generated somewhat stronger returns. Emerging-market bonds were
the period’s best fixed-income performers, with returns in line with U.S. equities. 
Stay on Track with Columbia Management

Backed by more than 100 years of experience, Columbia Management is one of the nation’s largest asset managers. At the heart of our
success and, most importantly, that of our investors, are highly talented industry professionals, brought together by a unique way of working. At Columbia Management, reaching our performance goals matters, and how we reach them matters just as
much. 
Visit columbiamanagement.com for: 
 
 
Detailed up-to-date fund performance and portfolio information 
 
Quarterly fund commentaries 
 
Thank you for your continued support of the Columbia Funds. We look forward to serving your investment needs for many years to come. 
Best Regards, 


