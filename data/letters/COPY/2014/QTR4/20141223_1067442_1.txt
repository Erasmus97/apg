Dear Shareholder:
We hope you find the annual report for the Prudential Income Builder Fund, formerly the Target Conservative Allocation Fund, informative and useful. The report covers performance for the period that ended
On Quantitative Management Associates LLC, Jennison Associates LLC, Prudential Fixed Income, and Prudential Real Estate Investors became the Funds new investment subadvisers. Since the Fund was
repositioned later in the reporting period, its performance is primarily based on the investment strategies of its previous subadvisers.
As always, Prudential
Investments&reg; is dedicated to helping you solve your toughest investment challengeswhether its capital growth,
reliable income, or protection from market volatility and other risks. We offer the expertise of Prudential Financials affiliated asset managers* that strive to be leaders in a broad range of funds to help you stay on course to the future you
envision. They also manage money for major corporations and pension funds around the world, which means you benefit from the same expertise, innovation, and attention to risk demanded by todays most sophisticated investors.
Thank you for choosing the Prudential Investments family of funds.
Sincerely,

Dear Shareholder:
We hope you find the annual report for the Prudential Defensive Equity Fund informative and useful. The report covers performance for the period that ended October Please note the Fund has
changed its year-end fiscal date from July to October Going forward, the Funds annual and semi-annual reports will be based on the new fiscal year-end cycle, and will not affect the management or operation of the Fund.
Since market conditions change over time, we believe it is important to maintain a
diversified portfolio of funds consistent with your tolerance for risk, time horizon, and financial goals.
Your financial advisor can help you create a diversified investment plan that may include funds covering all the basic asset classes and that reflects your personal investor profile and risk tolerance. Keep in
mind, however, that diversification and asset allocation strategies do not assure a profit or protect against loss in decliningmarkets.
Prudential
Investments&reg; is dedicated to helping you solve your toughest investment challengeswhether its capital growth,
reliable income, or protection from market volatility and other risks. We offer the expertise of Prudential Financials affiliated asset managers* that strive to be leaders in a broad range of funds to help you stay on course to the future you
envision. They also manage money for major corporations and pension funds around the world, which means you benefit from the same expertise, innovation, and attention to risk demanded by todays most sophisticated investors.
Thank you for choosing the Prudential Investments family of funds.
Sincerely,
