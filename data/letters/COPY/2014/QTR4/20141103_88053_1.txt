Dear Shareholder:



I am very pleased to tell you that the DWS funds have been renamed Deutsche funds, aligning more closely with the Deutsche Asset &amp; Wealth Management brand. We are proud to adopt the Deutsche name a brand that fully represents the global access, discipline and intelligence that support all of our products and services.



Deutsche Asset &amp; Wealth Management combines the asset management and wealth management divisions of Deutsche Bank to deliver a comprehensive suite of active, passive and alternative investment capabilities. Your investment in the Deutsche funds means you have access to the thought leadership and resources of one of the worlds largest and most influential financial institutions.



In conjunction with your funds name change, please note that the Deutsche funds Web address has changed as well. The former dws-investments.com is now deutschefunds.com.



In addition, key service providers have been renamedas follows:





Former Name


New name, effective August



DWS Investments Distributors, Inc.


DeAWM Distributors, Inc.



DWS Trust Company


DeAWM Trust Company



DWS Investments Service Company


DeAWM Service Company






These changes have no effect on the day-to-day management of your investment, and there is no action required on your part. You will continue to experience the benefits that come from our decades of experience, in-depth research and worldwide network of investment professionals.



Thanks for your continued support. We appreciate your trust and the opportunity to put our capabilities to work for you.



Best regards,
