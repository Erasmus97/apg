Dear Shareholder: 
With a backdrop of continued accommodative policies among the world’s central banks and a trend toward low growth, low inflation and low volatility, global financial markets stabilized and provided
positive returns for the twelve months ended August 31, 2014. In the U.S., low borrowing costs, a surge in corporate mergers and healthy corporate earnings drove U.S. equity indices to successive record highs during the latter half of the
period. Fixed income markets struggled early on but then swung to gains as long-term interest rates declined in 2014 and demand for debt securities outpaced supply. Bond yields, which generally move in the opposite direction of prices, tumbled lower
over the second half of the twelve month period and yields on longer maturity U.S. Treasury securities reached their lowest levels in more than a year. From May through June, market volatility retreated to lows not consistently seen since 2007,
before spiking in July on geopolitical tensions, and then retreating again at the end of August. The Barclays U.S. Aggregate Index returned 5.66% for the twelve month period. The Standard &amp; Poor’s 500 Index put an exclamation point on
the twelve months by breaching the 2,000-point level for the first time and closing at a record high 2003.37 points on August 29, 2014. 
 
