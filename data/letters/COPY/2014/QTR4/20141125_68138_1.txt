Dear Shareholder,
As you know, the Morgan Growth Fund invests in both mid- and large-capitalization growth stocks. In the period under review, that middle-market exposure pulled down the fund’s results a bit as large-cap growth stocks outpaced their smaller counterparts. Of course, the positions of mid- and large-cap stocks are bound to switch from time to time. Market leadership changes in unpredictable ways, and that’s one of the reasons that Vanguard believes broad diversification is the wisest choice for many investors.
If you hold shares in a taxable account, you may wish to review the information about after-tax returns, based on the highest federal income tax bracket, that appears later in the report.
Please note that as of September 30, 2014, the fund had realized short-term capital gains equal to about 1% of assets and long-term gains accounting for about 8% of fund assets. Gains are distributed in December.
2
Since January, the Fed has pared back its bond-buying program, with the aim of ending it in October. Until recently, interest rates did not rise as analysts had predicted. The yield of the 10-year U.S. Treasury note
 
3
ended September at 2.48%, down from 2.63% a year earlier. (Bond prices and yields move in opposite directions.)
Municipal bonds, which returned 7.93%, benefited from the broad market rally and a limited supply of new issues.
Following this advance for U.S. taxable and tax-exempt bonds, it’s worth remembering that the current low yields imply lower future returns: As yields drop, the scope for further declines—and increases in prices—diminishes.
International bond markets (as measured by the Barclays Global Aggregate Index ex USD) returned –0.81% after sinking in September.
Money market funds and savings accounts posted negligible returns, as the Fed kept its target for short-term interest rates at 0%–0.25%.
4
For the 12-month period, as I mentioned earlier, mid- and small-cap stocks under-performed their larger counterparts, reflecting investor concerns about smaller companies’ rich valuations. This provided a relative boost to the fund’s benchmark index, which has a greater weighting in large-cap stocks (based on median market capitalization).
Keep in mind that your fund differs from its benchmark in more ways than market capitalization (the total value of a company’s shares). For example, the fund has a small portion of assets in non-U.S. stocks, while the benchmark does not include foreign holdings. See the Fund Profile that follows the Advisors’ Report for more details.
The fund’s weightings among industry sectors also diverge, to some degree, from those of the benchmark. For example, as of the end of the period, the fund had greater representation in technology and less in consumer staples (a category that includes everything from drug stores to supermarkets to tobacco companies) than the Russell 3000 Growth Index did.
Diverging from benchmarks is how active fund managers strive to achieve outperformance. Of course, along with opportunity, this parting of ways brings the risk of underperformance.
During the fiscal year, the health care sector was fertile territory for your fund. Many of the advisors’ choices among biotechnology
5
and pharmaceutical stocks soared. The shares fared well amid clinical-trial advancements for new therapies and a flurry of merger and acquisition activity. The fund also had disappointments during the 12 months; for example, some high-flying tech and internet firms, which had enjoyed robust advances earlier, reversed course.
For more information about the advisors’ strategies and the fund’s positioning, please see the Advisors’ Report that follows this letter.
To put it mildly, this period included both highs and lows for investors. There was the extreme pain of the 2008–2009 financial crisis, followed by a sharp rebound that’s been marked by an extended rally for U.S. stocks.
Through it all, the multi-manager advisory team steered your fund to deliver solid long-term results. In addition to the advisors’ skill, low investment costs helped the fund’s bottom line.
Wouldn’t paying the highest fees allow you to purchase the services of the greatest talents, and therefore get you the best returns? As it turns out, the data don’t support that argument. The explanation is simple: Every dollar paid for management fees is a dollar less earning potential return. Keeping expenses down can help narrow the gap between what the markets return and what investors actually earn.
That’s why Vanguard always seeks to minimize costs. Indexing, of course, is the purest form of low-cost investing. And we negotiate low fees for our actively managed funds, which are run by world-class advisors. It’s a strategy that reflects decades of experience and research, boiled down to one tenet: The less you pay, the more you earn.
As always, thank you for investing with Vanguard.
Sincerely,


