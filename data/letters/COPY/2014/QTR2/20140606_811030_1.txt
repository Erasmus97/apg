Dear Shareholder:



Although stock investors have been faced with a number of uncertainties over the past year, most segments of the U.S. stock markets experienced solid returns for the twelve months that ended March S&amp;P Index finished this one-year period up and all of the Hodges Mutual Fund strategies experienced positive returns for the fiscal year ended March



Returns (Retail Shares) as of


















ending






Since







Year*


Year*


Year*


Year*


Inception*



Hodges Small









Cap Fund














N/A






Russell



























Hodges Fund





















S&amp;P





























Hodges Blue Chip









Fund











N/A


N/A






Russell

























Hodges Equity









Income Fund











N/A


N/A






S&amp;P

























Hodges Pure









Contrarian Fund











N/A


N/A






S&amp;P

























Hodges Small Intrinsic









Value Fund





N/A


N/A


N/A


N/A






Russell Value





















Hodges Small-Mid Cap









Fund





N/A


N/A


N/A


N/A






Russell




















*


Average Annualized








a


Not Annualized






Performance data quoted represents past performance and does not guarantee future results. The investment return and principal value of an investment will fluctuate so that an investors shares, when redeemed, may be worth more or less than their original cost. Current performance of the funds may be lower or higher than the performance quoted. Performance data current to the most recent month end may be obtained by calling The funds impose a redemption fee on shares held for thirty days or less days or less for Institutional Class shares). Performance data quoted does not reflect the redemption fee. If reflected, total returns would be reduced.





























Hodges Mutual Funds








Hodges


Small Cap


Blue Chip


Equity Income


Pure Contrarian




Fund^


Fund^


Fund^


Fund^


Fund^



Gross Expense Ratio


















Net Expense Ratio

























^


Ratios are from the Prospectus dated July








**


The Advisor has contractually agreed to reduce its fees and/or pay Fund expenses permanently, but until at least July This figure includes Acquired Fund Fees and Expenses, and excludes interest, taxes and extraordinary expenses.
