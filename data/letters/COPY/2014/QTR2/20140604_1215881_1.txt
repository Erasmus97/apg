Dear Fellow Shareholders:
Your Fund's return during the last fiscal year was -6.79 %. In contrast, the S&P500's return during the same period was 21.86% . The primary reason for the underperformance of the Fund was poor performance in derivative trading.
To enhance returns, the Fund engaged in short-term derivative trading, specifically, buying call and put options on US equities. These trades were executed in an attempt to capture short-term price moves. Generally, the Fund had limited derivative exposure at the time of purchase, say within five percent of the total assets of the Fund.
Though the Fund's performance has been dismal because of the derivative use, we continue to plan to use them. We believe that experience gained in derivative trading should help improve the Fund's performance.
Thank you for the continued support and trust in us.
