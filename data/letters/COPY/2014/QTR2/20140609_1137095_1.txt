Dear Shareholder:



Financial Conditions During Fiscal Year Period

During the fiscal year ended March the Federal Reserve began tapering or reducing its Quantitative Easing (QE) (fourth QE variation of U.S. debt purchases).As of March the total debt issued by the U.S. Treasury reached trillion, and the balance sheet of the Federal Reserve reached trillion.The Feds QE supported the U.S. equity markets, and unlike the previous versions of QE, there was no hard ending of the program.Starting after its December Open Market Committee meeting, the Fed reduced its monthly bond buying program by billion a month (from billion to billion).By the end of this current fiscal year, the total monthly program was purchasing billion a month (three tapers).



Equity markets worldwide performed well during the fiscal year ended March was no talk of Greek contagions to spook the markets and Spanish debt went from a bane to boon.Quantitative Easing was a worldwide panacea for all economic and market ailments.The S&amp;P Index was up of months, and the average monthly S&amp;P Index return was



Historical Market Performance of the Funds

During the fiscal year the S&amp;P Index returned the same period the Vice Fund Investor Class (VICEX) was up the Vice Fund Class A (VICAX) (without sales charge) was up and the Vice Fund Class C (VICCX) (without sales charge) was up alcohol and tobacco sectors underperformed the market during the period, while gaming and aerospace defense out performed.During the fiscal year, Galaxy Entertainment Group Ltd., the Macau based casino was up the best performing holding for the third year running, while Ladbrokes PLC of the United Kingdom, another gaming company was the worst performer down the course of the fiscal year, equity options were used successfully to reduce portfolio risk and generate additional income for the Vice Fund.



The investment strategy was formally changed in the Generation Wave Growth Fund (GWGFX) during the fiscal year ended March new strategy deployed a multi alternative approach that utilized five distinctly different strategies: Equity Option, Equity Long/Short, Long/Short Fixed Income, Real Assets/Commodities and Tactical Allocation.The Generation Wave Growth Fund gained over the fiscal year while the S&amp;P Index increased by the investment strategy was changed to multi alternative, the Generation Wave Growth Fund was not invested in a portfolio of equities that would be comparable with the S&amp;P Index.A comparable index that represents multi alternative strategies is the IQ Hedge Multi-Strategy Index, which was up for the same period.Short equity options were utilized in the Generation Wave Growth Fund as part of the multi alternative strategy, and helped produce a portfolio that did not have correlative returns to the S&amp;P Index.



Outlook Painting Watercolors in the Rain

The average maturity of the trillion U.S. Treasury debt in February was months or around years with an average coupon of Federal Reserve has communicated that they would allow interest rates to normalize if the economy produces the growth they forecast.The average rate of a year treasury over the past years


























was that rate is normal, the U.S. Government Deficit will increase by almost billion per year from the increases in servicing the debt if interest rates were normalized.Unfortunately cheap money encouraged fiscal imprudence in Congress with deficits still above the year average and interest rates at historical lows.



News from the Advisor

The advisor to the Vice Fund will formally change its name from Mutuals Advisors, Inc. to USA Mutuals Advisors, Inc. and will change the name of the Vice Fund to the Barrier Fund, recognizing the substantial barriers to entry to the four sectors of the Vice Fund effective with the filing of the updated registration statement.





Gerald Sullivan

Portfolio Manager



Past performance does not guarantee future results.



Opinions expressed are those of Mutuals Advisors, Inc. (the Advisor) and are subject to change, are not guaranteed, and should not be considered a recommendation to buy or sell any security.



This report is authorized for use when preceded or accompanied by a prospectus.Read it carefully before investing or sending money.



Fund holdings and sector allocations are subject to change and are not a recommendation to buy or sell any security.For a complete list of Fund holdings, please see the Portfolios of Investments and Schedules of Options Written in this report.



Mutual fund investing involves risk; principal loss is possible.The Vice Fund and the Generation Wave Growth Fund (each a Fund, together the Funds) are non-diversified, meaning they may concentrate their assets in fewer individual holdings than a diversified fund.Therefore, the Funds are more exposed to individual stock volatility than diversified funds.The Funds invest in foreign securities which involve greater volatility and political, economic and currency risks and differences in accounting methods.The Funds invest in small to mid-sized capitalization companies, which involve additional risks such as limited liquidity and greater volatility.Because the Generation Wave Growth Fund may invest in third-party investment companies, including open-end mutual funds and other investment companies, your cost of investing in a Fund will generally be higher than the cost of investing directly in the shares of the mutual funds in which it invests.By investing in the Generation Wave Growth Fund, you will indirectly bear your share of any fees and expenses charged by the underlying funds, in addition to indirectly bearing the principal risks of those funds.Please refer to the prospectus for more information about the Generation Wave Growth Fund, including risks, fees and expenses.



The Funds may also write call options, which may limit the Funds ability to profit from increases in the market value of a security, but cause it to retain the risk of loss should the price of the security decline.The Funds may also invest in companies that manufacture and distribute precious metals such as silver, which involves additional risks, such as the possibility for substantial price fluctuations over a short period of time.Diversification does not assure a profit or protect against loss in a declining market.



The Funds may invest in derivatives, specifically call and put options.Derivatives involve risks different from, and in certain cases, greater than the risks presented by more traditional investments.The Funds may engage in short sales of securities, which involves the risk that losses may exceed the original amount invested.



The S&amp;P Index is a broad based unmanaged index of stocks, which is widely recognized as representative of the equity market in general.You cannot invest directly in an index.



The IQ Hedge Multi-Strategy Index seeks to replicate the risk-adjusted return characteristics of the collective hedge funds using various hedge fund investment styles, including long/short equity, global macro, market neutral, event-driven, fixed income arbitrage and emerging markets.You cannot invest directly in an index.



The Funds are distributed by Quasar Distributors, LLC.
































EXPENSE EXAMPLE (Unaudited)
