Dear Shareholder: 
While the global economy achieved measured improvement during the past year, intermittent volatility in financial markets drove yield-hungry investors back into developed markets at an accelerated pace
through the 12 months ended February 28, 2014. Fixed-income markets generally struggled in the face of a concerted effort by central bankers to stimulate economic growth, while equity markets in developed nations generally performed well. From
a macro-economic perspective, the U.S. Federal Reserve (the “Fed”) served as the focal point of investors’ attention after the central bank signaled its intent to “taper” off its $85 billion in monthly asset purchases in
response to improvements in key economic indicators. The Fed’s initial statement injected volatility into the market as investors sought further clarity on the impact and timing of any move to reduce Fed purchases of agency asset-backed
securities and Treasury bonds, a program known as Quantitative Easing (QE). Volatility spiked again in September, when the Fed confounded expectations and abstained from tapering its asset purchase program. It wasn’t until December that the Fed
moved to cut its QE program by $10 billion a month. The Fed’s decision sparked a rally in equity markets and drove Treasury bond prices lower. The Fed followed in February with another $10 billion reduction, bringing the QE program to
$65 billion in monthly asset purchases. 
 
Dear Shareholder: 
While the global economy achieved measured improvement during the past year, intermittent volatility in financial markets drove yield-hungry investors back into developed markets at an accelerated pace
through the 12 months ended February 28, 2014. Fixed-income markets generally struggled in the face of a concerted effort by central bankers to stimulate economic growth, while equity markets in developed nations generally performed well. From a
macro-economic perspective, the U.S. Federal Reserve (the “Fed”) served as the focal point of investors’ attention after the central bank signaled its intent to “taper” off its $85 billion in monthly asset purchases in
response to improvements in key economic indicators. The Fed’s initial statement injected volatility into the market as investors sought further clarity on the impact and timing of any move to reduce Fed purchases of agency asset-backed
securities and Treasury bonds, a program known as Quantitative Easing (QE). Volatility spiked again in September, when the Fed confounded expectations and abstained from tapering its asset purchase program. It wasn’t until December that the Fed
moved to cut its QE program by $10 billion a month. The Fed’s decision sparked a rally in equity markets and drove Treasury bond prices lower. The Fed followed in February with another $10 billion reduction, bringing the QE program to $65
billion in monthly asset purchases. 
 
Dear Shareholder: 
While the global economy achieved measured improvement during the past year, intermittent volatility in financial markets drove yield-hungry investors back into developed markets at an accelerated pace
through the 12 months ended February 28, 2014. Fixed-income markets generally struggled in the face of a concerted effort by central bankers to stimulate economic growth, while equity markets in developed nations generally performed well. From a
macro-economic perspective, the U.S. Federal Reserve (the “Fed”) served as the focal point of investors’ attention after the central bank signaled its intent to “taper” off its $85 billion in monthly asset purchases in
response to improvements in key economic indicators. The Fed’s initial statement injected volatility into the market as investors sought further clarity on the impact and timing of any move to reduce Fed purchases of agency asset-backed
securities and Treasury bonds, a program known as Quantitative Easing (QE). Volatility spiked again in September, when the Fed confounded expectations and abstained from tapering its asset purchase program. It wasn’t until December that the Fed
moved to cut its QE program by $10 billion a month. The Fed’s decision sparked a rally in equity markets and drove Treasury bond prices lower. The Fed followed in February with another $10 billion reduction, bringing the QE program to $65
billion in monthly asset purchases. 
 
Dear Shareholder: 
While the global economy achieved measured improvement during the past year, intermittent volatility in financial markets drove yield-hungry investors back into developed markets at an accelerated pace
through the 12 months ended February 28, 2014. Fixed-income markets generally struggled in the face of a concerted effort by central bankers to stimulate economic growth, while equity markets in developed nations generally performed well. From
a macro-economic perspective, the U.S. Federal Reserve (the “Fed”) served as the focal point of investors’ attention after the central bank signaled its intent to “taper” off its $85 billion in monthly asset purchases in
response to improvements in key economic indicators. The Fed’s initial statement injected volatility into the market as investors sought further clarity on the impact and timing of any move to reduce Fed purchases of agency asset-backed
securities and Treasury bonds, a program known as Quantitative Easing (QE). Volatility spiked again in September, when the Fed confounded expectations and abstained from tapering its asset purchase program. It wasn’t until December that the Fed
moved to cut its QE program by $10 billion a month. The Fed’s decision sparked a rally in equity markets and drove Treasury bond prices lower. The Fed followed in February with another $10 billion reduction, bringing the QE program to $65
billion in monthly asset purchases. 
 
