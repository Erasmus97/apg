Dear Shareholders: 
The stock market has been as volatile as the weather in the final weeks of 2015 and so far
in 2016. 
Investors seem to be responding to a mixed bag of macro-economic factors. The U.S. economy continues to grow,
albeit slowly, as it weathers a contraction in the manufacturing sector brought on by historically low energy prices. Paying less for oil and gas is good for consumers and many energy-intensive businesses, but it’s a challenge for companies in
this sector to remain profitable. In China, growth has stalled as the government attempts to transition from an investment- and export-led economy to a consumer-driven economy. Policy makers in Europe and Japan have resorted to negative interest
rates in an effort to further stimulate their economies. After strengthening meaningfully in 2015, the U.S. dollar has declined marginally so far in 2016, offering hope that the currency headwinds faced by U.S. multi-national companies may be
abating. 
Given these economic cross currents and the recent spike in market volatility, there is increased uncertainty over
the path for U.S. interest rates. Initially, it was expected that the Federal Reserve’s first rate increase in December would be followed by several more hikes in 2016. The futures markets are currently signaling otherwise. 
We encourage our shareholders and clients to stay the course with their well-considered investment programs. Market volatility creates
opportunities for good stock and bond selectors, and the portfolio managers at the helm of our actively managed funds have years of experience navigating through choppy markets. 
Maintaining a diversified investment program is always important but never more so than in times of extreme volatility. Spreading your
money across different types of investments limits the impact any one type of investment would have on your overall portfolio. 
We believe the range of Homestead Funds available offers ample opportunity for diversification, and note especially our newly renamed
International Equity Fund. Shareholders voted in support of the board of director’s recommendation to name a new subadvisor for this fund, and on January 15, 2016, Harding Loevner LP assumed responsibility for managing the portfolio.

The portfolio is comprised of high-quality international securities and provides modest exposure to emerging markets. The
domestic stock market and international markets are not typically closely correlated, so adding international exposure may help smooth the ups and downs. 
We continue to look for ways to enhance our value to shareholders, both in the management of specific portfolios and the range of products
and services we offer at the firm level. We welcome your feedback and very much appreciate your choosing Homestead Funds to help you achieve your financial goals. 
 
Sincerely, 


