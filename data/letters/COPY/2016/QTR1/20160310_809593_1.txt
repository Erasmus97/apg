Dear Shareholders,
 
2015 was a lackluster year for domestic equity investors. The Dow Jones Industrial Average, a price-weighted average of 30 significant stocks that trade on the
New York Stock Exchange and The NASDAQ Stock Market, returned 0.21% for the 12-month period. The S&amp;P 500 Index, a U.S. large-cap stock bellwether, returned 1.38%. However, of the 10 industry sectors in which the S&amp;P 500 Index is divided,
half lost ground during the year. The Russell Midcap Index, a barometer of mid-cap stocks, declined -2.44%. And the Russell 2000 Index, which generally represents the small-cap segment, declined -4.41%.
The weakening of many currencies against the U.S. dollar also took a toll on total returns for dollar-based investors in many
international markets. For the period under review, international market benchmarks posted declines. The MSCI EAFE Index, which measures the world’s developed market, declined -0.81%; the MSCI Emerging Markets Index declined -14.92%; and the
MSCI All Country World Index, which includes the equity market performance of developed and emerging markets, declined -2.36%. 
At American Beacon
Advisors, we are proud to offer a broad range of equity funds that are capable of navigating economic storms and market downturns in the U.S. and abroad. We are fortunate to have several asset managers who have stayed the course for many years and
through a variety of economic and market conditions. 
For the 12 months ended December 31, 2015: 
 
 
 
 
 
www.americanbeaconfunds.com
 
