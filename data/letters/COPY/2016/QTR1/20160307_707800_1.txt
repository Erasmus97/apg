Dear Fellow Shareholder:



Volatility remained elevated throughout the fourth quarter of in both the high yield and equity markets. Continued pressures on commodity prices and further weakness in Chinese economic data increased investor concern about global economic growth. The Federal Reserve (the "Fed") finally raised the Fed Funds rate by in mid-December, which created added volatility. While the Fed maintained their commentary of lower for longer, they are currently indicating several more hikes throughout



Oils continued decline throughout placed significant pressure on energy and metals/mining related names, causing the securities of those companies to significantly underperform throughout the year. While we believe this should be a tailwind for consumer spending and serve to lessen input costs for many corporations, we have yet to see the potential positives of lower energy costs flow into economic data. Despite increased market volatility, elevated geopolitical tensions, and increased concerns regarding global economic growth throughout the U.S. economy remained relatively resilient. The positive trajectory of many employment statistics was a highlight of economic activity throughout The U.S. unemployment rate declined to the lowest level since early The prospects of lower energy costs and increased wage growth heading into may, in our view, bode well for continued gains in U.S. consumer confidence and spending.



There are a number of key topics and issues that we contemplate as we position our strategies. Similar to the Feds commentary surrounding future interest rate movement decisions, our strategy positioning is very much data dependent and with increased uncertainty in the capital markets, there is a great deal of information to consider.



Changing Monetary Policy The Federal Reserve moved the Fed Funds rate higher in December for the first time since beginning a near-zero interest rate policy seven years ago. The commentary from the Fed indicates they may raise the Fed Funds rate another times throughout This may increase the cost of financing at the consumer and business level, which could adversely impact consumer and business spending. Continued rate hikes may also place pressure on Treasury security yields, which may cause elevated volatility in the fixed-income markets. Increased volatility in the fixed-income markets could lead to increased volatility in the equity market.



US GDP Growth The strength of the US economy has come into question lately. Current expectations are for continued Gross Domestic Product ("GDP") growth for the US economy in however, a significant deviation from this expectation in either direction could influence investor expectations or alter how corporate decision makers allocate capital. Expectations for US GDP growth for the fourth quarter of were initially close to but declined significantly throughout the quarter and ended the year slightly below We are concerned this trend of slower growth may continue into



NOT A PART OF THE ANNUAL REPORT























Stronger US Dollar A stronger US dollar will likely continue to be a headwind to US exports as well as continue to negatively impact corporate earnings of multinational companies. On the other hand, it will, in our view, also likely help curb inflationary pressures on imported goods and could serve to boost economic growth abroad.



Oils Decline Lower utility and gasoline bills may boost US consumer spending and should help reduce input costs for many companies, but lower oil and natural gas prices pose a risk to corporate solvency for many energy companies, a particular headwind for the high yield asset class and perhaps in the investment-grade bond market. Low prices may also hinder some countries ability to balance budgets and service debt burdens. We continue to have an extremely cautious outlook on the energy industry and remain underweighted in the segment with our investment strategies.



Employment &amp; Wages Employment growth was reasonably solid in with monthly non-farm payrolls averaging over per month and the employment rate declining from to throughout the year. Several other employment measures, such as under-employment and job availability, also improved throughout the year. Wage growth was positive and accelerated in however, the growth rate was stubbornly low throughout the year, which makes us question how much slack there really is in the labor market. A further acceleration in wage growth that results in increased inflation measures could, in our view, force the Fed to raise rates quicker than expected.



US Consumer Auto and home sales improved throughout as did home values, employment data and consumer delinquency trends, indicating the US consumer should be on fairly solid footing heading into These trends may continue to be a positive for many consumer-related industries and companies; however, rising interest rates and continued sluggishness in wage growth may very well impact the consumers ability to purchase and finance large ticket items. Data regarding consumer confidence and consumer spending will continue to be scrutinized for any strength or weakness. With household spending accounting for approximately of U.S. economic activity, the strength of the consumer and the willingness and ability to purchase goods and services are a key driver of corporate profits and economic growth.



Corporate Earnings Corporate earnings have been somewhat challenged of late due to slower sales growth and the negative impact of a stronger dollar. The industrial and commodity-related industries continue to struggle and we have begun to see signs of sluggish earnings growth in other areas of the economy. Expectations are for corporate sales and earnings growth to continue to slow into



Increased Bond Yields and Yield Spread The corporate bond market, in particular the high yield market, has been building in an increased risk premium on corporate credit since June of which could increase the cost of capital for many companies and influence corporate decision makers as they allocate capital in The rise in bond yield spreads in the high yield market to levels not experienced since has us concerned that corporate solvency issues are on the rise.



NOT A PART OF THE ANNUAL REPORT























Declining Credit Trends The number of corporate credit rating downgrades by the Nationally Recognized Statistical Rating Organizations significantly outpaced the number of upgrades during We believe this reflects deterioration in credit metrics due to slower earnings growth and an increased use of debt to fund shareholder-friendly activities. We expect this trend to continue throughout The amount of distressed debt found in the high yield market increased significantly throughout Even excluding the stressed commodity-related industries, distressed debt increased fairly significantly across other industries. This metric ended the year at a post-financial crisis high and gives us reason to believe that corporate defaults are likely to be on the rise throughout



Thank you for your continued support and investment.



Sincerely,
