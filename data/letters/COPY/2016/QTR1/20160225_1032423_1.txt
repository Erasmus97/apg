Dear Fellow Shareholders: 
We are pleased to provide you with
this year-end update for the Diamond Hill Funds. 2015 was another challenging year for active managers. The debate around active versus passive management has intensified as market conditions over the past five years have made it difficult for many
active managers, including Diamond Hill, to outperform passive benchmarks. These performance headwinds, coupled with higher fees for active management strategies, have caused some investors to question whether active management is still relevant.

We believe that active managers with a consistent, disciplined, and repeatable investment process have the potential to outperform a passive alternative
over the long-term and to offer downside protection, limiting losses, during market declines. Diamond Hill Funds have generally demonstrated both of these qualities over long periods of time. The performance of active strategies relative to a
passive benchmark tends to be cyclical. These cycles are created because price momentum can persist in the short-term but reverts in the long-term. Market returns are often driven by investor emotions and unpredictable market psychology in the
short-term, which has been extended in recent years by unprecedented monetary stimulus; an extended bull market characterized by lower volatility, lower dispersion, and higher correlations; less focus on individual, bottom-up company fundamentals;
and self-reinforcing passive flows. 
While market conditions over the past five years have made it difficult for many active managers to outperform
passive alternatives, we expect the cycle will turn in favor of active management, and we are beginning to see signs of a turning point. Dispersion between individual stock returns is increasing and in December 2015, the Federal Reserve increased
its Fed Funds target rate for the first time since 2006. We continue to believe that Diamond Hill’s intrinsic value-focused approach will outperform over a full market cycle, supported by a shared commitment to our intrinsic value-based
investment philosophy, long-term perspective, disciplined approach, and alignment with our clients’ interests. 
In 2015, our Funds’ returns
relative to benchmarks were mixed. More importantly, as of December 31, 2015, the since-inception returns for nearly all of our Funds exceeded their respective benchmark returns. The only exceptions were our Research Opportunities Fund and our Mid
Cap Fund, which have less than a five-year track record. As always, we remain focused on five-year periods to evaluate our results, as we believe five years is the shortest time period for statistical significance. 
2015 Financial Markets 
U.S. stocks rose modestly in the
first half of 2015 but lost momentum in the third quarter as investor concerns over a slowdown in China’s economic growth negatively impacted commodity prices. China is the world’s largest importer of raw materials. This slowdown has also
strained many other developing countries and large portions of the global industrial sector as China has been a meaningful source of incremental demand for key industrial end markets. A strong fourth quarter was not enough to completely save the
year and most major market indexes ended 2015 somewhere close to even. The Russell 1000 Index posted a total return, including dividends, of 0.92% for the year, while the broader 
 
Letter to Shareholders 
Letter to Shareholders 
 
 
Russell 3000 Index posted a 0.48% total return. Small cap stocks fared worse than large cap stocks, with the Russell 2000 Index posting a total return of
-4.41%. 
Outlook 
The U.S. economy appears to be
continuing the healing process with real growth generally staying in the 2% - 3% range. Globally, central banks remain extraordinarily accommodative in an attempt to provide a backdrop for increased economic growth. Europe, which has been an
economic laggard over the past few years, has shown some signs of stabilization. However, China has seen a significant slowdown in its rate of economic expansion. While the U.S. Fed Funds rate remains extremely low, the 25 basis point increase in
December indicates policymakers’ confidence in the recovery of U.S. labor markets and an expectation of slowly rising inflation. It is expected that further rate increases will come at a gradual pace, which may be affected by volatility in
global financial markets. 
We continue to expect positive but below average equity market returns over the next five years. Our conclusion is primarily
based on the combination of above average price/earnings multiples applied to already very strong levels of corporate profit margins, which likely tempers prospective returns. This expectation also seems consistent with the current interest rate
environment. 
Diamond Hill Capital Management, Inc. 
 
