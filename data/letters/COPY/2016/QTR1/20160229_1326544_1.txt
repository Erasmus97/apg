Dear Fellow Wintergreen Fund Shareholder, 
 
As we look back on Wintergreen Fund, Inc.’s (the “Fund”
or “Wintergreen”) first ten years, we are extremely pleased with what we have accomplished. The Fund has sought to act with integrity and to stand up for our shareholders when faced with management teams who we believe have misbehaved. The
Fund has remained focused on its long-term, global value investing approach, and has not wavered in an attempt to chase short-term performance. It has clearly been a difficult period to be a true value investor, but we believe that remaining
disciplined, while so many others have strayed, offers our shareholders a great opportunity for future success. 
 
Since inception in 2005 through December 31, 2015, the Fund’s Investor Class of shares has a cumulative return of 68.73%. The Institutional Class of shares has a cumulative return of
15.95% since its inception in 2011. During 2015, the Fund’s Institutional Class of shares returned -6.70% and the Investor Class of shares returned -6.94%. The Fund had strong 2015 returns from long-term portfolio holdings Reynolds American
Inc. (“Reynolds”), Alphabet, Inc., and Altria Group Inc. Securities that underperformed included Franklin Resources, Inc., which was sold during the year, Birchcliff Energy Ltd., and Canadian Natural Resources Ltd. The Fund also utilized
forward currency contracts, which had an overall positive impact on performance during the period. 
 
The time is always right to do what is right. 
- Martin
Luther King, Jr. 
 
Every now and then, controlling
shareholders of an excellent business foolishly attempt to enrich themselves at the expense of minority shareholders. At the close of 2014, the Burkard family announced its intention to sell its controlling stake of Sika AG of Switzerland to a
French competitor, Cie de Saint-Gobain, which has long coveted this prize of a company. The problem in this proposed deal is that the Burkard family, which conducted negotiations away from Sika’s Board of Directors, would receive a premium in
the sale and all other shareholders would not. Now held up in the judicial process due to strong and swift legal action taken by Sika’s Board of Directors, and backed by key minority shareholders, we think this brazen attempt to deprive
shareholders of Sika’s full and fair value will fail. Shares of this little-known gem of a company—a maker of high value-added construction materials to satisfy growing demand for stronger, lighter, and more energy-efficient building and
automobile structures – have been knocked down to a discount, which we intend to exploit. Out of the range of possible outcomes in the battle for control, we believe the current plan of the Burkard family is the least likely, and when this
corporate governance distraction is resolved in the favor of the Board of Directors and minority shareholders, the focus will return to the company’s attractive fundamentals of organic sales growth in both developed and emerging markets,
earnings-accretive bolt-on acquisitions, and steady cash flow generation. 
 
Try not to become a man of success, but rather a man of value. 
- Albert Einstein 
 
Reynolds has been a core Wintergreen investment for many years. The Reynolds management team utilizes a value formula that consists of wielding
pricing power, generating substantial free cash flows, and returning much of that cash through raising dividends and share repurchases. The company acquired the Newport brand 
 
3 
Table of Contents
WINTERGREEN FUND, INC. 
A MESSAGE TO OUR SHAREHOLDERS (continued) 
DECEMBER 31, 2015 (Unaudited) 


