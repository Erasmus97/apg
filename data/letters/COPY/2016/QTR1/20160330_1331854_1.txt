Dear Shareowner,

Over the past several years, many investors experienced positive returns across
most major asset classes. However, was a tale of two markets, with
favorable market conditions in the first half of the year, followed by an abrupt
slowdown and increased volatility beginning in August. The global markets were
challenged by significant economic cross-currents in different geographic
regions and industrial sectors. While the U.S. economy gradually improved,
growth slowed in China. Emerging markets struggled following a decline in
commodity prices, especially oil. While lower energy prices are good for the
consumer, there were ripple effects throughout the global economy.

Against this backdrop, the Standard & Poor's Index rose by just in
international equity markets were essentially flat, and emerging market
equities fell sharply. Across U.S. fixed-income sectors, U.S. government and
investment-grade corporate bonds were fairly flat for the year, while high-yield
bonds, as measured by the Bank of America Merrill Lynch Master II High Yield
Index, posted a total return.

Entering we see the U.S. economy growing modestly, against an overall
global economic backdrop that remains fragile and points towards structurally
lower growth. As always in a Presidential election year, political rhetoric has
the potential to impact U.S. sectors such as health care in Economies
around the world in both developed and emerging markets are experiencing deep
structural change. Geopolitical instability on many fronts, the rising risk of
policy mistakes, and market liquidity issues all increase the possibility of
sharp swings in asset values. In this environment, financial markets remain
vulnerable to unusual levels of volatility. While divergences among regions and
industries is an important theme, we are generally optimistic about the outlook
for the U.S. economy, which we expect will see continued, positive growth led by
a strengthened consumer.

Throughout Pioneer's history, we have believed in the importance of active
management. In periods of market volatility, we believe that the value of active
management is even more compelling. Our experienced and tenured investment teams
focus on identifying value across global markets using proprietary research,
careful risk management, and a long-term perspective. Our ongoing goal is to
produce compelling returns consistent with the stated objectives of our
investment products, and with our shareowners' expectations. We believe our
shareowners can benefit from the experience and tenure of our investment teams
as well as the insights generated from our extensive research process.

Pioneer Classic Balanced Fund | Semiannual Report |


As always, and particularly during times of market uncertainty, we encourage you
to work with your financial advisor to develop an overall investment plan that
addresses both your short- and long-term goals, and to implement such a plan in
a disciplined manner.

We greatly appreciate the trust you have placed in us and look forward to
continuing to serve you in the future.

Sincerely,

Dear Shareowner,

Over the past several years, many investors experienced positive returns across
most major asset classes. However, was a tale of two markets, with
favorable market conditions in the first half of the year, followed by an abrupt
slowdown and increased volatility beginning in August. The global markets were
challenged by significant economic cross-currents in different geographic
regions and industrial sectors. While the U.S. economy gradually improved,
growth slowed in China. Emerging markets struggled following a decline in
commodity prices, especially oil. While lower energy prices are good for the
consumer, there were ripple effects throughout the global economy.

Against this backdrop, the Standard & Poor's Index rose by just in
international equity markets were essentially flat, and emerging market
equities fell sharply. Across U.S. fixed-income sectors, U.S. government and
investment-grade corporate bonds were fairly flat for the year, while high-yield
bonds, as measured by the Bank of America Merrill Lynch Master II High Yield
Index, posted a total return.

Entering we see the U.S. economy growing modestly, against an overall
global economic backdrop that remains fragile and points towards structurally
lower growth. As always in a Presidential election year, political rhetoric has
the potential to impact U.S. sectors such as health care in Economies
around the world in both developed and emerging markets are experiencing deep
structural change. Geopolitical instability on many fronts, the rising risk of
policy mistakes, and market liquidity issues all increase the possibility of
sharp swings in asset values. In this environment, financial markets remain
vulnerable to unusual levels of volatility. While divergences among regions and
industries is an important theme, we are generally optimistic about the outlook
for the U.S. economy, which we expect will see continued, positive growth led by
a strengthened consumer.

Throughout Pioneer's history, we have believed in the importance of active
management. In periods of market volatility, we believe that the value of active
management is even more compelling. Our experienced and tenured investment teams
focus on identifying value across global markets using proprietary research,
careful risk management, and a long-term perspective. Our ongoing goal is to
produce compelling returns consistent with the stated objectives of our
investment products, and with our shareowners' expectations. We believe our
shareowners can benefit from the experience and tenure of our investment teams
as well as the insights generated from our extensive research process.

Pioneer Multi-Asset Income Fund | Semiannual Report |


As always, and particularly during times of market uncertainty, we encourage you
to work with your financial advisor to develop an overall investment plan that
addresses both your short- and long-term goals, and to implement such a plan in
a disciplined manner.

We greatly appreciate the trust you have placed in us and
look forward to continuing to serve you in the future.

Sincerely,
