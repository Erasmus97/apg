Dear Shareholders:

The UTC North American Fund posted a total return of for the year ended December Global equity markets also declined in with the MSCI All Country World Index falling by on a total return basis. While the S&amp;P Index posted a total return, the price return was The Funds return continues to be driven primarily by the performance of its equity component.




The International Monetary Fund (IMF) in January revised its global economic growth rate downwards. Global GDP is now expected to increase by in from in October followed by in According to the IMF, several factors continue to influence the global outlook: the gradual slowdown and rebalancing of economic activity in China away from investment and manufacturing toward consumption and services, lower prices for energy and other commodities, and a gradual tightening in monetary policy in the U.S. in the context of a resilient U.S. recovery as several other major advanced economy central banks continue to ease monetary policy.




U.S. GDP is projected to have grown by quarter-over-quarter (q-o-q) in after growing in In previous quarters, the economy was provided buoyancy by resurgence in consumer spending, particularly in housing and in the retail space. It seems that the precipitous fall in commodity prices was not able to spur the expectant jump in retail sales which were affected by warmer weather across the states during




Additionally, Manufacturing and Construction data disappointed analysts. On the back of continued positive employment data, the U.S. Fed took the decision to raise its benchmark interest rate in December to to Analysts expect another interest rate hike in March U.S. GDP is estimated to be for




Canadas GDP is expected to have grown by q-o-q in after growing in For the first half of Canada was in a recession caused by the sharp fall in oil prices. Growth in the fourth quarter appears to be similarly dismal even as the Bank of Canada twice cut interest rates in in an effort to provide support to the economy. The Canadian economy was also negatively impacted by sub-par performances in the Manufacturing, Construction and Retail sectors. Canadas GDP growth is expected to be




Overall, global growth is projected to be fueled by the advanced economies with the emerging and developing economies to slow somewhat. The U.S. economy continues to be resilient, supported by still easy financial conditions and strengthening housing and labor markets. Growth in China is expected to slow reflecting weaker investment growth as the economy continues to rebalance.









FUND PERFORMANCE

For year ended December the UTC North American Funds equity component posted a gross return of compared to the blended index benchmark return of The Funds equity component benchmark, the S&amp;P Total Return Index, generated a return of while the fixed income components benchmark, the Barclays U.S. Aggregate Government/Credit Bond Index, returned




The Portfolio Price Return of represents a change in the price of the Fund. This decline was due to a significant dividend payout that the Fund was required to make in December as a direct result of capital gains taken from a rebalancing exercise (discussed below). The total return of the Fund for the period under review was therefore The rebalancing exercise undertaken on the equity portion of the portfolio in July by the newly appointed equity sub-advisor Goldman Sachs Asset Management resulted in large capital gains that had to be paid out as a dividend. This resulted in a much lower unit price of the NAV after the dividend payment.




The fixed income component returned above its benchmarks return of by In Management expanded its selection pool for fixed income assets, allowing for the investment in bonds with higher yields. However, a persistent low interest rate environment exerted downward pressure on the overall fixed income portfolio returns.




The strategy for will involve a balanced approach with a heavier weighting in equities versus fixed income securities.


















Ms. Amoy Van Lowe President

UTC North American Fund, Inc. (the Fund) is distributed in the U.S. by UTC Financial Services USA, Inc. (the Broker-Dealer), a FINRA member firm. The Fund and the Broker-Dealer are affiliated through UTC Fund Services, Inc., the registered investment adviser to the Fund and the parent corporation to the Broker- Dealer.

Past performance does not guarantee future results.

The above discussion and analysis of the Fund reflect the opinions of the Funds investment adviser as of December and are subject to change, and any forecasts made cannot be guaranteed and should not be considered investment advice.










This report is authorized for use when preceded or accompanied by a prospectus. Read it carefully before investing.




Fund holdings and sector allocations are subject to change and are not a recommendation to buy or sell any security. For a complete list of Fund holdings, please see the Schedule of Investments in this report.

MSCI All Country World Index - A market capitalization weighted index designed to provide a broad measure of equity-market performance throughout the world. The MSCI ACWI is maintained by Morgan Stanley Capital International, and is comprised of stocks from both developed and emerging markets.




Gross Domestic Product (GDP) is the total value of goods produced and services provided in a country during one year.




A basis point is one hundredth of one percent




















UTC North American Fund, Inc.

Investment Results

For the Year Ended December (Unaudited)
