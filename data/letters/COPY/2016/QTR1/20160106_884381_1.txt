Dear Shareholder:






We are pleased to present the annual report of the Glenmede
family of funds for the fiscal year ended
Equity markets were volatile during the fiscal year ended
as a result of concerns over global growth
which included a slowdown in China and a debt crisis in Greece.
Domestic equity returns were better than International developed
returns as the strong dollar muted positive returns in local
currencies. Emerging market equities were the worst performers
due to slower growth and lower commodity prices. The
U.S.Government yield curve flattened slightly in
anticipation of a rise in short-term Interest rates in
Money market funds suffered very low yields, as the Federal
Reserve maintained short-term rates at near zero.





At the fiscal year end on The Glenmede
Fund, Inc. and The Glenmede Portfolios (collectively the
Glenmede Funds) consisted of sixteen portfolios with
total assets of an increase of
over the year ended The
fund family includes two International Portfolios: the
International Portfolio, which is now directly managed by
Glenmede Investment Management LP and the International Secured
Options Portfolio also managed by Glenmede Investment Management
LP.





This fiscal year ended showed modest
returns across domestic equity asset classes in response to
moderate growth in the U.S.economy. The SP
Indexi

gained Small Cap stocks did not do nearly as well as
Large Cap stocks with the Russell


returning versus a return of on the Russell


for the fiscal year ended Interest rates
were stable as there was no action from the Federal Reserve to
raise short-term interest rates. The Barclays Capital
U.S.Aggregate Bond


gained and the Barclays Capital Municipal
Blend


gained for the fiscal year ended
The MSCI EAFE


did not perform as well as domestic stocks for the fiscal year
losing





The Glenmede Large Cap Core Portfolio achieved a five star
(*****) Overall Morningstar


among Large Blend Equity Funds for the period ended
(based on risk adjusted returns). The
Glenmede Large Cap Growth Portfolio achieved a five star (*****)
Overall Morningstar


among Large Growth Equity Funds for the period ended
(based on risk adjusted returns). The
Glenmede Total Market Portfolio achieved a four star (****)
Overall Morningstar


among Mid-Cap Value Equity Funds for the period ended
(based on risk adjusted returns). The
Glenmede Small Cap Equity Advisor Portfolio and Institutional
Portfolio achieved four star (****) and five star (*****)
Overall Morningstar


respectively, among Small Blend Equity Funds for the period
ended (based on risk adjusted returns).
The Glenmede Secured Options Portfolio achieved a four star
(****) Overall Morningstar


among Long/Short
























Equity Funds for the period ended
(based on risk adjusted returns).






Despite stronger employment data, improving housing markets and
moderate growth in the economy, the Federal Reserve has
maintained short-term rates at since
In late investors fled lower
global yields for U.S.Fixed income, which helped keep
long-term rates low. However, in anticipation of rising
short-term rates in and continued geopolitical concerns,
the yield curve flattened slightly during the current fiscal
year. One year maturity treasuries rose


and treasury yields decreased


Money Market fund yields remain at all-time lows.





The Glenmede bond portfolios all have a high quality bias. The
Muni Intermediate Portfolio achieved a four star (****) Overall
Morningstar


among Muni National Short Funds for the period ended
(based on risk-adjusted returns). The
Overall Morningstar
Ratingtm

for a fund is derived from a weighted average of the performance
figures associated with its three-, five- and ten-year (if
applicable) Morningstar Rating metric.





We have featured the Secured Options Portfolio and included
Portfolio Highlights for each individual fund in this report. We
welcome any questions about the Glenmede Funds and thank our
shareholders for their continued support.





Sincerely,
