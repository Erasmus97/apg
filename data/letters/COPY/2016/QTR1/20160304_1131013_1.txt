Dear Shareholder: 
2015 GuideStone Funds Annual Report
In 2015, GuideStone Funds was recognized
for the fourth consecutive year by the Lipper Fund Awards. The Extended-Duration Bond Fund was recognized as the Best Fund Over 3 Years, and the Best Fund Over 5 Years in the Corporate Debt A-Rated Funds category for the fund’s risk-adjusted
total return, ending November 30, 2014. The Extended-Duration Bond Fund won in the same categories the previous year. In 2012, the entire GuideStone Funds family was honored with Lipper’s Best Overall Small Fund Group in the U.S. over the
3-year period ending November 30, 2011, making it the first Christian-screened mutual fund family to receive this prestigious honor. In 2013, the MyDestination 2025 Fund was ranked No. 1 for its performance over the 3-year period ending
November 30, 2012. 
As previously communicated, on November 20 the Asset Allocation Funds I Series were closed and all assets were
reorganized into the Institutional Share Class of the Asset Allocation Funds. The I Series Funds — the Conservative Allocation Fund I, Balanced Allocation Fund I, Growth Allocation Fund I and the Aggressive Allocation Fund I — were
technically four stand-alone funds with an Institutional Share Class launched in 2003. During 2015, changes were made that eliminated any difference between the underlying investments of the Asset Allocation Funds I Series and the Asset Allocation
Investor Share Class; therefore, the need to maintain four stand-alone I Series funds no longer existed. 
Our affected investors experienced
no fee change, nor did they incur a taxable event as a result of the reorganization. Additionally, no commission or trading costs from the reorganization were generated. This gives GuideStone a single set of Asset Allocation Funds with a
multiple-class structure, similar to other funds in the GuideStone Funds family. 
GuideStoneFunds.com/Disclosures
Sincerely, 


