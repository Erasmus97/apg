DEAR SHAREHOLDER 
2
ECONOMIC AND MARKET OVERVIEW 
3
FEES AND EXPENSES 
5
MANAGER'S ANALYSIS 
7
PORTFOLIO SUMMARY 
8
SCHEDULES OF INVESTMENTS 
36
STATEMENTS OF ASSETS AND LIABILITIES 
95
STATEMENTS OF OPERATIONS 
99
STATEMENTS OF CHANGES IN NET ASSETS 
103
FINANCIAL HIGHLIGHTS 
110
NOTES TO FINANCIAL STATEMENTS 
124
REPORT OF INDEPENDENT REGISTERED PUBLIC ACCOUNTING FIRM 
135
OTHER INFORMATION 
136
INFORMATION ON BOARD OF TRUSTEES AND OFFICERS 
141
GUGGENHEIM INVESTMENTS PRIVACY POLICIES 
144
 
 
October 31, 2015
 
Dear Shareholder: 
 
Security Investors, LLC (the “Investment Adviser”) is pleased to present the annual shareholder report for a selection of our exchange-traded funds (“ETFs”) for the 12-month period ended October 31, 2015. 
 
The Investment Adviser is part of Guggenheim Investments, which represents the investment management businesses of Guggenheim Partners, LLC (“Guggenheim”), a global, diversified financial services firm. 
 
Guggenheim Funds Distributors, LLC is the distributor of the Funds. Guggenheim Funds Distributors, LLC is affiliated with Guggenheim and the Investment Adviser. 
 
We encourage you to read the Economic and Market Overview, which follows this letter, followed by the Manager’s Analysis for each ETF. 
 
Sincerely, 


DEAR SHAREHOLDER 
2
ECONOMIC AND MARKET OVERVIEW 
3
FEES AND EXPENSES 
5
MANAGER'S ANALYSIS 
7
PORTFOLIO SUMMARY 
8
SCHEDULES OF INVESTMENTS 
21
STATEMENTS OF ASSETS AND LIABILITIES 
35
STATEMENTS OF OPERATIONS 
37
STATEMENTS OF CHANGES IN NET ASSETS 
39
FINANCIAL HIGHLIGHTS 
43
NOTES TO FINANCIAL STATEMENTS 
50
REPORT OF INDEPENDENT REGISTERED PUBLIC ACCOUNTING FIRM 
58
OTHER INFORMATION 
59
INFORMATION ON BOARD OF TRUSTEES AND OFFICERS 
62
GUGGENHEIM INVESTMENTS PRIVACY POLICIES 
65
 
 
October 31, 2015
 
Dear Shareholder: 
 
Security Investors, LLC (the “Investment Adviser”) is pleased to present the annual shareholder report for a selection of our exchange-traded funds (“ETFs”) for the 12-month period ended October 31, 2015. 
 
The Investment Adviser is part of Guggenheim Investments, which represents the investment management businesses of Guggenheim Partners, LLC (“Guggenheim”), a global, diversified financial services firm. 
 
Guggenheim Funds Distributors, LLC is the distributor of the Funds. Guggenheim Funds Distributors, LLC is affiliated with Guggenheim and the Investment Adviser. 
 
We encourage you to read the Economic and Market Overview, which follows this letter, followed by the Manager’s Analysis for each ETF. 
 
Sincerely, 


DEAR SHAREHOLDER 
2
ECONOMIC AND MARKET OVERVIEW 
3
FEES AND EXPENSES 
5
MANAGER'S ANALYSIS 
6
PORTFOLIO SUMMARY 
7
SCHEDULE OF INVESTMENTS 
8
STATEMENT OF ASSETS AND LIABILITIES 
14
STATEMENT OF OPERATIONS 
15
STATEMENTS OF CHANGES IN NET ASSETS 
16
FINANCIAL HIGHLIGHTS 
17
NOTES TO FINANCIAL STATEMENTS 
18
REPORT OF INDEPENDENT REGISTERED PUBLIC ACCOUNTING FIRM 
24
OTHER INFORMATION 
25
INFORMATION ON BOARD OF TRUSTEES AND OFFICERS 
28
GUGGENHEIM INVESTMENTS PRIVACY POLICIES 
31
 
 
October 31, 2015
 
Dear Shareholder: 
 
 
The Investment Adviser is part of Guggenheim Investments, which represents the investment management businesses of Guggenheim Partners, LLC (“Guggenheim”), a global, diversified financial services firm. 
 
Guggenheim Funds Distributors, LLC is the distributor of the Fund. Guggenheim Funds Distributors, LLC is affiliated with Guggenheim and the Investment Adviser. 
 
We encourage you to read the Economic and Market Overview, which follows this letter, followed by the Manager’s Analysis for the ETF. 
 
Sincerely, 


