Dear Praxis shareholder: The strength of U.S. equity
markets faltered in as investors digested two significant changes a reversal in Federal Reserve policy and Chinas slowing growth. A third factor, the precipitous decline in oil and other energy prices, is also shifting the balance
of profitability from suppliers of energy products to energy users. For the past several years, market watchers were concerned that accommodative Federal Reserve
policy was the primary force behind investor confidence and that a change in Fed policy would negatively affect the markets. By the Feds December meeting, the U.S. economy made enough progress during the year with modest economic growth,
steady declines in unemployment, and benign inflation that the Fed began raising its short-term target interest rate. Now that the Fed commenced a reversal of their strategy, investors quickly shifted focus to China and the potentially wide-ranging
impacts of a slowdown in its growth. Despite investment markets that occasionally disappoint like they did in Praxis Mutual Funds continues to work for
positive impacts through our investments and advocacy with companies. The Praxis Intermediate Income Fund investment managers demonstrated their leadership by investing in bonds with both financial strength and positive climate and community
impacts. Our stewardship investing team labored alongside other like-minded investors to advance our goals for environmental sustainability and to end the scourge of modern slavery.
In Praxis Mutual Funds partnered with the Calvert Foundation, a leader in community development investing, to channel more than million into high impact
investments across the country and around the world. These investments representing approximately percent of each fund in the Praxis Mutual Fund family help open doors of economic opportunity to thousands of disadvantaged individuals
and communities. For many investors today, making money isnt enough. Like you, they also want their investments to make a difference. They want to invest in
what matters. Thats why shareholder advocacy, company screening, and community development investing are essential to our work at Praxis. Our goal is to invest in ways that make a positive impact on communities locally and around the world
and we are able to do that because of you. Thank you for entrusting your investments with Praxis Mutual Funds.
Sincerely,
