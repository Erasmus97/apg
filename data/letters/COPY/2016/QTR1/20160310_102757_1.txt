Dear Fellow Shareholders:
We are pleased to present you with this annual report for Value Line Premier Growth Fund, Inc., Value Line Mid Cap Focused Fund, Inc., Value Line Income and Growth Fund, Inc. and Value Line Larger Companies Focused Fund, Inc. (individually, a Fund and collectively, the Funds) for the months ended
During the annual period, equities generated mixed absolute returns, and two of the four Funds lagged their respective benchmark index on a relative basis. Still, the annual period was highlighted by each of the four equity and hybrid Value Line Funds being recognized for their long-term performance and attractive risk profiles.

Value Line Premier Growth Fund, Inc. outpaced the category average return of its peers for the ten-year period ended as noted by leading independent mutual fund advisory service Lipper (multi-cap growth category). Lipper also awarded its top Lipper Leader rating of to the Fund for Preservationi versus its peers overall as of As measured by the Fund outpaced the category average return of its peers for the one-, five- and ten-year periods ended (mid-cap growth category). Additionally, Morningstar gave the Fund an overall Risk rating of Below Averageii as of


The Value Line Mid Cap Focused Fund, Inc., formerly The Value Line Fund, Inc., outpaced the category average return of its peers for the one-, three- and five-year periods ended as noted by leading independent mutual fund advisory service Lipper (mid-cap growth category). Lipper also awarded its top Lipper Leader rating of to the Fund for Preservationiii and Tax Efficiencyiii versus its peers overall as of As measured by the Fund outpaced the category average return of its peers for the one-, three- and five-year periods ended (mid-cap growth category). Additionally, the Fund was given an overall Risk rating of Lowiv by Morningstar as of


Value Line Income and Growth Fund, Inc. outpaced the category average return of its peers for the one-, three-, five- and ten-year periods ended as noted by leading independent mutual fund advisory service Lipper (mixed-asset target allocation moderate category). Lipper also awarded its top Lipper Leader rating of to the Fund for Total Returnv and Consistent Returnv versus its peers overall as of As measured by the Fund outpaced the category average return of its peers for the one-, three-, five- and ten-year periods ended (moderate allocation category). Additionally, the Fund earned an overall four-star rating from in the moderate allocation category among funds as of based on risk-adjusted returns. Morningstar gave the Fund an overall Risk rating of Below Average.vi


Value Line Larger Companies Focused Fund, Inc., formerly Value Line Larger Companies Fund, Inc., outpaced the category average return of its peers for the one-, three- and five-year periods ended as noted by leading independent mutual fund advisory service Lipper (larger-cap growth category). Lipper also awarded its top Lipper Leader rating of to the Fund for Preservationvii versus its peers overall as of As measured by the Fund outpaced the category average return of its peers for the one-, three- and five-year periods ended (large growth category). Additionally, the Fund earned an overall four-star rating from in the large growth category among funds as of based on risk-adjusted returns. Morningstar gave the Fund an overall Risk rating of Below Averageviii by as of

On the following pages, the Funds portfolio managers discuss the management of their respective Funds during the annual period. The discussions highlight key factors influencing recent performance of the Funds. You will also find a schedule of investments and financial statements for each of the Funds.
Before reviewing the performance of your individual mutual fund investment(s), we encourage you to take a brief look at the major factors affecting the financial markets during the months ended especially given the newsworthy events of the annual period. With the significant shifts during in several long-standing drivers of the capital markets, we also invite you to take this time to consider a broader diversification strategy by including additional Value Line Funds in your investment portfolio. You can find out more about the entire family of Value Line Funds at our website, www.vlfunds.com.
Economic Review
The capital markets were focused for much of the annual period on possible tightening by the Federal Reserve (the Fed). While improving job numbers were supportive of a rise in rates, the Feds inflation target of remained elusive. Indeed, the headline Consumer Price Index (CPI) rose just year over year before seasonal adjustment as of Core inflation, which excludes food and energy, was up in from a year earlier. Notably, while the food segment of the CPI increased during the months ended the energy segment of the CPI, despite rising in the months of May, June, July and declined over the same span.
Amidst this backdrop, the long-awaited first hike of short-term interest rates in more than nine years was finally announced at the Feds meeting. The increase in the targeted federal funds rate was a modest basis points. (A basis point is of a percentage point.)










TABLE OF CONTENTS

Presidents Letter (unaudited)(continued)
