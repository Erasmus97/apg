Dear Shareholder, 
The US economy
expanded at a moderate pace during the twelve-month period ended September 30, 2016. Economic activity overseas was mixed and generally lackluster. Against this backdrop, US and international equities generated solid results. The global bond market
also rallied during the twelve-month period. 
12 Months in Review 
For the twelve-month period ended September 30, 2016, US stocks, as measured by the Standard &amp; Poor’s 500 Index, gained 15.43%. Two measures of stock
performance in developed international and global markets, the MSCI EAFE (Europe, Australasia and Far East) Index and the MSCI World Index, returned 6.52% and 11.36%, respectively, in dollar-denominated terms. Elsewhere, the MSCI Emerging Markets
Index rose 16.78%. With respect to bonds, the Bloomberg Barclays US Credit Index gained 8.30%, whereas the Bloomberg Barclays Global High Yield Index rose 13.51%. The Bloomberg Barclays US Government Bond Index returned 4.00%, while the broader bond
market index, as measured by the Bloomberg Barclays US Aggregate Bond Index, gained 5.19%. The Bloomberg Barclays Global Aggregate Index returned 8.83%. 
US gross
domestic product (“GDP”), which is an approximation of the value of all goods and services produced in the country, the broadest measure of economic activity and a key indicator of economic performance, expanded at a revised 0.9%
annualized pace during the fourth quarter of 2015. GDP then grew at an annualized pace of 0.8% and 1.4% during the first and second quarters of 2016, respectively. In addition, the US Commerce Department’s initial reading showed that GDP —
released after the twelve-month period had ended — grew at an annualized pace of 2.9% for the third quarter of 2016. 
At its meeting in December 2015, the
Federal Reserve (the “Fed”) raised interest rates for the first time in nearly a decade. More specifically, the Fed increased the federal funds rate from a range between 0% and 0.25% to a range between 0.25% and 0.50%. However, since that
time the Fed has paused increases in interest rates and has downgraded its projections for growth in the US for 2016. In its official statement following its meeting in September 2016 the Fed said, “The Committee judges that the case for an
increase in the federal funds rate has strengthened but decided, for the time being, to wait for further evidence of continued progress toward its

 
Table of Contents

objectives. The stance of monetary policy remains accommodative, thereby supporting further improvement in labor market conditions and a return to 2 percent inflation.” 
Outlook 
Nine years since the financial crisis
started, monetary easing is continuing with few signs of an end in sight. This appears to be especially true in a post-Brexit world rife with economic uncertainty and generally weak growth around the world. Against this backdrop, investor sentiment
is likely to be challenged at times. Other questions facing investors are the impact from the November election, the Fed’s plans to normalize monetary policy and a host of geopolitical issues. 
In such an environment, we expect investors will be in for a choppy ride. However, in our view it won’t be without opportunity, especially for those with long term
outlooks, a healthy risk appetite and the latitude to invest actively. As always, we will continue to conduct extensive research and focus on quality and sustainability to help our shareholders navigate the many uncertainties around the globe. 
Please note that on October 1, 2016, Allianz Global Investors Fund Management LLC (“AGIFM”), previously the Funds’ investment manager, merged with and
into Allianz Global Investors U.S. LLC (“AllianzGI U.S.”), previously sub-adviser to certain Funds. In connection with this merger, AllianzGI U.S. succeeded AGIFM as the investment manager of the Funds. NFJ Investment Group LLC remains a
sub-adviser to certain Funds. The merger did not result in any personnel changes and otherwise had no practical effect on the management of the Funds. 
On behalf of
Allianz Global Investors U.S. LLC and NFJ Investment Group LLC, the sub-adviser to certain funds, thank you for investing with us. We encourage you to consult with your financial advisor and to visit our website, us.allianzgi.com, for additional
information. We remain dedicated to serving your investment needs. 
Sincerely, 


