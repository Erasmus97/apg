Dear Shareholder: 
 
12-month
 
During the reporting period, the US economy experienced modest growth. Labor markets were healthy, and consumer confidence rose. The housing market brightened
somewhat, as momentum continued for the new home market. The Federal Reserve kept interest rates unchanged at its September meeting, but pointed to the strong possibility of a rate hike in December. Internationally, concerns over Brexit—the
term used to represent Britain’s decision to leave the European Union—remained in the spotlight. 
 
Equity markets in the US were firmly in positive territory at the end of the reporting period, as US stocks posted strong gains. European stocks struggled earlier, but found some traction in the third quarter.
Asian markets also advanced, and emerging markets rose sharply. 
 
US fixed
income markets experienced overall gains. High yield bonds posted very strong results. Corporate bonds and Treasuries also performed well. Accommodative monetary policy by central banks helped lift global bond markets. 
 
Given the uncertainty in today’s investment environment, we believe that active
professional portfolio management offers a potential advantage. Active managers often have the knowledge and flexibility to find the best investment opportunities in the most challenging markets. 
 
Even so, it’s best if investment decisions are based on your long-term goals
rather than on short-term market and economic developments. We also encourage you to work with an experienced financial advisor who can help you set goals, determine your tolerance for risk, build a diversified plan that’s right for you, and
make adjustments when necessary. 
 
By having Prudential Investments help you
address your goals, you gain the advantage of asset managers that also manage money for many major corporations and pension funds around the world. That means you benefit from the same expertise, innovation, and attention to risk demanded by
today’s most sophisticated investors. 
 
Thank you for choosing our
family of funds. 
 
Sincerely, 


Dear Shareholder: 
 
We hope you find the annual report for the Prudential QMA Defensive Equity Fund
informative and useful. The report covers performance for the 12-month period that ended October 31, 2016. 
 
During the reporting period, the US economy experienced modest growth. Labor markets were healthy, and consumer confidence rose. The housing market brightened somewhat, as momentum continued for the new home
market. The Federal Reserve kept interest rates unchanged at its September meeting, but pointed to the strong possibility of a rate hike in December. Internationally, concerns over Brexit—the term used to represent Britain’s decision to
leave the European Union—remained in the spotlight. 
 
Equity markets in
the US were firmly in positive territory at the end of the reporting period, as US stocks posted strong gains. European stocks struggled earlier, but found some traction in the third quarter. Asian markets also advanced, and emerging markets rose
sharply. 
 
US fixed income markets experienced overall gains. High yield
bonds posted very strong results. Corporate bonds and Treasuries also performed well. Accommodative monetary policy by central banks helped lift global bond markets. 
 
Given the uncertainty in today’s investment environment, we believe that active
professional portfolio management offers a potential advantage. Active managers often have the knowledge and flexibility to find the best investment opportunities in the most challenging markets. 
 
Even so, it’s best if investment decisions are based on your long-term goals
rather than on short-term market and economic developments. We also encourage you to work with an experienced financial advisor who can help you set goals, determine your tolerance for risk, build a diversified plan that’s right for you, and
make adjustments when necessary. 
 
By having Prudential Investments help you
address your goals, you gain the advantage of asset managers that also manage money for many major corporations and pension funds around the world. That means you benefit from the same expertise, innovation, and attention to risk demanded by
today’s most sophisticated investors. 
 
Thank you for choosing our
family of funds. 
 
Sincerely, 


