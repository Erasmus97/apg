Dear Shareholder: 
 
We hope you find the annual report for the Prudential Short Duration High Yield Income
Fund informative and useful. The report covers performance for the 12-month period that ended August 31, 2016. 
 
During the period, equity and fixed income markets achieved positive returns in the US, after a highly volatile and dramatic second quarter. Brexit—the term used to represent Britain’s decision to leave
the European Union—triggered a sharp sell-off in global stocks. Initial losses were steep, but positive investor sentiment prevailed as US equities rebounded quickly. European stocks were negatively impacted, while Asian stocks were
generally less affected. In the wake of Brexit, US Treasuries experienced a price rally, sending interest rate yields to all-time lows. 
 
While uncertainty lingers over the health of the global economy, the US economy grew, but at a very slow pace. Labor markets turned up sharply in June, after
disappointing numbers in May. The Federal Reserve kept rates unchanged at their July meeting but had a hawkish tone, citing strength in consumer spending and a tightening labor market. 
 
Given the volatility in today’s investment environment, we believe that active professional portfolio management offers a potential
advantage. Active managers often have the knowledge and flexibility to find the best investment opportunities in the most challenging markets. 
 
Even so, it’s best if investment decisions are based on your long-term goals rather than on short-term market and economic developments. We also encourage you
to work with an experienced financial advisor who can help you set goals, determine your tolerance for risk, and build a diversified plan that’s right for you and make adjustments when necessary. 
 
By having Prudential Investments help you address your goals, you gain the advantage
of asset managers that also manage money for many major corporations and pension funds around the world. That means you benefit from the same expertise, innovation, and attention to risk demanded by today’s most sophisticated investors.

 
Thank you for choosing our family of funds. 
 
Sincerely, 


Dear Shareholder: 
 
We hope you find the annual report for the Prudential High Yield Fund informative and
useful. The report covers performance for the 12-month period that ended August 31, 2016. 
 
During the period, equity and fixed income markets achieved positive returns in the US, after a highly volatile and dramatic second quarter. Brexit—the term used to represent Britain’s decision to leave
the European Union—triggered a sharp sell-off in global stocks. Initial losses were steep, but positive investor sentiment prevailed as US equities rebounded quickly. European stocks were negatively impacted, while Asian stocks were
generally less affected. In the wake of Brexit, US Treasuries experienced a price rally, sending interest rate yields to all-time lows. 
 
While uncertainty lingers over the health of the global economy, the US economy grew, but at a very slow pace. Labor markets turned up sharply in June, after
disappointing numbers in May. The Federal Reserve kept rates unchanged at its July meeting but had a hawkish tone, citing strength in consumer spending and a tightening labor market. 
 
Given the volatility in today’s investment environment, we believe that active professional portfolio management offers a potential
advantage. Active managers often have the knowledge and flexibility to find the best investment opportunities in the most challenging markets. 
 
Even so, it’s best if investment decisions are based on your long-term goals rather than on short-term market and economic developments. We also encourage you
to work with an experienced financial advisor who can help you set goals, determine your tolerance for risk, and build a diversified plan that’s right for you and make adjustments when necessary. 
 
By having Prudential Investments help you address your goals, you gain the advantage
of asset managers that also manage money for many major corporations and pension funds around the world. That means you benefit from the same expertise, innovation, and attention to risk demanded by today’s most sophisticated investors.

 
Thank you for choosing our family of funds. 
 
Sincerely, 


