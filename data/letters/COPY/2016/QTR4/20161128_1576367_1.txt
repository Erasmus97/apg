Dear Shareholder,




We are pleased to report our economic and financial market perspectives and the investment activities for the Baywood ValuePlus Fund (the "Fund") for the ten months ended September, The Fund is a large-capitalization value fund that purchases primarily dividend-paying companies traded on U.S. exchanges and uses SKBA Capital Management's ("SKBA") Relative Dividend Yield (RDY) discipline as the initial valuation framework. We believe that RDY points out attractive investment opportunities, not simply among companies with above-average dividend yield, but just as importantly among stocks for which low expectations are already discounted into their valuations at the time of purchase. This provides the potential for attractive capital appreciation opportunities, while offering desirable downside protection.




The ten month period ending September marked a new regime in the market. The robust market advances of the prior years abated and we have begun a period marked with uncertainty which precipitates wild market swings at just the slightest hint of disappointment. This was twice on display in The first instance occurred in the first month and a half of the year when the broad market lost as much as What struck us, however, was the nature of the sell-off not just the sell-off itself. A number of high-flying stocks responsible for advancing the indexes over the last years, companies like Facebook, Amazon, Netflix and Google (Alphabet) declined the most, as did other, less recognizable, highly valued companies. At the same time companies we tend to favor and own suddenly came into favor. These less exciting, stable, high cash flow yielding, yet out-of-favor companies declined much less than the recent high-flyers. During these early weeks of the year the Baywood ValuePlus fund outperformed its benchmarks meaningfully.




Not surprisingly, sectors commonly believed to be "defensive", many of which have high dividend yields like utilities and telecom also outperformed. We happen to be underweight telecom and utilities for the opposite reason so many others find them attractive. The historical tendency of these sectors to be defensive in periods of extreme volatility coupled with low worldwide investment yields has resulted in extraordinary demand for these expensive yet relatively high-yielding investments. Global bond substitution effects in the search for yield have resulted in valuations inconsistent with what we view as generally poor company fundamentals. As value investors having experienced many cycles, we can confidently state that market regimes change over time. Yesterday's low beta, low volatility, defensive, perceived to be safe investments are often tomorrow's blow-ups. Electric utilities, for example, are currently more highly valued than they arguably have ever been while their dividend yields are near record lows for the industry. Out-of-favor, they are not. But what about fundamentals? Are the high valuations associated with improving fundamentals? With valuations at record highs and dividend yields at record lows, what are investors paying for if not extraordinary fundamental improvement? Alas, this is where we take exception. The utility business model is under pressure from more efficient households and enterprises. Power demand as a function of GDP is at an all-time low, with conceivably more room to squeeze out efficiencies. Renewable resources put strain on the grid as well as lower the price of electricity without commensurate decreases in cost. Furthermore, with the recent strides in storage technology (batteries), prospective profitability looks to decrease, not increase.




This is where we differ from many of our contemporaries. As value investors we simply look for stocks that are out-of-favor, where the relative dividend yield is high due to a depressed stock price, not purely by dividend policy. Dividend policies in and of themselves can be highly misleading. For example, before the crash in oil prices, several exploration and production companies that have no business paying a dividend, let alone one high enough to be attractive, had high absolute dividend yields. This was not due to their stock prices being depressed or their cash flows being in excess of their capital needs, it was due to managements trying to appeal to a certain investor base looking for yield. When the price of the commodity dropped and no offsetting business like refining was able to mitigate losses tied to production; dividends had to be slashed and with it went the price of their shares.




Instead we look for sustainable businesses, often with market leading positions in their respective industries that have strong enough balance sheets to support a healthy dividend policy and have become temporarily out-of-favor. Companies like Packaging Corp, Albemarle, Baxter and Chubb all contributed meaningfully in recent months, yet would not otherwise be considered "defensive" under recently accepted definitions. These companies' historic betas may have been somewhat elevated at time of purchase, and would not have typically fit in a "low-vol" strategy, yet in fact proved to be much more defensive due to valuations low enough to provide downside protection and capture market upside during a recovery.




Market regimes change and what was true last year may no longer be this year. In the latter half of the fund's fiscal year, utility stocks began to fall out of favor. Bond substitution, the only leg upon which such securities were priced, vanished with the Fed signaling a change in policy. Technical factors driving up prices of traditionally high yielding sectors ceased, which helped contribute meaningfully to the fund's returns over the second half of the fiscal year. ValuePlus' absolute returns will not always keep up with very robust market advances as we experienced between and Under more normal environments and those with heightened volatility as we recently experienced, the increase in our investment opportunity set resulted in the fund performing














































BAYWOOD VALUEPLUS FUND

A MESSAGE TO OUR SHAREHOLDERS (Unaudited)

Dear Shareholder,




This letter covering the twelve months ended September is SKBA Capital Management's ("SKBA") first regarding the economic and financial market perspectives and the investment activities for the Baywood SociallyResponsible Fund (the "Fund"). This report also marks a new chapter in the organization of the Fund as SKBA was pleased to be elected, effective January the Advisor to the renamed Baywood SociallyResponsible Fund.




We have long desired to become the Advisor to the Fund in order to align our goals with those of our clients and shareholders through every aspect of fund management. During the transition, however, large client flows caused a material negative impact on Fund performance from November of through February of Yet SKBA has remained steadfast in its application of our investment approach to meet the needs of shareholders who want environmental, social, and governance (ESG) criteria applied to the stock selection decisions in the Fund and we are very excited at the Fund's prospects going forward. The Fund is a mid-to-large capitalization value-oriented portfolio of stock holdings selected from a universe of stocks created through the application of social screens and assessments of the ESG profile of each company. Among these stocks, we further evaluate and assess each prospective holding's valuation and fundamental business attraction to determine the current portfolio holdings.




As the new Advisor to the Fund we will do everything within our control to ensure we are aware of client commitments and intentions. Having completed the transition, we are looking forward to a fresh start as Advisor and as shareholders as well together with outside shareholders in the Fund. Additionally, we believe the Fund's return characteristics are more represented by the eight full months since the completion of the conversion.




In the most recently ended quarter, the Fund's returns meaningfully surpassed all of our benchmarks; a reminder that our investment horizons don't often match up with the clearly defined quarterly reporting that is commonplace. We manage for the long-term yet are measured in quarters. As value investors, there are periods of strong market advances where the Fund may not keep up; there are also periods in which the value of the securities we purchase tend to be unrecognized. These tend to be followed by periods in which those securities purchased do become recognized. Quite often those take place in times of either normal or volatile returns. While volatility may be difficult to "stomach", we seek it in order to take advantage of mis-pricings. The last year or so has been such an environment and we expect it to continue to provide opportunities over a considerable period.




During the recently ended quarter, consumer staples, an underperforming sector in the Russell Value Index, was flat in the benchmark. Not an unsurprising result given the high valuations and poor fundamentals. Our underweight position in this expensive sector contributed to returns as did the individual holdings which returned more than In a quarter where the portfolio outperformed in numerous ways, this sector's performance is more indicative of our valuation discipline and philosophy than of anything else. Consumer Staples, in general, are priced for perfection. The global hunt for yield and stability has pushed valuations to near record highs, reducing yields to record lows. Further, growth in the sector is pressured as emerging economies, once drivers of growth, slow down and pressure revenues. These two conditions make the decision to underweight an easy one, yet the two staple stocks the strategy does own - Pepsi and Procter &amp; Gamble - were some of the two best performing stocks in the sector.




Both Pepsi and Procter &amp; Gamble have divergent sets of fundamentals from other stocks in the sector. Where, on one pass, one might look at reported earnings and conclude they are expensive; normalizing the earnings yields a much different perspective. Procter &amp; Gamble is recovering from years of underperformance as smaller, more focused companies willing to sacrifice margins for market share have eaten away at its lead. Yet it is in the process of completing its biggest change in strategy since the Gillette acquisition by divesting of nearly of its slower growing, lower margin brands. The resulting company will be much more focused and the simple stemming of market share losses should result in large fundamental improvements. Pepsi, on the other hand has been widely successful in maintaining its market share in a competitive environment where carbonated, sugary drinks have lagged consumer preferences. Additionally, we have always liked its strategy of diversification with its snack foods segment. Indra Nooyi, to the extent that she is able, is shifting the company's beverage mix away from carbonated soft drinks and shifting its snacks mix to incorporate more healthy alternatives. This is not only good for consumer health and diets in general; it is also proving to be good for the company's financials. While the majority of stocks in the staples category suffer from high valuations and deteriorating fundamentals, these two companies have reasonable valuations and are showing improving fundamentals, which speaks to our philosophy. We don't chase sectors because somehow 'Value' has become defined or bound by certain sectors or stocks. We purchase stocks that are out-of-favor in which fundamentals aren't likely to be as bad as the valuation would suggest. It's not always what you invest in that will determine how you perform; it's also what you avoid. In this case, it was both.




Stocks in the utilities sector suffer from the same general condition as consumer staples, albeit to a much higher degree. Here, fundamentals are unambiguously poor and relative valuations are near stratospheric, the combination of which are the reasons we






















































BAYWOOD SOCIALLYRESPONSIBLE FUND

A MESSAGE TO OUR SHAREHOLDERS (Unaudited)

Dear Shareholders:

During the period from September through September we continued to manage the Gurtin National Municipal Opportunistic Value Fund and the Gurtin California Municipal Opportunistic Value Fund (the Gurtin Opportunistic Value Funds) under our disciplined investment approach of opportunistically deploying capital only as attractive investments become available. During this same period, we saw a continued decline in municipal bond yields which made for a difficult investment environment for the Gurtin Opportunistic Value Funds even following an increase in the fed funds rate by the Federal Reserve (the Fed) in December, which was the first rate increase seen since

Recognizing the importance of the Fed's actions in an eventual reversal of interest rates' downward trajectory, we have remained focused on the Fed for important clues regarding its approach to monetary policy. We believe that such clues were clearly provided in Fed actions and discussions which seemed to indicate its intentions to raise rates, albeit slowly, as long as the domestic economy continues to perform in a similar manner to and year-to-date.

In terms of the trajectory of Fed tightening, our research and the Fed's actions and projections lead us to believe that: the Fed will likely only raise rates when the markets are reasonably calm, the Fed wants to make sure that a fed funds rate increase is discounted in the markets prior to any move, and the trajectory of rates will likely be slow and deliberate, but subject to change based upon future market dynamics. Notably, the Fed was willing to be patient in October, and restrain from a rate hike during a period of significant volatility and uncertainty in overseas markets, the bond market absorbed the December rate hike without a hiccup, and the terminal fed funds rate is not expected to be reached until after

While we remain reticent to predict the path for interest rates, we believe a rising rate backdrop will likely result in a somewhat rocky and volatile path for intermediate and longer term municipal yields. Given the strained liquidity in the markets in general, and the municipal market in particular, we expect that any increase in volatility will result in spread widening not only between municipals and Treasuries, but also in municipal credit spreads which, if history is a guide, will be somewhat random in nature. We continue to believe that the Gurtin Opportunistic Value Funds are well positioned for rising rates, and look forward to being able to use market dynamics such as strained liquidity and randomness of spread widening to our advantage by exploiting opportunities presented to the Gurtin Opportunistic Value Funds by a volatile market.

Sincerely,

Dear Shareholders:

During the period from September through September we continued to manage the Gurtin National Municipal Opportunistic Value Fund and the Gurtin California Municipal Opportunistic Value Fund (the Gurtin Opportunistic Value Funds) under our disciplined investment approach of opportunistically deploying capital only as attractive investments become available. During this same period, we saw a continued decline in municipal bond yields which made for a difficult investment environment for the Gurtin Opportunistic Value Funds even following an increase in the fed funds rate by the Federal Reserve (the Fed) in December, which was the first rate increase seen since

Recognizing the importance of the Fed's actions in an eventual reversal of interest rates' downward trajectory, we have remained focused on the Fed for important clues regarding its approach to monetary policy. We believe that such clues were clearly provided in Fed actions and discussions which seemed to indicate its intentions to raise rates, albeit slowly, as long as the domestic economy continues to perform in a similar manner to and year-to-date.

In terms of the trajectory of Fed tightening, our research and the Fed's actions and projections lead us to believe that: the Fed will likely only raise rates when the markets are reasonably calm, the Fed wants to make sure that a fed funds rate increase is discounted in the markets prior to any move, and the trajectory of rates will likely be slow and deliberate, but subject to change based upon future market dynamics. Notably, the Fed was willing to be patient in October, and restrain from a rate hike during a period of significant volatility and uncertainty in overseas markets, the bond market absorbed the December rate hike without a hiccup, and the terminal fed funds rate is not expected to be reached until after

While we remain reticent to predict the path for interest rates, we believe a rising rate backdrop will likely result in a somewhat rocky and volatile path for intermediate and longer term municipal yields. Given the strained liquidity in the markets in general, and the municipal market in particular, we expect that any increase in volatility will result in spread widening not only between municipals and Treasuries, but also in municipal credit spreads which, if history is a guide, will be somewhat random in nature. We continue to believe that the Gurtin Opportunistic Value Funds are well positioned for rising rates, and look forward to being able to use market dynamics such as strained liquidity and randomness of spread widening to our advantage by exploiting opportunities presented to the Gurtin Opportunistic Value Funds by a volatile market.

Sincerely,

Dear Shareholders:

During the fiscal period* ended September we continued to manage the Gurtin National Municipal Intermediate Value Fund and the Gurtin California Municipal Intermediate Value Fund (the Gurtin Intermediate Value Funds) with a focus on maximizing risk-adjusted long-term income while seeking to preserve capital and liquidity. During this same period, we saw a continued decline in municipal bond yields which made for a difficult investment environment for the Gurtin Intermediate Value Funds even following an increase in the fed funds rate by the Federal Reserve (the Fed) in December, which was the first rate increase seen since

Recognizing the importance of the Fed's actions in an eventual reversal of interest rates' downward trajectory, we have remained focused on the Fed for important clues regarding its approach to monetary policy. We believe that such clues were clearly provided in Fed actions and discussions which seemed to indicate its intentions to raise rates, albeit slowly, as long as the domestic economy continues to perform in a similar manner to and year-to-date.

In terms of the trajectory of Fed tightening, our research and the Fed's actions and projections lead us to believe that: the Fed will likely only raise rates when the markets are reasonably calm, the Fed wants to make sure that a fed funds rate increase is discounted in the markets prior to any move, and the trajectory of rates will likely be slow and deliberate, but subject to change based upon future market dynamics. Notably, the Fed was willing to be patient in October, and restrain from a rate hike during a period of significant volatility and uncertainty in overseas markets, the bond market absorbed the December rate hike without a hiccup, and the terminal fed funds rate is not expected to be reached until after

While we remain reticent to predict the path for interest rates, we believe a rising rate backdrop will likely result in a somewhat rocky and volatile path for intermediate municipal yields. Given the strained liquidity in the markets in general, and the municipal market in particular, we expect that any increase in volatility will result in spread widening not only between municipals and Treasuries, but also in municipal credit spreads which, if history is a guide, will be somewhat random in nature. We look forward to being able to use market dynamics such as strained liquidity and randomness of spread widening to our advantage by exploiting opportunities presented to the Gurtin Intermediate Value Funds by a volatile market.

Sincerely,

Dear Shareholders:

During the fiscal period* ended September we continued to manage the Gurtin National Municipal Intermediate Value Fund and the Gurtin California Municipal Intermediate Value Fund (the Gurtin Intermediate Value Funds) with a focus on maximizing risk-adjusted long-term income while seeking to preserve capital and liquidity. During this same period, we saw a continued decline in municipal bond yields which made for a difficult investment environment for the Gurtin Intermediate Value Funds even following an increase in the fed funds rate by the Federal Reserve (the Fed) in December, which was the first rate increase seen since

Recognizing the importance of the Fed's actions in an eventual reversal of interest rates' downward trajectory, we have remained focused on the Fed for important clues regarding its approach to monetary policy. We believe that such clues were clearly provided in Fed actions and discussions which seemed to indicate its intentions to raise rates, albeit slowly, as long as the domestic economy continues to perform in a similar manner to and year-to-date.

In terms of the trajectory of Fed tightening, our research and the Fed's actions and projections lead us to believe that: the Fed will likely only raise rates when the markets are reasonably calm, the Fed wants to make sure that a fed funds rate increase is discounted in the markets prior to any move, and the trajectory of rates will likely be slow and deliberate, but subject to change based upon future market dynamics. Notably, the Fed was willing to be patient in October, and restrain from a rate hike during a period of significant volatility and uncertainty in overseas markets, the bond market absorbed the December rate hike without a hiccup, and the terminal fed funds rate is not expected to be reached until after

While we remain reticent to predict the path for interest rates, we believe a rising rate backdrop will likely result in a somewhat rocky and volatile path for intermediate municipal yields. Given the strained liquidity in the markets in general, and the municipal market in particular, we expect that any increase in volatility will result in spread widening not only between municipals and Treasuries, but also in municipal credit spreads which, if history is a guide, will be somewhat random in nature. We look forward to being able to use market dynamics such as strained liquidity and randomness of spread widening to our advantage by exploiting opportunities presented to the Gurtin Intermediate Value Funds by a volatile market.

Sincerely,
