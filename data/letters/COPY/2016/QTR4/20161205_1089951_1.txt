DEAR SHAREHOLDER: 
®
The Bloomberg Barclays U.S. Aggregate Bond Index, a broad U.S. bond market benchmark, returned 5.2% for the fiscal year ended September 30, 2016. Interest rates and
credit spreads gyrated during 2015 and 2016, at times putting some pressure on bond prices. Investors’ appetite for risk declined sharply during the latter part of 2015 and early 2016, before rebounding significantly as oil prices recovered,
reflected in the 12.7% return for the Bloomberg Barclays U.S. Corporate High Yield Index. 
We are excited to announce as of October 1, the AMG Funds family of
mutual funds fully integrated the former Aston Funds mutual funds. AMG Funds and Aston Funds shareholders will now have access to the differentiated solutions of AMG Funds, which represents a single point of access to one of the largest line-ups of
boutique managers and products in the world. 
AMG Funds appreciates the privilege of providing investment solutions to you and your clients. Our foremost goal at AMG
Funds is to provide investment solutions that help our shareholders successfully reach their long-term investment goals. By partnering with AMG’s affiliated investment boutiques, AMG Funds provides access to a distinctive array of
actively-managed return-oriented investment strategies.

Additionally, we oversee and distribute a number of complementary open-architecture mutual funds subadvised by unaffiliated
investment managers. We thank you for your continued confidence and investment in AMG Funds. You can rest assured that under all market conditions our team is focused on delivering excellent investment management services for your benefit. 
Respectfully, 


DEAR FELLOW SHAREHOLDERS,
 
When we wrote you one year ago, we detailed the challenges that historically slow global growth levels, a collapse in commodity prices and the rise of the
U.S. Dollar presented to our Funds. While these headwinds remain, we believe that the Fund’s portfolio companies are led by capable management teams that continue to leverage their financial flexibility to expand niche dominance and
reinvest cash flows prudently.
 
Our companies often find themselves at the intersection of
challenge and opportunity and we believe that they have encountered plenty of each recently. They manage and invest in their businesses against a current backdrop of slow global economic growth and anemic organic revenue growth opportunities that we
contend create challenges for them when modeling, budgeting and considering large capital allocation decisions. Many of them also have clean balance sheets, consistent cash flows and access to capital that, we assert, have allowed them to invest in
internal projects with attractive returns, make bolt-on acquisitions and support productivity initiatives. We are pleased with the investment opportunities on which our companies continued to capitalize during the period, and their response to
ongoing challenges.
 
Two current conditions that we believe provide an array of challenges and
opportunities are global trade and the U.S. Dollar’s strength versus the currencies of our trading partners. Trade has been a dominant issue in the U.S. presidential election, as the free trade agreements of the last two decades have positioned
the issue as a point of differentiation between the two candidates. Politics and trade have also become apparent as key issues in the United Kingdom, Russia and China recently. The Fund often includes a number of U.S.-based companies with
substantial global operations. We remain diligent in our assessment of the environment in which our companies operate and continue to analyze the potential impact on them of any changes in global trade behavior.
 
We have seen the impact of a strong U.S. Dollar during the last couple of years on our
companies’ reported revenue, balance sheets and their capital allocation decisions. While these are all challenges, we believe that the strong Dollar might create opportunities for some of our U.S.-based businesses to make bolt-on acquisitions
that might
prove additive to production, distribution, services or product lines. The financial flexibility we seek when qualifying companies for investment traces
its roots to balance sheets and discretionary cash flow but the global profile of many of our businesses may provide further investment opportunity in the face of slow global growth and volatile markets. We will continue to monitor our
companies’ ability to capitalize on the current value of the U.S. Dollar.
 
FUND PERFORMANCE
 
®
 
Over the trailing twelve-month period ending September 30,
2016, much of our underperformance was driven by our stock selection within the technology and materials &amp; processing sectors, led by our positions in Diebold, Inc. (DBD) and Intrepid Potash, Inc. (IPI). Our cash position also provided a
drag to performance. Chicago Bridge &amp; Iron Company, NV (CBI), a specialty engineering and construction firm that provides design, fabrication and construction for global energy, natural resources and infrastructure projects, was the leading
detractor from performance on a relative basis in the period. CBI is a leader in liquefied natural gas (LNG) projects, refinery projects and flat-bottomed tank construction for energy storage. The company also oversees projects in the petrochemical
and global power industries. Additionally, CBI maintains a conservative balance sheet and reports a backlog of roughly $20.0 billion in future projects (post-nuclear divestiture), as of September 30, 2016. We believe that management holds a
long-term view of the business and has developed competency in risk assessment; a diverse book of business should mitigate, in part, the cyclical elements of its business lines. While CBI has traded down in a weak commodity price environment, we
contend that demand for its services can grow. We have added to our position in CBI over the last year; in our view, the company’s attractive valuation and a consistent backlog and pipeline of projects outweigh the short-term news regarding the
timing of projects, as well as the dispute regarding the sale of the nuclear business to Westinghouse Electric Company and the CEO’s recent sale of shares. The
company has been repurchasing shares recently, which supports our view of its attractive valuation. We believe CBI’s LNG and midstream businesses
show continued strength and robust fundamentals. We applaud the company’s nuclear division divestiture and the accompanying regulatory and headline risk. We believe that CBI’s long-term projects and other recent new awards, including some
that “book and burn” quickly (enters the backlog and is then booked as revenue in the space of one quarter) at higher margins, position the company for continued future growth and value creation. We remain positive in our assessment of CBI
and continued to purchase shares during the period.
 
Our stock selection in the consumer staples
sector, led by our position in Sanderson Farms, Inc. (SAFM) combined with an overweight allocation to the producer durables sector, offset some of the underperformance in the period. Thor Industries, Inc. (THO), a leading manufacturer of towable
recreation vehicles (RVs) and motor homes, was the leading contributor to performance on a relative basis in the period. We remain impressed with THO’s financial flexibility, specifically its capital allocation acumen, which we believe is
evident by a history of acquisitions and divestitures and a lean organization that is responsive to changes in consumer preferences and encourages competition between business units. We believe that attractive demographics and elevated consumer
confidence should increase demand for recreational vehicles, and that towable RVs’ lower price point may sustain demand in economic downturns and periods of higher energy prices. In our opinion, THO’s balance sheet should also enable the
business to withstand adverse conditions better than competitors, many of whom lack the same financial flexibility. During July 2016, THO announced the acquisition of Jayco Corp. for $576 million in cash. We believe that this news, progress on its
integration, recent positive earnings’ reports and strong recent product introductions drove the stock higher.
 
We thank you for your continued trust in our ability to help you reach your investment goals.
 
The views expressed represent the opinions of SouthernSun Asset Management, LLC as of September 30, 2016, and are not intended as a forecast or guarantee of
future results, and are subject to change without notice.
 
Table of Contents
 
 
CUMULATIVE TOTAL RETURN PERFORMANCE 
®
 


DEAR FELLOW SHAREHOLDERS,
 
When we wrote you one year ago, we detailed the challenges that historically slow global growth levels, a collapse in commodity prices and the rise of the
U.S. Dollar presented to our Funds. While these headwinds remain, we believe that the Fund’s portfolio companies are led by capable management teams that continue to leverage their financial flexibility to expand niche dominance and
reinvest cash flows prudently.
 
Our companies often find themselves at the intersection of
challenge and opportunity and we believe that they have encountered plenty of each recently. They manage and invest in their businesses against a current backdrop of slow global economic growth and anemic organic revenue growth opportunities that we
contend create challenges for them when modeling, budgeting and considering large capital allocation decisions. Many of them also have clean balance sheets, consistent cash flows and access to capital that, we assert, have allowed them to invest in
internal projects with attractive returns, make bolt-on acquisitions and support productivity initiatives. We are pleased with the investment opportunities on which our companies continued to capitalize during the period and their response to
ongoing challenges.
 
Two current conditions that we believe provide an array of challenges and
opportunities are global trade and the U.S. Dollar’s strength versus the currencies of our trading partners. Trade has been a dominant issue in the U.S. presidential election, as the free trade agreements of the last two decades have positioned
the issue as a point of differentiation between the two candidates. Politics and trade have also become apparent as key issues in the United Kingdom, Russia and China recently. The Fund often includes a number of U.S.-based companies with
substantial global operations. We remain diligent in our assessment of the environment in which our companies operate and continue to analyze the potential impact on them of any changes in global trade behavior.
 
We have seen the impact of a strong U.S. Dollar during the last couple of years on our
companies’ reported revenue, balance sheets and their capital
allocation decisions. While these are all challenges, we believe that the strong Dollar might create opportunities for some of our U.S.-based businesses
to make bolt-on acquisitions that might prove additive to production, distribution, services or product lines. The financial flexibility we seek when qualifying companies for investment traces its roots to balance sheets and discretionary cash flow,
but the global profile of many of our businesses may provide further investment opportunity in the face of slow global growth and volatile markets. We will continue to monitor our companies’ ability to capitalize on the current value of the
U.S. Dollar.
 
FUND PERFORMANCE
 
®
 
Over the trailing twelve-month period ending September 30, 2016, much of our underperformance
was driven by our stock selection within the technology and materials &amp; processing sectors, led by our positions in Knowles Corporation (KN) and WestRock Co. (WRK). Our cash position also provided a drag to performance. Chicago
Bridge &amp; Iron Company, NV (CBI), a specialty engineering and construction firm that provides design, fabrication and construction for global energy, natural resources and infrastructure projects, was the leading detractor from performance
on a relative basis in the period. CBI is a leader in liquefied natural gas (LNG) projects, refinery projects and flat-bottomed tank construction for energy storage. The company also oversees projects in the petrochemical and global power
industries. Additionally, CBI maintains a conservative balance sheet and reports a backlog of roughly $20.0 billion in future projects (post-nuclear divestiture), as of September 30, 2016. We believe that management holds a long-term view of
the business and has developed competency in risk assessment; a diverse book of business should mitigate, in part, the cyclical elements of its business lines. While CBI has traded down in a weak commodity price environment, we contend that demand
for its
services can grow. We have added to our position in CBI over the last year; in our view, the company’s attractive valuation and a consistent
backlog and pipeline of projects outweigh the short-term news regarding the timing of projects, as well as the dispute regarding the sale of the nuclear business to Westinghouse Electric Company and the CEO’s recent sale of shares. The company
has been repurchasing shares recently, which supports our view of its attractive valuation. We believe CBI’s LNG and midstream businesses show continued strength and robust fundamentals. We applaud the company’s nuclear division
divestiture and the accompanying regulatory and headline risk. We believe that CBI’s long-term projects and other recent new awards, including some that “book and burn” quickly (enters the backlog and is then booked as revenue in the
space of one quarter) at higher margins, position the company for continued future growth and value creation. We remain positive in our assessment of CBI and continued to purchase shares during the period.
 
Our overweight allocation to the producer durables sector and our stock selection in the energy
sector, led by Newfield Exploration Company (NFX), offset some of the underperformance in the period. ADT Corporation (ADT), a provider of security services to residential and business customers, was the leading contributor to performance on a
relative basis in the period. In February 2016, private equity manager Apollo Global Management, LLC announced its intent to acquire ADT and to merge it with Protection 1, Apollo’s existing home security portfolio holding. We continued to
analyze the deal while trimming our holding in ADT back to our target weighting. We maintained our position in ADT and tendered our shares when the deal closed in May 2016.
 
We thank you for your continued trust in our ability to help you reach your investment goals.
 
The views expressed represent the opinions of SouthernSun Asset Management, LLC as of September 30, 2016, and are not intended as a forecast of guarantee of
future results, and are subject to change without notice.
 
Table of Contents
 
 
CUMULATIVE TOTAL RETURN PERFORMANCE 
TM
 


DEAR FELLOW SHAREHOLDERS,
 
While the headwinds created by historically slow global growth levels, a collapse in global commodity prices and the rise of the U.S. Dollar continue to challenge
our global businesses, we believe that the Fund’s portfolio companies are led by capable management teams that continue to leverage their financial flexibility to expand niche dominance and reinvest cash flows prudently.
 
Our companies often find themselves at the intersection of challenge and opportunity and we believe
that they have encountered plenty of each recently. They manage and invest in their businesses against a current backdrop of slow global economic growth and anemic organic revenue growth opportunities that we contend create challenges for them when
modeling, budgeting and considering large capital allocation decisions. Many of them also have clean balance sheets, consistent cash flows and access to capital that, we assert, have allowed them to invest in internal projects with attractive
returns, make bolt-on acquisitions and support productivity initiatives. We are pleased with the investment opportunities on which our companies continued to capitalize recently, and their response to ongoing challenges.
 
Two current conditions that we believe provide an array of challenges and opportunities are global
trade and the U.S. Dollar’s strength versus the currencies of our trading partners. Trade has been a dominant issue in the U.S. presidential election, as the free trade agreements of the last two decades have positioned the issue as a point of
differentiation between the two candidates. Politics and trade have also become apparent as key issues in the United Kingdom, Russia and China recently. The Fund often includes a number of companies with substantial global operations. We remain
diligent in our assessment of the environment in which our companies operate and continue to analyze the potential impact on them of any changes in global trade behavior.
 
We have seen the impact of a strong U.S. Dollar during the last couple of years on our companies’ reported revenue, balance sheets and their capital allocation
decisions. While these are all challenges, we believe that the strong Dollar might create opportunities for some of our U.S.-based businesses to make bolt-on acquisitions that might prove additive to production, distribution, services or product
lines. The financial flexibility we seek when qualifying companies for investment traces its roots to balance sheets and discretionary cash flow, but the global profile of many of our businesses may provide further investment opportunity in the face
of slow global growth and
volatile markets. We will continue to monitor our companies’ ability to capitalize on the current value of the U.S. Dollar.
 
FUND PERFORMANCE
 
®
 
Over the approximately twelve-week period from the Fund’s inception date in July through
September 30, 2016, much of our underperformance was driven by our stock selection in the producer durables sector, led by Chicago Bridge &amp; Iron Co. NV (CBI) and Darling Ingredients (DAR), and by our underweight allocation to the
technology sector. Our cash position also provided a drag to performance. Chicago Bridge &amp; Iron Company, NV (CBI), a specialty engineering and construction firm that provides design, fabrication and construction for global energy, natural
resources and infrastructure projects, was the leading detractor from performance on a relative basis in the period. CBI is a leader in liquefied natural gas (LNG) projects, refinery projects and flat-bottomed tank construction for energy storage.
The company also oversees projects in the petrochemical and global power industries. Additionally, CBI maintains a conservative balance sheet and reports a backlog of roughly $20.0 billion in future projects (post-nuclear divestiture) as of
September 30, 2016. We believe that management holds a long-term view of the business and has developed competency in risk assessment; a diverse book of business should mitigate, in part, the cyclical elements of its business lines. While CBI
has traded down in a weak commodity price environment, we contend that demand for its services can grow. We have added to our position in CBI over the last year; in our view, the company’s attractive valuation and a consistent backlog and
pipeline of projects outweigh the short-term news regarding the timing of projects, as well as the dispute regarding the sale of the nuclear business to Westinghouse Electric Company and the CEO’s recent sale of shares. The company has been
repurchasing shares recently, which supports our view of its attractive valuation. We believe CBI’s LNG and midstream businesses show continued strength and robust fundamentals. We applaud the company’s nuclear division divestiture and the
accompanying regulatory and headline risk. We believe that CBI’s long-term projects and other recent new awards, including some that “book and burn” quickly (enters the backlog and is then booked as revenue in the space of one
quarter) at
higher margins, position the company for continued future growth and value creation. We remain positive in our assessment of CBI and continued to
purchase shares during the period.
 
Our stock selection in the consumer staples sector, led by
Bakkafrost P/F (BAKKA), combined with our underweight allocation to the health care sector, offset some of the underperformance in the period. Tenneco, Inc. (TEN), a provider of clean air and smooth ride products to the global automotive industry,
was the leading contributor to performance on a relative basis in the period. Clean air products account for roughly 70% of its revenue, while smooth ride products are roughly 30%. The company operates globally and focuses on four structural growth
drivers, emphasizing the light vehicle platforms, capitalizing on emissions regulations, growth in suspension systems and growth in aftermarket opportunities. We believe that TEN’s focus on outpacing industry growth across its product lines has
been reflected in the margin improvement reported regularly in recent earnings’ releases. It continues to introduce new products and to report increasing content on existing customer platforms. We contend that TEN is poised to benefit from
increasing environmental regulations globally, and new content from recent platform wins. We maintained our target weighting in the stock during the period.
 
We thank you for your continued trust in our ability to help you reach your investment goals.
 
The views expressed represent the opinions of SouthernSun Asset Management, LLC as of September 30, 2016, and are not intended as a forecast of guarantee of
future results, and are subject to change without notice.
 
CUMULATIVE TOTAL RETURN PERFORMANCE
 
AMG SouthernSun Global Opportunities Fund’s cumulative total return is based on the daily change in net asset value (NAV), and assumes that all dividends and
distributions were reinvested. This graph compares a hypothetical $10,000 investment made in the AMG SouthernSun Global Opportunities Fund’s Institutional Class on July 12, 2016 (commencement of operations) to a $10,000 investment made in
the MSCI ACWI Index and MSCI ACWI SMID Cap Index for the same time period. Performance for periods longer than one year are annualized. The graph and table do not reflect the deduction of taxes that a shareholder would pay on a Fund distribution or
redemption of shares. The listed returns for the Fund are net of expenses and the returns for the index exclude expenses. Total returns would have been lower had certain expenses not been reduced.
 
Table of Contents
 
 
CUMULATIVE TOTAL RETURN PERFORMANCE

 


