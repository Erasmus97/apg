Dear Shareholder:


Seven years into our economic recovery, you might
be wondering &quot;where&rsquo;s the proof?&quot; The strong U.S. dollar and sluggish growth have hampered exports and manufacturing.
Low oil prices are raising concerns about the energy sector. A steep sell-off in the first quarter, plus a contentious U.S. election
campaign and ongoing geopolitical issues, have led many to question what lies ahead.

Our analysts see a case for continued, albeit modest,
growth in the U.S. economy. Households have reduced debt and are seeing gains in real income thanks to improving labor markets
and lower energy prices. Businesses remain reasonably well positioned financially, with an added boost to purchasing power from
lower energy prices. Lastly, while the Federal Reserve Board has initiated the process of raising short-term interest rates, we
are confident that &quot;low and slow&quot; will continue to be the watchwords for a while.

The later stages of an economic recovery tend to
bring increased volatility and more challenges to achieving positive investment returns. We believe that active management &mdash;
careful sector allocation and security selection driven by deep research &mdash; can make a difference in this environment.

In the end, it is important
to remember the core reason for investing: long- term goals and a desire for growth tempered by reasonable risk management. We
appreciate your trust and welcome the opportunity to put our resources, experience and expertise to work in helping you meet your
goals.

Best regards,
