Dear Shareholders:

Neiman Funds Management LLC


Dear Shareholders:


Past performance does not guarantee future results. The investment return and principal value of an investment will fluctuate so that an investor’s shares, when redeemed, may be worth more or less than their original cost. Current performance may be lower or higher than the performance data quoted. You may obtain performance data current to the most recent month-end by calling toll free (877) 385-2720. Investors should consider the investment objectives, risks, and charges and expenses of the Fund carefully before investing. The prospectus contains this and other information about the Fund. You may obtain a prospectus on our website www.neimanfunds.com or by calling toll free (877) 385-2720.
5297-NLD-05/25/2016
2016 Annual Report 1
Dear Shareholder,

Portfolio Manager to the Fund


