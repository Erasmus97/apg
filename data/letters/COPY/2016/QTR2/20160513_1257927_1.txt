Dear Fellow Shareholder,

The first quarter of began with a sharp drop in the stock market. The S&amp;P fell about in the first three weeks. Smaller company stocks fell further, and the Russell was down about by late January. Panic was in the air. Then, after a few weeks of wobbling sideways, the market drifted back up, closing the quarter roughly unchanged.

The recovery during March, in part, was attributed to Janet Yellen's remarks to the effect that the US and global economies were too weak to allow for the Fed to raise interest rates as rapidly as they had suggested a few months ago. We do not consider this cause for celebration. This pattern of bad news triggering stock market declines, followed by rallies based on the hint of renewed monetary stimulus, has occurred repeatedly over the past two years. We have described it as a "standoff" between positive and negative forces in the world economy.

Our take is that stock and bond prices got ahead of themselves as the Fed injected trillions of dollars into securities markets in order to create a "wealth effect." Artificially depressed interest rates have made fixed income returns unattractive for some time. Speculators have been able to earn trading profits by correctly anticipating the Fed's next short-term move, but we believe this is a version of "picking up quarters in front of an oncoming steamroller." Although not a target-rich environment due to low rates, Fixed Income Portfolio Managers Tom and Nolan were able to find pockets of opportunity during the first quarter, particularly in corporate credits as spreads widened. However, their overall focus remains on protecting shareholder capital by keeping maturities short and credit quality high.

As an aside, the plight of bond investors is worse in Europe and Japan where interest rates on many bonds are negative. (Imagine paying a bank to hold your money or buying a bond that pays no interest and sells at a premium to face valuei.e., guaranteed to lose money if held to maturity.) Economists and central bankers discuss negative interest rates as if they are just an extreme version of "low" rates. This seems somewhat absurd to us, and we suspect that future financial historians will conclude that this version of monetary policy was ill-advised.

Turning to the stock market, interest rates have been kept low, but the influx of new cash from the Fed stopped a couple of years ago. Meanwhile, economic fundamentals have been decidedly mixed. In this environment, our companies' business values have been growing, but their prices are still not in the "bargain" category. So, stock investors are faced with "fair" stock prices and an uninspiring outlook for business and corporate profits. This does not seem to be a recipe for a strong stock market. Fortunately, our companies have managements that are not content to drift passively with the economic current. There are things they can do to grow business value per share despite the mediocre environment.

Bull Market Not Required

We have no idea when the bull market will resume, but fortunately for investors, long-term growth in net worth does not depend on a perpetually rising stock market. Some stocks can go up for company-specific reasons while others are languishing, and companies can create the foundation for future stock market success by growing their business values even while investors are distracted by bad news. Each of our companies is busy trying to improve their position in the world. Below are a few examples.

Redwood Trust per share) is a "value investor" in mortgages and mortgage securities. It has a culture of prudence, great skills in credit risk assessment and a focus on growth in per share value. The company has struggled to earn good returns in this period of suppressed interest rates yet has continued to pay the dividend that now provides approximately a yield. It has recently made strategic moves that we think position it for more predictable growth going forward. The stock sells at a discount to book value, and we believe it will sell at or above book value again. We think the ingredients are in place for double-digit annual returns over the next few years regardless of what happens to the S&amp;P

Wells Fargo per share) is one of the country's largest banks, but it has avoided most of the headaches (and risks) associated with the global "money center" banks. It has a strong, conservative lending culture and it added an enormous quantity of cheap deposits during the financial crisis of by acquiring troubled banks. (It also has Warren Buffett, as a owner, looking over its shoulder.) Wells has suffered compressed net interest margins (as has Redwood) in this period of artificially low rates, yet its earnings have grown to over per share in fiscal year and again in When interest rates rise, Wells Fargo's margins should widen, earnings should increase and the price investors are willing to pay for those earnings (P/E) may increase.

TransDigm per share) makes (primarily replacement) parts for the aerospace industry. The company is typically one of two or three suppliers (sometimes the sole supplier) of important but relatively low-cost parts for commercial and military aircraft. As such, they enjoy fairly predictable sales volume and strong pricing power. Growth comes through acquisition and from a rising number of aircraft in service. The company is an active, but very disciplined, acquirer. They buy parts producers, squeeze costs out of the manufacturing process and gradually raise prices.

ANNUAL REPORT










WEITZINVESTMENTS.COM

Management is very disciplined in adhering to its investment criteria, and when acquisition candidates are not available, they return cash to shareholders through share buybacks and large special dividends. Although the stock price is close to our appraised business value, we think the likelihood of further consistent growth, regardless of economic conditions, will reward us for continuing to own the stock over the years.

MasterCard per share) is basically a royalty on global consumer spending. Regardless of near-term economic growth rates, MasterCard is likely to be able to produce annual earnings per share growth over the next several years. Its stock price is also near our appraised value, but if earnings grow at per year, they will double in years. Even if the P/E paid by investors shrinks a bit, our total return should be very attractive.

We could rest our case here, but long-time shareholders are surely asking, "What about Berkshire Hathaway and the Liberty family of companies?" Well, as a matter of fact, we think their businesses and prospects are at least as good as the companies listed above. Warren Buffett just made a billion acquisition (Precision Castparts) with cash on hand and cheap debt financing. Liberty Media will soon reorganize into three new tracking stocks, potentially aiding recognition of the value of some less well-known assets. The Charter acquisitions of Time Warner Cable and Bright House, which appear to be nearing approval, should have a very positive impact on Liberty Broadband and Liberty Ventures. Mike Fries at Liberty Global never stands still. In fact, Warren Buffett at Berkshire and John Malone and Greg Maffei at the Liberty companies are always working with the excellent raw material they have assembled to build value for us.

We are highly confident that the value of our businesses will continue to grow over time. The stock market may ignore this progress from time to time as it obsesses over Fed policy, China, terrorism, the presidential election, etc., but increased value will inevitably be reflected in higher stock prices.

The stock market may continue to go sideways, or it may suffer a major correction. Or not. We do not know, but over several decades we have learned that while we cannot knowwe do not need to know. All we (and our clients!) need is a collection of good businesses, patience and the courage of our convictions. We thank you for your ongoing support.

Sincerely,
