Dear Shareholders: 
Markets remained volatile at the beginning of the year but began to show signs of stabilization in February. Oil prices rebounded modestly from the lows set at


mid-month, and U.S. economic data improved following a downtick in growth in late 2015. The international backdrop was less rosy. Japan adopted a negative interest rate policy in an attempt to
revive growth and inflation while the European Central Bank indicated it may also ease monetary policy further. Interest rates in the United States are expected to rise at a gradual pace. 
China’s shift to a consumer-led economy continues to weigh on manufacturing and exports. A strong U.S. dollar and weak global demand persist as headwinds for U.S. exports. In Europe, attention is shifting to a
crucial referendum scheduled for June 23, 

in which British voters will decide whether the United Kingdom should remain in the European Union. 
As markets have become more focused on short-term trends in recent years, we believe it’s important for investors to lengthen their investment time horizon. At MFS®, we don’t trade on headlines or
trends; we invest for the long term. 
We believe that this approach, coupled with the professional guidance of a financial advisor, will help you reach
your investment goals. 
Respectfully, 


Dear Shareholders: 
Markets remained volatile at the beginning of the year but began to show signs of stabilization in February. Oil prices rebounded modestly from the lows set at


mid-month, and U.S. economic data improved following a downtick in growth in late 2015. The international backdrop was less rosy. Japan adopted a negative interest rate policy in an attempt to
revive growth and inflation while the European Central Bank indicated it may also ease monetary policy further. Interest rates in the United States are expected to rise at a gradual pace. 
China’s shift to a consumer-led economy continues to weigh on manufacturing and exports. A strong U.S. dollar and weak global demand persist as headwinds for U.S. exports. In Europe, attention is shifting to a
crucial referendum scheduled for June 23, 

in which British voters will decide whether the United Kingdom should remain in the European Union. 
As markets have become more focused on short-term trends in recent years, we believe it’s important for investors to lengthen their investment time horizon. At MFS®, we don’t trade on headlines or
trends; we invest for the long term. 
We believe that this approach, coupled with the professional guidance of a financial advisor, will help you reach
your investment goals. 
Respectfully, 


Dear Shareholders: 
Markets remained volatile at the beginning of the year but began to show signs of stabilization in February. Oil prices rebounded modestly from the lows set at


mid-month, and U.S. economic data improved following a downtick in growth in late 2015. The international backdrop was less rosy. Japan adopted a negative interest rate policy in an attempt to
revive growth and inflation while the European Central Bank indicated it may also ease monetary policy further. Interest rates in the United States are expected to rise at a gradual pace. 
China’s shift to a consumer-led economy continues to weigh on manufacturing and exports. A strong U.S. dollar and weak global demand persist as headwinds for U.S. exports. In Europe, attention is shifting to a
crucial referendum scheduled for June 23, 

in which British voters will decide whether the United Kingdom should remain in the European Union. 
As markets have become more focused on short-term trends in recent years, we believe it’s important for investors to lengthen their investment time horizon. At MFS®, we don’t trade on headlines or
trends; we invest for the long term. 
We believe that this approach, coupled with the professional guidance of a financial advisor, will help you reach
your investment goals. 
Respectfully, 


