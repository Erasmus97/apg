Dear Shareholders, 
As president and chief executive officer of the Rainier Funds (the
“Funds”), and on behalf of Rainier Investment Management, LLC, I would like to take the opportunity to express our appreciation for your valued investment in the Funds and your confidence in our investment approach and philosophy.

This Annual Report contains financial statements audited by Deloitte &amp; Touche LP for the fiscal year ending March 31, 2016. This
Report is designed to provide you with a detailed accounting for each of the Rainier Funds. Inside you will find investment commentaries followed by financial and operational information for each of the Rainier Funds for the 12 months ending
March 31, 2016. 
Throughout the year foreign and domestic equity markets varied considerably. As we entered 2016, investors had low
expectations in valuation, multiple expansion or earnings growth, which are the key components to a strong market. Sluggish growth in China and increasing international tension in the Middle East signaled a continuation of the volatility and
pressures felt in the markets in 2015. However, the U.S. consumer is in the healthiest state in years, with interest rates still at historic lows and likely to only rise gradually. 
Our approach to investing remains constant and consistent. We have employed a conservative growth approach for over 25 years and continue to do so today. We seek opportunities in companies with relatively
predictable profit and revenue growth at valuations that are reasonable. In the current environment, we have emphasized companies that are heavily skewed to the U.S. in revenue generation, have little commodity or product pricing risk, and maintain
reasonable and appropriate debt levels and fixed cost bases. Once again, we thank you for your trust and confidence in the Rainier Funds, and we look forward to continuing to pursue solid investment opportunities on your behalf in coming years.

Sincerely, 


