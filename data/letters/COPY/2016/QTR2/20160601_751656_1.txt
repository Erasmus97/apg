Dear Shareholders: 
Markets remained volatile in the first quarter of 2016, but after a tough start to the year stocks largely recouped their losses, with most major indices closing
out the 
service-sector
China’s economy — while suffering growing pains linked to its shift from an export-led to a consumer-driven economic model — appeared to stabilize as
the first quarter drew to a close. Europe awaits a crucial referendum scheduled for June 23 in which British voters will decide whether the United Kingdom should remain in the European Union. 
As markets have become more focused on short-term trends in recent years, we believe it’s important for investors to lengthen their
investment time horizon. At MFS®, we don’t trade on headlines or trends; we invest for the long term. 
We believe that this approach, coupled
with the professional guidance of a financial advisor, will help you reach your investment goals. 
Respectfully, 


Dear Shareholders: 
Markets
remained volatile in the first quarter of 2016, but after a tough start to the year stocks largely recouped their losses, with most major indices closing out the quarter roughly where they were at the beginning of the year. After falling to
multiyear lows in February, oil prices stabilized as oil-producing nations began taking steps to cap crude production. U.S. economic growth remained positive but unspectacular amid continued improvement in labor markets and upbeat service-sector
activity. Headwinds from abroad forced the U.S. Federal Reserve to table its plans to tighten monetary policy, while the Bank of Japan and the European Central Bank continued to ease policy aggressively, perpetuating a lower-for-longer global
interest rate environment. 
China’s economy — while suffering growing pains linked to its shift from an export-led to a consumer-driven economic model
— appeared to stabilize as the first quarter drew to a close. Europe awaits a crucial referendum scheduled for June 23 in which British voters will decide whether the United Kingdom should remain in the European Union. 
As markets have become more focused on short-term trends in recent years, we believe it’s important for investors to lengthen their investment time horizon. At
MFS®, we don’t trade on headlines or trends; we invest for the long term. 
We believe that this approach, coupled with the professional guidance of a
financial advisor, will help you reach your investment goals. 
Respectfully, 


Dear Shareholders: 
Markets
remained volatile in the first quarter of 2016, but after a tough start to the year stocks largely recouped their losses, with most major indices closing out the quarter roughly where they were at the beginning of the year. After falling to
multiyear lows in February, oil prices stabilized as oil-producing nations began taking steps to cap crude production. U.S. economic growth remained positive but unspectacular amid continued improvement in labor markets and upbeat service-sector
activity. Headwinds from abroad forced the U.S. Federal Reserve to table its plans to tighten monetary policy, while the Bank of Japan and the European Central Bank continued to ease policy aggressively, perpetuating a lower-for-longer global
interest rate environment. 
China’s economy — while suffering growing pains linked to its shift from an export-led to a consumer-driven economic model
— appeared to stabilize as the first quarter drew to a close. Europe awaits a crucial referendum scheduled for June 23 in which British voters will decide whether the United Kingdom should remain in the European Union. 
As markets have become more focused on short-term trends in recent years, we believe it’s important for investors to lengthen their investment time horizon. At
MFS®, we don’t trade on headlines or trends; we invest for the long term. 
We believe that this approach, coupled with the professional guidance of a
financial advisor, will help you reach your investment goals. 
Respectfully, 


