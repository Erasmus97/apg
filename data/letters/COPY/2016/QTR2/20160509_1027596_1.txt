Dear Fellow Shareholders,



The world economy continues its slow mend since the financial crisis, with the United States the standout among developed country economies and with subpar growth elsewhere. Markets continue to be roiled by collapsing commodity prices, notably oil and gas, as China transitions from an industrial/export-led model to a consumer economy. Interest rates remain at rock bottom levels, even turning negative for government bonds from Europe and Japan. And the U.S. stock market showed a very narrow breadth for most of led by a handful of very large internet stocks which pulled up market averages significantly.



Against this challenging backdrop, the American Trust Allegiance Fund (the Fund) has remained focused on quality companies bought at reasonable prices that we believe, in turn, should lead to solid long-term performance. The Portfolio Managers Q&amp;A section of this Annual Report discusses the Funds long-term approach in some detail.



For the fiscal year ended February the Fund delivered a total return of This compares to the return recorded by the S&amp;P Index (S&amp;P This underperformance relative to the S&amp;P is attributable to the following:










The Funds high exposure to financials, at close to for most of the year, was a casualty of on-going low interest rates. In particular, insurance company shares were hurt. We exited our insurance exposure toward the end of the year, with the exception of Berkshire Hathaway, Inc., which is broadly diversified beyond insurance.















While the broader U.S. stock market declined, international stocks declined much more. The MSCI EAFE and EEM indices, which respectively track international stocks from developed and emerging countries, posted declines of and for the months ended February The Fund invests both directly and indirectly in companies which have a large presence in international markets, including emerging market countries, which hurt performance relative to the S&amp;P even as it reduced risk through broader diversification.















Our GARP (Growth At a Reasonable Price) investment discipline, which we strongly believe can deliver good relative performance through the course of a market cycle, did not fare as well during the recent period in which momentum stocks, such as the highly volatile internet and biotechnology sectors, continued to outperform the broader market.






While we were not happy to underperform the S&amp;P last year, we are confident in the companies in which we are invested in the Fund and in the value we believe is inherent in their shares. In this years Annual Report, we
















American Trust Allegiance Fund




invite you to share our confidence in these companies and their prospects through an under the hood look at the portfolio and the investment process.



Our invitation has three parts: a) a statistical snapshot of selected value and growth measures for the portfolio, the significance of which we will explain; b) an extended Question &amp; Answer dialogue with the co-portfolio managers of the Fund which will give you a perspective on both the big picture investment opportunities in which we are investing as well as the security analysis which underpins our stock selections; and lastly, c) we stand ready to answer your questions on any and all stocks in the portfolio via email or through a telephone conversation.



The last part of our invitation is unusual, but is a hallmark of our firm and its philosophy. Very few fund managers invite shareholders to call them directly, but we do! As you read through the following sections of this report the Fund Snapshot and the Portfolio Manager Q&amp;A, please do call us if you have questions. We always stand ready to talk about the Fund or its investments, or if you would like to discuss ways in which our investment advisory firm might be able to assist you more broadly. While we know many of you personally, there are some we know less well and we would love to rectify that situation.



We appreciate your support of, and investment in, the American Trust Allegiance Fund. Thank you and we look forward to sharing with you the good things that we expect for and beyond.



Sincerely yours,
