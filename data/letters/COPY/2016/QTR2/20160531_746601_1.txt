Dear fellow shareholders: 
 
The capital markets were dominated by two overarching themes in the prior
fiscal year, expectations for the Federal Reserve (Fed) to end its zero interest rate policy and collapsing energy prices. These factors together created a challenging year for many investors. While annual figures may look benign, the path to get
there was anything but. Market volatility increased due to uncertainty regarding short-term interest rate increases and shifting investor risk sentiment. For example, the 2-year treasury began the period at 0.55%, reached a high of 1.09% near
year-end and ended the period at 0.72%. 
Over the last few years, the Fed has been carefully removing
emergency stimulus measures that were implemented during the financial crisis. First, it “tapered” and subsequently stopped the asset purchase program commonly referred to as Quantitative Easing or QE in December 2014. Throughout 2015,
market participants speculated as to when the Fed would finally raise interest rates. In December, 2015, the Fed raised interest rates for the first time in almost 10 years. The importance of the initial rate hike was not necessarily the magnitude,
as it was only 0.25%, but more of a symbolic move by ending the zero interest rate policy. With the initial rate rise now behind us, the focus turns to the path of future rate hikes. 
While the dual mandate of full employment and stable inflation continue to be the core focus of Fed policy, attention to
global financial conditions is becoming more integral to the discussion. Domestically, the United States seems to be faring better than its global counterparts. Labor markets continue to post steady gains with the unemployment rate hovering around
5%. Many inflation metrics are near the Fed’s own 2% target figure and gross domestic product (GDP) has been generally stable at 2%-2.5%. In contrast, there remain financial struggles globally. The Euro-Area continues to muddle thru aided by
their own large-scale asset purchase programs quite similar to the Fed’s own QE policy. China is experiencing slowing growth and Japan has negative interest rates. It is within this global context that the Fed finds itself in a quandary;
domestic stability tempered with global uncertainty. For all of those reasons, the Fed has decided to take a slow and cautious path in the progress of interest rate increases. Given the March decision to forgo an interest rate increase and the
discussion surrounding these affecting factors, market participants and the Fed itself have reduced projections for interest rate increases in 2016 to just two from the 4 expected after the December meeting. Janet Yellen, the Fed Chairwoman,
continues to have a generally dovish slant in her communications and emphasizes the data dependency of the future path of interest rates. 
Oil prices fell from $60/barrel in the summer of 2015 to the mid $20’s/barrel in February 2016. There has been a recovery from the recent low, but prices still remain well below prior peaks. The
initial declines were generally less drastic as far as market impacts are concerned. There was a sense that the rising dollar was partially responsible for the decline and falling energy prices could potentially be stimulative for consumer spending.
In the latter half of 2015 and early 2016, as oil prices fell to levels not seen since 2003, ripples


were felt throughout the capital markets and concerns arose regarding negative economic impacts due to potential bankruptcies, lower capital expenditures and possible layoffs in the energy
sector. Risk sentiment had become increasingly correlated to the movement in oil prices. As oil and other commodity prices declined, investors’ appetite for risk seems to have faded and vice versa. These sentiment shifts have caused high
quality bonds to outperform in the former environments and lower quality bonds to outperform in the latter. 
Strategy 
We expect short-term rates to gradually increase throughout the year ahead as economic data proves sustainable enough
for the Fed to continue its gradual path toward rate normalization. As such, we have maintained our Funds’ defensive positions against a rising interest rate environment. We expect markets to remain susceptible to active cross currents of
improving domestic growth, energy market fluctuations and global financial challenges. Uncertainty regarding economic growth, presidential elections and unexpected market influencing events can provide opportunistic tactical trading opportunities
that we take advantage of as appropriate for our various strategies while maintaining our overall defensive strategic positioning. 
We continue to position the U.S. Government Fund defensively against rising interest rates, with a focus on seasoned, high coupon agency mortgages. The income and principal stability exhibited by these
securities continues to be a fundamental focus of the fund. The Sit Quality Income Fund also maintains a defensive position for increases in short-term rates. The combination of high quality assets and short duration make it an attractive vehicle
for cash management purposes. 
The tax-exempt fixed income strategy employed in managing both the Tax-Free
Income Fund and the Minnesota Tax-Free Income Fund will continue to focus heavily on the use of high coupon bonds and bonds structured with put, call, sinking fund and prepayment provisions that provide regular cash flow. We believe that our
investment strategy’s focus on income, which we believe is the primary source of return over longer periods of time, will continue to deliver positive performance. We continue to focus on sectors and security structures that provide incremental
yield, while using diversification to help manage credit risk. 
We appreciate your continued interest in the
Sit family of funds. 
With best wishes, 
 


