Dear Shareholders: 
Market volatility remains elevated amid signs of slowing global growth and continued weakness in the price of oil and other commodities. Chinese markets remain
skittish 

following a series of policy missteps and currency depreciations, which have contributed to large capital outflows. 
A lower-for-longer interest rate environment is likely for the foreseeable future given accommodative global central bank policies. The Bank of Japan has adopted a negative interest rate policy while the European
Central Bank has signaled plans for additional monetary stimulus later this year. Meanwhile, expectations for further monetary policy tightening from the U.S. Federal Reserve have been reduced in the face of more challenging financial
conditions. 
The U.S. manufacturing sector is suffering the effects of a strong U.S. dollar and falling global demand. There is some reason for optimism,
however, as U.S. labor markets continue to improve while auto and home sales remain upbeat. 
As markets have become more focused on
short-term trends in recent years, we believe it’s important for investors to lengthen their investment time horizon. At MFS®, we don’t trade on headlines or trends; we invest for the long term. 
We believe that this approach, coupled with the professional guidance of a financial advisor, will help you reach your investment goals. 
Respectfully, 


Dear Shareholders: 
Market volatility remains elevated amid signs of slowing global growth and continued weakness in the price of oil and other commodities. Chinese markets remain
skittish 

following a series of policy missteps and currency depreciations, which have contributed to large capital outflows. 
A lower-for-longer interest rate environment is likely for the foreseeable future given accommodative global central bank policies. The Bank of Japan has adopted a negative interest rate policy while the European
Central Bank has signaled plans for additional monetary stimulus later this year. Meanwhile, expectations for further monetary policy tightening from the U.S. Federal Reserve have been reduced in the face of more challenging financial
conditions. 
The U.S. manufacturing sector is suffering the effects of a strong U.S. dollar and falling global demand. There is some reason for optimism,
however, as U.S. labor markets continue to improve while auto and home sales remain upbeat. 
As markets have become more focused on
short-term trends in recent years, we believe it’s important for investors to lengthen their investment time horizon. At MFS®, we don’t trade on headlines or trends; we invest for the long term. 
We believe that this approach, coupled with the professional guidance of a financial advisor, will help you reach your investment goals. 
Respectfully, 


Dear Shareholders: 
Market volatility remains elevated amid signs of slowing global growth and continued weakness in the price of oil and other commodities. Chinese markets remain
skittish 

following a series of policy missteps and currency depreciations, which have contributed to large capital outflows. 
A lower-for-longer interest rate environment is likely for the foreseeable future given accommodative global central bank policies. The Bank of Japan has adopted a negative interest rate policy while the European
Central Bank has signaled plans for additional monetary stimulus later this year. Meanwhile, expectations for further monetary policy tightening from the U.S. Federal Reserve have been reduced in the face of more challenging financial
conditions. 
The U.S. manufacturing sector is suffering the effects of a strong U.S. dollar and falling global demand. There is some reason for optimism,
however, as U.S. labor markets continue to improve while auto and home sales remain upbeat. 
As markets have become more focused on
short-term trends in recent years, we believe it’s important for investors to lengthen their investment time horizon. At MFS®, we don’t trade on headlines or trends; we invest for the long term. 
We believe that this approach, coupled with the professional guidance of a financial advisor, will help you reach your investment goals. 
Respectfully, 


Dear Shareholders: 
Market volatility remains elevated amid signs of slowing global growth and continued weakness in the price of oil and other commodities. Chinese markets remain
skittish 

following a series of policy missteps and currency depreciations, which have contributed to large capital outflows. 
A lower-for-longer interest rate environment is likely for the foreseeable future given accommodative global central bank policies. The Bank of Japan has adopted a negative interest rate policy while the European
Central Bank has signaled plans for additional monetary stimulus later this year. Meanwhile, expectations for further monetary policy tightening from the U.S. Federal Reserve have been reduced in the face of more challenging financial
conditions. 
The U.S. manufacturing sector is suffering the effects of a strong U.S. dollar and falling global demand. There is some reason for optimism,
however, as U.S. labor markets continue to improve while auto and home sales remain upbeat. 
As markets have become more focused on
short-term trends in recent years, we believe it’s important for investors to lengthen their investment time horizon. At MFS®, we don’t trade on headlines or trends; we invest for the long term. 
We believe that this approach, coupled with the professional guidance of a financial advisor, will help you reach your investment goals. 
Respectfully, 


Dear Shareholders: 
Market volatility remains elevated amid signs of slowing global growth and continued weakness in the price of oil and other commodities. Chinese markets remain
skittish 

following a series of policy missteps and currency depreciations, which have contributed to large capital outflows. 
A lower-for-longer interest rate environment is likely for the foreseeable future given accommodative global central bank policies. The Bank of Japan has adopted a negative interest rate policy while the European
Central Bank has signaled plans for additional monetary stimulus later this year. Meanwhile, expectations for further monetary policy tightening from the U.S. Federal Reserve have been reduced in the face of more challenging financial
conditions. 
The U.S. manufacturing sector is suffering the effects of a strong U.S. dollar and falling global demand. There is some reason for optimism,
however, as U.S. labor markets continue to improve while auto and home sales remain upbeat. 
As markets have become more focused on
short-term trends in recent years, we believe it’s important for investors to lengthen their investment time horizon. At MFS®, we don’t trade on headlines or trends; we invest for the long term. 
We believe that this approach, coupled with the professional guidance of a financial advisor, will help you reach your investment goals. 
Respectfully, 


