Dear shareholder,
The past 12 months marked a challenging stretch for all investors,
and the fixed-income markets offered no exception. Three themes
dominated headlines for the majority of the period and continue to be
sources of concern for investors: global growth, commodity prices,
and the direction of interest rates. On the economic front, worries
about a slowdown in China pushed the United States to center stage as
investors increasingly looked to domestic demand to fuel global
growth. Energy prices were highly volatile, plummeting in January to
multi-year lows before rebounding to finish at more sustainable
levels. As for interest rates, the U.S. Federal Reserve (Fed) finally
began in December the long process of normalizing monetary policy by
increasing the federal funds rate for the first time in nine years.
More rate hikes may be on the horizon, but the United States'
position as the one major economy tightening in this environment
makes the Fed's job that much more difficult.
Against this backdrop, investors tended to dial down their risk
exposure, selling out of equities, high-yield bonds, and emerging
markets in favor of the safety of U.S. Treasuries and other
less-volatile assets. That trend abruptly reversed course in
February, as attractive valuations eventually lured investors back
into riskier assets. After period end, the United Kingdom's vote to
leave the European Union set off more volatility as investors turned
back to safe-haven assets.
These kinds of market swings can be unsettling. We believe the global
economy will continue to make gains, but that periods of heightened
volatility could likely persist. At John Hancock Investments,
portfolio risk management is a critical part of our role as an asset
manager and our dedicated risk team is focused on these issues every
day. We continually strive for new ways to analyze potential risks
and have liquidity tools in place to meet the needs of our fund
shareholders. However, your best resource is your financial advisor,
who can help ensure your portfolio is sufficiently diversified to
meet your long-term objectives and to withstand the inevitable bumps
along the way.
On behalf of everyone at John Hancock Investments, I'd like to take
this opportunity to thank you for the continued trust you've placed
in us.
Sincerely,


