Dear Shareholder:



On behalf of the Board of Trustees of The Community Capital Trust, I am pleased to present the CCM Alternative Income Fund Annual Report to Shareholders for the year ended May



The Fund celebrated its three-year anniversary and, during the one-year period ending May achieved its goals of low volatility, income of to above T-Bills, monthly distributions and low correlation to the both the equity (S&amp;P and bond (Barclays Aggregate) markets.



We applaud the disciplined and productive efforts of Community Capital Management, Inc., registered investment advisor to the Fund, and Badge Investment Partners LLC, sub-advisor to the Fund, and we thank you, our shareholders.



Sincerely,

Dear Shareholder:



On behalf of the Board of Trustees of The Community Capital Trust, I am pleased to present the CRA Qualified Investment Fund Annual Report to Shareholders for the year ended May



Once again, the CRA Qualified Investment Fund demonstrated consistent financial performance and continued success identifying and purchasing securities that finance economic and community development activities throughout the nation. In fact, during the past fiscal year, the Fund surpassed billion invested in targeted, community development securities.



The impact of the Funds investments can be seen today in all states - in cities, towns, and neighborhoods - that have benefited from funding made possible by the securities purchased by the Fund.



We applaud the disciplined and productive efforts of Community Capital Management, Inc., registered investment advisor to the Fund, and we thank you, our shareholders, for your investments. We appreciate your continued confidence.



Sincerely,
