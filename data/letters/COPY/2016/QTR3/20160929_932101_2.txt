Dear Fellow Shareholder:

Through the first half of markets around the world have shown great resilience in the face of multiple challenges. Now, as we enter the fall, many additional factors raise new concerns.

Against a backdrop of sluggish growth and following a colorful political campaign, the United States will be electing a new president in a few short weeks. Overseas, challenges are widespread, from sluggish growth in Europe, Japan, and many emerging markets to global fallout from the United Kingdoms decision to leave the European Union. As non-U.S. central banks consider new actions to boost economic growth, here at home the Federal Reserve seeks stronger economic data before it raises interest rates. The uncertainty caused by these unfolding events could well spur renewed bouts of market volatility.

But we believe that opportunities can emerge despite the markets ups and downs. At Putnam, our portfolio managers actively pursue these opportunities. Backed by a network of global analysts, they draw on their long experience and expertise in navigating changing conditions.

We share Putnams deep conviction that an active approach based on fundamental research can play a valuable role in your portfolio. In the following pages, you will find an overview of your funds performance for the reporting period ended July as well as an outlook for the coming months.

Now may be a good time for you to consult with your financial advisor, who can help you in determining if your portfolio remains aligned with your long-term goals, time horizon, and tolerance for risk.

As always, thank you for investing with Putnam.























Interview with your funds portfolio manager


What was the market environment like during the funds reporting period ended

Market volatility picked up in the summer of due to a variety of global macroeconomic factors. The fourth calendar quarter of saw stocks bounce back from their third-quarter lows, but a variety of headwinds from Chinas economic weakness to depressed commodity markets made for a relatively difficult backdrop for investors. January opened with extreme volatility, primarily as concerns over China led investors to wonder whether the risks of a global recession were increasing. In addition, negative interest rates in Europe and Japan became an obstacle to global financial markets. Some of these pressures eased beginning in mid-February, particularly as oil prices began to rise and the U.S. dollar weakened relative to other global currencies.

The remainder of the period was somewhat calmer by comparison until, that is, U.K. voters elected in late June to leave the European Union. This surprising event caused some turmoil for U.S. markets, but we do not see it posing a substantial economic or market risk to the United States. Indeed, with the appointment of a new British Prime


Current performance may be lower or higher than the quoted past performance, which cannot guarantee future results. Share price, principal value, and return will fluctuate, and you may have a gain or a loss when you sell yourshares. Performance of class A shares assumes reinvestment of distributions and does not account for taxes.Fund returns in the bar chart do not reflect a sales charge of had they, returns would have been lower.See pages and for additional performance information. For a portion of the periods, the fund hadexpense limitations, without which returns would have been lower. To obtain the most recent month-end performance, visit putnam.com.













Research Fund











Minister in July, we became optimistic that the period of fear and uncertainty stemming from Brexit would prove to be short-lived.

How did the fund perform in this environment?

Though it turned in slightly positive results for the period, the fund lagged its benchmark, the S&amp;P Index, and the average return of its Lipper peer group, Large-Cap Core Funds. We avoided a number of mega-cap companies some of the largest, more stable, and defensive companies available to investors and this hurt relative results. In short, we believed these companies were not attractive on their fundamental merits but were more places for investors to hide during the period.

Similarly, our preference for smaller, growth-oriented biotech stocks over the stocks of larger, more stable pharmaceutical companies proved to be a drag on relative results. In addition, our emphasis on financial stocks that we believed would stand to benefit from rising interest rates detracted from relative returns, as rates actually declined during the period as a result of macroeconomic fears.

What were some of the stocks or strategies that detracted most from relative returns?

Bombardier, an out-of-benchmark holding, was the largest detractor. This company is a leading manufacturer of aircraft and trains. In early Bombardier announced the need to raise billion to cover the cost of a new fuel efficient passenger jet airliner, the CSeries, which had been delayed for two years and was over budget. The company also announced the replacement of its chief executive officer and suspended its dividend. Initially, we sensed there could be positive change following these financing and leadership announcements, but we lost confidence during the period that such changes would occur. Consequently, we sold the stock by the end of the period.

Perrigo, an Ireland-based specialty pharmaceutical company, was the second-largest detractor from the funds relative results. The stocks decline was primarily due to disappointment over the announcement in late that Perrigo would not be acquired by


This comparison shows your funds performance in the context of broad market indexes for the ended See pages and for additional fund performance information. Index descriptions can be found on page













Research











industry rival Mylan a potential deal that had been cheered by investors earlier in the year. We sold this stock from the portfolio by period-end.

Other detractors, as I mentioned, included our decisions to avoid or de-emphasize a number of mega-cap stocks that performed well during the period. We did not own the stock of General Electric [GE], for example, and we held a benchmark-relative underweight to consumer products and health-care giant Johnson &amp; Johnson. In general, risk-averse investors sought safer-seeming stock bets at different points in the funds fiscal period, and the share price of mega-cap companies like GE and Johnson &amp; Johnson generally benefited from that shift in investor demand.

What were some of the standout contributors to relative results?

Northrop Grumman, which is one of the largest defense conglomerates in the world, was the top contributor for the annual period. The company, which specializes in unmanned aerial vehicle manufacturing and defense systems, continued to win franchise programs and saw higher-than-expected earnings, which led to share price appreciation.

Communications Holdings also was a top contributor to relative results. This aerospace contractor reported solid earnings and expanded its business by penetrating new markets. In addition, the company strengthened its communication systems segment with an acquisition and returned cash to shareholders through share buybacks and dividends, all of which acted as tailwinds for the stock.


Allocations are shown as a percentage of the funds net assets as of Cash and net other assets, if any, represent the market value weights of cash, derivatives, short-term securities, and other unclassified assets in the portfolio. Summary information may differ from the portfolio schedule included in the financial statements due to the inclusion of derivative securities, any interest accruals, the exclusion of as-of trades, if any, the use of different classifications of securities for presentation purposes, and rounding. Holdings and allocations may vary over time.













Fund












A third solid contributor was the stock of Royal Dutch Shell, the Anglo-Dutch multinational oil and gas company. This out-of-benchmark stock rose, particularly after early February, when oil prices began to recover from multi-decade lows.

What was the impact of derivatives on the funds relative performance?

During the period, forward currency contracts stood out for their positive impact on the funds relative performance. These contracts, which are agreements between two parties to buy and sell currencies at a set price on a future date, can be used to hedge against foreign currency weakness, which was pronounced during the period.

This positive performance was partially offset by negative results in our positions in futures and total return swaps. We used these derivatives to equitize the portfolios cash holdings and to gain exposure to a basket of securities, respectively.

What is your outlook for the U.S. economy and stock market?

We believe the U.S. economy is in the later stages of its expansion, and that market volatility may continue to rise as this cycle matures. For the balance of we think the most likely scenario for U.S. company earnings is modestly accelerating, low- to mid-single-digit earnings growth. Although strength appears to have returned to the U.S. dollar post-Brexit, we do not think this will be too much of a headwind for U.S. exporters, for


This table shows the funds top holdings by percentage of the funds net assets as of Short-term investments and derivatives, if any, are excluded. Holdings may vary over time.













Research Fund











example, and we expect relative stability in the price of oil to continue providing a boost to the energy sector.

The global macroeconomic risks to this view cannot be ignored, however. Indeed, today we see a wider range of potential outcomes for the markets than we have seen in recent history. From our perspective, Chinas economic situation poses perhaps one of the more substantial risks to the global economy. We do not think China has shed any of its credit-growth problems, and the risk of policy error in China runs high, in our view. Indeed, the entire non-U.S. political arena is fraught with a variety of uncertainties, so focusing on what could go wrong has become an important part of our analytical forecasting.

Thank you, Aaron, for your time and insights today.

The views expressed in this report are exclusively those of Putnam Management and are subject to change. They are not meant as investment advice.

Please note that the holdings discussed in this report may not have been held by the fund for the entire period. Portfolio composition is subject to review in accordance with the funds investment strategy and may vary in the future. Current and future portfolio holdings are subject to risk.

Portfolio Manager Aaron M. Cooper, CFA, is Director of Global Equity Research at Putnam. He holds an A.B. from Harvard University. Aaron joined Putnam in and has been in the investment industry since

In addition to Aaron, your funds managers are Jacquelyne J. Cavanaugh; Kelsey Chen, Ph.D.; Neil P. Desai, Kathryn B. Lakin; Ferat Ongoren; and Walter D. Scully, CPA.




This chart shows the funds largest allocation shifts, by percentage, over the past six months. Allocations are shown as a percentage of the funds net assets. Current period summary information may differ from the portfolio schedule included in the financial statements due to the inclusion of derivative securities, any interest accruals, the exclusion of as-of trades, if any, the use of different classifications of securities for presentation purposes, and rounding. Holdings and allocations may vary over time.













Research Fund











IN THE NEWS

With central banks exhausting the more traditional methods aimed at stimulating their economies, some are considering more novel strategies. Increasingly, central bankers and economists are discussing the merits of so-called helicopter money, which conjures images of money being dropped on the populace from the sky. Considered somewhat radical, the term was adopted in by economist Milton Friedman, who described the idea of a central bank printing money and injecting the cash directly into the economy, with the aim of boosting consumer demand and spending, and kick-starting a recovery. It differs from traditional stimulus measures, such as the U.S. government selling U.S. Treasury securities to the public in order to finance spending. With interest rates at zero or even in negative territory in major world economies like Japan and some European nations, the concept of helicopter money is gaining popularity. Under this strategy, cash could be transferred to people in the form of a government tax break or by simply making a direct deposit into individual bank accounts. Critics of helicopter money, however, say it could cause runaway inflation.













Research











Your funds performance

This section shows your funds performance, price, and distribution information for periods ended July the end of its most recent fiscal year. In accordance with regulatory requirements for mutual funds, we also include performance information as of the most recent calendar quarter-end and expense information taken from the funds current prospectus. Performance should always be considered in light of a funds investment strategy. Data represent past performance. Past performance does not guarantee future results. More recent returns may be less or more than those shown. Investment return and principal value will fluctuate, and you may have a gain or a loss when you sell your shares. Performance information does not reflect any deduction for taxes a shareholder may owe on fund distributions or on the redemption of fund shares. For the most recent month-end performance, please visit the Individual Investors section at putnam.com or call Putnam at Class R, and Y shares are not available to all investors. See the Terms and Definitions section in this report for definitions of the share classes offered by your fund.

Fund performance Total return for periods ended





































Class A

Class B

Class C

Class M

Class R

Class

Class Y



(inception dates)



























Before

After









Before

After

Net

Net

Net





sales

sales

Before

After

Before

After

sales

sales

asset

asset

asset





charge

charge

CDSC

CDSC

CDSC

CDSC

charge

charge

value

value

value











Annual average

























(life of fund)

































years

























Annual average

































years

























Annual average

































years

























Annual average

































year

































Current performance may be lower or higher than the quoted past performance, which cannot guarantee future results. After-sales-charge returns for class A and M shares reflect the deduction of the maximum and sales charge, respectively, levied at the time of purchase. Class B share returns after contingent deferred sales charge (CDSC) reflect the applicable CDSC, which is in the first year, declining over time to in the sixth year, and is eliminated thereafter. Class C share returns after CDSC reflect a CDSC for the first year that is eliminated thereafter. Class R, and Y shares have no initial sales charge or CDSC. Performance for class B, C, M, R, and Y shares before their inception is derived from the historical performance of class A shares, adjusted for the applicable sales charge (or CDSC) and the higher operating expenses for such shares, except for class Y shares, for which fees are not applicable. Performance for class shares prior to their inception is derived from the historical performance of class Y shares and has not been adjusted for the lower investor servicing fees applicable to class shares; had it, returns would have been higher.

Recent performance may have benefited from one or more legal settlements.

For a portion of the periods, the fund had expense limitations, without which returns would have been lower.

Class B share performance reflects conversion to class A shares after eight years.













Research Fund











Comparative index returns For periods ended





















Lipper Large-Cap Core





S&amp;P Index

Funds category average*











Annual average (life of fund)















years







Annual average















years







Annual average















years







Annual average















year















Index and Lipper results should be compared with fund performance before sales charge, before CDSC, or at net asset value.

* Over the and life-of-fund periods ended there were and funds, respectively, in this Lipper category.


Past performance does not indicate future results. At the end of the same time period, a investment in the funds class B and C shares would have been valued at and respectively, and no contingent deferred sales charges would apply. A investment in the funds class M shares after sales charge) would have been valued at A investment in the funds class R, and Y shares would have been valued at and respectively.













Research Fund











Fund price and distribution information For the period ended































Distributions

Class A

Class B

Class C

Class M

Class R

Class

Class Y











Number

























Income

























Capital gains

























Total



























Before

After

Net

Net

Before

After

Net

Net

Net





sales

sales

asset

asset

sales

sales

asset

asset

asset



Share value

charge

charge

value

value

charge

charge

value

value

value







































































The classification of distributions, if any, is an estimate. Before-sales-charge share value and current dividend rate for class A and M shares, if applicable, do not take into account any sales charge levied at the time of purchase. After-sales-charge share value, current dividend rate, and current SEC yield, if applicable, are calculated assuming that the maximum sales charge for class A shares and for class M shares) was levied at the time of purchase. Final distribution information will appear on your year-end tax forms.

Fund performance as of most recent calendar quarterTotal return for periods ended





































Class A

Class B

Class C

Class M

Class R

Class

Class Y



(inception dates)



























Before

After









Before

After

Net

Net

Net





sales

sales

Before

After

Before

After

sales

sales

asset

asset

asset





charge

charge

CDSC

CDSC

CDSC

CDSC

charge

charge

value

value

value











Annual average

























(life of fund)

































years

























Annual average

































years

























Annual average

































years

























Annual average

































year

































See the discussion following the fund performance table on page for information about the calculation of fund performance.













Research Fund











Your funds expenses

As a mutual fund investor, you pay ongoing expenses, such as management fees, distribution fees fees), and other expenses. In the most recent six-month period, your funds expenses were limited; had expenses not been limited, they would have been higher. Using the following information, you can estimate how these expenses affect your investment and compare them with the expenses of other funds. You may also pay one-time transaction expenses, including sales charges (loads) and redemption fees, which are not shown in this section and would have resulted in higher total expenses. For more information, see your funds prospectus or talk to your financial representative.

Expense ratios





























Class A

Class B

Class C

Class M

Class R

Class

Class Y











Total annual operating

















expenses for the fiscal year

















ended

























Annualized expense ratio

















for the six-month period

















ended

























Fiscal-year expense information in this table is taken from the most recent prospectus, is subject to change, and may differ from that shown for the annualized expense ratio and in the financial highlights of this report.

Expenses are shown as a percentage of average net assets.

* Restated to reflect current fees resulting from a change to the funds investor servicing arrangements effective September

Expense ratios for each class are for the funds most recent fiscal half year. As a result of this, ratios may differ from expense ratios based on one-year data in the financial highlights.

Other expenses are based on expenses of class Y shares for the funds last fiscal year, adjusted to reflect the lower investor servicing fees applicable to class shares.

Expenses per

The following table shows the expenses you would have paid on a investment in each class of the fund from to It also shows how much a investment would be worth at the close of the period, assuming actual returns and expenses.





























Class A

Class B

Class C

Class M

Class R

Class

Class Y











Expenses paid per

























Ending value (after expenses)

























* Expenses for each share class are calculated using the funds annualized expense ratio for each class, which represents the ongoing expenses as a percentage of average net assets for the six months ended The expense ratio may differ for each share class.

Expenses are calculated by multiplying the expense ratio by the average account value for the period; then multiplying the result by the number of days in the period; and then dividing that result by the number of days in the year.













Research











Estimate the expenses you paid

To estimate the ongoing expenses you paid for the six months ended use the following calculation method. To find the value of your investment on call Putnam at


Compare expenses using the SECs method

The Securities and Exchange Commission (SEC) has established guidelines to help investors assess fund expenses. Per these guidelines, the following table shows your funds expenses based on a investment, assuming a hypothetical annualized return. You can use this information to compare the ongoing expenses (but not transaction expenses or total costs) of investing in the fund with those of other funds. All mutual fund shareholder reports will provide this information to help you make this comparison. Please note that you cannot use this information to estimate your actual ending account balance and expenses paid during the period.





























Class A

Class B

Class C

Class M

Class R

Class

Class Y











Expenses paid per

























Ending value (after expenses)

























* Expenses for each share class are calculated using the funds annualized expense ratio for each class, which represents the ongoing expenses as a percentage of average net assets for the six months ended The expense ratio may differ for each share class.

Expenses are calculated by multiplying the expense ratio by the average account value for the six-month period; then multiplying the result by the number of days in the six-month period; and then dividing that result by the number of days in the year.













Research Fund











Terms and definitions

Important terms

Total return shows how the value of the funds shares changed over time, assuming you held the shares through the entire period and reinvested all distributions in the fund.

Before sales charge, or net asset value, is the price, or value, of one share of a mutual fund, without a sales charge. Before-sales-charge figures fluctuate with market conditions, and are calculated by dividing the net assets of each class of shares by the number of outstanding shares in the class.

After sales charge is the price of a mutual fund share plus the maximum sales charge levied at the time of purchase. After-sales-charge performance figures shown here assume the maximum sales charge for class A shares and for class M shares.

Contingent deferred sales charge (CDSC) is generally a charge applied at the time of the redemption of class B or C shares and assumes redemption at the end of the period. Your funds class B CDSC declines over time from a maximum during the first year to during the sixth year. After the sixth year, the CDSC no longer applies. The CDSC for class C shares is for one year after purchase.

Share classes

Class A shares are generally subject to an initial sales charge and no CDSC (except on certain redemptions of shares bought without an initial sales charge).

Class B shares are not subject to an initial sales charge and may be subject to a CDSC.

Class C shares are not subject to an initial sales charge and are subject to a CDSC only if the shares are redeemed during the first year.

Class M shares have a lower initial sales charge and a higher fee than class A shares and no CDSC.

Class R shares are not subject to an initial sales charge or CDSC and are only available to employer-sponsored retirement plans.

Class shares are not subject to an initial sales charge or CDSC, and carry no fee. They are only available to employer-sponsored retirement plans.

Class Y shares are not subject to an initial sales charge or CDSC, and carry no fee. They are generally only available to corporate and institutional clients and clients in other approved programs.

Comparative indexes

Bloomberg Barclays U.S. Aggregate Bond Index is an unmanaged index of U.S. investment-grade fixed-income securities.

BofA Merrill Lynch U.S. Treasury Bill Index is an unmanaged index that seeks to measure the performance of U.S. Treasury bills available in the marketplace.

S&amp;P Index is an unmanaged index of common stock performance.

Indexes assume reinvestment of all distributions and do not account for fees. Securities and performance of a fund and an index will differ. You cannot invest directly in an index.

Lipper is a third-party industry-ranking entity that ranks mutual funds. Its rankings do not reflect sales charges. Lipper rankings are based on total return at net asset value relative to other funds that have similar current investment styles or objectives as determined by Lipper. Lipper may change a funds category assignment at its discretion. Lipper category averages reflect performance trends for funds within a category.













Research











Other information for shareholders

Proxy voting

Putnam is committed to managing our mutual funds in the best interests of our shareholders. The Putnam funds proxy voting guidelines and procedures, as well as information regarding how your fund voted proxies relating to portfolio securities during the period ended June are available in the Individual Investors section of putnam.com, and on the Securities and Exchange Commission (SEC) website, www.sec.gov. If you have questions about finding forms on the SECs website, you may call the SEC at You may also obtain the Putnam funds proxy voting guidelines and procedures at no charge by calling Putnams Shareholder Services at

Fund portfolio holdings

The fund will file a complete schedule of its portfolio holdings with the SEC for the first and third quarters of each fiscal year on Form N-Q. Shareholders may obtain the funds Form N-Q on the SECs website at www.sec.gov. In addition, the funds Form N-Q may be reviewed and copied at the SECs Public Reference Room in Washington, D.C. You may call the SEC at for information about the SECs website or the operation of the Public Reference Room.

Trustee and employee fund ownership

Putnam employees and members of the Board of Trustees place their faith, confidence, and, most importantly, investment dollars in Putnam mutual funds. As of July Putnam employees had approximately and the Trustees had approximately invested in Putnam mutual funds. These amounts include investments by the Trustees and employees immediate family members as well as investments through retirement and deferred compensation plans.













Fund











Important notice regarding Putnams privacy policy

In order to conduct business with our shareholders, we must obtain certain personal information such as account holders names, addresses, Social Security numbers, and dates of birth. Using this information, we are able to maintain accurate records of accounts and transactions.

It is our policy to protect the confidentiality of our shareholder information, whether or not a shareholder currently owns shares of our funds. In particular, it is our policy not to sell information about you or your accounts to outside marketing firms. We have safeguards in place designed to prevent unauthorized access to our computer systems and procedures to protect personal information from unauthorized use.

Under certain circumstances, we must share account information with outside vendors who provide services to us, such as mailings and proxy solicitations. In these cases, the service providers enter into confidentiality agreements with us, and we provide only the information necessary to process transactions and perform other services related to your account. Finally, it is our policy to share account information with your financial representative, if youve listed one on your Putnam account.













Research Fund











Trustee approval of management contract

General conclusions

The Board of Trustees of The Putnam Funds oversees the management of each fund and, as required by law, determines annually whether to approve the continuance of your funds management contract with Putnam Investment Management, LLC (Putnam Management), the sub-management contract with respect to your fund between Putnam Management and its affiliate, Putnam Investments Limited (PIL), and the sub-advisory contract among Putnam Management, PIL, and another affiliate, The Putnam Advisory Company (PAC). The Board, with the assistance of its Contract Committee, requests and evaluates all information it deems reasonably necessary under the circumstances in connection with its annual contract review. The Contract Committee consists solely of Trustees who are not interested persons (as this term is defined in the Investment Company Act of as amended (the Act)) of The Putnam Funds (Independent Trustees).

At the outset of the review process, members of the Boards independent staff and independent legal counsel discussed with representatives of Putnam Management the annual contract review materials furnished to the Contract Committee during the course of the previous years review, identifying possible changes in these materials that might be necessary or desirable for the coming year. Following these discussions and in consultation with the Contract Committee, the Independent Trustees independent legal counsel requested that Putnam Management and its affiliates furnish specified information, together with any additional information that Putnam Management considered relevant, to the Contract Committee. Over the course of several months ending in June the Contract Committee met on a number of occasions with representatives of Putnam Management, and separately in executive session, to consider the information that Putnam Management provided, as well as supplemental information provided in response to an additional request made by the Contract Committee. Throughout this process, the Contract Committee was assisted by the members of the Boards independent staff and by independent legal counsel for The Putnam Funds and the Independent Trustees.

In May the Contract Committee met in executive session to discuss and consider its recommendations with respect to the continuance of the contracts. At the Trustees June meeting, the Contract Committee met in executive session with the other Independent Trustees to review a summary of the key financial, performance and other data that the Contract Committee considered in the course of its review. The Contract Committee then presented its written report, which summarized the key factors that the Committee had considered and set forth its recommendations. The Contract Committee then recommended, and the Independent Trustees approved, the continuance of your funds management, sub-&shy;management and sub-advisory contracts, effective July (Because PIL and PAC are affiliates of Putnam Management and Putnam Management remains fully responsible for all services provided by PIL and PAC, the Trustees have not attempted to evaluate PIL or PAC as separate entities, and all subsequent references to Putnam Management below should be deemed to include reference to PIL and PAC as necessary or appropriate in the context.)

The Independent Trustees approval was based on the following conclusions:

That the fee schedule in effect for your fund represented reasonable compensation in light of the nature and quality of the services being provided to the fund, the fees paid by













Fund











competitive funds, the costs incurred by Putnam Management in providing services to the fund, and the continued application of certain reductions and waivers noted below; and

That the fee schedule in effect for your fund represented an appropriate sharing between fund shareholders and Putnam Management of such economies of scale as may exist in the management of the fund at current asset levels.

These conclusions were based on a comprehensive consideration of all information provided to the Trustees and were not the result of any single factor. Some of the factors that figured particularly in the Trustees deliberations and how the Trustees considered these factors are described below, although individual Trustees may have evaluated the information presented differently, giving different weights to various factors. It is also important to recognize that the management arrangements for your fund and the other Putnam funds are the result of many years of review and discussion between the Independent Trustees and Putnam Management, that some aspects of the arrangements may receive greater scrutiny in some years than others, and that the Trustees conclusions may be based, in part, on their consideration of fee arrangements in previous years. For example, with some minor exceptions, the funds current fee arrangements under the management contracts were first implemented at the beginning of following extensive review by the Contract Committee and discussions with representatives of Putnam Management, as well as approval by shareholders.

Management fee schedules and total expenses

The Trustees reviewed the management fee schedules in effect for all Putnam funds, including fee levels and breakpoints. The Trustees also reviewed the total expenses of each Putnam fund, recognizing that in most cases management fees represented the major, but not the sole, determinant of total costs to shareholders. (In a few instances, funds have implemented so-called all-in management fees covering substantially all routine fund operating costs.)

In reviewing fees and expenses, the Trustees generally focus their attention on material changes in circumstances for example, changes in assets under management, changes in a funds investment style, changes in Putnam Managements operating costs or profitability, or changes in competitive practices in the mutual fund industry that suggest that consideration of fee changes might be warranted. The Trustees concluded that the circumstances did not indicate that changes to the management fee structure for your fund would be appropriate at this time.

Under its management contract, your fund has the benefit of breakpoints in its management fee schedule that provide shareholders with economies of scale in the form of reduced fee rates as assets under management in the Putnam family of funds increase. The Trustees concluded that the fee schedule in effect for your fund represented an appropriate sharing of economies of scale between fund shareholders and Putnam Management.

As in the past, the Trustees also focused on the competitiveness of each funds total expense ratio. In order to support the effort to have fund expenses meet competitive standards, the Trustees and Putnam Management have implemented certain expense limitations that were in effect during your funds fiscal year ending in These expense limitations were: (i) a contractual expense limitation applicable to specified retail open-end funds, including your fund, of basis points on investor servicing fees and expenses and (ii) a contractual expense limitation applicable to specified open-end funds, including your fund, of basis points on so-called other expenses (i.e., all expenses exclusive of management fees,













Research Fund











distribution fees, investor servicing fees, investment-related expenses, interest, taxes, brokerage commissions, acquired fund fees and expenses and extraordinary expenses). These expense limitations attempt to maintain competitive expense levels for the funds. Most funds, including your fund, had sufficiently low expenses that these expense limitations were not operative during their fiscal years ending in Putnam Management has agreed to maintain these expense limitations until at least November and to reduce the contractual expense limitation on investor servicing fees and expenses from basis points to basis points effective September Putnam Managements support for these expense limitation arrangements was an important factor in the Trustees decision to approve the continuance of your funds management, sub-management and sub-advisory contracts.
