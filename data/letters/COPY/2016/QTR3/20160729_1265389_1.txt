Dear Shareowner,

The first half of has featured some remarkable twists and turns in the
global economy. The year began on shaky footing as surging market volatility
brought about by fears of weaker global economic growth as well as falling oil
prices depressed returns for investors. In the US, both equity and fixed-income
markets declined significantly through the first six weeks of the year, only to
recover the losses by the end of the first quarter when market sentiment
shifted, due in part to accommodative monetary policies from the world's central
banks.

Midway through the first quarter, for example, the US Federal Reserve System
(the Fed) backed off plans to raise interest rates four times in and the
European Central Bank announced a more comprehensive asset-purchasing program in
the hopes of encouraging lending, and boosting both inflation and economic
growth. The markets responded with a solid rally. By the end of May, in fact, US
equities, as measured by the Standard & Poor's Index, had generated a solid
year-to-date (YTD) return of and the YTD return (as of of the
Barclays Aggregate Bond Index, a common measure of the US fixed-income market,
was

As markets began to settle down, the Brexit vote - the vote confirming that the
United Kingdom (UK) would exit the European Union (EU) - surprised many, and the
rollercoaster ride began once again. The market sold off sharply in the first
few days after the vote, then rallied as investors sought bargains created by
the initial post-Brexit sell-off, and as more, less ominous information about
the implications of the Brexit results came to light.

From a macroeconomic perspective, Pioneer believes the negative economic impact
of Brexit on the US should be more limited compared with its effects on the UK
and Europe. However, we expect reduced global demand due to a higher level of
uncertainty and risk aversion among investors. While the spillover effects on
the US economy are unclear, we think it possible that, in the event of a
significant negative economic impact, the Fed might consider other monetary
policy options. Globally, we believe that central banks are ready to act and
that their initial focus will be to stabilize the markets and provide liquidity,
if needed.

While the Brexit vote is now official, the expectation is that the actual
process of separating the UK from the EU could take at least two years. Only in
the next several months may we begin to see signs of what path Europe will
follow as it adapts to the reality of an EU without one of its most prominent
members. Over the medium-term, however, we believe uncertainties over the future
of Europe and central banks' reactions will likely dominate financial markets,
and we believe the news flow surrounding Brexit will continue to weigh on
riskier assets. Competing for headlines, too, is the current financial condition
of many European banks. Ultimately, we think that the political and monetary
policy responses will be the major variables when it comes to managing an
orderly

Pioneer Select Mid Cap Growth Fund | Semiannual Report |


Brexit. In addition, as the second half of gets underway, we continue to
see central bank policies as generally supportive of the US economy -- for which
we maintain an expectation of modest growth this year.

Aside from the Brexit-caused uncertainties, economies around the world in both
developed and emerging markets are experiencing deep structural changes. Current
challenges include incomplete debt deleveraging in both emerging and developed
markets, where debt levels continue to grow, the transition of many emerging
markets economies from export/investment-driven models to more domestic
demand-driven models, and aging populations, which are reducing productivity and
limiting economic growth potential (primarily in the developed markets but also
in emerging markets such as China). Geopolitical instability on many fronts, the
rising risk of policy mistakes, and market liquidity issues combine to increase
the possibility of sharp swings in asset values. Meanwhile, in the US, as always
in a presidential election year, the political rhetoric of has the
potential to impact domestic sectors such as health care.

Throughout Pioneer's history, we have believed in the importance of active
management. During periods of market volatility, we view the value of active
management as even more compelling. Our experienced and tenured investment teams
focus on identifying value across global markets using proprietary research,
careful risk management, and a long-term perspective. We believe our shareowners
can benefit from the experience and tenure of our investment teams as well as
the insights generated from our extensive research process.

As always, and particularly during times of market uncertainty, we encourage you
to work with your financial advisor to develop an overall investment plan that
addresses both your short- and long-term goals, and to implement such a plan in
a disciplined manner.

We greatly appreciate the trust you have placed in us and look forward to
continuing to serve you in the future.

Sincerely,
