Dear Shareholders: 
Global headwinds continue to restrain growth and profits, though developed market equities have remained resilient, underpinned by record-low interest rates. Rates
are 

likely to stay low over the medium term, as many central banks have implemented accommodative monetary policies in an effort to reinvigorate their economies. Markets will have to deal with an
additional headwind in the coming months and years as investors grapple with the fallout from the United Kingdom’s decision to leave the European Union. That process will take a considerable period of time to play out against a highly uncertain
backdrop. Weakness in the pound sterling in the wake of the referendum has been a welcome shock absorber for UK financial markets. 
Emerging markets have
been beneficiaries of firmer commodity prices and diminishing fears of sharply higher interest rates from 

the U.S. Federal Reserve. China remains a source of concern for investors, as overcapacity in its manufacturing sector inhibits the government’s attempt to change its domestic economy from
one driven by exports to a consumer-driven model. 
long-term
long-term
Respectfully, 


Dear Shareholders: 
Global headwinds continue to restrain growth and profits, though developed market equities have remained resilient, underpinned by record-low interest rates. Rates
are 

likely to stay low over the medium term, as many central banks have implemented accommodative monetary policies in an effort to reinvigorate their economies. Markets will have to deal with an
additional headwind in the coming months and years as investors grapple with the fallout from the United Kingdom’s decision to leave the European Union. That process will take a considerable period of time to play out against a highly uncertain
backdrop. Weakness in the pound sterling in the wake of the referendum has been a welcome shock absorber for UK financial markets. 
Emerging markets have
been beneficiaries of firmer commodity prices and diminishing fears of sharply higher interest rates from 

the U.S. Federal Reserve. China remains a source of concern for investors, as overcapacity in its manufacturing sector inhibits the government’s attempt to change its domestic economy from
one driven by exports to a consumer-driven model. 
long-term
long-term
Respectfully, 


