Dear Shareholder,

We are pleased to provide the annual report of Western Asset Global Strategic Income Fund for the twelve-month reporting period ended July 31,
2016. Please read on for a detailed look at prevailing economic and market conditions during the Fund’s reporting period and to learn how those conditions have affected Fund performance. 
Special shareholder notice 
Effective at the close of business on June 3, 2016, the
individuals responsible for day-to-day portfolio management, development of investment strategy, oversight and coordination of the Fund are S. Kenneth Leech, Michael C. Buchanan, Paul Shuttleworth and Gordon S. Brown. These investment professionals,
all of whom are employed by Western Asset Management Company, work together with a broader investment management team. For additional information, please see the prospectus supplement dated June 3, 2016. 
As always, we remain committed to providing you with excellent service and a full spectrum of investment choices. We also remain committed to supplementing the
support you receive from your financial advisor. One way we accomplish this is through our website, www.leggmason.com. Here you can gain immediate access to market and investment information, including: 
 
Fund prices and performance, 
 
Market insights and commentaries from our portfolio managers, and 
 
A host of educational resources. 
We look
forward to helping you meet your financial goals. 
Sincerely, 


