Dear Fellow Shareholders:




Thank you for investing in The Henssler Equity Fund (The Fund). The Henssler Equity Fund believes that its focus on the fundamentals of the businesses it invests in results in the purchase of above-average, high-quality securities with strong growth potential.




If you would like to obtain periodic information regarding The Fund, we encourage you to visit our website at www.henssler.com. You may review The Henssler Equity Fund link as often as you like, as we update information regularly. Through our website, we provide details each quarter on our top holdings, industry allocation and other statistics. The website also provides media appearance information and links to articles featuring commentary and insight from The Funds management team.




The Henssler Equity Fund is more than years old since we started on June On behalf of our board of directors and management team, we thank many of you who invested in The Fund from its very beginning. We will continue to focus our best efforts exclusively on the one fund we manage, The Henssler Equity Fund.




Yours very truly,




Gene W. Henssler, Ph.D.

William G. Lako, Jr., CFP

Troy L. Harmon, CFA, CVA





This report is intended for shareholders of The Fund. It may not be distributed to prospective investors unless it is preceded or accompanied by the current fund prospectus that contains important information including risks, investment objectives, charges and expenses. The Funds prospectus is available, without charge, upon request by calling toll-free Please read and consider this information carefully before you invest or send money.








Registered representative of ALPS Distributors, Inc.








Certified Financial Planner Board of Standards Inc. owns the certification mark CFP, CERTIFIED FINANCIAL PLANNER and CFP (with flame logo) in the U.S., which it awards to individuals who successfully complete CFP Boards initial and ongoing certification requirements.








CFA Institute mark is a trademark owned by the CFA Institute.








Certified Valuation Analyst, accredited through the National Association of Certified Valuation Analysts.













Annual Report | April




















Managements Discussion On Fund Performance




April (Unaudited)
