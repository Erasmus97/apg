Dear Shareholder:






We are pleased to present the annual report of The Glenmede
family of funds for the fiscal year ended
It was another volatile year for the equity markets. The
U.S.and global economic recoveries slowed down in the
summer due to a sovereign debt crisis in Europe and an oil spill
in the Gulf of Mexico. In addition, housing and unemployment
continued to be drags on the U.S.economy. The slowdown led
to lower equity returns during the summer. However, the equity
markets recovered with very good returns for the fiscal year
ended Fixed income markets also
experienced very good returns as the Federal government
continued to increase liquidity in those markets. However, money
market funds suffered very low yields as the Federal Reserve
kept short term rates low.





At the fiscal year end on The Glenmede
Fund, Inc. and The Glenmede Portfolios (collectively the
Glenmede Funds) consisted of sixteen portfolios with
total assets of The Secured Options Portfolio
is the newest addition to the Glenmede family of funds and is
featured in our report this year. It was created to give
Glenmede clients an equity product that utilizes options
strategies to help manage risk. The fund family includes two
EAFE International Portfolios, International and Philadelphia
International, which are
sub-advised
and advised respectively by Philadelphia International Advisors
LP (PIA). These portfolios account for of the
fund familys assets as of





All of the Glenmede Funds, other than the money market funds,
are managed to seek long-term total returns consistent with
reasonable risk to principal for their asset category. Efforts
are made to keep expenses at competitive levels. All of the
portfolios managed by Glenmede Investment Management LP include
a quantitative style of investing.





This fiscal year ended showed positive
returns across all asset classes as the economy continued a slow
recovery. The SP


gained and the Barclays Capital U.S.Aggregate Bond


gained for the fiscal year ended
The Morgan Stanley EAFE


did not perform as well as domestic stocks for the fiscal year
gaining Small Cap stocks outperformed Large Cap stocks
with the Russell


returning versus a return of on the Russell


for the fiscal year ended The Glenmede
Large Cap Value Portfolio achieved a four star (****) Overall
Morningstar


among Large Value Equity Funds for the period ended
(based on risk adjusted returns). The
Overall Morningstar
RatingTM

for a fund is derived from a weighted average of the performance
figures associated with its three-, five- and ten-year (if
applicable) Morningstar
RatingTM

metric.





In response to the continuing weak economy, the Federal Reserve
last lowered short term rates to on
The yield curve flattened during the fiscal year as one
year treasuries dropped points in yield and the
treasury dropped


Money market yields remain at all time lows. The seven day
yields of the
























Government Cash Portfolio and Tax
Exempt Cash Portfolio were and respectively as of







Returns in the fixed income markets were very good across all
sectors, but especially in Corporates and High Yield where
credit spreads to U.S.Treasuries continued to narrow. The
Glenmede bond portfolios all have a high quality bias. The
Glenmede Muni Intermediate Portfolio achieved a five star
(*****) Overall Morningstar


among Muni Short Funds for the period ended
(based on risk-adjusted returns).





Portfolio Highlights for each individual fund are included in
this report. We welcome any questions about the Glenmede Funds
and thank our clients for their continued support.





Sincerely,
