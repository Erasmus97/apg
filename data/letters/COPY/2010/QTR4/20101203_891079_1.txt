Dear Shareholder: 
Attached is the annual report for the GE Funds for the twelve-month period ended September 30, 2010. The report contains information about the performance of your Fund, and other Fund specific data, along with
portfolio manager commentary. We have provided you with an overview of the investment markets, along with our investment outlook for the coming months. 
Market Overview 
During the fiscal year ended September 30, 2010, virtually all of the major U.S. equity indices enjoyed double-digit gains, while international equity markets, as measured by the MSCI EAFE Index, posted more modest
gains. The emerging equity markets as a whole significantly outperformed the developed equity markets on a relative basis, as investors appeared to favor the growth prospects of the developing world. As spreads tightened dramatically in the U.S.
fixed income markets, virtually all non-Treasury sectors outperformed Treasuries for the 12-month period overall, although U.S. Treasury securities still generated positive returns. This is not to say that the period was without significant
volatility. In our semi-annual report for the six-month period ended March 31, 2010, we noted that the sharp rise in global equities had slowed as market participants appeared to alternate between risk-seeking activities based on improving economic
indicators and risk aversion due to concerns about the withdrawal of government stimulus measures. This theme continued to play out over the last half of the fiscal year as uncertainty about the course of the global economic recovery persisted.

Although the second quarter of 2010 started quietly, stocks plunged in May as concerns about Europe’s sovereign debt problems pressured equity
markets and many investors sought defensive investments such as gold and treasury bonds. Adding to Greece’s sovereign debt woes, contagion anxieties grew after Standard and Poor’s lowered the debt rating of Spain, citing a possible further
deterioration in its budgetary position. In June, the 2010 G20 Toronto Summit confirmed that the industrialized world leaders were committed to reducing budget deficits through austerity measures. Some investors, however, worried that these measures
could reduce global growth and slow the economic recovery. Several other concerns fuelled greater risk aversion during the second quarter including increased evidence of 

slower growth in China, impending U.S. financial reform legislation, an alarming May 6th “flash crash” which sent U.S. stocks plummeting within minutes, the spreading impact of
BP’s massive oil spill in the Gulf of Mexico and a new resource tax in Australia. At quarter end, the S&amp;P 500 Index and the MSCI EAFE Index had tumbled 11.4% and 14.0%, respectively, in the three months ended June 30, 2010. In this
environment, U.S. Treasuries were the “safe” investment of choice for many investors seeking to avoid riskier asset classes. The “flight to quality” pulled treasury yields down across all maturity periods. 
Global equities moved firmly in a positive direction in the third quarter of 2010, but not without a bumpy ride. Stocks enjoyed strong gains in both July and
September, while August was marred by lackluster economic data and the resurrection of doubts about the growth prospects of developed economies. Volatility, while falling through much of the period, proved that, at the first sign of trouble,
investor sentiment is not yet robust enough to withstand doubts caused by a lack of recovery in consumption and lack of progress in deleveraging, especially at the government level. In Europe, a large focus was on financial reform and sovereign debt
during the third quarter. In July, two announcements gave rise to better market sentiment: first, results from the European Union’s stress test on the financial health of 91 banks showed that just seven banks failed; and second, the Basel
Committee on Banking Supervision proposed softer capital requirements for financial companies worldwide. In September, credit spreads for several European countries jumped as the focus of sovereign debt worries shifted from Greece to Ireland and
Portugal. The market became increasingly concerned about the ability of these countries to reduce their fiscal deficits in light of the weakening economic environment (Portugal) and the uncertain, but high costs associated with their bank bailouts
(Ireland). Moody’s downgraded Spain’s credit rating to Aa1 from Aaa on September 30th, citing the nation’s weak economic outlook. However, there was much to be optimistic about in the third quarter, including increased clarity about
bank capital requirements, historically low interest rates, better-than-expected corporate earnings and the prospect of more quantitative easing by the U.S. Federal Reserve. For the three months ended September 30, 2010, the S&amp;P 500 Index and
MSCI EAFE Index gained 11.3% and 16.5%, respectively. 
As the developed economies continued to move from recovery to sluggish expansion, financial
markets posted mostly positive results for the six- and 12-month periods ended September 30, 2010. 
 

