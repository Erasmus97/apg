Dear Shareholder,
Vanguard International Value Fund returned about 9% for the year ended October 31, 2010, lagging the average return of peer-group funds. The fund finished a little more than 1 percentage point ahead of its benchmark index, although the difference was due largely to an adjustment known as fair-value pricing, which is required of the fund but not the index. The concept and its rationale are explained on page 6.
The International Value Fund’s selections of European stocks, its largest allocation by far, and emerging markets stocks contributed the most to return. The fund was less successful in its choices of Pacific region stocks. The U.S. dollar fluctuated against various currencies during the period; most notably it strengthened against the euro and the British pound, which because of the fund’s heavy allocation to European stocks served to reduce the fund’s return for U.S.-based investors.
Note: If you invest in the fund through a taxable account, you may wish to review the after-tax returns for investors in the highest tax bracket, which appear later in this report.
Stock market performance 
2
months, the mood turned. Stock prices climbed on continued strength in corporate earnings. In the United States, stocks also seemed to get a boost from the Federal Reserve Board’s hints that it would try to stimulate the economy with a second round of U.S. Treasury bond purchases. (In early November, the Fed announced that it would buy as much as $600 billion in Treasuries.)
For the 12 months, the broad U.S. stock market returned about 19%, a performance that was better than it felt in a year of ups and downs. Small-capitalization stocks did even better. International stocks returned about 13% on the strength of a powerful rally in emerging markets and solid single-digit gains in developed markets in Europe and the Pacific region.
Despite shrinking yields, 
Market Barometer
 
3
Europe, emerging markets 
The fund’s advisors did miss a variety of opportunities among stocks in Switzerland, the U.K., and France, but at the same time, they navigated well among Europe’s troubled financial industry stocks, sidestepping some of the holdings that weighed on the fund’s benchmark.
The fund’s emerging markets holdings contributed almost as much to the portfolio’s return as the European selections, as a strong return (about 16%) compensated for this segment’s smaller slice of the portfolio (about a fifth of assets). The fund benefited from the general rally among emerging markets stocks and got an added boost from some selections in China, such as Weichai Power Co., a maker of diesel engines. The advisors also made
Expense Ratios
The fund expense ratio shown is from the prospectus dated February 24, 2010, and represents estimated costs for the current fiscal year. For 
4
some astute choices among information technology stocks, such as Hynix Semiconductor of South Korea.
Holdings in Japan and other developed countries in the Pacific region, which also represented about a fifth of the portfolio’s assets, provided only a marginal contribution to return. Some good selections, such as Canon in the information technology sector, were outweighed by poor choices elsewhere.
The fund’s long-term record 
Total Returns
Spliced International Index: MSCI EAFE Index through May 31, 2010; MSCI All Country World Index ex USA thereafter.
The figures shown represent past performance, which is not a guarantee of future results. (Current performance may be lower or higher than the performance data cited. For performance data current to the most recent month-end, visit our website at vanguard.com/performance.) Note, too, that both investment returns and principal value can fluctuate widely, so an investor’s shares, when sold, could be worth more or less than their original cost.
5
 
 
of close to 6%, despite the significant blow to markets around the world (and to long-term averages) of the steep plunge in stocks that occurred in 2008. By comparison, the average return of peer-group international funds over the period was about 3%.
The intricacies of investing in the world’s equity markets underscore the benefits that the fund derives from the expertise of some of the world’s most skilled advisors: AllianceBernstein, Lazard Asset Management, Edinburgh Partners, and Hansberger Global Investors. In addition, the fund’s low expenses help you keep more of its return, a benefit that compounds over time.
International investing 
The variation in performance from region to region highlights again the value of a diversified portfolio. Markets around the globe may be affected by similar events, but their response to those developments often differs. Our research suggests that, over time, those differences can become more pronounced. As the impact of a global shock such as the recent financial crisis recedes, stock markets once again take their cues from developments in local economies, enhancing the risk-return properties of a globally diversified portfolio.
6
That’s why we encourage you to build a portfolio that holds a mix of stocks—including international equities, both value- and growth-oriented—and bonds that is tailored to match your risk profile and time horizon. How future economic developments will play out around the globe is difficult to predict. But a time-tested way to meet those challenges is to hold a well-diversified portfolio while keeping costs low. Vanguard International Value Fund can play a role in such an investing strategy.
As always, thank you for entrusting your assets to Vanguard.
Sincerely,


Dear Shareholder,
For the fiscal year ended October 31, 2010, Vanguard Diversified Equity Fund returned 17.68%, a bit below the 18.60% result of its benchmark, the MSCI US Broad Market Index, but slightly ahead of the 17.23% average result for peer multi-capitalization core funds.
Diversified Equity Fund is a “fund of funds,” investing in eight actively managed Vanguard mutual funds. Through the underlying funds, which are intended to complement each other, it invests in growth and value stocks and is broadly exposed to all segments of the U.S. equity market. Large-cap stocks predominate, but stocks of all market capitalizations are represented.
If you hold shares of the fund in a taxable account, you may wish to review the section on the fund’s after-tax returns that appears later in this report.
Stock market performance 
2
the Federal Reserve Board’s hints that it would try to stimulate the economy with a second round of U.S. Treasury bond purchases. (In early November, the Fed announced that it would buy as much as $600 billion in Treasuries.)
For the 12 months, the broad U.S. stock market returned about 19%, a performance that was better than it felt in a year of ups and downs. Small-capitalization stocks did even better. International stocks returned about 13% on the strength of a powerful rally in emerging markets and solid single-digit gains in developed markets in Europe and the Pacific region.
Despite shrinking yields, 
Market Barometer
 
3
Overall performance illustrates 
All eight of Diversified Equity’s underlying funds posted double-digit returns for the fiscal year. With a few exceptions, the growth funds outperformed their value counterparts, and the mid- and small-cap funds fared better than the large-caps.
Expense Ratios
The acquired fund fees and expenses—drawn from the prospectus dated February 24, 2010—represent an estimate of the weighted average 


