Dear Shareholder,

Columbia Large Core Quantitative Fund (the Fund) Class A shares gained
(excluding sales charge) for the months ended July The Fund
outperformed the Standard & Poor's Index (S&P Index), which rose
as well as the Lipper Large-Cap Core Funds Index, representing the Fund's peer
group, which advanced for the same period.

SIGNIFICANT PERFORMANCE FACTORS
Against a strengthening economic backdrop supported by generally positive data,
the equity market rally that had begun in mid-March continued with little
interruption until May As investor risk aversion had abated significantly,
lower quality stocks led the way during these months. The rally then stalled in
May and June of as worries

SECTOR (at July
- ---------------------------------------------------------------------





Sectors can be comprised of several industries. Please refer to the section
entitled "Portfolio of Investments" for a complete listing. No single
industry exceeded of portfolio assets.
Percentages indicated are based upon total investments (excluding
Investments of Cash Collateral Received for Securities on Loan). The Fund's
composition is subject to change.
Cash & Cash Equivalents.

The sectors identified above are based on the Global Industry Classification
Standard (GICS), which was developed by, and is the exclusive property of,
Morgan Stanley Capital International Inc. and Standard & Poor's, a division of
The McGraw-Hill Companies, Inc.


- --------------------------------------------------------------------------------
COLUMBIA LARGE CORE QUANTITATIVE FUND -- ANNUAL REPORT



- --------------------------------------------------------------------------------

heightened about the sovereign debt crisis in Europe and about the trajectory of
the nascent global economic recovery. Investors questioned whether the recovery
could be sustained without the tremendous fiscal and monetary stimulus injected
into the economy since the credit crisis, as several government and Federal
Reserve programs expired in March and April Further, unemployment and
housing data indicated persistent weakness. Investors also were uncertain about
how much of the first quarter rebound in corporate earnings was
attributable merely to rebuilding of inventories. The potential of a "double-
dip" recession or even deflation became of heightened concern. Equity market
volatility rose. That said, by the end of the annual period, a strong start to
the second quarter earnings reporting season buoyed the equity market as
did an overall improvement in risk sentiment. The release of the results of the
European bank stress tests on July also had a positive impact on equity
markets broadly.

The Fund's performance was primarily driven by the three quantitative-based
investment themes we employed in selecting stocks for the Fund's
portfolio -- momentum, valuation and quality. During the period, the
valuation and momentum themes outperformed the S&P

TOP TEN (at July
- ---------------------------------------------------------------------





Percentages indicated are based upon total investments (excluding
Investments of Cash Collateral Received for Securities on Loan and Cash &
Cash Equivalents).

For further detail about these holdings, please refer to the section entitled
"Portfolio of Investments."

Fund holdings are as of the date given, are subject to change at any time, and
are not recommendations to buy or sell any security.


- --------------------------------------------------------------------------------
COLUMBIA LARGE CORE QUANTITATIVE FUND -- ANNUAL REPORT



MANAGER COMMENTARY (continued) -------------------------------------------------


Index, more than offsetting the trailing performance of the quality theme. The
valuation theme, which favors cheaper price-to-earnings ratio (P/E) stocks,
benefited from exposure to consumer discretionary and financials stocks. Indeed,
having increased the Fund's emphasis on the valuation theme in the last months
of the prior fiscal year, the Fund was well positioned during this annual period
for the rebound in value-oriented stocks that occurred in and into
The momentum theme, designed in part to capture investor sentiment over the near
to mid term, was aided mainly by information technology stocks. The quality
theme, which was intended to serve in part as a defensive measure during equity
market corrections when investor risk aversion increases, trailed due to
exposure to more defensive sectors such as health care. It is important to
remember that the themes we used take turns in leading performance over time,
demonstrating the advantages of employing style diversification. Such variance
in performance supports our research indicating that the style diversification
provided by the three very different quantitative-based themes is a significant
investment advantage over the long term, even though the Fund may experience
underperformance in the short term. At the same time, we are continuously
looking for ways to improve our investment process and to enhance the efficacy
of the quantitative-based themes we use in the Fund.

Following a specific, disciplined process, we do not make sector or industry
bets based on economic or equity market outlooks. That said, the Fund's
quantitative-based themes led to a modest bias toward higher quality stocks due
to market volatility. This bias hurt performance, particularly in the beginning
of the annual period when lower quality stocks led. However, this same bias
helped in the last months of the annual period when market volatility
heightened. The Fund's themes also positioned the Fund toward mega-cap, or the
largest cap, stocks. This positioning had minimal impact on Fund performance
during the annual period.

The Fund's quantitative-based themes led to various sector weightings that,
together, detracted from results. Having only a modest position in industrials,
which was the top performing sector of the S&P Index during the period, and
a more sizable weighting in health care, which lagged, hurt most. Partially
offsetting these negatives was the positive

- --------------------------------------------------------------------------------
COLUMBIA LARGE CORE QUANTITATIVE FUND -- ANNUAL REPORT



- --------------------------------------------------------------------------------


effect of a significant allocation to the relatively stronger performing
telecommunication services sector and having only a modest exposure to the
weaker utilities sector.

Because we use a bottom-up approach, it is not surprising that stock selection
was the primary driver of the Fund's results. Specifically, the Fund benefited
from strong stock selection in information technology, energy and financials.
This more than offset the combined effect of stock selection in consumer staples
and health care, which detracted.

Among individual holdings, information technology leaders APPLE and MICROSOFT,
each selected by the momentum theme, contributed significantly to the Fund's
returns. Other top contributors during the annual period included energy
companies CHEVRON and CONOCOPHILLIPS, each a valuation and quality pick, and
financials companies PNC FINANCIAL and AMERICAN EXPRESS, each selected by the
valuation theme.

Stocks that detracted most from the Fund's returns included financials company
GOLDMAN SACHS, selected by the valuation theme, energy giant EXXON MOBIL, a
quality theme pick, health benefits provider WELLPOINT, selected by the quality
theme, and integrated utilities company EXELON, a quality and valuation theme
pick.

At the end of July, the Fund's largest individual stock holdings were Apple,
Microsoft, Chevron, information technology company IBM, selected by the momentum
and quality themes, and WAL-MART STORES.



During the period, the valuation and momentum themes outperformed the
S&P Index, more than offsetting the trailing performance of the quality
theme.






- --------------------------------------------------------------------------------
COLUMBIA LARGE CORE QUANTITATIVE FUND -- ANNUAL REPORT



MANAGER COMMENTARY (continued) -------------------------------------------------

CHANGES TO THE FUND'S PORTFOLIO
As a result of quantitative-based theme-driven stock selection during the annual
period, the Fund's sector allocations changed somewhat. For example, the Fund's
allocation to information technology increased, although this remained a more
modest weighting than in the S&P Index. The Fund's already modest exposure
to consumer staples compared to the S&P Index became an even greater
underweighting.

Our risk models limit the size of individual holdings, as well as sector and
industry allocations, relative to the S&P Index. For instance, the
portfolio's weightings by sector and industry are limited to no more than
overweighted or underweighted relative to the S&P Index. We also apply
additional risk measures that impose constraints on market capitalization,
price, quality, turnover, transaction costs and other variables.

Perhaps the biggest change made was that a new portfolio management team was put
in place toward the end of the annual period. The Fund's principal investment
strategies remained based on quantitative analysis, using similar investment
themes. However, while we continue to use computer-based models to analyze
stocks within sectors, the new team's models are somewhat different from those
previously used. We seek to maintain sector weighting neutrality overall
relative to the benchmark index, and the models drive stock selection by
focusing on factors within three themes -- quality, valuation and catalyst.
Quality-theme factors include profitability as well as strength and
sustainability measures, such as return on assets, return on equity,
receivables, reserve management and cash flow accruals. Valuation-theme factors
measure profitability-at-a-reasonable-price and growth-at-a-reasonable-price and
include cash flow, operating income, sales, earnings, book value and risk-
adjusted return. Catalyst-theme factors include long-term and short-term
momentum measures and estimate revisions.

OUR FUTURE STRATEGY
We hold a cautious view regarding prospects for the financial markets over the
remainder of After a solid run over the months ended July we
believe stocks appeared to be closer to fair value than they had been one year
prior. At the same time, however, we expect volatility to remain somewhat
heightened. We believe the U.S. economy

- --------------------------------------------------------------------------------
COLUMBIA LARGE CORE QUANTITATIVE FUND -- ANNUAL REPORT



- --------------------------------------------------------------------------------

will continue to recover, albeit at a relatively slow pace, and that inflation
should remain under control.

Given this view, we intend to use our quantitative-based themes, as described
above, in our stock selection process, seeking to position the Fund's portfolio
to take advantage of a shift in underlying market dynamics. At the same time,
consistent with our disciplined approach, we remain focused on the long term and
maintain the Fund's diversification across sectors and securities. We intend to
continue seeking optimal returns for the Fund through the style diversification
offered by the various themes within our well-tested quantitative investment
models. We are convinced of the merit of our multifaceted, disciplined approach
to managing risk in the portfolio and believe this combination of style
diversification and rigorous risk management will allow us to maintain the high
quality of the Fund's portfolio in whatever market conditions lie ahead.

Brian Condon, CFA(R)
Portfolio Manager
