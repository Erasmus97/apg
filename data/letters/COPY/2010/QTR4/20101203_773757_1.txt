Dear Shareholder:
The Columbia Management story began over 100 years ago, and today, we are one of the nation's largest dedicated asset managers. The recent acquisition by Ameriprise Financial, Inc. brings together the talents, resources and capabilities of Columbia Management with those of RiverSource Investments, Threadneedle (acquired by Ameriprise in 2003) and Seligman Investments (acquired by Ameriprise in 2008) to build a best-in-class asset management business that we believe is truly greater than its parts.
RiverSource Investments traces its roots to 1894 when its then newly-founded predecessor, Investors Syndicate, offered a face-amount savings certificate that gave small investors the opportunity to build a safe and secure fund for retirement, education or other special needs. A mutual fund pioneer, Investors Syndicate launched Investors Mutual Fund in 1940. In the decades that followed, its mutual fund products and services lineup grew to include a full spectrum of styles and specialties. More than 110 years later, RiverSource continues to be a trusted financial products leader.
Threadneedle, a leader in global asset management and one of Europe's largest asset managers, offers sophisticated international experience from a dedicated U.K. management team. Headquartered in London, it is named for Threadneedle Street in the heart of the city's financial district, where British investors pioneered international and global investing. Threadneedle was acquired in 2003 and today operates as an affiliate of Columbia Management.
Seligman Investments' beginnings date back to the establishment of the investment firm J. & W. Seligman & Co. in 1864. In the years that followed, Seligman played a major role in the geographical expansion and industrial development of the United States. In 1874, President Ulysses S. Grant named Seligman as fiscal agent for the U.S. Navy—an appointment that would last through World War I. Seligman helped finance the westward path of the railroads and the building of the Panama Canal. The firm organized its first investment company in 1929 and began managing its first mutual fund in 1930. In 2008, J. & W. Seligman & Co. Incorporated was acquired and Seligman Investments became an offering brand of RiverSource Investments, LLC.
We are proud of the rich and distinctive history of these firms, the strength and breadth of products and services they offer, and the combined cultures of pioneering spirit and forward thinking. Together we are committed to providing more for our shareholders than ever before.
Our business is asset management, so investors are our first priority. We dedicate our resources to identifying timely investment opportunities and provide a comprehensive choice of equity, fixed-income and alternative investments to help meet your individual needs.
We are dedicated to helping you take advantage of today's opportunities and anticipate tomorrow's. We stay abreast of the latest investment trends and ideas, using our collective insight to evaluate events and transform them into solutions you can use.
We aren't distracted by passing fads. Our teams adhere to a rigorous investment process that helps ensure the integrity of our products and enables you and your financial advisor to match our solutions to your objectives with confidence.
When you choose Columbia Management, you can be confident that we will take the time to understand your needs and help you and your financial advisor identify the solutions that are right for you. Because at Columbia Management, we don't consider ourselves successful unless you are.
Sincerely,


Dear Shareholder: 
The Columbia Management story began over 100 years ago, and today, we are one of the nation’s largest dedicated asset managers. The recent
acquisition by Ameriprise Financial, Inc. brings together the talents, resources and capabilities of Columbia Management with those of RiverSource Investments, Threadneedle (acquired by Ameriprise in 2003) and Seligman Investments (acquired by
Ameriprise in 2008) to build a best-in-class asset management business that we believe is truly greater than its parts. 
RiverSource
Investments traces its roots to 1894 when its then newly-founded predecessor, Investors Syndicate, offered a face-amount savings certificate that gave small investors the opportunity to build a safe and secure fund for retirement, education or other
special needs. A mutual fund pioneer, Investors Syndicate launched Investors Mutual Fund in 1940. In the decades that followed, its mutual fund products and services lineup grew to include a full spectrum of styles and specialties. More than 110
years later, RiverSource continues to be a trusted financial products leader. 
Threadneedle, a leader in global asset management and one of
Europe’s largest asset managers, offers sophisticated international experience from a dedicated U.K. management team. Headquartered in London, it is named for Threadneedle Street in the heart of the city’s financial district, where British
investors pioneered international and global investing. Threadneedle was acquired in 2003 and today operates as an affiliate of Columbia Management. 
Seligman Investments’ beginnings date back to the establishment of the investment firm J. &amp; W. Seligman &amp; Co. in 1864. In the years that followed, Seligman played a major role
in the geographical expansion and industrial development of the United States. In 1874, President Ulysses S. Grant named Seligman as fiscal agent for the U.S. Navy — an appointment that would last through World War I. Seligman helped finance
the westward path of the railroads and the building of the Panama Canal. The firm organized its first investment company in 1929 and began managing its first mutual fund in 1930. In 2008, J. &amp; W. Seligman &amp; Co. Incorporated was
acquired and Seligman Investments became an offering brand of RiverSource Investments, LLC. 
We are proud of the rich and distinctive history
of these firms, the strength and breadth of products and services they offer, and the combined cultures of pioneering spirit and forward thinking. Together we are committed to providing more for our shareholders than ever before. 
 
A singular focus on our shareholders.
First-class research and thought leadership.
A disciplined investment approach.
When you choose Columbia Management, you can be confident that we will take the time to understand your needs and help you and your financial advisor identify the solutions that are right for you. Because
at Columbia Management, we don’t consider ourselves successful unless you are. 
Sincerely, 


Dear Shareholder: 
The Columbia Management story began over 100 years ago, and today, we are one of the nation’s largest dedicated asset managers. The recent
acquisition by Ameriprise Financial, Inc. brings together the talents, resources and capabilities of Columbia Management with those of RiverSource Investments, Threadneedle (acquired by Ameriprise in 2003) and Seligman Investments (acquired by
Ameriprise in 2008) to build a best-in-class asset management business that we believe is truly greater than its parts. 
RiverSource
Investments traces its roots to 1894 when its then newly-founded predecessor, Investors Syndicate, offered a face-amount savings certificate that gave small investors the opportunity to build a safe and secure fund for retirement, education or other
special needs. A mutual fund pioneer, Investors Syndicate launched Investors Mutual Fund in 1940. In the decades that followed, its mutual fund products and services lineup grew to include a full spectrum of styles and specialties. More than 110
years later, RiverSource continues to be a trusted financial products leader. 
Threadneedle, a leader in global asset management and one of
Europe’s largest asset managers, offers sophisticated international experience from a dedicated U.K. management team. Headquartered in London, it is named for Threadneedle Street in the heart of the city’s financial district, where British
investors pioneered international and global investing. Threadneedle was acquired in 2003 and today operates as an affiliate of Columbia Management. 
Seligman Investments’ beginnings date back to the establishment of the investment firm J. &amp; W. Seligman &amp; Co. in 1864. In the years that followed, Seligman played a major role
in the geographical expansion and industrial development of the United States. In 1874, President Ulysses S. Grant named Seligman as fiscal agent for the U.S. Navy — an appointment that would last through World War I. Seligman helped finance
the westward path of the railroads and the building of the Panama Canal. The firm organized its first investment company in 1929 and began managing its first mutual fund in 1930. In 2008, J. &amp; W. Seligman &amp; Co. Incorporated was
acquired and Seligman Investments became an offering brand of RiverSource Investments, LLC. 
We are proud of the rich and distinctive history
of these firms, the strength and breadth of products and services they offer, and the combined cultures of pioneering spirit and forward thinking. Together we are committed to providing more for our shareholders than ever before. 
 
A singular focus on our shareholders.
First-class research and thought leadership.
A disciplined investment approach.
When you choose Columbia Management, you can be confident that we will take the time to understand your needs and help you and your financial advisor identify the solutions that are right for you. Because
at Columbia Management, we don’t consider ourselves successful unless you are. 
Sincerely, 


