Dear Investor:




To learn more about the capital markets, your investment, and the portfolio management strategies American Century Investments provides, we encourage you to review this shareholder report for the financial reporting period ended August




On the following pages, you will find investment performance and portfolio information, presented with the expert perspective and commentary of our portfolio management team. This report remains one of our most important vehicles for conveying the information you need about your investment performance, and about the market factors and strategies that affect fund returns. For additional information on the markets, we encourage you to visit the Insights &amp; News tab at our Web site, americancentury.com, for updates and further expert commentary.




The top of our Web sites home page also provides a link to Our Story, which, first and foremost, outlines our commitmentsince helping clients reach their financial goals. We believe strongly that we will only be successful when our clients are successful. Thats who we are.




Another important, unique facet of our story and who we are is Profits with a Purpose, which describes our bond with the Stowers Institute for Medical Research (SIMR). SIMR is a world-class biomedical organizationfounded by our company founder James E. Stowers, Jr. and his wife Virginiathat is dedicated to researching the causes, treatment, and prevention of gene-based diseases, including cancer. Through American Century Investments private ownership structure, more than of our profits support SIMR.




Mr. Stowers example of achieving financial success and using that platform to help humanity motivates our entire American Century Investments team. His story inspires us to help each of our clients achieve success. Thank you for sharing your financial journey with us.



Sincerely,

Dear Investor:




To learn more about the capital markets, your investment, and the portfolio management strategies American Century Investments provides, we encourage you to review this shareholder report for the financial reporting period ended August




On the following pages, you will find investment performance and portfolio information, presented with the expert perspective and commentary of our portfolio management team. This report remains one of our most important vehicles for conveying the information you need about your investment performance, and about the market factors and strategies that affect fund returns. For additional information on the markets, we encourage you to visit the Insights &amp; News tab at our Web site, americancentury.com, for updates and further expert commentary.




The top of our Web sites home page also provides a link to Our Story, which, first and foremost, outlines our commitmentsince helping clients reach their financial goals. We believe strongly that we will only be successful when our clients are successful. Thats who we are.




Another important, unique facet of our story and who we are is Profits with a Purpose, which describes our bond with the Stowers Institute for Medical Research (SIMR). SIMR is a world-class biomedical organizationfounded by our company founder James E. Stowers, Jr. and his wife Virginiathat is dedicated to researching the causes, treatment, and prevention of gene-based diseases, including cancer. Through American Century Investments private ownership structure, more than of our profits support SIMR.




Mr. Stowers example of achieving financial success and using that platform to help humanity motivates our entire American Century Investments team. His story inspires us to help each of our clients achieve success. Thank you for sharing your financial journey with us.




Sincerely,

Dear Investor:








To learn more about the capital markets, your investment, and the portfolio management strategies American Century Investments provides, we encourage you to review this shareholder report for the financial reporting period ended August




On the following pages, you will find investment performance and portfolio information, presented with the expert perspective and commentary of our portfolio management team. This report remains one of our most important vehicles for conveying the information you need about your investment performance, and about the market factors and strategies that affect fund returns. For additional information on the markets, we encourage you to visit the Insights &amp; News tab at our Web site, americancentury.com, for updates and further expert commentary.




The top of our Web sites home page also provides a link to Our Story, which, first and foremost, outlines our commitmentsince helping clients reach their financial goals. We believe strongly that we will only be successful when our clients are successful. Thats who we are.




Another important, unique facet of our story and who we are is Profits with a Purpose, which describes our bond with the Stowers Institute for Medical Research (SIMR). SIMR is a world-class biomedical organizationfounded by our company founder James E. Stowers, Jr. and his wife Virginiathat is dedicated to researching the causes, treatment, and prevention of gene-based diseases, including cancer. Through American Century Investments private ownership structure, more than of our profits support SIMR.




Mr. Stowers example of achieving financial success and using that platform to help humanity motivates our entire American Century Investments team. His story inspires us to help each of our clients achieve success. Thank you for sharing your financial journey with us.




Sincerely,

Dear Investor:




To learn more about the capital markets, your investment, and the portfolio management strategies American Century Investments provides, we encourage you to review this shareholder report for the financial reporting period ended August




On the following pages, you will find investment performance and portfolio information, presented with the expert perspective and commentary of our portfolio management team. This report remains one of our most important vehicles for conveying the information you need about your investment performance, and about the market factors and strategies that affect fund returns. For additional information on the markets, we encourage you to visit the Insights &amp; News tab at our Web site, americancentury.com, for updates and further expert commentary.




The top of our Web sites home page also provides a link to Our Story, which, first and foremost, outlines our commitmentsince helping clients reach their financial goals. We believe strongly that we will only be successful when our clients are successful. Thats who we are.




Another important, unique facet of our story and who we are is Profits with a Purpose, which describes our bond with the Stowers Institute for Medical Research (SIMR). SIMR is a world-class biomedical organizationfounded by our company founder James E. Stowers, Jr. and his wife Virginiathat is dedicated to researching the causes, treatment, and prevention of gene-based diseases, including cancer. Through American Century Investments private ownership structure, more than of our profits support SIMR.




Mr. Stowers example of achieving financial success and using that platform to help humanity motivates our entire American Century Investments team. His story inspires us to help each of our clients achieve success. Thank you for sharing your financial journey with us.




Sincerely,
