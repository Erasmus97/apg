Dear Shareholder:
 
Over the 12-month reporting period, the U.S. financial markets and economy continued to recover, in fits and starts, from the “Great
Recession.” As economic data vacillated between good and bad news on
employment, housing trends, business strength, and consumer confidence, market
volatility and investor sentiment also see-sawed.  
During the winter, investors became less risk averse, pouring
money into higher-yielding areas of the bond market as well as stocks, which
reached 18-month highs in March. Later in the spring, however, investor
sentiment took an abrupt turn as confidence in the   global economic recovery
waned and fears of a double-dip recession grew. Following a dismal August for
the stock market, September saw a surge in stock prices lifted by strong
corporate earnings reports and renewed investor interest in bargain-priced
stocks. In the bond market, Treasury yields moved lower over the 12-month
reporting period and corporate bonds generally performed well.
 
Economic Recovery Slow But on Track
Looking ahead, the pace of economic recovery has clearly
slowed, causing Federal Reserve (Fed) Chairman Ben Bernanke to say that the Fed
stands ready to use all of the tools at its disposal to reinvigorate the U.S. economy.  In our view, while the country faces sobering challenges related to the
unemployment rate, high levels of government debt, and the stumbling housing
market, we also see encouraging signs of economic recovery. Overall, companies
have strong balance sheets and cash positions, have reported
stronger-than-expected corporate earnings, and are investing in their
businesses. Consumers are generally “deleveraging” by saving more and paying
down their debt.  Financial reform is under way in the U.S. that may help reassure investors and stabilize the markets. Globally, central banks around the
world are continuing to pursue extremely accommodative monetary policies to
encourage economic recovery.
In this transitional environment, we believe that both the
equity and fixed-income markets are likely to continue to be somewhat volatile.
In our view, investment strategies that include sustainability criteria may be
better positioned to weather these uncertainties and provide long-term value.
 
Markets Challenged, But Gain Ground 
Despite the volatility over the course of the 12-month
reporting period, domestic and international stocks had moved solidly ahead by
the end of the period. U.S. stock indexes reported 12-month gains across all
styles, strategies, and capitalization ranges. The large-cap Russell 1000 Index
and the Standard &amp; Poor’s 500 Index returned 10.75% and 10.16%,
respectively. Mid-cap stocks were the top-performing category, with the Russell
Midcap Index up 17.54%, while the small-cap Russell 2000 Index rose 13.35%. In
terms of style, growth stocks moderately outpaced value stocks. On the
international front, the MSCI EAFE Investable Market Index (IMI), a benchmark
for international stocks, edged up 4.23%, and the MSCI Emerging Markets IMI was
up 21.97%. 
In the fixed-income markets, the Barclays Capital U.S. Credit
Index, a market barometer for investment-grade bonds, was up 11.67%. In line
with the Fed’s federal funds rate target of  0% to 0.25%, money market returns
remained very low.
 
The Gulf of Mexico Oil Spill and the Extractives Industry
In the wake of the April 20 oil spill in the Gulf of Mexico,
Americans have continued to grapple with the devastation caused by the spill
and its long-term environmental, societal, and economic implications. Calvert
shares the concern and the frustration felt by the millions of people affected
by this tragedy.
Following the spill, Calvert met with BP officials, urging BP
not only to clean up the current spill, but also to implement stronger safety
and process management standards for its contractors. We are also evaluating
how our advocacy objectives with deepwater oil-drilling companies may help
prevent such 
 
 
In terms of extraction methodologies, Calvert has long
recognized that as readily accessible supplies of oil and gas dry up, companies
may be forced to seek mineral resources in countries with poor governance, weak
rule of law, and high levels of corruption. Accordingly, over the past two
years, we have been a leading advocate for transparency requirements for
extractive industries. In July, the U.S. Congress passed legislation requiring
companies to disclose payments that they make to the U.S. or foreign
governments for the purpose of commercial development of oil, natural gas, or
minerals. We believe this legislation is a milestone toward helping advance
environmental sustainability in this industry. 
In our view, the oil spill also underlines the urgency for
expanded investment—with greater federal incentives—in alternative energy
sources.
 
Financial Reform Under Way
Looking ahead, long-awaited financial reform is under way with
Congressional passage of the largest financial reform bill since the Great
Depression. The Dodd-Frank Wall Street Reform and Consumer Protection Act
(Dodd-Frank Act) is designed to address inadequate regulation of Wall Street
firms and the type of unrestrained environment that contributed to the credit
crisis of 2008 and the ensuing global market meltdown. The Dodd-Frank Act seeks
to establish strong consumer protections, shield taxpayers from future
corporate bailouts, shine a light on the “shadow markets” and derivatives
trading, and expand the role of shareholders in corporate governance. While
these goals are laudable, the impact of the Dodd-Frank Act on the financial
industry—and ultimately its ability to prevent another financial crisis—must
stand the test of time.
 
As the Obama administration and Congress work to implement key
financial reforms, we believe that over time these efforts may work to redress
some systemic imbalances in the financial system and provide additional
stability to the economy and markets. 
 
Review Your Portfolio Allocations
In our view, the financial markets are likely to be in
transition for some time as the government tackles financial reform, the global
economy continues to recover, and political elections in the U.S. impact a variety of government policies. Now may be an opportune time to review your overall
investment strategy and portfolio allocations with your financial advisor.
Check to ensure that your target mix of U.S. and international stocks, bonds,
and cash is well-diversified and appropriate given your investment goals, stage
of life, and attitude toward risk.  


 
As always, we appreciate your investing with Calvert.  
 
 
Sincerely, 


