Dear Shareholder, 
We are pleased to provide the annual report of Legg Mason ClearBridge Small Cap Value Fund for the twelve-month reporting period ended September 30, 2010. Please read on for a detailed look
at prevailing economic and market conditions during the Fund’s reporting period and to learn how those conditions have affected Fund performance. 
As always, we remain committed to providing you with excellent service and a full spectrum of investment choices. We also remain committed to supplementing the support you receive from your
financial advisor. One way we accomplish this is through our website, www.leggmason.com/individualinvestors. Here you can gain immediate access to market and investment information, including: 
 
Fund prices and performance, 
 
Market insights and commentaries from our portfolio managers, and 
 
A host of educational resources. 
Special shareholder notice 
At the Board’s regular meeting held in
November 2010, Western Asset Management Company (“Western Asset”) was appointed as an additional subadviser of the Fund solely for cash management purposes. Western Asset personnel previously managed cash for the Fund through an
arrangement with Legg Mason Partners Fund Advisor, LLC, the Fund’s investment manager. 
We look forward to helping you meet
your financial goals. 
Sincerely, 


