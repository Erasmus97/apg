Dear Shareholder:

The period ended was a productive one for investors as the nations
financial markets and economy continued to recover from a deep recession and ensuing investment asset sell off in the fall of and early I will offer some comments in review of the period as well as outline what we believe to be key topics
in forming an outlook for A brief review on the economy
The U.S. economy grew slowly over the reporting period, but did post positive growth nonetheless. Year-over-year economic growth as
measured by GDP (gross domestic product) through the third quarter of came in at
While this level of growth is far from the robust economic recovery required to generate the myriad of new jobs needed, it shows strong improvement since the middle of
The U.S. employment picture continues to be the major area of concern and the slowest part of the overall economy to recover. The
monthly U.S. non-farm payrolls report, which gives an indication of the overall health of the nations employment, has been decidedly mixed in recent months. In nearly all of the monthly reports showed a net decrease in jobs, while in
a slight improvement took hold. Year-to-date through October, the economy had added only jobscertainly an improvement over but still much less than the or jobs per month required to loosen a national
unemployment rate stubbornly stuck at
Inflation continues to be muted with the October Consumer Price Index registering a slight year-over-year increase. With inflation largely in
check, the Federal Reserve Open Markets Committee may feel more comfortable leaving the benchmark federal funds target interest rate unchanged at its current The general consensus appears to be that we have enough inflation to provide
evidence that the overall economy is growing, albeit weakly, but not so little that we run deflationary risks. That said, low inflation and a
low overall interest rate environment can be viewed as a double-edged sword. Low interest rates make borrowing money, funding


business plans and servicing debt less costly and can lead to greater economic growth. The flip side, unfortunately, is that more conservative investors and savers get little in exchange for
placing assets in bank accounts, money markets and fixed-rate products. A brief review on the markets
Overall, it was quite a rewarding period for investors as many asset classes posted double-digit positive returns. Stock prices posted strong
returns to close out and opened in much the same way before falling back in May and June. Concerns mounted over faltering U.S. economic data and debt worries in the European Union. The rally resumed toward the late summer as another round
of strong corporate profit reports, teamed with improved economic news, generally propelled stocks forward through the end of the reporting period.
As is typical of a strong upward market, the most aggressive investment types generally performed best over the reporting period. Small company stocks, as measured by the Russell
&reg; Index, recorded a total return. Their large-company cousins, as measured by the S&amp;P Index, posted a
total return. Large-company stocks typically trail the more nimble small-cap stocks in sharp market rallies and tend to outperform in more modest market conditions later in economic recoveries. The value and growth styles of investing varied
somewhat over the period as investor interest in growth sectors such as information technology and consumer discretionary led to larger gains as measured by the Russell
&reg; Growth Index, which returned Weighed down in part by uncertainty over the prospects for banks and other financial companies, the value style underperformed as
illustrated by the Russell Value Indexs total return of over the reporting period.
Overseas equities posted weaker returns than their domestic counterparts over concerns with the financial health and ongoing debt issues of
several European Union countries and weaker economic conditions in Europe. The Morgan Stanley Capital International Europe, Australasia and Far East (MSCI EAFE) Index, a common benchmark for large, higher-quality

















international company stocks, posted a total return. Holding true to higher-risk financial assets performing the best, the MSCI Emerging Markets Index recorded a robust total return.
Emerging markets stocks were the beneficiaries of strong retail and institutional cash flows that raised demand and prices for the securities. Also, many of these emerging market countries are simply growing faster than our more mature economy and
may offer greater growth prospects along with greater potential risks. The bond markets also proved rewarding for investors. Benign inflation
levels and solid business conditions combined with periodic demand for the safety of U.S. Treasuries set the stage for good returns. The higher-quality Barclays Capital U.S Aggregate Bond Index, a common proxy for the U.S. fixed-income market,
registered an total return for the period ended Holding true to the more aggressive investments outperforming over the period, the Barclays Capital U.S. Corporate High Yield Bond Index returned Investor
desire for higher yields and higher return potential along with stronger company balance sheets benefited high-yield investments. Outlook
Though improvement in the economy has been at a slow pace, economic data has shown recent signs of strengthening. In October, the
Institute for Supply Management reported that its Manufacturing Index improved at a level above expectations. Additionally, some leading indicators data showed marked improvement in new orders, while existing inventories dropped. The service
industry, which accounts for close to of the U.S. economy, also has shown signs of improvement. This kind of evidence supports a pickup in both the manufacturing and service sides of our economy and could spell new jobs if the improvements take
a more lasting hold. The Federal Reserve appears to be willing to actively seek to foster more economic growth. The second round of
quantitative easing, or was designed to improve consumer and business confidence. While I am not entirely sure of the necessity of such a program, the Feds commitment to growth could prove a positive for many asset classes and investment
types, at least for the near term. I think confidence is the key to a more resurgent economy. Consumers need to feel better about their
prospects for keeping or finding a job. Homeowners need to feel better about their financial security when it comes to affording their current mortgage or seeing their home value continue to drop. Businesses need


more confidence that the economic recovery isnt a false start but a slow beginning to better days ahead in order to invest in new business lines, hire additional workers, and buy new
equipment or supplies. We see conditions continuing to improve, but at a relatively slow pace. With economic growth in the or range, it
will be difficult to see major improvements in the employment picture. That said, the current fundamental underpinnings are stronger than they were a year ago, and thats one thing to be thankful for.
In Conclusion Let me offer a few final
thoughts as it is nearing the end of the year, which is a great time to get your financial house in order. Its important to remember
that stock market or equity investments should be viewed with a longer time frame. Equity investments can provide growth prospects for the long haul while adding a meaningful hedge against inflation versus bonds and other fixed-rate investments.
Most investors need a meaningful allocation to stocks in their portfolio to generate the kind of growth todays retirees require in order to not outlive their savings. But they have to be comfortable with the amount of risk in their portfolio.
If fear of the stock market or fear of not having enough growth in your retirement portfolio keeps you sleepless at night, its time to
sit down with your Thrivent Financial representative. He or she can help you build a personal strategy that seeks to balance your appetite for risk with the goals you have for your money. Thrivent Financial has many tools and product solutions that
can help. Let us know if we can help in any way, and thank you for continuing to put your trust with us. Sincerely,
