Dear Shareholder: 
Economic conditions have improved markedly since our last report to you. As the economic recovery that began in the spring of 2009 gained slight momentum, investors began to demonstrate some appetite for
risk, recognizing the power of the government stimulus and other initiatives that had been put in place. Stocks and bonds began to rally, and some positive signs began to appear, including an upturn in housing and a recovery in demand for consumer
durable goods. At the start of the fiscal year, the end of the third calendar quarter of 2009, gross domestic product expanded at a 2.8 percent annualized rate. Progress continued as 2009 wound to a close. In the fourth calendar quarter of 2009,
gross domestic product expanded to 5.6 percent. 
The momentum that drove the equity markets higher in late 2009 continued into the first
calendar quarter of 2010, albeit at a markedly slower pace. Many economic indicators pointed to continued recovery and increasing stability in the U.S. economy, and investor confidence was further bolstered by stabilization of global credit markets,
stronger-than-expected first-quarter 2010 corporate profits and improvement in the auto and housing markets. This optimism abruptly changed with the emergence of the European debt crisis, which created fears of a double-dip recession. The European
banking system was negatively impacted when investors began to question the credit quality of several sovereign governments. That scenario, in conjunction with data reflecting slowing U.S. economic growth and escalating concern about China’s
ability to slow the pace of its economy, drove a sharp global correction. The tide seemed to turn in September, however, traditionally a negative month for equities, with slightly stronger economic data and optimism that mid-term elections may drive
more investor-friendly changes in Washington. Stocks staged a dramatic four-week rally that restored some optimism toward the end of the fiscal period, assisted perhaps by an announcement from the National Bureau of Economic Research that the
‘Great Recession,’ the longest-lasting since the Great Depression, officially ended in June of 2009. 
The economy grew at a 1.7
percent annual rate in the second calendar quarter of 2010, slowing precipitously from the 3.7 percent rate in the first three months of the calendar year. Preliminary numbers for third-quarter growth appear positive. The S&amp;P 500 Index posted a
12.1 percent gain for the 12 months ended September 30, 2010. Fixed Income markets, as measured by the Citigroup Broad Investment Grade Index, saw yields decline significantly.

Numerous imponderables remain, including persistent high unemployment and a housing sector that continues
to struggle, particularly so after government stimulus was removed. Nonetheless, we are optimistic that better days are ahead. Interest rates are low, government policy remains accommodative, and companies are reporting more robust activity.
Acquisition activity, a sign that companies are growing more willing to loosen the purse strings and funnel money into growing their operations, appears to be picking up. 
Economic Snapshot 
 
 
 
S&P 500 Index
MSCI EAFE Index
Citigroup Broad Investment Grade Index (annualized yield to maturity)
U.S. unemployment rate
30-year fixed mortgage rate
Oil price per barrel
Sources:
Bloomberg, U.S. Department of Labor 
All government statistics shown are subject to periodic revision. The S&amp;P 500 Index is an unmanaged index
that tracks the stocks of 500 primarily large-cap U.S. companies. MSCI EAFE Index is an unmanaged index comprised of securities that represent the securities markets in Europe, Australasia and the Far East. Citigroup Broad Investment Grade Index is
an unmanaged index comprised of securities that represent the bond market. Annualized yield to maturity is the rate of return anticipated on a bond if it is held until the maturity date. It is not possible to invest directly in any of these indexes.
Mortgage rates shown reflect the average rate on a conventional loan with a 60-day lender commitment. Oil prices reflect the market price of West Texas intermediate grade crude. 
As always, we thank you for your continued trust in Waddell & Reed, and encourage you to share in our optimism for the future. 
Respectfully, 


