Dear Shareholder:
We are pleased to provide you with this annual report on the investment strategies and performance of the portfolios in the Saratoga Advantage Trust (the “Trust”).  This report covers the twelve months from September 1, 2009 through August 31, 2010.
ECONOMIC OVERVIEW
SM
NEW ADVISOR
I am pleased to inform you that effective June 1, 2010 Milestone Capital Management, LLC (“Milestone”) is the new investment advisor of the Saratoga Advantage Trust’s U.S. Government Money Market Portfolio.  Founded in 1994, Milestone is a well respected investment manager advising approximately $1.5 billion in assets.  
COMPARING THE PORTFOLIOS’ PERFORMANCE TO BENCHMARKS
When reviewing the performance of the portfolios against their benchmarks, it is important to note that the Trust is designed to help investors to implement an asset allocation strategy to meet their individual needs as well as select individual investments within each asset category among the myriad of choices available.  Each Saratoga portfolio was formed to represent a single asset class, and each portfolio’s institutional money manager was selected based on their ability to manage money within that class.  Therefore, the Saratoga portfolios can help investors to properly implement their asset allocation decisions, and keep their investments within the risk parameters that they establish with their investment consultants. Without the intended asset class consistency of the Saratoga portfolios, even the most carefully crafted allocation strategy could be negated. Furthermore, the benchmarks do not necessarily provide precise standards by wh
ich to measure the portfolios against in that the characteristics of the benchmarks can vary widely at different points in time from the Saratoga portfolios (e.g., characteristics such as: average market capitalizations, price-to-earnings and price-to-book ratios, bond quality ratings and maturities, etc.).  In addition, the benchmarks can potentially have a survivor bias built into them (i.e., the performance of only funds that are still in existence may remain part of the benchmark’s performance while funds that do not exist anymore may be removed from the benchmark’s performance).  
ELECTRONIC DELIVERY AVAILABLE
AUTOMATED ACCOUNT UPDATES
Finally, following you will find specific information on the investment strategy and performance of each of the Trust’s portfolios.  Please speak with your financial advisor if you have any questions about your investment in the Saratoga Advantage Trust or your allocation of assets among the Trust’s portfolios.
We remain dedicated to serving your investment needs.  Thank you for investing with us.
Best wishes,

