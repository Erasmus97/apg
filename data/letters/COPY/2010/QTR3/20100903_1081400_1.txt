Dear Valued Shareholder,

We are pleased to provide you with this annual report for the WELLS FARGO
ADVANTAGE MUNICIPAL INCOME FUNDS that covers the period that ended June


During this period, fixed-income markets continued to show strong signs of
improvement, resulting in positive returns in nearly every sector. In general,
municipal securities performed relatively well over the recent period,
benefiting from growing economic activity, the low yields in U.S. Treasuries,
and improving fiscal conditions for state and local governments. Investors
continued to seek tax-exempt municipal bonds for their higher levels of relative
yield compared with U.S. Treasuries and their tax-exempt benefits.

GOVERNMENT INTERVENTIONS INSPIRED STRONG PERFORMANCE IN THE FIXED-INCOME
MARKETS.

In the early months of concerns over deflationary pressures were rampant.
Investors were still wary of market risks, deepening economic declines, and
wondered if government interventions would be effective in bolstering the
financial system. Whether economic conditions would improve in the near term was
not as important to the markets' performance as the basic understanding that
they would likely not get any worse. Extensive government policy
measures--including extraordinarily accommodative monetary policy in the first
half of investor confidence, which made several securities that
had been priced for the worst-case scenarios become attractively undervalued in
the assessment of many investors. As market sentiment improved leading into the
second half of the non-U.S. Treasury fixed income markets rallied through
the end of year. Thus, the recent period that ended June
began with a strong rally and increasing confidence across the fixed-income
markets.

The best-performing fixed-income markets over the past months were those that
offered a good level of income in the current low short-term rate and
extraordinarily steep yield-curve environment, most notably lower-quality and
longer maturities. In municipal bonds, the BBB-rated sub-index of the Barclays
Capital Municipal Bond Index returned during the period, while
the AAA-rated sub-index returned the least of the investment-grade municipal
credit tiers, with a return. Each respectively lower-quality credit tier
performed better than the one above it. By comparison, the Barclays Capital U.S.
Aggregate Bond Index and the broad-based Barclays Capital U.S. Treasury Index
returned and respectively, during the period.

By the end of investors appeared less concerned with the risks of a
possible deepening recession and more focused on the potential for increasing
interest rates. During the first six months of however, not only did
short-term interest rates not rise, but yields essentially remained unchanged,
or even fell, across the Treasury and municipal curves. The result was among the
steepest yield curve on record, certainly steeper than at any point during
The investor sentiment that strengthened towards the end of was tempered by



Wells Fargo Advantage Municipal Income Funds


Letter to Shareholders

global credit fears and signals from the Fed suggesting that short-term interest
rates would remain at the current historic lows until Moreover, renewed
concerns about possible bankruptcies and increased levels of default rates
affecting a few municipalities increased risk premiums across the municipal
marketplace. However, while it is true that many municipalities are still facing
large budget gaps and little or no revenue growth, most state and local
governments demonstrated that they will continue to take the necessary actions
to improve their situation. The overall results for the first half of were
strong returns for Treasuries and positive, but more realistic, returns for
municipals with lower-quality and longer maturities performing best.

AN EXCEPTIONALLY STEEP YIELD CURVE AND ATTRACTIVE VALUATIONS DEFINED THE
MUNICIPAL MARKET.

While U.S. Treasuries rallied to return for the recent six-month period,
Treasuries posted a return over the past months. Municipals, by
comparison, returned over the recent six-month period, and returned
over the past months.

Although the rally moved the fixed-income markets closer towards a state of
normalcy, several segments of the municipal market were still offering
above-average yields versus comparable Treasuries as we entered Those
segments of the municipal market continue to be defined by an unusually steep
yield curve and wide spreads to comparable Treasuries, which by late were
still above long-term averages.

Yield curves in the municipal market are some of the steepest on record; this
has rewarded investing further out the curve, with yields significantly
increasing as maturities lengthen. In addition, medium- and lower-quality
investment-grade municipal securities continue to offer unusually generous yield
spreads versus Treasuries. During the last three months, strong demand for U.S.
government debt pushed Treasury yields down further than the decline in
municipal yields. As a result, municipal valuations became even more attractive
relative to Treasuries as ratios of municipal bond yields to equivalent
Treasuries widened across most maturities.

ACTIVE MANAGEMENT AND CREDIT ANALYSIS WILL DRIVE FUND CONSTRUCTION AND
PERFORMANCE.

In this environment, we believe that fundamental credit analysis, issue
selection, and yield-curve positioning will be the key drivers of portfolio
construction and relative performance. At WELLS FARGO ADVANTAGE FUNDS(R), we
intend to continue assessing relative value opportunities throughout the
municipal bond markets and across our lineup of municipal funds.

We believe that interest rates will rise, and we believe the WELLS FARGO
ADVANTAGE MUNICIPAL INCOME FUNDS are positioned to perform well whether the
yield curve remains unchanged or flattens, and whether interest rates stay
stable or move higher, through bonds that offer higher income and benefit from
the presently steep yield curve.

YIELD CURVES IN THE MUNICIPAL MARKET ARE SOME OF THE STEEPEST ON RECORD; THIS
HAS REWARDED INVESTING FURTHER OUT THE CURVE, WITH YIELDS SIGNIFICANTLY
INCREASING AS MATURITIES LENGTHEN. IN ADDITION, MEDIUM- AND LOWER-QUALITY
INVESTMENT-GRADE MUNICIPAL SECURITIES CONTINUE TO OFFER UNUSUALLY GENEROUS YIELD
SPREADS VERSUS TREASURIES.



Wells Fargo Advantage Municipal Income Funds


Letter to Shareholders

Thank you for choosing WELLS FARGO ADVANTAGE Funds. We appreciate your
confidence in us. Through each market cycle, we are committed to helping you
meet your financial needs. If you have any questions about your investment,
please contact your investment professional, or call us at You
may also want to visit our Web site at www.wellsfargo.com/advantagefunds.

Sincerely,

Dear Valued Shareholder,

We are pleased to provide you with this annual report for the WELLS FARGO
ADVANTAGE MUNICIPAL INCOME FUNDS that covers the period that ended June


During this period, fixed-income markets continued to show strong signs of
improvement, resulting in positive returns in nearly every sector. In general,
municipal securities performed relatively well over the recent period,
benefiting from growing economic activity, the low yields in U.S. Treasuries,
and improving fiscal conditions for state and local governments. Investors
continued to seek tax-exempt municipal bonds for their higher levels of relative
yield compared with U.S. Treasuries and their tax-exempt benefits.

GOVERNMENT INTERVENTIONS INSPIRED STRONG PERFORMANCE IN THE FIXED-INCOME
MARKETS.

In the early months of concerns over deflationary pressures were rampant.
Investors were still wary of market risks, deepening economic declines, and
wondered if government interventions would be effective in bolstering the
financial system. Whether economic conditions would improve in the near term was
not as important to the markets' performance as the basic understanding that
they would likely not get any worse. Extensive government policy
measures--including extraordinarily accommodative monetary policy in the first
half of investor confidence, which made several securities that
had been priced for the worst-case scenarios become attractively undervalued in
the assessment of many investors. As market sentiment improved leading into the
second half of the non-U.S. Treasury fixed income markets rallied through
the end of year. Thus, the recent period that ended June
began with a strong rally and increasing confidence across the fixed-income
markets.

The best-performing fixed-income markets over the past months were those that
offered a good level of income in the current low short-term rate and
extraordinarily steep yield-curve environment, most notably lower-quality and
longer maturities. In municipal bonds, the BBB-rated sub-index of the Barclays
Capital Municipal Bond Index returned during the period, while
the AAA-rated sub-index returned the least of the investment-grade municipal
credit tiers, with a return. Each respectively lower-quality credit tier
performed better than the one above it. By comparison, the Barclays Capital U.S.
Aggregate Bond Index and the broad-based Barclays Capital U.S. Treasury Index
returned and respectively, during the period.

By the end of investors appeared less concerned with the risks of a
possible deepening recession and more focused on the potential for increasing
interest rates. During the first six months of however, not only did
short-term interest rates not rise, but yields essentially remained unchanged,
or even fell, across the Treasury and municipal curves. The result was among the
steepest yield curve on record, certainly steeper than at any point during
The investor sentiment that strengthened towards the end of was



Wells Fargo Advantage Municipal Income Funds


Letter to Shareholders

tempered by global credit fears and signals from the Fed suggesting that
short-term interest rates would remain at the current historic lows until
Moreover, renewed concerns about possible bankruptcies and increased levels of
default rates affecting a few municipalities increased risk premiums across the
municipal marketplace. However, while it is true that many municipalities are
still facing large budget gaps and little or no revenue growth, most state and
local governments demonstrated that they will continue to take the necessary
actions to improve their situation. The overall results for the first half of
were strong returns for Treasuries and positive, but more realistic,
returns for municipals with lower-quality and longer maturities performing best.

AN EXCEPTIONALLY STEEP YIELD CURVE AND ATTRACTIVE VALUATIONS DEFINED THE
MUNICIPAL MARKET.

While U.S. Treasuries rallied to return for the recent six-month period,
Treasuries posted a return over the past months. Municipals, by
comparison, returned over the recent six-month period, and returned
over the past months.

Although the rally moved the fixed-income markets closer towards a state of
normalcy, several segments of the municipal market were still offering
above-average yields versus comparable Treasuries as we entered Those
segments of the municipal market continue to be defined by an unusually steep
yield curve and wide spreads to comparable Treasuries, which by late were
still above long-term averages.

Yield curves in the municipal market are some of the steepest on record; this
has rewarded investing further out the curve, with yields significantly
increasing as maturities lengthen. In addition, medium- and lower-quality
investment-grade municipal securities continue to offer unusually generous yield
spreads versus Treasuries. During the last three months, strong demand for U.S.
government debt pushed Treasury yields down further than the decline in
municipal yields. As a result, municipal valuations became even more attractive
relative to Treasuries as ratios of municipal bond yields to equivalent
Treasuries widened across most maturities.

ACTIVE MANAGEMENT AND CREDIT ANALYSIS WILL DRIVE FUND CONSTRUCTION AND
PERFORMANCE.

In this environment, we believe that fundamental credit analysis, issue
selection, and yield-curve positioning will be the key drivers of portfolio
construction and relative performance. At WELLS FARGO ADVANTAGE FUNDS(R), we
intend to continue assessing relative value opportunities throughout the
municipal bond markets and across our lineup of municipal funds.

We believe that interest rates will rise, and we believe the WELLS FARGO
ADVANTAGE MUNICIPAL INCOME FUNDS are positioned to perform well whether the
yield curve remains unchanged or flattens, and whether interest rates stay
stable or move higher, through bonds that offer higher income and benefit from
the presently steep yield curve.

YIELD CURVES IN THE MUNICIPAL MARKET ARE SOME OF THE STEEPEST ON RECORD; THIS
HAS REWARDED INVESTING FURTHER OUT THE CURVE, WITH YIELDS SIGNIFICANTLY
INCREASING AS MATURITIES LENGTHEN. IN ADDITION, MEDIUM- AND LOWER-QUALITY
INVESTMENT-GRADE MUNICIPAL SECURITIES CONTINUE TO OFFER UNUSUALLY GENEROUS YIELD
SPREADS VERSUS TREASURIES.



Wells Fargo Advantage Municipal Income Funds


Letter to Shareholders

Thank you for choosing WELLS FARGO ADVANTAGE FUNDS. We appreciate your
confidence in us. Through each market cycle, we are committed to helping you
meet your financial needs. If you have any questions about your investment,
please contact your investment professional, or call us at You
may also want to visit our Web site at www.wellsfargo.com/advantagefunds.


Sincerely,
