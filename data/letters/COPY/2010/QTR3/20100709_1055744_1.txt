Dear Fellow Shareholders: 
Thank you for investing in The Henssler Equity Fund (“The Fund”). The Fund’s return for the fiscal
year ended April 30, 2010 was up 37.98% versus the Standard &amp; Poor’s 500 Index (“S&amp;P 500”) being up 38.84% for the same time
period.
The returns shown above do not reflect the deduction of taxes a shareholder would pay on fund distributions or
redemption of fund shares. The performance data quoted represents past performance. Past performance cannot guarantee future results, and current performance may be lower or higher than the performance quoted. Both the return from and the principal
value of an investment in The Fund will fluctuate so that an investor’s shares, when redeemed, may be worth more or less than their original cost. To obtain performance as of the most recent-month end, please contact 1-800936-3863.

If you would like to obtain periodic information regarding The Fund, we encourage you to visit our website at www.henssler.com. You may
review The Henssler Equity Fund link as often as you like, as we update information regularly. Through our website, we provide details each quarter on our top 10 holdings, industry allocation and other statistics. The website also provides media
appearance information and links to articles featuring commentary and insight from The Fund’s management team. 
The
Henssler Equity Fund is more than 11 years old since we started on June 10, 1998. On behalf of our board of directors and management team, we thank many of you who invested in The Fund from its very beginning. We will continue to focus our best
efforts exclusively on the one fund we manage, The Henssler Equity Fund. 
Yours very truly, 
 
Gene W. Henssler, Ph.D.
Theodore L. Parrish, CFA*
Co-Manager
  Co-Manager
 
 
This report is intended for shareholders of The Fund. It may not be distributed to prospective investors unless it is preceded or
accompanied by the current fund prospectus that contains important information including risks, investment objectives, charges and expenses. The Fund’s prospectus is available, without charge, upon request by calling toll-free 1-800-936-3863.
Please read and consider this information carefully before you invest or send money. 
 
The S&amp;P 500 Index by Standard &amp; Poor’s Corp. is an unmanaged, capitalization-weighted index comprising of 500 issues listed on
various exchanges, representing the performance of the stock market generally. Please note that indices do not take into account any fees and expenses of investing in the individual securities that they track, and individuals cannot invest directly
in any index. 
 
The Fund’s expense ratio, which is net of expenses paid directly and indirectly and as reported in The Fund’s prospectus dated
August 28, 2009, is 1.29%. The Fund bears a pro rata share of the fees and expenses of the other funds in which The Fund invests (“Acquired Funds”). The operating expenses in the prospectus may not correlate to the expense ratio in
The Fund’s financial statements (or the financial highlights of this prospectus) because the financial statements include only the direct operating expenses incurred by The Fund, not the indirect costs of investing in the Acquired Funds.

 
 
 
HE
Table of Contents
