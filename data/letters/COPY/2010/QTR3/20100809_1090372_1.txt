Dear Fellow Investors,






Since we began managing the Wisdom Fund last December, we have
continued the Funds style of investing, which is to own
companies that in our view display sustainable competitive
advantages that should allow them to generate above average
returns on capital over long periods of time. By acquiring the
shares of these companies at reasonable prices, we feel that any
potential financial rewards to our shareholders should reflect
their strong economic performance.





Over short periods of time, the performance of the Fund will
often diverge from the benchmark, both up and down. It is only
over longer periods of three years or more that our value
approach to investing is designed to help generate superior
returns for our shareholders.





In the period ending we have seen increased signs
that the economic recovery is indeed in place, albeit more muted
than might be hoped. Consumer spending has not been as strong as
we would like, constrained as it is by high unemployment and low
housing prices. Meanwhile, the banking sector is still
struggling, meaning that lending will likely continue to be
dampened over the next six to





That said, even in this anemic environment, there are strong
companies to be found. Among non-financial companies, many have
healthy balance sheets, high cash levels and strong earnings.
Furthermore, we believe that growth rates in the second half of
this year may be a little stronger than many analysts expect,
though next years growth may be a little weaker. The
reason for this is the widely held expectation that taxes will
go up significantly next year. In anticipation, we believe that
many corporations, small businesses and individuals will take
every opportunity to realize as much income as possible in
rather than This should give growth rates a bit of an
artificial boost.





Our outlook on the stock market is optimistic, for the simple
reason that stock valuations are currently low enough that much
bad news already appears to be built in. With the SP
hovering a little over the index is selling at just
times expected earnings, and at less than times
earnings. Thats well below historically normal
price-to-earnings
ratios of around At these levels, the market appears to have
essentially already priced in the worst-case scenario of a
double-dip recession: if we get any growth, however modest,
stocks should be propelled higher.





Since December of last year, we have made some significant
changes to the portfolio. We have trimmed the number of stocks
to from Of that original number, had values that were
less than one percent of the Funds portfolio and many of
these were so small as to be inconsequential to future results.
We feel that to individual holdings are sufficient to give
the necessary diversification to the Funds portfolio. At
the same time, such a number allows us to concentrate a bit more
on our most favored stocks, which we believe should result in
attractive performance over time.





With many banks still struggling to strengthen their balance
sheets, and with the recent passage of the financial reform
bill, we thought it prudent to eliminate all the domestic bank
stocks from the Funds portfolio. Consequently, we sold off
holdings in MT Bank, SunTrust, U.S.Bancorp and Wells
Fargo. In addition, we sold off all four of the Exchange Traded
Mutual Funds (ETFs) that had been in the Funds portfolio,
including two bond funds, a contra-stock fund and a gold fund,
none of which we felt were in line with our primary















investing strategy. Lastly, our top three holdingsAmerican
Express, Proctor Gamble, and
Coca-Colanow
make up a more conservative of the Funds portfolio,
down from an over-weighted





Since December weve added stocks to the Funds
portfolio. In particular, we are upbeat on the Federal Agency
mortgage-backed real estate investment trusts (REITs), and we
have been building substantial positions in three
companiesAnnaly Capital Management, Anworth Mortgage Asset
Corp and MFA Financial. Together, these three now account for
of the Funds portfolio. At the time of purchase, each
was selling at or below estimated book value, and we think they
have the potential to earn around to on their book
value in the coming year.





In closing, we feel confident that the stocks we now own have
significant competitive advantages that give them intrinsic or
franchise value greater than what they currently sell for in the
marketplace. This should help them to perform well regardless of
what happens with the general economic environment. Thank you
for entrusting us with your investments. We look forward to a
bright future together.





Francis Alexander



Portfolio Manager
