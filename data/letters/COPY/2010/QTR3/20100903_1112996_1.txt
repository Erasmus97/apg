Dear Shareholders:


Im pleased to present this report on your Funds performance for the ended
Whether youre a long-time Invesco client or a shareholder who joined us as a result of
our June acquisition of Morgan Stanleys retail asset management business, including Van
Kampen Investments, Im glad youre part of the Invesco family.



At Invesco, were committed to providing you with timely information about market
conditions, answering questions you may have about your investments and offering outstanding
customer service. At our website, invesco.com/us, you can obtain unique market perspectives,
useful investor education information and your Funds most recent quarterly commentary.


Near the end of this letter, Ive provided the number to call if you have specific
questions about your account; Ive also provided my email address so you can send a general
Invesco-related question or comment to me directly.


The benefits of Invesco


As a leading global investment manager, Invesco is committed to helping investors worldwide achieve
their financial objectives. I believe Invesco is uniquely positioned to serve your needs.



First, we are committed to investment excellence. We believe the best investment insights come
from specialized investment teams with discrete investment perspectives, each operating under a
disciplined philosophy and process with strong risk oversight and quality controls. This approach
enables our portfolio managers, analysts and researchers to pursue consistent results across market
cycles.


Second, we offer you a broad range of investment products that can be tailored to your needs
and goals.
In addition to traditional mutual funds, we manage a variety of other investment products.
These products include single-country, regional and global investment options spanning major
equity, fixed income and alternative asset classes.


And third, we have just one focus: investment management. At Invesco, we believe that focus
brings success, and thats why investment management is all we do. We direct all of our
intellectual capital and global resources toward helping investors achieve their long-term
financial objectives.


Your financial adviser can also help you as you pursue your financial goals. Your financial
adviser is familiar with your individual goals and risk tolerance, and can answer questions about
changing market conditions and your changing investment needs.


Our customer focus


Short-term market conditions can change from time to time, sometimes suddenly and sometimes
dramatically. But regardless of market trends, our commitment to putting you first, helping you
achieve your financial objectives and providing you with excellent customer service will not
change.



If you have questions about your account, please contact one of our client services
representatives at If you have a general Invesco-related question or comment for me,
please email me directly at phil@invesco.com.


I want to thank our existing Invesco clients for placing your faith in us. And I want to
welcome our new Invesco clients: We look forward to serving your needs in the years ahead. Thank
you for investing with us.


Sincerely,

Dear Fellow Shareholders:


Although the global markets have improved since their lows of
they remain challenging as governments around the world work to ensure
the recovery remains on track. In this volatile environment, its
comforting to know that your Board is committed to putting your
interests first. We realize you have many choices when selecting a
money manager, and your Board is working hard to ensure you feel youve
made the right choice.



To that end, Im pleased to share the news that Invesco has completed its acquisition of
Morgan Stanleys retail asset management business, including Van Kampen Investments. This
acquisition greatly expands the breadth and depth of investment strategies we can offer you. As a
result of this combination, Invesco gained investment talent for a number of investment strategies,
including U.S. value equity, U.S. small cap growth equity, tax-free municipals, bank loans and
others. Another key advantage of this combination is the highly complementary nature of our
cultures. This is making it much easier to bring our organizations together while ensuring that our
investment teams remain focused on managing your money.


We view this addition as an excellent opportunity for you, our shareholders, to have access to
an even
broader range of well-diversified mutual funds. Now that the acquisition has closed, Invesco
is working to bring the full value of the combined organization to shareholders. The key goals of
this effort are to ensure that we have deeply resourced and focused investment teams, a compelling
line of products and enhanced efficiency, which will benefit our shareholders now and over the long
term.


It might interest you to know that the mutual funds of the combined organization are overseen
by a single fund Board composed of current members, including four new members who joined us
from Van Kampen/Morgan Stanley. This expanded Board will continue to oversee the funds with the
same strong sense of responsibility for your money and your continued trust that we have always
maintained.


As always, you are welcome to contact me at bruce@brucecrockett.com with any questions or
concerns you may have. We look forward to representing you and serving your interests.


Sincerely,
