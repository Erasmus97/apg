DEAR FELLOW SHAREHOLDERS,

The Jensen Portfolio -- Class J Shares --
returned for the year ended May compared to a return of
for the Standard &amp; Poors Index over this period. Please see pages
through of this report for complete standardized performance information for
the Portfolio.

Stock selection added value in the Consumer
Staples and Health Care sectors, while detracting from returns in the Consumer
Discretionary sector. From a sector perspective, our overweighting in the
Industrials and Information Technology sectors added value, although our
underweighting in the Consumer Discretionary sector and overweighting in the
Health Care sector detracted from returns. In addition, our lack of
representation in the Energy, Telecom Services and Utilities sectors benefited
us as these sectors saw much more modest returns than the benchmark as a whole.
These sectors produce virtually no companies with the long-term record of
consistent business performance that Jensen requires, measured by consecutive
years of or greater Return on Equity as determined by the Investment
Adviser.

Market Perspective

The S&amp;P Index has certainly rebounded
since the low of March Since that low, which was near the end of our
last fiscal year, the S&amp;P has risen from to -- or nearly --
as of May Yet the index is still down from its previous
peak.

What is interesting to note about the market
rebound is that it has once again been led by lower quality companies which
have, in general, significantly outperformed higher quality companies. One way
to view this is to utilize the quality rankings from Standard &amp; Poors,
which are a measure of the consistency of companies earnings and dividends
growth. Companies with an A+ ranking are considered to be higher quality. These
companies returned slightly more than underperforming against the S&amp;P
Indexs return of nearly for the year ending May Lower
quality companies, those ranked B, B- and C, outperformed the S&amp;P Index,
in some cases significantly, over the same period.

We have also seen continuing volatility in the
markets likely due to the uncertainties that cloud the global economy.These
include the strength and durability of the recovery, future inflation and fears
over sovereign debt in Europe and elsewhere.

Domestically, the U.S. unemployment rate still
exceeds the housing market remains sluggish, business activity continues to
be weak and consumers remain cautious about spending. Credit remains difficult
to obtain despite low interest rates as lenders worry about default risk. All of
these factors are weighing on investors as they consider returning to the
markets.

The Effect at Jensen

Despite ongoing volatility, or perhaps because
of it, we continue to find what we believe are attractive opportunities across
the portfolio. That is particularly true for companies that are positioned for
growth from overseas operations, especially in emerging
markets.

Our top contributors
to performance for the fiscal year were Cognizant Technology
Solutions, Waters and Emerson Electric. While these
companies are in different sectors, their combination of strong future business
prospects, technological prowess, innovation-focused strategies and attractive
market prices made these businesses compelling. Each of them represents a sector
that we believe is well positioned for future growth.

Significant detractors for the fiscal year
included C.H. Robinson
Worldwide and C.R.
Bard, our two newest additions to the portfolio. (Please see details
below under Portfolio Additions and Eliminations) However, we believe that
their short-term performance is not representative of the long-term opportunity
that these companies represent.

Also detracting from the Portfolios
performance were Abbott
Labs and Praxair. Health care companies
have lagged given the uncertainty surrounding domestic reform legislation. In
addition, the slowing economy had impacted Praxairs business. However, both
companies have been positioning themselves to take advantage of economic
recovery, particularly in emerging markets. We have been adding to these
positions given their compelling opportunities ahead and the attractive
valuations we believe they offer.

Portfolio Additions and
Eliminations

C.R. Bard was added to the Portfolio during
the year ending May Bard designs and markets diverse medical devices,
with a core focus on catheters, primarily to hospitals. Bard also sells to
healthcare professionals at extended healthcare and alternate site facilities.
Over of Bards sales come from products in which it holds the number one or
number two market position, making market leadership a competitive advantage.
Bard serves as a key collaborative partner with physicians in product
innovation, creating products that incrementally improve outcomes and lower the
cost of care. Bard markets its products to physicians based on efficacy and to
administrators based on cost reduction. We believe this strategy should allow
Bard to successfully navigate healthcare reform.



















the Jensen Portfolio







We were also pleased to
add C.H. Robinson, a leading aggregator in the shipping market, providing
freight transportation using contracted trucks, trains, ships and airplanes.
C.H. Robinson does not own any transportation assets outright. Unencumbered by
shipping assets, the company does not have to maximize utilization. It can meet
customers needs by finding the best balance of service and cost while managing
its own returns. C.H. Robinson serves clients via a national branch network
larger than any competitors.

Both Danaher Corporation and Ametek Inc. were sold from the Portfolio during the early
months of as each failed to meet Jensens minimum Return on Equity (ROE) of
in calendar Its not yet clear whether the prolonged recession or
other business factors contributed to this decline. A consecutive
business performance record of or greater ROE is required for inclusion in
The Jensen Portfolio. This hurdle serves as an indicator of a business
competitive advantages and indicates returns in excess of the companys capital
costs. Historically, we have found that when the ROE falls below it likely
signals an erosion of its competitive edge. Our sale of these companies allowed
us to acquire compelling alternative businesses, as detailed
earlier.

The Jensen Outlook

Jensens investment
process emphasizes building portfolios one quality business at a time. Yet
themes have developed in what we believe are pockets of
opportunity.

The Health Care
sector offers companies with compelling growth potential and attractive
valuations. The passage of overhaul legislation produced a benign reaction by
most portfolio companies. Reaction has remained muted despite positive business
performance in the last quarter or two. Post-legislation pricing pressures may
impact some companies. But additional people in the healthcare system with
greater access to coverage and the resulting higher sales volumes all stand as
positives. Additional clarity around aspects of legislation remain a concern.
But the long-term prospects continue to encourage us in our convictions for this
sector. We are also starting to see cash flow utilized toward strategic
acquisitions which should fuel additional growth.

The build-out of
global infrastructure should benefit holdings in the Industrial sector. Results
during the last quarter showed relatively robust growth for industrial
companies. We see potential in the backlog of unfilled orders and resumed
capital spending as the economic recovery takes hold. We believe that many of
these market leading companies are positioned to gain business in faster growing
regions of the world. Sporting leaner operations after rightsizing during the
downturn, sustained growth should benefit margins and cash flow generation
handsomely.

Deferred spending,
fueled by the upgrade cycle and a demand by corporations for greater
efficiencies, should positively impact Technology holdings. Leading companies in
this sector have both strong business models and balance sheets. Their
substantial cash hordes make merger and acquisition activity likely, as mature
businesses seek added sources of growth at attractive valuations.

Consumer Staple
companies, particularly those selling goods impacting our everyday lives, remain
favored in our portfolio. Many of these companies are investing in emerging
market opportunities and results thus far have been fairly positive. Because
these companies are defensive and less economically sensitive, their stocks have
been out of favor for the extended low quality market rally. Nonetheless, we
believe the cash flows that these market leaders generate remain a powerful tool
for creating additional shareholder value.

The Jensen
Portfolios companies benefit from a balance of revenues from inside and outside
the U.S. Growth in the U.S. may be slow and the economic recovery thus far has
been choppy. The challenges suppressing the recovery appear stubborn and will
likely persist for the near term. But the strong free cash flows from the
domestic operations of companies are expected to be effectively deployed to
faster growing economies around the world, as well as toward acquisitions, or
returned to shareholders as dividends. We believe these quality businesses are
positioned well for the next stage of the economic recovery, however it plays
out.

We invite you to seek
additional information on The Jensen Portfolio at www.jenseninvestment.com where
additional content, including updated holdings and performance, is available. We
take our investment responsibilities seriously and appreciate the trust you have
placed in us. As always, we welcome your feedback.

Sincerely,
