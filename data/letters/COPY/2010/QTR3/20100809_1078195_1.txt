Dear Shareholder:



On behalf of the Board of Trustees of The Community Reinvestment Act Qualified Investment Fund, I am pleased to present the Annual Report to Shareholders for the year ended May



Once again, the Fund demonstrated consistent financial performance and continued success in identifying and purchasing securities that finance economic and community development activities throughout the nation. In fact, during the past fiscal year, the Fund surpassed billion invested in targeted, community development securities.



The impact of the Funds investments can be seen today in all states - in cities, towns, and neighborhoods - that have benefited from funding made possible by the securities purchased by the Fund.



We applaud the disciplined and productive efforts of Community Capital Management, Inc., registered investment advisor to the Fund, and we thank you, our shareholders, for your investments. We appreciate your continued confidence.



Sincerely,
