Dear PIMCO Funds
Shareholder: 
 
What a difference a year makes. In March 2009,
financial markets generally reached their lows in response to one of the most severe systemic market crises since the Great Depression. Over the past twelve months, however, significant government intervention focused on unprecedented fiscal
stimulus and near zero short-term interest rates helped to stabilize financial markets. As a result, risk sectors such as equity and high-yield credit benefited and rebounded in a methodic rally throughout the year. 
 
In this environment, and broadly across all potential market environments, we
look for ways to position our clients’ portfolios to take advantage of opportunities, yet helping protect against larger risks and uncertainties. By designing and implementing global investment solutions for our clients, we can guide them
through the investment journey. This becomes increasingly important as we look to meet the challenges associated with unwinding the significant excess monetary reserves created through quantitative easing policies and government asset purchase
programs in the U.S., a rising sovereign debt burden, and the evolving reality of a desynchronized global economic recovery. 
 
®
 
Included below are highlights of the financial markets during our twelve-month
fiscal reporting period: 
 
The Federal Reserve maintained a target range for the Federal Funds Rate of 0.00% to 0.25% and the Bank of England kept its key-lending rate at 0.50%,
while the European Central Bank reduced its overnight rate by 0.25% to 1.00%. The Bank of Japan maintained its lending rate at 0.10%. 
 
Returns on corporate bonds, mortgage-backed securities (“MBS”), and asset-backed securities (“ABS”) were positive as investors moved
into higher yielding, riskier asset classes. The benchmark ten-year U.S. Treasury note yielded 3.83% at the end of the reporting period, or 1.12% higher than at March 31, 2009. The Barclays Capital U.S. Aggregate Index, a widely used index of
U.S. high-grade bonds, returned 7.69% for the reporting period. 
 
Corporate bonds, especially high-yield bonds, were among the best performing fixed-income asset classes during the reporting period. Credit premiums
continued to tighten, prompting strong new issuance in both the investment-grade and high-yield corporate bond markets. Improving corporate balance sheets among the largest corporations, as compared to a deteriorating U.S. government balance sheet,
encouraged investors to prefer corporate bonds over U.S. Treasury securities. 
 
Agency MBS performed better than comparable U.S. Treasury securities due in part to the success of the Federal Reserve’s MBS Purchase Program, which
absorbed $1.25 trillion of Agency MBS since the program’s inception. Non-Agency MBS also performed well as a lack of new issuance over the last two years and anticipated demand from the U.S. Government’s Public-Private Investment Program
caused prices to move higher. In the ABS market, the U.S. Government’s Term Asset- Backed Securities Loan Facility (“TALF”) was successful in inducing investor demand for high-quality consumer ABS. 
 
Table of Contents
 
Municipal bonds outperformed comparable U.S. Treasury securities as investors moved into higher yielding asset classes. The strong performance within the
municipal bond sector was led by consistent demand from investors and lower tax-exempt supply due in large part to the Build America Bond program. The Barclays Capital Municipal Bond Index returned 9.69% for the reporting period.

 
U.S. Treasury Inflation-Protected Securities (“TIPS”) outperformed their nominal U.S. Treasury counterparts as breakeven inflation levels (or
the difference between nominal and real yields) moved higher. Real yields declined while nominal yields generally rose as markets stabilized with signs of an economic recovery and the potential for an increase in inflation becoming more apparent.
The Barclays Capital U.S. TIPS Index returned 6.18% for the reporting period. In addition, commodities index returns were positive, as represented by the Dow Jones-UBS Commodity Index Total Return, which returned 20.53% for the reporting period.

 
Emerging market (“EM”) bonds denominated in both U.S. dollars and local EM currencies performed well during the reporting period. Increasing
concern over sovereign debt levels towards the latter part of the reporting period, namely in Greece and in some other developed countries, caused EM credit premiums to grow, but ultimately benefited EM returns given the generally stronger initial
conditions and growth outlook of EM countries. 
 
Equity markets worldwide trended higher as investors returned due to the low value of certain equities and the peak in the liquidation cycle in early
March 2009. U.S. equities, as measured by the S&amp;P 500 Index, returned 49.77% and international equities, as represented by the MSCI World Index, returned 52.37% for the reporting period. 
 
On the following pages of this PIMCO Funds Annual Report, please find specific
details as to each Fund’s total return investment performance and a discussion of those factors that most affected performance. 
 
Thank you for the trust you have placed in us. We value your trust and will continue to work diligently to meet your broad investment needs. If you have
questions regarding any of your PIMCO Funds investments, please contact your account manager, or call one of our shareholder associates at 1-866-746-2606. We also invite you to visit our website at www.pimco-funds.com or our investment
manager’s website at www.pimco.com. 
 
Sincerely, 


Dear PIMCO Funds
Shareholder: 
 
What a difference a year makes. In March 2009,
financial markets generally reached their lows in response to one of the most severe systemic market crises since the Great Depression. Over the past twelve months, however, significant government intervention focused on unprecedented fiscal
stimulus and near zero short-term interest rates helped to stabilize financial markets. As a result, risk sectors such as equity and high-yield credit benefited and rebounded in a methodic rally throughout the year. 
 
In this environment, and broadly across all potential market environments, we
look for ways to position our clients’ portfolios to take advantage of opportunities, yet helping protect against larger risks and uncertainties. By designing and implementing global investment solutions for our clients, we can guide them
through the investment journey. This becomes increasingly important as we look to meet the challenges associated with unwinding the significant excess monetary reserves created through quantitative easing policies and government asset purchase
programs in the U.S., a rising sovereign debt burden, and the evolving reality of a desynchronized global economic recovery. 
 
®
 
Included below are highlights of the financial markets during our twelve-month
fiscal reporting period: 
 
The Federal Reserve maintained a target range for the Federal Funds Rate of 0.00% to 0.25% and the Bank of England kept its key-lending rate at 0.50%,
while the European Central Bank reduced its overnight rate by 0.25% to 1.00%. The Bank of Japan maintained its lending rate at 0.10%. 
 
Returns on corporate bonds, mortgage-backed securities (“MBS”), and asset-backed securities (“ABS”) were positive as investors moved
into higher yielding, riskier asset classes. The benchmark ten-year U.S. Treasury note yielded 3.83% at the end of the reporting period, or 1.12% higher than at March 31, 2009. The Barclays Capital U.S. Aggregate Index, a widely used index of
U.S. high-grade bonds, returned 7.69% for the reporting period. 
 
Corporate bonds, especially high-yield bonds, were among the best performing fixed-income asset classes during the reporting period. Credit premiums
continued to tighten, prompting strong new issuance in both the investment-grade and high-yield corporate bond markets. Improving corporate balance sheets among the largest corporations, as compared to a deteriorating U.S. government balance sheet,
encouraged investors to prefer corporate bonds over U.S. Treasury securities. 
 
Agency MBS performed better than comparable U.S. Treasury securities due in part to the success of the Federal Reserve’s MBS Purchase Program, which
absorbed $1.25 trillion of Agency MBS since the program’s inception. Non-Agency MBS also performed well as a lack of new issuance over the last two years and anticipated demand from the U.S. Government’s Public-Private Investment Program
caused prices to move higher. In the ABS market, the U.S. Government’s Term Asset- Backed Securities Loan Facility (“TALF”) was successful in inducing investor demand for high-quality consumer ABS. 
 
Table of Contents
 
Municipal bonds outperformed comparable U.S. Treasury securities as investors moved into higher yielding asset classes. The strong performance within the
municipal bond sector was led by consistent demand from investors and lower tax-exempt supply due in large part to the Build America Bond program. The Barclays Capital Municipal Bond Index returned 9.69% for the reporting period.

 
U.S. Treasury Inflation-Protected Securities (“TIPS”) outperformed their nominal U.S. Treasury counterparts as breakeven inflation levels (or
the difference between nominal and real yields) moved higher. Real yields declined while nominal yields generally rose as markets stabilized with signs of an economic recovery and the potential for an increase in inflation becoming more apparent.
The Barclays Capital U.S. TIPS Index returned 6.18% for the reporting period. In addition, commodities index returns were positive, as represented by the Dow Jones-UBS Commodity Index Total Return, which returned 20.53% for the reporting period.

 
Emerging market (“EM”) bonds denominated in both U.S. dollars and local EM currencies performed well during the reporting period. Increasing
concern over sovereign debt levels towards the latter part of the reporting period, namely in Greece and in some other developed countries, caused EM credit premiums to grow, but ultimately benefited EM returns given the generally stronger initial
conditions and growth outlook of EM countries. 
 
Equity markets worldwide trended higher as investors returned due to the low value of certain equities and the peak in the liquidation cycle in early
March 2009. U.S. equities, as measured by the S&amp;P 500 Index, returned 49.77% and international equities, as represented by the MSCI World Index, returned 52.37% for the reporting period. 
 
On the following pages of this PIMCO Funds Annual Report, please find specific
details as to each Fund’s total return investment performance and a discussion of those factors that most affected performance. 
 
Thank you for the trust you have placed in us. We value your trust and will continue to work diligently to meet your broad investment needs. If you have
questions regarding any of your PIMCO Funds investments, please contact your account manager, or call one of our shareholder associates at 1-866-746-2606. We also invite you to visit our website at www.pimco-funds.com or our investment
manager’s website at www.pimco.com. 
 
Sincerely, 


Dear PIMCO Funds Shareholder: 
 

What a difference a year makes. In March 2009, financial markets generally reached their lows in response to one of the most severe systemic
market crises since the Great Depression. Over the past twelve months, however, significant government intervention focused on unprecedented fiscal stimulus and near zero short-term interest rates helped to stabilize financial markets. As a result,
risk sectors such as equity and high-yield credit benefited and rebounded in a methodic rally throughout the year. 
 
In this environment, and broadly across all potential market environments, we look for ways to position our clients’ portfolios to take advantage of
opportunities, yet helping protect against larger risks and uncertainties. By designing and implementing global investment solutions for our clients, we can guide them through the investment journey. This becomes increasingly important as we look to
meet the challenges associated with unwinding the significant excess monetary reserves created through quantitative easing policies and government asset purchase programs in the U.S., a rising sovereign debt burden, and the evolving reality of a
desynchronized global economic recovery. 
 
®
 
Included below are highlights of the financial markets during our
twelve-month fiscal reporting period: 
 
The Federal Reserve maintained a target range for the Federal Funds Rate of 0.00% to 0.25% and the Bank of England kept its key-lending rate at 0.50%,
while the European Central Bank reduced its overnight rate by 0.25% to 1.00%. The Bank of Japan maintained its lending rate at 0.10%. 
 
Returns on corporate bonds, mortgage-backed securities (“MBS”), and asset-backed securities (“ABS”) were positive as investors moved
into higher yielding, riskier asset classes. The benchmark ten-year U.S. Treasury note yielded 3.83% at the end of the reporting period, or 1.12% higher than at March 31, 2009. The Barclays Capital U.S. Aggregate Index, a widely used index of
U.S. high-grade bonds, returned 7.69% for the reporting period. 
 
Corporate bonds, especially high-yield bonds, were among the best performing fixed-income asset classes during the reporting period. Credit premiums
continued to tighten, prompting strong new issuance in both the investment-grade and high-yield corporate bond markets. Improving corporate balance sheets among the largest corporations, as compared to a deteriorating U.S. government balance sheet,
encouraged investors to prefer corporate bonds over U.S. Treasury securities. 
 
Agency MBS performed better than comparable U.S. Treasury securities due in part to the success of the Federal Reserve’s MBS Purchase Program, which
absorbed $1.25 trillion of Agency MBS since the program’s inception. Non-Agency MBS also performed well as a lack of new issuance over the last two years and anticipated demand from the U.S. Government’s Public-Private Investment Program
caused prices to move higher. In the ABS market, the U.S. Government’s Term Asset-Backed Securities Loan Facility (“TALF”) was successful in inducing investor demand for high-quality consumer ABS. 
 
Municipal bonds outperformed comparable U.S. Treasury securities as investors moved into higher yielding asset classes. The strong performance within the

 
Table of Contents
 
 

municipal bond sector was led by consistent demand from investors and lower tax-exempt supply due in large part to the Build America Bond program. The Barclays Capital Municipal Bond Index
returned 9.69% for the reporting period. 
 
U.S. Treasury Inflation-Protected Securities (“TIPS”) outperformed their nominal U.S. Treasury counterparts as breakeven inflation levels (or
the difference between nominal and real yields) moved higher. Real yields declined while nominal yields generally rose as markets stabilized with signs of an economic recovery and the potential for an increase in inflation becoming more apparent.
The Barclays Capital U.S. TIPS Index returned 6.18% for the reporting period. In addition, commodities index returns were positive, as represented by the Dow Jones-UBS Commodity Index Total Return, which returned 20.53% for the reporting period.

 
Emerging market (“EM”) bonds denominated in both U.S. dollars and local EM currencies performed well during the reporting period. Increasing
concern over sovereign debt levels towards the latter part of the reporting period, namely in Greece and in some other developed countries, caused EM credit premiums to grow, but ultimately benefited EM returns given the generally stronger initial
conditions and growth outlook of EM countries. 
 
Equity markets worldwide trended higher as investors returned due to the low value of certain equities and the peak in the liquidation cycle in early
March 2009. U.S. equities, as measured by the S&amp;P 500 Index, returned 49.77% and international equities, as represented by the MSCI World Index, returned 52.37% for the reporting period. 
 
On the following pages of this PIMCO Funds Annual Report, please find specific
details as to each Fund’s total return investment performance and a discussion of those factors that most affected performance. 
 
Thank you for the trust you have placed in us. We value your trust and will continue to work diligently to meet your broad investment needs. If you have
questions regarding any of your PIMCO Funds investments, please contact your financial advisor, or call Allianz Global Investors at 1-800-426-0107. We also invite you to visit www.allianzinvestors.com. 
 
Sincerely, 


Dear PIMCO Funds Shareholder:

 
What a difference a year makes. In March 2009, financial markets
generally reached their lows in response to one of the most severe systemic market crises since the Great Depression. Over the past twelve months, however, significant government intervention focused on unprecedented fiscal stimulus and near zero
short-term interest rates helped to stabilize financial markets. As a result, risk sectors such as equity and high-yield credit benefited and rebounded in a methodic rally throughout the year. 
 
In this environment, and broadly across all potential market environments, we
look for ways to position our clients’ portfolios to take advantage of opportunities, yet helping protect against larger risks and uncertainties. By designing and implementing global investment solutions for our clients, we can guide them
through the investment journey. This becomes increasingly important as we look to meet the challenges associated with unwinding the significant excess monetary reserves created through quantitative easing policies and government asset purchase
programs in the U.S., a rising sovereign debt burden, and the evolving reality of a desynchronized global economic recovery. 
 
®
 
Included below are highlights of the financial markets during our twelve-month
fiscal reporting period: 
 
The Federal Reserve maintained a target range for the Federal Funds Rate of 0.00% to 0.25% and the Bank of England kept its key-lending rate at 0.50%,
while the European Central Bank reduced its overnight rate by 0.25% to 1.00%. The Bank of Japan maintained its lending rate at 0.10%. 
 
Returns on corporate bonds, mortgage-backed securities (“MBS”), and asset-backed securities (“ABS”) were positive as investors moved
into higher yielding, riskier asset classes. The benchmark ten-year U.S. Treasury note yielded 3.83% at the end of the reporting period, or 1.12% higher than at March 31, 2009. The Barclays Capital U.S. Aggregate Index, a widely used index of
U.S. high-grade bonds, returned 7.69% for the reporting period. 
 
Corporate bonds, especially high-yield bonds, were among the best performing fixed-income asset classes during the reporting period. Credit premiums
continued to tighten, prompting strong new issuance in both the investment-grade and high-yield corporate bond markets. Improving corporate balance sheets among the largest corporations, as compared to a deteriorating U.S. government balance sheet,
encouraged investors to prefer corporate bonds over U.S. Treasury securities. 
 
Agency MBS performed better than comparable U.S. Treasury securities due in part to the success of the Federal Reserve’s MBS Purchase Program, which
absorbed $1.25 trillion of Agency MBS since the program’s inception. Non-Agency MBS also performed well as a lack of new issuance over the last two years and anticipated demand from the U.S. Government’s Public-Private Investment Program
caused prices to move higher. In the ABS market, the U.S. Government’s Term Asset-Backed Securities Loan Facility (“TALF”) was successful in inducing investor demand for high-quality consumer ABS. 
 
Municipal bonds outperformed comparable U.S. Treasury securities as investors moved into higher yielding asset classes. The strong performance within the
municipal bond sector was led by consistent demand from investors and lower tax-exempt supply due in large part to the Build America Bond program. The Barclays Capital Municipal Bond Index returned 9.69% for the reporting period.

 
Table of Contents
 
U.S. Treasury Inflation-Protected Securities (“TIPS”) outperformed their nominal U.S. Treasury counterparts as breakeven inflation levels (or
the difference between nominal and real yields) moved higher. Real yields declined while nominal yields generally rose as markets stabilized with signs of an economic recovery and the potential for an increase in inflation becoming more apparent.
The Barclays Capital U.S. TIPS Index returned 6.18% for the reporting period. In addition, commodities index returns were positive, as represented by the Dow Jones-UBS Commodity Index Total Return, which returned 20.53% for the reporting period.

 
Emerging market (“EM”) bonds denominated in both U.S. dollars and local EM currencies performed well during the reporting period. Increasing
concern over sovereign debt levels towards the latter part of the reporting period, namely in Greece and in some other developed countries, caused EM credit premiums to grow, but ultimately benefited EM returns given the generally stronger initial
conditions and growth outlook of EM countries. 
 
Equity markets worldwide trended higher as investors returned due to the low value of certain equities and the peak in the liquidation cycle in early
March 2009. U.S. equities, as measured by the S&amp;P 500 Index, returned 49.77% and international equities, as represented by the MSCI World Index, returned 52.37% for the reporting period. 
 
On the following pages of this PIMCO Funds Annual Report, please find specific
details as to each Fund’s total return investment performance and a discussion of those factors that most affected performance. 
 
Thank you for the trust you have placed in us. We value your trust and will continue to work diligently to meet your broad investment needs. If you have
questions regarding any of your PIMCO Funds investments, please contact your financial advisor, or call Allianz Global Investors at 1-800-426-0107. We also invite you to visit www.allianzinvestors.com. 
 
Sincerely, 


Dear PIMCO Funds
Shareholder: 
 
What a difference a year makes. In
March 2009, financial markets generally reached their lows in response to one of the most severe systemic market crises since the Great Depression. Over the past twelve months, however, significant government intervention focused on unprecedented
fiscal stimulus and near zero short-term interest rates helped to stabilize financial markets. As a result, risk sectors such as equity and high-yield credit benefited and rebounded in a methodic rally throughout the year. 
 
In this environment, and broadly across all potential market
environments, we look for ways to position our clients’ portfolios to take advantage of opportunities, yet helping protect against larger risks and uncertainties. By designing and implementing global investment solutions for our clients, we can
guide them through the investment journey. This becomes increasingly important as we look to meet the challenges associated with unwinding the significant excess monetary reserves created through quantitative easing policies and government asset
purchase programs in the U.S., a rising sovereign debt burden, and the evolving reality of a desynchronized global economic recovery. 
 
®
 
Included below are highlights of the financial markets during
our twelve-month fiscal reporting period: 
 
The Federal Reserve maintained a target range for the Federal Funds Rate of 0.00% to 0.25% and the Bank of England kept its key-lending
rate at 0.50%, while the European Central Bank reduced its overnight rate by 0.25% to 1.00%. The Bank of Japan maintained its lending rate at 0.10%. 
 

Returns on corporate bonds, mortgage-backed securities (“MBS”), and asset-backed securities (“ABS”) were positive as
investors moved into higher yielding, riskier asset classes. The benchmark ten-year U.S. Treasury note yielded 3.83% at the end of the reporting period, or 1.12% higher than at March 31, 2009. The Barclays Capital U.S. Aggregate Index, a widely
used index of U.S. high-grade bonds, returned 7.69% for the reporting period. 
 
Corporate bonds, especially high-yield bonds, were among the best performing fixed-income asset classes during the reporting period.
Credit premiums continued to tighten, prompting strong new issuance in both the investment-grade and high-yield corporate bond markets. Improving corporate balance sheets among the largest corporations, as compared to a deteriorating U.S. government
balance sheet, encouraged investors to prefer corporate bonds over U.S. Treasury securities. 
 
Agency MBS performed better than comparable U.S. Treasury securities due in part to the success of the Federal Reserve’s MBS Purchase
Program, which absorbed $1.25 trillion of Agency MBS since the program’s inception. Non-Agency MBS also performed well as a lack of new issuance over the last two years and anticipated demand from the U.S. Government’s Public-Private
Investment Program caused prices to move higher. In the ABS market, the U.S. Government’s Term Asset-Backed Securities Loan Facility (“TALF”) was successful in inducing investor demand for high-quality consumer ABS.

 
Table of Contents
 
Municipal bonds outperformed comparable U.S. Treasury securities as investors moved into higher yielding asset classes. The strong
performance within the municipal bond sector was led by consistent demand from investors and lower tax-exempt supply due in large part to the Build America Bond program. The Barclays Capital Municipal Bond Index returned 9.69% for the reporting
period. 
 
U.S. Treasury Inflation-Protected Securities (“TIPS”) outperformed their nominal U.S. Treasury counterparts as breakeven
inflation levels (or the difference between nominal and real yields) moved higher. Real yields declined while nominal yields generally rose as markets stabilized with signs of an economic recovery and the potential for an increase in inflation
becoming more apparent. The Barclays Capital U.S. TIPS Index returned 6.18% for the reporting period. In addition, commodities index returns were positive, as represented by the Dow Jones-UBS Commodity Index Total Return, which returned 20.53% for
the reporting period. 
 
Emerging market (“EM”) bonds denominated in both U.S. dollars and local EM currencies performed well during the reporting
period. Increasing concern over sovereign debt levels towards the latter part of the reporting period, namely in Greece and in some other developed countries, caused EM credit premiums to grow, but ultimately benefited EM returns given the generally
stronger initial conditions and growth outlook of EM countries. 
 
Equity markets worldwide trended higher as investors returned due to the low value of certain equities and the peak in the liquidation
cycle in early March 2009. U.S. equities, as measured by the S&amp;P 500 Index, returned 49.77% and international equities, as represented by the MSCI World Index, returned 52.37% for the reporting period. 
 
On the following pages of this Annual Report for the Private
Account Portfolio Series (the “Portfolios”), the separate portfolios of the PIMCO Funds, please find specific details as to each Portfolio’s total return investment performance and a discussion of those factors that most affected
performance. 
 
Thank you for the trust you have
placed in us. We value your trust and will continue to work diligently to meet your broad investment needs. If you have questions regarding any of your PIMCO Funds investments, please contact your account manager, or call one of our shareholder
associates at 1-866-746-2606. We also invite you to visit our website at www.pimco-funds.com or our investment manager’s website at www.pimco.com. 
 
Sincerely, 


