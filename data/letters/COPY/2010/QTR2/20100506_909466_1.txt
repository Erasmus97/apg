Dear Shareholders:


Always unpredictable, equity markets have been highly volatile in recent years. So far in
markets have been somewhat choppy reacting to short-term news without establishing a clear,
long-term direction.



Like the future direction of equity markets, the health of the U.S. economy was also open to
debate. By the U.S. economy had ended its year-long contraction and had enjoyed
two quarters of healthy expansion. While most indicators suggested recovery was slowly taking hold,
the unemployment rate remained high by historical standards and the durability of the recovery
and the speed with which unemployment might normalize was uncertain.

Increased communication


Unpredictable and volatile markets, together with economic uncertainty, caused many of you to seek
information relative to your investments in recent months. Some of you contacted your financial
advisers to ask questions and obtain guidance. Others visited our website, invescoaim.com, where we
offer timely market commentary, investor education information and sector updates. In particular, I
recommend the Investment Perspectives articles featured on our home page; they are written by
Invesco Aims investment professionals and cover a wide range of topics that are updated regularly.



Also at invescoaim.com, you can access your Funds latest quarterly commentary. Simply click on
Mutual Funds inside the Financial Products box. Then, in the Fund Information box, click on
Quarterly Commentary and select your Fund.


Timely information like that available at our website together with the advice and guidance of
a trusted financial adviser can be especially important in uncertain times. Market volatility and
economic uncertainty are two factors that can prompt investors to abandon their long-term saving
and investment plans. A financial adviser can show you just how costly that could be over the long
term and can explain that saving more and investing more regularly is a time-tested way to build
a solid portfolio. He or she can help you identify appropriate investments, given your individual
risk tolerance, time horizon and investment goals.


Fund names and our corporate name are changing


In the months ahead, youll begin to see signs, small and large, of changes to our fund and
corporate names. For example, the name Aim will no longer be part of our branding, so our logo
graphic will be Invesco rather than Invesco Aim and your AIM fund soon will be renamed an
Invesco fund. (For example, AIM Charter Fund will become Invesco Charter Fund.) And effective April
our Web address will change from invescoaim.com to invesco.com. These changes are the next
steps in our rebranding process begun two years
ago, when for our corporate name we added Invesco in front of Aim. The marketplace now widely
recognizes that Aim is part of Invesco, and thus its time for us to formally acknowledge that
connection.



While market conditions change from time to time, our commitment to putting our clients first,
helping you achieve your financial goals and providing excellent customer service will not.


If you have questions about your account, please contact one of our client services representatives
at If you have a question or comment for me, please email me at phil@invesco.com.


Thank you for investing with us.


Sincerely,

Dear Fellow Shareholders:


By all accounts, was a challenging year for all of us. Although the economy and financial
markets whipsawed us through much of last year, the final months of the decade concluded with many
of us feeling somewhat more optimistic about as we began to see the markets and economy
evidence the first green shoots of recovery.



Perhaps the most valuable takeaway from is the manner in which it underscored the importance
of adopting the long-term, appropriately diversified investment strategy Ive mentioned in my
previous letters. If anything, last year was the litmus test for this approach.


Please be assured that your Board continues to oversee the AIM Funds with a strong sense of
responsibility for your savings and a deep appreciation for your trust. We have already begun the
annual review and management contract renewal process and will continue to seek to manage costs and
reward performance in ways that put your interests first. (It might also interest you to know that
the Board currently has five committees Compliance, Audit, Governance, Investments, and Valuation
Distribution and Proxy Voting whose members exercise oversight to maintain the AIM Funds
Investor First orientation.)


To that end, some of you may have seen it reported in
that Invesco will assume the management of the Van Kampen family of mutual funds as well as the
Morgan Stanley retail funds. The closing will be later this year and we view this addition as an
excellent opportunity to provide you, our shareholders, access to an even broader range of
well-diversified mutual funds under the Invesco umbrella. Ill keep you updated on the work were
doing to deliver the value of this acquisition to our shareholders in my upcoming letters.


As always, you are welcome to contact me at bruce@brucecrockett.com with any questions or concerns
you may have. We look forward to representing you and serving you in the coming year.


Sincerely,

Dear Shareholders:


Always unpredictable, equity markets have been highly volatile in recent years. So far in
markets have been somewhat choppy reacting to short-term news without establishing
a clear, long-term direction.



Like the future direction of equity markets, the health of the U.S. economy was also open
to debate. By the U.S. economy had ended its year-long contraction and had
enjoyed two quarters of healthy expansion. While most indicators suggested recovery was slowly
taking hold, the unemployment rate remained high by historical standards and the durability
of the recovery and the speed with which unemployment might normalize was uncertain.


Increased communication



Unpredictable and volatile markets, together with economic uncertainty, caused many of you to
seek information relative to your investments in recent months. Some of you contacted your
financial advisers to ask questions and obtain guidance. Others visited our website,
invescoaim.com, where we offer timely market commentary, investor education information and sector
updates. In particular, I recommend the Investment Perspectives articles featured on our home page;
they are written by Invesco Aims investment professionals and cover a wide range of topics that
are updated regularly.


Also at invescoaim.com, you can access your Funds latest quarterly commentary. Simply click
on Mutual Funds inside the Financial Products box. Then, in the Fund Information box, click on
Quarterly Commentary and select your Fund.


Timely information like that available at our website together with the advice and
guidance of a trusted financial adviser can be especially important in uncertain times. Market
volatility and economic uncertainty are two factors that can prompt investors to abandon their
long-term saving and investment plans. A financial adviser can show you just how costly that could
be over the long term and can explain that saving more and investing more regularly is a
time-tested way to build a solid portfolio. He or she can help you identify appropriate
investments, given your individual risk tolerance, time horizon and investment goals.


Fund names and our corporate name are changing


In the months ahead, youll begin to see signs, small and large, of changes to our fund and
corporate names. For example, the name Aim will no longer be part of our branding, so our logo
graphic will be Invesco rather than Invesco Aim and your AIM fund soon will be renamed an
Invesco fund. (For example, AIM Charter Fund will become Invesco Charter Fund.) And effective April
our Web address will change from invescoaim.com to invesco.com. These changes are the next
steps in our rebranding process begun two years ago, when for our corporate name we added Invesco
in front of Aim. The marketplace now widely recognizes that Aim is part of Invesco, and thus its
time for us to formally acknowledge that connection.



While market conditions change from time to time, our commitment to putting our clients first,
helping you achieve your financial goals and providing excellent customer service will not.


If you have questions about your account, please contact one of our client services
representatives at If you have a question or comment for me, please email me at
phil@invesco.com.








Thank you for investing with us.





Sincerely,

Dear Fellow Shareholders:


By all accounts, was a challenging year for all of us. Although the economy and
financial markets whipsawed us through much of last year, the final months of the decade
concluded with many of us feeling somewhat more optimistic about as we began to see the
markets and economy evidence the first green shoots of recovery.



Perhaps the most valuable takeaway from is the manner in which it underscored the
importance of adopting the long-term, appropriately diversified investment strategy Ive
mentioned in my previous letters. If anything, last year was the litmus test for this
approach.


Please be assured that your Board continues to oversee the AIM Funds with a strong sense
of responsibility for your savings and a deep appreciation for your trust. We have already
begun the annual review and management contract renewal process and will continue to seek to
manage costs and reward performance in ways that put your interests first. (It might also
interest you to know that the Board currently has five committees Compliance, Audit,
Governance, Investments, and Valuation Distribution and Proxy Voting whose members exercise
oversight to maintain the AIM Funds Investor First orientation.)


To that end, some of you may have seen it reported in that Invesco will
assume the management of the Van Kampen family of mutual funds as well as the Morgan Stanley
retail funds. The closing will be later this year and we view this addition as an excellent
opportunity to provide you, our shareholders, access to an even broader range of
well-diversified mutual funds under the Invesco umbrella. Ill keep you updated on the work
were doing to deliver the value of this acquisition to our shareholders in my upcoming
letters.


As always, you are welcome to contact me at bruce@brucecrockett.com with any questions or
concerns you may have. We look forward to representing you and serving you in the coming year.


Sincerely,

Dear Shareholders:


Always unpredictable, equity markets have been highly volatile in recent years. So far in
markets have been somewhat choppy reacting to short-term news without establishing a
clear, long-term direction.



Like the future direction of equity markets, the health of the U.S. economy was also open
to debate. By the U.S. economy had ended its year-long contraction and had
enjoyed two quarters of healthy expansion. While most indicators suggested recovery was slowly
taking hold, the unemployment rate remained high by historical standards and the durability
of the recovery and the speed with which unemployment might normalize was uncertain.


Increased communication



Unpredictable and volatile markets, together with economic uncertainty, caused many of you to
seek information relative to your investments in recent months. Some of you contacted your
financial advisers to ask questions and obtain guidance. Others visited our website,
invescoaim.com, where we offer timely market commentary, investor education information and sector
updates. In particular, I recommend the Investment Perspectives articles featured on our home page;
they are written by Invesco Aims investment professionals and cover a wide range of topics that
are updated regularly.


Also at invescoaim.com, you can access your Funds latest quarterly commentary. Simply click
on Mutual Funds inside the Financial Products box. Then, in the Fund Information box, click on
Quarterly Commentary and select your Fund.


Timely information like that available at our website together with the advice and
guidance of a trusted financial adviser can be especially important in uncertain times. Market
volatility and economic uncertainty are two factors that can prompt investors to abandon their
long-term saving and investment plans. A financial adviser can show you just how costly that could
be over the long term and can explain that saving more and investing more regularly is a
time-tested way to build a solid portfolio. He or she can help you identify appropriate
investments, given your individual risk tolerance, time horizon and investment goals.


Fund names and our corporate name are changing


In the months ahead, youll begin to see signs, small and large, of changes to our fund and
corporate names. For example, the name Aim will no longer be part of our branding, so our logo
graphic will be Invesco rather than Invesco Aim and your AIM fund soon will be renamed an
Invesco fund. (For example, AIM Charter Fund will become Invesco Charter Fund.) And effective April
our Web address will change from invescoaim.com to invesco.com. These changes are the next
steps in our rebranding process begun two years ago, when for our corporate name we added Invesco
in front of Aim. The marketplace now widely recognizes that Aim is part of Invesco, and thus its
time for us to formally acknowledge that connection.



While market conditions change from time to time, our commitment to putting our clients first,
helping you achieve your financial goals and providing excellent customer service will not.


If you have questions about your account, please contact one of our client services
representatives at If you have a question or comment for me, please email me at
phil@invesco.com.








Thank you for investing with us.




Sincerely,

Dear Fellow Shareholders:


By all accounts, was a challenging year for all of us. Although the economy and
financial markets whipsawed us through much of last year, the final months of the decade
concluded with many of us feeling somewhat more optimistic about as we began to see the
markets and economy evidence the first green shoots of recovery.



Perhaps the most valuable takeaway from is the manner in which it underscored the
importance of adopting the long-term, appropriately diversified investment strategy Ive
mentioned in my previous letters. If anything, last year was the litmus test for this approach.


Please be assured that your Board continues to oversee the AIM Funds with a strong sense of
responsibility for your savings and a deep appreciation for your trust. We have already begun
the annual review and management contract renewal process and will continue to seek to manage costs
and reward performance in ways that put your interests first. (It might also interest you to know
that the Board currently has five committees Compliance, Audit, Governance, Investments, and
Valuation Distribution and Proxy Voting whose members exercise oversight to maintain the AIM
Funds Investor First orientation.)


To that end, some of you may have seen it reported in that Invesco will assume
the management of the Van Kampen family of mutual funds as well as the Morgan Stanley retail funds.
The closing will be later this year and we view this addition as an excellent opportunity to
provide you, our shareholders, access to an even broader range of well-diversified mutual funds
under the Invesco umbrella. Ill keep you updated on the work were doing to deliver the value of
this acquisition to our shareholders in my upcoming letters.


As always, you are welcome to contact me at bruce@brucecrockett.com with any questions or
concerns you may have. We look forward to representing you and serving you in the coming year.

Sincerely,
