Dear Shareholders:









We are pleased to share with you the Pacific Life Funds Annual Report dated





Pacific Life Funds is comprised of separate funds (each individually a fund and
collectively, the funds). Pacific Life Fund Advisors LLC (PLFA), as adviser to the funds,
supervises the management of all of the funds and manages six of the funds directly. PLFA also does
business under the name Pacific Asset Management and manages the PL Money Market Fund under that
name. For the other funds, Pacific Life Funds and PLFA have retained other firms to serve as
portfolio managers under PLFAs supervision. The funds and the portfolio managers as of
are listed below:










Portfolio Manager

Fund





Pacific Life Fund Advisors LLC (PLFA)


PL Portfolio Optimization Conservative Fund
PL Portfolio Optimization Moderate-Conservative Fund
PL Portfolio Optimization Moderate Fund
PL Portfolio Optimization Moderate-Aggressive Fund
PL Portfolio Optimization Aggressive Fund







Pacific Asset Management


PL Money Market Fund







Fred Alger Management, Inc. (Alger)


PL Small-Cap Growth Fund







AllianceBernstein L.P. (AllianceBernstein)


PL International Value Fund







ClearBridge Advisors, LLC (ClearBridge)


PL Large-Cap Value Fund







Goldman Sachs Asset Management, L.P. (Goldman Sachs)


PL Short Duration Bond Fund







Highland Capital Management, L.P. (Highland Capital)


PL Floating Rate Loan Fund







Janus Capital Management LLC (Janus)


PL Growth LT Fund







Lazard Asset Management LLC (Lazard)


PL Mid-Cap Equity Fund







MFS Investment Management (MFS)


PL International Large-Cap Fund







NFJ Investment Group LLC (NFJ)


PL Small-Cap Value Fund







OppenheimerFunds, Inc. (Oppenheimer)


PL Main Street Core Fund
PL Emerging Markets Fund







Pacific Investment Management Company LLC (PIMCO)


PL Managed Bond Fund
PL Inflation Managed Fund







UBS Global Asset Management (Americas), Inc. (UBS)


PL Large-Cap Growth Fund







Van Kampen


PL Comstock Fund
PL Mid-Cap Growth Fund
PL Real Estate Fund






We appreciate your confidence in the Pacific Life Funds and look forward to serving your
financial needs in the years to come.

Sincerely,
