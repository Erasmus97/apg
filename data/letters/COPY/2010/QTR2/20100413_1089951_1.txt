Dear Shareholder: 
Our foremost goal at Managers Investment Group (“MIG”) is to structure and manage mutual funds that will help our shareholders and clients
successfully reach their investment goals and objectives. 
Each of our Funds is geared to provide you with exposure to a specific asset class
or style of investing. Investors tend to use our Funds as part of their broader portfolio in order to tailor their asset allocation to meet their individual needs. Most of our Funds, like those detailed in this report, are therefore designed to be
building blocks. 
At MIG, we have overall responsibility for the investment management and administration of the Funds. As a “manager of
managers,” we work with external investment managers that make the day-to-day investment decisions in the Funds (the “Portfolio Managers”). We devote considerable resources to our disciplined process of identifying and selecting
Portfolio Managers for the Funds. As a manager of managers, MIG performs many activities to monitor the ongoing investment, compliance, and administrative aspects of all of the Funds, which gives our shareholders added confidence in their
investments. 
Our parent company, Affiliated Managers Group (“AMG”) is a global asset management company with ownership interests in
a diverse group of boutique investment management firms (its “Affiliates”). MIG has the unique opportunity to access the investment skills and acumen of some of AMG’s Affiliates. The set of our Funds managed by these proprietary firms
also benefit from our activities to monitor the investment, compliance, and administrative aspects of the Funds. 
Below is a brief overview of
the securities markets and the performance results for the Funds. Following this letter, we also provide the Portfolio Managers’ discussion of their investment management approach, performance results, and market outlook. 
The year 2009 will go down in the history books as one in which securities markets sank to unimaginable levels and some investors briefly questioned the
viability of capitalism. As it turned out, capitalism did not cease to exist and equities, as well as credit-sensitive fixed income securities, managed to regroup and record one of the most impressive rebounds in the history of the capital markets.
The government’s unprecedented efforts with respect to healing the economy and stabilizing the securities markets via programs such as the Troubled Asset Relief Program (“TARP”), the Public-Private Investment Program
(“PPIP”), and the Term Asset-Backed Securities Loan Facility (“TALF”) seem to have achieved their desired short-term effects, although some would argue that the programs played only a minor role in the stabilization of the
markets and that free market forces simply corrected oversold conditions, just as they have on numerous occasions in the past. 
Managers AMG Systematic Value Fund 
 
Periods Ended 02/28/10
Systematic Value Fund (I)
®
Systematic Mid Cap Value Fund (I)
®
 
(I) = Institutional Class 
Performance for all share classes and detailed Fund positioning reviews are included within this report. 
 
1 
Table of Contents
Letter to Shareholders (continued) 
 
 
®
®
The following report covers the one-year period ended February 28, 2010. Should you have any questions about this report, or if you’d like to
receive a prospectus and additional information, including fees and expenses for either of these or any of the other Funds in our family, please feel free to contact us at 1-800-835-3879, or visit our Web site at www.managersinvest.com. As always,
please read the prospectus carefully before you invest or send money. 
If you are curious about how you can better diversify your investment
program, visit the Knowledge Center on our Web site and view our articles in the investment strategies section. You can rest assured that under all market conditions our team is focused on delivering excellent investment management services for your
benefit. 
We thank you for your continued confidence and investment in The Managers Funds. 
 
