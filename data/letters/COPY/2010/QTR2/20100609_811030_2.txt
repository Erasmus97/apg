Dear Shareholder,



The Osterweis Fund (the Fund) had a total return of in the first quarter of versus a total return of for the S&amp;P Index.The Funds annualized total returns over the one year, five year, ten year, and fifteen year periods ending March were and compared to total returns of and for the S&amp;P Index, in the same periods respectively.While we are pleased that the Fund continues to participate in the ongoing stock market rally, the more conservative asset allocation of the Fund coupled with our bias to less cyclical, larger cap names led to modest underperformance in the first quarter.The more marked underperformance over the past months also reflects the conservative posture of the Fund, a
s well as the fact that the Fund did not fall as far as the broader market did in the first quarter of and was, therefore, unlikely to experience as much of a recovery bounce.Of note, the Funds two year performance ending March which captures both the market plunge in to early as well as the rally from the March lows, shows an annualized gain of versus a loss of for the S&amp;P believe this outperformance reflects the emphasis we place on delivering full cycle returns for our investors.



Performance data quoted represents past performance; past performance does not guarantee future results.The investment return and principal value of an investment will fluctuate so that an investors shares, when redeemed, may be worth more or less than the original investment.Current performance of the Fund may be lower or higher than the performance quoted.Performance data current to the most recent month end may be obtained by calling investment should not be made solely on returns.The Fund imposes a redemption fee on shares held for less than days.Performance periods shown above are greater than days and do not reflect the redemption fee.If the investment period were shorter than days, perf
ormance would be lowered by the amount of the redemption fee.The Funds total expense ratio was as of March



The stock market experienced a strong rally in March.The rise in the S&amp;P in March was the ninth strongest month of performance looking back over the past years.Investors rotated back into riskier assets of all stripes in March, following several months of investor caution in January and























February.Of note, small cap stocks outperformed large cap, emerging market equities outperformed EAFE markets, high yield bonds outperformed investment grade, and distressed debt was the strongest of the major hedge fund categories.



In our quarterly outlook enclosed with this letter, we discuss many of the longer-term concerns that we have relating to the economy and markets.We continue to see only a modest economic recovery ahead, featuring stubbornly high levels of unemployment, a subdued consumer, a struggling housing sector, and unprecedented deficits and funding challenges at all levels of government.Looking out over the next three to five years, inflation, interest rates and taxes are likely to rise and a dollar crisis cannot be ruled out.In the short run, however, clear signs of economic stabilization abound:housing and unemployment trends are no longer deteriorating, consumer confidence and spending are beginning to rise, and capital spending trends are showing signs of recovering.There is no que
stion that this recovery remains fragile and critically dependent on the largess of Federal Government stimulus, both fiscal and monetary.However, with inflationary signals still benign, longer-term interest rates relatively stable and the dollar actually firming over the past four to five months, the Fed and Congress have the political cover needed to keep the monetary and fiscal stimulus flowing as long as it takes for the economy to get back on a more self-sustaining growth path i.e. until employment begins an unambiguous recovery.



In the meantime, corporate profits are staging a sharp recovery.Only time will tell if the rebound in earnings is the beginning of a sustained multi-year growth pattern or just a temporary, unsustainable snap back from the bombed-out levels of a year ago.If the economy does not falter in coming quarters, then even modest top-line growth coming on top of sharply streamlined cost structures could result in impressive corporate profit growth.At the same time, if the zero interest rate policy of the Fed continues, investors may continue to put idle money to work in both stocks and corporate bonds to participate in the resurgence of corporate income statements and balance sheets.



With these constructive near-term trends as backdrop, a continued rally in markets does not come as a surprise.Our sense is that the rally and the rotation into riskier assets will continue as long as investors believe the economic recovery both in the U.S. and globally is alive and well, and is unlikely to falter any time soon and the Federal Reserve and the other major central banks around the world are unlikely to drain the extraordinary liquidity injected into the financial system over the past months any time soon.



Over the past twelve months, we have steadily and thoughtfully brought the equity exposure in the Fund up.At the end of March, equities comprised of Fund assets, with cash and cash substitutes comprising the balance.Our investment team continues to find interesting, overlooked, attractively valued equities that hold out the potential to deliver compelling returns even if the economic recovery stalls out.Our cash balances give us the liquidity needed to























continue adding new promising names as we find them.In addition, if the market does experience a crisis of confidence in coming months, we should be well positioned to either add to existing portfolio names at more attractive price levels and/or add new names.



Thematically, we remain underweight Financials, Consumer Discretionary and IT stocks and overweight Health Care and Utility stocks.Our long standing emphasis on capital preservation steers us toward less cyclical sectors and toward companies whose drivers of revenue, earnings and cash flow growth are less dependent on the broader economy than on company specific factors.We remain attracted to companies that pay a significant dividend and that have the wherewithal to increase dividend payments in coming quarters.We also favor companies with strong balance sheets and cash flows that can be deployed to generate growth via both capital spending and M&amp;A.While this kind of high quality portfolio may underperform during periods of speculative exuberance, we believe it should prove out over
a complete market cycle.



As always, we thank you for your continued confidence in our management.



Sincerely,

Dear Shareholder,



During the first quarter of The Osterweis Strategic Income Fund (the Fund) had a total return of compared to for the Barclays Capital Aggregate Bond Index (the BC Agg) and for the Bank of America Merrill Lynch U.S. Corporate &amp; Government Master Index (the Merrill).The Funds annualized total returns for the one year, five year, seven year and since inception (August periods ending March were and respectively, compared in the same periods to and for the BC Agg and and for the Merrill.As of March the Funds SEC yield was



The performance data quoted above represents past performance.Past performance does not guarantee future results.The investment return and principal value of an investment will fluctuate so that an investors shares, when redeemed, may be worth more or less than their original cost.Current performance of the Fund may be lower or higher than the performance quoted.Performance data current to the most recent month-end may be obtained by calling investment should not be made solely on the basis of returns.The Fund imposes a redemption fee on shares held for less than days.Performance periods shown above are greater than days and do not reflect the redemption fee.If the investment period w
ere shorter than days, performance would be lowered by the amount of the redemption fee.The Funds total expense ratio was as of March



While there are still many concerning factors in the economic environment including slow improvement in employment, continued reluctance of banks to lend, the large trade deficit with China, the Greek crisis and, of course, the long-term implications of the massive government stimulus, the markets have continued their upward trend, with general sentiment being that the economy has turned the corner.Certainly, the steep yield curve would support that view.



As we discussed in past letters, we steadily increased the effective duration of the Fund from about nine months at the end of to over two years currently.We now feel it is prudent to selectively begin shortening duration























again.A portion of this move is occurring naturally as we sell longer-dated, equity-sensitive convertible bonds that have run up with the stock market.Another factor in this move is related to the refinancing wave that has been occurring in corporate bonds.We have been participating in this boom by selectively adding bonds of companies we feel have attractive long-term prospects.However, as the market gets saturated with new issues, and many investors shift their portfolios out of shorter-term paper to fund new purchases, we are finding more short-dated paper at attractive yields and are increasing our weightings there.



The final factor driving this duration shift is our ever-present aversion to risk.The markets may be indicating an economic rebound; the steep yield curve may be indicating higher future interest rates; and though inflation remains muted at present, the level of government stimulus may be indicating rising inflation in the future.In an improving economic environment and a rising interest rate environment, we have always maintained that the greatest risks are in long-term, investment grade bonds.As such we are reducing the Funds duration profile and continuing our focus on high yield and convertible securities.



As always, we thank you for your continued confidence in our management.











Carl P. Kaufman


Simon T. Lee



Portfolio Manager


Assistant Portfolio Manager










This commentary contains the current opinions of the authors as of the date above, which are subject to change at any time.This commentary has been distributed for informational purposes only and is not a recommendation or offer of any particular security, strategy or investment product.Information contained herein has been obtained from sources believed to be reliable, but is not guaranteed.



The Fund is non-diversified, meaning it may concentrate its assets in fewer holdings than a diversified fund. Therefore, the Funds share price may be more influenced by fluctuations in each holdings value than a diversified fund. The Fund may invest in debt securities that are un-rated or rated below investment grade. Such lower-rated securities may present an increased possibility of default, price volatility or illiquidity compared to higher-rated securities. The Fund may invest in foreign securities which will involve greater volatility and political, economic and currency risks and differences in accounting methods. Investments in debt securities typically decrease in value when interest rates rise. This risk is usually greater for longer-term debt securities. Small- and mid-capitalization companies
tend to have limited liquidity and greater price volatility than large-capitalization companies.



The Funds investment performance reflects reimbursement of expenses previously waived.Please refer to the prospectus for further details.























The Barclays Capital Aggregate Bond Index and the Bank of America Merrill Lynch U.S. Corporate &amp; Government Master Index are unmanaged indices which are regarded as standards for measuring the U.S. investment grade bond market in general, and are provided for comparison purposes.These indices do not incur expenses and are not available for investment.



Must be preceded or accompanied by a current prospectus.Please refer to the prospectus for important information about the investment company including investment objectives, risks, charges and expenses.



The Osterweis Strategic Income Fund is distributed by Quasar Distributors, LLC.



















The Osterweis Strategic Income Fund










MANAGERS DISCUSSION OF FUND PERFORMANCE







For the fiscal year ended March The Osterweis Strategic Income Fund (the Fund) had a total return of outperforming the Barclays Capital U.S. Aggregate Bond Index (the BC Agg) and the Bank of America Merrill Lynch U.S. Corporate &amp; Government Master Index (the Merrill), which had total returns of and for the same period, respectively.The remainder of this discussion will compare the Fund against the Merrill, as constituent data is not available for the BC Agg.



The Funds outperformance was primarily attributable to the Funds investments in higher yielding securities.Over the period, the Fund had an average current yield of while the Merrill had an average current yield of the Fund had an average yield to maturity of while the Merrill had an average yield to maturity of addition, the shorter effective duration of the Fund contributed to relative performance, as short-term yields (less than one year) fell slightly, and longer-term yields (greater than one year) increased.Over the period, the Funds average effective duration was years, compared to years for the Merrill.The Funds lower average quality also contributed to relative performance, as returns on lower quality rated issues outp
erformed investment grade issues.



The Banking sector performed well during the period, and the Funds lack of exposure to this sector was a detractor from relative performance.The Finance and Agency sectors also performed positively during the period, and the Funds underexposure and lack of exposure, respectively, in these sectors were also detractors from relative performance.



The Funds top five contributors to performance during the period were RSC Equipment Rental Corp. Brown Shoe Company Inc. Corp. Cadence Design Systems Inc. Hertz Corp. and Coleman Cable Inc. Funds top five detractors from performance were Emigrant Capital II Gencorp Inc Pegasus Solutions Inc. Rentech Inc. and Real Mex Restaurants Inc.



Current and future holdings are subject to risk.



Past performance is not indicative of future results.The relative performance attribution results noted in the and paragraphs were based on a holdings-based attribution system and were based on the Funds fixed income holdings and cash.Fund holdings and/or sector allocations are subject to change at any time and are not recommendations to buy or sell any security.Please see pages of this report for complete holdings information.



















The Osterweis Funds











SECTOR ALLOCATION at March (Unaudited)
