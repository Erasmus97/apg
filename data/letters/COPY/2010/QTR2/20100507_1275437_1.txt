Dear Fellow Shareholders,




What a long, strange trip its been The Grateful Dead




Weve never been a huge Grateful Dead fan, but looking back over the year, you cant help but think of this lyric.While was a year of disaster, was a year of began where left off.The market was in free fall and the economy was headed for the next great depression.The Russell Value index bottomed on at down from its peak of ended the year at up from the low.




In last years letter we commented that we were flirting with disaster.We believed the most important decision an investor needed to make at the time was whether we were just flirting or [were] we headed for a serious relationship?We believed the easy thing to do at the time was to be negative because there was a lot of support for that view in the media and in the economic data.However, we felt that was the wrong trade to make.In our opinion, the risk was more than adequately reflected in the market, while any hope of an improving economic situation was barely more than a cheap option in a stocks price.As our pricing discipline tells us, we chose to go where we saw the best risk reward. We removed our conservative
achieved this by adding a larger number of more aggressive companies in smaller weights.We believed this would position us for the revaluation opportunity in the market without taking on a lot of company-specific risk.




We are pleased to report that our positioning paid off for our shareholders.The Veracity Small Cap Value Fund (the Fund) Class R returned versus the Russell Value Index (the Benchmark) return of for the year ended February addition to our relative outperformance, the Fund also experienced solid absolute returns.Much of the out performance is attributable to removing our conservative overweight and adding cheaper, more aggressive companies to the portfolio.From a sector standpoint, Producer Durables and Consumer Discretionary were the two largest contributors to relative performance, adding over and respectively.Our investments in Producer Durables increased over for the year while our holdings in the
Consumer Discretionary sector returned almost two sectors, in addition to Technology (up almost were the poster children for the types of companies we were adding to the portfolio.They were cyclical businesses which were priced as if the next great depression were a sure thing.Some stellar performers included Veeco Instruments (VECO), a semi conductor equipment company up Jones Apparel Group (JNY), a clothing company up over and Fairchild Semiconductor (FCS) up almost























Cash was our biggest detractor for the year, costing us over in performance.Our cash levels were not large, but the strong market severely punished any cash that was held.Outside of cash, Health Care and Consumer Staples, two conservative sectors, caused minor pain costing us less than bps each of relative performance.On an individual stock basis, negative performance came from more cautious names in the Fund that didnt keep up with the overall market.Two examples include: Northwest Natural Gas (NWN) and Vectren (VVC), both utilities which were up and respectively.




Indeed, what a long strange trip it has been this year.We have gone from the brink of financial disaster and depression in the beginning of the year to a market rally leaving people asking have we come too far too fast by the end.




As the song says, sometimes the lights all shinin on me, other times I can barely see.Looking ahead leaves more questions than answers.The consensus opinion expects a strong market in the first half of the year as the economy recovers, followed by a sell-off in the second half of the year as macro headwinds take their toll.There are plenty of headwinds to go around: stimulus runoff, the fed raising rates, the end of MBS purchases, and the end of the homebuyer tax credit.In addition, the country faces rising tax burdens, a ballooning deficit, and increased government regulation.And the list goes on.




What is intriguing is that everyone knows about these headwinds.Our guess is that this is not the first time youve heard about these issues.If it is, sorry to bring you down!Any student of the market would ask the question if the market really is expected to sell off in the second half of the year, why would anyone buy today?If this really is the consensus AND the market is a discounting mechanism, then these concerns should be reasonably reflected in current prices.




The market still looks attractive to us at current levels.We continue to believe that sentiment is, at best, cautiously bullish.The individual investor has shunned this rally, unlike when inflows were positive.Earnings should also be strong this quarter and revenue comparisons are getting easier.The last point is the most important.We think too much emphasis is being placed on the macro factors.Big picture issues have dominated the markets mind share for the last months and rightfully so.We think were coming to an end of its dominance and entering a period where individual company performance and valuation will matter.In short, we think we are heading back to a more normal market environment.We ag
ree that the macro factors are important.We just think the trade is crowded, and that we will have a better chance generating alpha by returning to the roots of our philosophy and process looking for undervalued stocks with catalysts.




This back-to-basics approach is a change from the last months when we focused more on macro inputs and factors, reducing company-specific risk and positioning us for a revaluation in the market.That trade worked, and we think the risk/reward no longer favors that positioning.The market is, again, rewarding company specific risk, and we are re-positioning the portfolio to benefit.We have begun bringing the number of























holdings back in line with our historical range (i.e. to holdings), using the earnings period as a catalyst to make decisions on the smaller positions we hold.




From a sector basis, we dont foresee any large deviations from current positioning, with the possible exception of Financials where we are a little more underweight the Benchmark than we would like.From a characteristic basis, we will continue to look for value, but will no longer be focusing exclusively on the cheaper, controversial and smaller names as we did over the last months.





In summary, we are concerned about the macro headwinds.However, we also believe there is a lot of positive fundamental momentum that is not being reflected in stocks.We will be getting back to the basics of buying undervalued stocks with catalysts to try to capture the alpha opportunity that we see there.




Thank you for the trust you have placed in us.





Sincerely,
