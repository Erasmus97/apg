Dear Shareholders: 
After
having suffered their biggest declines since the Great Depression, most global markets experienced an impressive resurgence during the latter months of 2009 and the first quarter of 2010. The global economy was able to reap the benefits of two major
trends. The first of these was the massive efforts of governments and central banks to increase liquidity in the financial system as they sought to prevent the credit crisis from further affecting the banking system. The second was the move by
companies around the world to cut costs and operations to prepare for rapidly changing market conditions. We believe that these moves not only shortened the length of the downturn but also set the stage for the recovery we are seeing today.

Even with the significant market gains of 2009 and the early part of 2010, the recovery is unrolling at a moderate pace, with rebounds in the
manufacturing sector and corporate America leading the way. Central bankers are proceeding with caution and have held benchmark interest rates unchanged as they debate the best way to withdraw stimulus measures without disrupting the fragile growth
process. 
While hurdles remain, we believe that the global economy is on the road to recovery. As always, we continue to be
mindful of the many challenges faced at the individual, national, and international levels. It is at times such as these that we want to remind investors of the merits of maintaining a long-term view, adhering to basic investing principles such as
asset allocation and diversification, and working closely with advisors to identify and research investment opportunities. At MFS®, we take particular pride in how well mutual funds can help investors by providing the diversification that is
important in any type of market climate. 
Respectfully, 


Dear Shareholders: 
After
having suffered their biggest declines since the Great Depression, most global markets experienced an impressive resurgence during the latter months of 2009 and the first quarter of 2010. The global economy was able to reap the benefits of two major
trends. The first of these was the massive efforts of governments and central banks to increase liquidity in the financial system as they sought to prevent the credit crisis from further affecting the banking system. The second was the move by
companies around the world to cut costs and operations to prepare for rapidly changing market conditions. We believe that these moves not only shortened the length of the downturn but also set the stage for the recovery we are seeing today.

Even with the significant market gains of 2009 and the early part of 2010, the recovery is unrolling at a moderate pace, with rebounds in the
manufacturing sector and corporate America leading the way. Central bankers are proceeding with caution and have held benchmark interest rates unchanged as they debate the best way to withdraw stimulus measures without disrupting the fragile growth
process. 
While hurdles remain, we believe that the global economy is on the road to recovery. As always, we continue to be
mindful of the many challenges faced at the individual, national, and international levels. It is at times such as these that we want to remind investors of the merits of maintaining a long-term view, adhering to basic investing principles such as
asset allocation and diversification, and working closely with advisors to identify and research investment opportunities. At MFS®, we take particular pride in how well mutual funds can help investors by providing the diversification that is
important in any type of market climate. 
Respectfully, 


Dear Shareholders: 
After
having suffered their biggest declines since the Great Depression, most global markets experienced an impressive resurgence during the latter months of 2009 and the first quarter of 2010. The global economy was able to reap the benefits of two major
trends. The first of these was the massive efforts of governments and central banks to increase liquidity in the financial system as they sought to prevent the credit crisis from further affecting the banking system. The second was the move by
companies around the world to cut costs and operations to prepare for rapidly changing market conditions. We believe that these moves not only shortened the length of the downturn but also set the stage for the recovery we are seeing today.

Even with the significant market gains of 2009 and the early part of 2010, the recovery is unrolling at a moderate pace, with rebounds in the
manufacturing sector and corporate America leading the way. Central bankers are proceeding with caution and have held benchmark interest rates unchanged as they debate the best way to withdraw stimulus measures without disrupting the fragile growth
process. 
While hurdles remain, we believe that the global economy is on the road to recovery. As always, we continue to be
mindful of the many challenges faced at the individual, national, and international levels. It is at times such as these that we want to remind investors of the merits of maintaining a long-term view, adhering to basic investing principles such as
asset allocation and diversification, and working closely with advisors to identify and research investment opportunities. At MFS®, we take particular pride in how well mutual funds can help investors by providing the diversification that is
important in any type of market climate. 
Respectfully, 


