Dear Shareholder, 
We are pleased to provide the annual report of Legg Mason Western Asset New Jersey Municipals Fund for the twelve-month reporting period ended March
31, 2010. 
Please read on for a detailed look at prevailing economic and market conditions during the Fund’s reporting period and to
learn how those conditions have affected Fund performance. Important information with regard to recent regulatory developments that may affect the Fund is contained in the Notes to Financial Statements included in this report. 
As always, we remain committed to providing you with excellent service and a full spectrum of investment choices. We also remain committed to
supplementing the support you receive from your financial advisor. One way we accomplish this is through our website, www.leggmason.com/individualinvestors. Here you can gain immediate access to market and investment information, including:

 
Fund prices and performance, 
 
Market insights and commentaries from our portfolio managers, and 
 
A host of educational resources. 
We look forward to helping you meet your financial goals. 
Sincerely, 


