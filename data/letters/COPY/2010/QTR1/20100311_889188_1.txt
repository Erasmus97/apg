Dear Shareholder: 
I recently read a market commentary that stated the ‘easy money’ has been made in the stock and bond markets. I couldn’t disagree more. I
believe that the hardest money investors have ever made was over the past twelve harrowing months. While it took a high degree of fortitude to remain invested, as we look forward, there is an emerging sense that the markets have stabilized and
rebounded. 
As we look out over the investment and economic landscape, we believe the preponderance of indicators are beginning to turn more
positive. While many pundits agree that the global economy and capital markets are recovering, what remains in doubt is the shape of the recovery. The burning question remains—what is the shape of things to come? 
There are those who argue persuasively that we have entered a ‘new normal’ where the forces of the deleveraging process will be marked by a
long-term desire to minimize debt rather than to maximize profits, consumption or growth. They project a dour view of paltry economic growth as far as the eye can see. 
Then there are those who argue convincingly that history has shown the deeper the economic pullback, the more dynamic the rebound. They claim that deep inventory reductions coupled with pent up demand
from foregone consumption, combined with fiscal and monetary stimulus, create a powerful force that catapults economic activity and corporate profits to unforeseen levels of robust growth. 
The question of what comes next becomes even more confusing when it is complicated by divergent views on government deficits, currency movements, inflation
threats and the forces of demographic shifts as well as globalization of trade and competition. I believe that attempting to predict the outcome of these different scenarios with any kind of certainty is fraught with peril. No doubt there will be
fits and starts to the signs of recovery, as well as confirming data and non confirming data validating or invalidating these various prognostications. 
At Forward, we believe that a reasoned approach to diversification is the best way to proceed. We have always maintained that asset allocation with non-correlated asset classes is the best way to maintain a strong portfolio. Coming out of a
distressed period, it would be reasonable to expect to see diversification benefits improve as correlations across asset classes decline to more normal patterns. As the dust settles, local markets and different asset classes typically return to
their own unique rhythms and cycles. 
We remain focused on asset classes that will benefit from the continued recovery, as well as overall
global growth. Past experience has shown that diversifying across various return patterns and rebalancing assets to an identified level of tolerable risk benefits investors. Of course, discussion of an appropriate allocation of assets needs to
include an assessment of one’s risk tolerance and time horizon, and we believe such an evaluation is best undertaken with a financial advisor or an investment professional. 
This year was a year of growth for Forward Funds. In June, we added the Kensington Funds to the Forward Funds family. These four funds—Forward Global Infrastructure Fund, Forward International Real
Estate Fund, Forward Select Income Fund and Forward Strategic Realty Fund—complement Forward’s extensive roster of funds and offer investors further opportunities to diversify their portfolios with income-oriented products. The portfolio
management teams have built deep expertise in global

 
Table of Contents
 

real estate and infrastructure—the Global Infrastructure Fund invests in companies that may benefit from economic stimulus packages and the Select Income Fund offers investors the potential
to generate significant income with a focus on REIT preferred equities. 
Additionally, our ongoing conversations with investment professionals
have uncovered the need for products that allow investors to be more nimble and tactical in asset allocation decisions. In response, Forward launched the Forward Tactical Growth Fund which has broad latitude in being able to position itself long or
short in response to changing market dynamics. We invite you to learn more about our new funds at www.forwardfunds.com. 
We remain deeply
committed to helping our shareholders attain true portfolio diversification and achieve their long-term financial goals. We believe that transparent practices give our investors access to the information they need to make important investment
decisions. I invite you to review the information in this report and the performance of the Forward Funds in 2009, and thank you for the continued confidence that you place in our Funds. 
Best regards, 


