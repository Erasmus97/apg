Dear Shareowner,

Stock and bond markets around the globe have begun to recover over the past
year from one of their most tumultuous periods in history. This is a welcome
relief, and we are generally optimistic about the prospects for the economy
going forward. Still, challenges remain: unemployment is high; consumer demand
and loan growth are weak; and housing has not returned to normal.

At Pioneer, we have long advocated the benefits of investing for the long term.
This strategy has generally performed well for many investors. Those who
remained invested in the market during the downturn have most likely seen their
portfolios start to recover over the past year, as the Dow Jones Industrial
Average climbed back from the depressed levels we saw in early Many bond
investors have similarly seen a strong rebound, with a broad-based recovery
occurring across many different fixed-income asset classes. The riskiest asset
classes, such as high-yield bonds, outperformed other fixed-income asset
classes during most of

At Pioneer, we are not changing the approach to investing that we have used for
more than years. We remain focused on company fundamentals and risk
management. Our investment process is based on careful research into individual
companies, quantitative analysis, and active portfolio management. This
three-pillared process, which we apply to each of our portfolios, is supported
by an integrated team approach and is designed to carefully balance risk and
reward. While we see potential opportunities for making money in many corners
of the markets around the globe, it takes research and experience to separate
solid investment opportunities from speculation.

Following this difficult period, many investors are rethinking their approach
to investing and risk management. Some are questioning whether the basic
investment principles they were taught in the past are still useful in today's
markets. Complicating matters is that financial markets remain unpredictable.
Our advice, as always, is to work closely with a trusted financial advisor to
discuss your goals and work together to develop an investment strategy that
meets your individual needs. There is no single best strategy that works for
every investor.


Pioneer Ibbotson Asset Allocation Series | Semiannual Report |


We invite you to learn more about Pioneer and our time-tested approach to
investing by consulting with your financial advisor or visiting us online at
www.pioneerinvestments.com. We greatly appreciate your trust in us and we thank
you for investing with Pioneer.

Respectfully,
