Dear Shareholder,





October witnessed events reminiscent of times past for investors
and baseball fans alike. The New York Yankees returned to the
World Series after a six-year hiatus, and double-digit
unemployment returned to the United States after a

break. New Yorkers may have regarded the Yankees
championship as a restoration of order to the world, but
investors found little relief as unemployment rose to


Nevertheless, the economy did not collapse and the markets did
not melt down. Although higher unemployment is not good news,
most recognize that it represents a lagging effect of the
recession, and as such is likely to persist even after the
economy has swung into recovery.





On the other hand, fund investors seem still to be focusing more
on safety than appreciation potential. Year-to-date, investors
have moved large amounts out of money market funds. They have
been adding significantly to bond funds, but disinvesting from
stock







Volatile equity markets over the past year and a half have led
many investors to seek refuge in fixed income investments, a
move that may have made sense when the highest priority was to
avoid losses. Despite Octobers news, it seems like a good
time to start thinking longer term again; this may be an
opportunity for investors to rethink their balance between
potential risk and potential return.





Of course, only you and your financial advisor can determine
what is best for your particular situation, so we encourage you
to discuss these points thoroughly with your advisor before
taking any action in your portfolio.





Thank you for your continued confidence in ING Funds. We look
forward to serving your investment needs in the future.





Sincerely,
