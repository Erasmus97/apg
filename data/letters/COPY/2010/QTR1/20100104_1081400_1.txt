Dear Valued Shareholder,

We are pleased to provide you with this annual report for the Wells Fargo
Advantage Specialty Funds(SM) for the period that ended October
The period brought welcome signs of a potentially sustainable economic
recovery, sparking a sharp rally in the financial markets that ended a streak of
six consecutive quarterly declines. We believe that the sharp reversal that
occurred in the financial markets during this period underscores the importance
of maintaining a disciplined focus on a long-term investment strategy. Although
periods of volatility can be challenging for investors, staying focused on a
long-term strategy based on individual goals and risk tolerance can help avoid
missing potential periods of strong recovery.

SIGNS OF ECONOMIC IMPROVEMENT SPARKED A SUSTAINED MARKET RALLY.

During the early part of the period, the stock market continued to
tumble, reaching levels in March that had last been seen in However,
early signs of potential economic improvement began to emerge in the spring,
sparking the strongest rally since The market continued to advance through
summer and into fall, with seven consecutive months of positive returns for the
S&P Index--its second-longest stretch in a decade. By the end of the period,
volatility had begun to climb again on questions about the sustainability of the
economic improvement. That resulted in a slight correction in the equity markets
during October

As the period began, fallout from the credit crisis continued amid mounting
fears that the economy had fallen into recession. Volatility spiked across the
financial markets in fall after Lehman Brothers filed for bankruptcy and
the government enacted emergency measures to rescue AIG, Fannie Mae, Freddie
Mac, and other large financial institutions. Volatility rose to unprecedented
heights as the global financial system froze up and investors feared a total
economic collapse. Concerns about potential nationalization of the banking
system kept volatility elevated early in but the release of bank stress
tests in May helped to alleviate those concerns, and volatility declined
steadily as the market rallied strongly until the final month of the period.

ECONOMIC GROWTH TURNED POSITIVE AFTER A SEVERE CONTRACTION.

Positive economic growth resumed in the third quarter of following the
first string of four consecutive quarters of negative economic growth in at
least years. Gross Domestic Product growth was for the third quarter of
strongest since Economic consensus was that the recession had
likely ended during the second quarter of However, with much of the growth
attributable to government stimulus programs, questions remained over the
potential sustainability of the recovery. By the end of the period, the
National Bureau of Economic Research had not declared an official end to the
recession.

UNEMPLOYMENT REMAINED HIGH, BUT OTHER ECONOMIC INDICATORS IMPROVED.

The unemployment rate rose throughout the period, reaching in October
highest level in more than years. However, the pace of job losses
had slowed as the period came to a close. Payroll employment



Wells Fargo Advantage Specialty Funds


Letter to Shareholders

declined an average of jobs during the last three months of the period,
well below the average from earlier in Still, since the start of
the recession in December more than million jobs had been lost by the
end of the period.

Although unemployment remained high, other economic statistics began to show
signs of improvement. Industrial production, manufacturing, and consumer
sentiment had all improved significantly as the period came to a close. Retail
sales improved in the latter months of the period, helped in part by the "Cash
for Clunkers" program that temporarily boosted auto sales. Home sales and prices
also seemed to have stabilized and began to show signs of improvement in many
areas of the country, spurred in part by the government's tax credit for
first-time home buyers.

FED CONTINUED TO FOCUS ON THE ECONOMIC STIMULUS PROGRAMS.

Despite extensive economic stimulus measures by the Federal Reserve, bank
lending did not expand during the period. This limited access to credit
indicates that the trillions of dollars that were added to the monetary system
through the government stimulus programs might not have an inflationary impact
in the near term. In the second half of the period, debate began to escalate
over the need for the Fed to outline an "exit strategy" from its stimulus
programs. Despite that debate, however, the Federal Open Market Committee (FOMC)
held the federal funds rate at the range of to that it first targeted
in December The Fed concluded its purchases of longer-term U.S. Treasuries
in October but continued to buy mortgage-backed securities, with that
program slated to end in March In its final statement during the
period, the FOMC noted the signs of economic improvement but reiterated that it
was likely to keep the federal funds rate at exceptionally low levels for an
extended period because the economy remains sluggish.

EQUITY MARKETS EXPERIENCED EXTREME VOLATILITY BUT REBOUNDED IN MARCH

For the period, the equity markets can be split into two distinct
sub-periods. From the beginning of the period until early March stocks
declined sharply after falling steadily during the first half of The broad
market, as measured by the S&P Index, began the period in the wake of a
nearly decline in October worst month since October The
declines continued through March when the market reversed abruptly amid talk
that several large banks would report stronger-than-expected earnings during the
first quarter. Those reports helped dispel investor fears of potential bank
nationalization and sparked the biggest rally in the equity markets since
with the S&P Index surging in trading days. That rally moderated but
persisted throughout the summer and into fall with the S&P Index
gaining from the March low through September In the final month of
the period, volatility began to increase again, resulting in a slight
correction in the equity markets. For October the S&P Index declined
about



Wells Fargo Advantage Specialty Funds


Letter to Shareholders

STOCKS IN THE FINANCIALS SECTOR EXPERIENCED EXTREME VOLATILITY DURING THE
PERIOD.

INFORMATION TECHNOLOGY (IT) WAS THE STRONGEST SECTOR DURING THE PERIOD,
BENEFITING FROM THE GENERALLY HEALTHIER FUNDAMENTALS OF IT COMPANIES.

For the full period, the S&P Index gained and the Dow Jones
Industrial Average rose Among domestic equity indices, the tech-heavy
NASDAQ Composite Index was the leader, with a return over the
period due to the strength of the rebound in technology stocks in

Stocks in the financials sector experienced extreme volatility during the
period. The ongoing credit crisis and severe economic contraction culminated
with the bankruptcy of Lehman Brothers in late and fears of bank
nationalization in early Those pressures drove down the shares of
financial companies to extreme levels in February and March as investors
feared a complete collapse of the global financial system. In the subsequent
rally, however, the financials sector was among the leaders, as the threat of
nationalization dissipated and signs of stabilization began to emerge. Stocks in
the sector remained volatile amid ongoing restructuring across the financial
landscape. The S&P Financials Index fell during the period.

By contrast, the S&P North American Technology Index gained for the
period. Information technology (IT) was the strongest sector during the
period, benefiting from the generally healthier fundamentals of IT companies. In
the decade since the technology bubble burst, many companies in the sector
worked off their excesses and have strong balance sheets, with significant
levels of cash and minimal debt. With access to short-term financing a major
concern during the period, companies in the IT sector that were able to
self-fund tended to be rewarded by shareholders. Another tailwind is the ability
of IT companies to help improve the productivity of other companies, which have
been highly focused on controlling costs in the challenging economic
environment.

DON'T LET SHORT-TERM VOLATILITY DERAIL LONG-TERM INVESTMENT GOALS.

The period that ended October was challenging for investors
but brought a welcome reprieve from the volatility of The rapid rebound in
market performance that occurred in early underscores the importance of
maintaining focus on long-term investment goals through periods of volatility so
as not to miss potential periods of strong recovery. Although periods of
volatility can present challenges, experience has taught us that maintaining a
long-term investment strategy based on individual goals and risk tolerance can
be an effective way to plan for the future.

As a whole, WELLS FARGO ADVANTAGE FUNDS(R) represents investments across a broad
range of asset classes and investment styles, giving you an opportunity to
create a diversified investment portfolio. Although diversification may not
prevent losses in a downturn, it may help to reduce them and provide you with
one way of managing risk. Our diverse family of mutual funds may also help. We
offer more than Funds that cover a broad spectrum of investment styles and
asset classes.



Wells Fargo Advantage Specialty Funds


Letter to Shareholders

Thank you for choosing WELLS FARGO ADVANTAGE FUNDS. We appreciate your
confidence in us. Through each market cycle, we are committed to helping you
meet your financial needs. If you have any questions about your investment,
please contact your investment professional or call us at You
may also want to visit our Web site at www.wellsfargo.com/advantagefunds.

Sincerely,

Dear Valued Shareholder,

We are pleased to provide you with this annual report for the Wells Fargo
Advantage Small and Mid Cap Stock Funds for the period that ended
October The period brought welcome signs of a potentially sustainable
economic recovery, sparking a sharp rally in the financial markets that ended a
streak of six consecutive quarterly declines. We believe that the sharp reversal
that occurred in the financial markets during this period underscores the
importance of maintaining a disciplined focus on a long-term investment
strategy. Although periods of volatility can be challenging for investors,
staying focused on a long-term strategy based on individual goals and risk
tolerance can help avoid missing potential periods of strong recovery.

SIGNS OF ECONOMIC IMPROVEMENT SPARKED A SUSTAINED MARKET RALLY.

During the early part of the period, the stock market continued to
tumble, reaching levels in March that had last been seen in However,
early signs of potential economic improvement began to emerge in the spring,
sparking the strongest rally since The market continued to advance through
summer and into fall, with seven consecutive months of positive returns for the
S&P Index--its second-longest stretch in a decade. By the end of the period,
volatility had begun to climb again on questions about the sustainability of the
economic improvement. That resulted in a slight correction in the equity markets
during October

As the period began, fallout from the credit crisis continued amid mounting
fears that the economy had fallen into recession. Volatility spiked across the
financial markets in fall after Lehman Brothers filed for bankruptcy and
the government enacted emergency measures to rescue AIG, Fannie Mae, Freddie
Mac, and other large financial institutions. Volatility rose to unprecedented
heights as the global financial system froze up and investors feared a total
economic collapse. Concerns about potential nationalization of the banking
system kept volatility elevated early in but the release of bank stress
tests in May helped to alleviate those concerns, and volatility declined
steadily as the market rallied strongly until the final month of the period.

ECONOMIC GROWTH TURNED POSITIVE AFTER A SEVERE CONTRACTION.

Positive economic growth resumed in the third quarter of following the
first string of four consecutive quarters of negative economic growth in at
least years. Gross Domestic Product growth was for the third quarter of
strongest since Economic consensus was that the recession had
likely ended during the second quarter of However, with much of the growth
attributable to government stimulus programs, questions remained over the
potential sustainability of the recovery. By the end of the period, the
National Bureau of Economic Research had not declared an official end to the
recession.

UNEMPLOYMENT REMAINED HIGH, BUT OTHER ECONOMIC INDICATORS IMPROVED.

The unemployment rate rose throughout the period, reaching in October
highest level in more than years. However, the pace of job losses
had slowed as the period came to a close. Payroll employment declined




Wells Fargo Advantage Small and Mid Cap Stock Funds


Letter to Shareholders

an average of jobs during the last three months of the period, well
below the average from earlier in Still, since the start of the
recession in December more than million jobs had been lost by the end of
the period.

Although unemployment remained high, other economic statistics began to show
signs of improvement. Industrial production, manufacturing, and consumer
sentiment had all improved significantly as the period came to a close. Retail
sales improved in the latter months of the period, helped in part by the "Cash
for Clunkers" program that temporarily boosted auto sales. Home sales and prices
also seemed to have stabilized and began to show signs of improvement in many
areas of the country, spurred in part by the government's tax credit for
first-time home buyers.

THE FED CONTINUED TO FOCUS ON THE ECONOMIC STIMULUS PROGRAMS.

Despite extensive economic stimulus measures by the Federal Reserve, bank
lending did not expand during the period. This limited access to credit
indicates that the trillions of dollars that were added to the monetary system
through the government stimulus programs might not have an inflationary impact
in the near term. In the second half of the period, debate began to escalate
over the need for the Fed to outline an "exit strategy" from its stimulus
programs. Despite that debate, however, the Federal Open Market Committee (FOMC)
held the federal funds rate at the range of to that it first targeted
in December The Fed concluded its purchases of longer-term U.S. Treasuries
in October but continued to buy mortgage-backed securities, with that
program slated to end in March In its final statement during the
period, the FOMC noted the signs of economic improvement but reiterated that it
was likely to keep the federal funds rate at exceptionally low levels for an
extended period because the economy remains sluggish.

EQUITY MARKETS EXPERIENCED EXTREME VOLATILITY BUT REBOUNDED IN MARCH

For the period, the equity markets can be split into two distinct
sub-periods. From the beginning of the period until early March stocks
declined sharply after falling steadily during the first half of The broad
market, as measured by the S&P Index, began the period in the wake of a
nearly decline in October worst month since October The
declines continued through March when the market reversed abruptly amid talk
that several large banks would report stronger-than-expected earnings during the
first quarter. Those reports helped dispel investor fears of potential bank
nationalization and sparked the biggest rally in the equity markets since
with the S&P Index surging in trading days. That rally moderated but
persisted throughout the summer and into fall with the S&P Index
gaining from the March low through September In the final month of
the period, volatility began to increase again, resulting in a slight
correction in the equity markets. For October the S&P Index declined
about




Wells Fargo Advantage Small and Mid Cap Stock Funds


Letter to Shareholders

FOR THE FULL PERIOD, THE S&P INDEX GAINED AND THE DOW JONES
INDUSTRIAL AVERAGE ROSE

For the full period, the S&P Index gained and the Dow Jones
Industrial Average rose Among domestic equity indices, the tech-heavy
NASDAQ Composite Index was the leader, with a return over the
period due to the strength of the rebound in technology stocks in The
Russell Midcap(R) gained while the Russell of
small cap stocks advanced

Over the period, mid cap stocks led in performance, while large cap and small
cap stocks had nearly equal returns. Market cap leadership was split, however,
between the early and later months of the period. Large cap stocks led during
the early months as investors rotated into larger, more stable companies during
the market decline in late and early Small caps outperformed large
caps during the subsequent rally, however, as investors' risk appetite returned.
During the period, the growth investment style significantly
outperformed the value investment style across all market capitalizations, as
measured by the Russell indices, benefiting from the growth indices' significant
exposure to the information technology sector and lower exposure to the
financials sector.

DON'T LET SHORT-TERM VOLATILITY DERAIL LONG-TERM INVESTMENT GOALS.

The period that ended October was challenging for investors
but brought a welcome reprieve from the volatility of The rapid rebound in
market performance that occurred in early underscores the importance of
maintaining focus on long-term investment goals through periods of volatility so
as not to miss potential periods of strong recovery. Although periods of
volatility can present challenges, experience has taught us that maintaining a
long-term investment strategy based on individual goals and risk tolerance can
be an effective way to plan for the future.

As a whole, WELLS FARGO ADVANTAGE FUNDS(R) represents investments across a broad
range of asset classes and investment styles, giving you an opportunity to
create a diversified investment portfolio. Although diversification may not
prevent losses in a downturn, it may help to reduce them and provide you with
one way of managing risk. Our diverse family of mutual funds may also help. We
offer more than Funds that cover a broad spectrum of investment styles and
asset classes.

- ----------
The Russell Midcap(R) Index measures the performance of the smallest
companies in the Russell Index, which represent approximately of
the total market capitalization of the Russell Index. You cannot
invest directly in an index.

The Russell Index measures the performance of the smallest
companies in the Russell Index, which represents approximately
of the total market capitalization of the Russell Index. You cannot
invest directly in an index.




Wells Fargo Advantage Small and Mid Cap Stock Funds


Letter to Shareholders

Thank you for choosing WELLS FARGO ADVANTAGE FUNDS. We appreciate your
confidence in us. Through each market cycle, we are committed to helping you
meet your financial needs. If you have any questions about your investment,
please contact your investment professional or call us at You
may also want to visit our Web site at www.wellsfargo.com/advantagefunds.

Sincerely,

Dear Valued Shareholder,

We are pleased to provide you with this annual report for the Wells Fargo
Advantage Small and Mid Cap Stock Funds for the period that ended
October The period brought welcome signs of a potentially sustainable
economic recovery, sparking a sharp rally in the financial markets that ended a
streak of six consecutive quarterly declines. We believe that the sharp reversal
that occurred in the financial markets during this period underscores the
importance of maintaining a disciplined focus on a long-term investment
strategy. Although periods of volatility can be challenging for investors,
staying focused on a long-term strategy based on individual goals and risk
tolerance can help avoid missing potential periods of strong recovery.

SIGNS OF ECONOMIC IMPROVEMENT SPARKED A SUSTAINED MARKET RALLY.

During the early part of the period, the stock market continued to
tumble, reaching levels in March that had last been seen in However,
early signs of potential economic improvement began to emerge in the spring,
sparking the strongest rally since The market continued to advance through
summer and into fall, with seven consecutive months of positive returns for the
S&P Index--its second-longest stretch in a decade. By the end of the period,
volatility had begun to climb again on questions about the sustainability of the
economic improvement. That resulted in a slight correction in the equity markets
during October

As the period began, fallout from the credit crisis continued amid mounting
fears that the economy had fallen into recession. Volatility spiked across the
financial markets in fall after Lehman Brothers filed for bankruptcy and
the government enacted emergency measures to rescue AIG, Fannie Mae, Freddie
Mac, and other large financial institutions. Volatility rose to unprecedented
heights as the global financial system froze up and investors feared a total
economic collapse. Concerns about potential nationalization of the banking
system kept volatility elevated early in but the release of bank stress
tests in May helped to alleviate those concerns, and volatility declined
steadily as the market rallied strongly until the final month of the period.

ECONOMIC GROWTH TURNED POSITIVE AFTER A SEVERE CONTRACTION.

Positive economic growth resumed in the third quarter of following the
first string of four consecutive quarters of negative economic growth in at
least years. Gross Domestic Product growth was for the third quarter of
strongest since Economic consensus was that the recession had
likely ended during the second quarter of However, with much of the growth
attributable to government stimulus programs, questions remained over the
potential sustainability of the recovery. By the end of the period, the
National Bureau of Economic Research had not declared an official end to the
recession.

UNEMPLOYMENT REMAINED HIGH, BUT OTHER ECONOMIC INDICATORS IMPROVED.

The unemployment rate rose throughout the period, reaching in October
highest level in more than years. However, the pace of job losses
had slowed as the period came to a close. Payroll employment declined an average
of jobs during the last three months of the period, well below



Wells Fargo Advantage Small and Mid Cap Stock Funds


Letter to Shareholders

the average from earlier in Still, since the start of the
recession in December more than million jobs had been lost by the end of
the period.

Although unemployment remained high, other economic statistics began to show
signs of improvement. Industrial production, manufacturing, and consumer
sentiment had all improved significantly as the period came to a close. Retail
sales improved in the latter months of the period, helped in part by the "Cash
for Clunkers" program that temporarily boosted auto sales. Home sales and prices
also seemed to have stabilized and began to show signs of improvement in many
areas of the country, spurred in part by the government's tax credit for
first-time home buyers.

THE FED CONTINUED TO FOCUS ON THE ECONOMIC STIMULUS PROGRAMS.

Despite extensive economic stimulus measures by the Federal Reserve, bank
lending did not expand during the period. This limited access to credit
indicates that the trillions of dollars that were added to the monetary system
through the government stimulus programs might not have an inflationary impact
in the near term. In the second half of the period, debate began to escalate
over the need for the Fed to outline an "exit strategy" from its stimulus
programs. Despite that debate, however, the Federal Open Market Committee (FOMC)
held the federal funds rate at the range of to that it first targeted
in December The Fed concluded its purchases of longer-term U.S. Treasuries
in October but continued to buy mortgage-backed securities, with that
program slated to end in March In its final statement during the
period, the FOMC noted the signs of economic improvement but reiterated that it
was likely to keep the federal funds rate at exceptionally low levels for an
extended period because the economy remains sluggish.

EQUITY MARKETS EXPERIENCED EXTREME VOLATILITY BUT REBOUNDED IN MARCH

For the period, the equity markets can be split into two distinct
sub-periods. From the beginning of the period until early March stocks
declined sharply after falling steadily during the first half of The broad
market, as measured by the S&P Index, began the period in the wake of a
nearly decline in October worst month since October The
declines continued through March when the market reversed abruptly amid talk
that several large banks would report stronger-than-expected earnings during the
first quarter. Those reports helped dispel investor fears of potential bank
nationalization and sparked the biggest rally in the equity markets since
with the S&P Index surging in trading days. That rally moderated but
persisted throughout the summer and into fall with the S&P Index
gaining from the March low through September In the final month of
the period, volatility began to increase again, resulting in a slight
correction in the equity markets. For October the S&P Index declined
about

For the full period, the S&P Index gained and the Dow Jones
Industrial Average rose Among domestic equity indices, the tech-heavy
NASDAQ Composite Index was the leader, with a return over the

FOR THE FULL PERIOD, THE S&P INDEX GAINED AND THE DOW JONES
INDUSTRIAL AVERAGE ROSE


Wells Fargo Advantage Small and Mid Cap Stock Funds


Letter to Shareholders

period due to the strength of the rebound in technology stocks in The
Russell Midcap(R) gained while the Russell of
small cap stocks advanced

Over the period, mid cap stocks led in performance, while large cap and small
cap stocks had nearly equal returns. Market cap leadership was split, however,
between the early and later months of the period. Large cap stocks led during
the early months as investors rotated into larger, more stable companies during
the market decline in late and early Small caps outperformed large
caps during the subsequent rally, however, as investors' risk appetite returned.
During the period, the growth investment style significantly
outperformed the value investment style across all market capitalizations, as
measured by the Russell indices, benefiting from the growth indices' significant
exposure to the information technology sector and lower exposure to the
financials sector.

DON'T LET SHORT-TERM VOLATILITY DERAIL LONG-TERM INVESTMENT GOALS.

The period that ended October was challenging for investors
but brought a welcome reprieve from the volatility of The rapid rebound in
market performance that occurred in early underscores the importance of
maintaining focus on long-term investment goals through periods of volatility so
as not to miss potential periods of strong recovery. Although periods of
volatility can present challenges, experience has taught us that maintaining a
long-term investment strategy based on individual goals and risk tolerance can
be an effective way to plan for the future.

As a whole, WELLS FARGO ADVANTAGE FUNDS(R) represents investments across a broad
range of asset classes and investment styles, giving you an opportunity to
create a diversified investment portfolio. Although diversification may not
prevent losses in a downturn, it may help to reduce them and provide you with
one way of managing risk. Our diverse family of mutual funds may also help. We
offer more than Funds that cover a broad spectrum of investment styles and
asset classes.

Thank you for choosing WELLS FARGO ADVANTAGE FUNDS. We appreciate your
confidence in us. Through each market cycle, we are committed to helping you
meet your financial needs. If you have any questions about your investment,
please contact your investment professional or call us at You
may also want to visit our Web site at www.wellsfargo.com/advantagefunds.

Sincerely,
