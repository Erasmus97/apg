Dear Shareholder: 
We are pleased to provide this shareholder report detailing your fund’s performance, portfolio holdings and financial statements. We hope this
information is helpful in monitoring your investments as we work through these challenging economic times. We recognize that you have entrusted us with your money and want you to know that our professional investment teams work to interpret the
latest economic and market trends with the goal of optimizing portfolio construction for our clients. 
The first half of 2009 was defined by
extremes. The multiyear lows we witnessed in the early months gave way to a stunning rally for the U.S. financial markets into the fourth quarter of 2009. A global market rebound may be underway, thanks to the massive fiscal and aggressive monetary
policies of governments around the world. The S&amp;P 500 Index
Retirement income planning has become an increasingly significant focus in
the lives of millions of Americans. Recent economic conditions make it even more important to manage short-term obligations such as mortgages, monthly bills and credit card debt while also taking the steps necessary to prepare for or maximize
retirement benefits. Better nutrition and medical services can result in U.S. citizens living longer, healthier lives. This means the risk of outliving one’s assets in retirement is very real without proper planning. Financial security and
retirement planning is an ongoing process that requires active management of your savings, investments and risks. We encourage you to review your retirement plan regularly so you’ll be better able to meet your retirement needs in the future.

We recognize that economic uncertainty creates great challenges for many investors. Our professional investment teams work diligently to help
investors navigate through difficult markets. Thank you for your business and for the opportunity to work together towards your investment goals. 
Sincerely, 


