
Dear Shareholder:



Over the last 12 months, investors have endured what is perhaps best described as a Tale of Two Markets.  The first market occurred in the first three months of the year, as the global financial crisis that began in 2008 persisted. Despite a $787 billion federal stimulus package, the U.S. economy, like other developed markets receiving governmental stimulus, continued to struggle. Unemployment rose, banks failed at an alarming rate and the federal deficit soared. By March 9, 2009, U.S. equities had fallen to their lowest levels since 1997. Many thousands of Americans had watched their savings evaporate, their investment portfolios shrink, and many lost their homes.


The second market emerged later that same month, when several stimulus programs were implemented, including a $75 billion housing rescue plan, the Federal Reserve Bank's plan to buy long-term Treasury securities and mortgage-backed securities and the Treasury decision to buy toxic real estate assets from banks. These aggressive efforts, and similar monetary and fiscal policies being implemented in foreign developed markets, did much to deter the global financial and economic meltdown that had just months earlier been feared inevitable.


Despite a steady stream of relatively weak economic and earnings data, investors began to demonstrate a return of some appetite for risk. In early spring, a rise in existing home sales suggested that the housing market may have found its floor. In the third quarter, gross domestic product expanded at a 2.8 percent annualized rate. Progress continued into the fourth quarter, despite a minor stumble in November when the Dubai World conglomerate in the Persian Gulf asked creditors for a suspension of interest payments on $60 billion of its $80 billion in debt. The year ended with all the major indexes posting double-digit returns, and some posting record single-year gains. Specifically, the S&amp;P 500 Index gained 26.47 percent for the year ended December 31, 2009. Fixed-income markets, as measured by the Citigroup Broad Investment Grade Index, did well, increasing 5.07 percent for the year.


Although numerous problems remain, we are optimistic that better days are ahead.




On the pages that follow, Michael L. Avery and Ryan F. Caldwell, portfolio managers of the Waddell &amp; Reed InvestEd Portfolios, discuss positioning, performance and results for the fiscal year ended December 31, 2009. Mr. Avery has 31 years of industry experience and has managed the Portfolios since inception. Mr. Caldwell joined the team in January 2007 and our investment management organization in 2000. He has 12 years of industry experience.


Michael L. Avery



Because each Portfolio typically invests in a variety of mutual funds that invest in multiple asset classes, we compare each Portfolio's results to three benchmark indexes, as shown below. The S&amp;P 500 Index represents large- and medium-sized U.S. stocks. The Citigroup Broad Investment Grade Index represents bonds with ratings of BBB or higher. The Citigroup Short-Term Index for 1 Month Certificates of Deposit represents cash.


During the fiscal year we made modifications to all three InvestEd Portfolios in response to the changing market environment. The most significant change occurred in the Conservative Portfolio, where we sold approximately 90 percent of the Portfolio's Waddell &amp; Reed Advisors Cash Management position and reallocated the bulk of the proceeds to the Waddell &amp; Reed Advisors Core Investment and Waddell &amp; Reed Advisors Government Securities Funds, with a smaller allocation into Waddell &amp; Reed Advisors Bond Fund. We did this in an effort to better position the Conservative Portfolio to benefit from the market rally that began near the end of the first quarter of the year - a rally that ultimately persevered throughout the remainder of 2009 and significantly boosted the Conservative Portfolio returns.


The most significant change we made to the Growth Portfolio was selling its entire Waddell &amp; Reed Advisors Cash Management position and using the proceeds to more than double the Portfolio's stake in Waddell &amp; Reed Advisors International Growth Fund. We did this in an effort to capitalize on the tremendous growth underway in a number of the developed foreign markets that Fund pursues. We took similar action - and for the same reason - with respect to the Balanced Portfolio, taking the bulk of its Waddell &amp; Reed Advisors Cash Management exposure (which represented nearly a quarter of the Portfolio's assets in the first quarter) and redeploying those assets into Waddell &amp; Reed Advisors International Growth Fund and Waddell &amp; Reed Advisors Vanguard Fund.


At the time of our last annual report to you, the global financial system tottered on the edge of collapse as the burgeoning credit crisis in the United States served as the catalyst for the greatest worldwide recession since the 1930s. The malaise continued into 2009, as America's balance-sheet recession deepened, home prices plummeted and unemployment spiraled. On March 6, 2009, the Dow Jones Industrial Average broke below 6500 and the S&amp;P 500 Index declined to 666.79, falling another 25 percent from the end of 2008 and recording losses of more than 50 percent from their 2007 peaks. This ultimately proved to be the turning point.


Since then, due to massive stimulus implemented by governments in many developed economies, markets rallied impressively. Investment banks, particularly those that had received TARP assistance, began predicting a positive first quarter, which in turn sparked a reversal in investor sentiment. Investors once again discovered their appetite for risk and demonstrated their preference for lower-quality or less financially secure businesses, driving a low-quality rally that ultimately drove up the prices of those businesses that had declined the most in the credit crisis. In April, all capitalization segments of stocks recorded double-digit gains, and eight of the nine market sectors in the broad-based Russell 3000 Index were in positive territory. By May, news of improving earnings and better consumer sentiment about the global economic outlook bolstered equities, and the S&amp;P 500 Index completed its third consecutive month of gains. Global stocks fared even better due to renewed strength in developed markets 
and a powerful rise in emerging markets. The upward trajectory continued through the summer months.


During the third quarter of 2009, and for the first time in more than a year, U.S. gross domestic product registered growth, and the final quarter of 2009 saw improvements in rising home sales, better retail sales, slightly less negative employment trends and continued low interest rates. Manufacturing indices from Europe to Asia showed positive momentum, reflecting a potentially broad manufacturing recovery. Auto sales in places such as China surged as a burgeoning middle class there demonstrated its growing buying power. Strong investment in infrastructure also recovered, as did prices of commodities that support infrastructure build out.


The U.S. stock market's breathtaking decline and subsequent recovery were mirrored by the bond market, which staged its own roller coaster ride. When 2009 opened, the credit markets were virtually frozen and investors sought the relative safety of U.S. Treasuries, ultimately driving the yield curve to nearly unprecedented levels. As the year progressed and investors' risk tolerance returned to more normal levels, demand for corporate bonds increased, driving their prices higher and weighing on yields. During the 12-month fiscal period, both taxable and municipal bonds returned more than 13 percent. At the same time, low interest rates had a negative effect on short-term instruments, such as money market funds.


As 2009 wound to a close, stock indices hit new recovery highs; the S&amp;P 500 Index surged more than 65 percent from its March 2009 lows to record a 26.47 percent gain. Many global markets did even better; the MSCI EAFE Index ended the year with an annual gain of more than 32 percent. The Hang Seng Hong Kong Index returned 53.94 percent, while MSCI's emerging market index surged nearly 110 percent since early March.


We believe 2010 will likely be a volatile year for investors, due largely to concerns about sovereign credit risk. What we went through 18 months ago was a crisis of confidence; confidence may well have been restored, but a tremendous debt is still there, it's just been absorbed by governments that injected massive fiscal stimulus to create liquidity. We very well may encounter an environment in which volatility is punctuated by concern about what policy makers are going to do and what outcomes that may drive.


One of the significant themes we're anticipating in the months ahead is a transition from a liquidity-driven market to a "stock picker's" market. For that reason, we are less concerned with where a stock is domiciled - its geographical location - than we are in identifying what we feel are the steady-growth companies that fit our criteria for sustainable competitive advantage. We believe the way to succeed in 2010 is to spend less time on the macro view and more time on micro analysis and individual security selection. This plays very well into the investment theme that we have had in place for some time - our belief in the opportunities presented by the emerging middle class as the global economy rebalances.


As college costs continue to rise, most students and their families can expect to pay, on average, from $172 to $1,096 more than last year for this year's tuition and fees, depending on the type of college or higher-education institution. That's according to recently released reports from the College Board, a not-for-profit association that provides programs and services to college-bound students. Unfortunately, given the economic downturn, student debt loads are increasing too, while the high unemployment rate has many recent grads unable to find positions in their chosen fields. Sadly, fewer than 1 in 5 students in the class of 2009 had a job upon graduation. This discouraging environment may leave some parents and students wondering if attaining that degree is worth the investment of time, energy and effort.


The rising costs of college present additional challenges for families during periods of recession. But as money managers, we wish to stress the importance of maintaining your focus on your long-term goals, despite current market and economic conditions, and not let a short-term setback deter your progress toward the goals you've set to fund your student's higher education, so that he or she can in turn work to achieve their full potential and promise. In the year ahead, we will be especially mindful that the realization of your dreams depends in part on our successful management of your assets.


As with any mutual fund, the value of each Portfolio's shares will change, and you could lose money on your investment. These and other risks are more fully described in the Portfolios' prospectus.


The opinions expressed in this report are those of the portfolio managers and are current only through the end of the period of the report as stated on the cover. The managers' views are subject to change at any time based on market and other conditions, and no forecasts can be guaranteed.



 

							SHARES

							SHARES

							SHARES

							INDEX

							INDEX

							OF DEPOSIT
10/1/01
9,425
9,425
9,425
10,000
10,000
10,000
12/31/01
10,239
9,991
9,640
11,068
10,002
10,058
12/31/02
8,812
9,147
9,673
8,622
11,011
10,238
12/31/03
10,796
10,601
10,067
11,098
11,474
10,361
12/31/04
11,874
11,401
10,360
12,306
11,987
10,508
12/31/05
12,941
12,200
10,621
12,911
12,295
10,857
12/31/06
14,536
13,551
11,271
14,951
12,828
11,416
12/31/07
16,340
15,020
11,964
15,772
13,754
12,033
12/31/08
12,112
12,240
12,150
9,936
14,719
12,400
12/31/09
15,135
14,237
12,954
12,566
15,465
12,440



As a shareholder of a Portfolio, you incur two types of costs: (1) transaction costs, including sales charges (loads) on purchase payments, redemption fees and exchange fees; and (2) ongoing costs, including management fees, distribution and service fees, and other Portfolio expenses. The following tables are intended to help you understand your ongoing costs (in dollars) of investing in a Portfolio and to compare these costs with the ongoing costs of investing in other mutual funds. As a shareholder in the underlying Waddell &amp; Reed Advisors Funds, your Portfolio will indirectly bear its pro rata share of the expenses incurred by the underlying funds. These expenses are not included in a Portfolio's annualized expense ratio or the expenses paid during the period. These expenses are, however, included in the effective expenses paid during the period. The example is based on an investment of $1,000 invested at the beginning of the period and held for the six-month period ended December 31, 2009.


The first line in the following tables provides information about actual account values and actual expenses. You may use the information in this line, together with the amount you invested, to estimate the expenses that you paid over the period. Simply divide your account value by $1,000 (for example, a $7,500 account value divided by $1,000 = 7.5), then multiply the result by the number in the first line under the heading entitled "Expenses Paid During Period" to estimate the expenses you paid on your account during this period. There may be additional fees charged to holders of certain accounts that are not included in the expenses shown in the table. As of the close of the six months covered by the tables, a customer is charged an initial fee of ten dollars for each new account. You should consider the additional fees that were charged to your Portfolio account over the six-month period when you estimate the total ongoing expenses paid over the period and the impact of these fees on your ending 
account value as such additional expenses are not reflected in the information provided in the expense table. Additional fees have the effect of reducing investment returns.

The second line in the following tables provides information about hypothetical account values and hypothetical expenses based on the Portfolio's actual expense ratio and an assumed rate of return of five percent per year before expenses, which is not the Portfolio's actual return. The hypothetical account values and expenses may not be used to estimate the actual ending account balance or expenses you paid for the period. You may use this information to compare the ongoing costs of investing in the Portfolio and other funds. To do so, compare this five percent hypothetical example with the five percent hypothetical examples that appear in the shareholder reports of the other funds.


