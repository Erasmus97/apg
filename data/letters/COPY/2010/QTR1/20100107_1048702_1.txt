Dear Shareholder,





During the past year, investors have experienced tumultuous
markets, the most severe economic recession in our generation
and a job market that has stressed many of us to our financial
and emotional limits.





The severe crisis has been met with equally aggressive action.
In an effort to reverse the seizure of the credit markets and
prevent a systemic collapse, the U.S.Treasury and the
Federal Reserve System have lent unprecedented support to the
financial system. This aid, in the form of access to capital,
bailouts and nonrecourse loans, was granted to many of the
institutions who arguably exploited the system in the buildup to
the crisis. The federal government also has endeavored to boost
economic activity through the economic stimulus package, which
has added significantly to the federal deficit.





The actions of many financial institutions and the remedies
employed by the federal government, while exhibiting
well-meaning intent during a period of intense crisis, have
resulted in widespread distrust of the financial system. The
current easy access to capital and the potential longer-term
issue of unprecedented deficits have caused the once
over-leveraged private economy to be replaced by public
financing. While we believe the worst of the crisis is over,
some uncertainty lingers. Inflation fears remain in check, but
the fundamentals of economic growth and job creation appear to
be weak, and threats still may imperil financial market
stability.





On I wrote to you ...the market has
shown some signs of cautious rebounding. In fact, the
stock market run has outstripped the expectations of all market
prognosticators. From through
the Standard


(SP rose more than Overselling during
the fourth quarter of and first quarter of provided a
portion of the catalyst for the double-digit positive stock
market gains. Better-than-expected corporate earnings,
inexpensive capital and an accommodating monetary policy may
provide additional explanation for the current exuberance. These
current events, however, do not appear quite so positive when
examined more closely. Corporate earnings have largely been
driven by (a)reductions in inventory achieved through
reductions in capacity and (b)productivity gains
accomplished by worker reductions and layoffs.





Our opinion is that amid the recent good news, there is cause
for caution. First and foremost, cheap capital has led the
markets straight back to risk-taking behavior. As a society, we
havent displayed greater financial literacy and dont
appear to have learned any financial lessons well. The dichotomy
of the very recent stock market euphoria versus the financial
stress and uncertainty endured by individual investors is
unsettling.





We exist to serve the long-term interests of our investors. At
no time in our collective memories has the future direction of
the economy and the financial markets been so unclear. Our
reaction to this environment is to remain vigilant against
anticipated and unanticipated risks, stay focused on the long
term, and continue to select and monitor investment managers of
the highest quality. We remain committed to the underlying
investing principles of prudent asset allocation and
diversification that provide optimal risk-adjusted returns over
the long term.





Thank you for entrusting your investments to Nationwide Mutual
Funds.





Sincerely,

Dear Shareholder,





During the past year, investors have experienced tumultuous
markets, the most severe economic recession in our generation
and a job market that has stressed many of us to our financial
and emotional limits.





The severe crisis has been met with equally aggressive action.
In an effort to reverse the seizure of the credit markets and
prevent a systemic collapse, the U.S.Treasury and the
Federal Reserve System have lent unprecedented support to the
financial system. This aid, in the form of access to capital,
bailouts and nonrecourse loans, was granted to many of the
institutions who arguably exploited the system in the buildup to
the crisis. The federal government also has endeavored to boost
economic activity through the economic stimulus package, which
has added significantly to the federal deficit.





The actions of many financial institutions and the remedies
employed by the federal government, while exhibiting
well-meaning intent during a period of intense crisis, have
resulted in widespread distrust of the financial system. The
current easy access to capital and the potential longer-term
issue of unprecedented deficits have caused the once
over-leveraged private economy to be replaced by public
financing. While we believe the worst of the crisis is over,
some uncertainty lingers. Inflation fears remain in check, but
the fundamentals of economic growth and job creation appear to
be weak, and threats still may imperil financial market
stability.





On I wrote to you ...the market has
shown some signs of cautious rebounding. In fact, the
stock market run has outstripped the expectations of all market
prognosticators. From through
the Standard


(SP rose more than Overselling during
the fourth quarter of and first quarter of provided a
portion of the catalyst for the double-digit positive stock
market gains. Better-than-expected corporate earnings,
inexpensive capital and an accommodating monetary policy may
provide additional explanation for the current exuberance. These
current events, however, do not appear quite so positive when
examined more closely. Corporate earnings have largely been
driven by (a)reductions in inventory achieved through
reductions in capacity and (b)productivity gains
accomplished by worker reductions and layoffs.





Our opinion is that amid the recent good news, there is cause
for caution. First and foremost, cheap capital has led the
markets straight back to risk-taking behavior. As a society, we
havent displayed greater financial literacy and dont
appear to have learned any financial lessons well. The dichotomy
of the very recent stock market euphoria versus the financial
stress and uncertainty endured by individual investors is
unsettling.





We exist to serve the long-term interests of our investors. At
no time in our collective memories has the future direction of
the economy and the financial markets been so unclear. Our
reaction to this environment is to remain vigilant against
anticipated and unanticipated risks, stay focused on the long
term, and continue to select and monitor investment managers of
the highest quality. We remain committed to the underlying
investing principles of prudent asset allocation and
diversification that provide optimal risk-adjusted returns over
the long term.





Thank you for entrusting your investments to Nationwide Mutual
Funds.





Sincerely,

Dear Shareholder,





During the past year, investors have experienced tumultuous
markets, the most severe economic recession in our generation
and a job market that has stressed many of us to our financial
and emotional limits.





The severe crisis has been met with equally aggressive action.
In an effort to reverse the seizure of the credit markets and
prevent a systemic collapse, the U.S.Treasury and the
Federal Reserve System have lent unprecedented support to the
financial system. This aid, in the form of access to capital,
bailouts and nonrecourse loans, was granted to many of the
institutions who arguably exploited the system in the buildup to
the crisis. The federal government also has endeavored to boost
economic activity through the economic stimulus package, which
has added significantly to the federal deficit.





The actions of many financial institutions and the remedies
employed by the federal government, while exhibiting
well-meaning intent during a period of intense crisis, have
resulted in widespread distrust of the financial system. The
current easy access to capital and the potential longer-term
issue of unprecedented deficits have caused the once
over-leveraged private economy to be replaced by public
financing. While we believe the worst of the crisis is over,
some uncertainty lingers. Inflation fears remain in check, but
the fundamentals of economic growth and job creation appear to
be weak, and threats still may imperil financial market
stability.





On I wrote to you ...the market has
shown some signs of cautious rebounding. In fact, the
stock market run has outstripped the expectations of all market
prognosticators. From through
the Standard


(SP rose more than Overselling during
the fourth quarter of and first quarter of provided a
portion of the catalyst for the double-digit positive stock
market gains. Better-than-expected corporate earnings,
inexpensive capital and an accommodating monetary policy may
provide additional explanation for the current exuberance. These
current events, however, do not appear quite so positive when
examined more closely. Corporate earnings have largely been
driven by (a)reductions in inventory achieved through
reductions in capacity and (b)productivity gains
accomplished by worker reductions and layoffs.





Our opinion is that amid the recent good news, there is cause
for caution. First and foremost, cheap capital has led the
markets straight back to risk-taking behavior. As a society, we
havent displayed greater financial literacy and dont
appear to have learned any financial lessons well. The dichotomy
of the very recent stock market euphoria versus the financial
stress and uncertainty endured by individual investors is
unsettling.





We exist to serve the long-term interests of our investors. At
no time in our collective memories has the future direction of
the economy and the financial markets been so unclear. Our
reaction to this environment is to remain vigilant against
anticipated and unanticipated risks, stay focused on the long
term, and continue to select and monitor investment managers of
the highest quality. We remain committed to the underlying
investing principles of prudent asset allocation and
diversification that provide optimal risk-adjusted returns over
the long term.





Thank you for entrusting your investments to Nationwide Mutual
Funds.





Sincerely,

Dear Shareholder,





During the past year, investors have experienced tumultuous
markets, the most severe economic recession in our generation
and a job market that has stressed many of us to our financial
and emotional limits.





The severe crisis has been met with equally aggressive action.
In an effort to reverse the seizure of the credit markets and
prevent a systemic collapse, the U.S.Treasury and the
Federal Reserve System have lent unprecedented support to the
financial system. This aid, in the form of access to capital,
bailouts and nonrecourse loans, was granted to many of the
institutions who arguably exploited the system in the buildup to
the crisis. The federal government also has endeavored to boost
economic activity through the economic stimulus package, which
has added significantly to the federal deficit.





The actions of many financial institutions and the remedies
employed by the federal government, while exhibiting
well-meaning intent during a period of intense crisis, have
resulted in widespread distrust of the financial system. The
current easy access to capital and the potential longer-term
issue of unprecedented deficits have caused the once
over-leveraged private economy to be replaced by public
financing. While we believe the worst of the crisis is over,
some uncertainty lingers. Inflation fears remain in check, but
the fundamentals of economic growth and job creation appear to
be weak, and threats still may imperil financial market
stability.





On I wrote to you ...the market has
shown some signs of cautious rebounding. In fact, the
stock market run has outstripped the expectations of all market
prognosticators. From through
the Standard


(SP Index rose more than Overselling during the
fourth quarter of and first quarter of provided a
portion of the catalyst for the double-digit positive stock
market gains. Better-than-expected corporate earnings,
inexpensive capital and an accommodating monetary policy may
provide additional explanation for the current exuberance. These
current events, however, do not appear quite so positive when
examined more closely. Corporate earnings have largely been
driven by (a)reductions in inventory achieved through
reductions in capacity and (b)productivity gains
accomplished by worker reductions and layoffs.





Our opinion is that amid the recent good news, there is cause
for caution. First and foremost, cheap capital has led the
markets straight back to risk-taking behavior. As a society, we
havent displayed greater financial literacy and dont
appear to have learned any financial lessons well. The dichotomy
of the very recent stock market euphoria versus the financial
stress and uncertainty endured by individual investors is
unsettling.





We exist to serve the long-term interests of our investors. At
no time in our collective memories has the future direction of
the economy and the financial markets been so unclear. Our
reaction to this environment is to remain vigilant against
anticipated and unanticipated risks, stay focused on the long
term, and continue to select and monitor investment managers of
the highest quality. We remain committed to the underlying
investing principles of prudent asset allocation and
diversification that provide optimal risk-adjusted returns over
the long term.





Thank you for entrusting your investments to Nationwide Mutual
Funds.





Sincerely,

Dear Shareholder,





During the past year, investors have experienced tumultuous
markets, the most severe economic recession in our generation
and a job market that has stressed many of us to our financial
and emotional limits.





The severe crisis has been met with equally aggressive action.
In an effort to reverse the seizure of the credit markets and
prevent a systemic collapse, the U.S.Treasury and the
Federal Reserve System have lent unprecedented support to the
financial system. This aid, in the form of access to capital,
bailouts and nonrecourse loans, was granted to many of the
institutions who arguably exploited the system in the buildup to
the crisis. The federal government also has endeavored to boost
economic activity through the economic stimulus package, which
has added significantly to the federal deficit.





The actions of many financial institutions and the remedies
employed by the federal government, while exhibiting
well-meaning intent during a period of intense crisis, have
resulted in widespread distrust of the financial system. The
current easy access to capital and the potential longer-term
issue of unprecedented deficits have caused the once
over-leveraged private economy to be replaced by public
financing. While we believe the worst of the crisis is over,
some uncertainty lingers. Inflation fears remain in check, but
the fundamentals of economic growth and job creation appear to
be weak, and threats still may imperil financial market
stability.





On I wrote to you ...the market has
shown some signs of cautious rebounding. In fact, the
stock market run has outstripped the expectations of all market
prognosticators. From through
the Standard


(SP Index rose more than Overselling during the
fourth quarter of and first quarter of provided a
portion of the catalyst for the double-digit positive stock
market gains. Better-than-expected corporate earnings,
inexpensive capital and an accommodating monetary policy may
provide additional explanation for the current exuberance. These
current events, however, do not appear quite so positive when
examined more closely. Corporate earnings have largely been
driven by (a)reductions in inventory achieved through
reductions in capacity and (b)productivity gains
accomplished by worker reductions and layoffs.





Our opinion is that amid the recent good news, there is cause
for caution. First and foremost, cheap capital has led the
markets straight back to risk-taking behavior. As a society, we
havent displayed greater financial literacy and dont
appear to have learned any financial lessons well. The dichotomy
of the very recent stock market euphoria versus the financial
stress and uncertainty endured by individual investors is
unsettling.





We exist to serve the long-term interests of our investors. At
no time in our collective memories has the future direction of
the economy and the financial markets been so unclear. Our
reaction to this environment is to remain vigilant against
anticipated and unanticipated risks, stay focused on the long
term, and continue to select and monitor investment managers of
the highest quality. We remain committed to the underlying
investing principles of prudent asset allocation and
diversification that provide optimal risk-adjusted returns over
the long term.





Thank you for entrusting your investments to Nationwide Mutual
Funds.





Sincerely,
