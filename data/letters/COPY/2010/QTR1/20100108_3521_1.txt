Dear
  Shareholders,
November
  30, 2009
 
Year-in-Review
 
At Alger, we are
optimistic that the unprecedented turmoil that began roughly a year ago in September
2008 has ended, although we believe there may be some “pause” in the dramatic
rally that began in March 2009. We think it will be, in 2010, a pause that
serves to “refresh”. Ultimately, we believe a new bull market has begun, and
one that will play out over several years.
 
Since March 2009, equity
performance worldwide has been remarkable amid stronger-than-expected corporate
earnings and signs of economic stabilization across the globe. For the one year
period ending 10/31/09, the S&amp;P 500 Index delivered 9.79%. The Russell
1000, Russell Midcap and Russell 2000 Indexes each returned 6.46%, 18.75%, and
11.20%, respectively, with midcap stocks outperforming small and large cap
stocks. Growth stocks significantly outperformed value as indicated by the
Russell 1000 Growth and Value Indexes, which returned 17.52% and 4.78%,
respectively. On the international front, European equity markets gained more
than 23.04% (MSCI Europe) while emerging equity markets (MSCI EMF) rose an
astounding 64.63%. In addition to positive performance by equity markets around
the globe, credit markets showed some of the strongest performance on record
with the U.S. High Yield market (Barclays Capital U.S. High Yield Index)
returning 30.26% for the 1-year period ended 10/31/09. The investment grade
credit market (Barclays Capital U.S. Credit Index) also showed a significant
1-Year return, gaining nearly 27.58%, over the same time period. Thus, in the
broader context of financial markets, U.S. equities have trailed their credit
and international counterparts as U.S. companies grapple with an uneven period
of financial crisis and economic paralysis. 
 
We believe the recession
in the U.S. largely ended in third quarter of 2009, but a full economic recovery
in the U.S. is far from complete. The various government programs and stimulus
introduced over the past several months, along with the amount of liquidity
provided by the U.S. Federal Reserve and other central banks around the world,
have succeeded in taking the worst case scenario of a total financial system
collapse off the table. This success is evident, in our opinion, by the
improving economic activity throughout the world, not to mention better
functioning capital and funding markets. For the U.S., it is likely that the
broadest measures of our economy will support our view at Alger that, near
term, a stronger than expected recovery continues into early 2010. However, we
expect at some point that the ability of the U.S. economy to leverage the momentum
of recovery with “true” growth in 2010 and beyond will be a source of anxiety
for U.S. markets. The consensus economic view appears to be that the U.S. will
experience a long period of subdued expansion. We, too, believe weakness in the
job market is likely to persist and will make whatever economic conditions the
U.S. experiences “feel” recessionary for many Americans well into 2010.
 
U.S. unemployment is
approximately 10%, its highest level in nearly 26 years, and discouraged and
part-time workers (those who have either temporarily given up looking for work
or have part-time jobs but want full-time work) likely place the true
 
 
unemployment rate closer
to 17%. While unemployment does have an adverse impact on consumer sentiment
and spending, it has historically been a lagging indicator that typically
improves after the overall economy improves. Therefore, we believe unemployment
is likely to remain stagnant in 2010, even as other areas of the economy continue
to show signs of improvement. This pattern is typical of post-recessionary
periods of recovery, and we see no reason it won’t hold true today.
 
As we look back over the
past year, it is very clear that Corporate America responded to this downturn more
rapidly and with more decisiveness than past recessions. Quite likely, this
rapid response to deteriorating business conditions, as well as financial
markets, was assisted by the widespread adoption of “just in time” inventory
management, sophisticated customer demand prediction and analysis software and
methodologies, and the development of flexible, outsourced supply chain
management by Corporate America over the last two decades. The result this past
year was very aggressive expense cutting, headcount reductions, and capital
expenditure deferrals (and cancellations) as we moved through the financial
crisis and this recession. This was evident in second quarter earnings reports
in July, and repeated recently in October, in the surprisingly strong margins,
earnings and cash flows within Corporate America (represented by the S&amp;P
500 and publicly traded companies’ profit performance as tracked by Alger’s
analysts), even as revenues were often weaker than expected (especially in the
second quarter) or remain significantly lower than prior year levels
(especially in the third quarter).
 
Looking
Ahead
 
Companies now face
different decisions as they look to the future — many are already rehiring
employees or restarting capital expenditures or deferred investments in their
businesses. While some companies will wait for more clarity on economic
conditions before increasing investments, others, particularly those
participating in markets that have strong long term growth outlooks, will
recognize in today’s markets an opportunity to gain market share or enter new
markets when costs are lower, and perhaps competition is less than it was
(e.g., due to the elimination of financially weaker players). Many of the best
known, most successful businesses today were started in the midst of economic
recessions for this very reason.
 
Moreover, it is in the
midst of wrenching change within an economy or industry, that opportunity is
created for new business models, new ways of doing things, new services and
goods that attract a customer with a “changed” perception of need, want and
value. We are, clearly, in the midst of such a period. Indeed, the news today
is often only tangentially about “the facts” of an event, and much more a
debate in our newspapers and on TV, in the halls of government and around the
Thanksgiving dinner table, about what are the new values of our society —
represented both by what we believe and what we do both as citizens and
consumers and, yes, capitalists.
 
Change is inevitable.
Companies — even those with long-standing track records — are not static. Truly
successful companies are run by dynamic people who can adapt, change, and grow.
This also holds true for the U.S. economy, which has thrived because of its
ability to adapt to the positive dynamic changes that are occurring globally.
When change happens, some investors panic, while others turn a blind eye,
 
 
whereas others, like
ourselves, accept and embrace change. At Alger, change equals opportunity. We
believe that when change occurs — whether it is in a company, sector, industry
or the economy - the best investment opportunities emerge and we seek to
quickly capture them. This is exactly what we have done over the last year as
the market created so many attractive opportunities at very reasonable
valuations. The current environment continues to provide us with strong
opportunities demonstrating the benefits of our rigorous, original and
fundamental research approach in uncovering companies with the strongest
fundamentals and the ability to leverage change strategically.
 
We think the key to
success going forward will be dependent largely on identifying companies
undergoing Positive Dynamic Change, which we believe offer the best long-term
growth potential for our clients. We have been utilizing this approach since
1964 and it has been the driving force behind our investment philosophy.
 
At Alger, we are bullish
on the future of U.S. equity investing. However, we think the stock markets
will, and probably should, pause in their rally since March 2009 — and that
such a period of consolidation is likely in 2010. Economic uncertainty is high,
and that alone should give investors reason to doubt in 2010. We have already
seen in 2009 that the vast majority of investment flows in the mutual fund
industry has been into cash, short term bond funds, and international
(especially emerging market equities), and out of U.S. equities generally.
However, publicly traded U.S. equities are not a simple representation of the
U.S. economy; in fact, they represent an elite subset of Corporate America. In
general, they are the leaders in their industries — some by innovations long
ago, and others by innovations so recent we have trouble remembering that they
didn’t exist a decade or two ago. They represent some of the most astute global
competitors, and derive a significant percentage of their revenues and profits
from their success in international markets. Their fundamental operating
results so far in 2009 have significantly outperformed investor expectations
and led those of the broader U.S. economy. Moreover, we believe these companies
are likely to continue to improve as global economic recovery takes hold in
late 2010 and the years beyond. Thus, we believe 2010 will offer investors an
excellent period to “buy the dips” and for stock investing based on identifying
companies with superior growth long term and the financial and other strengths
to capitalize on their market opportunities globally.  As investors become comfortable with the
strength of the U.S. and global economic recovery, we believe U.S. equities,
particularly “growth” equities, will once again find strong favor, offering a
combination of fundamental performance, growth opportunities and sector
leadership, and attractive valuation within a global universe. Thus, while
credit and international stocks have led the first phase of this recovery in
financial markets, we think it is the natural course of the investment cycle to
return to U.S. equities in the years ahead.
 
 
Portfolio
Matters
 
Alger
Capital Appreciation Fund
 
The Alger Capital
Appreciation Fund returned 24.24% for the twelve months ended October 31, 2009,
compared to the Russell 3000 Growth Index return of 17.03%.
 
During the period, the
largest portfolio weightings in the Alger Capital Appreciation Fund were in the
Information Technology and Health Care sectors. The largest sector overweight
for the period was in Financials. The largest sector underweight for the period
was in Industrials. Relative outperformance in the Information Technology and
Health Care sectors were the most important contributors to performance.
Sectors that detracted from the portfolio included Consumer Discretionary and
Consumer Staples.
 
Among the most important
relative contributors were Apple Inc., Chesapeake Energy Corp., Expedia Inc.,
Tyco International Ltd., and Transocean Ltd. Conversely, detracting from
overall results on a relative basis were Satyam Computer Services Ltd. (ADS),
Gildan Activewear Inc. (-73 bps), Activision Blizzard Inc. , Intel Corp. , and
Skyworks Solutions Inc.
 
Alger
LargeCap Growth Fund
 
The Alger LargeCap Growth
Fund returned 18.54% for the twelve-months ended October 31, 2009, compared
with a return of 17.52% for the Russell 1000 Growth Index.
 
During the period, the
largest portfolio weightings in the Alger LargeCap Growth Fund were in the
Information Technology and Health Care sectors. The largest sector overweight
for the period was in Financials. The largest sector underweight for the period
was in Industrials. Relative outperformance in the Energy and Health Care
sectors were the most important contributors to performance. Sectors that
detracted from the portfolio included Industrials and Materials.
 
Among the most important
relative contributors were Wyeth, Weatherford International Ltd., Tyco
International Ltd., Cognizant Technology Solutions Corp., and Chesapeake Energy
Corp.. Conversely, detracting from overall results on a relative basis were McDermott
International Inc., General Electric Co., Freeport-McMoRan Copper &amp; Gold
Inc., Devon Energy Corp., and JPMorgan Chase &amp; Co.
 
Alger
MidCap Growth Fund
 
For the twelve-months
ended October 31, 2009, the Alger MidCap Growth Fund returned 22.95% compared
to the Russell MidCap Growth Index, which had a return of 22.49%.
 
During the period, the
largest portfolio weightings in the Alger Midcap Growth Fund were in the
Information Technology and Health Care sectors. The largest sector overweight
for the period was in Information Technology. The largest sector underweight
for the period was in Industrials. Relative outperformance in the Financials
and Energy sectors were the most important contributors to performance.
 
 
Sectors that detracted
from the portfolio included Industrials and Information Technology.
 
Among the most important
relative contributors were Expedia Inc., Apple Inc., Whole Foods Market Inc.,
Las Vegas Sands Corp., and Chico’s FAS Inc. Conversely, detracting from overall
results on a relative basis were Satyam Computer Services Ltd. (ADS), McDermott
International Inc., Gildan Activewear Inc., United Therapeutics Corp., and
Tessera Technologies Inc.
 
Alger
SMidCap Growth Fund
 
The Alger SMidCap Growth
Fund returned 19.03% for the twelve-months ended October 31, 2009, compared to
the Russell 2500 Growth Index return of 18.19%. 
 
During the period, the
largest portfolio weightings in the Alger Smidcap Growth Fund were in the
Health Care and Information Technology sectors. The largest sector overweight
for the period was in Financials. The largest sector underweight for the period
was in Industrials. Relative outperformance in the Industrials and Information
Technology sectors were the most important contributors to performance. Sectors
that detracted from the portfolio included Health Care and Consumer Staples.
 
Among the most important
relative contributors were Marvell Technology Group Ltd., Mylan Inc., Expedia
Inc., Darden Restaurants Inc., and Mellanox Technologies Ltd. Conversely,
detracting from overall results on a relative basis were Gildan Activewear
Inc., Tenet Healthcare Corp., Satyam Computer Services Ltd. (ADS), Microsemi
Corp., and NCR Corp.
 
Alger
SmallCap Growth Fund
 
For the twelve-months
ending October 31, 2009, the Alger SmallCap Growth Fund returned 20.60%
compared to the Russell 2000 Growth Index, which returned 11.34%.
 
During the period, the
largest portfolio weightings in the Alger Smallcap Growth Fund were in the
Information Technology and Health Care sectors. The largest sector overweight
for the period was in Information Technology. The largest sector underweight
for the period was in Industrials. Relative outperformance in the Information
Technology and Industrials sectors were the most important contributors to
performance. The only sector that detracted from the portfolio was Consumer
Discretionary.
 
Among the most important
relative contributors were Vistaprint N.V., AECOM Technology Corp.,
priceline.com Inc., Mellanox Technologies Ltd., and URS Corp. Conversely,
detracting from overall results on a relative basis were Tenet Healthcare
Corp., AnnTaylor Stores Corp., Tessera Technologies Inc., International Coal
Group Inc., and Microsemi Corp.
 
 
The
Alger Growth Opportunities Fund
 
The Alger Growth
Opportunities Fund returned 21.22% for the twelve-months ended October 31,
2009, compared to the Russell 2500 Growth Index return of 18.19%.
 
During the period, the
largest portfolio weightings in the Alger Growth Opportunities Fund were in the
Information Technology and Health Care sectors. The largest sector overweight
for the period was in Information Technology. The largest sector underweight for
the period was in Industrials. Relative outperformance in the Information
Technology and Energy sectors were the most important contributors to
performance. Sectors that detracted from the portfolio included Consumer
Discretionary and Materials.
 
Among the most important
relative contributors were Vistaprint N.V., Optimer Pharmaceuticals Inc.,
Expedia Inc., Marvell Technology Group Ltd., and Mylan Inc. Conversely,
detracting from overall results on a relative basis were Gildan Activewear
Inc., NCR Corp., Tenet Healthcare Corp., American Apparel Inc., and Ness
Technologies Inc.
 
Alger
Health Sciences Fund
 
The Alger Health Sciences
Fund returned 15.46% for the twelve months ended October 31, 2009, compared to
the S&amp;P 500 Index return of 9.79%.
 
During the period, the
largest portfolio weightings in the Alger Health Sciences Fund were in the
Pharmaceuticals and Health Care Equipment &amp; Supplies sectors. The largest
sector overweight for the period was in Pharmaceuticals. The largest sector
underweight for the period was in Consumer Staples. Relative outperformance in the
Pharmaceuticals and Life Sciences Tools &amp; Services sectors were the most
important contributors to performance. Sectors that detracted from the
portfolio included Consumer Discretionary and Biotechnology.
 
Among the most important relative
contributors were Medicis Pharmaceutical Corp., Optimer Pharmaceuticals Inc.,
Inverness Medical Innovations Inc., Mylan Inc., and Wyeth. Conversely,
detracting from overall results on a relative basis were XenoPort Inc., United
Therapeutics Corp., Cephalon Inc., Boston Scientific Corp., and Genzyme Corp.
 
Alger
Balanced Fund
 
The Alger Balanced Fund
returned 17.91% for the twelve months ended October 31, 2009, compared to the
Russell 1000 Growth Index return of 17.52%.
 
During the period, the
largest portfolio weightings in the Alger Balanced-equity Portfolio were in the
Information Technology and Consumer Staples sectors. The largest sector
overweight for the period was in Financials. The largest sector underweight for
the period was in Information Technology. Relative outperformance in the Health
Care and Consumer Discretionary sectors were the most important contributors to
performance. Sectors that detracted from the portfolio included Information
Technology and Industrials.
 
 
Among the most important
relative contributors were J. Crew Group Inc., Yamana Gold Inc., Expedia Inc.,
Anadarko Petroleum Corp., and Cognizant Technology Solutions Corp. Conversely,
detracting from overall results on a relative basis were General Electric Co.,
Schlumberger Ltd., JPMorgan Chase &amp; Co., Gildan Activewear Inc., and Devon
Energy Corp.
 
As of October 31, 2009,
60% of the fixed-income portion of the portfolio was in corporate securities,
23% in mortgage/asset-backed securities, 12% in U.S. Treasuries, 3% in
government agencies, and 2% in cash.
 
After a challenging 2008
the market was left to repair itself and the rally and returns in the credit
markets in 2009 have been nothing short of remarkable. Corporations responded
well to the credit crunch significantly bolstering liquidity, cutting costs,
better managing inventories and reducing debt. The result of repairing balance
sheets was a rapid decline in credit spreads as investors’ embraced risk and
felt they were being adequately compensated as the world emerged from the fear
of a systemic failure. The Federal Reserve has stated that they will maintain a
zero rate policy as long as needed and it is this coupled with an emerging
economic recovery that will provide a constructive backdrop in 2010.
 
Alger
Convertible Fund
 
The Alger Convertible
Fund return 26.26% for the twelve-months ending October 31, 2009, versus the
Merrill Lynch All Convertible Index return of 37.26% and the Lipper Convertible
Securities Fund Index return of 34.37%. Standardized returns are located in the
following pages.
 
Convertibles rebounded
strongly through 10/31/09 and rivaled High Yield as one of the best performing
asset classes YTD.  A robust equity
market coupled with a resurgence of new issues, and attractive yield levels
pushed valuations higher. As the US economy continues to emerge from a
recession, an accommodative monetary policy coupled with an appetite for risk
has proved to be a winning combination for convertibles.
 
Even as the worst of the
recent crisis appears behind us the top performing factors for equities and
debt were high beta, small capitalization, and CCC rated credits. Securities in
each of these areas topped the market returns by significant amounts. Because
these securities tend to be speculative, have very high volatility and are
typically very illiquid, we do not believe a shift into higher risk without
adequate liquidity is warranted or in the best interests of our clients.
 
Alger
Money Market Fund
 
The Alger Money Market
Fund posted a return of 0.08% for the twelve-month period ending October 31,
2009.
 
Money Market Funds, which
saw a rapid inflow of money throughout 2008 and into 2009 have seen a reversal
of fortune as investors seek more attractive investment opportunities. Money
Market funds on whole have endured $550bn of outflows due to the zero rate
policy of the Federal Reserve. We look for the Fed to keep its target rate
unchanged throughout 2010 and will continue to pressure short term yields.
 
 
As always, we strive to
deliver consistently superior investment results for you, our shareholders, and
we thank you for your business and your continued confidence in Alger.
 
 
Respectfully submitted,


