Dear Shareholder,
Although the year began gloomily, stocks reversed course in March and rose steadily from then on. Vanguard’s four domestic Tax-Managed Funds followed suit.
The Tax-Managed Capital Appreciation Fund was the best performer, with a return of about 29%. The weakest performer was the Tax-Managed Balanced Fund, with a still-robust return of about 19%. Municipal bonds––which account for about half of the Tax-Managed Balanced Fund––produced a solid gain for the year, but trailed equities.
The funds met their tax-efficiency objective, as they have since their inception. In the Performance at a Glance chart on page 8, you can see that the funds’ advisors, Vanguard’s Quantitative Equity Group and Fixed Income Group, continued to avoid distributing capital gains to shareholders.
You may wish to review the table and discussion of after-tax returns for the fiscal year that appear later in this report.
2
Impressive stock returns 
Stock markets outside the United States fared even better during 2009, especially many emerging markets, which weathered the financial crisis in relatively better fiscal and economic shape than their developed-market counterparts.
Bond markets calmed down, 
3
corporate bonds increased, raising their prices and bringing down their yields. For the 12 months ended December 31, the broad taxable bond market returned about 6%, while municipal bonds returned about 13%.
The stock market rally 
1 The fund expense ratios shown are from the prospectuses dated April 24, 2009, and represent estimated costs for the current fiscal year based on the funds’ net assets as of the prospectus date. For the fiscal year ended December 31, 2009, the expense ratios were: for the Tax-Managed Balanced Fund, 0.15%; for the Tax-Managed Growth and Income Fund, 0.21% for Investor Shares, 0.15% for Admiral Shares, and 0.09% for Institutional Shares; for the Tax-Managed Capital Appreciation Fund, 0.21% for Investor Shares, 0.15% for Admiral Shares, and 0.09% for Institutional Shares; for the Tax-Managed Small-Cap Fund, 0.19% for Investor Shares and 0.09% for Institutional Shares.
The peer-group expense ratios are derived from data provided by Lipper Inc. and capture information through year-end 2008.
4
(We review the performance of Vanguard Tax-Managed International Fund in a separate report.)
The Tax-Managed Capital Appreciation Fund, which includes large- and mid-capitalization stocks, led the way with a return of about 29%, as mid-cap stocks generally outpaced their large- and small-cap brethren. The Tax-Managed Growth
and Income Fund has investment characteristics similar to those of its benchmark, the Standard and Poor’s 500 Index, and the fund and index produced nearly identical returns of about 26%. The Tax-Managed Small-Cap Fund advanced about 25%. The Tax-Managed Balanced Fund’s gain of about 19% reflected its nearly equal allocations to stocks and tax-exempt bonds.
The figures shown represent past performance, which is not a guarantee of future results. (Current performance may be lower or higher than the performance data cited. For performance data current to the most recent month-end, visit our website at www.vanguard.com/performance.) Note, too, that both investment returns and principal value can fluctuate widely, so an investor’s shares, when sold, could be worth more or less than their original cost.
1 50% Russell 1000 Index, 50% Barclays Capital 7 Year Municipal Bond Index.
5
Three of the funds recorded gains in all ten equity sectors—a striking contrast to 2008, when all sectors finished with losses. In the Tax-Managed Small-Cap Fund, two sectors—financials and the tiny telecommunication services sector—declined. Growth stocks outpaced value stocks for the year, as higher-quality, dividend-paying companies lagged in the earlier stages of the recovery.
The information technology and consumer discretionary sectors were the top two contributors for all four funds. Corporations, which slashed their budgets during the financial crisis, resumed spending on communications equipment and on computer hardware and software. Sales of home computers and electronic components also helped lift the IT sector.
Although the downturn spurred an increase in the nationwide savings rate, certain consumer discretionary companies were well-positioned to benefit from customers’ new cost-consciousness. Internet retailers, media companies, home-improvement retailers, and recreation-oriented companies were among the firms that bounced back and drove the sector’s powerful return.
To varying degrees, all four funds benefited from the performance of the energy, materials, health care, industrials, and consumer staples sectors as the stock market rally continued.
Metals and mining firms and chemical companies rose as worldwide demand for commodities revived. Global demand and signs of improvement in the economy also helped conglomerates and construction firms in the industrial sector. As oil prices rose during the year, so did the energy sector, particularly exploration and production, drilling, and equipment companies. Mergers and acquisitions boosted health care stocks. Consumer staples stocks increased along with consumer confidence, led by soft drink and tobacco companies.
The Tax-Managed Balanced Fund’s performance reflected that of its composite benchmark: The Russell 1000 Index returned about 28% and the Barclays Capital 7 Year Municipal Bond Index returned more than 7%. Fixed income returns were driven by increased demand for municipal bonds and by the federal government’s willingness to stabilize state and local governments along with the overall economy.
6
Tax-efficient strategy backed 
To say the least, the decade was unkind to investors, with two bear markets smothering returns. It was the first calendar decade (a decade starting with a “0” year) that the S&P 500 Index finished with a negative total return––even the 1930s had a gain of about 1% when dividends are included. While we all wish the outcomes were better, it’s worth noting that three of the four funds in this report outperformed their peer-group averages during the period. The Tax-Managed Capital Appreciation Fund was about 1 percentage point off the multi-cap core funds average.
While maintaining tax-efficiency, all four funds have stayed close to the returns of their benchmarks, a testament to the skill and discipline employed by Vanguard’s Quantitative Equity and Fixed Income Groups. Low operating costs help the funds closely follow their indexes even in volatile investment environments.
Two turbulent years underscore 
The wisest way to confront such risk, in Vanguard’s view, is to construct a diversified portfolio with allocations suited to your own tolerance for market swings, as well as your investment goals. For tax-conscious investors, Vanguard’s Tax-Managed Funds, with their proven tax-efficiency and their low expense ratios, can play a useful role in such a portfolio.
On another matter, I would like to inform you that on January 1, 2010, we completed a leadership transition that began in March 2008. I succeeded Jack Brennan as chairman of Vanguard and each of the funds. Jack has agreed to serve as chairman emeritus and senior advisor.
7
Under Jack’s leadership, Vanguard has grown to become a preeminent firm in the mutual fund industry. Jack’s energy, his relentless pursuit of perfection, and his unwavering focus on always doing the right thing for our clients are evident in every facet of Vanguard policy today.
Thank you for entrusting your assets to Vanguard.
Sincerely,


Dear Shareholder,
For the year ended December 31, 2009, Vanguard Tax-Managed International Fund and its exchange-traded share class, Vanguard Europe Pacific ETF, returned about 28%, the fund’s second-best performance since its inception in 1999.
For the fiscal period, the fund met its tax-efficiency objective, as it has since inception. As you can see in the Performance at a Glance table on page 1, the fund’s advisor, Vanguard Quantitative Equity Group, continued to avoid distributing taxable capital gains to shareholders.
2
Impressive stock returns 
The market’s outsized result came against a backdrop of economic uncertainty. The economy, which struggled to regain its footing as the financial crisis abated, began to show signs of growth in the second half of the year, even as unemployment climbed to levels not seen since 1983.
Stock markets outside the United States fared even better during 2009, especially many emerging markets, which weathered the financial crisis in relatively better fiscal and economic shape than their developed-market counterparts.
Bond markets calmed down, 
3
Meanwhile, money market funds and other short-term savings vehicles did poorly in 2009. Interest rates at the shortest end of the maturity spectrum remained at historic lows, a casualty of the Federal Reserve Board’s efforts to revive the economy. The Fed kept its target for the federal funds rate between 0% and 0.25% for the entire year.
Market gains across the globe 
After enduring the global financial crisis in 2008—which led to its worst annual performance ever—the Tax-Managed International Fund benefited from a strong rally in developed-market securities in 2009. Although these markets didn’t soar as dramatically as emerging markets during the year, they surpassed the performance of the U.S. stock market.
Every country and sector represented in the fund had a positive return for the year. European stocks, which account for about two-thirds of the fund’s assets, rose nearly 37%. The United Kingdom market, which had the largest weighting in the portfolio at the end of the period, contributed the most to the overall return. The French and German markets also were strong contributors.
1 The fund expense ratios shown are from the prospectuses dated April 24, 2009, and represent estimated costs for the current fiscal year 
4
Among the driving factors behind European markets’ strong performance was a rebound in the financial sector. Europe’s troubled banks, battered in the credit crisis of 2008, have since benefited from hefty government rescue efforts. Strong gains in the materials and industrial sectors reflected increased demand for commodities as economies began to stabilize.
In the Pacific region, which accounts for about a third of the portfolio, the fund’s holdings returned nearly 25% in 2009. Australia was the largest contributor in this region, buoyed by its financial and materials stocks. The Hong Kong and Singapore markets also performed very strongly.
Japan, whose market had the second-largest weighting in the fund at the end of the period, produced the weakest 12-month return. The Japanese economy has never quite recovered from sharp declines in exports that followed the financial crisis. A strong yen, which made Japanese products less competitive abroad, has complicated the problem.
Still, the Tax-Managed International Fund recorded one of its best years ever as investors regained optimism about the markets and grew more willing to invest in sectors that could benefit from an economic turnaround: financials, materials, and consumer discretionary stocks.
The figures shown represent past performance, which is not a guarantee of future results. (Current performance may be lower or higher than the performance data cited. For performance data current to the most recent month-end, visit our website at www.vanguard.com/performance.) Note, too, that both investment returns and principal value can fluctuate widely, so an investor’s shares, when sold, could be worth more or less than their original cost.
1 Derived from data provided by Lipper Inc.
5
A note on fair-value pricing
The return of a fund that tracks an index sometimes may appear to diverge from the index’s return a bit more than would be expected. This may be the result of a fair-value pricing adjustment.
These adjustments address pricing discrepancies that may arise because of time-zone differences among global stock markets. Foreign stocks may trade on exchanges that close many hours before a fund’s closing share price is calculated in the United States, generally at 4 p.m., Eastern time. In the hours between the foreign close and the U.S. close, the value of these foreign securities may change—because of company-specific announcements or market-wide developments, for example. Such price changes are not immediately reflected in international index values.
Fair-value pricing takes such changes into account in calculating the fund’s daily net asset value, thus ensuring that the NAV doesn’t include “stale” prices. The result can be a temporary divergence between the return of the fund and that of its benchmark index—a difference that usually corrects itself when the foreign markets reopen.
The fund’s long-term record 
For the decade ended December 31, 2009, the Tax-Managed Index Fund returned an average of 1.24% per year for Investor Shares, ahead of its peer-group average and roughly in line with the benchmark’s return. This performance is a credit to the fund’s advisor, Vanguard Quantitative Equity Group, whose long-tenured professionals have developed sophisticated portfolio construction, trading, and tax-management methodologies. The advisor’s efforts are supported, of course, by the fund’s low operating expenses.
Think long-term and diversify, 
At Vanguard, we encourage you to focus on the long term and avoid making rash decisions based on the markets’ short-
6
term volatility. We believe it’s important to stick with time-tested principles of balance and diversification, both within and across asset classes. By establishing a portfolio that includes stocks, bonds, and short-term reserves in a mix that suits both your goals and your risk tolerance, you’ll prepare yourself to meet the markets’ emotional challenges as well as their financial ones.
For tax-conscious investors, Vanguard Tax-Managed International Fund, with its low expenses, proven strategy, and broad exposure to developed markets abroad, can play a useful role in such a diversified portfolio.
Under Jack’s leadership, Vanguard has grown to become a preeminent firm in the mutual fund industry. Jack’s energy, his relentless pursuit of perfection, and his unwavering focus on always doing the right thing for our clients are evident in every facet of Vanguard policy today.
Thank you for entrusting your assets to Vanguard.
Sincerely,


