Dear Shareholders: 
There remains some question as to when the global economy will achieve a sustainable recovery. While some economists and market watchers are optimistic that the worst is behind us, a number also agree with U.S. Federal Reserve Board
Chairman Ben Bernanke who said in September that “even though from a technical perspective the recession is very likely over at this point, it’s still going to feel like a very weak economy for some time.” 
Have we in fact turned the corner? We have seen tremendous rallies in the markets over the past six months. The Fed has cut interest rates aggressively
toward zero to support credit markets, global deleveraging has helped diminish inflationary concerns, and stimulus measures have put more money in the hands of the government and individuals to keep the economy moving. Still, unemployment remains
high, consumer confidence and spending continue to waiver, and the housing market, while improving, has a long way to go to recover. 
Regardless of lingering market uncertainties, MFS® is confident that the fundamental principles of long-term investing will always apply. We encourage investors to speak with their advisors to
identify and research long-term investment opportunities thoroughly. Global research continues to be one of the hallmarks of MFS, along with a unique collaboration between our portfolio managers and sector analysts, who regularly discuss potential
investments before making both buy and sell decisions. 
As we continue to dig out from the worst financial crisis in decades, keep in mind that
while the road back to sustainable recovery will be slow, gradual, and even bumpy at times, conditions are significantly better than they were six months ago. 
Respectfully, 


