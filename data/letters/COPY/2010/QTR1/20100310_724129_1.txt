Dear Shareholders,

We are pleased to present the annual shareholder report for the SunAmerica
Money Market Fund and the SunAmerica Municipal Money Market Fund. We would also
like to take the opportunity to discuss the market conditions that have shaped
the investment environment over the Funds' annual period, ended December


Both the financial and economic markets withstood many remarkable twists and
turns during the past twelve months as fears that the Great Recession would
become the second Great Depression dissipated. The annual period began on a
precarious note as both the U.S. and global economic conditions continued to
deteriorate. On the fiscal front, the U.S. government responded by supporting a
variety of programs designed to support financial institutions and restart
consumer credit markets. With short-term interest rates effectively at zero,
the Federal Reserve embarked on a program of buying government bonds to create
new money in the economy.

The second fiscal quarter brought a belief that the economy had bottomed and
was on its way to a sustainable recovery. This renewed economic optimism
increased investors' risk appetite launching a rally in the equity and credit
markets. The Federal Reserve met twice during the three-month period, leaving
rates unchanged and reiterating its commitment to employ all available tools to
promote price stability and economic recovery.

This Fed policy was warranted as conditions worsened over the course of the
summer and the unemployment rate rose to its highest rate in over years,
leading some to fear that a weak labor market would undermine consumer spending
and damage the environment for a strong sustainable economic recovery.

At year-end, a challenging economic landscape confronted both policymakers and
investors. Along with high unemployment, other critical issues included the
uncertain impact of consumer de-leveraging on the economy, a growing federal
budget deficit, and future inflationary threats. While the residential housing
market has begun showing signs of improvement, concerns remain regarding the
stability of the commercial real estate market.

The Fed Funds rate ended the year unchanged, in a range of The low
interest rate environment was challenging for yields. In order to preserve
yields, the SunAmerica Money Market Fund added value through active management
of the Fund's duration, specifically by purchasing longer-dated fixed rate
instruments as well as floating rate notes. The SunAmerica Municipal Money
Market Fund maintains a portfolio diversified by sector and region with the
majority of the portfolio being invested in variable rate obligations during
the period.

In September the U.S. Treasury announced the establishment of a temporary
guarantee program for eligible money market funds. This program was ultimately
extended through September to support ongoing stability in the
market. The SunAmerica Money Market Fund and the SunAmerica Municipal Money
Market Fund both participated throughout the program's existence.+

Though the fixed income markets have been devoid of yield in we remain
diligent in the management of your assets and thank you for your continued
investment in the Funds. If you have any questions, or require additional
information on this or other SunAmerica Mutual Funds, I invite you to visit our
website at www.sunamericafunds.com or call our Shareholder Services Department
at

Sincerely,
