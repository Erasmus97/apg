Dear Shareholders,

We are pleased to present the annual shareholder report for the SunAmerica
Senior Floating Rate Fund. Below, we've briefly outlined the market conditions
that have shaped the investment environment over the annual period ended
December

Both the financial and economic markets withstood many remarkable twists and
turns during the past twelve months as fears that the Great Recession would
become the second Great Depression dissipated. The fiscal year began on a
precarious note as both the U.S. and global economic conditions continued to
deteriorate. On the fiscal front, the U.S. government responded by supporting a
variety of programs designed to support financial institutions and restart
consumer credit markets. With short-term interest rates effectively at zero,
the Federal Reserve embarked on a program of buying government bonds to create
new money in the economy.

The second fiscal quarter brought a belief that the economy had bottomed and
was on its way to a sustainable recovery. This renewed economic optimism
increased investors' risk appetite, launching a rally in the equity and credit
markets. The Federal Reserve met twice during the three-month period, leaving
rates unchanged and reiterating its commitment to employ all available tools to
promote price stability and economic recovery.

This Fed policy was warranted as conditions worsened over the course of the
summer and the unemployment rate rose to its highest rate in over years,
leading some to worry that a weak labor market would undermine consumer
spending and damage the environment for a strong, sustainable economic recovery.

At year-end, a challenging economic landscape confronted both policymakers and
investors. Along with high unemployment, other critical issues included the
uncertain impact of consumer de-leveraging on the economy, a growing federal
budget deficit, and future inflationary threats. While the residential housing
market has begun showing signs of improvement, concerns remain regarding the
stability of the commercial real estate market.

The improvement in the credit markets and investors' greater appetite for risk
both benefited the leveraged loan market. As of year end, fundamentals had
improved as demand for bank loans have been strong. The new issue market has
also re-opened, which is providing new investment opportunities. With current
interest rates at all-time lows, the floating-rate coupons on bank loans can
provide investors a hedge against a future rise in rates. In addition, loans
benefit from being secured by collateral, which can help to provide downside
protection against negative surprises.

We remain diligent in the management of your assets and thank you for your
continued investment in the Fund. If you have any questions, or require
additional information on this or other SunAmerica Funds, we invite you to
visit www.sunamericafunds.com or call the Shareholder Services Department at


Sincerely,
