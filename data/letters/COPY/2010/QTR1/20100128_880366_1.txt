Dear Shareholder,
While the U.S. economy remained weak during much of the twelve-month reporting period ended the lengthiest recession since the Great Depression finally appeared to have ended
during the third quarter of Looking back, the U.S. Department of Commerce reported that fourth quarter
U.S. gross domestic product (GDP)i contracted Economic weakness accelerated during the first quarter of as GDP fell However, the economic
environment started to get relatively better during the second quarter, as GDP fell The economys more modest contraction was due, in part, to smaller declines in both exports and business spending. After contracting four consecutive
quarters, the Commerce Department reported that third quarter GDP growth was A variety of factors helped the economy to expand, including the governments billion stimulus program and its Cash for Clunkers car rebate
program, which helped spur an increase in car sales. Even before GDP advanced in the third quarter, there were signs
that the economy was starting to regain its footing. The manufacturing sector, as measured by the Institute for Supply Managements PMIii, rose to in August
the first time it surpassed since January (a reading below indicates a contraction, whereas a reading above indicates an expansion). PMI data subsequently showed that manufacturing expanded from September through November as well.
The housing market also saw some improvement during the reporting period. According to its most recent data, the
S&amp;P/Case-Shiller Home Price Indexiii indicated that home prices rose for the fourth straight month in September. In addition, the Commerce Department reported that,
during October, sales of existing homes reached their highest level in two years. One area that remained weak and could hamper
the magnitude of economic recovery was the labor market. While monthly job losses have moderated compared to earlier in the year, the unemployment rate remained elevated during the reporting period. After reaching a twenty-six-year high of
in October the unemployment rate fell to









Legg Mason ClearBridge Large Cap Growth Fund

I





Table of Contents



Letter from the chairman
continued

in November. Since December the unemployment rate has more than doubled and the number of unemployed workers has risen by
The Federal Reserve Board (Fed)iv continued
to pursue an accommodative monetary policy during the reporting period. After reducing the federal funds ratev from in August to a range of to percent in
December a historic low the Fed maintained this stance through the end of In conjunction with its December meeting, the Fed said that it will maintain the target range for the federal funds rate at to
percent and continues to anticipate that economic conditions, including low rates of resource utilization, subdued inflation trends, and stable inflation expectations, are likely to warrant exceptionally low levels of the federal funds rate for an
extended period. After falling nearly from September through November (before the reporting period
began), the U.S. stock market, as measured by the S&amp;P Indexvi (the Index), rallied and, overall, generated strong results during the twelve-month
reporting period. Stock prices fell during two of the first three months of the reporting period. This was due to a number of factors, including the rapidly weakening global economy, an ongoing credit crisis and plunging corporate profits. Stock
prices continued to decline in early March, reaching a twelve-year low on Stocks then rallied sharply through the end of September, as the Index rose
approximately from its March low. The Index then fell in October, its first monthly loss since February. However, the reporting period ended on a bright note, as the Index gained in November. The markets strong rebound was due
to a variety of factors, including optimism that the economy was gaining traction and that corporate profits would continue to improve. All told, the Index returned over the twelve-month reporting period ended
Looking at the U.S. stock market more closely, in terms of market capitalizations, large-, mid- and small-cap
stocks, as measured by the Russell Russell Midcapviii and Russell
ix Indices, returned and respectively, during the twelve-month period ended From an investment style perspective, growth and value
stocks, as measured by the Russell Growthx and Russell Valuexi Indices, returned
and respectively. Special shareholder notice
Effective Peter Bourbeau and Scott Glasser of ClearBridge Advisors, LLC are co-portfolio managers of the Fund. Mr.Bourbeau has shared the responsibility for the
day-to-day management of the Funds portfolio since July Mr.Glasser has shared the responsibility for the day-to-day management of the Funds portfolio since August









II

Legg Mason ClearBridge Large Cap Growth Fund





Table of Contents



A special note
regarding increased market volatility Dramatically higher volatility in the financial markets has been very challenging for many
investors. Market movements have been rapid sometimes in reaction to economic news, and sometimes creating the news. In the midst of this evolvingmarket environment, we at Legg Mason want to do everything we can to help you reach your
financial goals. Now, as always, we remain committed to providing you with excellent service and a full spectrum of investment choices. Rest assured, we will continue to work hard to ensure that our investment managers make every effort to deliver
strong long-term results. We also remain committed to supplementing the support you receive from your financial advisor. One way we
accomplish this is through our enhanced website, www.leggmason.com/individualinvestors. Here you can gain immediate access to many special features to help guide you through difficult times, including:





Fund prices and performance,




Market insights and commentaries from our portfolio managers, and





A host of educational resources.
During periods of market unrest, it is especially important to work closely with your financial advisor and remember that reaching ones investment goals unfolds over time and through multiple market cycles. Time and
again, history has shown that, over the long run, the markets have eventually recovered and grown. Information about your fund
Please read on for a more detailed look at prevailing economic and market conditions during the Funds reporting period and to
learn how those conditions have affected Fund performance. Important information with regard to recent regulatory developments that may
affect the Fund is contained in the Notes to Financial Statements included in this report. As always, thank you for your confidence in
our stewardship of your assets. We look forward to helping you meet your financial goals. Sincerely,
