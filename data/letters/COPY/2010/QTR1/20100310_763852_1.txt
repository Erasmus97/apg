Dear Shareholder:
It is hard to imagine a more negative and confusing environment
than the one we faced one year ago. At the time, the global financial system was in the midst of an unprecedented crisis, stock markets were crashing
around the world and the U.S. economy was in the midst of its worst recession since World War II. Today, things look better in many ways; the financial
crisis has abated, stock markets are up substantially from their lows, and the economy appears to have embarked on a recovery.
