Dear Shareholder:

We are pleased to provide the Annual Report to shareholders of the Schroder
Mutual Funds, which covers the twelve months ended October The Report
includes information designed to help you understand the status of your
investment -- the Management Discussion and Analysis prepared by portfolio
managers explains how they put your money to work in various markets; the
Schedules of Investments give you a point-in-time picture of the holdings in
your fund; and additional information includes a detailed breakdown of other
financial information. We encourage you to read the Report and thank you for
making Schroders part of your financial plan.

The twelve-month period ended October was extremely volatile for
investors. The reporting period began on a very negative note with the market
falling sharply through November Despite recovering some ground toward the
end of November, and trading sideways in December, the S&P ended calendar
down The roller coaster ride for investors continued well into
as the S&P hit its lowest closing level in twelve years on March The
problems have been broad-based and heavily reported, including the banking
crisis, massive job losses, and the collapse of the housing market and auto
industry. In the end, governments worldwide were forced to take unprecedented
monetary steps to ease the economic pain, including the over trillion in
stimulus packages, expansionary monetary policies and new programs to assist
banks in clearing bad debt off their balance sheets.

Markets have staged a turnaround toward the end of the reporting period with
investors returning to risk assets. Subsequent to March global equities
and domestic markets began to rise, while the emerging markets are up some
over the same period. As a result, the S&P ended with an increase of for
the twelve-month period ended October We have also seen strong rallies
in the credit and commodity markets while the US dollar has weakened. Meanwhile,
alongside hopes of recovery, government bond yields have risen.

The end of the reporting period saw us upgrade our global growth forecasts, with
the world economy now expected to contract by this year before growing at
just over in The upgrade has largely been driven by a
better-than-expected GDP performance in the second quarter and an improvement in
business surveys, with the biggest forecast increases having been made in Europe
and Japan. We have also raised our forecasts for emerging markets due to the
growth upgrade in the Organisation for Economic Co-operation and Development
("OECD") region and resilient Chinese growth. The latter has continued to enjoy
upgrades as fiscal policy has boosted activity.

Meanwhile, we expect the recovery in the global economy to be driven by the
industrial sector as the inventory cycle turns and stronger government spending
kicks in.

On the inflation front, we are forecasting a sharp fall in helped by lower
commodity prices and the slack created by the downturn. In inflation
should move up again due to higher energy prices. However, deflationary
pressures persist and we believe the US will experience a decline in core
inflation throughout

In line with the upgrade to global growth, we have brought forward our forecasts
for monetary policy tightening. We now expect the first rate hikes from the US
Federal Reserve (Fed) and the Bank of England (BoE) to occur in June next year,
thus marking an end to the period of ultra-loose policy. Meanwhile, the European
Central Bank (ECB) and the Bank of Japan (BoJ) are expected to follow in August
and September respectively. However, a coordinated move with all the major
central banks tightening together is also a possibility.

While inflation and credit growth are still likely to be subdued at this point,
the tightening of policy will be in recognition of the stabilization of the
banking sector and financial system. The case for setting interest rates at
emergency levels will no longer be valid. At the same time, with the world
economy returning to grow, we would expect rates to increase gradually over the
rest of and to rise further in Nonetheless, we believe they should
remain at very low levels by historical standards. Moreover, concerns about the
sustainability of the recovery, the need to rein in budget deficits and an
absence of inflationary pressure will mean that rates will rise slowly and are
likely to peak at levels lower than in previous cycles.

Our view is that returns on cash will likely be negligible for some time,
forcing investors to continue to search for yield. Consequently, we would expect
more gains for credit markets and a potential flattening of the yield curve as
investors reach for longer maturities. More generally, we expect to see a return
of carry trades with investors taking on more risk to generate income. Alongside
continued growth, this is a favorable backdrop for equity markets. The recovery
combined with aggressive cost cutting has brought a substantial improvement in
earnings growth expectations.






SCHRODER MUTUAL FUNDS

As we stated in the Funds' Semi-Annual Report, in this type of environment, we
believe that the investor who maintains a diversified portfolio -- both across
asset classes and geographic borders -- should be able to weather the bumpy
periods better than those who have high concentrations in one or two sectors or
regions. We encourage you to consult with your financial advisor to ascertain
whether your current mix of investments is suitable for your long-term
objectives.

Again, we thank you for including Schroders in your financial plan and we look
forward to our continued relationship.

Sincerely,
