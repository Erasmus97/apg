Dear Fellow Shareholders:
 
I am happy to share with you the results for our latest fiscal year. With exception of a few rough weeks in February, shareholders of the Fund might not have realized that the last twelve months have perhaps been the most volatile in financial market history. 
 
In the prior two annual reports, I discussed the “panics” which result from excessive easy and unregulated credit. This year saw the governmental and business responses to those panics. Two PhD economists, Federal Reserve Chairman, Ben Bernanke, and Director of the White House's National Economic Council, Lawrence Summers, followed the textbook response to the banking crisis learned from the
        1930s. While support of the credit markets was necessary in both cases, this country’s circumstances are very different in this period. We entered the 1930s as the world’s largest creditor nation. We entered the current downturn as the world’s largest debtor. The combination of massive monetary expansion accompanied by the large fiscal boost stemming from the federal deficit has, in the short run, proved a tonic for both investment banks and the stock
        market.
 
In the longer run, this increase in the country’s total debt and the supply of money could create barriers to our prosperity. The outsized financial sector has once again absorbed resources which the most productive areas of the economy needed. Also, large international companies which we own a lot of, have been able to consolidate their positions, but more entrepreneurial smaller domestic companies
        have had their growth stunted. Remarkably in the last nine years, the private sector has lost more jobs than it has created. Only governments have added new positions. Further, the focus on Wall Street at the expense of creating incentives for the private sector to hire and retain workers means that many Americans could still have severalyears to recover their economic well being.
 
Meanwhile, we have accomplished in the current year what we said we would in my most recent letters. The decline has enabled us to buy first rate companies at reasonable prices. We start the new year with what we believe are the highest caliber companies that we have ever had in the portfolio. In addition, we jettisoned several lower grade companies which we had bought because of their more attractive
        valuations.
 
We remain a very small fund with the same regulations and burdens of larger funds. Only by attracting new shareholders should we have the ability to lower our expenses. That is why I need your help in spreading the word. Despite our necessarily higher expenses, we have served our shareholders well. As Marathon’s largest shareholder, I will work towards the end that we all may benefit from the
        profitability of America’s corporations.
 
I once again thank long term shareholders who have helped make the Marathon Value Portfolio a unique proposition in today’s investment world.
I thank you for your continued loyalty.
 
Sincerely,


Dear Fellow Shareholder,
 
Considering the returns posted by the U.S. stock market for the 12 months ending 10/31/2009, an investor who didn’t know better might be inclined to think it was a rather unremarkable year. After all, many investors have heard over and over again that the stock market has averaged gains of 9%-11% per year over many decades, so the past year’s gain of 11.36% for the Wilshire 5000 and 9.80% for
        the S&amp;P 500 seem to fit that expectation almost perfectly.
 
Of course, having lived through the past year, you know that the past 12 months have been 
 
The Sound Mind Investing (“SMI”) Funds had mixed results navigating these treacherous market currents. The flagship Sound Mind Investing Fund (SMIFX) outperformed the broad U.S. market, gaining 16.57% over the past twelve months (compared to 11.36% for the Wilshire 5000 and 9.80% for the S&amp;P 500). The core investing strategy of the SMI Funds, called Fund Upgrading, was generally successful
        in rotating the portfolios toward those sectors showing relative strength at the time. This helped us lose a bit less when the market was falling, and subsequently gain a bit more when the market started rising. We’re particularly pleased with the results of the past year, given that many quantitative strategies such as ours struggled because of the speed of the changes that occurred during the financial crisis and subsequent recovery.
 
Unfortunately, the SMI Managed Volatility Fund (SMIVX) didn’t perform as well, gaining just 1.62% over the past twelve months. The Managed Volatility Fund’s hedging component, which protected investors so well when the market was falling rapidly, left it flat-footed when the market suddenly reversed course and zoomed higher. Naturally the fund was quite heavily hedged at the March bottom, and
        the speed of the subsequent rise in stock prices meant the fund missed a large portion of the market’s early gains as a result of our system being somewhat conservative in removing those hedges.
 
However, despite its lagging performance over the past year, investors who have been invested in the Managed Volatility Fund since its 12/29/06 inception are still ahead of the market by a cumulative margin of -13.92% vs. -21.49% (Wilshire 5000) and -22.55% (S&amp;P 500). It’s also worth noting that since SMIVX was launched, it actually leads the flagship SMI Fund (SMIFX) by a slight margin: -13.92%
        vs. -14.17%. And, as one might expect, it has outperformed both SMIFX and the overall market while generating 
 
In light of the fact that SMIVX’s performance over the longer market cycle since its inception has been relatively good, despite lagging the market considerably in recent months, it’s clear the fund’s approach requires patience. Understanding that its strengths are best seen over longer time periods will help investors during those inevitable stretches, such as much of this year, when
        the performance of SMIVX is disappointing over the short-term.
 
What Next?
 
What a difference a year makes! A year ago, many investors truly feared the end of the financial system as we know it. The markets were in total disarray. Now, a year later, we’ve weathered that panic and the markets have rebounded considerably from their lows (though they still remain well below their 2007 highs). The government and Federal Reserve appear committed to “whatever it
        takes” levels of interest rates and stimulus in support of economic recovery. While the long-term implications of these policies remain to be seen, in the short-term, they seem to have put a floor under markets and helped avert the worst-case scenarios that seemed so plausible a year ago. While financial markets still face considerable risks, on balance the economic signs appear to be brightening.
 
But what about the serious financial questions that remain unanswered? Will the next year hold inflation or deflation? Will commercial real estate be the next financial shoe to drop, pulling down more banks and financial stocks? Is gold in a bubble or simply responding appropriately to unprecedented government fiscal and monetary policies? Will the bull market in stocks continue or reverse as the economy
        hits the double-dip of renewed recession?
 
As you might expect, we have opinions as to how these questions ultimately will be answered. But we learned long ago that investing based on flawed predictions of a largely unknowable future is a tough way to make money. Instead, we have confidence that whatever the answers to these questions, our Upgrading process will systematically lead us to the pockets of market strength that best reflect the
        eventual outcomes. Rather than trying to 
 
Upgrading isn’t a perfect investing system. But it has proven itself to be an 
 
 
 
Sincerely, 


