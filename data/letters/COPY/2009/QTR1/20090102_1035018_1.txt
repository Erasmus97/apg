Dear Shareholder: 
 
We hope you find the annual report for the Dryden Mid Cap Value Fund informative and useful. Because
market volatility climbed sharply in 2008, we understand that this is a difficult time to be an investor. While it is impossible to predict what the future holds, we continue to believe a prudent response to uncertainty is to maintain a diversified
portfolio, including stock and bond mutual funds consistent with your tolerance for risk, time horizon, and financial goals. 
 
A diversified asset allocation offers two potential advantages: it limits your exposure to any particular asset class, plus it provides a better opportunity to invest some of your
assets in the right place at the right time. Your financial professional can help you create a diversified investment plan that may include mutual funds covering all the basic asset classes and that reflects your personal investor profile and risk
tolerance. Keep in mind that diversification and asset allocation strategies do not assure a profit or protect against loss in declining markets. 
 
®
 
Thank you for choosing JennisonDryden Mutual Funds. 
 
Sincerely, 


Dear Shareholder: 
 
We hope you find the annual report for the Jennison Equity Income Fund informative and useful. Because
market volatility climbed sharply in 2008, we understand that this is a difficult time to be an investor. While it is impossible to predict what the future holds, we continue to believe a prudent response to uncertainty is to maintain a diversified
portfolio, including stock and bond mutual funds consistent with your tolerance for risk, time horizon, and financial goals. 
 
A diversified asset allocation offers two potential advantages: it limits your exposure to any particular asset class, plus it provides a better opportunity to invest some of your
assets in the right place at the right time. Your financial professional can help you create a diversified investment plan that may include mutual funds covering all the basic asset classes and that reflects your personal investor profile and risk
tolerance. Keep in mind that diversification and asset allocation strategies do not assure a profit or protect against loss in declining markets. 
 
®
 
Thank you for choosing JennisonDryden Mutual Funds. 
 
Sincerely, 


Dear Shareholder: 
 
We hope you find the annual report for the Dryden Money Market Fund informative and useful. Because
market volatility climbed sharply in 2008, we understand that this is a difficult time to be an investor. While it is impossible to predict what the future holds, we continue to believe a prudent response to uncertainty is to maintain a diversified
portfolio, including stock and bond mutual funds consistent with your tolerance for risk, time horizon, and financial goals. 
 
A diversified asset allocation offers two potential advantages: it limits your exposure to any particular asset class, plus it provides a better opportunity to invest some of your
assets in the right place at the right time. Your financial professional can help you create a diversified investment plan that may include mutual funds covering all the basic asset classes and that reflects your personal investor profile and risk
tolerance. Keep in mind that diversification and asset allocation strategies do not assure a profit or protect against loss in declining markets. 
 
®
 
Thank you for choosing JennisonDryden Mutual Funds. 
 
Sincerely, 


