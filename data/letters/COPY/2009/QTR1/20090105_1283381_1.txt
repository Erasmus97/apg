Dear Shareholder,

Enclosed is the Annual Report to shareholders of the TFS Market Neutral Fund
(ticker: TFSMX) and the TFS Small Cap Fund (ticker: TFSSX) for the fiscal period
ended October On behalf of the investment manager, TFS Capital LLC
("TFS"), I'd like to thank you for your investment. As previously mentioned, we
are pleased to announce that TFSMX's strong performance earned it the
Lipper Award in the Equity Market Neutral category. This award is presented to
the fund in the category with the best return ending December
For this period TFSMX was ranked out of Below are performance
highlights for the Funds.

TFS MARKET NEUTRAL FUND (TICKER: TFSMX)

- --------------------------------------------------------------------------------
Since Inception
year return year return
- --------------------------------------------------------------------------------
TFS Market Neutral Fund
S&P Index
- --------------------------------------------------------------------------------
Average annual total returns for periods ended and since inception.

The TFS Market Neutral Fund has struggled during the recent liquidity crisis.
Similar to August of the rapid unwinding of positions by long-short
managers is believed to have had a significant adverse affect on TFSMX. This
activity has led TFSMX into its largest peak-to-valley drawdown since inception
and resulted in its first ever negative return as measured on a
basis. While we continue to expect above average volatility
in the coming months, we are optimistic given that we perceive conditions to be
improving.

Despite TFSMX's recent challenges, it continues to perform extremely well
relative to the broad equity markets. For example, the return" in the
chart above shows the greatest separation versus the S&P Index that we have
ever reported in an annual report. Also, from inception through the fiscal
period-end of October TFSMX generated an average annual return
and an annualized standard deviation of whereas the S&P Index
generated a average annual return and an annualized standard deviation of
Therefore, from inception through October TFSMX produced an
annual return that was percentage points higher than that of the S&P
Index while generating only of the volatility. Given a slightly positive
market exposure (beta) since inception, the negative growth of the overall U.S.
equity market (i.e. S&P Index) did slightly hurt TFSMX's performance over
the periods noted. That impact is small, however, and most of the performance
can be attributed to equity selection (alpha generation) by TFS. So, one can
conclude that the models utilized by TFS to assist in picking stocks were
generally successful at predicting the relative price movement of stocks within
TFSMX's target universe.





The strategies used in managing TFSMX have resulted in it having a very low
correlation to other asset classes. According to Modern Portfolio Theory
("MPT"), investors may benefit significantly by combining non-correlated
investments. As such, we encourage investors to evaluate the impact that the
inclusion of TFSMX would have on their portfolios. We believe TFSMX is best used
as a component of a diversified portfolio of investments rather than a single
holding for a given shareholder. We do not recommend allocating the majority of
one's assets to TFSMX. For your reference, below are the correlations to several
other asset classes.

- --------------------------------------------------------------------------------
Index Correlation Since Inception
- --------------------------------------------------------------------------------
S&P Index
Russell Index
MSCI EAFE Index
Lehman Brothers U.S. Aggregate Bond Index
Wilshire REIT ETF Index
Dow Jones Commodity Futures
- --------------------------------------------------------------------------------

Please note that we have developed an interactive "Asset Allocation Tool" on our
website. We encourage you to visit our web site to determine if an allocation to
TFSMX may have improved the performance of your portfolio.

TFS SMALL CAP FUND (TICKER: TFSSX)

- --------------------------------------------------------------------------------
Since Inception
year return
- --------------------------------------------------------------------------------
TFS Small Cap Fund
Russell Index
- --------------------------------------------------------------------------------
Average annual total returns for periods ended and since inception.

Through October TFSSX continued to achieve its objective of
outperforming the Russell Index (the "Index") as measured by its
performance since inception. From inception on March through the
recent fiscal period-end of October it generated an average annual
return of versus for the Index. It did, however, underperform the
Index during the fiscal period posting a return versus a return
for the Index. It is noteworthy that TFSSX generated its returns while having an
average beta versus the Index since inception of TFSSX's recent struggles
are believed to be attributable to the rapid unwinding of positions by longshort
managers. TFSSX's portfolio appears to have significant overlap with the
holdings of the hedge fund universe in aggregate. As such, rapid unwinding of
positions by hedge fund managers can cause TFSSX's portfolio to decline more
rapidly than its benchmark Index while this activity is occurring. We attribute
the recent poor (relative) performance to this activity.

For both Funds, there were no significant changes to the investing strategies
during the period that impacted performance. We did, however, take a defensive
cash position in TFSMX in an effort to dampen volatility and mitigate further
declines in TFSMX's net asset value. As described in the prospectus, TFS
develops quantitative models through





rigorous historical analysis of the stock market. These models help TFS make
objective decisions that we believe will lead to good performance.

Best regards,
