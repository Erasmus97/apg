Dear Shareholders,

We are pleased to present you with the financial report for the AIG Series
Trust for the annual period ended October

The annual period covers one of the most challenging periods in both the fixed
income and equity markets that we have seen in recent history. The
deterioration in the housing market created concerns regarding the subprime
mortgage market. This concern spread throughout the credit markets and created
great uncertainty as the era of seemingly endless liquidity came to an end.
Banks tightened lending standards and investors flocked to short-term
Treasuries driving yields down. As the equity markets sold off, particularly in
the last months of the period, and the credit markets were not functioning
efficiently, the state of the economy hung in the balance.

The AIG Series Trust High Watermark Funds are target maturity funds that are
designed to provide investors with risk-controlled exposure to the S&P in
addition to downside protection. Trajectory Asset Management, LLC, the
subadviser, utilizes a proprietary methodology to respond to current market
conditions with investments in S&P Index Futures and related options as
well as high-quality fixed income. Even in this volatile market, at a Fund's
maturity date, the High Watermark Funds are designed to return to shareholders
the highest net asset value achieved during the life of the Fund adjusted for
dividends, distributions and extraordinary expenses, subject to certain
conditions. The Funds' offer a unique commitment to preserve both principal and
any investment gains over the life of each Fund.

The commentary from the Trajectory portfolio team enclosed in this report
provides useful information which I urge you to read. We thank you for
including the SunAmerica Mutual Funds in your investment plan. We value your
ongoing confidence in us and look forward to serving your investment needs in
the future.

Sincerely,
