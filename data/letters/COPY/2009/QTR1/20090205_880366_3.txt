Dear Shareholder,
Economic growth in the U.S. was mixed during the reporting period ended
Looking back, third quarter U.S. gross domestic product (GDP)i growth was a strong However, continued weakness in the housing market, an ongoing credit crunch and soaring oil and food prices then took their toll on
the economy, as fourth quarter GDP declined The economy then expanded and during the first and second quarters of respectively. This rebound was due, in part, to rising exports that were buoyed by a weakening U.S. dollar
and solid consumer spending, which was aided by the governments tax rebate program. The dollars rally and the end of the rebate program, combined with other strains on the economy, then caused GDP to take a step backward in the third
quarter of According to the U.S. Department of Commerce, third quarter GDP declined In early December after the reporting
period ended, the National Bureau of Economic Research (NBER) announced that a recession had begun in December While one definition of a recession is based on having two consecutive quarters of negative growth (which has not yet
occurred), the NBER determined that a recession had already started using its definition, which is based on a significant decline in economic activity spread across the economy, lasting more than a few months, normally visible in production,
employment, real income and other indicators. Regardless of how one defines a recession, it certainly has felt like we are in the midst of an
economic contraction. Consumer spending, which represents approximately two-thirds of GDP, has fallen sharply. During November sales at U.S. retailers experienced their largest monthly decline in nearly years. In terms of the job market,
the U.S. Department of Labor reported that payroll employment declined in each of the of During as a whole, million jobs were lost, the largest annual decline since World War II ended in In addition, at the end of
the unemployment rate had risen to its highest level since January








Legg Mason Partners Mid Cap Core Fund

I







Letter from the chairman continued

Ongoing issues related to the housing and subprime mortgage markets and seizing credit markets prompted the Federal Reserve Board (Fed)ii to take aggressive and, in some cases, unprecedented actions.
Beginning in September the Fed reduced the federal funds rateiii from to This marked the first such reduction since June The Fed then reduced the federal funds rate on six additional occasions through April
bringing the federal funds rate to The Fed then shifted gears in the face of mounting inflationary prices and a weakening U.S. dollar. At its meetings in June, August and September the Fed held rates steady. Then, on
in a global coordination effort with six central banks around the world, interest rates were cut in an attempt to reduce the strains in the global financial markets. At that time, the Fed lowered the federal funds rate from to The
Fed again cut rates from to at its regularly scheduled meeting on On after the reporting period ended, the Fed cut rates to a range of zero to the lowest level since the Fed started
publishing the federal funds rate in In conjunction with its December meeting, the Fed stated that it will employ all available tools to promote the resumption of sustainable economic growth and to preserve price stability. In
particular, the Committee anticipates that weak economic conditions are likely to warrant exceptionally low levels of the federal funds rate for some time.
In addition to the interest rate cuts, the Fed took several actions to improve liquidity in the credit markets. In March the Fed established a new lending program allowing certain brokerage firms, known as primary dealers, to
also borrow from its discount window. Also in March, the Fed played a major role in facilitating the purchase of Bear Stearns by JPMorgan Chase. In mid-September it announced an billion rescue plan for ailing AIG and pumped billion
into the financial system as Lehman Brothers bankruptcy and mounting troubles at other financial firms roiled themarkets. The U.S.
Department of the Treasury has also taken an active role in attempting to stabilize the financial system, as it orchestrated the governments takeover of mortgage giants Fannie Mae and Freddie Mac in September In addition, on
the Treasurys billion Troubled Asset Relief Program (TARP) was approved by Congress and signed into law by President Bush. As part of TARP, the Treasury had planned to purchase bad loans and other
troubled financial assets. However, in November Treasury Secretary Paulson said, Our assessment at this time is that this is not the most effective way to use TARP funds, but we will continue to examine whether targeted forms of asset
purchase can play a useful role, relative to other potential uses of TARP resources, in helping to strengthen our financial system and support lending.








II

Legg Mason Partners Mid Cap Core Fund








The U.S. stock market was extremely volatile and generated poor results during the months ended Stock prices declined during each of the first four months of the reporting period. This was due, in
part, to the credit crunch, weakening corporate profits, rising inflation and fears of an impending recession. The market then reversed course and posted positive returns in April and May The markets gains were, in part, attributed to
hopes that the U.S. would skirt a recession and that corporate profits would rebound as the year progressed. However, given the escalating credit crisis and the mounting turmoil in the financial markets, stock prices moved lower during five of the
last six months of the period, including S&amp;P Indexiv declines of and in September, October and November, respectively. All told, the S&amp;P Index returned during the reporting period
ended Looking at the U.S. stock market more closely, its descent was
broad in scope, with every major index posting double-digit losses. In terms of market capitalizations, large-, mid- and small-cap stocks, as measured by the Russell Russell Midcapvi and Russell
Indexes, returned and respectively, during the period ended From an investment style perspective, growth and value stocks, as measured by the Russell Growthviii and Russell
Valueix Indexes, returned and respectively. A special note regarding increased market volatility
In recent months, we have experienced a series of events that have impacted the financial markets and created concerns among both novice and seasoned investors
alike. In particular, we have witnessed the failure and consolidation of several storied financial institutions, periods of heightened market volatility, and aggressive actions by the U.S. federal government to steady the financial markets and
restore investor confidence. While we hope that the worst is over in terms of the issues surrounding the credit and housing crises, it is likely that the fallout will continue to impact the financial markets and the U.S. economy well into
Like all asset management firms, Legg Mason has not been immune to these difficult and, in some ways, unprecedented times. However, todays
challenges have only strengthened our resolve to do everything we can to help you reach your financial goals. Now, as always, we remain committed to providing you with excellent service and a full spectrum of investment choices. And rest assured, we
will continue to work hard to ensure that our investment managers make every effort to deliver strong long-term results.








Legg Mason Partners Mid Cap Core Fund

III







Letter from the chairman continued
We also remain committed to
supplementing the support you receive from your financial advisor. One way we accomplish this is through our enhanced website, www.leggmason.com/individualinvestors. Here you can gain immediate access to many special features to help guide you
through difficult times, including:




Fund prices and performance,




Market insights and commentaries from our portfolio managers, and





A host of educational resources. During
periods of market unrest, it is especially important to work closely with your financial advisor and remember that reaching ones investment goals unfolds over time and through multiple market cycles. Time and again, history has shown that,
over the long run, the markets have eventually recovered and grown. Information about your fund
As you may be aware, several issues in the mutual fund industry have come under the scrutiny of federal and state regulators. Affiliates of the Funds manager
have, in recent years, received requests for information from various government regulators regarding market timing, late trading, fees, and other mutual fund issues in connection with various investigations. The regulators appear to be examining,
among other things, the Funds response to market timing and shareholder exchange activity, including compliance with prospectus disclosure related to these subjects. The Fund is not in a position to predict the outcome of these requests and
investigations. Please read on for a more detailed look at prevailing economic and market conditions during the Funds reporting period and to
learn how those conditions have affected Fundperformance. Important information with regard to recent regulatory developments that may affect
the Fund is contained in the Notes to Financial Statements included in this report. As always, thank you for your confidence in our stewardship of
your assets. We look forward to helping you meet your financial goals. Sincerely,
