Dear Fellow
Shareholder:

Last year saw the largest U.S. equity
market loss since the as measured by the Standard &amp; Poors

Index, which recorded its
worst annual return of since when the Index returned In this
difficult environment, the Westport Funds did well on a relative basis,
outperforming their respective benchmark indices for both the twelve month
period and the full eleven years since inception. Erosion of the foundation for
the financial markets and finally for the real economy started after domestic
housing pricesa
reached their peak in July
A clear indication that housing price appreciation had become a bubble and
that its deflation might cause significant damage to the financial system was
the collapse in June of two highly leveraged Bear Stearns &amp; Co.
sponsored hedge funds that invested in mortgage backed securities and their
derivatives. After additional financial market dislocations primarily involving
subprime mortgages, Federal Reserve Chairman Ben Bernanke estimated in August
that losses from defaulting mortgages would approximate billion. The
first half of saw losses from defaulting mortgages in the United States broaden beyond the subprime category.
By the end of losses were occurring from other credit and financial
instruments, other financial markets and in other countries, many unexpected. A
number of high profile U.S. financial institutions were forced to
merge, placed into conservatorship, allowed to go bankrupt or ceded substantial
ownership to the Federal government in exchange for a capital injection.
However, the most disappointing development was that financial and credit market
losses along with market and asset value dislocations began to disrupt the real
economy.

The spreading effects of the deflating
housing price bubble have produced the largest U.S. financial and economic contraction
since the The financial impact from loan losses on delinquent
U.S. residential mortgages is estimated by
Goldman Sachs economistsb
to hit trillion. The
economic impact is thought by some economists to be a contraction in fourth
quarter real GDP that might reach The second half contraction in the
U.S. real economy contributed through trade and financial markets to slowing
economic growth in a number of other countries spreading the recession
geographically.

The sharp slide in the economy and
deterioration in the financial markets caused substantial equity market losses.
Neither of The Westport Funds was immune to the equity market downdraft but
relative returns were positive. The return from the Westport Select Cap Fund
exceeded the return of its benchmark the Russell
Index by slightly more than
percentage point. Since its inception eleven years ago the Westport Select Cap
Funds average annual return has exceeded that of its benchmark by almost
percentage points. The return for the Westport Fund for exceeded that of
its benchmark, the Russell Midcap
Index, by more than
percentage points. Since inception eleven years ago the Westport Funds average
annual return has exceeded that of its benchmark by


















more than percentage points. Lipper,
Inc. classifies the Westport Fund as a Multi-Cap Core Fund since its portfolio
holdings include companies in each market capitalization category - small, mid
and large. The return for the Westport Fund exceeded that of the Lipper
Multi-Cap Core Index by more than percentage points for the year. Lipper
ranked the Westport Fund in the top of Multi-Cap Core mutual funds for the
and year time periods ended December and in the top for
Of course, these historical results do
not guarantee future performance.

Outlook

The bursting of the housing price bubble
and the continuing price slide spread credit losses from subprime to alt-A to
prime mortgages. Losses on mortgage backed securities and derivatives greatly
contributed to the demise of many large financial firms, especially those that
were highly leveraged. Financial system problems spread to the real economy
during last years final quarter, causing inventory liquidations, substantial
layoffs and credit losses on business loans and consumer loans not
collateralized by real estate such as credit cards and automobile loans. In
addition to the trillion in projected loan losses on delinquent
U.S. residential mortgages, Goldman Sachs
economists estimate losses from commercial real estate, credit cards, automobile
loans and business debt will total another trillion. Only half of these
trillion in projected losses have been recognized.b


The key to maintaining a functioning
financial system in this situation is to stabilize the banks even as they absorb
substantial credit losses. Credit is essential for a modern financial system
where economic growth is dependent upon debt financing. Loan losses not only
cause banks and other credit providers to deleverage and reduce available credit
as the value of collateral supporting existing loans declines, but deplete
capital needed for new loans. A damaged financial system can cause a negative
feedback loop to form with the real economy, that depresses consumer and
business activity.

There are relatively few historical
economic and financial system contractions that approach the current one in
magnitude. One of the best known is the Japanese experience in the when
government policy actions failed to restart economic growth after their real
estate bubble burst, resulting in Japans lost decade. In contrast,
Sweden in the early successfully dealt
with a similar episode, acting on its belief that banks were the key to
returning the nations financial system to stability. Swedish officials
aggressively injected capital into the countrys banks, purchased and wrote down
troubled assets and temporarily nationalized the weakest banks, even large ones.
Within two years the countrys currency was stabilized and the economy was again
growing at a reasonable rate.

The Federal Reserve and the U.S.
Treasury, undoubtedly influenced by Swedens success, have taken a number of
actions focused on supporting domestic banks as a key to stabilizing the
financial system and as a precursor to economic growth. Probably, the most
important accomplishment in the Treasurys recovery program was persuading
Congress to pass enabling legislation for the billion Troubled Asset Relief
Program


















(TARP) and using a portion of these
monies to inject capital, primarily through the issuance of preferred stock,
into a number of U.S. banks. While this action did not
prevent the real economy from contracting sharply in the fourth quarter in
response to deleveraging in the financial system, it did support the banking
system.

Other actions have been taken by the Fed
to support the financial system and economy including dramatically easing
monetary policy by reducing the fed funds rate from to nearly zero, the
provision of de-facto credit insurance for a broad range of non-government loans
and the execution of a new program under which it will purchase billion of
Government Sponsored Enterprise (GSE) mortgage backed securities and
billion of GSE debt to lower the cost of residential mortgages. The Obama
administration has proposed an billion stimulus package composed of
additional spending and tax cuts. Collectively, these efforts should help
stabilize certain financial markets and cushion the economy.

It is clear that this financial and
economic downturn is unique in both its size and geographic scope with credit
market and equity market losses worldwide in the current contraction estimated
at trillion. It is also clear to us that U.S. officials should continue to closely
follow the model of the Swedish intervention given the dearth of successful
precedents, even though differences in the size of the economy and the global
nature of the current contraction make the timing and the financial resources
necessary to renew economic growth uncertain. If Goldman Sachs economists
estimates of U.S. banking system loan losses of
trillion are correct, returning stability to the banking system will require
substantial added capital to offset projected losses. Raising the needed funds
must be accomplished carefully by the Obama administration so that new economic
imbalances are not created.

In summary, the logical pathway for
economic intervention in this instance is the Swedish model, but the challenges
to implementation require care. The near term results of the Federal
governments actions are unpredictable, but the odds of success improve
substantially with the passage of time. We appreciate the confidence of our
shareholders during this period as we endeavor to navigate these volatile
markets.


Sincerely,
