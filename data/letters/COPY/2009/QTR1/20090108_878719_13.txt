Dear Shareholders:

We are pleased to provide you with our annual report for the period ended
October on the Advisors' Inner Circle Portfolios managed by Thompson,
Siegel & Walmsley LLC (TS&W).
