Dear Shareholder:

In the sub-prime mortgage debacle revealed itself to be a much larger
problem than generally believed and, in fact, was symptomatic of a wide range of
abuses that had become embedded in the Nation's investment banking system. These
abuses have led to the collapse, or impairment, of major financial institutions
and resulted in major stock and bond market declines during the fourth quarter
of last year. Fear dominated the behavior of businessmen, consumers and
investors, while the economy slowed dramatically.

The Berwyn Funds did not escape what many have described as the worst bear
market since the Great Depression. However, I am pleased to report that for the
year each of our Funds did well versus their reference indices. As detailed in
the individual Fund reports, the Berwyn Fund outperformed the Russell Index
and the Berwyn Cornerstone outperformed the S&P Index as well as the S&P
Mid-Cap Index. The Berwyn Income Fund's performance compared favorably with the
Lipper Income Fund Index and Merrill Lynch High Yield Master II Index, but did
not do as well as the Citigroup Broad Investment Grade Index, which is weighted
heavily with Government bonds. Our performance was not an accident, in my
opinion, but was a consequence of our adherence to a value oriented investment
style that tempered the vicious declines in our stock and bond investments.
Moreover, our investments had minimal exposure to the residential housing and
mortgage banking sectors of the economy.

Despite my personal disappointment with the fraud, greed and recklessness
exhibited by the perpetrators of this global financial disaster, as well as the
failure of our regulatory bodies, I remain bullish on capitalism and the future
of the United States. Indeed, as horrific as it was, the action within the
equity markets paralleled the bottoming processes exhibited in previous bear
markets, most notably the downturn. In fact, it is possible that the
net asset values per share of The Berwyn Funds reached their nadir during the
third week of November. At this time, market volatility is less, the number of
stocks making new lows is minimal and the credit markets are beginning
to function more normally as compared to last autumn.

It is also fair to point out that this bear market is different. Bear markets of
this magnitude begin from a period characterized by excessive stock valuations
across a broad spectrum of equities and/or very high interest rates. Neither of
these conditions existed last year. Whereas falling stock prices and a reduction
in the money supply led to banking failures during the Bear Market,
the genesis of this decline was in the failure of certain segments within the
financial system. Clearly, the speed with which the large banking institutions
can be stabilized and confidence restored will be a major determinant in the
pace of our economic recovery.





Meanwhile, at The Berwyn Funds the team is working hard with its signature
passion for value investing. While we are proud of last year's performance, we
are far from satisfied and today we feel a little wiser, but maybe somewhat
humbler, than a year ago. We thank you for your continued confidence.

Very truly yours,

Dear Berwyn Fund Shareholder:

Berwyn Fund's ("BF") net asset value per share fell from to during
the calendar year. The decline included a small dividend payment of
per share at year-end. On a total return basis, the Berwyn Fund dropped
percent for the year.

The BF's primary benchmark, the Russell Index, fell percent for the
year, while the Russell Value Index declined percent. In addition to
outperforming these two indices, according to Morningstar the BF finished in the
upper of small-cap value mutual funds for the year.
Morningstar is the well-known mutual fund rating service that is recognized and
relied upon by many professional investors.

As we entered we thought that the stock market was fairly priced and, as
we said in last year's report, were anticipating a positive year. As the year
progressed, however, the scope of the sub-prime mortgage problem continued to
grow and the economy slowed. Nevertheless, we were not directly invested in the
housing or mortgage-related sectors and through the first nine months of the
year the BF's net asset value per share had fallen a modest percent. In
late September and early October, when the implications of certain derivative
financial instruments, including credit default swaps and structured debt
products came to light, the BF was faced with the most tumultuous financial
markets since the Great Depression. Small-cap stocks, which as a group had held
up very well earlier in the year, succumbed to widespread fear and panic
selling. Mutual fund redemptions, margin calls and the collapse of many hedge
funds contributed heavily to the "forced" selling of both stocks and bonds.
Volatility was incredibly high as confusion reigned about the future of the
Nation's economy.

================================================================================
BERWYN FUND
TOTAL INVESTMENTS AND CASH
(% OF TOTAL INVESTMENTS)
DECEMBER (UNAUDITED)

Dear Berwyn Income Fund Shareholder:

Total return to shareholders of the Berwyn Income Fund ("BIF") was negative
percent for the year ended December Net asset value ("NAV") per
share declined from on December to on December
The NAV declined in part due to distributions paid to shareholders. BIF
distributed quarterly dividends from net investment income totaling in
versus in No long-term or short-term capital gains
distributions were paid during

During the year, the sub-prime mortgage crisis expanded to such a degree that it
undermined the global banking system and investors' confidence in the future.
Amidst turbulent financial markets, the prudence of BIF's balanced approach to
investing, with its emphasis on in-depth analysis of individual securities, was
evident. As the year progressed, BIF used its relatively high quality fixed
income allocation and its cash reserves to gradually increase its lower rated
corporate bond holdings at, in our estimation, very favorable prices.

================================================================================
BERWYN INCOME FUND
TOTAL INVESTMENTS AND CASH
(% OF TOTAL INVESTMENTS)
DECEMBER (UNAUDITED)

Dear Berwyn Cornerstone Fund Shareholder:

Berwyn Cornerstone Fund's ("BCF") net asset value per share declined from
to for the year ended December BCF paid a modest income dividend
of for the year. BCF's total return for the year was negative
percent. BCF's return, while disappointing on an absolute basis, nevertheless
outperformed its primary benchmarks, the S&P Index and the S&P Midcap
Index, which declined percent and percent, respectively, during


================================================================================
BERWYN CORNERSTONE FUND
TOTAL INVESTMENTS AND CASH
(% OF TOTAL INVESTMENTS)
DECEMBER (UNAUDITED)
