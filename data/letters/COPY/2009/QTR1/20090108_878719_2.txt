Dear Shareholders:

ECONOMIC AND MARKET PERSPECTIVE

The United States, Europe, and Japan are all now in recession. The emerging
economies, especially China, are continuing to grow but at significantly slower
rates. Aggressive interest rate reductions by the world's central banks have
eased the global credit crisis, but interest rates remain somewhat elevated. As
growth has slowed, material prices have fallen sharply, and inflation is no
longer a concern. The dollar has risen over the period as hedge funds carry
trades and global investors sought safety. Corporate profits are under pressure,
and analyst estimates for reflect negative GDP forecasts. Managements have
little visibility for their businesses beyond the next several months. After the
severe decline, equity prices are discounting significant contractions in
economic growth and corporate profits, and valuations are more attractive.

PERFORMANCE

For the twelve months ending October the Portfolio's return of
was basis points greater than the return for the benchmark EAFE
Index. Stock selection helped performance while underweighting utilities,
telecommunications and consumer staples sector hurt results.

PORTFOLIO STRUCTURE

As of October the Portfolio was invested in countries. The Fund is
significantly underweight in Japan and Australia. There are no significant
overweights among the developed market countries. Emerging markets, which are
not included in EAFE, accounted for about of assets. The Portfolio is
overweight in industrials, information technology, health care, and energy.
Financials, consumer staples, utilities telecom services, consumer discretionary
and materials are underweight. On October the Portfolio was invested in
companies.

OUTLOOK

Markets await greater clarity concerning the depth and duration of the global
recession. Volatility will probably remain elevated until new consensus growth
expectations form. However, stock prices are likely to turn up before the
recession ends. Credit markets should continue to recover in response to well
coordinated interest rate reductions and capital injections. Until credit
spreads normalize, businesses






THE ADVISORS' INNER CIRCLE FUND MCKEE INTERNATIONAL
EQUITY PORTFOLIO
OCTOBER

may continue to horde cash. The de-leveraging process is likely to continue for
years with negative implications for growth. Lower commodity prices, especially
for energy, will aide consumers in the developed countries but will restrain
growth in the emerging economies as production is reduced. Recent price declines
have reduced equity valuations to multi-year lows. Stocks have good appreciation
potential once the global recession is fully discounted.

Yours truly,
