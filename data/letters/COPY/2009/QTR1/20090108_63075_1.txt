Dear Shareholders: 
The global economy is not a very
welcoming place these days. Headlines tell the story of slowing growth, accelerating inflation, and credit collapse. We have watched the rampant selling that has typified equity and credit markets since the strains in the financial system first
became apparent last year. 
The volatility in commodity and currency markets has further complicated investment choices. There are so many parts moving in
so many directions; it has become very easy to get overwhelmed. 
At MFS® we remind investors to
keep their eye on the long term and not become panicked by the uncertainty of the day to day. 
Remember that what goes down could very easily come back up.
And that is where we as money managers like to turn our focus. 
Investment opportunities may arise in declining markets. When markets experience substantial
selloffs, assets often become undervalued. At MFS, we have a team of global sector analysts located in Boston, London, Mexico City, Singapore, Sydney, and Tokyo working together to do the kind of bottom-up research that will root out these
investment opportunities. 
In times like these, we encourage our investors to check in with their advisors to ensure they have an investment plan in place
that will pay heed to the present, but that is firmly tailored to the future. 
Respectfully, 


