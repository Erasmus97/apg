Dear Shareholder:

We are pleased to present the annual report for the Acadian Emerging Markets
Portfolio. This commentary covers the twelve months from November to
October focusing on the portfolio's performance and some of the
conditions and decisions that impacted returns.

PORTFOLIO PERFORMANCE REVIEW

For the twelve months ended October the Acadian Emerging Markets
Portfolio returned versus for the IFC Investable Index, a
widely followed emerging markets benchmark.

ECONOMIC AND MARKET CONDITIONS

Emerging equity markets fell steeply over the period as the global economic
crisis deepened. Some markets held up quite well in the first half of the
period, bolstered in part by high commodity prices, but as oil prices began to
fall in the third quarter of the emerging asset class saw broad-based
declines. September and October were particularly difficult months amid a
stronger U.S. dollar, bleak macroeconomic data, heightened recession anxiety and
ongoing turmoil in the world's financial systems. In an environment of extreme
risk aversion, many investors pulled out of equities and sought the perceived
safety of government debt and other highly liquid investments.

Emerging equity markets, as measured by the IFC index, fell in USD terms
over the year, with all regions posting significant declines. In Latin America,
Brazil and Mexico were both down approximately Brazil was hurt by a
pullback in metals prices, a key export, while Mexico struggled with higher
interest rates and lower remittances from workers abroad. The decline in the
Europe/Middle East/Africa region was in part driven by Russia, which fell close
to as investors reacted to the aggression against neighboring Georgia.
Turkey posted a loss of approximately as positive reaction to renewed
political stability was offset by tepid domestic activity and waning business
sentiment. Asian emerging markets saw the Chinese market juggernaut begin to
lose steam, hampered by tighter monetary conditions, climbing inflation and
slower growth. China was down over for the year. Another high-flying Asian
market, India, saw a similar decline as inflation and higher borrowing costs
eroded consumer spending. Korea, Taiwan and Thailand were down in the
range for the year.

INVESTMENT STRATEGY USED DURING THE PERIOD

Acadian continues to pursue its highly structured and disciplined approach to
the emerging markets, using a database of information on over emerging
markets and over stocks. Our emerging markets process incorporates both
country selection and a variety of factors at the stock level. As a result, the
portfolio was invested in emerging equity






THE ADVISORS' INNER CIRCLE FUND ACADIAN EMERGING
MARKETS PORTFOLIO
OCTOBER

markets, with benchmark-relative overweightings in such markets as Taiwan,
Korea, Turkey, Brazil, Poland and Thailand. The Portfolio was underweighted in
China, India, Russia, Chile and South Africa.

The resulting portfolio had very attractive valuation characteristics, with a
price/book ratio, price/sales ratio and price/earnings ratio all lower than the
benchmark index. The Portfolio also held stocks that were on average somewhat
smaller in capitalization size than those in the IFC index. Materials, energy
and telecoms were significant active sector exposures over the year.

COMMENTARY ON THE FUND'S INVESTMENT PERFORMANCE

The Portfolio trailed the benchmark for the period, with most of the loss coming
from active stock selection. Country and sector allocations were also slightly
negative for return but losses here were relatively contained. Specific
investments impacting performance included the following:

HELPED PORTFOLIO

- - Market underweight in China

- - Market underweight in Russia

- - Stock selection in Brazil (offset by the market overweighting)

- - Overweight in Thailand (offset by stock selection)

- - Positioning and stock selection in telecoms, capital equipment and durables
sectors

HURT PORTFOLIO

- - Stock selection and a market overweight in Korea

- - Stock selection in Poland

- - Stock selection in Taiwan

- - Stock selection in Indonesia

- - Overweight in Turkey

- - Underweight in staples

- - Positioning and stock selection in finance, materials, technology and
energy sectors

CURRENT OUTLOOK

The past year has been turbulent for most asset classes, not just emerging
markets. In fact, the recent crisis has been driven not by emerging markets but
by financial problems arising in the developed world, underscoring the fact that
risks in industrialized countries can be unexpectedly high. In other words, the
current negative situation should be viewed as global, rather than arising from
issues inherent in emerging markets specifically.






THE ADVISORS' INNER CIRCLE FUND ACADIAN EMERGING
MARKETS PORTFOLIO
OCTOBER

Overall, we believe the emerging markets will weather this period. The larger
markets are in relatively good shape with regard to their sources of long-term
growth and other strengths, particularly the accumulation of large foreign
currency reserves. The more vulnerable ones have opportunities for assistance
from the IMF, the World Bank and other sources. While this assistance is not
unlimited, it should provide support through the immediate turmoil. Meanwhile,
stronger productivity growth in emerging markets bodes well for continued
economic growth and renewed currency appreciation over the longer term.

One favorable result of the recent turmoil is that emerging markets have reached
extraordinarily attractive valuation levels. On several key measures, including
price/earnings and price to cash earnings, we are now at levels not seen in over
years -- and this includes the periods of the Mexican, Asian, and Russian
crises of the

While we certainly could see further financial market distress over the near
term, emerging markets appear to be priced for a far greater global economic
downturn than seems plausible to occur. We believe a continued emerging markets
allocation made now will ultimately see strong rewards.

We hope this information has been helpful.

SINCERELY,
