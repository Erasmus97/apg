Dear Shareholder:

The year will go down in history as the year of the storm that
destroyed the credit markets and it is very difficult for me to imagine that any
other year in my lifetime will approach the magnitude and severity of events
that have unfolded over the past year. The carnage that remains in the wake of
this cataclysmic event has been incomprehensible to many familiar with the world
of finance (or at least the world of finance that we once knew). And for many, a
very important question that we all need answered is, "Is the worst finally
behind us and can we now look forward to the recovery that will help pull us out
of the abyss?" Unfortunately, I do not have those answers and can only guess as
we sail through these un-chartered waters.

In early we had expressed concern about the financial market turmoil that
had begun to unfold, stressing the importance of diversification and balance in
investment portfolios. Given the many issues that plagued the markets at that
time - deterioration in the housing market, growing weakness in the financial
sector, and the impending slowdown in economic activity worldwide - we
recommended a strategic shift away from credit risk and a focus on quality and
safety. Unfortunately, our greatest fears were realized leading to a systematic
re-pricing of risk that sent the world's financial markets into a tail spin.

Perhaps most noteworthy were the failures of Wall Street titans, Bear Stearns
and Lehman Brothers, as shock waves from their failures rippled across the
globe, requiring unprecedented Central Bank actions to save the world's
financial system from collapse. Credit markets froze and a liquidity squeeze
ensued, forcing the U.S. Treasury's takeover of mortgage firms Fannie Mae and
Freddie Mac and the Federal Reserve entering into a revolving credit facility
agreement with American International Group (AIG). These events, along with
others, will be a story for our grandchildren and the subject of many a

Bishop Street Funds


(BISHOP STREET FUNDS LOGO)

business school lecture no doubt, surpassing even the tales of Long-Term Capital
Management, the Japanese bubble, and more.

So what does the future hold? More pain and uncertainty as well as a new
President to lead the charge. But with these seemingly insurmountable near-term
challenges comes undeniable opportunity to re-engineer the "system," to fix what
is broken, and to create solutions that will help to make the system and the
world's economy better and stronger.

Over the years, we have consistently urged our shareholders to take a
longer-term view and apply sound diversification principles when designing an
effective investment program, and these basic principles have become even more
important today. Our Bishop Street Funds are managed using the same basic
principles that have guided our investment thinking over the years. And with our
portfolio management teams focused on each Fund's investment goals and
objectives as well as the consistent execution of our investment strategy, our
Funds continue to offer excellent diversification across industries, sectors,
and individual issues. Lastly, we continue to believe that a balanced portfolio
comprised of a combination of our equity and fixed income funds should serve as
the building blocks of your carefully designed investment portfolio, reflective
of your investment goals and risk appetite.

Thank you for investing in the Bishop Street Funds. We appreciate your continued
trust and confidence.

Sincerely,
