Dear Shareholder:

The year was an extraordinary and stressful time for investors and those of
us who have worked in financial markets for many years. A dramatic, synchronized
global slowdown amid severe economic conditions pushed market volatility to
record levels. Most major equity indexes suffered double-digit losses for the
year. Although this difficult environment is bound to provoke great concern, we
think it is important to put short-term market developments in perspective. Keep
in mind that as daunting as current conditions may be, we have navigated through
other periods of high market volatility, such as the stock market crash of
We remain committed to our long-term perspective and our value investment
philosophy. Therefore, we view recent declines as potential opportunities to
find bargains that we believe may be well positioned to become eventual winners.
Although conditions remain challenging, our experience gives us ample reason to
be optimistic about future market stabilization and recovery.

In the enclosed annual report for Templeton Global Opportunities Trust, the
portfolio manager discusses market conditions, investment management decisions
and Fund performance during the period under review. You will also find
performance data and financial information. Please remember that all securities
markets fluctuate, as do mutual fund share prices.

Sign up for EDELIVERY of your Shareholder Report

Shareholders who are registered at franklintempleton.com can receive this report
via email by selecting eDelivery options under "My Profile." Not all accounts
are eligible for eDelivery.

NOT FDIC INSURED | MAY LOSE VALUE | NO BANK GUARANTEE


Not part of the annual report |



If you would like more frequent updates, FRANKLINTEMPLETON.COM provides daily
prices, monthly performance figures, portfolio holdings and other information.
You can also access your account, buy and sell shares, read timely articles, and
find helpful financial planning tools. We hope you will take advantage of these
online services.

Historically, patient investors have achieved rewarding results by evaluating
their goals, diversifying their assets globally and maintaining a disciplined
investment program, all hallmarks of the Templeton investment philosophy
developed more than years ago. We continue to recommend investors consult
their financial advisors and review their portfolios to design a long-term
strategy and portfolio allocation that meet their individual needs, goals and
risk tolerance. We firmly believe that most people benefit from professional
advice, and that advice is invaluable as investors navigate changing market
environments.

We thank you for investing with Franklin Templeton, welcome your questions and
comments, and look forward to serving your investment needs in the years ahead.

Sincerely,
