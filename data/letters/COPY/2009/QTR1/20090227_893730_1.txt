Dear
Shareholder:
Enclosed is the report of
the operations for the Williston Basin/Mid-North America Stock Fund (the
"Fund") for the year ended December 31, 2008. The Fund's portfolio and related
financial statements are presented within for your review.
Management's Discussion & Analysis
Williston Basin/Mid-North America Stock Fund (ICPAX)
outperformed the S&amp;P 500 and Russell 3000 Indexes for 2008 and our strategy
was recognized in 2008 by being given 4 Stars overall by Morningstar. I say
given and not earned, because recognition like this comes not because of my
particularly unique talents as a fund manager, but because we have consistently
followed a disciplined investment policy. Fund managers with good track records
receive far too much credit for performance and far too little for consistency
of investment discipline.
Periodically events conspire to make us lucky or unlucky, no matter how well-conceived the discipline or how rigorously followed, which is why we should never invest solely for the track record. We should invest because we understand the investment discipline and the fund manager's views on life, investing, the universe and everything, and they are similar to ours. Investors can't know all that, of course, but we hope this letter will help you
better understand our world view. It will make the inevitable disappointments
easier to bear, and enable us to avoid getting too carried away during the also
inevitable good years.
But first the grim details for 2008. The Fund's raw numbers
total return for 2008 was -19.36%, vs. -37.00% for the S&amp;P 500 and -37.31%
for the Russell 3000. The Fund's cash position at
year end was 14.1%, the weighted average market cap at year end was
approximately $5 billion, comprised of 25 companies. The biggest losses for the
Fund this year have been in the energy and natural resource sectors.
As you can see from the table of risk-adjusted returns below, the Fund's risk has risen dramatically, along with that of the stock market, by more than that of the S&P 500 and the Russell 3000, despite a large cash position for most of the year. While higher volatility is to be expected for any fund that maintains a large position in natural resources stocks, it is also important to keep portfolio level risk under control, and to ensure that our higher risk is accompanied by higher risk-adjusted returns. So far we have been fortunate enough to have done this. Volatility for the market overall seems to have peaked in November 2008, and has been dropping irregularly since then. We do, however, expect volatility to remain high in 2009 as well.
Risk-Adjusted Return Comparison
 

				12 months ended 12/31/08
 
 
Raw
Standard
Risk-Adjusted
 
Return
Deviation
Return
ICPAX
-19.36%
8.69%
-2.23%
S&P 500
-37.00%
6.66%
-5.56%
Russell 3000
-37.31%
6.00%
-6.22%
Note:
Past risk-adjusted returns may not be a reliable guide to
future risk-adjusted returns. 
When returns are negative, relative risk-adjusted returns
may be misleading because they are not intuitive.
Fund Investment Objective Changes
You have no doubt received literature on the nature of the changes to the objectives of the Fund in 2008, so I won't go into them again here. I'd like to discuss the strategic implementation of its focus on mid-continent North America. The Fund's goal is to give investors exposure to natural resources, but to keep risk well below that of a pure natural resources fund and provide risk-adjusted returns better than those of the overall stock market. Holdings will fluctuate around an average of 50% in natural resources and 50% in non-natural resources related companies.
The main vehicle of risk control is the expansion and contraction,
as a percent of Fund assets, of the natural resources half of the Fund. Maximum
exposure during periods of rising prices for natural resources could rise above
50%, while during unfavorable periods it could shrink significantly while the
non-natural resource portion expands above 50%. Because most of the volatility
(risk) will be the result of changes in natural resource prices, it makes sense
to make expansion and contraction of this half of the Fund the primary risk
control tactic.
The strategic objective of structuring the Fund this way is
to balance the unusual volatility of natural resources stocks with investment
in more conservative, non-natural resources companies in the same regions.
Also, if the mid-continent region does experience greater economic success due
to the world's rapidly-growing demand for energy and all the natural resources and materials required for what is likely to be the biggest infrastructure development push in human history, other companies in the same region will benefit as well. The Upper Midwest, though, especially the Williston Basin region containing the Bakken Shale formation, is very thinly populated. While it's an area of intense exploration and development activity in oil and natural gas, investment necessarily involves companies mostly headquartered in other parts of the mid-continent.
We also define the term 'natural resources' very broadly in the Fund, in order to give our investors the widest possible scope for taking advantage of the region's resources and achieving a wider diversity of natural resource investments. As a result, it will include not just oil & gas companies like EnCana (ECA), but metals and natural fertilizer mining operations like Stillwater Mining (SWC), agriculture, wind power generation, and geothermal resources operations such as Ormat Technologies (ORA). We also view natural resources stocks as including mid-continent-based service companies like National Oilwell Varco (NOV), equipment companies like Caterpillar (CAT), and storage & transmission companies like Plains All America Pipeline (PAA).
On the western margin of the region there are significant geothermal fields, as well as strategic metals and gold mining companies. The Upper Midwest is one of the two largest sources of wind energy in North America. The other is the Southwest, some of which is our mid-continent region. The Upper Midwest, encompassing as it does the Great Plains, is an enormous agricultural resource. In addition, the company can hold as much as 20% of assets in companies outside the mid-continent that participate to a significant degree in the development of the area's resources. An example would be the world's largest wind turbine manufacturer, Denmark-based Vestas Wind Systems (VWDRY).
Most natural resources funds are globally diversified. While that can be an advantage sometimes, it increases sovereign risk (adverse political changes locally, such as Nigeria, for example), currency risk and environmental risk. Restricting the Fund's operations area to the mid-continent puts the companies we purchase for you in two of the best-regulated parts of the world, and in stable, first world democracies. Natural resources development is, at its best, harmful to the environment, but we all need these resources to generate power, make essential materials and feed the hungry throughout the world. We feel that the Fund offers a superior environmental choice for investors who are nevertheless interested in natural resources.
Finally, most natural resources are internationally traded
in dollars. Given the level of credit creation that will likely be necessary to
get the US economy moving again, inflation is a serious concern at some point
in the not too distant future. Natural resources stocks should better reflect
the higher prices in dollar terms resulting from inflation or a weaker dollar
than would many other US stock investments. Exposure to the Canadian dollar, a
strong, but natural resource-based democratic economy, should also offer some
degree of diversification in the event of dollar weakness or inflation.
We welcome any questions you may have on the new objectives
of your Fund, and would like to thank you once again for your loyalty and trust
in our management methodology during a period of great market and economic stress.
If you would like more frequent updates, visit the Fund's website
at www.integrityfunds.com for daily prices along with pertinent Fund information.
Robert Loest, Ph.D., CFA


Dear Shareholder:
Enclosed is the report of the operations for the Integrity Growth & Income Fund (the "Fund") for the year ended December 31, 2008. The Fund''s portfolio and related financial statements are presented within for your review.
Management's Discussion & Analysis
Integrity Growth & Income Fund (IGIAX) became a no-load fund again in 2008. We continued to outperform the S&P 500 for 2008 while keeping overall risk low. Our strategy was recognized in 2008 by being given the prestigious Lipper Leader designation by Morningstar. I say given and not earned, because recognition like this comes not because of my particularly unique talents as a fund manager, but because we have consistently followed an ethical and disciplined investment policy. Fund managers with good track records receive far too much credit for performance and far too little for consistency of discipline.
We continue to keep risk as low as possible through our use of the four basic strategies of 1) consistent, large free cash flows and high ROICs; 2) balancing the fund internally by offsetting cyclical issues with non-cyclical and counter-cyclical companies, as well as diversifying by currency and by geography; 3) employment of portfolio level statistical risk management tools; and 4) buying only stocks that have clear long term drivers, are priced well below fair value based on fundamentals, and that don't require that we step outside our moral universe to make money; and selling them when they reach our estimate of fair value.
The Fund's cash position at year-end was 18.4%, and the average weighted market cap was approximately $19 billion. Risk is still high, but seems to have peaked, although day-to-day and monthly volatility is likely to remain high for an extended period of time. It has been widely noted in the financial press that volatility (risk) rose dramatically over the last year. It rose for the Fund as well, albeit much less, thanks to our risk management discipline. Since our 2007 year end report to you, the Fund's standard deviation of monthly returns (a standard statistical measure of risk) rose from 3.87% to 6.49%. That of the S&P 500 rose from 3.67% to 6.66%. At the end of 2007 the Fund was slightly riskier than the S&P 500. At year-end 2008 the S&P 500 is slightly riskier than the Fund and experienced a much greater loss.
In our opinion, risk-adjusted returns are the best measure of the ability of a fund manager. There is no other way to compare management ability, and we believe all funds should present standardized risk-adjusted results for this reason. As you can see from the table below, although we have lost money, we have been successful in improving our risk-adjusted returns from 2007 relative to the S&P 500. We trust that this will result also in improved risk-adjusted returns when the market recovers.
Risk-Adjusted Return Comparison
IGIAX vs. S&P 500, 12 months ended 12/31/08
 
Raw Return
Standard Deviation
Risk-Adjusted Return
IGIAX
-27.10%
6.49%
-4.18%
S&amp;P
      500
-37.00%
6.66%
-5.56%
Past risk-adjusted returns may not be a reliable guide to future risk-adjusted returns.
Through a Glass, Darkly
Let's think back to the best times to invest over the last century. They have inevitably followed market crashes due to major changes or disasters such as the period following the end of WW I and a long economic malaise; after the market crash in 1929 bottomed out in 1932 preceding the Great Depression; after the market plummeted in 1973  '74 during Vietnam and oil price spikes wrecked the US economy, and created rising inflation and economic stagnation. While the conditions, timing and causes this time are different, they are always different. What isn't different is that once again we are seeing the potential for investors to create long term value with far less risk than has been the case now for decades. I'm not going to promise you we'll make money for you this year. That is beyond my skills as a manager. All I can promise is that we'll do our best to keep risk low, follow our investment and risk control discipline, and invest with pr
omise for the future. The rest is beyond my control as a mere human being.
Yields on short term Treasury securities are now well below 1%, and money market fund and CD rates are quickly heading there. From recent Fed comments, they are likely to stay low for as long as it takes to stimulate a decent recovery. Given the magnitude of Americans' lack of financial responsibility at all levels over a half century; the consequent destruction of debt and capital; the fact that real estate turnarounds normally take many years; and what is likely to be "official" unemployment (e.g., distorted by previous Presidents to look lower than they really are) approaching 10% and unofficial unemployment approaching 20%, I expect rates to stay low for many years.
This will be good for borrowers, but for shorter term lenders like retirees it could mean a much lower standard of living. If rates stay very low  and the Federal Reserve indicates they will keep rates low for as long as it takes - many of these folks will become increasingly desperate for income, as they could easily see their incomes drop 10% - 50% from declines in short term Treasury yields, and who wants to lock up their money for 10 years at 3.0%? High, secure dividends are likely to become a major income source for a significant number of people who have been living on CDs and short term Treasury rates via money market mutual funds.
We believe it's important for investors to understand that capital gains-oriented investors in stocks over the last 25 years have been driven primarily by P/E multiple expansion. This was caused by interest rates falling from the high teens in the early 1980s to 1% earlier this decade. At current rates there is little potential for further market multiple expansion from this source, only from economic expansion, and that seems most unlikely for at least a couple of years.
These conditions are much more likely to produce a more dividend income-oriented investment climate than any we have seen since the 1950s & 1960s. It is our belief that investors must adapt to this reality for some years to come, and we have been adjusting your Fund to reflect these changes in the investment climate over the second half of 2008. In the shorter term, though, we could see US stocks make new lows the first half of this year.
Because US general economic growth is unlikely to return to previous levels for years, if ever, buying stocks for growth or capital gains may become an exercise in frustration for the remainder of 2009. In my opinion, investors have not yet fully discounted the enormous amount of capital and credit destruction in the US. There is simply no way to print our way out of it, it will probably take many years to rebuild what has been lost. It will require higher personal taxes, much higher savings rates, and frankly a lower standard of living.
I am not a long term investor. I'm not a trader either, I'm merely an investor, and frankly, I'd prefer to realize your profits sooner rather than later. Self images like these can lead us astray when conditions change. Sitting on a big profit in a lousy market because I think I'm a long term investor' can be a recipe for poorer returns than otherwise. Until growth returns, we are likely to depend to a greater extent on income and short term capital gains.
However, it's not all bad news. That's mostly only in the short term. When (and if'  I've been wrong before) the market bottoms this year, we are likely to see dividend yields and capital gains potential for the next few years that are greater than anything we've seen since the beginning of WWII. That's the hope. At worst I'm wrong short term about a lower market ahead sometime in the first half, and we'll be very conservatively invested with higher than normal income for a pure stock fund.
Environmental Stewardship
We would like to illustrate the deeper part of our investment obligation as an ethical fund with a mining company, Stillwater Mining Co. (SWC). It's the only significant source of platinum group metals in North America, and by far the largest source globally after the repeated failures of their only two major competitors in South Africa and Russia. Platinum group metals (ruthenium, rhodium, palladium, osmium, iridium, and platinum), because of their atomic properties, are widely used catalysts for many chemical processes without which life today would be very different - auto catalysts, fuel cells, hydrogen purification, electronics, dentistry, medicine, water treatment and many other uses. If you drive a Toyota Prius, for example, your emissions would not be effectively zero without these catalysts. Some of the life-giving drugs we use would not be possible without them.
That doesn't mean we have to own them, of course. Most ethical investors avoid mining companies because their operations can be quite toxic for the environment and their managements are more often than not irresponsible. Yet we all use and benefit daily from products that would not be possible without these metals. We cannot boycott their products like we do many others, but in a few cases we can own companies whose managements demonstrate an unusual level of care for the communities that support them, as a model for the rest of the world.
In 2000 Stillwater Mining, as a result of pressure by local activist groups, agreed with the Stillwater Protective Association (SPA) in central Montana, a group of local ranchers, conservationists and citizens, to create the Stillwater Good Neighbor Agreement. It's the only agreement like it in the world between local communities and conservationists and a hardrock mining company. In 2005 the agreement was further strengthened by mutual consent. This is a legally binding agreement between SWC and the surrounding communities to help ensure the most environmentally responsible operations possible for the mines.
Among other things, the agreement includes:
1) a provision for the independent review of the company's performance bonds (a financial assurance deposit) and reclamation plans every five years;
2) water management plans for both mines that ensure that 90% of pollutants are treated for the duration of mining operations;
3) citizen access to information through audits of the mine's compliance with clean water and air laws, independent reviews, and an open-door policy between the mine and community;
4) better protections for clean air and water through a baseline water quality study and strict water pollution limits; 
5) protection for open space through over 2,220 acres of conservation easements; pilot testing and field study of innovative new technologies to address mine waste tailings; 
6) providing citizen oversight of mining operations to ensure protection of the area's quality of life and productive agricultural land; 
7) establishing clear and enforceable water quality standards that go above and beyond state requirements;
8) a volunteer, citizen-based water sampling system to monitor water quality of the East Boulder and Stillwater watersheds.
This doesn't ensure there won't ever be problems or disagreements, of course, or even that the agreement will last forever. It doesn't mean SWC is the ideal company, although it also goes well beyond even the requirements above in terms of community relationships. The company's management, and its surrounding communities and organizations working together, have created a model agreement over many years that can be used throughout the world to create cleaner environments and healthier communities, while still allowing people to enjoy the benefits of all the products made possible by these rare catalyst metals. It's a model for other mining company managements to follow.
We have not lost sight of our obligation to the rest of humanity to walk as lightly as possible upon the Earth that is our only home. The stocks we have purchased for you have been bought with that always in mind, and we welcome any questions you may have regarding your fund.
Thank you again for your trust, and may 2009 bring you peace, wisdom and enjoyment.
If you would like more frequent updates, visit the Fund's website at www.integrityfunds.com for daily prices along with pertinent Fund information.
Robert A. Loest, Ph.D., CFA


Dear Shareholder:
Enclosed is the report of the operations for the Integrity High Income Fund (the "Fund") for the year ended December 31, 2008. The Fund's portfolio and related financial statements are presented within for your review.
Market Recap
The high yield market just experienced its worst year on record, plunging 26% in 2008, a far worse drop than the prior annual record set in 1990 (-9.59%). The year began poorly as the market posted three consecutive negative returns in the first quarter. April ushered in a slight rebound as the market posted one of its best monthly returns ever. But after meager performance in May, the macro background weakened significantly and the high yield market marched into negative territory where it remained for the balance of the year. July and August were the calm before the storm. September, October and November marked the three worst months of performance for the high yield market. In total, the market lost nearly one-third of its value in that time. After bottoming on December 16th, the high yield market rallied through month-end to post its second best month on record. Despite the rally, the high yield market still finished down for the year.
As would be expected in a flight to quality environment, double-B credits significantly outperformed both single-B and triple-C rated credits for the year. Though all sectors ended the year in negative territory, wireless credits led all sectors closely followed by healthcare, aerospace/defense, electric and packaging as measured by the Barclays Capital U.S. Corporate High Yield Index. The worst returns were experienced by REITs, gaming, media non-cable, building materials and refining sectors.
The yield-to-worst on the Barclays Capital U.S. Corporate High Yield Index ended the year at 19.43% as spreads widened 1,093 basis points (bps) to +1662 on an option-adjusted (OAS) basis. For the year, the Barclays Capital U.S. Corporate High Yield Index returned -26.15%, significantly underperforming the 10-year Treasury (+20.22%), while outperforming both the Merrill Lynch High Yield Master II Constrained Index (-27.10%) and the S&P 500 Index (-37.00%).
As spreads widened significantly through 2008, default rates began to rise. During the year, the 12-month trailing domestic default rate increased from its record low of 0.34% at the end of 2007 to 2.25% at the end of 2008. This level, though, is still well below historical average of 4.34%.
High yield issuers priced $52.9 billion in 2008, a 64% decline from the $147.9 billion priced last year and the lowest volume since 2000. Though issuance levels first dropped beginning in May, issuance activity was virtually nonexistent during the last five months of the year. Activity will likely remain muted given the environment and risk-averse appetites. In 2008, high yield mutual funds experienced $2.7 billion inflows, compared with $5.0 billion inflows during 2007. Year-to-date redemptions, maturities, tenders and upgrades to investment grade totaled $96.1 billion, versus $158.5 billion for the same period last year. These activities effectively create additional market demand for remaining issues.
Portfolio Performance and Positioning
For the period since inception (4/30/04), the Integrity High Income Fund Class A and Class C returned -5.38% and -6.16%, respectively compared to a -1.19% return for the benchmark (Barclays Capital U.S. Corporate High Yield Index) and -1.28% for the Merrill Lynch High Yield Master II Constrained Index for the comparable period. During the period, performance relative to the constrained index was aided by our security selection in the media-cable, gaming and telecommunications sectors with the largest performance contribution coming from our relative weightings in GMAC, Brigham Exploration, Echostar DBS Corp., DirecTV Holdings/Finance, and Energy Future Holdings. Underperforming sectors included our relative exposure to home builders, technology, and consumer products. Performance was hindered by our relative weightings in Sirius Satellite Radio, Tousa, Simmons, American Axle and Manufacturing, and Smurfit Stone Container.
The portfolio is currently overweight to the consumer products, automotive, and media-cable sectors. This is driven by our view of the relative value opportunities within those sectors. The portfolio is currently underweight to electric utilities, home construction, and paper sectors. We are not compelled by the valuations within these sectors due to challenging fundamental outlooks or rich valuations.
Market Outlook
In the short-term, we expect volatility to remain elevated, reflecting the uncertain economic environment. However, we believe current valuations already factor in substantial weakness and will ultimately prove attractive as we expect spreads will tighten as current levels overly discount our expectation for defaults. We expect that protracted economic weakness will drive the default rate higher in 2009, but believe it will remain below market expectations due to low refinancing requirements, flexible debt terms and reasonable corporate cash flows. We expect the forced selling pressure as a result of hedge fund de-leveraging in October and November to abate and we anticipate that attractive valuations will create significant demand for the asset class for both mutual funds and institutional accounts. Security and sector selection will be extremely important as we manage through 2009 and we will rely on our bottom-up security selection process to
 capitalize on dislocations in relative value.
If you would like more frequent updates, visit the Fund's website at www.integrityfunds.com for daily prices along with pertinent Fund information.
Sincerely,


