Dear Shareholders:

GLOBAL CREDIT CRUNCH AND BEAR MARKET - The year brought a
once-in-a-lifetime collapse in global financial markets, with stock prices
declining and credit conditions deteriorating virtually everywhere. No market,
whether developed or emerging; oil producer or oil consumer; capital saver or
capital spender; was spared. Storied firms like Lehman Brothers and Merrill
Lynch breathed their last in victims of reckless lending and investing.
Household net worth dwindled with house prices and stock values shrinking so
precipitously as to put economic activity into a sharp decline in the fourth
quarter. And in a cruel bit of irony, the year was punctuated by a billion
Ponzi scheme that further sapped confidence and faith in American capitalism.
What the big bear markets of the postwar period did in two years or
three years did in less than months, producing more fear and
volatility and negative returns than all but the most wizened of today's
investors had ever experienced. This secular decline for equities represents the
unwinding of the postwar build-up in debt throughout the U.S. financial system.

FINANCIAL CONDITIONS SHOW SIGNS OF IMPROVEMENT AS BEGINS - While risk
aversion was the order of the day last year, there were some signs as the new
year began that investors might be a bit more inclined to take on risk in
For one thing, last year's indiscriminate selling created some attractive
opportunities in the stock and credit markets. Over the past month, credit
spreads between risky securities and U.S. Treasuries have narrowed by some
three-quarters of a percentage point, and the S&P price/earnings multiple
has bounced off of lows.

It may take longer for economic activity and corporate profits to turn higher.
Economic data for the month of December have been decidedly weak, and leading
indictors suggest that economic activity is likely to stay soft at least through
midyear. Given that the U.S. economy is judged to have peaked in December
a decline longer than months in duration (the case in and
and deeper than the peak-to-trough declines in GDP seen in those two
recessions is possible. Fortunately, U.S. monetary and fiscal policy has become
increasingly aggressive - which may set in motion inflation pressures down the
road, but that is a worry for another day.

THE ECONOMIC POLICY RESPONSE - The Federal Reserve (the "Fed") is having some
success in lowering mortgage rates. House affordability has improved
significantly due to the declines in rates and prices, and increased real estate
traffic even in the softest markets suggests that the worst of the forecasts for
how much more house prices need to decline are exaggerated. The European Central
Bank is playing catch-up in the area of interest rate reductions, but further
rate cutting is expected, and central banks everywhere are in easing mode.

The Congressional Budget Office is projecting a trillion deficit for the
U.S. in fiscal The stimulus plan working its way toward President Obama's
signature may produce a trillion dollar deficit in as well. In the
President Reagan's Budget Director, David Stockman famously predicted
billion budget deficits as far as the eye can see. Since Reagan's inauguration,
the average federal deficit has in fact been billion or about of U.S.
GDP. In today's bigger economy, a trillion deficit equates to of GDP, a
postwar record, but not entirely out of line with our history in deep recessions
- - e.g., in fiscal While one wonders how many bridges to nowhere might
be included in such a budget, there is reason to think it will produce a
worthwhile stimulus for the U.S. economy.

THE TOPSY-TURVY ENVIRONMENT OF VIRTUE AS A RISK - When an individual
increases his savings, his financial condition improves. When too many
individuals boost savings, the resulting downturn in spending can crimp the
overall level of economic activity, having the perverse result of weakening
everyone's finances. For the U.S., where savings rates have been in decline for
years, there may be no way around an increase in savings (deferred spending)
ahead, since the Chinese and others that we have relied on to finance our growth
may no longer be willing to invest as freely in U.S. dollar assets. While
becoming more self reliant - whether it be in savings or in energy - may be a
laudable long-term objective, realistically we can't get from here to there
without a period of reduced economic growth.

While the private sector is deleveraging, the government is rapidly adding debt
- - including an unprecedented expansion of the Fed's balance sheet. Trying to
cure the economy's excess of debt by throwing trillions of dollars in new credit
facilities at the financial system is at the very least counterintuitive. The
point of this expansion of the government sector is to mitigate some of the pain
caused by the contraction in the private sector; the risk in this unbridled
growth in the public sector is a return of inflation and debasement of the U.S.
dollar. Investors have already endured a lot of pain, and workers and consumers
are increasingly finding their finances strained by the recession of
which stands to be the worst since It stands to reason in this
environment that volatility will remain elevated.

THE OUTLOOK FOR INFLATION AND THE ECONOMY - As gets under way, there is
some reason to think that the economy will get back into a growth mode before
the end of the year. The Fed's stimulation efforts and the bailout steps taken
by the Fed and Treasury have put the financial system on a sounder footing.
Policy efforts have provided new capital to financial institutions and brought
down the short-term interest rates at which banks borrow. Bond issuance picked
up late in the fourth quarter, also a signal that the credit markets are
loosening. Mortgage rates have declined, with the rate on conventional mortgages
down close to in early In addition, spending power for both consumer
and businesses has benefited from the drop in energy prices, which are less than
one-third their peak levels.

Before the economy returns to a growth mode, Wright expects that GDP will
contract for at least the first two quarters of and that the recovery may
be subpar for a while thereafter. With home values still falling and stock
market values below their peaks, consumer balance sheets are in need of
strengthening. Add to this the prospect of a weak jobs market through and
it looks like consumer spending, which constitutes of U.S. economic
activity, will be soft for several more quarters even with the benefits of lower
energy prices and the Obama stimulus package. In the short run, we expect
inflation to moderate, possibly by a good amount. The inflationary effects of
increases in money supply resulting from the liquidity the Fed and Treasury
continue to pump into the financial system are likely to be offset to a large
extent by the deflationary pressures of a global economy operating well below
its potential. During the first half of headline inflation will be
depressed by the sharp decline in energy prices compared to a year ago, and
sellers of most consumer goods are cutting prices as much as possible to
stimulate demand. Eventually the huge increase in the nation's money supply may
contribute to higher inflation, but that shouldn't be a problem in

STOCK AND CORPORATE BONDS ARE PRICED AT ATTRACTIVE LEVELS - By virtue of the
worst markets in over years, stock prices and corporate bond yield spreads
(the excess over Treasury yields) have reached levels that one would have to
consider bargain basement cheap. At their lows, stocks were discounting severe
economic distress, the worst conditions in perhaps years. At their widest,
corporate yield spreads were priced for depression.

Investors struggling with what the losses of mean for their retirement
plans are understandably doubtful about trusting stocks. The financial
crisis and its resolution will have a significant impact on the U.S. economy and
financial markets for some time to come. Certainly the effects of tight credit
and a mood of risk avoidance will take a long time to be purged from the economy
even after recovery begins. One likely positive is that, to the extent that
excessively risky behavior is reduced through regulation or self-discipline, a
stronger financial system will come out of this turmoil. Human nature being what
it is, the risk/return (greed/fear) cycle will come round to these same mistakes
again, but we hope not for a long time.

We expect that over the next several years both stocks and bonds will provide
investors with positive real investment returns. In our view, the
financial crisis, like earlier ones, will create as many opportunities as
challenges. While the bottom in stocks may not yet have been seen, we take
encouragement from the cheapness of equities and the fact that credit conditions
are starting to improve.

Sincerely,
