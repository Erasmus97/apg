Dear Fellow Shareholders,
 
The 12-months ended 10/31/2008 were about as bad as it gets for stock investors. Only once in the past 70 years has the S&amp;P 500 lost more over a 12-month period, and that was 34 years ago in the 12-months ending September 1974. Before that instance, you have to go all the way back to May 1938 to find a 12-month loss worse than the -36.08% suffered by the S&amp;P 500 over the past year (the Wilshire
        5000 was down -36.43%) for the same 12-month period.
 
Disappointingly for us, Upgrading wasn’t able to provide any shelter in the midst of this market storm. SMI’s pure Upgrading fund, The Sound Mind Investing Fund (SMIFX) was down
-39.86% for the year, slightly worse than the benchmark’s performance. The SMI Managed Volatility Fund held up somewhat better, dropping -28.51% for the period.
 
The steep declines of Upgrading were a surprise for many of us that followed the same general approach through the bear market of 2000-2002. Then, Upgrading was able to cushion much of the downside, allowing many Upgraders to trim losses considerably vs. the market’s returns (though Upgrading still lost money during the period).
 
A key difference between this year’s bear market and the 2000-2002 period has been the breadth of the decline. There simply haven’t been any areas of strength within the stock market (or anywhere else, for that matter) in which to hide. In the last bear market, small-value, mid-value, real estate, and a handful of other fund types were able to completely sidestep, or at least minimize, their
        losses. That hasn’t been the case this year. Morningstar reports that as of 10/31, the
 
As a trend-following system, Upgrading helps us migrate towards the better performing funds and sectors. But as the numbers above indicate, there simply haven’t been any safe areas to migrate to. As a result, the Upgrading portfolios of both SMI funds have suffered right along with the market.
 
Both portfolios were able to temporarily buck the market’s losses by loading up on energy-heavy funds in the early part of 2008. But when energy stocks collapsed over the summer, Upgrading gave back those gains and then some. Since then, the portfolios have gradually rotated into relatively conservative fare which we expect to perform somewhat better than the market for the duration of this bear
        market decline. However, it also means the SMI funds will likely lag the market in the early stages of the next bull market, whenever it begins. This is a predictable pattern when following a trend-following system.
 
While the SMI Managed Volatility Fund (SMIVX) was whipsawed back and forth a bit in the early part of 2008, the value of its hedging practice became evident as losses mounted in October. We might have hoped for a little more dampening of losses early in the year, but overall for the 12-month period, SMIVX’s hedging was able to reduce losses relative to SMIFX by more than one-fourth (-28.51% vs.
        -39.86%). It is our expectation that SMIVX will continue to offer less potential downside relative to both SMIFX and the market for the duration of this bear market. However, the same hedges that provide downside protection will eventually cause it to lag during the initial rally of the next bull market. Investors should clearly understand these dynamics when considering an investment in the SMI Managed Volatility Fund.
 
 
Brighter Days Ahead...But When?
 
While the past year has been dismal for most stock market investors, it’s worth noting that the sharp decline in stock prices has caused several prominent value-oriented investors (including Jeremy Grantham, John Hussman, and Warren Buffett) to publicly comment that stocks haven’t been this attractively priced in many years. It’s important to understand that these aren’t
        predictions that the bear market is over, or even nearly over. Rather, these investors are merely observing that investments made at today’s relatively low valuations will likely be quite profitable when measured over the
 
We agree with this assessment. We don’t believe investors should ever put money in the stock market that they don’t intend to leave invested for at least five years. Investors able to wait at least that long (and ideally even longer, measuring their time frame in decades rather than months or years), will likely look back at current prices as an attractive historic buying point.
 
That doesn’t mean the market won’t fall further before finally turning higher. Nobody knows what the market will do in the short-term. However, one thing is clear. The goal of every stock market investor is to “buy low and sell high,” and that can’t be done consistently if you sell at times when the market is already down substantially. In other words, you can’t
 
As a result, we encourage investors to establish a long-term investing strategy and stick with it regardless of what the market is doing at the moment. The events of the past year have reemphasized the importance of eliminating debt and having a fully-funded emergency fund. We encourage investors to work diligently on those two priorities first, even if it means waiting to invest in the SMI funds as a
        result. Operating from a solid financial base will allow you to weather the periodic market storms with much greater confidence.
 
While the past year has been dark, we know that won’t last forever. When the investing skies brighten eventually, we fully expect Upgrading to speed our recovery from these bear market losses and power our portfolios to greater heights.
 
 
Sincerely,


Dear Fellow Shareholders:
 
Our fund had a year of superb relative and miserable absolute performance during one of the worst periods in U.S. stock market history. For a discussion of Marathon Value Portfolio’s (The “Fund”) performance during its latest fiscal year ended October 31, 2008, please see “Management Discussion” in the annual report.
 
In looking at the current market decline compared to previous sharp declines, it is important to note that speculation in common stocks was not the root cause. Valuations of most companies were reasonable in relation to the riskless rate of return. In contrast, in 1929 even a successful corporation like RCA was so overvalued that it took until 1962 for RCA to recover to its 1929 price. In 1973, so-called
        nifty-fifty stocks carried price earnings ratios of 50 or higher. As I have commented in previous letters, the root of the current crisis lies in the extraordinary credit boom and the unregulated lending environment. As such, the current crisis is more akin to the panics which preceded the establishment of the Federal Reserve.
 
While margin requirements on stocks are far higher than in 1929, today’s hedge funds succeeded in borrowing large sums in order to enhance their returns. In the current market, this leverage works against them, and they have been forced to sell stocks to pay down borrowings and meet redemptions. Their selling and the credit crunch continue to weigh on stocks.
 
If I am correct that we are seeing a panic, then once confidence is restored, we should see the end of the secular bear market which began in March 2000. At that point, a major sustained bull market can begin. The final months of a bear market present the rare opportunity to purchase the highest quality companies at prices that do not reflect their value as businesses. Think of a high-end store which
        suddenly must put its goods on sale at distress prices. If you have the money, you might skip the trip to Wal-Mart. Our strategy will be to sell some of our less high quality investments which were purchased because of their more favorable price value equation and to purchase shares of the best businesses that are available.
 
During the year, I have added substantially to my holdings in the Fund. I do not minimize the personal hardships which many face as a result of the economic decline which the financial world precipitated. But, these same conditions have created rare opportunities which I believe will benefit long-term investors in the Fund. Unlike many mutual funds, we have the cash to seize these opportunities thanks to our
        low level of redemptions and my previous caution. After the first half of the new year, I look forward to reporting on our progress. I thank long-term shareholders who have truly made Marathon Value Portfolio a unique proposition in today’s investment world.
 
Sincerely,


