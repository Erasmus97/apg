Dear Shareholder, 
We are very pleased to send you a copy of the annual report for The Spirit of America High Yield Tax
Free Bond Fund (the Fund). The Fund began operations on February 29th, 2008. 
We are excited to be able to share this report with you as the
Funds first year ends. The year has shown strong and steady growth for the Fund and we look forward to continued inflows and further development of the Fund in the year to come. 
We remain committed to our philosophy of pursuing an optimal balance between yield and risk by utilizing our many years of experience in the municipal bond market. Our goal is to continue seeking high current income
that is exempt from federal income tax, while employing a relatively conservative approach to investing in the high yield sector of the municipal market. 
We have been proud to watch the increasing number of investors in the Fund throughout its first year. We appreciate your support of our Fund and look forward to your future investment in the Spirit of America High Yield Tax Free Bond Fund.

Thank you for being a part of the Spirit of America Family of Funds. 
Sincerely, 


Dear Shareholder, 
We are very pleased to present you with the annual report for the Spirit of America Real Estate Income and Growth Fund (the Fund). There is no question that
2008 was fraught with challenges. Nonetheless, we view the current REIT landscape as providing numerous opportunities, and are therefore optimistic about prospects for a successful 2009. 
Throughout 2008, declines in single family housing values continued to grab national headlines. Although we remain of the view that there is limited correlation between residential real estate fundamentals and those
of the commercial real estate investment trusts (REITs) in which the Fund invests, negative investor sentiment toward real estate in general has undoubtedly pressured REIT share prices. Perhaps more importantly, the rapid deterioration of credit
markets caused shares of REITs with near term debt maturities to be disproportionately impacted. 
Although REITs, as measured by the MSCI US REIT Index
(RMS), ended 2008 in 
negative territory, in our opinion, the recent stock market dislocation has led to a tremendous opportunity for the Fund to invest in REIT shares at very
attractive prices. According to our analysis, the decline in the industry stock valuation, coupled with relatively firm property values, has caused some REIT shares to trade at a significant discount to estimates of the underlying real estate value.

Our investment philosophy continues to be to seek value in the bricks and mortar of America through real estate investment trusts that own office
buildings, shopping malls, hotels, apartments, and other commercial properties. We are very optimistic about the performance of recent investments made by the Fund, and look forward to 2009. 
We are grateful for your support, and look forward to your continued investment in the Spirit of America Real Estate Income and Growth Fund. 
 
Sincerely, 


Dear Shareholder, 
We are very pleased to send you a copy of the annual report for the Spirit of America Large Cap Value
Fund (the Fund). 
At Spirit of America, we take a comprehensive approach to investing. We evaluate economic trends, we then analyze sectors
that could benefit from those trends, and finally, invest in companies with strong fundamentals. 
Recent market activity has been providing numerous
opportunities to pick up stocks at historical low valuations. We believe that investing in sound companies with reasonable valuations will help enhance the long-term returns of the Fund. 
During the fourth quarter of 2008, the Fund was upgraded by Morningstar to a 4-star rating from a 2-star rating.* 
We have
been proud to watch the increasing number of investors in the Fund throughout its history. We appreciate your support of our Fund and look forward to your future investment in the Spirit of America Large Cap Value Fund. 
Sincerely, 


