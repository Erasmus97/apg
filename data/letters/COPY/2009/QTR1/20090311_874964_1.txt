Dear Shareholder:
A year ago, the mortgage
crisis, the surging price of oil, and the weakening U.S. dollar all pointed to
a significant economic downturn. However, few could have foreseen the extent to
which these events would wreak havoc in the financial marketplace. After
established Wall Street investment banks, such as Bear Stearns and Lehman
Brothers, collapsed, and financial giants like AIG, Fannie Mae, and Freddie Mac
required government assistance to stay afloat, investor confidence was severely
shaken. All of this led to some of the most volatile financial markets in
history and a cascade of economic problems in the latter part of 2008. The U.S.
government is addressing these issues with a commitment of over $4.5 trillion
to fight the credit crisis and economic downturn, with additional stimulus
likely. At the same time, the fed funds rate has been reduced to, effectively,
0%.
As the credit crisis made
its way around the globe during the past year, it became painfully evident that
equity investors had very few places to hide. Global stock markets moved
sharply lower in unison, a phenomenon that is common in times of heightened
volatility. While credit markets are showing some tentative signs of thawing,
risk aversion among investors remains historically high. Most leading
indicators of global growth show few signs of bottoming, reflecting continued
erosion in economic conditions.
While these events created a
difficult environment for investing during the year, most of the Lazard Funds
Portfolios outperformed their respective benchmarks for the whole of 2008. We
believe that this outperformance was due to our disciplined approach to
investment analysis and portfolio management.
During Lazards decades of
asset management, there have been many notable downturns in the markets. In
every instance, the market eventually recovered. Of course, everyones
investment needs are unique. Seeking advice from your trusted investment
advisor can also be an important step in making informed and intelligent
decisions.
We would like to express our
deepest gratitude to you as shareholders. We sincerely appreciate your
continued confidence in our management capabilities, especially during these
difficult times. We are honored that you have turned to Lazard for future
guidance.


