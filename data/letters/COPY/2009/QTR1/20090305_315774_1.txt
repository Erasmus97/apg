Dear Shareholder: 
 
2008 will go down as one of the worst years for equity markets I have seen in my thirty
years in the business. The fourth quarter of 2008 was the culmination of a horrible overall year in the equity markets as the credit crunch impacted the global economy much more than investors feared; consequently affecting earnings results and
market multiples across every industry. It seemed during the quarter that every day there was another bad economic headline followed by earnings preannouncements that affected most industries and stocks in an indiscriminate manner. By the end of the
quarter, it was being mentioned that Gross Domestic Product (GDP) could be down as much as five to six percent, which was much worse than economists were forecasting. 
 
For the year, the Adams Harkness Small Cap Growth Fund (the Fund) underperformed its benchmark. The Fund was most impacted
by being overweight in the technology sector coupled with the underperformance of individual companies in that sector. As the economy and the market turned, technology companies came under the most multiple compression coupled with earnings results
that were much less than the street projected as we moved through the third and fourth quarters. Companies like Cybersource, Ariba, Inc., Vocus, and Monolithic Power Systems, Inc. were all major detractors for the reasons mentioned above. Also,
being overweight in the consumer discretionary sector detracted from performance. On the positive side was underweighting of the industrial sector throughout the year, which accounted for our largest sector contribution. Also positively contributing
to performance was being overweight in the energy and materials sectors at the beginning of the year, where Patriot Coal and Comstock Resources, Inc. were strong performers, and then exiting those particular companies and sectors as they started to
rollover with the economy and the stock market. 
 
Despite the economic
uncertainties as we enter 2009, we remain constructive in our outlook for small cap growth stocks. We believe that much of the bad news has been priced in at this point, and that the new Presidents stimulus plan may have a positive impact
on the economic environment. We do not expect to see a significant rebound in GDP, but rather a slow lift as we move through 2009, putting us back in the 0-2% GDP range by the end of the year. We think the environment will be
in many ways similar to that of 1980-1982, when the institution of credit controls by the government in an attempt to control inflation caused two quarters of severe contraction in GDP, followed by several quarters of virtually no growth. If the
fourth quarter of 2008 marks the most severe point of GDP contraction, then the market lows in the fall of 2008 should hold. We believe the most likely outcome for the market overall is a trading range environment, with wide disparity between
individual issues. Several months into the credit crunch, what is becoming clear as we talk to the companies we hold in your portfolio and other companies in which we have an interest, is that there remain many opportunities for smaller companies to
continue to grow, and to improve their business models. While growth may be more difficult to achieve, some companies with strong new product cycles and mission critical products and services have found good opportunities for growth; companies
that offer a unique product or service, particularly one that is differentiated from the competition, have in some cases seen an improvement in their business. The U.S. economy is not monolithic; underneath the surface, there are a number of
ebbs and flows and cross-currents that provide good opportunities for those companies quick enough and dynamic enough to seize them. Although it may seem counterintuitive, environments such as this are a bit easier for our process because it is
easier to identify companies that can potentially grow and prosper. Because the vast majority of companies will not see strong growth, we believe those companies that can will be rewarded with premium multiples.
 
1 
ADAMS HARKNESS SMALL CAP GROWTH FUND 
ADAMS HARKNESS SMALL CAP GROWTH FUND 
SHAREHOLDER LETTER 
DECEMBER 31, 2008 
 
Sincerely, 


Dear Fellow Shareholder: 
 
37.00%
 
The Fund faired poorly in the second half of the year,
principally due to the unprecedented drop in energy prices and other industrial materials, which have been major themes in the Fund. Clearly we did not anticipate the seizure experienced by the global financial system that caused a
massive sequential decline in economic activity in the fourth quarter. As a consequence of this and the forced liquidation of investment funds, many of our stocks such as Peabody Energy Corp. (BTU), ABB, Ltd. (ABB) and Schlumberger, Ltd. (SLB)
rapidly went from being reasonably priced and our top performers earlier in the year, to incredibly cheap fire sale victims by year-end. 
 
Regrettably, the energy sector positioning of the Fund suffered right into the end of the year. This is illustrated most dramatically by the continuing
fall of oil prices as oil inventories in Cushing, Oklahoma (the delivery point for U.S. based oil futures trading) reached saturation levels. In the last two weeks of 2008, oil prices dropped from $50 to $35, increasing pressure on all
commodity-based investments. We did not anticipate that global commodity prices would experience one of their worst declines in recorded history in the second half of 2008. Now that this has happened, we believe that structuring the Fund with a view
on prospects for a commodity price rebound is critical. 
 
We
believe that commodity prices will rebound because: 1) the global economy nearly ceased to function in the fourth quarter due to the global banking crisis, but we will soon witness dramatic sequential increases in usage because the economy needs to
consume a base-level amount of commodities simply to 
 
2 
JORDAN OPPORTUNITY FUND 
JORDAN OPPORTUNITY FUND 
A MESSAGE TO OUR SHAREHOLDERS 
DECEMBER 31, 2008 
 
function; 2) there will be a sea of government spending coming which will stimulate consumption; and 3) most raw material commodities,
especially oil and gas, face severe long-term supply problems which will quickly resurface on any meaningful decline in resource investment or increase in demand, both of which should become more apparent later in 2009. 
 
Therefore, we continue to favor commodities, principally the energy sector.
We believe that oil will rise sharply in the months ahead. The last three major bear markets in oil (1991, 1998 and 2001) ended between the months of November and February. Oil services companies and oil producers have very low valuations and are
well positioned, especially with their strong balance sheets, for the resumption of strong profit growth once the global economy rebounds. We also see similar opportunities among the industrial materials and infrastructure companies. A rebound in
commodity prices, coupled with President Obamas stimulus proposal, will create significantly improved sentiment among these deeply undervalued equities. 
 
In contrast to the Funds energy theme, healthcare and medical stocks were very helpful in 2008. Companies like Abbot Laboratories (ABT),
AmerisourceBergen Corp. (ABC), and Cardinal Health, Inc. (CAH) had a positive influence on the Fund, given the strong decline in Energy and Commodity stocks. 
 
Going forward, the Fund continues to emphasize healthcare and medical stocks, along with some select technology stocks such as Apple, Inc. (AAPL) and
Google, Inc. (GOOG). As valuations for traditional growth stocks stay very compelling, we believe strong new product cycles will emerge. Finally, we believe that the carnage in the financial sector is starting to settle, and we feel that
opportunities among the survivors, especially among the non-bank financials such as Chicago Mercantile Exchange (CME) and the IntercontinentalExchange, Inc. (ICE), may be positioned to rebound. 
 
In conclusion, 2008 was a terrible year, but it has created numerous
opportunities for capital appreciation. The backdrop of panic, forced liquidation, low interest rates and economic stimulus creates a powerful risk/reward structure to see attractive returns in 2009. 
 
3 
JORDAN OPPORTUNITY FUND 
JORDAN OPPORTUNITY FUND 
A MESSAGE TO OUR SHAREHOLDERS 
DECEMBER 31, 2008 
 
Please feel free to contact us, or visit our website,
www.jordanopportunity.com, if you have any comments or questions. 
 
Sincerely, 


Dear Fellow Shareholder, 
 
For the year
ended December 31, 2008, the Polaris Global Value Fund (the Fund) had a total return of -46.19%, while the MSCI World Index, net, the Funds benchmark, dropped -40.71%. Prior to 2008, the Funds performance history
experienced only one calendar year of performance declining greater than -10% (1990: -11.74%), while the MSCI World Index experienced four calendar year returns greater than -10%, three of which were greater than -15% (2002: -19.89%, 2001: -16.82%,
2000: -13.18%, 1990: -17.02%). 
 
Volatility continued to hamper
stock exchanges worldwide: the financial institution upheaval affected the general economy. Focused on identifying conservatively-managed companies, with strong earnings potential and sustainable free cash flow, the Fund successfully avoided direct
investment in mismanaged financial companies. However, selection of better managed companies did not matter, as stocks in almost every market sector fell in reaction to these recent developments. 
 
The 10-, 15- and inception-to-date (7/31/89) returns continued to exceed
the benchmark. The Funds returns have been accompanied by lower market risk, as measured by the beta statistic of 0.86 since the Funds inception (volatility measurement relative to the MSCI World Index). 
 
The following table summarizes total returns through December 31, 2008.

 
Polaris Global Value Fund
MSCI World Index, net dividends reinvested
 
Performance data
quoted represents past performance and is no guarantee of future results. Current performance may be lower or higher than the performance data quoted. Investment return and principal value will fluctuate so that an investors shares, when
redeemed, may be worth more or less than original cost. For the most recent month end performance, please visit the Funds website at www.polarisfunds.com. As stated in the current prospectus, the Funds annual operating expense ratio
(gross) is 1.19%. Shares redeemed or exchanged within 180 days of purchase will be charged a 1.00% fee. Fund performance shown for periods of 180 days or less does not reflect this fee; otherwise, if reflected, the returns would have been lower.
Returns greater than 1 year are annualized. See page 5 for additional disclosure. 
 
2008 PERFORMANCE ANALYSIS: 
 
Of the entire decline in 2008, approximately 70% was due to four sectors that contributed about equal amounts (7-9%) to performance: financials, industrials, consumer discretionary and materials, in order of importance. Country and
industry factors had a pronounced impact on individual holdings. The Funds research focused on companies with strong cash flows and low debt levels; yet in some instances, individual stock fundamentals were overwhelmed by country and industry
factors. 
 
Such an effect was evidenced in financials, as the
entire sector declined due to large institutional failures (none of which were held by the Fund) and questions surrounding U.S. bank bailout programs. Such concerns also affected bank performance in the UK, Ireland and Belgium. It is worth noting
that the Funds financial holdings 
 
1 
POLARIS GLOBAL VALUE FUND 
POLARIS GLOBAL VALUE FUND 
A MESSAGE TO OUR SHAREHOLDERS 
DECEMBER 31,
2008 
 
Please note that all current and future portfolio holdings are subject to risk.
 
The Funds portfolio stocks generally avoided subprime/overleveraged conditions, keeping fundamentals (strong balance sheets, good customer funding
and clean loan portfolios) intact, yet stock prices dropped. Fund management believes holding select financial stocks at such depressed valuations may invariably result in strong performance over the long term. Positive signs within these
investments have been evidenced by 1) relatively stable second quarter thrift/regional bank earnings and 2) outperformance in the third quarter. 
 
Knowing the portfolio held stocks benefiting from global growth, Fund management invested in more defensive sectors to balance this exposure, which helped
performance in certain financials, information technology, telecommunication services and utilities. Consequently, companies in these sectors had more stable cash flows unaffected by current economic volatility. The holdings in these sectors not
only protected value, but also provided diversification in these times of market turbulence. 
 
Many of these companies were based in Japan, including telephone operator KDDI Corporation, utility companies Kansai Electric Power and Tokyo Electric Power and within consumer staples, Meiji Dairies
and Asahi Breweries. In the depressed consumer discretionary market, Culture Convenience, a Japan-based holding company in the operation of video/music rental chains, also gained substantially. The Japanese Yen appreciation further helped
domestic-oriented stocks. 
 
Offsetting these gains, U.S.
healthcare holdings were negatively impacted by concerns about reimbursement, cost ratios and rising medical costs, which slowed earnings growth. The U.S. flu season increased utilization of healthcare; costs rose accordingly, but were not yet
offset by insurance premium increases. Once healthcare organizations re-price premiums in early 2009 and address medical cost increases, earnings may stabilize. 
 
Consumer discretionary results were negatively affected by UK homebuilders, which underperformed due to house and land price declines triggered by
reduced mortgage lending and buyer apprehension. Fund management believes the market unfavorably compared British homebuilders to their U.S. counterparts. The dramatic drop in homebuilder valuations anticipates more drastic declines in fundamentals
than Fund management believes to be realistic over the next three to five years. Thus far, these companies have preserved cash flow by rapidly decreasing costs, quickly controlling new building to match decreasing sales rates and paying down debt,
all of which have allowed the firms to avoid issuing new equity at depressed valuations thus preventing dilution. The Fund continued to invest cautiously in consumer-oriented sectors, favoring select companies with substantial free cash flow and
strong corporate teams. 
 
Global economic concerns and
constrained credit dampened demand for goods and services. Materials and industrials were negatively impacted as a result. Depressed materials stock prices were evident worldwide, ranging from Australias diversified commodities company, BHP
Billiton, and U.S. industrial gas manufacturer Praxair to South Africa holdings, Metorex and Sappi. 
 
A hallmark of Fund managements stock selection strategy is to pinpoint companies that have flexible business models with a base level of service business, not solely new growth business. If a
crane, elevator, escalator or factory stops running entirely, those critical elements will have to be repaired or replaced with new parts. As such, service businesses within the industrial sector historically have provided strong margins and
sustainable cash flow even in the worst of economic conditions. 
 
2 
POLARIS GLOBAL VALUE FUND 
POLARIS GLOBAL VALUE FUND 
A MESSAGE TO OUR SHAREHOLDERS 
DECEMBER 31,
2008 
 
ASSET ALLOCATION: 
 
The following
table shows the Funds asset allocation on December 31, 2008. 
 
Polaris Global Value Fund Asset
Allocation
N. America
Japan
Other Asia
Europe
Scandinavia
Africa & S. America
Cash
Industry Totals
Market Weighting
 
Table
may not foot due to rounding 
 
Fund management continued to seek
better values overseas; consequently, the Fund remained underweight in North America relative to the World Index. The Funds Scandinavian and Japanese holdings were weighted greater than the benchmark, while investments in continental Europe
were less heavily weighted. 
 
During the year, Fund management
sold holdings in the materials and industrials sectors where analysis revealed deterioration in fundamentals in relation to evolving market dynamics. Other stocks were sold when they advanced and reached their valuation limits. By executing such
sells, management was able to improve the valuation of the portfolio and reduce exposure in some sectors that may experience further weakness. 
 
Based on the extreme declines in many company valuations worldwide, management is actively making portfolio changes and evaluating a host of new
investments. 
 
INVESTMENT ENVIRONMENT AND STRATEGY:

 
Without clear visibility to an end in the current
recession, companies worldwide have reacted with amazing swiftness to reduce their cost basis. Such efforts have created strong macro-economic headwinds, as cutbacks have created more unemployment and contributed to investor pessimism. In light of
these developments, Fund management believes the prudent course of action is to remain cautious and watchful of economic trends. 
 
On a brighter note, there are several factors that may provide the needed stimulus for economic improvement. First, oil and other price declines will
signal lower heating and transportation costs that previously contributed to material declines in disposable incomes in large consuming countries such as the U.S. During the past few years, oil rose from $25 to $147 a barrel. The negative impact on
consumers cash flow was underappreciated. Likewise, the stimulative effect of the recent drop in oil below $40 may also be underappreciated. 
 
3 
POLARIS GLOBAL VALUE FUND 
POLARIS GLOBAL VALUE FUND 
A MESSAGE TO OUR SHAREHOLDERS 
DECEMBER 31,
2008 
 
Although creating more unemployment, companies that are restructuring can stabilize the cost structure of the economy with flexibility to hold up cash flows better than in past recessions. In addition, massive amounts of stimulus are
being injected into economies by governments worldwide. All of these actions may ultimately have a positive effect on the economy despite the headwinds each catalyst may represent. 
 
The Fund has been nimble enough to make changes to try to avoid further capital declines, reducing materials exposure, while
shifting weightings to companies that have experienced stronger cash flows and low levels of debt. Companies that might experience refinancing difficulties or sustain lower cash flows have been sold. 
 
Fund management has retained positions that may weather further
macro-economic declines, with the potential for dramatic increases when the markets return to normalized levels. Fund management strongly believes that valuation declines have overshot the fundamental values of the portfolio companies. Moreover,
there is growing evidence that portfolio companies with strong cash flows and manageable debt levels may be growing stronger as the credit crisis persists and competitors struggle or fail. When the economy reaches bottom, and finally turns, the Fund
should be well situated with strong performing companies. The value of having fellow mutual fund shareholders and investors who understand and act in accordance with this philosophy cannot be over-emphasized. Investors tend to buy when returns have
been positive, and sell when returns turn negative. 
 
In
conclusion, 2008 performance was unsatisfactory. Nevertheless, we remain confident that the Funds investment philosophy, discipline and current strategy may result in investments that should be fundamentally sound in the current economic
crisis. Moreover, the valuations profile of the current portfolio illustrated in the following table represents compelling value. 
 
Portfolio Characteristics (as of December 31, 2008) 
 
 
P/E: Price divided by earnings per share 
P/CE: Price divided by cash earnings (earnings + depreciation on fixed asset) per share 
P/BV: Price divided by the
book value per share 
Dividend Yield: The median yield associated with the Funds holdings and does not reflect the Funds yield.

ROE: (return on equity): Earnings divided by book value 
ROA: (return on assets): Earnings divided by assets 
Free Cash Flow: A measure of financial
performance calculated as operating cash flow minus capital expenditures. 
 
Free Cash Flow Yield: 
 
An overall return
evaluation ratio of a stock, which standardizes the free cash flow per share a company is expected to earn against its market price per share. The ratio is calculated by taking the free cash flow per share divided by the share price. 
 
 
4 
POLARIS GLOBAL VALUE FUND 
POLARIS GLOBAL VALUE FUND 
A MESSAGE TO OUR SHAREHOLDERS 
DECEMBER 31,
2008 
 
DIVIDEND AND CAPITAL GAINS DISTRIBUTION UPDATE: 
 
On November 20, 2008, the Polaris Global Value Fund (Fund) declared a long term capital gain distribution of $ 0.37512 per share to shareholders of record as of November 19, 2008. The distribution was
payable on November 21, 2008. The distribution was made pursuant to Subchapter M of the Internal Revenue Code of 1986, as amended. There was no short term gain distribution declared. 
 
On December 31, 2008, the Fund declared a net investment income dividend of $0.31111 per share for shareholders of
record on December 30, 2008. This dividend was paid from interest and dividends received on investments less fund expenses during 2008. 
 
Please note that the net asset value (price) of the Fund was reduced on the day dividends were paid by the amount of the dividend distributions. Most
shareholders reinvested their distributions to purchase more shares. After reinvesting the distributions, the shareholders owned more shares at the lower price, making the value of their respective accounts equal to their value just prior to the
dividend distributions plus or minus any change due to portfolio fluctuations that day. 
 
We welcome your questions and comments. 
 
Sincerely, 


