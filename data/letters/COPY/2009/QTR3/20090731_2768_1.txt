Dear Shareholder:

The period ended May was an extraordinary and stressful
time for investors and those of us who have worked in financial markets for
many years. During this turbulent period, the U.S. recession deepened, the
unemployment rate surged and consumer spending fell. Most stocks suffered
major losses as investors worried about an uncertain future. With a new
president, Barack Obama, the country and the financial markets were eager
to see how effectively the government could address these problems.
Although this environment is bound to provoke great concern, we think it is
important to put short-term market developments in perspective. Keep in
mind that as daunting as current conditions may be, we have navigated
through other periods of high market volatility, such as the U.S. stock
market's severe decline of and the bursting of the technology bubble
in We remain committed to our long-term perspective and disciplined
investment philosophy. Therefore, we view recent declines as potential
opportunities to find bargains that we believe may be well positioned to
become eventual winners. Although conditions remain challenging, our
experience gives us ample reason to be optimistic about future market
stabilization and recovery.

Franklin High Income Fund's annual report goes into greater detail about
prevailing conditions during the period under review. In addition, the
portfolio managers discuss investment decisions and Fund performance. You
will also find performance data and financial information. Please remember
that all securities markets fluctuate, as do mutual fund share prices.

If you would like more frequent updates, FRANKLINTEMPLETON.COM provides
daily prices, monthly performance figures, portfolio holdings and other
information.

NOT FDIC INSURED | MAY LOSE VALUE | NO BANK GUARANTEE


Not part of the annual report |



You can also access your account, buy and sell shares, and find helpful
financial planning tools. We hope you will take advantage of these online
services.

Although market conditions are constantly changing, we remain committed to
our disciplined strategy as we manage the Fund, keeping in mind the trust
you have placed in us. As always, we recommend investors consult their
financial advisors and review their portfolios to design a long-term
strategy and portfolio allocation that meet their individual needs, goals
and risk tolerance. We firmly believe that most people benefit from
professional advice, and that advice is invaluable as investors navigate
current market environments.

We thank you for investing with Franklin Templeton, welcome your questions
and comments, and look forward to serving your investment needs in the
years ahead.

Sincerely,
