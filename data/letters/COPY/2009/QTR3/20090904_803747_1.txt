Dear Shareholder:
As you know, the past year has been among the most dramatic and
difficult in modern financial history, with an unprecedented financial crisis sending the U.S. and global economies into a deep and painful recession.
As we enter the second half of 2009, we have seen some hints of optimism in both the markets and economy, but we still face a long and bumpy road to
recovery.
