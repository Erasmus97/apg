Dear Shareholder:
Recent events have shown great volatility in the markets and uncertainty in the economy. During these challenging times, it becomes even more important to focus on long-term horizons and key investment tools that can help manage volatility. This may be the time to reflect on your investment goals and evaluate your portfolio to ensure you are positioned for any potential market rebound.
A long-term financial plan can serve as a road map and guide you through the necessary steps designed to meet your financial goals. Your financial plan should take into account your investment goals, time horizon, overall financial situation, risk tolerance and willingness to ride out market volatility. Your investment professional can be a key resource as you work through this process. The knowledge and experience of an investment professional can help as you create or reevaluate your investment strategy.
The importance of diversification
Although diversification does not ensure a profit or guarantee against loss, a diversified portfolio can be a strategy for successful long-term investing. Diversification refers to the mix of investments within a portfolio. A mutual fund can contribute to portfolio diversification given that a mutual fund's portfolio represents several investments. Additionally, the way you allocate your money among stocks, bonds and cash, and geographically between foreign and domestic investments, can help to reduce risks. Diversification can result in multiple investments where the positive performance of certain holdings can offset any negative performance from other holdings. Having a diversified portfolio doesn't mean that the value of the portfolio will never go down, but rather helps strike a balance between risk and reward.
Reevaluate your strategy
An annual review of your investments is a key opportunity to determine if your investment needs have changed or if you need minor adjustments to rebalance your portfolio. Life events like a birth, marriage, home improvement, or change in employment can have a major effect on your spending and goals. Ask yourself how your spending or goals have changed and factor this into your financial plan. Are you using automated investments or payroll deductions to help keep your savings on track? Are you able to set aside additional savings or increase your 401(k) plan contributions? If during your review you find that your investments in any one category (e.g., stocks, bonds or cash) have grown too large based on your diversification plan, you may want to consider redirecting future investments to get back on track.
Sincerely,


Dear Shareholder:
Recent events have shown great volatility in the markets and uncertainty in the economy. During these challenging times, it becomes even more important to focus on long-term horizons and key investment tools that can help manage volatility. This may be the time to reflect on your investment goals and evaluate your portfolio to ensure you are positioned for any potential market rebound.
A long-term financial plan can serve as a road map and guide you through the necessary steps designed to meet your financial goals. Your financial plan should take into account your investment goals, time horizon, overall financial situation, risk tolerance and willingness to ride out market volatility. Your investment professional can be a key resource as you work through this process. The knowledge and experience of an investment professional can help as you create or reevaluate your investment strategy.
The importance of diversification
Although diversification does not ensure a profit or guarantee against loss, a diversified portfolio can be a strategy for successful long-term investing. Diversification refers to the mix of investments within a portfolio. A mutual fund can contribute to portfolio diversification given that a mutual fund's portfolio represents several investments. Additionally, the way you allocate your money among stocks, bonds and cash, and geographically between foreign and domestic investments, can help to reduce risks. Diversification can result in multiple investments where the positive performance of certain holdings can offset any negative performance from other holdings. Having a diversified portfolio doesn't mean that the value of the portfolio will never go down, but rather helps strike a balance between risk and reward.
Reevaluate your strategy
An annual review of your investments is a key opportunity to determine if your investment needs have changed or if you need minor adjustments to rebalance your portfolio. Life events like a birth, marriage, home improvement, or change in employment can have a major effect on your spending and goals. Ask yourself how your spending or goals have changed and factor this into your financial plan. Are you using automated investments or payroll deductions to help keep your savings on track? Are you able to set aside additional savings or increase your 401(k) plan contributions? If during your review you find that your investments in any one category (e.g., stocks, bonds or cash) have grown too large based on your diversification plan, you may want to consider redirecting future investments to get back on track.
Sincerely,


Dear Shareholder: 
Recent events have shown great volatility in the markets and uncertainty in the economy. During these challenging times, it becomes even more important to focus on
long-term horizons and key investment tools that can help manage volatility. This may be the time to reflect on your investment goals and evaluate your portfolio to ensure you are positioned for any potential market rebound. 
A long-term financial plan can serve as a road map and guide you through the necessary steps designed to meet your financial goals. Your financial plan should take into
account your investment goals, time horizon, overall financial situation, risk tolerance and willingness to ride 
The
importance of diversification 
Although diversification does not ensure a profit or guarantee against loss, a diversified portfolio can be a strategy for
successful long-term investing. Diversification refers to the mix of investments within a portfolio. A mutual fund can contribute to portfolio diversification given that a mutual fund’s portfolio represents several investments. Additionally,
the way you allocate your money among stocks, bonds and cash, and geographically between foreign and domestic investments, can help to reduce risks. Diversification can result in multiple investments where the positive performance of certain
holdings can offset any negative performance from other holdings. Having a diversified portfolio doesn’t mean that the value of the portfolio will never go down, but rather helps strike a balance between risk and reward. 
Reevaluate your strategy 
An annual review of your investments is a key
opportunity to determine if your investment needs have changed or if you need minor adjustments to rebalance your portfolio. Life events like a birth, marriage, home improvement, or change in employment can have a major effect on your spending and
goals. Ask yourself how your spending or goals have changed and factor this into your financial plan. Are you using automated investments or payroll deductions to help keep your savings on track? Are you able to set aside additional savings or
increase your 401(k) plan contributions? If during your review you find that your investments in any one category (e.g., stocks, bonds or cash) have grown too large based on your diversification plan, you may want to consider redirecting future
investments to get back on track. 
History has shown that the U.S. stock market has been remarkably resilient.¹ Volatility can lead to opportunity.
Patience and a commitment to your long-term financial plan may position you to potentially benefit over your investment horizon. We appreciate your business and continued support of Columbia Funds. 
Sincerely, 


