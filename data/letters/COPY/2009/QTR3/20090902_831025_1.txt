Dear Shareholders:

RiverSource Dividend Opportunity Fund (the Fund) Class A shares declined
(excluding sales charge) for the months ended June The Fund
outperformed the Russell Value Index (Russell Index), which decreased
The Fund also outperformed its peer group, as represented by the Lipper
Equity Income Funds Index, which fell during the same period.

SIGNIFICANT PERFORMANCE FACTORS
The performance of the U.S. equity markets can be divided into two distinct
portions during the months ended June -- the second largest equity
market correction ever and then the fifth biggest equity market recovery ever.
It was the end of one era and the start of a new one. Indeed, it would be an
understatement to call this fiscal year a tumultuous time, and it was certainly
one during which all equity investors were forced to re-think what was "normal"
market behavior.


SECTOR (at June % of portfolio assets)
- ---------------------------------------------------------------------



Sectors can be comprised of several industries. Please refer to the section
entitled "Portfolio of Investments" for a complete listing. No single
industry exceeds of portfolio assets.

Percentages indicated are based upon total investments (excluding
Investments of Cash Collateral Received for Securities on Loan) as of June
The Fund's composition is subject to change.

Cash & Cash Equivalents.

The sectors identified above are based on the Global Industry Classification
Standard (GICS), which was developed by and is the exclusive property of Morgan
Stanley Capital International Inc. and Standard & Poor's, a division of The
McGraw-Hill Companies, Inc.


- --------------------------------------------------------------------------------
RIVERSOURCE DIVIDEND OPPORTUNITY FUND -- ANNUAL REPORT



MANAGER COMMENTARY (continued) -------------------------------------------------

Through early March the U.S. equity markets were characterized by
negativity and soaring volatility, as the equity markets reacted to a litany of
bad economic news. Investor concerns were fostered by rising unemployment and a
still-fragile housing market as well as by continued financial disruptions.
Underlying this uncertainty were signs that U.S. economic growth was moving into
a recession, as evidenced by a waning consumer and a dimming export sector,
which, until recently, had been a rare bright spot in the U.S. economic picture.
Global financial institutions cut back lending as other major financial
institutions either went bankrupt, were forced to merge or were taken over by
the U.S. government. Together, these factors fostered heightened investor risk
aversion and fear. The result was that investors sold off all types of equity
assets in a flight to the relative safety of U.S. Treasuries.

Then, economic news became less pessimistic and, in early March, "green shoots"
began to appear. Investor sentiment improved. The Treasury Department finally
revealed details on key parts of its financial rescue plans, and investors
recognized that the U.S. government was not interested in nationalizing
companies that could be viable as privately-owned enterprises. Consumer
confidence took an upturn, and U.S. equities rallied strongly. Although the
equity markets stalled somewhat as

TOP TEN HOLDINGS (at June % of portfolio assets)
- ---------------------------------------------------------------------



Excludes cash & cash equivalents.

For further detail about these holdings, please refer to the section entitled
"Portfolio of Investments."

Fund holdings are as of the date given, are subject to change at any time, and
are not recommendations to buy or sell any security.


- --------------------------------------------------------------------------------
RIVERSOURCE DIVIDEND OPPORTUNITY FUND -- ANNUAL REPORT



- --------------------------------------------------------------------------------


the annual period came to a close, most of the major equity indices enjoyed
gains through June, marking the fourth month in a row of equity gains. Despite
growing concerns about building inflationary pressures, including oil prices
that jumped more than since the start of the equity markets' big push
since early March primarily reflected a belief among investors that the economy
was positioning for recovery.

Still, the late-period rally in the equity markets was not enough to recoup
prior months' losses, and so virtually all equity asset classes generated steep
double-digit declines for the period overall. Cyclical sectors within
the Russell Index performed worst, including basic materials, energy,
industrials and financials. More defensive sectors, such as consumer staples,
health care and utilities held up best. Perhaps most surprisingly, the
technology sector also generated strong returns, relative to the Russell Index,
for the fiscal year.

Importantly, the Fund continued to increase its dividend payout at a rate
greater than inflation during the annual period, despite economic weakness and
market turmoil. The Fund was able to largely avoid the dividend cuts faced by
the broader equity market (as represented by the S&P Index). Also, the Fund
maintained a net dividend yield at a level in excess of of the broader
market yield (as represented by the S&P Index) during the annual period. The
net dividend yield is the dividend yield after the deduction of fund expenses.

The Fund's absolute returns were certainly disappointing. While its strong
relative results may be of limited solace, they do serve as testament to


Importantly, the Fund continued to increase its dividend payout at a rate
greater than inflation during the annual period, despite economic weakness and
market turmoil.







- --------------------------------------------------------------------------------
RIVERSOURCE DIVIDEND OPPORTUNITY FUND -- ANNUAL REPORT



MANAGER COMMENTARY (continued) -------------------------------------------------


our emphasis on both risk management and investment opportunity. The Fund's
relative results benefited from both effective sector allocation and individual
stock selection during the annual period. Having a significant exposure to
consumer staples helped most, as this sector fulfilled its defensive role,
particularly during the period from September through early March when market
volatility was at its highest. Tobacco giant LORILLARD was a meaningful
outperformer within this sector. Similarly, a sizable allocation to utilities
helped, especially positions in telecommunications companies QWEST
COMMUNICATIONS INTERNATIONAL and EMBARQ, which proved to be stable cash flow
companies able to withstand the economic decline. Having only a modest
allocation to the comparatively weaker performing capital goods industry group
also boosted the Fund's results, as did stock selection within capital goods.
For example, the Fund established a sizable position in construction machinery
giant CATERPILLAR and increased its position in aerospace and defense
manufacturer HONEYWELL INTERNATIONAL, both of which were standout performers
during the annual period. The Fund held only a small position in GENERAL
ELECTRIC, which declined substantially. We subsequently eliminated the Fund's
position in General Electric by the end of the period. Stock selection was also
effective in the health care sector, where pharmaceuticals companies BRISTOL-
MYERS SQUIBB and BIOVAIL performed especially well. Elsewhere, positions in
automotive parts and accessories manufacturer JOHNSON CONTROLS and digital in-
network theatres provider NATIONAL CINEMEDIA were especially strong performers.

Having only a modest allocation to large commercial banks and other financials-
related firms, which led the equity market rally during the latter months of the
annual period, detracting from performance most. Positioning in U.S. BANCORP,
WELLS FARGO, JPMORGAN CHASE and GOLDMAN SACHS particularly disappointed. Having
a sizable allocation to chemicals also hurt, as a slowdown in global industrial
production brought demand for these companies' products to a crawl. Positions in
DOW CHEMICAL and E.I. DU PONT DE NEMOURS performed especially poorly. Elsewhere,
positions in oil services firms HALLIBURTON and TRANSOCEAN detracted from the
Fund's results.


- --------------------------------------------------------------------------------
RIVERSOURCE DIVIDEND OPPORTUNITY FUND -- ANNUAL REPORT



- --------------------------------------------------------------------------------

CHANGES TO THE FUND'S PORTFOLIO
We increased the Fund's exposure to the capital goods industry group, a strategy
which buoyed the Fund's results during the latter months of the annual period.
We also increased the Fund's allocation to the technology sector, adding to the
Fund's positions in semiconductor companies INTEL and TAIWAN SEMICONDUCTOR
MANUFACTURING and establishing a new position in rigid disc drive manufacturer
SEAGATE TECHNOLOGY.

We correspondingly reduced the Fund's exposure to the food and beverage products
industry, trimming the Fund's positions in such companies as KRAFT FOODS and
COCA-COLA. We also decreased the Fund's allocation to banks as they reduced
their dividends and thus no longer fit with the Fund's investment strategy.
Other than these noted changes, we made no meaningful changes to the Fund's
portfolio during the period, maintaining a low turnover rate.

OUR FUTURE STRATEGY
While certainly encouraged by the late-period equity market rally, we believe
that the equity market will likely trade in a rather range-bound manner for the
remainder of the calendar year, with the potential for volatility to spike
again, depending on the ambitiousness of the U.S. government's agenda and the
clarity of economic data. Valuations, we believe, remained modestly attractive
at the end of June but certainly not as attractive as they were just a few
months prior. Our view, therefore, remains positive going forward, though
somewhat neutralized by both the robustness of the March-through-June rally and
by our belief that the market will need to see better corporate revenues and
earnings before achieving a more sustained upward trend.

Our longer-term view is a bit more bullish and so we intend to maintain the
Fund's stance as more offensive rather than defensive. We believe that equity
valuations at the end of June were still relatively attractive from a longer-
term perspective and that returns on equity could be quite good. At the end of
June, we were biased toward sectors and companies that we believe will be the
beneficiaries of global economic growth, not just U.S. economic growth. For
example, given our view of the lack of alternative energy sources as a viable
realistic option for several years ahead, we intend to maintain an emphasis on
natural gas companies and oil services companies within the energy sector. We
expect to maintain an emphasis

- --------------------------------------------------------------------------------
RIVERSOURCE DIVIDEND OPPORTUNITY FUND -- ANNUAL REPORT



MANAGER COMMENTARY (continued) -------------------------------------------------


on technology, especially semiconductors, given its appealing long-term
dynamics. While capital expenditures may have declined as of late, the demand
for data on a global basis has not and we anticipate a rebound within several of
these technology companies. We also favored basic materials and other
industrial-related areas of the market at the end of June, given the development
of infrastructure anticipated under various government stimulus packages. We
expect to maintain the Fund's more modest exposure to traditionally defensive
sectors, such as consumer staples and utilities.

Overall, we believe that individual stock selection will continue to be key to
investment performance. As always, we intend to take positions in individual
stocks across all of the industries and sectors in which the Fund invests when
we believe we have identified factors that other investors have either missed,
ignored or strongly disagree with, and whose share prices we believe have the
potential to move higher. We intend to focus on larger-cap stocks and to
continue to add stocks offering greater dividend-yield potential. Of course, we
intend to continue carefully monitoring economic data and shifts in market
conditions as we seek stock-specific and industry-level opportunities to add
value for the Fund's shareholders.

Even with the media focus on dividend cuts among financial companies and the
U.S. equity market more broadly, we continue to believe that companies with a
consistent high dividend payout and a commitment to growing its dividend will be
among those most attractive to risk-tolerant investors seeking yield. We
therefore intend to continue to seek opportunities for yield from investments in
companies across a wide variety of sectors, including health care and utilities,
in an effort to mitigate the risk of dividend cuts seen during the period among
several companies within the financials sector. Indeed, as has been our strategy


- --------------------------------------------------------------------------------
RIVERSOURCE DIVIDEND OPPORTUNITY FUND -- ANNUAL REPORT



- --------------------------------------------------------------------------------


for some time now, we continue to seek a diversified mix of dividend-paying
stocks, with a focus on large-cap, value-oriented companies.





Percentages indicated are based upon total investments (excluding
Investments of Cash Collateral Received for Securities on Loan) as of June
The Fund's composition is subject to change.
Cash & Cash Equivalents.


- --------------------------------------------------------------------------------
RIVERSOURCE REAL ESTATE FUND -- ANNUAL REPORT



MANAGER COMMENTARY (continued) -------------------------------------------------


SIGNIFICANT PERFORMANCE FACTORS
It would be an understatement to say that the annual period ended June
was one of unprecedented volatility in the equity market in general and in the
real estate market in particular. The annual period actually started off well
for real estate investments, with the Wilshire Index generating positive returns
and significantly outpacing the broader equity market from July through
September. However, from mid-September through mid-November, when the credit
markets were suddenly frozen and investors became concerned that most companies,
including real estate investment trusts (REITs) and real estate companies, would
not have access to credit at any price, real estate investments declined
precipitously. It was in mid-September that the financial turmoil culminated
with the bankruptcy of Lehman Brothers. At that point, all investments with
leverage, or debt, were shunned. As REITs typically use approximately
leverage, the fear was that REITs would go bankrupt, too. Those REITs with the
highest leverage were punished most severely; those with lower leverage, less
so.

Volatility, with or more intraday moves up or down in the Wilshire Index,
were relatively common in the ensuing months. The Wilshire Index reached its low
point for the fiscal year on March From the start

TOP TEN HOLDINGS (at June % of portfolio assets)
- ---------------------------------------------------------------------



Excludes cash & cash equivalents.

For further detail about these holdings, please refer to the section entitled
"Portfolio of Investments."

Fund holdings are as of the date given, are subject to change at any time, and
are not recommendations to buy or sell any security.


- --------------------------------------------------------------------------------
RIVERSOURCE REAL ESTATE FUND -- ANNUAL REPORT



- --------------------------------------------------------------------------------



of the annual period through March the Wilshire Index fell more than
The risk of REITs being forced to file for bankruptcy was highlighted when the
regional mall REIT General Growth Properties did indeed file for bankruptcy on
April due to its inability to refinance near-term debt maturities.
Still, as the broad equity market rebounded from early March through the end of
June, the Wilshire Index rebounded approximately from its March low through
June Economic "green shoots" began to appear and balance sheet
concerns were reduced. Such concerns were reduced through government programs
such as Term Asset-Backed Securities Loan Facility (TALF), reduced credit
spreads (i.e. the difference in yields between non-Treasury securities and those
of duration-equivalent U.S. Treasuries), and a significant amount of equity
raising and debt repayment activity by REITs. Some of the greatest gains from
the Wilshire Index's March lows were seen in REITs that had suffered most from
balance sheet concerns, including many lower quality names.

The weakness in REITs for the annual period overall came despite a decline in
the Treasury yield, which fell from on June to a low of
on December before rising to at the end of June
REITs actually performed worse when Treasury yields declined and better when
Treasury yields increased, which was unusual from a historical perspective. The
reason for this historical anomaly was that the credit crisis-driven risk
aversion that drove down Treasury yields negatively impacted real estate assets,
which were perceived to be riskier investments. Similarly, when risk aversion


It is important to note that while underlying real estate fundamentals did
deteriorate with the economy, they remained relatively strong in select
subsectors.






- --------------------------------------------------------------------------------
RIVERSOURCE REAL ESTATE FUND -- ANNUAL REPORT



MANAGER COMMENTARY (continued) -------------------------------------------------



ebbed and investors shifted back to more "risky" assets in the last months of
the annual period, REITs benefited.

It is important to note that while underlying real estate fundamentals did
deteriorate with the economy, they remained relatively strong in select
subsectors. Health care and self-storage properties, for example, held up fairly
well. Health care properties proved to be a rather defensive area due to being
less tied to the economy than other segments of the real estate market. Self-
storage did well, as homeowners downsized. On the other hand, industrial
properties and regional malls performed poorly, as consumer spending turned down
sharply and factory orders dropped.

Against this challenging backdrop and the absolute losses experienced by the
Fund, it may be small solace that the Fund outperformed the Wilshire Index and
its peer group for the months ended June Still, such outperformance
is testament to our emphasis on both risk management and investment opportunity.
It also indicates that even in a broad sector decline, subsector allocation and
individual stock selection decisions can help.

Having comparatively modest exposure to the weakly-performing community shopping
center and regional mall REIT subsectors benefitted the Fund's results most
during the annual period. Stock selection within these two REIT subsectors as
well as in the apartment REIT subsector also helped. Within the retail
subsectors, a position in FEDERAL REALTY INVESTMENT TRUST performed well.
Federal Realty Investment Trust primarily owns, manages, develops and redevelops
retail and mixed-use properties in the Washington, D.C. area. We also
successfully steered away from several of the weakest performers in these
subsectors during the annual period. Within the apartment subsector, a sizable
position in MID-AMERICA APARTMENT COMMUNITIES, which owns, acquires and operates
multifamily apartment communities mainly in the southeastern U.S., was a strong
performer for the Fund. Mid-America Apartment Communities benefited from having
virtually no development in progress and thus was not compelled to take write-
downs on its balance sheets. The apartment REIT also benefited from its
comparatively low rent levels during the recessionary economy.


- --------------------------------------------------------------------------------
RIVERSOURCE REAL ESTATE FUND -- ANNUAL REPORT



- --------------------------------------------------------------------------------


Significant allocations to several other individual stocks that performed well
further boosted the Fund's relative results. These included a position in
specialty office REIT DIGITAL REALTY TRUST, health care REIT SENIOR HOUSING
PROPERTIES TRUST and mixed-office REIT HIGHWOODS PROPERTIES. Digital Realty
Trust specializes in technology-related real estate, including buildings that
house servers, and as such, benefited as demand for Internet access continued to
expand. Senior Housing Properties Trust benefited from favorable demographic
trends and as part of the health care subsector more broadly. Highwoods
Properties performed well, due primarily to its strong management team.

Detracting from the Fund's performance most was stock selection in the
industrial REIT subsector and having only modest allocations to the better-
performing self-storage and health care REIT subsectors. From an individual
stock perspective, poor timing in its trades of industrial REIT PROLOGIS
detracted from the Fund's results. Having an underweighted position in HEALTH
CARE REIT, relative to the Wilshire Index, also hurt, as its shares outpaced the
Wilshire Index. Health Care REIT's investments include skilled nursing
facilities, independent living or continuing care retirement communities,
assisted living facilities and specialty care facilities. Sizable positions in
THE MACERICH COMPANY and in BROOKFIELD PROPERTIES detracted from the Fund's
results as well, as both of these real estate companies underperformed during
the period. The Macerich Company acquires, owns, redevelops, manages and leases
regional and community shopping centers. Brookfield Properties owns, develops
and manages office properties in the U.S. and Canada.

CHANGES TO THE FUND'S PORTFOLIO
Given extreme weakness in the real estate market, extraordinarily tight credit
conditions and heightened investor fears regarding the potential direction of
the economy, we continued to seek to upgrade the quality of the portfolio
overall. We eliminated various holdings with higher leverage and reduced
positions where we felt discomfort based on stock-specific or event-specific
concerns. We increased holdings in what we believe are higher quality companies
with strong balance sheets, low leverage, superior management teams and a
sustainable competitive advantage. We also made adjustments based on relative
valuation analysis and shifts in market conditions. Toward the end of the annual
period, we began to

- --------------------------------------------------------------------------------
RIVERSOURCE REAL ESTATE FUND -- ANNUAL REPORT



MANAGER COMMENTARY (continued) -------------------------------------------------


establish or increase Fund positions in companies that we believe will be able
to take advantage of what many consider to be the best property buying
opportunities seen in several years. In doing so, our focus is on the quality
and skills of a real estate company's management team.

As a by-product of our bottom-up individual stock selection decisions, the
Fund's exposure to the regional mall REIT subsector decreased. The Fund's
exposure to the manufactured homes subsector increased, though this remains a
small percentage of both the Fund's total allocation and the Wilshire Index.
Further, by virtue of experiencing less depreciation than some other subsectors,
weightings in the health care subsector increased and, conversely, weightings in
areas that experienced greater depreciation, such as industrials, regional malls
and community shopping centers, decreased as a percentage of both the Wilshire
Index and the Fund.

At the end of the period, the Fund had significant exposure to the hotels,
manufactured homes and office subsectors compared to the Wilshire Index. The
Fund had more modest allocations than the Wilshire Index to the diversified
properties, health care, regional malls, community shopping center and self-
storage subsectors. As of June the Fund was virtually equally-weighted
to the Wilshire Index in the apartment and industrial subsectors.

OUR FUTURE STRATEGY
We are generally constructive in our view for the real estate market going
forward. At the end of the annual period, property sales activity levels were
very low, making it difficult to assess current market values for real estate
assets. However, we feel that the REIT price decline had already fully priced in
any underlying property value declines. In addition, the rebound in REIT shares
from March through June may be indicating a bottom in the property market
in the near term. Many of the stronger REITs were active sellers of assets
during the peak of the market and have been raising capital to take advantage of
any distressed sales opportunities that may arise with more highly leveraged
buyers. Indeed, REITs raised billion in equity year-to-date through June
Given the ability to issue equity to raise capital, we believe public
companies will likely have greater access to properties for sale at reasonable
prices than private companies with limited capital, thereby reducing
competition. While we expect property level fundamentals to

- --------------------------------------------------------------------------------
RIVERSOURCE REAL ESTATE FUND -- ANNUAL REPORT



- --------------------------------------------------------------------------------


continue to decline for the remainder of the year, the limited amount of new
supply in most regional markets gives us further reason for optimism. With less
commercial real estate currently being built, when the economic recovery does
come and demand revives, the turnaround for existing real estate should be
relatively compelling. Of course, the timing of such a turnaround has yet to be
seen.

As we wait for more sustained signs of improvement, we will seek to take
advantage of stock-specific buying opportunities created by market weakness
during the annual period. We intend to look for select real estate companies and
REITs with experienced management teams, low leverage, strong balance sheets,
quality assets and credibility with their shareholders. We also intend to look
for real estate companies and REITs where we believe exaggerated valuation
declines or a sustainable competitive advantage puts them in a position to take
advantage of current market conditions. Our goal, as always, will be to use in-
depth, bottom-up analysis of real estate fundamentals and market performance to
find undervalued companies across the U.S. that have solid dividend-paying
abilities and attractive long-term growth potential.



