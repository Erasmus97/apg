Dear iShares Shareholder: 
Electronic delivery is the easiest, most convenient way to receive reporting on your iShares holdings. In addition, its a way we can all care for our environment. To that end, we are pleased to offer
shareholder reports and prospectuses online. 
To sign up for electronic delivery, please follow these simple steps:

1. Go to www.icsdelivery.com. 
2. From the main page, select the first letter of your brokerage firms name. 
3. Select your brokerage institution from the list that follows. If your brokerage firm is not listed, electronic delivery may not be available. Please contact your brokerage firm or financial adviser. 
4. Fill out the appropriate information and provide the e-mail address where you would like your information sent. 
Your information and email address will be kept confidential and only used to deliver documents to you. If at any time you are not
satisfied, you can cancel electronic delivery at www.icsdelivery.com and once again receive physical delivery of your materials. If you have any questions, please contact your brokerage firm or financial adviser. 
Once you have enrolled, you will no longer receive prospectuses and shareholder reports in the mail. Instead, you will receive e-mail
notifications announcing that the shareholder report or prospectus has been posted on the iShares website at www.iShares.com. 
Table of Contents
 
Management’s Discussions of Fund Performance
Shareholder Expenses (Unaudited)


Dear iShares Shareholder: 
Electronic delivery is the easiest, most convenient way to receive reporting on your iShares holdings. In addition, it’s a way we can all care for our environment To that end, we are pleased to offer shareholder reports and
prospectuses online. 
To sign up for electronic delivery, please follow these simple steps: 
1. Go to www.icsdelivery.com. 
2. From the main page, select the first letter of your brokerage firm’s name. 
3. Select your
brokerage institution from the list that follows. If your brokerage firm is not listed, electronic delivery may not be available. Please contact your brokerage firm or financial adviser. 
4. Fill out the appropriate information and provide the e-mail address where you would like your information sent. 
Your information and email address will be kept confidential and only used to deliver documents to you. If at any time you are not
satisfied, you can cancel electronic delivery at www.icsdelivery.com and once again receive physical delivery of your materials. If you have any questions, please contact your brokerage firm or financial adviser. 
Once you have enrolled, you will no longer receive prospectuses and shareholder reports in the mail. Instead, you will receive e-mail
notifications announcing that the shareholder report or prospectus has been posted on the iShares website at www.iShares.com. 
Table of Contents
 
Management’s Discussions of Fund Performance
Shareholder Expenses (Unaudited)


Dear iShares Shareholder: 
Electronic delivery is the easiest, most convenient way to receive reporting on your iShares holdings. In addition, it’s a way we can all care for our environment. To that end, we are pleased to
offer shareholder reports and prospectuses online. 
To sign up for electronic delivery, please follow these simple steps:

1. Go to www.icsdelivery.com. 
2. From the main page, select the first letter of your brokerage firm’s name. 
3. Select your brokerage institution from the list that follows. If your brokerage firm is not listed, electronic delivery may not be available. Please contact your brokerage firm or financial adviser. 
4. Fill out the appropriate information and provide the e-mail address where you would like your information sent. 
Your information and email address will be kept confidential and only used to deliver documents to you. If at any time you are not
satisfied, you can cancel electronic delivery at www.icsdelivery.com and once again receive physical delivery of your materials. If you have any questions, please contact your brokerage firm or financial adviser. 
Once you have enrolled, you will no longer receive prospectuses and shareholder reports in the mail. Instead, you will receive e-mail
notifications announcing that the shareholder report or prospectus has been posted on the iShares website at www.iShares.com. 
Table of Contents
 
Management’s Discussions of Fund Performance
Shareholder Expenses (Unaudited)


