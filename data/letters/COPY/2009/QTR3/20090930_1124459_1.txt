Dear Shareholder:

The one-year period ended July was an extraordinary and stressful time
for investors and those of us who have worked in financial markets for many
years. During much of this turbulent period, the U.S. recession deepened, the
unemployment rate surged and consumer spending fell. In the past four months,
the U.S. economy, while still in a downturn, became marginally "less worse" as
the recession seemed to loosen its grip. The results of government "stress
tests" of major financial institutions were generally positive, enabling those
institutions to raise capital in the public markets and avoid a worst-case
scenario of government takeovers and/or failures of major banks. For most of the
reporting period, stocks suffered major losses as investors worried about an
uncertain future. After reaching cyclical lows in March, the stock indexes began
a rally as many investors reentered the market. At the same time, U.S. Treasury
yields rose from multi-year lows as investor risk aversion waned.

Amid recent events, we think it is important to put short-term market
developments in perspective. Keep in mind that we have navigated through past
periods of high market volatility by remaining committed to our long-term
perspective and disciplined investment philosophy. During such times, we search
for bargains that we believe may be well positioned to become eventual winners.
Although conditions remain challenging, our experience gives us ample reason to
be optimistic about future market stabilization and recovery.

In the enclosed annual report for Franklin Large Cap Equity Fund, the portfolio
managers discuss market conditions, investment management decisions and Fund
performance during the period under review. You will also find performance data
and financial information. Please remember that all securities markets
fluctuate, as do mutual fund share prices.

NOT FDIC INSURED | MAY LOSE VALUE | NO BANK GUARANTEE


Not part of the annual report |



If you would like more frequent updates, franklintempleton.com provides daily
prices, monthly performance figures, portfolio holdings and other information.
You can also access your account, buy and sell shares, read timely articles, and
find helpful financial planning tools. We hope you will take advantage of these
online services.

Although market conditions are constantly changing, we remain committed to our
disciplined strategy as we manage the Fund, keeping in mind the trust you have
placed in us. As always, we recommend investors consult their financial advisors
and review their portfolios to design a long-term strategy and portfolio
allocation that meet their individual needs, goals and risk tolerance. We firmly
believe that most people benefit from professional advice, and that advice is
invaluable as investors navigate changing market conditions.

We thank you for investing with Franklin Templeton, welcome your questions and
comments, and look forward to serving your investment needs in the years ahead.

Sincerely,

Dear Shareholder:

During the months ended July global economies experienced a
significant slowdown that began in with the U.S. subprime mortgage and
credit crises. Despite coordinated efforts by many governments to address
spreading liquidity and credit problems, deteriorating economic conditions and
mounting uncertainty contributed to market woes. Later in the period some
indicators offered a better outlook, and global markets rallied beginning in
March. Amid recent events, we think it is important to put short-term market
developments in perspective. Keep in mind that as daunting as current conditions
may be, we have navigated through past periods of high market volatility by
remaining committed to our long-term perspective and disciplined investment
philosophy. During such times, we search for bargains that we believe may be
well positioned to become eventual winners. Although conditions remain
challenging, our experience gives us reason to be optimistic about future market
stabilization and recovery.

In the enclosed annual report for Franklin Global Real Estate Fund, the
portfolio managers discuss market conditions, investment management decisions
and Fund performance during the period under review. You will also find
performance data and financial information. Please remember that all securities
markets fluctuate, as do mutual fund share prices.

If you would like more frequent updates, franklintempleton.Com provides daily
prices, monthly performance figures, portfolio holdings and other information.
You can also access your account, buy and sell shares, read timely articles, and
find helpful financial planning tools. We hope you will take advantage of these
online services.

NOT FDIC INSURED | MAY LOSE VALUE | NO BANK GUARANTEE


Not part of the annual report |



Although market conditions are constantly changing, we remain committed to our
disciplined strategy as we manage the Fund, keeping in mind the trust you have
placed in us. As always, we recommend investors consult their financial advisors
and review their portfolios to design a long-term strategy and port folio
allocation that meet their individual needs, goals and risk tolerance. We firmly
believe that most people benefit from professional advice, and that advice is
invaluable as investors navigate current market conditions.

We thank you for investing with Franklin Templeton, welcome your questions and
comments, and look forward to serving your investment needs in the years ahead.

Sincerely,

Dear Shareholder:

During the months ended July global economies experienced a
significant slowdown that began in with the U.S. subprime mortgage and
credit crises. Despite coordinated efforts by many governments to address
spreading liquidity and credit problems, deteriorating economic conditions and
mounting uncertainty contributed to market woes. Later in the period some
indicators offered a better outlook, and global markets rallied beginning in
March. Amid recent events, we think it is important to put short-term market
developments in perspective. Keep in mind that as daunting as current conditions
may be, we have navigated through past periods of high market volatility by
remaining committed to our long-term perspective and disciplined investment
philosophy. During such times, we search for bargains we believe may be well
positioned to become eventual winners. Although conditions remain challenging,
our experience gives us reason to be optimistic about future market
stabilization and recovery.

In the enclosed annual report for Franklin Global Trust, the portfolio managers
discuss market conditions, investment management decisions and Fund performance
during the period under review. You will also find performance data and
financial information. Please remember that all securities markets fluctuate, as
do mutual fund share prices.

If you would like more frequent updates, franklintempleton.com provides daily
prices, monthly performance figures, portfolio holdings and other information.
You can also access your account, buy and sell shares, read timely articles, and
find helpful financial planning tools. We hope you will take advantage of these
online services.

NOT FDIC INSURED | MAY LOSE VALUE | NO BANK GUARANTEE


Not part of the annual report |



Although market conditions are constantly changing, we remain committed to our
disciplined strategy as we manage the Funds, keeping in mind the trust you have
placed in us. As always, we recommend investors consult their financial advisors
and review their portfolios to design a long-term strategy and portfolio
allocation that meet their individual needs, goals and risk tolerance. We firmly
believe that most people benefit from professional advice, and that advice is
invaluable as investors navigate changing market conditions.

We thank you for investing with Franklin Templeton, welcome your questions and
comments, and look forward to serving your investment needs in the years ahead.

Sincerely,
