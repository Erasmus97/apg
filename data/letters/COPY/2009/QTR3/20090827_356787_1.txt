Dear Fellow Shareholders:
          Despite
the strong second quarter rebound, global equity prices have experienced sharp
declines over the past twelve months. We believe, however, that recent
stability in financial markets is an indicator that the worst of the economic
contraction has occurred and a recovery should begin to slowly unfold in the
second half of 2009.
2 
 
 
 
 
 
 
 
 
