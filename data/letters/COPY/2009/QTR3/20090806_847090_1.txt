Dear Shareholders:


This annual report for PNC Funds, Inc. covers what virtually all agree was one of the most
challenging and volatile periods ever for investorsthough a period that brought glimmers of
positive news in its latter months. This report provides a detailed review of the markets, the
portfolios and our management strategies. Also included are financial statements and a schedule of
portfolio investments listing securities held as of for each of the PNC Mutual Funds.


Given the extraordinary developments and dramatically shifting conditions in the financial markets
over the period, we at PNC Capital Advisors, Inc. are pleased to be able to assure you,
our shareholders, on several fronts.










First and foremost, seven of the eight equity and fixed income PNC Mutual Funds
outperformed their respective benchmark index and/or Lipper peer group or Morningstar
category for the ended All five of the fixed income PNC Mutual Funds
also generated solid positive absolute returns during the annual period. The three equity
PNC Mutual Funds absolute performance was most disappointing. The strong relative results
of two of the three equity PNC Mutual Funds may be of limited solace but do serve as
testament to the portfolio managers emphasis on both risk management and investment
opportunity.








We maintained our strong business relationships with top-line asset managers such as Morgan
Stanley, Artio Global and Delaware Management and welcomed GE Asset Management as a third
sub-adviser to the PNC International Equity Fund. We also increasingly drew on the proven
experience of several of our own PNC portfolio managers.








We continue to believe that by offering the opportunity to diversify your investments
among a wide array of equity, taxable and tax-exempt fixed income and money market mutual
funds, PNC Capital Advisors, Inc. may help you and your adviser fulfill your individual
asset allocation objectives.




On the following pages, each of the portfolio managers discusses the management of his or her Fund
over the annual period. The conversations highlight key factors influencing recent performance of
the Funds. For your convenience, those Funds with similar objectives and strategies have been
combined into one discussion. Before reviewing the performance of your individual mutual fund
investments, it may be useful to take a brief look at the major factors affecting the financial
markets over the period.



* * *



Economic Review


On a committee of economists at the private National Bureau of Economic Research
declared that the U.S. had entered into a recession in and it was downhill from
there. Clearly, it was the ended that brought the most bad news. Real Gross
Domestic Product (GDP) increased in the second quarter of before decreasing at an
annual rate of in the third quarter of in the fourth quarter of and in
the first quarter of The decrease in real GDP during the first quarter primarily reflected negative contributions from exports,
equipment and software, private inventory investment, nonresidential structures, and residential
fixed investment that were partly offset by a positive contribution from personal consumption
expenditures (PCE). Imports, which are a subtraction in the calculation of GDP, decreased. The
smaller decrease in real GDP in the first quarter than in the fourth reflected a larger decrease in
imports, an upturn in PCE for durable goods, and a smaller decrease in PCE for nondurable goods
that were partly offset by larger decreases in private inventory investment and in nonresidential
structures and a downturn in federal government spending. Further impacting the economy was
nonfarm payroll growth, which fell dramatically over the annual period. The unemployment rate rose
to in up significantly from the rate just earlier and the highest it
has been since In the month of May alone, nonfarm payrolls contracted by
jobsabout half the average monthly decline for the six months prior but job losses continued to
be widespread across major industry sectors. By the end of the number of unemployed
Americans had risen by nearly six million compared to just one year prior. Retail sales fell by
over














in the year ended and business investment fell by in the ended March
(latest data available). Chrysler filed for bankruptcy toward the end of the annual period,
and General Motors was preparing to do the same.


Making the most headlines, perhaps, was the panic that ensued during the last months of as
the specter of a complete collapse of the global financial system and a global recession became a
possibility. In mid-September, Lehman Brothers declared bankruptcy followed by Washington Mutual.
Fannie Mae, Freddie Mac and AIG were taken over by the U.S. government. Merrill Lynch was forced to
merge with Bank of America. Wachovia sold itself to Wells Fargo. Goldman Sachs and Morgan Stanley
converted themselves into commercial banks, seeking the relative safety of deposit capital.
Governments worldwide began to intervene with rescue packages for their own financial institutions.
The Federal Reserve Board (the Fed) also lowered the targeted federal funds rate several more
times during the period, bringing it to an unprecedented range of to and implemented a
policy of quantitative easing, whereby the Fed actively purchases bonds in the open market. The
U.S. Treasury implemented a Troubled Asset Relief Program to provide needed liquidity
to the U.S. financial system. Toward the end of November, the U.S. Treasury Department and the Fed
unveiled a pair of additional programs intended to provide to help unfreeze consumer
lending. Indeed, over the next several months, a virtual alphabet soup of programs were introduced
by the Fed, the U.S. Treasury Department and the Federal Deposit Insurance Corporation (FDIC)in an
effort to stimulate the economy, loosen the credit markets, inject liquidity back into the system
and alleviate the stress on the financial markets broadly.


On the more positive side, economic green shoots began to appear in March and continued to sprout
through the end of the annual period in May. Consumer spending, which dropped sharply in the second
half of grew in the first quarter. Consumer sentiment improved. Certain key drivers in the
housing market appeared to have stabilized. Some major banks indicated earnings would be better
than expected. More broadly, domestic profits of financial corporations increased in
the first quarter of in contrast to a decrease of in the fourth. Indeed,
companies across a range of industries indicated that business conditions had begun to stabilize.
Domestic profits of non-financial corporations decreased in the first quarter
compared with a decrease of in the fourth. Also, inflation became less of a concern,
at least for now. Oil prices declined dramatically, from approximately per barrel at the end
of to just over per barrel at the end of Due mainly to a drop in
energy prices overall, the Consumer Price Index fell to an annualized rate of at the end of
the annual period, compared to at the end of This is the largest decline in the
Consumer Price Index since Core inflation, which excludes food and energy prices, stood
at a annualized rate.



Equities


Virtually all of the major U.S. equity indices declined dramatically during the ended May
and volatility soared, as the U.S. equity markets reacted to the litany of bad economic
news. Equity markets were characterized by negativity, particularly in September and October.
Early optimism that the equity markets had bottomed in October was quickly undercut
and a new low was established on with the fear of a potential bankruptcy of Citibank.
Uncertainty over the extent of government intervention in the financial and automotive sectors
added to selling pressures as well. The equity markets subsequently rebounded in December on hopes
that the new Presidential administration would take swift action to address the economic and
financial problems the U.S. was facing. When Treasury Secretary Geithner revealed the
administrations plans in February, there were few details and stocks slumped to yet new lows in
early March. Then, as economic green shoots began to appear, consumer sentiment improved. The
Treasury Department finally revealed details on key parts of its financial rescue plans, and
investors recognized that the government was not interested in nationalizing companies that could
be viable as privately-owned enterprises. U.S. equities rallied strongly. The major equity indices
then enjoyed gains through May, making it three months in a rowthe longest monthly winning streak
since August through Despite growing concerns about building inflationary pressures,
including oil prices jumping more than since the start of the equity markets big push
since early March primarily reflected a belief among investors that the economy was getting ready
to recover, probably later this year or early in


Still, the late-period rally in the equity markets was not enough to recoup prior months losses,
and so virtually all equity asset classes generated steep double-digit declines for the
period overall. Within the U.S. equity market, small-cap stocks declined least followed closely by
large-cap stocks. Mid-cap stocks lagged a bit further behind. Growth stocks modestly outperformed
value stocks across the capitalization spectrum.














International equity markets overall, as measured by the MSCI EAFE Index, posted similar declines
to the U.S. equity markets, underperforming by only a modest margin. The sharp declines were
widespread, experienced across geographic regions, sectors and market capitalizations. On a
country basis, all markets declined over the one-year period. Switzerland, Japan and Hong Kong were
the relative outperformers, while the U.K., aided in the latter months of the fiscal year by the
markets high weighting to oil and metal stocks, modestly underperformed the MSCI EAFE Index.
Ireland, Austria, Belgium and Norway were the worst performers. The emerging equity markets as a
whole slightly outperformed the developed equity markets on a relative basis during the
ended boosted by the waning of risk aversion in the last months of the fiscal year.
Indeed, in early March, as in the U.S., negative sentiment among international equity investors
rather quickly turned more hopeful, shifting from extreme risk aversion to an environment in which
risk-taking was handsomely rewarded. However, such a late-period rally was not enough to offset the
negative effects of the banking crisis in which went far beyond subprime mortgages and spread
to every corner of economic activity, leaving no safe havens in which to hide. On a sector basis,
all ten sectors declined. However, the traditionally defensive sectors, such as health care and
consumer staples, outperformed the MSCI EAFE Index, while the cyclical sectors lagged. Materials
and financials were the worst performing sectors for the annual period, despite their strong
rebound since Materials in particular outperformed significantly late in the period
on the back of rising oil and metal prices in anticipation of restocking and a resumption of demand
by China.



Fixed Income


The annual period ended was a tale of two halves for the fixed income market, as bond
returns reflected shifting economic conditions. During the first half, interest rates across the
Treasury yield curve declined dramatically as the economy weakened, the financial markets seized
up, and a classic flight to quality gripped the bond market with enhanced vigor. The Treasury yield
curve steepened, meaning short-term rates declined more than long-term rates, as the Fed reduced
the targeted federal funds rate by basis points by the end of bringing
it to a range of to For non-Treasury sectors, spreads, or the difference in yields
between these securities and Treasuries, widened, as investor risk aversion remained high.


Strong government response, led by the Fed, the Treasury Department and the FDIC, helped stabilize
the financial markets late in Since that time, interest rates across the Treasury yield curve
rose, as the economy was seen as bottoming and financial conditions improved. Still, the Treasury
yield curve remained steep, as the Fed held the targeted federal funds rate near zero in an effort
to stimulate economic activity. Spreads on non-Treasury sectors tightened in to date in
response to the credit and quantitative easing by the various government arms.


For the annual
period overall, two-year Treasury yields fell to a level of Treasury yields
dropped to and Treasury yields declined to


Agency-issued mortgage-backed securities were the best performing sector, boosted primarily by Fed
support programs. U.S. Treasury security returns, which dominated during the first half of the
period, followed, weakening in through May, as investor risk aversion ebbed. Still, U.S.
Treasury securities generated solid positive returns for the annual period. Returns of government
agency securities followed those of Treasuries close behind. Asset-backed securities generated
modest positive returns, and corporate bonds eked out barely positive returns, dragged down by
financials-related issues. Commercial mortgage-backed securities (CMBS)were by far the worst
performing sector, posting double-digit negative returns. High-yield corporate bonds and emerging
market debt, which are not included in the Barclays Capital U.S. Aggregate Index, posted the worst
returns during the first half of the year but significantly outpaced U.S. Treasuries during the
second half of the year as risk premiums drifted lower. Overall, high-quality issues dramatically
outperformed low-quality issues, and, in general, intermediate maturities (that is, those with
maturities of seven to ten years) outperformed both longer and shorter maturities.


The tax-exempt bond market overall significantly outperformed the equity markets but lagged the
taxable fixed income market for the annual period, due primarily to difficulties experienced during
the first half of the fiscal year. In fact, for the second half of the annual period, the
tax-exempt fixed income market significantly outperformed both the equity market and the taxable
fixed income market.















Money Markets


The annual period ended was one filled with unprecedented events for both the taxable
and tax-exempt money markets, as dramatic action by the Fed, increasing weakness and subsequent
green shoots in economic growth, the global financial crisis and the resulting liquidity freeze
had great effect.



* * *


Maintaining a long-term perspective is a basic tenet of effective investing for both managers and
investors at all times. In the annual period like the one ended such a perspective is
especially important. By resisting real yet what are admittedly emotional urges and staying
invested in assets allocated based on your individual goals, you may lessen the effects of severe
corrections, such as that experienced this period, and keep time on your side.


We thank you for being a part of the PNC Mutual Funds. We value your ongoing confidence in us and
look forward to serving your investment needs in the years ahead.



Best Regards,
