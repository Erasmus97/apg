Dear Shareholder:


We are pleased to present this semiannual report for Dreyfus Municipal Bond Fund, covering the six-month period from September through February

The reporting period was extremely challenging for investors.The U.S. economy has continued to weaken amid plunging housing prices, rising unemployment and tight credit conditions. Meanwhile, the
worst financial crisis of our generation has driven major financial institutions and automobile manufacturers to the brink of bankruptcy.Although government and monetary authorities have responded aggressively with massive bailouts, low interest
rates, liquidity injections and economic stimulus programs,most asset classes have performed poorly.Municipal bonds have encountered heightened volatility as credit conditions and the fiscal conditions of most states and municipalities deteriorated,
but the major municipal bond indices still managed to eke out positive total returns for the reporting period.

We so far have seen no signs of imminent improvement in the economic climate, but the financial markets appear to have priced in investors generally low expectations. In previous
recessions, the markets have tended to anticipate economic rebounds before they occur, leading to major market rallies when few expected them.That is why it makes sense to remain disciplined, maintain a long-term perspective and adopt a consistent
asset allocation strategy that reflects ones future goals and attitudes toward risk. As always, we urge you to consult with your financial advisor, who can recommend the course of action that is right for you. For information about how the
fund performed during the reporting period, as well as market perspectives, we have provided a Discussion of Fund Performance given by the funds Portfolio Manager.

Thank you for your continued confidence and support.

















DISCUSSION OF FUND PERFORMANCE

For the period of September through February as provided by James Welch, Senior Portfolio Manager

Fund and Market Performance Overview

For the six-month period ended February Dreyfus Municipal Bond Fund produced a total return of
In comparison, the Barclays Capital Municipal Bond Index (the Index), the funds benchmark, produced a total return of average total return for the funds
in the Lipper Municipal Debt Funds category was for the same

Municipal bonds suffered bouts of poor liquidity and heightened volatility due to a severe financial crisis and economic downturn. The fund produced lower returns than its benchmark mainly due to
the severe effects of the recession and financial crisis on lower rated municipal bonds. However, the fund produced higher returns than its category average, as relatively defensive security selection and interest-rate strategies cushioned losses
during downdrafts but limited participation in market rallies.

The Funds Investment Approach

The fund seeks to maximize current income exempt from federal income tax, to the extent consistent with the preservation of capital.

To pursue this goal, the fund normally invests substantially all of its net assets in municipal bonds that provide income exempt from federal income tax.The fund will invest at least of its
assets in municipal bonds rated A or better or the unrated equivalent as determined by The fund may invest
up to of its assets in municipal bonds rated below A or the unrated equivalent as determined by Dreyfus, including bonds rated below investment-grade quality (high yield or junk bonds).The dollar-weighted average maturity
of the funds portfolio is not restricted, but normally exceeds years.

We may buy and sell bonds based on credit quality, market outlook and yield potential. In selecting municipal bonds for investment, we may

The Fund











assess the current interest rate environment and the municipal bonds potential volatility under different interest rate scenarios. We focus on bonds with the potential to offer attractive
current income or that are trading at attractive market prices.A portion of the funds assets may be allocated to discount bonds, which are bonds that sell at a price below their face value, or to premium bonds, which
are bonds that sell at a price above their face value. The funds allocation to either discount bonds or premium bonds will change along with our changing views of the current interest rate and market environments.We also may look to select
bonds that are most likely to obtain attractive prices when sold.

Financial Crisis and Recession Sparked Heightened Volatility

An intensifying credit crisis and a severe recession roiled most financial markets, including municipal bonds, over the reporting period. Slumping home values, rising unemployment and plunging
consumer confidence contributed to one of the worst recessions since the Great Depression, putting pressure on the fiscal conditions of most states and municipalities. Meanwhile, an ongoing credit crunch escalated in September into a global
financial crisis that punished a number of large financial institutions, including major municipal bond insurers and dealers. These developments sparked a flight to quality in which investors fled riskier assets in favor of traditional
safe havens, especially U.S.Treasury securities.

While municipal bonds generally fared far better than riskier assets, such as equities, yield differences between municipal bonds and comparable Treasuries moved toward historically wide levels
over the first half of the reporting period, when deleveraging among institutional investors resulted in widespread selling pressure even for creditworthy securities. A subsequent market rally helped the municipal bond market regain some of its
previous losses as investors began to recognize attractive values among bonds from fundamentally sound issuers.

Defensive Strategies Cushioned Losses

In this tumultuous market environment, we adopted a generally defensive investment posture. Whenever market liquidity allowed, we attempted to upgrade the funds credit profile by reducing
its positions











in corporate-backed municipal bonds in favor of general obligation bonds and essential-purpose revenue bonds from municipal issuers we considered fiscally sound.We generally avoided new purchases
of bonds from California and other states experiencing severe budget pressures.

Our interest-rate strategies also reflected a conservative position. We maintained the funds average duration in a range we considered modestly shorter than industry averages. Although this
strategy prevented the fund from participating more fully in the benefits of declining short-term interest rates, it helped mitigate the effects of heightened market volatility.We generally focused on bonds in the to maturity range,
which provided much of the yield of longer-term bonds but entailed less risk should inflation accelerate as a result of massive government borrowing and spending.

Maintaining a Cautious Investment Posture

As of the reporting periods end, the U.S. economy has remained weak, and the financial crisis has persisted. However, municipal bond prices currently appear to reflect investors low
economic expectations, and we have begun to identify attractive values among bonds that may have been punished too severely in the downturn. Consequently, while we have maintained a defensive posture in anticipation of heightened market volatility
over the foreseeable future, we may begin to seek to take advantage of opportunities later in the year.

March





Total return includes reinvestment of dividends and any capital gains paid. Past performance is no guarantee of future results. Share price, yield and investment return fluctuate such
