Dear
Fellow Shareholder,
We are
pleased to provide you with the Quantitative Group of Funds' Annual Report for
the twelve months ended March 31, 2009 to update you on recent market conditions
and the performance of the Quant Funds.
For
current performance information, please visit our website at www.quantfunds.com.
We thank you for your continued confidence in the Quant Funds. Please feel free
to e-mail us at feedback@quantfunds.com or call us at 800-326-2151 with any
questions or for assistance on your account.
Sincerely,


