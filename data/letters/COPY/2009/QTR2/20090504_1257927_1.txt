Dear Fellow Shareholder:
          The economy continued to weaken during
the first quarter of 2009 and the stock market extended its losses. By early March, the S&amp;P
500 had fallen an additional 25%. Investor gloom was thick and pervasive. Then, for no apparent reason, stocks began to go up. At
this writing the S&amp;P has climbed 26% from its March 6 low. Bear market rallies can be explosive and the recent move may well be
another false start. We continue to believe, though, that stocks will turn up long before the economic news turns positive and that
it is impractical to try to wait to invest until all the market “uncertainties” are cleared up.
          The
“relative” performance of each of our stock funds (vs. the S&amp;P 500) was positive in the quarter (see table below). Several
smaller company stocks were particularly helpful to Hickory and Partners III. “Absolute” performance has been harder to come by over
the past year and a half. Partners III was the only stock fund showing black ink for the first calendar quarter.
          Our fixed income funds have shown positive returns over the past quarter and 12-months. Chaos in the credit markets over the past
several quarters has created opportunities for Tom Carney and Brad to buy corporate bonds at very attractive prices for our
Short-Intermediate Income and Balanced Funds. Tom has also found bargains in tax-free municipal bonds for our Nebraska Tax-Free
Income Fund.
          There is additional information on each of our eight funds following this letter. Portfolio holdings as of March 31, major changes in
holdings and contributors to performance are shown in a series of tables, and there is commentary about each fund in the “Management
Discussion &amp; Analysis” sections (MD&amp;A’s). Since this is the “Annual Report” for our March 31 fiscal year, there is also commentary in
the MD&amp;A’s on the major factors affecting results for the past 12 months and data on portfolio turnover, expenses, etc.
          The
table below shows investment results over various time periods (after deducting fees and expenses) for our four stock funds and
for the S&amp;P 500 (larger companies), the Russell 2000 (smaller companies), and the Nasdaq Composite (a proxy for technology
companies).
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
Total Returns*
 
Average Annual Total Returns*
 
 
 
 
 
 
 
3 Mos.
 
1 Year
 
3 Year
 
5 Year
 
10 Year
 
15 Year
 
20 Year
 
25 Year
 
 
 
 
 
 
 
 
 
 
 
Value
 
-9.3
%
 
-38.6
%
 
-17.0
%
 
-9.0
%
 
-0.4
%
 
7.4
%
 
8.5
%
 
N/A
 
 
Partners Value**
 
-4.4
 
 
-32.0
 
 
-13.7
 
 
-6.6
 
 
0.5
 
 
8.4
 
 
9.3
 
 
10.9
 
 
Hickory
 
-1.7
 
 
-35.3
 
 
-16.3
 
 
-6.9
 
 
-2.6
 
 
6.8
 
 
N/A
 
 
N/A
 
 
Partners III**
 
1.6
 
 
-26.7
 
 
-12.0
 
 
-4.7
 
 
4.0
 
 
10.0
 
 
10.5
 
 
11.6
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
-11.0
 
 
-38.1
 
 
-13.0
 
 
-4.8
 
 
-3.0
 
 
5.9
 
 
7.4
 
 
9.3
 
 
 
-15.0
 
 
-37.5
 
 
-16.8
 
 
-5.2
 
 
1.9
 
 
4.9
 
 
N/A
 
 
N/A
 
 
 
-2.8
 
 
-32.3
 
 
-12.5
 
 
-4.4
 
 
-4.1
 
 
4.9
 
 
6.8
 
 
7.5
 
 
These performance numbers reflect the deduction of each Fund’s annual operating expenses. The current annual operating expenses for
the Value, Partners Value, Hickory and Partners III Opportunity Funds, as stated in the most recent Prospectus are 1.16%, 1.16%,
1.23% and 1.54%, respectively, of each Fund’s net assets. This information represents past performance and past performance does not
guarantee future results. The investment return and the principal value of an investment in any of the Funds will fluctuate so that
an investor’s shares, when redeemed, may be worth more or less than the original cost. Current performance may be higher or lower
than the performance data quoted above. Performance data current to the most recent month-end may be obtained at
www.weitzfunds.com/performance/monthly.asp.
  
  
* 
All performance numbers assume reinvestment of dividends (except for the 15, 20 and 25 year Nasdaq numbers for which reinvestment of
dividend information was not available).
  
  
** 
  
  
# 
Index performance is hypothetical and is for illustrative purposes only.
4
 
 
 
          The current recession has
unfolded more dramatically and threatens to be longer and deeper than any since
the 1930’s. Government policy responses have been massive and there is
anecdotal evidence of improvement in the credit and housing markets, yet most
economic indicators continue to deteriorate. The S&amp;P 500 fell by 56% from
its October 2007 high to the March 2009 low and has been incredibly
volatile—the market’s mood swings are giving many investors whiplash.
          Our initial response to the developing
credit crisis and recession was, paraphrasing Kipling, to “keep our heads while
those about us were losing theirs.” Taking patient, contrary positions in the
face of “temporary” bad news has served us well for the past 25+ years, but the
scale of this bear market has made that approach expensive. As our friend Tom
Gayner pointed out in the recent Markel annual letter, “pure unadulterated
panic would have been the best investment approach to take in 2008.” 
          So, how are we investing as
this economic storm unfolds? The goal of our research process has not changed:
We look for understandable businesses with strong balance sheets and defensible
competitive positions that generate excess cash and are run by managements we
trust to allocate capital well. We estimate the price that an informed buyer
would pay for the whole business and try to buy shares at a deep discount to
that price. Given the state of the economy and the capital markets, we pay
particular attention to each company’s likely capital needs, debt maturity
schedule, loan covenants, etc. We have always tried to anticipate how each
company would deal with a recession, but we are now trying to be even more
imaginative as we ponder what might go wrong.
          Many of the companies we
already own are performing well, yet their stocks have fallen significantly. If
a company’s long-term business prospects are still good and its price falls
precipitously, we are happy to add to our position. Over the past six months,
as the market has staged several strong rallies followed by renewed weakness,
we have been able to use this volatility to upgrade the portfolios, harvest tax
losses, and capture some modest trading profits.
          We reduced our exposure to
financial service companies in late 2007 and the first half of 2008 as it
became clear that the paralysis of the credit markets was not going to be a
short-term phenomenon. We also realized the degree to which many banks, savings
and loans and mortgage companies had been “over-earning” because of the cheap
credit generated through the securitization of mortgages and other types of
loans. There will undoubtedly be some spectacular winners among financial
stocks when this bear market is over, but until we have a better understanding
of how financial company business models will adapt to the new realities of
tighter, more expensive credit and a new regulatory climate, we are likely to
move slowly in that arena.
          Our largest “financial”
holding now is Berkshire Hathaway which has a large insurance business but is
very broadly diversified. Our other significant financial position is Redwood
Trust. Redwood invests in troubled mortgage securities—a target-rich area in
today’s world, to say the least. Redwood does not use borrowed money to buy
these securities, so while their investments are subject to credit risk,
Redwood is not subject to margin calls and cannot be forced to sell into a weak
market. 
          We were arguably
over-exposed to retail and consumer products in general as the recession began
and we have been adjusting these positions opportunistically over the past
year. As consumer-related stocks have gyrated in response to alternate waves of
optimism and pessimism, we have reduced overall exposure, tried to concentrate
on companies offering staples and “value” (e.g. Wal-Mart), and have been more
willing than usual to “trade” portions of our major positions when the price
swings have been unusually wide. 
          Another area of interest for
us in recent quarters has been commodity-related companies. As we wrote in our
last quarterly letter, we have generally avoided energy, metals, fertilizer and
other commodity producers. These tend to be capital intensive, cyclical, low
return on capital businesses, so we were never seriously tempted to “chase” the
stocks during their spectacular bull market. That “bubble” burst abruptly in
mid-2008 and prices of most commodities collapsed (e.g. oil prices fell from
$147 in July to $34 eight months later). 
5
 
 
 
per share 
Outlook
trillions 
          We would expect this tug of
war between the bullish and bearish forces to continue for some time—maybe
years—but our working assumption is that the American economy will emerge from
this recession in recognizable form. We expect that good companies will find
ways to cope with this environment and come out of this period intact. We are
wary of the short-term economic outlook, but we believe that there are a number
of quality businesses whose stocks are selling well below their conservatively
calculated business values and that we will eventually be rewarded for buying
these stocks at bargain prices. We appreciate shareholders’ patience during
this extraordinary period.
 
 
 
Sincerely,


