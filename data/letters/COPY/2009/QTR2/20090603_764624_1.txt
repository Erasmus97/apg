Dear Shareholder,
The U.S. economy weakened significantly during the twelve-month reporting period ended
Looking back, U.S. gross domestic product (GDP)i growth was during the second quarter of Contributing to the economys expansion were rising exports that were buoyed by a weakening U.S. dollar. In addition,
consumer spending increased, aided by the governments tax rebate program. However, the dollars rally and the end of the rebate program, combined with other strains on the economy, caused GDP to take a step backward during the second half
of According to the U.S. Department of Commerce, third and fourth quarter GDP contracted and respectively, the latter being the worst quarterly reading since Economic weakness continued in early as the advance
estimate for first quarter GDP decline was It may seem like ancient history, but when the reporting period began, speculation remained as
to whether the U.S. would experience a recession. This ended in December when the National Bureau of Economic Research (NBER) which has the final say on when one begins and ends announced that a recession had begun in
December Based on a variety of economic indicators, we believe it is now likely that the current recession could be the lengthiest since the Great Depression. Contributing to the economys troubles is the accelerating weakness in the
labor market. Since December approximately jobs have been shed, with more than two million being lost during the first three months of In addition, the unemployment rate continued to move steadily higher, rising from
to in March to reach its highest rate since Another strain on the economy, the housing market, has yet to bottom. This is
evidenced by the decline in new home sales in February versus February In addition, the median price of a new home fell more than over those twelve months. However, not all of the economic news is bad. Inflation remains low and,
in March data were released showing increases in durable goods orders, manufacturing and consumer sentiment, albeit all from depressed levels.
Ongoing issues related to the housing and subprime mortgage markets and seizing credit markets prompted the
Federal Reserve Board (Fed)ii to take aggressive and, in some cases, unprecedented actions. When the reporting









Legg Mason Partners New York Municipals Fund

I







Letter from the chairman continued

period began, the federal funds rateiii was The Fed continued its campaign of lowering rates in April to It then left
rates on hold for several months due to growing inflationary pressures as a result of soaring oil and commodity prices, coupled with the sagging U.S. dollar. However, as inflation receded along with oil prices and the global financial crisis
escalated, the Fed cut rates twice in October to Then, in December it reduced the federal funds rate to a range of zero to a historic low and maintained this stance during its next meetings in January, March and
April In conjunction with the April meeting, the Fed stated that it will employ all available tools to promote economic recovery and to preserve price stability. The Committee . . . anticipates that economic conditions are likely to
warrant exceptionally low levels of the federal funds rate for an extended period. In addition to the interest rate cuts, the Fed took several
actions to improve liquidity in the credit markets. In September it announced an billion rescue plan for ailing AIG and pumped billion into the financial system as Lehman Brothers bankruptcy and mounting troubles at other
financial firms roiled the markets. More recently, the Fed has taken additional measures to thaw the frozen credit markets, including the purchase of debt issued by Fannie Mae and Freddie Mac, as well as introducing the Term Asset-Backed Securities
Loan Facility (TALF). In March the Fed continued to pursue aggressive measures as it announced its intentions to:




Purchase up to an additional billion of agency mortgage-backed securities, bringing its total purchases of these securities to up to trillion in





Increase its purchases of agency debt this year by up to billion to a total of up to billion.





Buy up to billion of longer-term Treasury securities over the next sixmonths.
The U.S. Department of the Treasury has also taken an active role in attempting to stabilize the financial system, as it orchestrated the governments takeover
of mortgage giants Fannie Mae and Freddie Mac in September In October, the Treasurys billion Troubled Asset Relief Program (TARP) was approved by Congress and signed into law by former President Bush. Then, in March
Treasury Secretary Geithner introduced the Public-Private Partnership Investment Program (PPIP), which will be used to facilitate the purchase of billion to trillion of troubled mortgage assets from bank balance sheets.
President Obama has also made reviving the economy a priority in his administration, the cornerstone thus far being the billion stimulus package that was signed into law in February
During the twelve-month reporting period ended both short- and long-term Treasury yields experienced periods of extreme volatility.









II

Legg Mason Partners New York Municipals Fund








While earlier in investors were focused on the subprime segment of the mortgage-backed market, these concerns broadened to include a wide range of
financial institutions and markets. As a result, other fixed-income instruments also experienced increased price volatility. This unrest triggered several flights to quality, causing Treasury yields to move lower (and their prices
higher), while riskier segments of the market saw their yields move higher (and their prices lower). This was particularly true toward the end of as the turmoil in the financial markets and sharply falling stock prices caused investors to flee
securities that were perceived to be risky, even high-quality corporate bonds and high-grade municipal bonds. On several occasions, the yield available from short-term Treasuries fell to nearly zero, as investors were essentially willing to forgo
any return potential in order to access the relative safety of government-backed securities. During the twelve months ended two-year Treasury yields fell from to Over the same time frame, ten-year Treasury yields
moved from to The municipal bond market underperformed its taxable bond
counterpart over the twelve months ended Over that period, the Barclays Capital Municipal Bond Indexiv and the Barclays Capital U.S. Aggregate Indexv returned and respectively. While municipal
securities significantly outperformed the taxable market in January and February it was not enough to overcome the aforementioned flights to quality, which negatively impacted the tax-free bond market.












Lipper Fund Award Recipient
In the category of New York Municipal Debt Funds, Legg Mason Partners New York Municipals Fund (Class I) was named Best Individual Fund among ninety-three New York
Municipal Debt Funds for risk-adjusted performance for the three-year period ended at the Lipper FundAwards.

The annual Lipper Fund Awards recognize funds that have been leaders in delivering consistently strong
risk-adjusted performance, relative to their peers, for the three-, five- or ten-year time periods. All of us at Legg Mason are very proud of this award and the Western Asset Management Company




The Lipper Fund Awards winners are selected using the Lipper Leader rating for consistent returns for funds with at least thirty-six months of performance history as of
the end of the evaluation year. Awards are presented for the highest Lipper Leader for Consistent Return within each eligible classification over three, five or ten years. Lipper ratings for Consistent Return reflect funds historical
risk-adjusted returns, adjusted for volatility, relative to peers as of The ratings are subject to change every month, and are based on an equal-weighted average of percentile ranks for the Consistent Return metric over
three-, five- and ten-year periods (if applicable). The awards incorporate risk-adjusted performance data as of A high Lipper rating does not necessarily imply that a fund had the best total performance or that the fund
achieved positive results for that period. ClassA was rated second in this category. Lipper, Inc., a wholly-owned subsidiary of Reuters, provides independent insight on global collective investments. Lipper ratings are not intended to predict
future results, and Lipper does not guarantee the accuracy of this information. Past performance is no guarantee of future results.








Legg Mason Partners New York Municipals Fund

III







Letter from the chairman continued

portfolio management team that received this honor, particularly at a time of tremendous market challenges.
A special note regarding increased market volatility In recent
months, we have experienced a series of events that have impacted the financial markets and created concerns among both novice and seasoned investors alike. In particular, we have witnessed the failure and consolidation of several storied financial
institutions, periods of heightened market volatility, and aggressive actions by the U.S. federal government to steady the financial markets and restore investor confidence. While we hope that the worst is over in terms of the issues surrounding the
credit and housing crises, it is likely that the fallout will continue to impact the financial markets and the U.S. economy well into Like all
asset management firms, Legg Mason has not been immune to these difficult and, in some ways, unprecedented times. However, todays challenges have only strengthened our resolve to do everything we can to help you reach your financial goals.
Now, as always, we remain committed to providing you with excellent service and a full spectrum of investment choices. Rest assured, we will continue to work hard to ensure that our investment managers make every effort to deliver strong long-term
results. We also remain committed to supplementing the support you receive from your financial advisor. One way we accomplish this is through our
enhanced website, www.leggmason.com/individualinvestors. Here you can gain immediate access to many special features to help guide you through difficult times, including:





Fund prices and performance,




Market insights and commentaries from our portfolio managers, and





A host of educational resources. During
periods of market unrest, it is especially important to work closely with your financial advisor and remember that reaching ones investment goals unfolds over time and through multiple market cycles. Time and again, history has shown that,
over the long run, the markets have eventually recovered and grown. Information about your fund
As you may be aware, several issues in the mutual fund industry have come under the scrutiny of federal and state regulators. Affiliates of the Funds manager
have, in recent years, received requests for information from various government regulators regarding market timing, late trading, fees, and other mutual fund issues in connection with various investigations. The regulators appear to be examining,
among other things, the Funds response









IV

Legg Mason Partners New York Municipals Fund








to market timing and shareholder exchange activity, including compliance with prospectus disclosure related to these subjects. The Fund is not in a
position to predict the outcome of these requests and investigations. Please read on for a more detailed look at prevailing economic and market
conditions during the Funds reporting period and to learn how those conditions have affected Fund performance. Important information with
regard to recent regulatory developments that may affect the Fund is contained in the Notes to Financial Statements included in this report. As
always, thank you for your confidence in our stewardship of your assets. We look forward to helping you meet your financial goals. Sincerely,
