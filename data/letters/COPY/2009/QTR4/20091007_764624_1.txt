Dear Shareholder,
While the U.S. economy remained weak during the twelve-month reporting period ended there were indications that the worst may be over. Looking back, the U.S. Department of Commerce reported that third and
fourth quarter U.S. gross domestic product (GDP)i contracted and respectively. The economic contraction accelerated during the first quarter of
as GDP fell However, the news was relatively better during the second quarter, as the preliminary estimate for GDP was a decline. The economys more modest contraction was due, in part, to smaller declines in both exports and
increased business spending. The U.S. recession, which began in December now has the dubious distinction of being the lengthiest since the
Great Depression. Contributing to the economys troubles has been extreme weakness in the labor market. Since December approximately jobs have been shed and we have experienced nineteen consecutive months of job losses.
In addition, the unemployment rate remains high, reported as in July Another strain on the economy, the housing market, may finally be
getting closer to reaching a bottom. After plunging late in new single-family home starts have been fairly stable and sales of single-family homes increased for the third consecutive month in June In addition, while home prices have
continued to fall, the pace of the decline has moderated somewhat. Other recent economic news also seemed to be less negative. Inflation remained low, manufacturing contracted at a slower pace and job losses in July were the lowest
monthly amount since August Ongoing issues related to the housing and subprime mortgage markets and seizing credit markets
prompted the Federal Reserve Board (Fed)ii to take aggressive and, in some cases, unprecedented actions. After reducing the federal funds rate
iii from in August to a range of to percent in December a historic low the Fed has maintained this stance thus far in In conjunction with
its August meeting, the Fed stated that it will maintain the target range for the federal funds rate at to percent and continues to anticipate that economic conditions are likely to warrant exceptionally low levels of the federal
funds rate for an extended period.








Legg Mason Partners Strategic Income Fund

I







Letter from the chairman continued
In addition to maintaining
extremely low short-term interest rates, the Fed took several actions to improve liquidity in the credit markets. Back in September it announced an billion rescue plan for ailing AIG and pumped billion into the financial system as
Lehman Brothers bankruptcy and mounting troubles at other financial firms roiled the markets. More recently, the Fed has taken additional measures to thaw the frozen credit markets, including the purchase of debt issued by Fannie Mae and
Freddie Mac, as well as introducing the Term Asset-Backed Securities Loan Facility (TALF). In March the Fed continued to pursue aggressive measures as it announced its intentions to:





Purchase up to an additional billion of agency mortgage-backed securities, bringing its total purchases of these securities to up to trillion in





Increase its purchases of agency debt this year by up to billion to a total of up to billion.





Buy up to billion of longer-term Treasury securities over the next sixmonths.
The U.S. Department of the Treasury has also taken an active role in attempting to stabilize the financial system, as it orchestrated the governments takeover
of mortgage giants Fannie Mae and Freddie Mac in September In October, the Treasurys billion Troubled Asset Relief Program (TARP) was approved by Congress and signed into law by former President Bush. Then, in March
Treasury Secretary Geithner introduced the Public-Private Partnership Investment Program (PPIP), which is intended to facilitate the purchase of troubled mortgage assets from bank balance sheets. President Obama has also made
reviving the economy a priority in his administration, the cornerstone thus far being the billion stimulus package that was signed into law in February
During the twelve-month reporting period ended both short- and long-term Treasury yields experienced periods of extreme volatility. When the period began, two- and ten-year Treasury yields were and
respectively. While earlier in investors were focused on the subprime segment of the mortgage-backed market, these concerns broadened to include a wide range of financial institutions and markets. As a result, other fixed-income instruments
also experienced increased price volatility. This unrest triggered several flights to quality, causing Treasury yields to move lower (and their prices higher), while riskier segments of the market saw their yields move higher (and their
prices lower). This was particularly true toward the end of as the turmoil in the financial markets and sharply falling stock prices caused investors to flee securities that were perceived to be risky, even high-quality corporate bonds and
high-grade municipal bonds. When the first half of the reporting period ended on two- and ten-year Treasury yields were and









II

Legg Mason Partners Strategic Income Fund








respectively. During the second half of the period, Treasury yields generally moved higher (and their prices lower) until early June. Two- and ten-year
yields peaked at and respectively, before falling and ending the reporting period at and respectively. In a reversal from investor risk aversion faded as the twelve-month reporting period progressed, driving spread
sector (non-Treasury) prices higher. For the twelve-month period ended the Barclays Capital U.S. Aggregate Indexiv returned
The high-yield bond market produced positive results for the twelve months ended After generating extremely poor results
from September through November the asset class posted positive returns during seven of the last eight months of the reporting period. This strong rally was due to a variety of factors, including signs that the frozen credit markets were
thawing, some modestly better economic data and increased demand from investors searching for higher yields. All told, over the twelve months ended the Citigroup High Yield Market Index
v After falling sharply in September
and October emerging market debt prices rallied sharply posting positive returns during eight of the last nine months of the reporting period. This was triggered by firming and, in some cases, rising commodity prices, optimism that the
worst of the global recession was over and increased investor risk appetite. Over the twelve months ended the JPMorgan Emerging Markets Bond Index Global (EMBI Global)
vi returned Special shareholder notices
Effective September the Funds portfolio managers are S. Kenneth Leech, Stephen A. Walsh, Edward A. Moody, Carl L. Eichstaedt,
Mark S. Lindbloom, Michael C. Buchanan and Keith J. Gardner. The portfolio managers are responsible for the day-to-day portfolio management and oversight of the Fund and are employed by Western Asset Management Company. Messrs. Leech, Walsh,
Lindbloom, Buchanan and Gardner have been portfolio managers of the Fund since February and Messrs. Moody and Eichstaedt became portfolio managers of the Fund on May The Fund is managed by a team of portfolio managers, sector
specialists and other investment professionals. The portfolio managers lead the team and their focus is on portfolio structure, including sector allocation, durationvii
weighting and term structure decisions. Effective the Funds name changed from Legg Mason Partners Diversified Strategic
Income Fund to Legg Mason Partners Strategic Income Fund.








Legg Mason Partners Strategic Income Fund

III







Letter from the chairman continued
A special note regarding
increased market volatility Dramatically higher volatility in the financial markets has been very challenging for many investors. Market movements
have been rapid sometimes in reaction to economic news, and sometimes creating the news. In the midst of this evolving market environment, we at Legg Mason want to do everything we can to help you reach your financial goals. Now, as always,
we remain committed to providing you with excellent service and a full spectrum of investment choices. Rest assured, we will continue to work hard to ensure that our investment managers make every effort to deliver strong long-term results.
We also remain committed to supplementing the support you receive from your financial advisor. One way we accomplish this is through our enhanced
website, www.leggmason.com/individualinvestors. Here you can gain immediate access to many special features to help guide you through difficult times, including:




Fund prices and performance,




Market insights and commentaries from our portfolio managers, and





A host of educational resources. During
periods of market unrest, it is especially important to work closely with your financial advisor and remember that reaching ones investment goals unfolds over time and through multiple market cycles. Time and again, history has shown that,
over the long run, the markets have eventually recovered and grown. Information about your fund
Please read on for a more detailed look at prevailing economic and market conditions during the Funds reporting period and to learn how those conditions have
affected Fund performance. Important information with regard to recent regulatory developments that may affect the Fund is contained in the Notes to
Financial Statements included in this report.








IV

Legg Mason Partners Strategic Income Fund







As always, thank you for your
confidence in our stewardship of your assets. We look forward to helping you meet your financial goals. Sincerely,
