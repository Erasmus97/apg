Dear Shareholder:
Our market outlook continues to be cautiously optimistic for the short term through the remainder of 2009 and into 2010. The uncertainties surrounding the US financial markets, recession, housing, and the world markets are beginning to ease.  
Volatility has begun to moderate in the markets recently.  Over the last 90 days the VIX has traded in a range of 23-33, a significant improvement over the last year.  Implied volatility is a measurement of the risk portion of option premium, which is calculated by one of the option pricing models.  It is a significant factor in determining how the marketplace values perceived risk over the next 1 ½ months.  
The price of oil has begun to rebound, as expectation of the ending recession begins to be forecast by many industry analysts and Federal Reserve Chief Ben Bernanke.  A barrel of oil is trading at about $70 and natural gas has jumped to over $1 per MCF in the last 30 days.  This has provided some modest support for the coal stocks.  However, demand remains strong worldwide.  The growth of China, India, Eastern Europe and other third world countries has slowed, but consumption of coal has been maintained.
 
We continue to acquire GNMAs for the fixed income portion of the Fund’s portfolio.  GNMAs continue to be our largest core fixed income investments with coupons ranging from 5 ½ - 7% (GNMAs represent 40% of the portfolio).  The GNMAs should contribute strength and a defensive posture to our portfolio as interest rates continue to be lowered.  We have also added some selective tax-free municipals.  These securities are typically zero-coupon bonds.  We have targeted securities which are 100% defeased and backed by US Treasuries averaging 4.75% - 5.25%.  The average life of these securities will average 10-15 years.
The second half of 2009 could see a continuing base beginning to process.  As we move into 2010 the equities with energy could begin to lead again.
As always, if you have any questions about your investment in the Jacobs & Company Mutual Fund, please call us.  Thank you again for allowing us to help you achieve your investment objectives.
Sincerely,


