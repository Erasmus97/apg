Dear Shareholder:

During the period ended September economic activity weakened
before showing signs of stabilization. The slowdown began in with the U.S.
subprime mortgage and credit crises and eventually spread worldwide. Despite
coordinated efforts by many governments to address spreading liquidity and
credit problems, deteriorating economic conditions and mounting uncertainty
contributed to market woes through the period's first half. Later in the period,
some indicators offered a better economic outlook, and markets rallied beginning
in March erasing much of the earlier losses. At period-end, although some
observers thought the worst of the economic crises was behind us, others
believed significant challenges remained. We think it is important to put
short-term market developments in perspective. Keep in mind that as uncertain as
current conditions may be, we have navigated through past periods of high market
volatility by remaining committed to our long-term perspective and disciplined
investment philosophy. During such times, we search for bargains that we believe
may be well positioned to become eventual winners. Although conditions remain
challenging, our experience gives us reason to be optimistic about future market
stabilization and recovery.

Franklin Custodian Funds' annual report goes into greater detail about
prevailing conditions during the period under review. In addition, the portfolio
managers discuss investment management decisions and Fund performance for the
period. You will also find performance data and financial information. Please
remember that all securities markets fluctuate, as do mutual fund share prices.

NOT FDIC INSURED | MAY LOSE VALUE | NO BANK GUARANTEE


Not part of the annual report |



If you would like more frequent updates, franklintempleton.com provides daily
prices, monthly performance figures, portfolio holdings and other information.
You can also access your account, buy and sell shares, read timely articles, and
find helpful financial planning tools. We hope you will take advantage of these
online services.

Although market conditions are constantly changing, we remain committed to our
disciplined strategy as we manage the Funds. As always, we recommend investors
consult their financial advisors and review their portfolios to design a
long-term strategy and portfolio allocation that meet their individual needs,
goals and risk tolerance. We firmly believe that most people benefit from
professional advice, and that advice is invaluable as investors navigate current
market conditions.

We thank you for investing with Franklin Templeton, welcome your questions and
comments, and look forward to serving your investment needs in the years ahead.

Sincerely,
