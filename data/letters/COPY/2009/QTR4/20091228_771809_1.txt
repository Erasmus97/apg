Dear Fellow Shareholders: 
I am pleased to present the annual report and investment performance review of the Eagle mutual funds for the fiscal year ended October 31, 2009 (“reporting
period”). 
The reporting period was one of extremes for equity investors. We began with credit fears permeating the market, the economy in a severe
recession and headlines filled with a barrage of negative economic data. These conditions led to a massive selloff phase that lasted through early March. The final step in this decline was based, in large part, on fears over the potential collapse
of our banking system, an outcome that fortunately was avoided. This period was followed by a dramatic recovery phase that continued through the end of the reporting period. The recovery phase was fueled by massive financial stimulus and liquidity
programs by governments worldwide. In this phase, profitable operations have returned to the large banks at the center of the credit crisis, credit markets have loosened and corporate earnings have improved. 
As we begin to emerge from the longest recession in the post-World War II era, uncertainty still exists as to the strength and sustainability of the recovery. Many
economists are expecting a modest recovery in gross domestic product over the next year and there is the potential for improving corporate profits. Concerns exist about the level of government debt resulting from the stimulus and other spending
programs. Such debt levels have the potential to result eventually in higher taxes or inflation, neither of which is good for investors. Unemployment, which is slow to drop in a recovery, will likely remain high. 
In light of the dramatic market volatility, we believe that a consistent and disciplined investing strategy is more important than ever. 
 
In the commentaries that follow, each fund’s portfolio managers discuss the specific
performance in their funds. While there can be no way to accurately predict short-term market movements, our portfolio managers hope to take advantage of investment opportunities that may arise during the current market. Our portfolio managers base
their investment strategies on idea generation and proprietary research and avoid momentum-driven investments, risky bets and trendy market securities with unreasonable risk/return tradeoffs. 
If you would like to begin receiving this report and other reports from the Eagle Family of Funds electronically, please visit our website, eagleasset.com and enroll
for electronic delivery. Doing so will reduce the amount of paper we consume which saves the Funds (and their shareholders) money and will help the environment. Enrolling in this service will not affect the delivery of your account statements or
other confidential communications. 
To obtain a prospectus, which contains important information about the Eagle Family of Funds, contact your financial
advisor, call us at 800.421.4184 or visit our website at eagleasset.com. Our website also has timely information about the funds, including performance and portfolio holdings. We are grateful for your continued support and confidence in the Eagle
Family of Funds. 
Sincerely, 


