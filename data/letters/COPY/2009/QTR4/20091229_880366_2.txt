Dear Shareholder,
At a meeting held in August the Funds Board of Trustees approved a recommendation from Legg Mason Partners Fund Advisor, LLC, the Funds investment manager, to change the fiscal
year-end of the Fund from to As a result of this change, shareholders are being provided with a short period annual report and a stub-period audit for the ten-month period from
through Please read on for a more detailed look at the prevailing economic and market conditions during the
Funds abbreviated reporting period and to learn how those conditions have affected Fund performance. Important information with regard to recent regulatory developments that may affect the Fund is contained in the Notes to Financial Statements
included in thisreport. As always, thank you for your confidence in our stewardship of your assets. We look forward to helping you
meet your financial goals. Sincerely,
