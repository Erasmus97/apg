Dear Shareholder,
Vanguard Wellesley Income Fund’s portfolio of high-quality bonds and dividend-paying blue-chip stocks performed well in a tough economic environment, returning about 9% for the fiscal year ended September 30, 2009. This result closely aligned with the fund’s blended benchmark, the Wellesley Composite Index (+9.12%), and significantly outperformed the average return of mixed-asset target conservative funds (+5.10%).
Wellesley’s strong performance relative to the broad stock market (–5.83%) was largely influenced by the outstanding performance of high-quality bonds during the fiscal year. The fund’s fixed income portfolio returned about 18% for the period, compared with about –6% for its equity counterpart.
The fund’s 30-day SEC yield was 3.66% for the Investor Shares and 3.76% for the Admiral Shares as of September 30.
If you own the fund in a taxable account, you may wish to review the details of its after-tax returns, contained later in this report.
