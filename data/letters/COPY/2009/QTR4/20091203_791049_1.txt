Dear Fellow Shareholder:

We know it. And, you know it too. The United States has seen better
economic days.

Like it or not, unpleasant market cycles do occur periodically. This tends
to distract some people's focus from the longer-term objective that influenced
their investment decision in the first place.

Our shareholder and financial professional surveys have consistently
indicated over the years that the original investment objective and decision for
investors in Tax-Free Trust of Oregon, has generally been aligned with the
Trust's objective: to seek as high a level of current income exempt from Oregon
state and regular Federal income taxes as is consistent with preservation of
capital.

How does the Trust seek to fulfill its and your investment objective?

Perhaps the single most significant matter we wish to emphasize is the
element of professional management that we have continually sought to bring to
bear on your behalf. The job of investment managers is to be keen observers of
the scene, setting aside emotions. This is particularly true in the constantly
changing environment that exists today. One must make every effort to be as
objective as possible, adjusting the portfolio of investments as necessary, to
try to be of most benefit to shareholders.

Management of Tax-Free Trust of Oregon and its municipal bond portfolio
management team believe that when you are dealing with investments, quality
counts.

As you may recall, there are nine separate credit ratings assignable to
municipal securities, ranging from the most conservative to the highly
speculative. For protection of investors' capital, the Trust intentionally
limits its purchases to securities rated (or, if unrated, deemed by the
Investment Sub-Adviser to be) investment grade quality - that is rated within
the four highest credit ratings: AAA, AA, A, and BBB.

NOT A PART OF THE ANNUAL REPORT



In general, the higher the quality rating of a municipal security, the
greater or more reliable the cash flow there is for the municipality to cover
interest and principal payments when due on the security. While exaggerated
price changes may occur in emotionally charged securities markets, they normally
are not reflective of a municipal issuer's capability to pay interest and
principal in a timely manner on any particular security. It is the cash flow and
solidness of the municipal issuer that count - and this is reflected in the
quality level of the credit rating.

We can assure you that the Trust's portfolio management team pays
considerable attention to this factor before any security is purchased for the
portfolio as well as in conducting continuing analysis and evaluation with each
and every security once it is a part of the Trust's investment portfolio. It is
additionally important for you to know that, with any insured securities, our
portfolio management team has always sought to look beyond the insurance to the
credit quality of the underlying issuer rather than relying upon any insurance.

We fully recognize that the current times can be unsettling. However, we
hope that you are comforted to know that we believe you have a knowledgeable
team of financial experts, which has continually sought to carefully choose the
securities in the Trust's portfolio and seeks to continuously monitor your
investment in Tax-Free Trust of Oregon.

Sincerely,
