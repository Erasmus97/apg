Dear Shareholder:



Economic Review



The Federal Reserve in their latest meeting kept the Fed Funds
rate steady as expected and signaled that it is not yet
satisfied with the recent drop in core inflation to the top end
of its long-assumed comfort zone. While officials acknowledged
recent price improvement and said they expect inflation to keep
moderating, they said the downtrend hasnt been
convincingly established. They also upgraded their
economic assessment, further evidence that officials arent
contemplating rate cuts in the foreseeable future. Many
economists expect Fed policy to remain on hold through the end
of



Despite the recent drop in core inflation, the
predominant concern at the Federal Reserve is that
this trend may not be sustainable. Officials have said on many
occasions that high resource utilization and tight labor markets
are the main portions of the economy that may lead to future
inflation risk. Recent reports on employment and manufacturing
have been largely upbeat suggesting the economy will expand at
potential going forward.



A global consolidation of the broad equity markets has been
evident recently and includes all market capitalizations. This
was somewhat expected due to the extreme optimism and overbought
condition. Whats uncertain is whether stocks find support
and rebound from the bottom of the range that they have
established, or if they retreat to lower support established in
February of this year. One thing that is certain, is the rise in
volatility in the equity markets. Although this rise is coming
from historically low levels, it remains significant because it
could signal a change in trend. Although aware of the broad
based interest rate pressure on equities, we remain bullish on
the market until further evidence leads us to change.



Although economic growth is expected to pick up somewhat,
conditions will remain mixed across sectors. There remain a
number of risks to the outlook: higher energy prices, a further
correction in the housing market, slower productivity growth,
and higher bond yields.



Sincerely,
