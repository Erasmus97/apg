Dear Shareholders:

Stock markets rallied strongly during the one-year period ended June that
comprised the funds' fiscal It is very satisfying to see double-digit
one-year returns across all of our equity funds. Our approach of emphasizing
companies that we believe have excellent financial prospects and a good record
of social responsibility was a winning formula. I am particularly pleased with
the results of Citizens Value Fund which is approaching five years under our
in-house management. Bill Rocco at Morningstar has called this fund a "worthy
core holding for socially conscious investors." We agree and hope you will
consider the fund as an option for your socially responsible investment
portfolio.

An interesting discussion that has been taking place in the socially responsible
investing community is the consideration of how socially responsible investors
can have the greatest impact. Some funds have recently adopted what they are
calling a "positive" approach to screening, looking for companies that have
positive social characteristics instead of screening out those with negative
practices. Other fund groups are actively investing in specific companies in
order to gain a platform for advocating change. Still others are investing with
an eye only to financial return but charging their social research team with
executing specific social initiatives. Each of these strategies presents an
opportunity and we look forward to partnering in the future with funds taking
new approaches, as well as strengthening our relationships with existing
institutional investors employing traditional social screens similar to
Citizens.

(PHOTO OF SOPHIA COLLIER)

"IT IS VERY SATISFYING TO SEE DOUBLE-DIGIT ONE-YEAR RETURNS ACROSS ALL OF OUR
EQUITY FUNDS."

- -- SOPHIA COLLIER, PRESIDENT
