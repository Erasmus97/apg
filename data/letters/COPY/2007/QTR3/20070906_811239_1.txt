Dear Fellow Shareholder:

Rising interest rates, reaching for yield and tightening yield curves - these
are all topics that have been in the news as of late. While there is a plethora
of information available on these topics and others if you are interested, the
good news is that when it comes to Tax-Free Fund of Colorado you don't need to
be versed in "financialese" any more than you wish.

Why? Because one of the conveniences afforded you through an investment in
Tax-Free Fund of Colorado is local active professional portfolio management. The
Fund's portfolio manager continuously monitors economic, monetary and financial
trends and adjusts the portfolio accordingly as and when he deems appropriate.

Your Fund's portfolio manager provides ongoing surveillance of the marketplace
and strives to maneuver cautiously through its ups and downs - much like a
ship's captain whose job it is to ride out sudden storms.

Not unlike the prudent ship's captain who ensures that the cargo is properly
balanced and positioned, the Fund's portfolio manager is constantly monitoring
the investment portfolio and the economic weather conditions striving to provide
as stable a net asset value and as high a level of tax-free income as possible.

As with a ship in choppy seas, your investment requires constant vigilance and
nimble reflexes in all phases of its management. This does not, however, mean
selling and buying securities continuously based upon market blips. What it does
mean is being a keen observer of the scene, monitoring securities very carefully
and taking action as appropriate whenever significant changes occur or are
anticipated.

NOT A PART OF THE SEMI-ANNUAL REPORT


Your portfolio manager seeks opportunities that make sense within the prevailing
marketplace and which provide double tax-free income while maintaining the
overall conservative nature of the portfolio.

In short, your professional manager does the work for you. So, the next time you
come across a boring, complex analysis of the bond market, feel free to flip
right past it. Your Fund's portfolio management team's job (and their passion)
is to keep on top of and make sense out of all the news. Thus, like a seasoned
traveler, you can enjoy your trip knowing that there are experienced eyes
watching the waters and steady hands on the helm.

Sincerely,
