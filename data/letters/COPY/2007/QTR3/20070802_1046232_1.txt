Dear Shareholder:

We are pleased to provide the annual report for Evergreen Intermediate Municipal Bond Fund covering the twelve-month period ended May

The domestic fixed-income markets, including the municipal bond market, delivered moderate, positive returns during the twelve-month period. Interest rates, and therefore bond prices, showed relatively little volatility
over most of the fiscal year as the economys growth appeared to decelerate and inflationary fears receded. The U.S. Federal Reserve Board, after repeatedly raising the fed funds rate in the preceding months in an effort to quell price
pressures, hiked the key rate to in June The nations central bank then left the influential short-term interest rate unchanged for the remainder of the period. Market interest rates on longer-maturity securities declined somewhat,
which resulted in a flattening of the yield curve a narrowing of the difference in yields between short-term and longer-maturity bonds and better performance by securities with longer maturities. At the same time, the continued expansion of the economy encouraged
investors to seek higher yields, while lower-quality, higher-yielding municipal securities tended to outperform high-grade bonds.

The trajectory of the economys growth showed clear signs of a deceleration as the fiscal year progressed. U.S. Gross Domestic Product grew by a rate of during then decelerated to a rate of for the first
three months of Weakness in









LETTER TO SHAREHOLDERS continued

housing activity was the most visible evidence of a softening in the economy, but solid job growth, rising wages and increases in personal consumption and business investment helped sustain the positive momentum of economic
activity. The growth in corporate profits showed few signs of moderation during the twelvemonth period, and equity markets produced healthy returns for the fiscal year, overcoming some periods of short-term volatility. Periodically interrupting the
upward movement of stock prices were a variety of concerns, ranging from international geopolitical tensions to worries about a return to the stagflation that undermined growth a quarter century earlier.

During this period, Evergreens municipal bond fund managers analyzed the factors that influenced bond values, managing each portfolio within its investment universe in pursuit of both total return and yield. The portfolio managers of Evergreen Municipal Bond Fund, Evergreen Intermediate Municipal Bond Fund and Evergreen Short-Intermediate
Municipal Bond Fund, for example, all extended their funds duration to capture the price appreciation potential as the yield curve flattened. At the same time, they sought selective opportunities among lower-rated, higher-yielding securities.
The manager of Evergreen High Grade Municipal Bond Fund also extended the funds duration, while maintaining a focus on the high quality, insured municipal securities. The team managing the Evergreen Strategic Municipal Bond Fund, meanwhile,
sought opportunities to increase yield without extending duration by placing greater emphasis on higher quality cushion bonds, which offer









LETTER TO SHAREHOLDERS continued

competitive yield, relatively stable prices and greater protection against the risks of rising interest rates.

Please visit us at EvergreenInvestments.com for more information about our funds and other investment products available to you. Thank you for your continued support of Evergreen Investments.

Sincerely,
