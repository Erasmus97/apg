Dear Shareholder:
We are pleased to present this annual report for the JPMorgan
Mid/Multi Cap Funds for the 12 months ended June 30, 2007. Inside, youll find information detailing the performance of the Funds, along with
reports from the portfolio managers.
