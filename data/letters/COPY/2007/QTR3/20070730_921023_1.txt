Dear Shareowner,
- --------------------------------------------------------------------------------
Staying diversified and keeping your portfolio invested in the markets are two
general investment principles that have served investors well over time. Those
were particularly useful guides during the past year, as U.S. and global stock
and bond markets grew strongly during the period.

In the months ending May global stock markets were supported by
relatively low short-term interest rates and generally strong economies. The
U.S. stock market, as measured by the Standard & Poor's Stock Index, gained
approximately over the period. International developed and emerging
markets equities performed even better, with MSCI's indexes of those regions
rising approximately and respectively. The general U.S. bond market, as
measured by the Lehman Brothers Aggregate Bond Index, returned roughly The
high yield market, as measured by the Merrill Lynch High Yield Bond Master II
Index, returned approximately during the same period.

While still strong, the rate of U.S. economic growth has slowed over the past
year. That was due in large part to a slowdown in new home construction and in
part to the lagging effects of rising energy and commodity prices as well as
rising short-term interest rates. But slowing economic activity is also due in
part to the natural maturation of the cyclical expansion as U.S. factories
approach full utilization and the labor markets approach full employment.

We have enjoyed a cyclical recovery with strong economic growth, and while U.S.
economic growth has slowed, we believe that continuing growth at a slower rate
appears to be more likely than a recession. The Federal Reserve Board has
indicated a reduced likelihood of future rate hikes, but continues to highlight
its commitment to keeping inflationary pressures contained. This is in keeping
with "best practices" among the world's central banks: low and stable inflation
is believed to be the best backdrop for stable economic growth and low average
unemployment.

In Europe, healthy labor markets are supporting growing consumption and solid
GDP growth, helped by productivity gains and a positive operating environment
for European companies that are finding strong export markets for their goods
and services around the globe. European inflationary pressures appear to be
largely under control,




Letter

with the European Central bank remaining strongly vigilant. Japanese economic
growth continues to make progress, and the country has become a more attractive
market as deflationary problems recede. Economic growth in emerging market
countries remains faster than in the developed world as they continue to "catch
up." Leading the way is China, which continues its rise as a world economic
power.

Looking forward, we believe that the overall climate for investors generally
will continue to be positive, although valuations are less attractive than they
were a year ago and a correction after a period of strong performance is
possible. Sudden swings in the markets are always to be expected. Just as
staying diversified and invested are important investment principles, it is also
important to pay attention to asset allocation. As always, we encourage
shareholders to work closely with their financial advisor to find the mix of
stocks, bonds and money market assets that is aligned to their particular risk
tolerance and investment objective.

Respectfully,
