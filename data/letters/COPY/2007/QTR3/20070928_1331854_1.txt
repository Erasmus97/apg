Dear Shareowner,
- --------------------------------------------------------------------------------
Staying diversified and keeping your portfolio invested in the markets are two
general investment principles that have served investors well over time. Those
were particularly useful guides during the past year, as U.S. and global stock
and bond markets grew strongly during the period.

In the months ending July equity investors were generally rewarded
as, despite a late-July decline, the Standard & Poor's Index generated a
return of over that period, the Dow Jones Industrial Average returned
and the NASDAQ Composite returned International developed and emerging
markets equities performed even better than U.S. equities, with the MSCI EAFE
Developed Market Index returning and the MSCI Emerging Markets Index
returning in the period.

The U.S. bond market, as measured by the Lehman Aggregate Bond Index, returned
in the months ending July with returns comprising both coupon
income and price gains linked to a modest decline in interest rates over the
months. The high-yield market, as measured by the Merrill Lynch High Yield Bond
Master II Index, returned reflecting its higher coupon yields and
continuing investor confidence in the strength of the U.S. economy.

U.S. economic growth has slowed recently, but continuing growth at a moderate
rate appears more likely than a recession. Slowing growth was due in large part
to a decline in the rate of new home construction and in part to the lagging
effects of rising energy and commodity prices and rising short-term interest
rates. It was also due to the natural maturation of the cyclical expansion as
U.S. factories approach full utilization and the labor market approaches full
employment. This slowdown, therefore, was not entirely unwelcome, as it reduces
the threat of higher inflation.

The Federal Reserve Board (the Fed) continues to highlight its commitment to
keeping inflationary pressures contained. This is in keeping with "best
practices" among the world's central banks: low and stable inflation is
believed to be the best backdrop for stable economic growth and low average
unemployment over the long term. Keeping inflation low is also an important
support for stock and bond valuations, and so the Fed's policy is "investor
friendly."




Letter

In Europe, solid GDP growth driven by a positive operating environment for
European companies, especially those that are benefiting from strong export
markets for their goods and services, is pushing unemployment lower and
supporting growing consumption. European inflationary pressures appear to be
largely under control, with the European Central bank remaining strongly
vigilant. Japanese economic growth continues, and the country has become a more
attractive market as deflationary problems recede. Economic growth in emerging
market countries remains faster than in the developed world as they continue to
"catch up," led by China, which continues its rise as a world economic power.

Looking forward, the economic outlook and equity valuations continue to appear
generally positive, although the current environment is plagued by fears that
sub-prime mortgage problems will result in a systemic liquidity/credit crunch.
While falling risk tolerances may depress asset prices in the short term,
valuations appear reasonable if credit market problems do not spread to the
broader economy.

Sudden swings in the markets are always to be expected. Just as staying
diversified and invested are important investment principles, it is also
important to pay attention to asset allocation. As always, we encourage
shareholders to work closely with their financial advisor to find the mix of
stocks, bonds and money market assets that is aligned to your particular risk
tolerance and investment objective.

Respectfully,

Dear Shareowner,
- --------------------------------------------------------------------------------
Staying diversified and keeping your portfolio invested in the markets are two
general investment principles that have served investors well over time. Those
were particularly useful guides during the past year, as U.S. and global stock
and bond markets grew strongly during the period.

In the months ending July equity investors were generally rewarded
as, despite a late-July decline, the Standard & Poor's Index generated a
return of over that period, the Dow Jones Industrial Average returned
and the NASDAQ Composite Index returned International developed and
emerging markets equities performed even better than U.S. equities, with the
MSCI EAFE Developed Market Index returning and the MSCI Emerging Markets
Index returning in the period.

The U.S. bond market, as measured by the Lehman Aggregate Bond Index, returned
in the months ending July with returns comprising both coupon
income and price gains linked to a modest decline in interest rates over the
months. The high-yield market, as measured by the Merrill Lynch High Yield Bond
Master II Index, returned reflecting its higher coupon yields and
continuing investor confidence in the strength of the U.S. economy.

U.S. economic growth has slowed recently, but continuing growth at a moderate
rate appears more likely than a recession. Slowing growth was due in large part
to a decline in the rate of new home construction and in part to the lagging
effects of rising energy and commodity prices and rising short-term interest
rates. It also stemmed from the natural maturation of the cyclical expansion as
U.S. factories approach full utilization and the labor market approaches full
employment. This slowdown, therefore, was not entirely unwelcome, as it reduces
the threat of higher inflation.

The Federal Reserve Board (the Fed) continues to highlight its commitment to
keeping inflationary pressures contained. This is in keeping with "best
practices" among the world's central banks: low and stable inflation is
believed to be the best backdrop for stable economic growth and low average
unemployment over the long term. Keeping inflation low is also an important
support for stock and bond valuations, and so the Fed's policy is "investor
friendly."





Letter

In Europe, solid GDP growth driven by a positive operating environment for
European companies, especially those that are benefiting from strong export
markets for their goods and services, is pushing unemployment lower and
supporting growing consumption. European inflationary pressures appear to be
largely under control, with the European Central bank remaining strongly
vigilant. Japanese economic growth continues, and the country has become a more
attractive market as deflationary problems recede. Economic growth in emerging
market countries remains faster than in the developed world as they continue to
"catch up," led by China, which continues its rise as a world economic power.

Looking forward, the economic outlook and equity valuations continue to appear
generally positive, although the current environment is plagued by fears that
sub-prime mortgage problems will result in a systemic liquidity/credit crunch.
While falling risk tolerances may depress asset prices in the short term,
valuations appear reasonable if credit market problems do not spread to the
broader economy.

Sudden swings in the markets are always to be expected. Just as staying
diversified and invested are important investment principles, it is also
important to pay attention to asset allocation. As always, we encourage
shareholders to work closely with their financial advisor to find the mix of
stocks, bonds and money market assets that is aligned to your particular risk
tolerance and investment objective.

Respectfully,

Dear Shareowner,
- --------------------------------------------------------------------------------
Staying diversified and keeping your portfolio invested in the markets are two
general investment principles that have served investors well over time. Those
were particularly useful guides during the past year, as U.S. and global stock
and bond markets grew strongly during the period.

In the months ending July equity investors were generally rewarded
as, despite a late-July decline, the Standard & Poor's Index generated a
return of over that period, the Dow Jones Industrial Average returned
and the NASDAQ Composite returned International developed and emerging
markets equities performed even better than U.S. equities, with the MSCI EAFE
Developed Market Index returning and the MSCI Emerging Markets Index
returning in the period.

The U.S. bond market, as measured by the Lehman Aggregate Bond Index, returned
in the months ending July with returns comprising both coupon
income and price gains linked to a modest decline in interest rates over the
months. The high-yield market, as measured by the Merrill Lynch High Yield Bond
Master II Index, returned reflecting its higher coupon yields and
continuing investor confidence in the strength of the U.S. economy.

U.S. economic growth has slowed recently, but continuing growth at a moderate
rate appears more likely than a recession. Slowing growth was due in large part
to a decline in the rate of new home construction and in part to the lagging
effects of rising energy and commodity prices and rising short-term interest
rates. It also stemmed from the natural maturation of the cyclical expansion as
U.S. factories approach full utilization and the labor market approaches full
employment. This slowdown, therefore, was not entirely unwelcome, as it reduces
the threat of higher inflation.

The Federal Reserve Board (the Fed) continues to highlight its commitment to
keeping inflationary pressures contained. This is in keeping with "best
practices" among the world's central banks: low and stable inflation is
believed to be the best backdrop for stable economic growth and low average
unemployment over the long term. Keeping inflation low is also an important
support for stock and bond valuations, and so the Fed's policy is "investor
friendly."





Letter

In Europe, solid GDP growth driven by a positive operating environment for
European companies, especially those that are benefiting from strong export
markets for their goods and services, is pushing unemployment lower and
supporting growing consumption. European inflationary pressures appear to be
largely under control, with the European Central bank remaining strongly
vigilant. Japanese economic growth continues, and the country has become a more
attractive market as deflationary problems recede. Economic growth in emerging
market countries remains faster than in the developed world as they continue to
"catch up," led by China, which continues its rise as a world economic power.

Looking forward, the economic outlook and equity valuations continue to appear
generally positive, although the current environment is plagued by fears that
sub-prime mortgage problems will result in a systemic liquidity/credit crunch.
While falling risk tolerances may depress asset prices in the short term,
valuations appear reasonable if credit market problems do not spread to the
broader economy.

Sudden swings in the markets are always to be expected. Just as staying
diversified and invested are important investment principles, it is also
important to pay attention to asset allocation. As always, we encourage
shareholders to work closely with their financial advisor to find the mix of
stocks, bonds and money market assets that is aligned to your particular risk
tolerance and investment objective.

Respectfully,

Dear Shareowner,
- --------------------------------------------------------------------------------
Staying diversified and keeping your portfolio invested in the markets are two
general investment principles that have served investors well over time. Those
were particularly useful guides during the past year, as U.S. and global stock
and bond markets grew strongly during the period.

In the months ending July equity investors were generally rewarded
as, despite a late-July decline, the Standard & Poor's Index generated a
return of over that period, the Dow Jones Industrial Average returned
and the NASDAQ Composite returned International developed and emerging
markets equities performed even better than U.S. equities, with the MSCI EAFE
Developed Market Index returning and the MSCI Emerging Markets Index
returning in the period.

The U.S. bond market, as measured by the Lehman Aggregate Bond Index, returned
in the months ending July with returns comprising both coupon
income and price gains linked to a modest decline in interest rates over the
months. The high-yield market, as measured by the Merrill Lynch High Yield Bond
Master II Index, returned reflecting its higher coupon yields and
continuing investor confidence in the strength of the U.S. economy.

U.S. economic growth has slowed recently, but continuing growth at a moderate
rate appears more likely than a recession. Slowing growth was due in large part
to a decline in the rate of new home construction and in part to the lagging
effects of rising energy and commodity prices and rising short-term interest
rates. It also stemmed from the natural maturation of the cyclical expansion as
U.S. factories approach full utilization and the labor market approaches full
employment. This slowdown, therefore, was not entirely unwelcome, as it reduces
the threat of higher inflation.

The Federal Reserve Board (the Fed) continues to highlight its commitment to
keeping inflationary pressures contained. This is in keeping with "best
practices" among the world's central banks: low and stable inflation is
believed to be the best backdrop for stable economic growth and low average
unemployment over the long term. Keeping inflation low is also an important
support for stock and bond valuations, and so the Fed's policy is "investor
friendly."



Letter

In Europe, solid GDP growth driven by a positive operating environment for
European companies, especially those that are benefiting from strong export
markets for their goods and services, is pushing unemployment lower and
supporting growing consumption. European inflationary pressures appear to be
largely under control, with the European Central bank remaining strongly
vigilant. Japanese economic growth continues, and the country has become a more
attractive market as deflationary problems recede. Economic growth in emerging
market countries remains faster than in the developed world as they continue to
"catch up," led by China, which continues its rise as a world economic power.

Looking forward, the economic outlook and equity valuations continue to appear
generally positive, although the current environment is plagued by fears that
sub-prime mortgage problems will result in a systemic liquidity/credit crunch.
While falling risk tolerances may depress asset prices in the short term,
valuations appear reasonable if credit market problems do not spread to the
broader economy.

Sudden swings in the markets are always to be expected. Just as staying
diversified and invested are important investment principles, it is also
important to pay attention to asset allocation. As always, we encourage
shareholders to work closely with their financial advisor to find the mix of
stocks, bonds and money market assets that is aligned to your particular risk
tolerance and investment objective.

Respectfully,

Dear Shareowner,
- --------------------------------------------------------------------------------
Staying diversified and keeping your portfolio invested in the markets are two
general investment principles that have served investors well over time. Those
were particularly useful guides during the past year, as U.S. and global stock
and bond markets grew strongly during the period.

In the months ending July equity investors were generally rewarded
as, despite a late-July decline, the Standard & Poor's Index generated a
return of over that period, the Dow Jones Industrial Average returned
and the NASDAQ Composite returned International developed and emerging
markets equities performed even better than U.S. equities, with the MSCI EAFE
Developed Market Index returning and the MSCI Emerging Markets Index
returning in the period.

The U.S. bond market, as measured by the Lehman Aggregate Bond Index, returned
in the months ending July with returns comprising both coupon
income and price gains linked to a modest decline in interest rates over the
months. The high-yield market, as measured by the Merrill Lynch High Yield Bond
Master II Index, returned reflecting its higher coupon yields and
continuing investor confidence in the strength of the U.S. economy.

U.S. economic growth has slowed recently, but continuing growth at a moderate
rate appears more likely than a recession. Slowing growth was due in large part
to a decline in the rate of new home construction and in part to the lagging
effects of rising energy and commodity prices and rising short-term interest
rates. It was also due to the natural maturation of the cyclical expansion as
U.S. factories approach full utilization and the labor market approaches full
employment. This slowdown, therefore, was not entirely unwelcome, as it reduces
the threat of higher inflation.

The Federal Reserve Board (the Fed) continues to highlight its commitment to
keeping inflationary pressures contained. This is in keeping with "best
practices" among the world's central banks: low and stable inflation is
believed to be the best backdrop for stable economic growth and low average
unemployment over the long term. Keeping inflation low is also an important
support for stock and bond valuations, and so the Fed's policy is "investor
friendly."




Letter

In Europe, solid GDP growth driven by a positive operating environment for
European companies, especially those that are benefiting from strong export
markets for their goods and services, is pushing unemployment lower and
supporting growing consumption. European inflationary pressures appear to be
largely under control, with the European Central bank remaining strongly
vigilant. Japanese economic growth continues, and the country has become a more
attractive market as deflationary problems recede. Economic growth in emerging
market countries remains faster than in the developed world as they continue to
"catch up," led by China, which continues its rise as a world economic power.

Looking forward, the economic outlook and equity valuations continue to appear
generally positive, although the current environment is plagued by fears that
sub-prime mortgage problems will result in a systemic liquidity/credit crunch.
While falling risk tolerances may depress asset prices in the short term,
valuations appear reasonable if credit market problems do not spread to the
broader economy.

Sudden swings in the markets are always to be expected. Just as staying
diversified and invested are important investment principles, it is also
important to pay attention to asset allocation. As always, we encourage
shareholders to work closely with their financial advisor to find the mix of
stocks, bonds and money market assets that is aligned to your particular risk
tolerance and investment objective.

Respectfully,
