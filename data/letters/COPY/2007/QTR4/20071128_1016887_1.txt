Dear Profit Fund Shareholders:

During the fiscal year ended September The Profit Fund returned
percent versus the S&P Index's percent return. The graphic and table
below show the historical performance (in percentage terms) of The Profit Fund
and the S&P Index (the "Index") over each fiscal year since

THE PROFIT FUND VS. S&P INDEX
(Total Returns for Fiscal Years Ended September

[BAR CHART OMITTED]


MARKET REVIEW:

The best performing sector in the Index during fiscal year was Energy,
which was up percent. Supply constraints, combined with growing demand
from developing countries have driven energy prices to record highs. Exxon, with
a percent weight in the Index and a percent return was the largest
contributor to the Index performance.

The rising energy costs and weakening dollar have fueled inflation fears and a
rise in commodity prices. Specifically, the demand for ethanol has driven up the
price of corn, a major ingredient in the production of ethanol. As a result,
Materials was the second best performing sector in the Index and rose
percent. Even though Materials has the smallest weight in the Index, companies
like the Monsanto Company, which was up percent during the fiscal year,
enabled the Materials sector to make a significant contribution to benchmark
returns.





Telecommunications was the best performing sector last fiscal year and turned in
another stellar performance of percent this year. The bulk of the
performance was driven by Verizon Communications and AT&T, Inc. Both companies
are benefitting from the growth in the demand for wireless data.

Information Technology was the worse performer in but roared back with a
stellar performance rising percent in fiscal year Apple Inc. was the
source of much of the outperformance, more than doubling as a result of product
innovations like the i-Phone. The Index also benefited materially from EMC
Corporation, Juniper Networks Corporation and NVIDIA Corporation. which were up
percent, percent, and percent, respectively. NVIDIA and
Juniper have both benefitted from the desire to watch video over the internet
driving a demand for hi definition video cards.

The Health Care sector underperformed the Index, rising only percent. The
large cap pharmaceuticals which have significant weights in the Index are facing
patent expiration issues. Given lackluster research and development
productivity, they are forced to fuel the pipeline with expensive acquisitions.
Amgen and Johnson & Johnson also suffered from regulatory issues. Hospitals and
managed care companies continue to suffer from negative trends and bad debt. The
small biotech companies like Medimmune which have done well this past year due
to acquisition activity are too small in the Index to have a material impact.

It is no surprise that the worse performing sector was Financials. The housing
crisis was the catalyst for the weakness in Financials. Home foreclosures
doubled in September as subprime borrowers struggled to make payments on
adjustable rate mortgages. These foreclosures are aggravating the growing
housing decline by adding more homes onto a market where sales are dropping amid
a record breaking supply of unsold homes. All recent indicators point to further
deterioration, and we see no signs of stabilization on the horizon.

Several financial institutions (particularly the larger ones) are exposed to
subprime problems through collaterized debt obligations (CDO) or similar
derivatives. As defaults have occurred, the rating agencies have slashed the
ratings of CDO's thereby reducing their value. Since the real value of these
CDO's is still not fully understood, we do expect more write downs and greater
media attention. In summary, the problems have been recognized on a fundamental
basis but the uncertainty over the capital values could remain for a while.

FUND PERFORMANCE:

The Profit Fund outperformed the S&P Index during fiscal year
primarily due to positions in Information Technology and Consumer Discretionary
sectors. We were overweight the Consumer Discretionary and Technology sectors
and the names we were excited about worked. EMC stock has been on a tear, mostly
because of their interest in a company called VMware which is capitalizing on
the trend toward virtualization which enables increased productivity of back
office infrastructure. Garmin Ltd. has more than doubled, as the demand for
personal navigation devices continues to increase.





However, given our concerns about the economy in general, and the housing market
in particular, we took a more defensive posture by overweighting Health Care and
underweighting Financials. Our caution on the Financials sector worked as we
avoided the worst of the downturn in the sector. However, we were negatively
impacted by our positions in E Trade Financial, Moody's and Blackstone Group.

While the sector underperformed, our Health Care names outperformed. We were
able to offset the negative impact of Amgen on our portfolio with our position
in Cytyc. Cytyc Corporation was acquired by Hologic and more than doubled during
the year driving our returns in the Health Care sector to percent.

In hopes that we would see a pull back in commodity costs, we were underweight
Energy and Materials. Unfortunately, those sectors were the two best performing
sectors. Rather than chase these names which are trading at record highs, we
await a pull back to add to our positions.

We were also hurt by the Consumer Staples sector. Our sector positions rose
percent versus the Index sector positions which rose percent.
Specifically, our position in Wal-Mart Stores continues to be impacted by the
consumer malaise and apparel and home merchandising mis-steps.

PURCHASES AND SALES:
PURCHASES

o AKAMI - Akamai is an Internet delivery content services company. We
believe this company will be the long term benefactor as companies try to
optimize their web strategies.

o ALCOA - We initiated our position in Alcoa to benefit from the soaring
demand for aluminum.

o COMCAST - Comcast is winning business from their new Triple Play packages
which combine cable, internet and phone services on one bill.

o CITRIX - Citrix is owner of the popular GoTo brand which allows users to
remotely access their computers. The company is beginning to benefit from
a push into popular segments such as WAN optimization and Virtualization

o CSX - CSX Corp., through its subsidiaries, provides rail, intermodal and
rail-to-truck transload services. It operates railroad in the eastern U.S.
and provides coast-to-coast intermodal transportation. In addition to
having more pricing power, rail becomes more cost effective during periods
of high energy prices.

o EXXON MOBIL - We initiated this position to benefit from the upward trend
in oil prices.

o GENENTECH - Amgen's valuation became attractive after pulling back below
mainly over concerns over emerging competition to one of the
company's key drugs, as well as a general decline in Biotech. Genentech
has benefited from the missteps of Amgen. Earnings growth in is
expected to be a healthy percent for this biotech bellweather.





o GLOBALSANTAFE CORP. - We believe that energy prices will continue to rise
and accelerate the strong fundamental trends in GSF business. Given the
improving fundamentals and the aggressive cost cutting program, the
company valuation offered a compelling risk to reward proposition.

o GOLDMAN SACHS - The stock price of Goldman Sachs had been negatively
impacted by the market's concerns about their exposure to mortgage
derivatives. In fact, Goldman has successfully avoided absorbing huge
mortgage related losses. We viewed this as an opportunity to buy a high
quality financial company at a discounted valuation.

o ILLINOIS TOOL WORKS - This maker of fasteners, food services and welding
equipment is one of the few industrial companies that has not appreciated
much in We believe investors will eventually realize ITW is firing
on all cylinders and will purchase shares.

o JOHNSON & JOHNSON - In a weakening economy, investors will take safe haven
in health care giants.

o MARRIOTT - Marriott has some hidden assets such as the Ritz Carlton. Hotel
rates are increasing.

o ORACLE - The database king is growing faster than its competitors across
all of its business segments.

o QUALCOMM - Qualcomm has one of the largest patent portfolios in the
wireless space. The company should continue to collect huge royalties as
more than a billion cell phones are sold annually.

o TIFFANY - The luxury goods maker is seeing a strong demand from emerging
markets as consumers from these markets are prospering.

o VERIZON COMMUNICATIONS - We were attracted to the strong trends in
Verizon's FiOS TV. Also going forward we expect the consumer to increase
spending on wireless services due to the significant ramp up in data
services provided by companies like Verizon.

o WASTE MANAGEMENT - This garbage company has a monopoly like structure in
most of the cities it operates in.

o WHOLE FOODS - Recently acquired Wild Oats will help to reinvigorate
growth. This niche grocery store has a history of successfully integrating
acquisitions and realizing synergies.

SALES

o ADOBE - Adobe's valuation was unjustified and we felt the name would
underperform other technology positions.

o AETNA - The health insurer's enrollment targets appeared aggressive as of
early especially when compared to peers' guidance and the company's
own historic trends of late. Aetna had compromised pricing in order to
gain share in the past, something which did not go over well with
investors.





o APPLE - We believe Apple's valuation has gotten ahead of itself and it
will likely need to continue to innovate in order to maintain its high
earnings growth rate.

o CARNIVAL CORP. - After experiencing percent increase since the lower
Fed funds rate on September the downside risk outweighed the
appreciation potential.

o CATERPILLAR - The company reported weak declining fundamentals during its
earnings call. As a result, we chose to exit the position.

o CEPHALON - We had a strong return in CEPH and took profits at just the
right time, above earlier this year after the company reported a
strong then revised guidance upward to a level which appeared too
optimistic in our view.

o COUNTRYWIDE FINANCIAL - We exited this position due to our concerns about
the subprime woes.

o DEVON ENERGY - We exited this position when the security met our price
target.

o E*TRADE FINANCIALS - E Trade owns a bank with significant mortgage
exposure. We exited this position due to our concerns about the subprime
woes.

o FEDEX - FedEx does not have a good plan in place for international growth
and expansion.

o LEHMAN BROTHERS - Lehman brothers had significant positions in CDO's and
other mortgage derivatives. We exited this position due to our concerns
about the subprime woes.

o MOTOROLA - The once hot cell phone designer has struggled to come up with
a new hit cell phone since the RAZR.

o NVIDIA - The high end video card maker will likely start to see increased
competition from the recently merged AMD-ATI.

o SYMANTEC - The antivirus software maker is starting to face increased
competition from competitors who are giving away antivirus software for
free.

o WALGREEN - Walgreen recently missed earnings as a result of an inability
to manage cost in an environment in which proliferation of generics has
pressured gross margins. Having always been a growth company, we believe
it will be quite some time before they are able to adjust their operating
expenses to be in line with sales growth.

o MCCORMICK FOODS - We exited this position when the security met our price
target.

o PEPSI BOTTLING GROUP - We exited this position when the security met our
price target.

OUTLOOK:

Looking forward, we believe the factors that lead to the subprime lending
debacle have mostly ended. Credit requirements have tightened considerably and
even good FICA scores do not assure individuals will get a mortgage loan.
However, we do expect more write downs and greater media attention as more
clarity is added to the value of the CDO's. Further, we believe that the housing
decline will spill over into other areas of the economy and domestic corporate
profits will protract as a result of all the factors just mentioned.





On September the Fed cut the Fed funds rate by basis points to ease the
slowdown brought on by the mortgage decline. This was the first Fed funds rate
cut in over four years. The Fed cut the Fed funds rate and the discount rate
again by basis points on October while noting solid growth in the
third quarter and vigilance on inflation. The market responded positively to
both rate cuts, up almost percent on September and percent on October


However, the dollar has weakened further and is now near a record low.
Additionally, the price of gold and other commodities has risen materially.
These combined with a tight job market continue to drive concerns about rising
inflation. Therefore, we expect the market to tread water between now and the
end of the year. Longer term, we expect the Fed rate cuts to avert a recession,
but the market to continue to show weakness well into By the market
should begin to rebound from the housing malaise and grow more in line with
historical norms.

We would like to take the opportunity to express our sincere appreciation to our
valued and growing family of shareholders for entrusting your assets to our care
and your continued belief in The Profit Fund investment style. We look forward
to serving your investment needs for many years to come.

Sincerely,
