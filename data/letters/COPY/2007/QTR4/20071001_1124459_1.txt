Dear Shareholder:

I am pleased to present the enclosed Franklin Global Trust Funds' annual report
covering the fiscal year ended July In each Fund report, the Fund's
portfolio managers discuss market conditions and Fund performance. A complete
list of each Fund's holdings, as well as the financial statements, is included
in the annual report.

/s/ Rupert H. Johnson

Rupert H. Johnson, Jr.
President and Chief Executive Officer - Investment Management
Franklin Global Trust

- ---------------------------------------------------------
NOT FDIC INSURED | MAY LOSE VALUE | NO BANK GUARANTEE
- ---------------------------------------------------------


Annual Report |



Economic and Market Overview

During the months ended July the U.S. economy advanced at a
moderate but uneven pace. Although gross domestic product (GDP) grew at and
annualized rates in the third and fourth quarters of due in part to
strong domestic demand and a healthy increase in exports, the U.S. entered
with a record current account deficit. Corporate profits and government spending
generally remained robust. The struggling housing market grew more fragile with
the abrupt unraveling of the subprime mortgage market. This exacerbated already
weak housing prices and an inventory glut in most of the nation's residential
real estate markets. GDP growth slowed to an annualized rate in first
quarter In the second quarter, however, growth advanced at an estimated
annualized rate, supported by federal defense spending, accelerating
exports and declining imports, greater business inventory investment and
increased spending for nonresidential structures.

The unemployment rate decreased from at the beginning of the period to
in July Although consumer confidence in July neared a six-year high,
consumer spending slowed toward period-end as worries about the housing slump
and high gasoline prices made individuals more cautious. After a welcome drop in
energy costs during the latter half of elevated oil prices returned in
reaching a record high on July in response to perceived tight supply
and high demand for oil, refinery maintenance issues, and ongoing geopolitical
tensions in the Middle East and Africa. The core Consumer Price Index (CPI),
which excludes food and energy costs, rose for the months ended July
which was the same as the average.

The Federal Reserve Board (Fed) kept the federal funds target rate at for
the year under review. At period-end, the Fed was still more concerned with
inflation pressures than an economic slowdown. The Treasury note yield
fell from at the beginning of the period to on July

Source: Bureau of Labor Statistics.


| Annual Report



After stock market sell-offs in late February and mid-March, markets rebounded
in the second quarter of amid generally strong first quarter corporate
earnings reports. However, volatility picked up again in July when the markets
fluctuated widely almost daily due to investor concerns about the housing
market, lending environment and mixed second quarter earnings reports. The last
full week in July was the worst in five years for many markets, and stock prices
continued to struggle until period-end. The blue chip stocks of the Dow Jones
Industrial Average posted a total return of the broader
Standard & Poor's Index (S&P returned and the technology-heavy
NASDAQ Composite Index returned Materials, information technology and
telecommunications stocks performed particularly well.

Source: Standard & Poor's Micropal. The Dow Jones Industrial Average is price
weighted based on the average market price of blue chip stocks of companies
that are generally industry leaders. The S&P consists of stocks chosen
for market size, liquidity and industry group representation. Each stock's
weight in the index is proportionate to its market value. The S&P is one of
the most widely used benchmarks of U.S. equity performance. The NASDAQ Composite
Index measures all NASDAQ domestic and international based common type stocks
listed on The NASDAQ Stock Market. The index is market value weighted and
includes more than companies.

THE FOREGOING INFORMATION REFLECTS OUR ANALYSIS, OPINIONS AND PORTFOLIO HOLDINGS
AS OF JULY THE END OF THE REPORTING PERIOD. THE INFORMATION IS NOT A
COMPLETE ANALYSIS OF EVERY ASPECT OF ANY MARKET, COUNTRY, INDUSTRY, SECURITY OR
FUND. STATEMENTS OF FACT ARE FROM SOURCES CONSIDERED RELIABLE.


Annual Report |



Fiduciary Large Capitalization
Growth and Income Fund

YOUR FUND'S GOAL AND MAIN INVESTMENTS: Fiduciary Large Capitalization Growth and
Income Fund seeks long-term growth of principal and income through investing at
least of its net assets in large capitalization companies with market
capitalizations of more than billion, or that are within the top of the
Russell Index, at the time of purchase. The Fund invests mainly in
dividend-paying stocks, while attempting to keep taxable capital gains
distributions relatively low.

- --------------------------------------------------------------------------------
PERFORMANCE DATA REPRESENT PAST PERFORMANCE, WHICH DOES NOT GUARANTEE FUTURE
RESULTS. INVESTMENT RETURN AND PRINCIPAL VALUE WILL FLUCTUATE, AND YOU MAY HAVE
A GAIN OR LOSS WHEN YOU SELL YOUR SHARES. CURRENT PERFORMANCE MAY DIFFER FROM
FIGURES SHOWN.
- --------------------------------------------------------------------------------

We are pleased to bring you Fiduciary Large Capitalization Growth and Income
Fund's annual report for the fiscal year ended July

PERFORMANCE OVERVIEW

Fiduciary Large Capitalization Growth and Income Fund delivered a cumulative
total return of for the months ended July The Fund
outperformed its benchmark, the Standard & Poor's Index (S&P which
returned during the same period.

INVESTMENT STRATEGY

We are research-driven, fundamental investors, pursuing a blend of growth and
value strategies. We use a top-down analysis of macroeconomic trends, market
sectors (with some attention to the sector weightings in the Fund's comparative
index) and industries combined with a bottom-up analysis of individual
securities. In selecting investments for the Fund, we look for companies we
believe are positioned for growth in revenues, earnings or assets, and are
selling at reasonable prices. We employ a thematic approach to identify sectors
that may benefit from longer dynamic growth. Within these sectors, we consider
the

The Russell Index is market capitalization weighted and measures
performance of the largest companies in the Russell Index, which
represent approximately of total market capitalization of the Russell
Index.

Source: Standard & Poor's Micropal. The S&P consists of stocks chosen
for market size, liquidity and industry group representation. Each stock's
weight in the index is proportionate to its market value. The S&P is one of
the most widely used benchmarks of U.S. equity performance. One cannot invest
directly in an index, nor is an index representative of the Fund's portfolio.

THE DOLLAR VALUE, NUMBER OF SHARES OR PRINCIPAL AMOUNT, AND NAMES OF ALL
PORTFOLIO HOLDINGS ARE LISTED IN THE FUND'S STATEMENT OF INVESTMENTS (SOI). THE
SOI BEGINS ON PAGE


| Annual Report



basic financial and operating strength and quality of a company and company
management. The Fund, from time to time, may have significant positions in
particular sectors such as technology or industrials. We also seek to identify
companies that we believe are temporarily out of favor with investors, but have
a good intermediate- or long-term outlook.

MANAGER'S DISCUSSION

During the year under review, the Fund's outperformance relative to its
benchmark, the S&P was mainly due to stock selection. On an individual
stock basis, the top three contributors to relative performance, Juniper
Networks, InBev and Komatsu, enjoyed brisk foreign business largely because of
the strong global economy. Juniper, an Internet infrastructure company, derives
approximately of its revenues from overseas. Belgium-based brewery InBev
does most of its business in Europe and growing Latin American countries.
Japanese construction equipment maker Komatsu benefited from robust demand for
construction equipment, mining machinery and bulldozers worldwide.

The three greatest detractors from relative Fund returns were laboratory and
testing company Quest Diagnostics, office supply chain stores Office Depot and
integrated circuits producer Marvell Technology Group. All three companies'
earnings estimates declined during the period under review and they each
experienced significantly slower earnings growth versus earnings. The
deterioration in fundamentals contributed to market underperformance. We sold
Quest and Marvell but retained Office Depot as of period-end.

On a sector basis, the Fund's relative returns during the period
benefited most from consumer staples sector investments, particularly in foreign
holdings such as InBev and Additionally, our industrials sector
overweighting and stock selection boosted the Fund's relative performance as our
sector investments outperformed the same sector in the S&P Although Quest
Diagnostics and our health care sector stock selection detracted from relative
returns, the Fund's sector underweighting versus the benchmark offset some of
this loss.

PORTFOLIO BREAKDOWN
Fiduciary Large Capitalization
Growth and Income Fund


- --------------------------------------------------------------------------------
% OF TOTAL
SECTOR/INDUSTRY NET ASSETS
- --------------------------------------------------------------------------------
Communications Equipment
- --------------------------------------------------------------------------------
Insurance
- --------------------------------------------------------------------------------
Pharmaceuticals
- --------------------------------------------------------------------------------
Aerospace & Defense
- --------------------------------------------------------------------------------
Machinery
- --------------------------------------------------------------------------------
Oil, Gas & Consumable Fuels
- --------------------------------------------------------------------------------
Energy Equipment & Services
- --------------------------------------------------------------------------------
Specialty Retail
- --------------------------------------------------------------------------------
Media
- --------------------------------------------------------------------------------
Food & Staples Retailing
- --------------------------------------------------------------------------------
Beverages
- --------------------------------------------------------------------------------
Semiconductors & Semiconductor
Equipment
- --------------------------------------------------------------------------------
Industrial Conglomerates
- --------------------------------------------------------------------------------
Food Products
- --------------------------------------------------------------------------------
Diversified Telecommunication Services
- --------------------------------------------------------------------------------
Commercial Banks
- --------------------------------------------------------------------------------
Other
- --------------------------------------------------------------------------------
Short-Term Investments &
Other Net Assets
- --------------------------------------------------------------------------------

The consumer staples sector comprises beverages, food and staples
retailing, food products and tobacco in the SOI.

The industrials sector comprises aerospace and defense, industrial
conglomerates and machinery in the SOI.

The health care sector comprises pharmaceuticals in the SOI.


Annual Report |



Thank you for your continued participation in Fiduciary Large Capitalization
Growth and Income Fund. We look forward to serving your future investment needs.

/s/ S. Mackintosh Pulsifer

S. Mackintosh Pulsifer
Vice President of Franklin Templeton Institutional, LLC (FT Institutional)

/s/ Kenneth J. Siegel

Kenneth J. Siegel
Vice President of FT Institutional
