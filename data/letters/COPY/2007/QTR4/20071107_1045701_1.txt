Dear Shareholder:

Welcome to Monteagle Funds! For those who are new to the fund family, thank you
for becoming part of the family. If you are a long time investor in the fund
family, thank you for allowing us to continue serving your investment needs.

The Monteagle Funds trust has five series of investment offerings for your
investment needs: the Monteagle Fixed Income Fund (MFHRX), the Monteagle Quality
Growth Fund (MFGIX), the Monteagle Large Cap Growth Fund (MEHRX), the Monteagle
Select Value Fund (MVEIX) and the Monteagle Value Fund (MVRGX). Before you make
an investment in one of our funds, I encourage you to visit our website,
www.monteaglefunds.com, and review a prospectus of the fund you are interested
in and review our other public informational filings to determine if the fund is
suitable for your investment needs.

The annual report of the Monteagle Funds for the twelve month period ended
August is presented in the following pages. Please take time to read
the report as it contains important information about Monteagle Funds, including
the investment advisers' reports discussing items such as the performance and
the factors that influenced the performance, the investment approach and the
current investment strategy for each Fund. The audited financial statements with
footnotes and information about the Funds' Trustees and Officers are also
included.

Over the last twelve months, we have continued to see a familiar story in the
U.S. economy. We have seen the Federal Reserve raise short-term interest rates
to stem inflation only to then cut short-term rates to deal with a market
liquidity concern related to the subprime mortgage market crisis. Major equity
indices have reached new all-time highs based on strong corporate earnings and a
strong global economy only to see them decline from those new highs on fears of
a slowing global economy and the subprime mortgage market crisis. Oil prices
have recently reached new highs on concerns of global supply shortage fears due
to the rapidly expanding global economy, while seeing our own domestic economic
growth rate slow down. Also in our country will elect a new President
which will impact the markets positively or negatively depending on the
perspective of each party toward taxes and the effects tax rates have on
corporations and individuals. Our fund managers are aware of these issues and
are focused on managing each fund within their respective styles and strategies
to take advantage of the market opportunities that these market dynamics create.

Monteagle Funds is committed to providing you with a fund family that you can be
proud to own. We strive to help you achieve your investment goals with our fund
offerings. If you have any questions about our Funds or their investment styles,
please give me a call. Also, if you have any investment needs we do not
currently provide a solution for, give me your suggestions. I want to answer
your questions and I want to work to provide you with investment options you can
and will use. Thank you for choosing Monteagle Funds.

Sincerely,
