Dear Shareholder:

We are pleased to present this semiannual report for Dreyfus Premier GNMA Fund, Inc., covering the six-month period from May through October

After a prolonged period of relative price stability, volatility has returned to the U.S. financial markets.The third quarter of provided greater swings in security valuations than weve seen in several years, as
the economic cycle matured and credit concerns spread from the sub-prime mortgage sector to other credit-sensitive areas of the fixed-income markets. In contrast, U.S. Treasury securities rallied strongly as investors became more averse to credit
risks and engaged in a flight to quality.

In our view, these developments signaled a shift to a new phase of the credit cycle in which the price of risk has increased to reflect a more borrower-friendly tone in the credit markets. Although the housing downturn and
sub-prime turmoil may persist for the next few months or quarters, lower short-term interest rates from the Federal Reserve Board should help keep credit available to borrowers and forestall a more severe economic downturn. In addition, turning
points such as this one may be a good time to review your portfolio with your financial advisor, who can help you reposition your fixed-income investments for a changing market environment.

For information about how the fund performed during the reporting period, as well as market perspectives, we have provided a Discussion of Fund Performance given by the funds Portfolio Manager.

Thank you for your continued confidence and support.











DISCUSSION OF FUND PERFORMANCE

For the period of May through October as provided by Robert Bayston, Portfolio Manager

Fund and Market Performance Overview

Like most other parts of the U.S. fixed-income markets, Ginnie Mae securities were affected by a credit crisis originating in the sub-prime mortgage sector. However, because Ginnie Mae pass-through securities are backed by
the full faith and credit of the U.S. government, they held up better than most other bond market The fund produced lower returns than its benchmark, primarily
due to our efforts to capture higher yields through allocations to securities other than Ginnie Mae pass-through securities.

For the six-month period ended October Class Z shares of Dreyfus Premier GNMA Fund achieved a total return of %, Between their inception on May through October Class A, Class B and Class C
shares of the fund, produced total returns of and In comparison, the funds benchmark, the Lehman Brothers GNMA Index (the
Index), achieved a total return of for the full six-month

The Funds Investment Approach

The fund seeks to maximize total return, consisting of capital appreciation and current income. To pursue this goal, the fund invests at least of its assets in Government National Mortgage Association
(GNMAor Ginnie Mae) securities.The remainder may be allocated to other mortgage-related securities, including U.S. government agency securities and privately issued mortgage-backed securities, as well as to asset-backed
securities, U.S.Treasuries and repurchase agreements.

Sub-Prime Contagion Undermined Bond Market Performance

The reporting period was characterized by a moderate slowdown in U.S. economic growth led by a softer housing market. While fixed-income securities historically have tended to do well when the economy slows










DISCUSSION OF FUND PERFORMANCE (continued)

gradually, investor sentiment deteriorated quite rapidly in mid-June when credit concerns spread to other areas of the fixed-income markets from the troubled sub-prime mortgage sector, which encountered a substantially
greater-than-expected number of defaults and delinquencies among sub-prime borrowers. Sharp declines in many of the bond markets more credit-sensitive market sectors were exacerbated by escalating losses among highly leveraged hedge funds and
other institutional investors, many of whom were compelled to sell more liquid and creditworthy securities to meet margin calls and redemption requests.

The liquidity crunch peaked in August with the freezing of the asset-backed commercial paper market.The Federal Reserve Board (the Fed) intervened in August to promote greater market liquidity, but
heightened credit concerns caused a massive flight to higher credit quality, such as U.S.Treasury securities, causing yields of two-year Treasury notes to fall sharply and their prices to rise. In September and October, the Fed took action again,
cutting the overnight federal funds rate at each meeting. While the Feds response to the credit crisis helped restore a degree of investor confidence and some market sectors rebounded, risk premiums remained generally elevated at the end of
the reporting period.

Non-Agency Mortgages Detracted from Relative Performance

Ginnie Mae securities held up relatively well during the credit crunch as investors flocked to the perceived safety of government-backed bonds. Other parts of the mortgage-backed securities market fared less well, including
highly rated non-Agency mortgages.With Ginnie Mae pass-through securities trading at levels we considered richly valued, we had turned to AAA-rated non-Agency residential mortgages, commercial mortgage-backed securities and asset-backed securities
for their higher yields and more attractive valuations. However, as the credit crisis unfolded, the funds positions in these securities detracted from its performance relative to the benchmark, which is comprised solely of Ginnie Mae
securities.

The fund achieved better results from our yield curve strategy, which positioned the fund for wider yield differences along the markets









maturity range.This strategy worked well later in the reporting period, when the Fed reduced the overnight federal funds rate by basis points in two separate moves. However, it was not enough to fully offset
allocation-related weakness.

Finding Opportunities in a Changing Market

The Feds prompt intervention has had an important stabilizing influence on fixed-income markets, but uncertainty has persisted with regard to the impact of elevated energy prices and the housing recession on economic
growth. In addition, it is also worth noting that the price declines experienced by the bond markets earlier this year, especially by mortgage-related securities, can be attributed to the credit-quality concerns of only a small sub-section of that
market, and that credit fundamentals of the mortgage-related market as a whole are still relatively sound.Accordingly, we have retained the funds positions in non-Agency residential and commercial mortgages.With the Fed stating that it is
prepared to move against additional signs of economic weakness, we have favored bonds with premium structures that, in our judgment, are likely to benefit from a reduction in prepayment rates in a lower interest-rate environment. We believe that
these are prudent strategies in todays more turbulent market environment.

November








Neither the market value of Ginnie Maes, nor the funds shares, carry this guarantee.









Total return includes reinvestment of dividends and any capital gains paid, and does not take into
