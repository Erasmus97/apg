Dear Shareholder,
Vanguard STAR Fund produced a 13.1% return for the fiscal year ended October 31, 2007, coincidentally the same return as in the previous fiscal year. The funds return outpaced that of its benchmark and edged past the average return of its composite peer group, an approximation of STARs asset allocation made up of the relevant Lipper fund groups.
STAR is a fund of funds, meaning that it invests in other mutual funds. STAR holds a portfolio that is balanced between a diversified group of domestic and international stocks (60% to 65% of fund assets) and fixed income investments that cover all maturity categories (35% to 40%). During the fiscal year, each of the funds in these asset classes made significant contributions to STARs return on an absolute basis, and nine of STARs 11 underlying funds bested or came close to their respective benchmarks.
If you own the STAR Fund in a taxable account, you may wish to review a report on the funds after-tax returns on page 20.
Stocks rode a bumpy path to impressive results 
Despite some volatility, the U.S. stock market produced strong results during the funds fiscal year. Ongoing problems with low-quality mortgage loans (an unpleasant postscript to the housing downturn) rattled financial markets in the spring and summer, and continued to make investors skittish through the close of the fiscal period. At the end of October, crude oil prices touched historic highs, while the U.S. dollar dipped to record lows versus other major currencies. 
Still, the broad U.S. stock market returned an impressive 15.3%. Large-capitalization stocks outperformed small-caps, and growth stocks outperformed value stocksboth continuing recent months reversals of longer-term trends.
International companies performed even better than domestic issues. Stocks in emerging markets fared particularly well, followed by European and Pacific region stocks (Japan was a notable laggard). The weak U.S. dollar boosted foreign stock returns for U.S.-based investors.
Bond investors converged on high-quality issues 
As troubles in the subprime credit markets rippled across the financial markets, bond investors sought the relative safety of U.S. Treasury bonds. This flight to quality drove prices for Treasuries higher and yields lower, and widened the spread between Treasury yields and the much higher yields demanded by investors for riskier bonds.
Declines in Treasury yields were steepest at the short end of the maturity spectrum, aided by the actions of the Federal Reserve Board. The central bank lowered the target for short-term interest rates to 4.50% in two separate rate cuts (a half-percentage-point in September and a quarter-point on October 31). The yield of the 3-month Treasury bill finished the fiscal period at 3.92%, after spending much of the year near 5%; the 10-year Treasury note ended at 4.47%.
 
2
 
 
Market Barometer
 
 
 
 
Average Annual Total Returns
 
Periods Ended October 31, 2007
 
One Year
Three Years
Five Years
Stocks
 
 
 
15.0%
13.8%
14.5%
9.3   
13.7   
18.7   
15.3   
14.2   
15.3   
33.0   
27.4   
26.4   
 
 
 
 
 
 
 
 
Bonds
 
 
 
5.4%
3.9%
4.4%
Lehman Municipal Bond Index
2.9   
3.7   
4.5   
Citigroup 3-Month Treasury Bill Index
5.0   
4.1   
2.9   
 
 
 
 
 
 
 
 
CPI
 
 
 
Consumer Price Index
3.5%
3.1%
2.9%
 
 
3
 


For the year, the broad taxable bond market returned 5.4%. Returns from tax-exempt bonds were lower, as these issues did not benefit from the late-summer rally in Treasuries.
STARs international funds continued their impressive showing
The returns of each of the 11 actively managed funds in STARs portfolio contributed significantly to its 13.1% return during the fiscal year, allowing STAR to outshine both its benchmark and peer group.
The most impressive returns came from STARs international stock investments, as the long rally in international stock markets continued. For U.S.-based investors, international returns were also helped by the declining U.S. dollar. 
Vanguard International Growth Fund earned a 30.0% return, beating its benchmark by five percentage points primarily because of exceptional performance in emerging markets and good stock picks in Asia. Vanguard International Value Fund, helped along by its holdings in developing Latin American markets, outperformed its benchmark by almost three percentage points to deliver a 27.6% return.
 
Expense Ratios
 
 
Your fund compared with its peer group
 
 
 
Aquired
Composite
 
Fund Fees


Dear Shareholder,
Despite an unsettling summer in stock and bond markets, returns were solid across asset classes for the year ended October 31, 2007. The well-diversified Vanguard LifeStrategy Funds captured these gains, with returns ranging from 8.1% for the most conservative fund in the series (the LifeStrategy Income Fund) to 16.3% for the most aggressive (the LifeStrategy Growth Fund).
The funds enjoyed a performance advantage both over their composite benchmarks and the returns that could have been earned from a composite of equity and fixed income mutual funds allocated in similar proportions. Each funds 25% exposure to Vanguard Asset Allocation Fund helped to sharpen that performance edge. The manager of the Asset Allocation Fund has continued to heavily favor stocks over bonds, which in turn boosted the LifeStrategy Funds stock exposure and performance.
Yields for the two more conservative funds, LifeStrategy Income and LifeStrategy Conservative Growth, were 4.03% and 3.36%, respectively, at the end of the fiscal year. 
Stocks rode a bumpy path to impressive results 
Despite some volatility, the U.S. stock market produced strong results during the funds fiscal year. Ongoing problems with low-quality mortgage loans (an unpleasant postscript to the housing downturn) rattled financial markets in the spring and summer, and continued to make investors skittish through the close of the fiscal period. At the end of October, crude oil prices touched historic highs, while the U.S. dollar dipped to record lows versus other major currencies. 
Still, the broad U.S. stock market returned an impressive 15.3%. Large-capitalization stocks outperformed small-caps, and growth stocks outperformed value stocksboth continuing recent months reversals of longer-term trends.
International companies performed even better than domestic issues. Stocks in emerging markets fared particularly well, followed by European and Pacific region stocks (Japan was a notable laggard). The weak U.S. dollar boosted foreign stock returns for U.S.-based investors.
 
2
 


Bond investors converged on high-quality issues 
As troubles in the subprime credit market rippled across the financial markets, bond investors sought the relative safety of U.S. Treasury bonds. This flight to quality drove prices for Treasuries higher and yields lower, and widened the spread between Treasury yields and the much higher yields demanded by investors for riskier bonds. Declines in Treasury yields were steepest at the short end of the maturity spectrum, aided by the actions of the Federal Reserve Board. The central bank lowered the target for short-term interest rates to 4.50% in two separate rate cuts (a half-percentage-point in September and a quarter-point on October 31). The yield of the 3-month Treasury bill finished the fiscal period at 3.92%, after spending much of the year near 5%; the 10-year Treasury note ended at 4.47%.
 
Market Barometer
 
 
 
 
Average Annual Total Returns
 
Periods Ended October 31, 2007
 
One Year
Three Years
Five Years
Stocks
 
 
 
Russell 1000 Index (Large-caps)
15.0%
13.8%
14.5%
Russell 2000 Index (Small-caps)
9.3   
13.7   
18.7   
Dow Jones Wilshire 5000 Index (Entire market)
15.3   
14.2   
15.3   
MSCI All Country World Index ex USA (International)
33.0   
27.4   
26.4   
 
 
 
 
 
 
 
 
Bonds
 
 
 
Lehman U.S. Aggregate Bond Index (Broad taxable market)
5.4%
3.9%
4.4%
Lehman Municipal Bond Index
2.9   
3.7   
4.5   
Citigroup 3-Month Treasury Bill Index
5.0   
4.1   
2.9   
 
 
 
 
 
 
 
 
CPI
 
 
 
Consumer Price Index
3.5%
3.1%
2.9%
 
 
3
 


For the year, the broad taxable bond market returned 5.4%. Returns from tax-exempt bonds were lower, as these issues did not benefit from the late-summer rally in Treasuries.
Elevated stock exposure benefited all four funds 
The four LifeStrategy portfolios are funds of funds that invest in up to five other Vanguard mutual funds, primarily index portfolios that track the broad U.S. stock or bond markets, or international equity markets. For the 12-month period, the underlying funds performed as follows: 
Total International Stock Index Fund: 32.5% Total Stock Market Index Fund: 14.9% Asset Allocation Fund: 14.1% Short-Term Investment-Grade Fund: 5.4% Total Bond Market Index Fund: 5.3%
 
The return for each LifeStrategy Fund reflected its exposure to each of these asset classes. LifeStrategy Growth had the heaviest weighting in domestic and international stocks, which boosted its return to 16.3%. LifeStrategy Incomes 8.1% gain reflected its more-modest exposure to stocks and the fact that it holds no international stocks, in accordance with its more-conservative mandate. (See your funds respective Fund Profile page for a breakdown of its underlying holdings.)
 
Expense Ratios
 
 
Your fund compared with its peer group
 
 
 
Acquired
Peer-Group
 
Fund Fees


Dear Shareholder,
Like their U.S. counterpart, international stock markets experienced sharp declines during the summer; however, the growth juggernaut in developing countries was not to be stopped. Vanguard Emerging Markets Stock Index Fund returned an eye-popping 69.6% for Investor Shares. Gains in Europe and the Pacific region were more modest, but remained well ahead of the U.S. broad market. 
Results for the Vanguard International Stock Index Funds exceeded those for their respective benchmark indexes and the average returns for their peer groups, with the exception of the Vanguard Developed Markets Index Fund and its institutional counterpart, which is available at a minimum initial investment of $5 million. The funds outperformance relative to the benchmarks was an anomaly driven by short-term discrepancies that arose from fair-value pricing, which is described more fully below. 
If you own one or more of the funds in a taxable account, you may wish to review the details of the funds after-tax returns on page 120. 
Stocks rode a bumpy path to impressive results 
The turmoil that hit stock markets during the summer arose in the arena of U.S. housing finance. Years of extending home loans to U.S. buyers with weak credit came home to roost, ruffling balance sheets across the United States and Europe, where many financial institutions invested heavily in income-oriented products based on these mortgages. Skittish investors also had to contend with soaring crude oil prices, which touched historic highs at the end of the fiscal year, and a U.S. dollar that dipped to record lows versus other major currencies. 
 
4
 


Still, the broad U.S. stock market returned an impressive 15.3%, while gains were significantly larger in Europe and emerging markets, which also benefited from a falling dollar. In the United States, large capitalization stocks outperformed small-caps, and growth stocks outperformed value stocksboth continuing the recent months reversals of longer-term trends. In most markets around the globe, top performances came from stocks in the energy, materials, and industrials sectors, which have benefited from rapid industrialization in emerging market economies.
Bond investors converged on high-quality issues 
As troubles in the subprime credit markets rippled across the financial markets, bond investors sought the relative safety of U.S. Treasury bonds. This flight to quality drove prices for Treasuries higher and yields lower, and widened the spread between Treasury yields and the much higher yields demanded by investors for riskier bonds. Declines in Treasury yields were steepest at the short end of the maturity spectrum, aided by the actions of the Federal Reserve Board. The central bank lowered the target for short-term interest rates to 4.5% in two separate rate cuts (a half-percentage-point in September and a quarter-point on October 31). The yield of the 3-month Treasury bill finished the fiscal period at 3.92% after spending much of the year near 5%; the 10-year Treasury note ended at 4.47%.  
 
Market Barometer
 
 
 
 
Average Annual Total Returns
 
Periods Ended October 31, 2007
 
One Year
Three Years
Five Years
Stocks
 
 
 
33.0%
27.4%
26.4%
15.0   
13.8   
14.5   
9.3   
13.7   
18.7   
15.3   
14.2   
15.3   
 
 
 
 
 
 
 
 
Bonds
 
 
 
Lehman U.S. Aggregate Bond Index (Broad taxable market)
5.4%
3.9%
4.4%
Lehman Municipal Bond Index
2.9   
3.7   
4.5   
Citigroup 3-Month Treasury Bill Index
5.0   
4.1   
2.9   
 
 
 
 
 
 
 
 
CPI
 
 
 
Consumer Price Index
3.5%
3.1%
2.9%
 
 
5
 


For the year, the broad taxable U.S. bond market returned 5.4%. Returns from tax-exempt bonds were lower, as these issues did not benefit from the late-summer rally in Treasuries.
 
 
 
 
Your fund compared with its peer group
 
 
 
 
Fund
Acquired
Peer-Group
 
Expense
Fund Fees and


