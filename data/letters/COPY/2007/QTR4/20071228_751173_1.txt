Dear
Shareholders: 
Both equity and bond markets experienced increasing volatility over the course of the past year. What began in
November 2006 as a relatively uneventful fiscal year for the Series evolved into a volatile environment led by the downturn of the U.S. housing market. The once contained deflating of the housing bubble spread across the broader Financial Services
sector and created tighter credit markets. Ultimately, the U.S. Federal Reserve (the Fed) cut interest rates first by fifty basis points (0.50%) followed most recently by an additional 25 basis points (0.25%) in an attempt to contain the
contraction in credit markets from spilling over into the overall economy. These cuts represent the first interest rate reductions by the Fed since 2003. 
Although the overall performance of the equity portions of the Series portfolios has been driven more by selective individual stock opportunities than broad trends, the Series have benefited from holdings in the
Information Technology (IT) and Energy sectors and an underweight position in Financials. 
Within the Information Technology
sector, IT security and communications infrastructure companies performed well. The sector in general derived gains from a renewed interest in computer hardware and an increased demand for technological innovation. The sector generated solid gains
for the Series both as a result of these industry trends and company specific factors. 
The Series also benefited from their stock
positions in the Energy sector. Oil prices continually reached new highs during the year, while the U.S. Dollar continued to weaken against most major currencies. Industry-wide, the rise in oil prices, strong demand and diminishing supply
resulted in increasing drilling activity. Industry oil and gas suppliers were well poised to take advantage of the increasing capital expenditures needed to increase supply. We pared back holdings in this sector and harvested gains during the year
as positions appreciated to what our analysis showed to be their fair value. 
Given the turbulence within the Financials sector, the Series
benefited from their underweight position in this area. Subprime defaults and contracting credit markets hindered the performance of Financials stocks sector-wide, although the Series exposure to subprime mortgages has been minimal.

As a result of recent economic conditions such as record oil prices and increased housing foreclosures, downward pressure has been placed
on consumer spending. Specifically, the performance of the cable stocks negatively impacted the Series. Recent company-specific factors such as product safety concerns have had a negative impact on some of the Series Health Care holdings.
This, when combined with the Series relative overweight in the sector, had a negative impact on the Series performance for the year. 
st
The volatile environment of this past year was not isolated to equity markets; fixed income instruments were also impacted. In
general, bond yields rose earlier in the year, as investors reacted to positive economic data and a reduced probability of near-term Fed rate cuts. By the end of the year, as the Fed cut rates, bond yields fell as a result of a flight to quality
sparked by the volatile equity markets. Within the Series fixed income allocations, we focused mainly on duration-based investments through high quality U.S. Treasuries, with modest positions in corporate bonds and mortgage-backed U.S.
Government agency assets. 
(unaudited)


Dear
Shareholders: 
®
Recent performance has been driven primarily by our stock selections in the Information Technology
(IT) and Energy sectors. On a relative basis versus the benchmark, an underweight to the Financials sector also helped performance, due to the weakness of the broader Financials sector in the third quarter of 2007. Areas that detracted
from performance included security selection in the Consumer Discretionary, Industrials, and Materials sectors. 
Holdings in the
Information Technology sector, more specifically companies that provide IT security and communications infrastructure, did particularly well during the later part of 2006 and first half of 2007. Stock selection and an overweight relative to the
benchmark added to performance on an absolute and relative basis. 
The Energy sector also was a source of strong performance over the
course of the year, as a result of the Series holdings in later-cycle energy services companies. Stock selection and an overweight to the sector in the first half of 2007 added to performance during the year. We were able to take advantage of
strong price appreciation in holdings in this sector through the second and third quarter of 2007. We reduced holdings in the Energy sector late in the year as valuations increased along with the price of oil. 
An underweight to Financials compared to the benchmark helped relative performance during the year mainly as a result of weakness in the sector brought
on by the deterioration of the real estate market and the subprime mortgage market. Due to a concern over real estate valuations and residential lending practices, we favored financial service companies with businesses that derive revenue from
fee-based services. 
Weakness in cable providers in the Consumer Discretionary sector detracted from performance overall. Two other
sectors, Industrials and Materials, underperformed the benchmark on a relative basis due to stock selection and slight underweights to both areas. In the Industrials sector, exposure to airline stocks was the main detractor. Starting in April, we
selectively added holdings in the Materials sector related to the housing industry, which also detracted from performance. 
The Series
continues to have comparatively large positions in the Health Care and Information Technology areas, even after reducing Information Technology holdings as a result of strong performance over the last two quarters. The two significant underweights
for the Series are in the Financials and Energy sectors. We selectively added to the Financials sector towards the end of the year, and reduced our position in the Energy sector, moving from an overweight relative to the index to an underweight
throughout the course of the year. 
We appreciate your business and wish you all the best in the coming year. 


Dear
Shareholders: 
®
Recent performance has been driven primarily by our stock selections in the Information Technology
(IT) and Energy sectors. On a relative basis versus the benchmark, an underweight to the Financials sector also helped performance, due to the weakness of the broader Financials sector in the third quarter of 2007. Areas that detracted
from performance included security selection in the Consumer Discretionary, Industrials, and Materials sectors. 
Holdings in the
Information Technology sector, more specifically companies that provide IT security and communications infrastructure, did particularly well during the later part of 2006 and first half of 2007. Stock selection and an overweight relative to the
benchmark added to performance on an absolute and relative basis. 
The Energy sector also was a source of strong performance over the
course of the year, as a result of the Series holdings in later-cycle energy services companies. Stock selection and an overweight to the sector in the first half of 2007 added to performance during the year. We were able to take advantage of
strong price appreciation in holdings in this sector through the second and third quarter of 2007. We reduced holdings in the Energy sector late in the year as valuations increased along with the price of oil. 
An underweight to Financials compared to the benchmark helped relative performance during the year mainly as a result of weakness in the sector brought
on by the deterioration of the real estate market and the subprime mortgage market. Due to a concern over real estate valuations and residential lending practices, we favored financial service companies with businesses that derive revenue from
fee-based services. 
Weakness in cable providers in the Consumer Discretionary sector detracted from performance overall. Two other
sectors, Industrials and Materials, underperformed the benchmark on a relative basis due to stock selection and slight underweights to both areas. In the Industrials sector, exposure to airline stocks was the main detractor. Starting in April, we
selectively added holdings in the Materials sector related to the housing industry, which also detracted from performance. 
The Series
continues to have comparatively large positions in the Health Care and Information Technology areas, even after reducing Information Technology holdings as a result of strong performance over the last two quarters. The two significant underweights
for the Series are in the Financials and Energy sectors. We selectively added to the Financials sector towards the end of the year, and reduced our position in the Energy sector, moving from an overweight relative to the index to an underweight
throughout the course of the year. 
We appreciate your business and wish you all the best in the coming year. 


Dear
Shareholders: 
The Overseas Series produced strong absolute returns while slightly trailing the return of the Morgan Stanley
Capital International (MSCI) All Country World Index ex U.S. for the 12-month period ended October 31, 2007. Another year of a weakening dollar versus the Euro and British Pound helped international stock markets to positive gains. Since the
current international stock market cycle (which includes both rising and falling markets) began on January 1, 2000, the Overseas Series has substantially outperformed the benchmark. 
Our team of analysts uses time-tested strategies to choose stocks for the portfolio. These strategies include the Profile Strategy, Hurdle Rate Strategy, and the Bankable Deal Strategy. Our
investment process also follows strict pricing disciplines intended to avoid buying stocks that are not attractively valued. 
The Series
currently has an overweight position versus the benchmark in the Health Care sector. A majority of the stocks in this sector were bought under our Profile Strategy. In this strategy our analysts seek to identify companies that we believe can grow
their earnings faster than global economic growth. Typically this is accomplished by some sustainable competitive advantage that keeps competitors from entering their markets. Our holdings in the Health Care sector are spread among large
pharmaceutical companies, specialty health care companies, diagnostic companies, and firms that supply products to the drug discovery market. While our holdings had positive absolute returns, the overweight position detracted from relative returns
versus the benchmark as the Health Care sector lagged the overall market. 
The Series has maintained a larger position than the benchmark in
the Information Technology sector. Interestingly, many of these companies in the Information Technology sector we have owned under the Hurdle Rate Strategy. In this strategy we seek to identify industries in a downturn where profits are temporarily
depressed and future expectations are low. When capacity is being taken out of the industry and the supply/demand situation is more in balance we want to own those companies that are in the strongest competitive position to increase profits and
market share. We currently own manufacturers of flat panel displays in this strategy; these stocks aided both absolute and relative returns over the year. 
The Series is currently underweight in the Financials sector, holding roughly half the position of the benchmark. We have largely shied away from companies with direct exposure to the U.S. subprime mortgage market and
positioned the portfolio in companies that earn most of their revenues from fee-based business. These include many large diversified banks in developed European countries. We also hold an insurance brokerage firm and a German bank that is focused in
the structured property finance property management business. We hold a few stocks in the Bankable Deal Strategy within the Financials sector. This strategy involves buying companies that are trading at a substantial discount to their intrinsic
value. Our analysts look at a variety of valuation techniques in this strategy to identify companies including sum-of-the-parts, transaction-based, or break-up value. While the performance of the Series compared to the benchmark was helped by its
relatively small position in Financials, stock selection was a detractor to relative returns. 
The Series continues to have a smaller
position in Japan than the benchmark, and this benefited performance on both an absolute and relative basis. Japan was one of the worst performing countries in the developed world. Emerging markets was the best performing region over the year and
the Series was slightly underweight, which impacted returns compared to the benchmark. The Series is maintaining a large allocation to developed European countries as our analysis has concluded that valuations in certain emerging markets look
stretched. 
We appreciate your business and wish you all the best in the coming year. 


