Dear Shareholder, 
i
Late in the reporting period, the U.S. fixed-income markets were
extremely volatile, which negatively impacted market liquidity conditions. Initially, the concern on the part of market participants was limited to the subprime segment of the mortgage-backed market. These concerns broadened, however, to include a
wider range of financial institutions and markets. As a result, domestic and international equity markets experienced heightened volatility at the end of the fiscal year. 
ii
 
 
 
growth more generally. Todays action is intended to help forestall some of the adverse effects on the broader economy that might otherwise arise from
the disruptions in financial markets and to promote moderate growth over time. 
iv
v
Please read on for a more detailed look at prevailing economic and market conditions during the Funds fiscal year and to learn how those conditions
have affected Fund performance. 
Information About Your Fund 
As you may be aware, several issues in the mutual fund industry have come under the scrutiny of federal and state regulators. Affiliates of the Funds manager have, in recent years, received requests for
information from various government regulators regarding market timing, late trading, fees, and other mutual fund issues in connection with various investigations. The regulators appear to be examining, 
 
II         
 
 
among other things, the Funds response to market timing and shareholder exchange activity, including compliance with prospectus disclosure related to
these subjects. The Fund is not in a position to predict the outcome of these requests and investigations. 
Important information with
regard to recent regulatory developments that may affect the Fund is contained in the Notes to Financial Statements included in this report. 
As always, thank you for your confidence in our stewardship of your assets. We look forward to helping you meet your financial goals. 
Sincerely,



