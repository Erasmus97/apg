Dear Shareholder:

We are pleased to present this annual report for Dreyfus Premier Enterprise Fund, covering the period from October through September

An intensifying credit crisis over the summer of produced heightened turbulence in U.S. financial markets. As credit concerns spread from the sub-prime mortgage sector of the bond market to other areas of the financial
markets, investors appetite for risk shifted from ample to cautious.This change led to sharp stock market declines in July and early August. However, rate cuts from the Federal Reserve Board (the Fed) in August and September seemed
to restore investor confidence, and some equity market indices ended the reporting period near their all-time highs.

It appears that the downside risks to the U.S. economy have increased, with the housing recession and consumer slowdown likely to be more intense than previously expected. Plus, a weakening U.S. dollar and rising
commodities and energy prices have given the Fed the daunting task of balancing both inflationary concerns and the risk of an economic slowdown simultaneously. In our view, we believe that the U.S. economy is still quite resilient and the Feds
recent actions are likely to continue to produce opportunities for some market sectors and additional challenges for others. Since each investors situation is unique, we encourage you to talk about these investment matters with your financial
advisor, who can help you make the right adjustments for your portfolio.

For information about how the fund performed during the reporting period, as well as market perspectives, we have provided a Discussion of Fund Performance given by the funds Portfolio Manager.

Thank you for your continued confidence and support.









DISCUSSION OF FUND PERFORMANCE

For the period of October through September as provided by James D. Padgett, CFA, Portfolio Manager

Fund and Market Performance Overview

Although stocks generally advanced during the reporting period, market leadership shifted from small-cap companies to larger ones as the U.S. economy slowed, a credit crisis spread from sub-prime mortgages to other
financial markets, and investors grew less tolerant of risk. Micro-cap stocks fared particularly poorly in this changing environ-ment.As a result, the fund produced lower returns than its benchmark, which has a higher average market cap than the
fund at approximately billion vs. million for the fund. In addition, the funds results were constrained by disappointing security selections in the consumer discretionary, health care and technology sectors.

For the period ended September Dreyfus Premier Enterprise Fund produced total returns of for Class A shares, for Class B shares, for Class C shares and for ClassT
In comparison, the funds benchmark, the Russell Growth Index (the Index), produced a total return of .


The Funds Investment Approach

The fund seeks capital appreciation. To pursue this goal, the fund normally invests at least of its assets in the stocks of micro-cap companies, which typically are small (under million market cap) and relatively
unknown.The fund may also invest in larger companies if we believe they represent better prospects for capital appreciation. Although the fund normally will invest in common stocks of U.S.-based companies, it may invest up to of its total assets
in foreign securities. The funds investments may include common stocks, preferred stocks and convertible securities, including those purchased in initial public offerings. The fund may also invest in securities issued by exchange-traded
investment companies, which are designed to track a specific equity index.

We look for companies with fundamental strengths that indicate the potential for strong profit growth.We focus on individual stock selection, searching one by one for companies with one or more of the following
characteristics: strong management teams; competitive industry positions; focused business plans;compelling product or market opportunities;strong

The Fund







DISCUSSION OF FUND PERFORMANCE (continued)

business prospects; and the ability to benefit from changes in technology, regulations or industry sector trends.

A Maturing Economy and Sub-Prime Mortgage Woes Weighed on Micro-Cap Stocks

Macroeconomic factors produced headwinds that caused micro-cap stocks to lag other capitalization ranges by a substantial margin during the reporting period. As a point of reference, during the first three calendar quarters
of the Russell Microcap Growth Index advanced only far below the return of the Russell Growth Index over the same time period. In addition, during this period, the growth style of investing generally outperformed the
growth at a reasonable price approach that we employ.These two trends were influenced by the maturation of the economic cycle and expectations around U.S. monetary policy. After several years of expansion, U.S. economic growth began to
moderate early in the reporting period, largely triggered by weakness in the housing sector.Yet, because of persistent inflationary pressures, the Federal Reserve Board (the Fed) during the first half of refrained from reducing
short-term interest rates, which had climbed from to between and

The combination of lower housing prices and higher short-term interest rates put pressure on homeowners, an unexpectedly large number of whom defaulted on their sub-prime mortgages.Turmoil in the sub-prime mortgage sector
spread to other areas of the financial markets in mid-June, causing investors to reassess their attitudes toward risk and turn away from smaller, more speculative companies. In an attempt to restore market stability, the Fed reduced key short-term
interest rates in August and September of subsequently rallied,but the rebound was more pronounced among large-cap stocks than smaller ones.

Our Stock Selection Strategy Produced Mixed Results

The funds performance also was influenced by some disappointing stock selections, which were particularly negative within the funds consumer discretionary holdings. Directed Electronics, which makes speakers for
home theater systems and other audio equipment, was hurt by sales shortfalls and concerns regarding consolidation among customers in the satellite radio industry.Heelys,the makers of skate shoes,struggled with higher-than-expected
retail inventories.The position was sold during the reporting period. In the health care sector, a new drug from Penwest Pharmaceuticals failed to meet early sales and prescription expectations.







Among technology companies, Liquidity Services, which helps companies liquidate unsold goods through online auctions, realized some minor setbacks in ramping relationships with commercial customers. Fund performance was
also hurt by Flanders Corp., which makes air filtration products, as the company continued to be hampered by poor management execution.

These disappointments were offset to a degree by better results in other market sectors.The consumer staples sector was a bright spot during the reporting period, with above-average returns from a number of holdings. The
funds holdings in the industrial sector also fared relatively well.In this sector, private prison operator GEO Group benefited from favorable supply-and-demand dynamics; meanwhile, floor cleaning equipment maker Tennant Co. achieved
greater-than-expected manufacturing efficiencies. In the energy sector, oil drilling equipment provider Energy Services boosted its earnings prospects by expanding into new markets. In other areas, restaurant chain Buffalo Wild Wings bolstered
same-store sales with an effective advertising campaign, while digital audio pioneer DTS Inc. gained value after announcing plans to shed a money-losing operation.

Positioned for a New Phase of the Economic Cycle

We remain confident in the ability of our longstanding investment approach to identify attractive investments among micro-cap companies. In fact, recent market weakness may provide new opportunities to purchase, at more
attractive prices, the stocks of small companies that we believe are poised for long-term growth.

October








Total return includes reinvestment of dividends and any capital gains paid, and does not take into

Dear Shareholder:

We are pleased to present this annual report for Dreyfus Premier Natural Resources Fund, covering the period from October through September

An intensifying credit crisis over the summer of produced heightened turbulence in U.S. financial markets. As credit concerns spread from the sub-prime mortgage sector of the bond market to other areas of the financial
markets, investors appetite for risk shifted from ample to cautious.This change led to sharp stock market declines in July and early August. However, rate cuts from the Federal Reserve Board (the Fed) in August and September seemed
to restore investor confidence, and some equity market indices ended the reporting period near their all-time highs.

It appears that the downside risks to the U.S. economy have increased, with the housing recession and consumer slowdown likely to be more intense than previously expected. Plus, a weakening U.S. dollar and rising
commodities and energy prices have given the Fed the daunting task of balancing both inflationary concerns and the risk of an economic slowdown simultaneously. In our view, we believe that the U.S. economy is still quite resilient and the Feds
recent actions are likely to continue to produce opportunities for some market sectors and additional challenges for others. Since each investors situation is unique, we encourage you to talk about these investment matters with your financial
advisor, who can help you make the right adjustments for your portfolio.

For information about how the fund performed during the reporting period, as well as market perspectives, we have provided a Discussion of Fund Performance given by the funds Portfolio Managers.

Thank you for your continued confidence and support.









DISCUSSION OF FUND PERFORMANCE

For the reporting period of October through September as provided by Alexander S. Marshall and William E. Costello, Portfolio Managers

Fund and Market Performance Overview

Stocks were driven higher by continued global economic growth.While most market sectors gained value, the energy and materials areas led the markets advance. Rising oil prices and strong industrial demand for energy
and raw materials created a profitable environment for companies with exposure to natural resource production. As a result, the Goldman Sachs Natural Resource Index produced significantly greater returns than the funds more broadly based
benchmark.The fund outperformed both indices, due primarily to effective individual stock selections and an emphasis on equipment and independent oil exploration and production companies.

For the period ended September the Dreyfus Premier Natural Resources Fund produced total returns of for Class A shares, for Class B shares, for Class C shares, for Class I
shares and for Class T This compares with the funds benchmark, the Standard &amp; Poors Composite Stock Price Index (S&amp;P
Index), which produced a total return of for the same Goldman Sachs Natural Resource Index, which more closely reflects the funds
composition, returned for the

The Funds Investment Approach

The fund seeks long-term capital appreciation, normally investing at least of its assets in stocks of companies in natural resources and natural-resource-related sectors. While the fund typically invests in U.S.-based
companies, it may invest up to of its total assets in foreign securities, including emerging market securities. The fund invests in growth and value stocks, typically maintaining exposure to the major natural resources sectors. Using fundamental
research and direct management contact, we seek stocks of companies with strong positions in their sector, sustained achievement records and strong financial conditions.We also look for special situations, such as corporate restructurings,
turnarounds or management changes that could increase the stock price.

The Fund







DISCUSSION OF FUND PERFORMANCE (continued)

High Petroleum Prices Supported Energy Stocks

In contrast to the slowdown in the United States, the global economy continued to grow robustly over the reporting period.The economic expansion was driven, in part, by the ongoing growth of former third-world nations that
increasingly have become the worlds low-cost providers of labor and manufacturing. As countries such as China and India build out their industrial infrastructures, demand has remained high for a relatively limited supply of the worlds
energy and construction commodities.

After briefly bottoming near a barrel in early crude oil prices rebounded sharply, ending the reporting period at over a barrel. Because oil-related holdings comprised roughly of the funds
investments, the fund was well positioned to benefit from these conditions.

The fund further enhanced performance within the energy sector by emphasizing equipment and independent exploration and production companies, which tend to be more sensitive to rising commodity prices than major integrated
energy companies. Leading equipment holdings included oil and gas production equipment providers Cameron International and FMC Technologies, drilling rig equipment maker National Oilwell Varco and offshore engineering specialist McDermott
International. In the alternative energy sub-sector, Danish wind power equipment maker Vestas Wind Systems also contributed to the funds strong returns.

Several independent oil and gas companies helped bolster the funds relative performance, including Southwestern Energy, Noble Energy and Range Resources. One of the funds smaller-cap names, Arena Resources,
produced impressive gains on the strength of its relatively high concentration of oil-related activities. A few of the funds investments in the independent oil and gas area, such as Parallel Petroleum, underperformed. However, such laggards
proved to be mild compared to gains from other holdings.

While virtually every other industry group in which the fund invested contributed positively to performance, some proved more profitable than others. On the positive side, the fund took advantage of seasonal trading
patterns affecting oil refiners, such as Tesoro. Favorable timing in the purchases and sales of refinery stocks generated stronger returns than a simple buy-and-hold strategy would have produced. On the other hand,







the funds underweighted exposure compared to the Goldman Sachs Natural Resource Index to the diversified metals and mining sector proved less favorable. In particular, lack of exposure to aluminum producers, which
benefited from merger activity, and copper producers, which saw higher commodity prices, detracted from the funds relative performance.

High Commodity Prices Underlie a Bright Outlook

In light of continued U.S. and global economic growth and the weakening of the U.S. dollar, we believe petroleum prices are likely to remain at profitable levels for independent exploration and production
companies.Accordingly, as of the end of the reporting period, we have continued to emphasize companies in this area, particularly those with strong exposure to oil-related activities.While we also have maintained overweighted positions in equipment
companies, we have trimmed some of this exposure due to high current valuations. Finally, we are currently deemphasizing refiners based on seasonal trends, and major integrated energy companies appear limited in their growth potential due to their
size.

October








Total return includes reinvestment of dividends and any capital gains paid, and does not take into
