Dear Shareholder:

We are pleased to present this semiannual report for Dreyfus Growth Opportunity Fund, Inc., covering the six-month period from March through August

An intensifying credit crisis later in the reporting period produced heightened volatility in most financial markets. As credit concerns spread from the sub-prime mortgage sector of the bond market to other areas of the
financial markets, investors appetite for risk shifted from ample to cautious.While this change led to declines across most capitalization ranges of the U.S. stock market, gains achieved earlier in the reporting period still enabled most
equity indices to post positive nominal results for the reporting period overall.

In our analysis, the downside risks to the U.S. economy have increased, with the housing recession and consumer slowdown likely to be more intense than previously expected. Many investors were encouraged by the Federal
Reserve Boards decision in August to lower the discount rate and even more so by its decision to lower the federal funds rate basis points at its September meeting.We believe that these monetary policy changes will help to stabilize
short-term market volatility and allow for the current economic cycle to moderate gradually.With that said, we also feel that these factors are likely to continue to produce opportunities for some market sectors and challenges for others, which is
why we encourage you to talk with your financial advisor to help ensure that your portfolio remains aligned with your current financial needs and future investment goals.

For information about how the fund performed during the reporting period, as well as market perspectives, we have provided a Discussion of Fund Performance given by the funds Portfolio Managers.

Thank you for your continued confidence and support.











DISCUSSION OF FUND PERFORMANCE

For the period of March through August as provided by Elizabeth Slover and Martin Stephan, Portfolio Managers

Fund and Market Performance Overview

After generally rallying in the spring, the U.S. stock market later encountered heightened volatility stemming from intensifying credit and liquidity concerns.The fund fared relatively well in this changing market
environment, as our security selection strategy produced higher returns than the benchmark in eight of economic sectors.

For the six-month period ended August Dreyfus Growth Opportunity Fund produced a total return of In comparison, the funds
benchmark, the Russell Growth Index (the Index), produced a total return of during the same

The Funds Investment Approach

The fund seeks long-term capital growth consistent with the preservation of capital. Current income is a secondary goal.To pursue these goals, the fund invests at least of its assets in stocks of U.S. companies. We
employ both bottom-up and top-down approaches to create a portfolio of growth stocks. Our top-down analysis of economic, market or industry trends may lead to overweighted or underweighted positions in certain market sectors.
We then use bottom-up fundamental analysis within those sectors to identify companies with sound financial health, growing earnings or cash flows, strong competitive positions and the presence of a catalyst that can trigger an increase in the
companys stock price.

Mortgage Market Turmoil Dampens Equities Performance

Over the first half of the reporting period, moderate economic growth, robust corporate earnings and stable short-term interest rates supported higher stock prices, driving some market indices to record highs. However, by
the reporting periods end, turmoil in the sub-prime mortgage sector had spread to other parts of the financial markets.

The Fund







DISCUSSION OF FUND PERFORMANCE (continued)

Resulting credit, liquidity and economic concerns weighed heavily on equity markets over the summer, causing many stocks to give back a portion of their previous gains.

Our security selection strategy enabled the fund to participate in the early rally and hold up relatively well in the subsequent downturn. In the health care area, the fund benefited from stock selection as several solid
purchases were made during times of temporary weakness. For example, we acquired shares of Amylin Pharmaceuticals during a downturn in the companys prescription trends for its lead diabetes drug, Byetta. Subsequent stock performance was strong
as demand for Byetta improved in the wake of controversy surrounding competitors diabetes drugs, and optimism increasing on Amylins follow-on drug called Byetta LAR.Another good performer was Schering-Plough, which posted two solid
quarterly earnings results, driven by strong product sales.

The energy sector bolstered the funds performance as several oil services firms produced positive contributions. National Oilwell Varco gained market share strength and achieved revenue growth due to increasing orders
for drilling equipment. Cameron International also climbed on heightened demand for its high-end pump and valve products for use in deep-sea wells. Diamond Offshore Drilling, a second-tier operator of offshore drilling rigs, benefited from high
demand trickling down from reduced top-tier capacity. Finally, oil refiners Valero Energy and Marathon Oil both fared well as a result of positive pricing trends, while Marathon also benefited from the efficient integration of its exploration,
refining and sales operations.

On the other hand, the fund received disappointing results in the information technology segment. Data storage company Network Appliance fell when the company missed earnings forecasts. Internet portal Yahoo! performed
poorly amid intensifying management, market share and strategy challenges. The fund also did not invest in International Business Machines and Intel, strong performers for the Index, as we believed greater growth opportunities could be found among
smaller companies in the personal-computer supply chain.









Although we maintained light exposure to underperforming financial industries, such as real-estate investment trusts and banks, our security selection strategy in the financial sector nonetheless hampered relative
performance. MGIC Investment Corp. slid due to a deteriorating mortgage market and an unsuccessful merger.While brokerage firms such as Goldman Sachs Group were strong performers over the first three months of the reporting period, their asset
management and investment banking activities were hurt by the credit crisis over the summer. Automobile financing company AmeriCredit also declined due to deteriorating investor sentiment in the weakened credit environment.

Finding Opportunities in a More Selective Marketplace

We have continued to find what we believe to be attractive growth opportunities in the energy sector, particularly in the oil services arena, as well as in most areas of the health care sector.We have favored semiconductor
manufacturers in the information technology sector, as well as software firms and other companies that are part of the personal computer supply chain. On the other hand, we expect the financials sector to continue to be adversely affected by
delinquencies and defaults in the mortgage market, and we have continued to pare back the funds exposure to businesses that, in our judgment, may be negatively affected by these issues.

September








Total return includes reinvestment of dividends and any capital gains paid. Past performance is no
