Dear Shareowner,
- --------------------------------------------------------------------------------
Staying diversified and keeping your portfolio invested in the markets are two
general investment principles that have served investors well over time. They
were particularly useful guides during the past year, when an otherwise healthy
long-term bull market was buffeted by problems in the mortgage and banking
industries.

Since mid-year, issues tied to poor underwriting practices in the subprime
sector of the mortgage industry and to problems with risk management by banks
and hedge funds have resulted in increased market volatility and rising concern
about risks to U.S. economic growth.

Even against this backdrop, the long-term performance of major asset classes
remained positive. In the months ending September equity investors
were generally rewarded as, despite several interim corrections, the Standard &
Poor's Index returned the Dow Jones Industrial Average returned
and the NASDAQ Composite Index returned International developed and
emerging markets equities performed even better, reflecting both a weakening
dollar (which boosts returns for U.S. dollar-based investors) and strong local
currency returns, with the MSCI EAFE Developed Market Index returning and
the MSCI Emerging Markets Index returning over the same period. The
U.S. bond market, as measured by the Lehman Aggregate Bond Index, returned in
the months ending September The U.S. high-yield bond market, as
measured by the Merrill Lynch High Yield Bond Master II Index, returned for
the same period, reflecting its higher coupon yield.

U.S. economic growth has slowed in the past year, but this was due as much to
the natural maturation of the cyclical expansion, as U.S. factories approached
full utilization and the labor market approached full employment, as it was to
rising commodity prices or short-term interest rates. This slowdown was,
therefore, not entirely unwelcome, as it reduced the threat of higher inflation.
More recently, however, there has been increasing concern that falling home
prices, and/or disruptions in financial markets pose a larger threat to
continuing economic growth. A weaker U.S. dollar has put upward pressure on some
prices, but it has also benefited exporters and companies competing in the
global marketplace.

Although U.S. economic growth has slowed, growth in the rest of the world
remains relatively strong. In Europe, robust GDP growth driven by a positive
operating environment for European companies, especially exporters, has driven
unemployment lower and supported growing consumption, although concerns about
the impact of the strong Euro are a persistent source of worry. Japanese
economic growth continues, albeit at a muted rate, and the country's
deflationary problems are gradually receding. Economic growth in





Letter

emerging market countries remains faster than in the developed world as they
persist in "catching up," led by China, which continues its rise as a world
economic power.

The U.S. Federal Reserve and the world's other central banks have pursued
policies aimed at producing low and stable inflation, believed to be the best
backdrop for steady economic growth and low average unemployment over the long
term. Keeping inflation low is also an important support for stock and bond
valuations, and so central bank policies have also been "investor friendly."

Looking forward, the economic outlook appears generally positive, although real
estate prices, subprime mortgage defaults, and the possibility of a systemic
liquidity/credit crunch all represent sources of risk. Central banks responded
to this summer's liquidity squeeze in the inter-bank and commercial paper
markets by acting as "lenders of last resort" and, in September, the Fed cut
rates to limit the risk of credit market problems spreading to the broader
economy. While falling risk tolerances and reducing leverage may depress asset
prices in the short term, valuations look reasonable if "Wall Street" problems
do not spread to the broader "Main Street" economy.

Sudden swings in the markets are always to be expected. Just as staying
diversified and invested are important investment principles, it is also
important to pay attention to asset allocation. As always, we encourage you to
work closely with your financial advisor to find the mix of stocks, bonds and
money market assets that is aligned to your particular risk tolerance and
investment objective.


Respectfully,
