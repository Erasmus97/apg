Dear Shareholder,
Vanguard Strategic Equity Fund returned 13.8% for the fiscal year ended September 30, 2007. Although a solid result in its own right, the funds return trailed the return of its benchmark and the average return of its competitive peers.
Broadly underlying the shortfall was a growing shift in investor preference during the year toward growth stocks and away from the more reasonably valued stocks favored by the funds advisor, Vanguard Quantitative Equity Group. This shift accelerated during the quarter ended September 30 as a result of shorter-term, worldwide concerns about the impact of the subprime mortgage market crises. This period of turbulence coincided with a sharp uptick in the prices of large-capitalization, growth-oriented stocks. The fund faced some headwinds, despite the balanced nature of its investment approach that gives it simultaneously, an exposure to attractively valued stocks and also stocks that have higher earnings growth. The valuation component hurt more than the growth exposure helped.
In other developments during the fiscal year, the fund was reopened to investors on November 9, 2006, and the minimum investment was raised to $10,000. The fund had been closed the previous April to moderate cash flow and to protect the interests of its shareholders. 
 
 
 
 
 
 
 
 
 
2


 
Please note: Our preliminary estimates suggest that the fund will distribute capital gains of roughly $2.15 per share in December 2007. Also, if you hold the Strategic Equity Fund in a taxable account, you may wish to review our report on the funds after-tax returns on page 27. 
Strong returns for U.S. stocks; even better for markets abroad
U.S. stocks produced excellent returns for the fiscal year. The gains came despite a midsummer shakeup brought on by problems in the subprime mortgage-loan market. Financials stockswhich represent a sizable share of the U.S. markets valuewere hardest hit, as investment banking and consumer lending businesses throttled back.
 
The broad U.S. equity market returned 17.1% for the year. Returns from large-capitalization stocks outpaced those of small-caps, and growth-oriented stocks outperformed their value-oriented counterparts. As investors took account of risk, they seemed to exhibit a preference for large-cap growth stocks, which seem better positioned to thrive in a period of economic uncertainty.
Although not immune from the effects of the turmoil in U.S. credit markets, international stocks handily surpassed the returns of domestic stocks over the 12 months. The dollars ongoing weakness further enhanced foreign market gains for U.S.-based investors.
 
Market Barometer
 
 
 
 
Average Annual Total Returns
 
Periods Ended September 30, 2007
 
One Year
Three Years
Five Years
Stocks
 
 
 
Russell 1000 Index (Large-caps)
16.9%
13.8%
16.0%
Russell 2000 Index (Small-caps)
12.3
13.4
18.8
Dow Jones Wilshire 5000 Index (Entire market)
17.1
14.0
16.5
MSCI All Country World Index ex USA (International)
31.1
26.5
26.3
 
 
 
 
 
 
 
 
Bonds
 
 
 
Lehman U.S. Aggregate Bond Index (Broad taxable market)
5.1%
3.9%
4.1%
Lehman Municipal Bond Index
3.1
3.9
4.0
Citigroup 3-Month Treasury Bill Index
5.0
4.0
2.8
 
 
 
 
 
 
 
 
CPI
 
 
 
Consumer Price Index
2.8%
3.2%
2.9%
 
 
 
 
 
 
 
 
3


 
The bond market was shaken, but regained ground in the end
Turmoil in the corporate bond and subprime lending markets caused a flight to quality that drove prices of U.S. Treasury bonds sharply higher, particularly toward the end of the fiscal period. As the bonds prices rose, their yields fell. The declines were greatest among Treasury securities with the shortest maturities. The yield of the 3-month Treasury bill, which started the fiscal year at 4.89%, dropped more than a full percentage point to 3.81%.
As short-term yields fell, the yield curvewhich illustrates the relationship between short- and long-term bond yieldsreturned to its usual, upward-sloping pattern. The curve had been mildly inverted at the start of the period, with yields of shorter-term bonds above those of longer-term issues. For the year ended September 30, the broad taxable bond market returned 5.1%. Returns from tax-exempt bonds were lower, as these issues did not benefit from the late-summer rally in Treasuries.
A positive year includes a second-half downshift
The second half of the funds fiscal year differed significantly from its first half. As you may recall from our semiannual report, the Strategic Equity Fund did quite well during the six months ended March 31, 2007, on both an absolute basis and relative to its benchmark. Its gains were restrained during the second
 
 
 
Your fund compared with its peer group
 
 
 
 
Average
 
 
Mid-Cap
 
Fund
Core Fund
Strategic Equity Fund
0.30%
1.83%
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
1 Fund expense ratio reflects the 12 months ended September 30, 2007. Peer-group expense ratio is derived from data provided by Lipper Inc. and captures information through year-end 2006.
 
4
 


half by a negative return in the final fiscal quarter as investors favored large-cap stocks, as we discussed above. 
More broadly, your funds investment methodology was under pressure during the entire year. Its not unusual for an investment approach to be out of sync with the market from time to time, and fiscal 2007 was one of those periods. As described in more detail in the advisors letter on page 7, the fund seeks to limit its deviations from the markets sector and industry weightings and risk characteristics. Instead, the fund relies on a sophisticated computer model to attempt to outperform its benchmark through superior security selection, and the success of this selection process is, of course, what determines the funds out- or underperformance. 
The three components of the funds model work together to identify attractive stocks. One component seeks stocks that are favorably valuedinvestment bargains, so to speak. Another focuses on earnings quality, or growth potential. And a third component plugs into market sentiment, as reflected by the direction and pace of change in a stocks price. 
During the past year, the funds stock selections came up short more often than not. Stocks in the information technology, health care, and financials sectors were a significant source of underperformance.
 
Total Returns
 
Ten Years Ended September 30, 2007
 
 
Average
 
Annual Return
Strategic Equity Fund
9.7%
9.7
10.4
The figures shown represent past performance, which is not a guarantee of future results. (Current performance
may be lower or higher than the performance data cited. For performance data current to the most recent month-
end, visit our website at www.vanguard.com/performance.) Note, too, that both investment returns and principal
value can fluctuate widely, so an investors shares, when sold, could be worth more or less than their original 
cost.
 
 
 
 
 
 
 
 
 
 
 
1 The Spliced Small and Mid Cap Index reflects the returns of the Russell 2800 Index through May 31, 2003, and the MSCI US Small + Mid Cap 2200 Index thereafter.
2 Derived from data provided by Lipper Inc.
 
 
5
 


The financials sector, the largest weighting for both the fund and its benchmark, was most directly affected by the subprime mortgage crises.
Even so, the Strategic Equity Fund rang up a solid 13.8% return, and every sector made a positive contribution. About two-thirds of the funds absolute return came from stocks in the industrials, materials, and energy sectors. 
Looking at a longer time horizon, the Strategic Equity Fund has recorded a 9.7% average annual total return for the ten years ended September 30, a period that includes one of the worst bear markets in modern financial history. This means that a hypothetical investment of $10,000 made in the fund ten years ago would have more than doubled to $25,334. The funds result matched that of its benchmark, but trailed slightly behind the performance of peer funds.


Dear Shareholder,
During the fiscal year ended September 30, 2007, Vanguard Capital Opportunity Fund returned more than 24%, outpacing its benchmark, the Russell Midcap Growth Index, and the average return of multi-cap growth funds. The funds return was also superior to the performance of the broad U.S. stock market.
The funds strong performance reflected outsized returns from an interesting mix of holdings. Information technology stocks, the funds largest commitment and a longtime focus of advisor PRIMECAP Management Company, returned more than 30%. At the other end of the industrial production chain, energy and materials stocks also earned strong returns for the fund; these sectors benefited from high prices for oil, fertilizer, and the economys other basic building blocks.
If you own the Capital Opportunity Fund in a taxable account, see page 25 for a report on the funds after-tax returns for the 12 months ended September 30. Please note that our preliminary estimates suggest that the fund will distribute capital gains of roughly $3.30 per share for Investor Shares and $7.65 per share for Admiral Shares in December 2007. As of September 30, the fund remained closed to most new investors. Existing accounts may contribute up to $25,000 per year. (The fund closure and contribution limits do not apply to Flagship members.) 
 
Strong returns for U.S. stocks; even better for markets abroad 
U.S. stocks produced excellent returns for the fiscal year. The gains came despite a midsummer shakeup brought on by problems in the subprime mortgage-loan market. Financials stockswhich represent a sizable share of the U.S. markets valuewere hardest hit, as investment banking and consumer lending businesses throttled back.
The broad U.S. equity market returned 17.1% for the year. Returns from large-capitalization stocks outpaced those of small-caps, and growth-oriented stocks outperformed their value-oriented counterparts. As investors took account of risk, they seemed to exhibit a preference for large-cap growth stocks, which seem better positioned to thrive in a period of economic uncertainty.
Although not immune from the effects of the turmoil in U.S. credit markets, international stocks handily surpassed the returns of domestic stocks over the 12 months. The dollars ongoing weakness further enhanced foreign market gains for U.S.-based investors.
The bond market was shaken, but regained ground in the end
Turmoil in the corporate bond and subprime lending markets caused a flight to quality that drove prices of U.S. Treasury bonds sharply higher, particularly toward the end of the fiscal period. As the bonds prices rose, their yields fell. The declines were greatest among Treasury securities with the shortest maturities. The yield of the 3-month Treasury bill, which started the fiscal year at 4.89%, dropped more than a full percentage point to 3.81%.
 
2
 


Market Barometer
 
 
 
 
Average Annual Total Returns
 
Periods Ended September 30, 2007
 
One Year
Three Years
Five Years
Stocks
 
 
 
16.9%
13.8%
16.0%
12.3   
13.4   
18.8   
17.1   
14.0   
16.5   
31.1   
26.5   
26.3   
 
 
 
 
 
 
 
 
Bonds
 
 
 
5.1%
3.9%
4.1%
Lehman Municipal Bond Index
3.1   
3.9   
4.0   
Citigroup 3-Month Treasury Bill Index
5.0   
4.0   
2.8   
 
 
 
 
 
 
 
 
CPI
 
 
 
Consumer Price Index
2.8%
3.2%
2.9%
 
As short-term yields fell, the yield curvewhich illustrates the relationship between short- and long-term bond yieldsreturned to its usual, upward-sloping pattern. The curve had been mildly inverted at the start of the period, with yields of shorter-term bonds above those of longer-term issues. For the year ended September 30, the broad taxable bond market returned 5.1%. Returns from tax-exempt bonds were lower, as these issues did not benefit from the late-summer rally in Treasuries.
 
Tech stocks took the stage with an unlikely supporting cast
Over the past few years, Vanguard Capital Opportunity Fund has established sizable stakes in technology and health care stocks. At the end of the period, these broad categories accounted for almost 65% of fund assets. 
During the past 12 months, tech stocks helped power your fund to a peer-group and benchmark-beating return of more than 24%. The most notable holding was Research In Motionmaker of the ubiquitous BlackBerryrepresenting almost 6% of fund assets. The stock returned 188% during the past 12 months.
 
 
 
 
Your fund compared with its peer group
 
 
 
 
 
 
Average
 
Investor
Admiral
Multi-Cap
 
Shares
Shares
Growth Fund
Capital Opportunity Fund
0.45%
0.37%
1.49%
 
1  Fund expense ratios reflect the 12 months ended September 30, 2007. Peer-group expense ratio is derived from data provided by Lipper Inc. and captures information through year-end 2006.
 
3
 


Health care stocks, the funds other major position, were less successful on a relative basis. Although these holdings generated a very respectable 14.7% return, the gain fell short of the returns available more generally among health care stocks. Those in the Russell Midcap Growth Index, for example, returned 19%. Weak spots included pharmaceuticals companies, many of which have struggled with regulatory and product-development challenges.
The relative weakness in health care was more than offset by the exceptionally strong returns of the funds energy and materials stocks. At first glance, these sectors might seem odd complements to the funds high-tech tilt, but labels rarely provide meaningful insight to the advisors approach. The advisor relies on exhaustive research to identify stocks with long-term growth prospects unrecognized by the marketoften found at the economys cutting edge, but not exclusively. In materials, the fund earned an exceptional return from its sizable position in Monsanto, which has invested heavily in research and development to produce high-value-added seeds and fertilizer. Monsanto has benefited from soaring grain and agricultural prices. Among energy stocks, the story was simply the continued boom in energy prices.
 
Total Returns
 
Ten Years Ended September 30, 2007
 
 
Average
 
Annual Return
Capital Opportunity Fund Investor Shares
16.7%
Russell Midcap Growth Index
7.5   
5.9   
 
The figures shown represent past performance, which is not a guarantee of future results. (Current performance may be lower or higher than the performance data cited. For performance data current to the most recent month-end, visit our website at www.vanguard.com/performance.) Note, too, that both investment returns and principal value can fluctuate widely, so an investors shares, when sold, could be worth more or less than their original cost.
 
 
1  Derived from data provided by Lipper Inc. 
 
4
 


Dont let exceptional performance give rise to unreasonable expectations
Over the past ten years, Vanguard Capital Opportunity Fund has relied on its wide-ranging search for growth to produce an exceptional annualized return of 16.7%, more than twice the return of its benchmark index for the period and a full 10.8 percentage points better than the peer groups average annual return. An initial investment of $25,000 in Vanguard Capital Opportunity Fund would have compounded to more than $116,000 in wealth. At the peer groups average rate of return, the same $25,000 investment would have compounded to just $44,541. 
 
It would be unreasonable to expect such margins of superiority to persist. Even the best investment managers go through periods of weakness. But we remain confident that, over time, the advisors strategy will prove a formula for success. And in both good times and bad, the funds modest expense ratio will help investors maximize their share of the funds returns.
Uncertainty is par for the course
After several years of unusual calm, the financial markets experienced a jolt in the last three months of your funds fiscal year. Stock market volatility increased sharply, and several other long-established trends seemed to shift into reverse. The changes were dramatic, but a long-term perspective suggests that these occasionaland unpredictabledislocations are an enduring feature of the financial markets.
A prudent response to uncertainty is diversification both within and across asset classes, which is why we counsel investors to hold a broadly diversified portfolio of stocks and fixed income investments in proportions consistent with their goals, risk tolerance, and time horizon. Vanguard Capital Opportunity Fund can play a valuable role in such a portfolio.
Thank you for entrusting your assets to Vanguard.
Sincerely,


Dear Shareholder, 
Vanguard Global Equity Fund returned 27.0% for the fiscal year ended September 30, 2007. The fund topped the average return of its peers in the global fund category and its unmanaged benchmark index. Much of the funds success relative to its comparative standards can be attributed to its advisors strong stock choices in several sectors. 
If you hold shares of Vanguard Global Equity Fund in a taxable account, you may wish to review our report on after-tax returns on page 32. 
Overseas markets continued to outpace the U.S. market
Although they were not immune from some effects of the turmoil that rattled the U.S. stock and bond markets, international stocks handily surpassed their domestic counterparts over the 12 months.
In Europe, the largest markets showed some weaknesses amid inflationary and economic growth concerns; some of the smaller, more peripheral markets, though, boasted solid returns. Japan, the dominant Pacific market, produced mediocre returns, but smaller markets in that region posted solid results, and several reached record highs. Emerging markets continued to post extremely strong results. The dollars ongoing weakness further enhanced foreign market gains for U.S.-based investors. 
In the U.S. market, stocks produced excellent returns for the fiscal year, with the broad U.S. equity market up 17.1%.
 
 
 
 
 
 
 
 
 
2
 


Returns from large-capitalization stocks outpaced those of small-caps, and growth-oriented stocks outperformed their value-oriented counterparts. As investors took account of risk, they seemed to exhibit a preference for large-cap growth stocks, which appeared better positioned to thrive in a period of economic uncertainty. 
The U.S. bond market was shaken, but regained ground in the end
Turbulence in the corporate bond and sub-prime lending markets caused a flight to quality that drove prices of U.S. Treasury bonds sharply higher, particularly toward the end of the fiscal period. As the bonds prices rose, their yields fell. The declines were greatest among Treasury securities with the shortest maturities. The yield of the 3-month Treasury bill, which started the fiscal year at 4.89%, dropped more than a full percentage point to 3.81%.
 
As short-term yields fell, the yield curvewhich illustrates the relationship between short- and long-term bond yieldsreturned to its usual, upward-sloping pattern. The curve had been mildly inverted at the start of the period, with yields of shorter-term bonds above those of longer-term issues. For the year ended September 30, the broad taxable bond market returned 5.1%. Returns from tax-exempt bonds were lower, as these issues did not benefit from the late-summer rally in Treasuries.
The funds advance was driven by strong stock-picking worldwide
During the fiscal year, the Global Equity Funds advisory team skillfully navigated the choppy waters of the domestic and international markets to earn an outstanding 27.0% return. The funds success is a credit to the fundamental research skills of Marathon Asset
 
Market Barometer
 
 
 
 
Average Annual Total Returns
 
Periods Ended September 30, 2007
 
One Year
Three Years
Five Years
Stocks
 
 
 
MSCI All Country World Index ex USA (International)
31.1%
26.5%
26.3%
Russell 1000 Index (Large-caps)
16.9
13.8
16.0
Russell 2000 Index (Small-caps)
12.3
13.4
18.8
Dow Jones Wilshire 5000 Index (Entire market)
17.1
14.0
16.5
 
 
 
 
 
 
 
 
Bonds
 
 
 
Lehman U.S. Aggregate Bond Index (Broad taxable market)
5.1%
3.9%
4.1%
Lehman Municipal Bond Index
3.1
3.9
4.0
Citigroup 3-Month Treasury Bill Index
5.0
4.0
2.8
 
 
 
 
 
 
 
 
CPI
 
 
 
Consumer Price Index
2.8%
3.2%
2.9%
 
 
 
3
 
Management LLP and AllianceBernstein L.P. as well as to the quantitative approach of Acadian Asset Management, Inc. Together, the advisors generated positive returns in nine of ten industry sectors in which the fund invested.
Around 40% of the funds assets, on average, were invested in U.S. companiesroughly 6 percentage points below the index weighting. The funds average weightings in the developed markets of Europe and the Pacific region were also slightly smaller than the corresponding benchmark weightings. The funds emerging-markets exposure was higher than the benchmarks in the Pacific and Middle East/Africa regions, but a bit lower in Europe and Latin America. 
 
Among the funds European holdings, those in smaller marketsparticularly Italy, Finland, and Denmarkoffered the best returns, while many larger marketsincluding the United Kingdom, France, and the Netherlandswere less impressive. In the Pacific region, the funds Australian, Hong Kong, and Singaporean holdings had strong returns. Brazilian and Mexican stocks gave the fund its strongest gains among emerging markets. 
Central to the funds success was strong stock selection. The advisors choices were especially good in the industrials and materials sectors. Among industrials, several companies that make machinery for agricultural and industrial applications turned in strong results, as did some of
 
 
 
Your fund compared with its peer group
 
 
 
 
Average
 
 
Global
 
Fund
Fund
Global Equity Fund
0.64%
1.56%
 
Total Returns
 
Ten Years Ended September 30, 2007
 
 
Average
 
Annual Return
Global Equity Fund
12.5%
MSCI All Country World Index
7.8
7.2
The figures shown represent past performance, which is not a guarantee of future results. (Current performance
may be lower or higher than the performance data cited. For performance data current to the most recent month-
end, visit our website at www.vanguard.com/performance.) Note, too, that both investment returns and principal
value can fluctuate widely, so an investors shares, when sold, could be worth more or less than their original cost.
 
 
 
 
1 Fund expense ratio reflects the fiscal year ended September 30, 2007. Peer-group expense ratio is derived from data provided by Lipper Inc. and captures information through year-end 2006.
2 Derived from data provided by Lipper Inc. the funds holdings in the aerospace & defense subsector. A modest overweighting in industrials also proved beneficial. 
 
 
4
 


In the materials sector, several chemical makers, construction companies, diversified metals companies, and steel makers turned in outstanding results. Many of these businesses have profited from high commodity prices and industry consolidation. Demand for building materials, chemicals, and metalsprimarily from developing economiesdrove the double- and triple-digit returns of several stocks in these subsectors. Security selection also added value in the consumer discretionary area, where a number of hotel, resort, cruise, and restaurant operators, automobile manufacturers, and media companies made sizable contributions. 
On the negative side, the funds holdings in health care and information technology were the largest detractors. In health care, several drug makers around the world posted disappointing results because of poor clinical trial results, a weak pipeline of new drugs, and a tightened regulatory environment. Tech holdings were hurt by weakness among chip makers and manufacturers of storage devices and peripherals. 
For U.S.-based investors, the dollars weakness enhanced the results from several countries once returns were converted from the local currencies into dollars.
For more details on the funds positioning and performance, see the Advisors Report on page 7. 
The funds long-term record has been outstanding 
As you know, its best to measure a mutual funds investment returns over several years, not months or quarters. Assessing a fund over extended market cycles can filter out short-term fluctuations and reveal how an investment performs through a variety of market conditions. This is particularly true of any fund that invests outside the United States. 
During the ten years ended September 30, 2007, the Global Equity Fund had an average annual return of 12.5%, outpacing both its benchmark index and its peer-group average by roughly five percentage points per year. In fact, a hypothetical investment of $10,000 in Global Equity made ten years ago would have grown to $32,472more than $10,000 better than the result of a corresponding investment in the index, which bears no expenses.


Dear Shareholder,
Vanguard Strategic Small-Cap Equity Fund returned 12.6% for the fiscal year ended September 30, 2007its first full fiscal year of operations. Though a solid result in its own right, the funds return trailed the return of its benchmark and the average return of its competitive peers. 
Broadly underlying the shortfall was a growing shift in investor preference during the year toward growth stocks and away from the more reasonably valued stocks favored by the funds advisor, Vanguard Quantitative Equity Group. This shift accelerated during the quarter ended September 30 on shorter-term, worldwide concerns about the impact of the subprime mortgage market crises. This period of turbulence coincided with a sharp uptick in the prices of large-capitalization, growth-oriented stocks. The fund faced some headwinds despite the balanced nature of its investment approach, which simultaneously gives it exposure to attractively valued stocks and stocks with above-average earnings growth. The valuation component hurt more than the growth exposure helped.
Note that if you hold the Strategic Small-Cap Equity Fund in a taxable account, you may wish to review our report on the funds after-tax returns on page 25.
 
 
 
 
2
 


 
Strong returns for U.S. stocks; even better for markets abroad 
U.S. stocks produced excellent returns for the fiscal year. The gains came despite a midsummer shakeup brought on by problems in the subprime mortgage-loan market. Financials stockswhich represent a sizable share of the U.S. markets valuewere hardest hit, as investment banking and consumer lending businesses throttled back.
The broad U.S. equity market returned 17.1% for the year. Returns from large-capitalization stocks outpaced those of small-caps, and growth-oriented stocks outperformed their value-oriented counterparts. As investors took account of risk, they seemed to exhibit a preference for large-cap growth stocks, which seem better positioned to thrive in a period of economic uncertainty.
Although not immune from the effects of the turmoil in U.S. credit markets, international stocks handily surpassed the returns of domestic stocks over the 12 months. The dollars ongoing weakness further enhanced foreign market gains for U.S.-based investors.
The bond market was shaken, but regained ground in the end 
Turmoil in the corporate bond and subprime lending markets caused a flight to quality that drove prices of U.S. Treasury bonds sharply higher, particularly toward the end of the fiscal period. As the bonds
Market Barometer
 
 
 
 
Average Annual Total Returns
 
Periods Ended September 30, 2007
 
One Year
Three Years
Five Years
Stocks
 
 
 
16.9%
13.8%
16.0%
12.3   
13.4   
18.8   
17.1   
14.0   
16.5   
31.1   
26.5   
26.3   
 
 
 
 
 
 
 
 
Bonds
 
 
 
5.1%
3.9%
4.1%
Lehman Municipal Bond Index
3.1   
3.9   
4.0   
Citigroup 3-Month Treasury Bill Index
5.0   
4.0   
2.8   
 
 
 
 
 
 
 
 
CPI
 
 
 
Consumer Price Index
2.8%
3.2%
2.9%
 
 
3


 
prices rose, their yields fell. The declines were greatest among Treasury securities with the shortest maturities. The yield of the 3-month Treasury bill, which started the fiscal year at 4.89%, dropped more than a full percentage point to 3.81%.
 
As short-term yields fell, the yield curvewhich illustrates the relationship between short- and long-term bond yieldsreturned to its usual, upward-sloping pattern. The curve had been mildly inverted at the start of the period, with yields of shorter-term bonds above those of longer-term issues. For the year ended September 30, the broad taxable bond market returned 5.1%. Returns from tax-exempt bonds were lower, as these issues did not benefit from the late-summer rally in Treasuries.
 
A positive year includes a second-half downshift
The second half of the funds fiscal year differed significantly from its first half. As you may recall from our semiannual report, for the six months ended March 31, 2007, the Strategic Small-Cap Equity Funds return was in line with that of its benchmark and was ahead of both the broad markets return and the average return of its mutual fund peers. During the second half, however, the funds gains were restrained by a negative return in the final fiscal quarter as investors favored large-cap stocks, as we discussed above.
More broadly, your funds investment methodology was under pressure during the entire year. Its not unusual for an investment approach to be out of sync with the market from time to time, and fiscal 2007 was one of those periods. As described in more detail in the advisors letter on page 7, the fund seeks to limit its deviations from the markets sector and industry weightings and risk characteristics. Instead, the fund relies on a sophisticated computer model that seeks to outperform its benchmark through superior security selection, and the success of this selection process is, of course, what determines the funds out- or underperformance. 
 
 
 
Your fund compared with its peer group
 
 
 
 
Average
 
Investor
Small-Cap
 
Shares
Core Fund
Strategic Small-Cap Equity Fund
0.38%
1.50%
 
 
 
4
4
 


The three components of the funds model work together to identify attractive stocks. One component seeks stocks that are favorably valuedinvestment bargains, so to speak. Another focuses on earnings quality, or growth potential. A third component plugs into market sentiment, as reflected by the direction and pace of change in a stocks price.
During the past year, the funds stock selections came up short in almost all sectors when compared with benchmark sector returns. The most disappointing was the consumer discretionary group, which encompasses a wide variety of companies that compete for the consumers spare dollar. The only sector that provided a positive relative return was materials, which received a boost from the funds stock choices in the steel and metals and mining industries.
 
Total Returns
 
 
 
Average
 
Annual Return
Strategic Small-Cap Equity Fund
4.9%
MSCI US Small Cap 1750 Index
6.7   
4.6   
 
The figures shown represent past performance, which is not a guarantee of future results. (Current performance may be lower or higher than the performance data cited. For performance data current to the most recent month-end, visit our website at www.vanguard.com/performance.) Note, too, that both investment returns and principal value can fluctuate widely, so an investors shares, when sold, could be worth more or less than their original cost.
 
 
1  Funds inception date.
2  Derived from data provided by Lipper Inc.
 
 
5
 
 


Even so, the Strategic Small-Cap Equity Fund rang up a solid 12.6% return, and almost every sector made a positive contribution to the funds absolute return. The exception was financials, the largest sector weighting for both the fund and its benchmark, which was most directly affected by the subprime mortgage crises.
The funds methodology bypasses market fickleness 
Since it came out of the chute in April 2006, Vanguard Strategic Small-Cap Equity Fund has had a rough ride. During its initial months of operations, the fund experienced a down market. Most recently, in its fiscal fourth quarter, the fund faced a market that was extremely challenging for quantitatively managed portfolios.
More to the point, prudent investing is a long-term proposition, and there has not been sufficient time to judge the merits of the funds long-term strategy. One thing we can say with certainty: the market can be capricious. That is why the discipline and consistency of Vanguard Strategic Small-Cap Equity Funds investment methodology can be invaluable. The advisors stock-selection model helps to immunize the funds holdings against the emotional impulses that can occasionally bedevil even the most experienced and disciplined manager.
Like any fund employing an actively managed strategy, of course, this fund can be expected to go through periods of relative strength and weakness. Over the long term, however, we expect the funds periods of strength to more than offset the occasional downturns. The Strategic Small-Cap Equity Fund offers you an excellent long-term wealth-building opportunity as part of a balanced portfolio of stocks, bonds, and money market assets that is also diversified within each asset class. 
Thank you for entrusting your assets to Vanguard.
Sincerely,


