Dear Shareholders:

The past year has been a great example of why investors should keep their eyes
on the long term.

In the Dow Jones Industrial Average returned But the Dow's upward
rise has not been without hiccups. After hitting new records in July the
Dow lost in the following weeks as a crisis swept global credit markets. As
we have said before, markets can be volatile, and investors should make sure
they have an investment plan that can carry them through the peaks and troughs.

If you are focused on a long-term investment strategy, the short-term ups and
downs of the markets should not necessarily dictate portfolio action on your
part. Both the bond and stock markets are cyclical. In our view, investors who
remain committed to a long-term plan are more likely to achieve their financial
goals. We believe you should not let the headlines guide you in your investment
decisions and should be cautious about overreacting to short-term volatility.

In any market environment, we believe individual investors are best served by
following a three-pronged investment strategy of allocating their holdings
across the major asset classes, diversifying within each class, and regularly
rebalancing their portfolios to maintain their desired allocations. Of course,
these strategies cannot guarantee a profit or protect against a loss. Investing
and planning for the long term require diligence and patience, two traits that
in our experience are essential to capitalizing on the many opportunities the
financial markets can offer -- through both up and down economic cycles.

Respectfully,

Dear Shareholders:

The past year has been a great example of why investors should keep their eyes
on the long term.

In the Dow Jones Industrial Average returned But the Dow's upward
rise has not been without hiccups. After hitting new records in July the
Dow lost in the following weeks as a crisis swept global credit markets. As
we have said before, markets can be volatile, and investors should make sure
they have an investment plan that can carry them through the peaks and troughs.

If you are focused on a long-term investment strategy, the short-term ups and
downs of the markets should not necessarily dictate portfolio action on your
part. Both the bond and stock markets are cyclical. In our view, investors who
remain committed to a long-term plan are more likely to achieve their financial
goals. We believe you should not let the headlines guide you in your investment
decisions and should be cautious about overreacting to short- term volatility.

In any market environment, we believe individual investors are best served by
following a three-pronged investment strategy of allocating their holdings
across the major asset classes, diversifying within each class, and regularly
rebalancing their portfolios to maintain their desired allocations. Of course,
these strategies cannot guarantee a profit or protect against a loss. Investing
and planning for the long term require diligence and patience, two traits that
in our experience are essential to capitalizing on the many opportunities the
financial markets can offer -- through both up and down economic cycles.

Respectfully,
