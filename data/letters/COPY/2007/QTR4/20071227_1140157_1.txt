Dear Shareowner,
- --------------------------------------------------------------------------------
Staying diversified and keeping your portfolio invested in the markets are two
general investment principles that have served investors well over time. They
were particularly useful guides in recent months, when an otherwise healthy
long-term bull market was buffeted by problems in the mortgage and banking
industries.

Since mid-year issues tied to poor underwriting practices in the subprime
sector of the mortgage industry and to problems with risk management by banks
and hedge funds have resulted in increased market volatility and rising concern
about risks to U.S. economic growth.

U.S. economic growth had slowed over the past two years, but this has been due
as much to the natural maturation of the cyclical expansion, as U.S. factories
approached full utilization and the labor market approached full employment, as
to rising commodity prices or short-term interest rates. The slowdown was,
therefore, not entirely unwelcome, as it reduced the threat of higher
inflation. More recently, however, there has been increasing concern that
falling home prices, and/or disruptions in financial markets pose a larger
threat to continuing economic growth, and we have seen two cuts in short-term
interest rates from the Federal Reserve despite strong economic growth in the
second and third quarters of this year. A weaker U.S. dollar has put upward
pressure on some prices, but it has also significantly benefited exporters and
companies competing in the global marketplace, stimulating U.S. economic
growth.

Economic growth in the rest of the world remains relatively positive. In
Europe, solid GDP growth has driven unemployment lower and supported growing
consumption, although concerns about the impact of the strong Euro are a
persistent source of worry. Japanese economic growth continues, albeit at a
muted rate, and the country's deflationary problems are gradually receding.
Economic growth in emerging market countries remains faster than in the
developed world, led by China, which continues its rise as a world economic
power.

Looking forward, the economic outlook continues to appear generally positive,
although real estate prices, subprime mortgage defaults, and the possibility of
a liquidity/credit crunch represent growing sources of risk. Central banks have
responded to the stresses in the inter-bank and commercial paper markets by
acting as "lenders of last resort" and, in the case of the Federal Reserve, by
cutting rates.

The U.S. Federal Reserve and the world's other central banks continue to pursue
policies aimed at producing low and stable inflation, believed to be the best
backdrop for steady economic growth and low average unemployment over the long
term. Keeping inflation low is also an





Letter

important support for stock and bond valuations, and so central bank policies
have also been "investor friendly." We view the Fed's recent rate cuts as
temporarily "buying insurance" against a credit crunch, which would threaten
economic growth rather than as an abandonment of its commitment to keeping
inflation low.

Even against this "wall of worry" backdrop, the long-term performance of major
asset classes has remained positive. In the months ending October
equity investors were by and large rewarded as, despite several interim
corrections, the Standard & Poor's Index returned the Dow Jones
Industrial Average returned and the NASDAQ Composite Index returned
International developed and emerging markets equities performed even better,
reflecting both a weakening dollar (which boosts returns for U.S. dollar-based
investors) and strong local currency returns, with the MSCI EAFE Developed
Market Index returning and the MSCI Emerging Markets Index returning
over the period. The U.S. bond market, as measured by the Lehman
Aggregate Bond Index, returned in the months ending October The
U.S. high-yield bond market, as measured by the Merrill Lynch High Yield Bond
Master II Index, returned for the same period, reflecting its higher coupon
yield.

Sudden swings in the markets are always to be expected. Just as staying
diversified and invested are important investment principles, it is also
important to pay attention to asset allocation. As always, we encourage you to
work closely with your financial advisor to find the mix of stocks, bonds and
money market assets that is best aligned to your particular risk tolerance and
investment objective.

Respectfully,
