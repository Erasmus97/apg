DEAR PHOENIX FUNDS SHAREHOLDER:

We are pleased to provide this report for the fiscal year ended August
It includes valuable information about your Phoenix mutual fund(s)--such
as performance- and fee-related data and information about each fund's portfolio
holdings and transactions for the reporting period. The report also provides
commentary from your fund's management team with respect to the fund's
performance, its investment strategies, and how the fund performed against the
broader market.

At Phoenix, we are committed to helping investors succeed over the long
term and we strive to provide investors with choice. The Phoenix family of funds
offers a wide array of investment options to help make diversification easy. Our
multi-manager, multi-style approach gives you access to a complete suite of
investment products, including numerous equity, fixed income and money market
funds.

We are proud to offer this diversified portfolio of funds managed by more
than a dozen accomplished independent investment managers--many of whom were
previously only available to large institutional investors. Their investment
expertise allows us to offer a variety of styles, including growth, value and
core products, along with asset allocation funds and alternative investments.

Phoenix is also committed to providing you best-in-class service. Whether
you need to check your account value or transfer funds, our customer service
team stands ready to provide superior, dependable assistance to help you make
informed decisions.

Because we offer such an extensive selection of investment options, it's
important that you consult an experienced financial professional for help
reviewing or rebalancing your portfolio on a regular basis. A regular "checkup"
can be an excellent way to help ensure that your investments are aligned with
your financial goals.

As president of Phoenix Investment Partners, Ltd., I would like to thank
you for entrusting your assets with us. It's our privilege to serve you.

Sincerely,
