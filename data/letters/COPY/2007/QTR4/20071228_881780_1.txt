Dear Shareholder:

We are pleased to present this annual report for Dreyfus Growth and Income Fund, Inc., covering the period from November through October

After a prolonged period of relative price stability, volatility has returned to the U.S. stock market.The third quarter of provided greater swings in stock prices than weve seen on a relative year-to-date basis,
as the economic cycle matured and credit concerns spread from the sub-prime mortgage sector of the bond market to other areas of the equity and fixed-income markets. A high degree of leverage within parts of the financial system has made recent
price fluctuations more intense than they might be otherwise.

In our view, these developments signaled a shift to a new phase of the credit cycle.Although we expect somewhat slower financial conditions in the aftermath of the credit crisis, lower short-term interest rates from the
Federal Reserve Board should help keep credit available to borrowers and forestall a more severe economic downturn. In addition, robust global economic growth may continue to support earnings for many U.S.-based companies. Such market events may
indicate that its a good time to review your portfolio with your financial advisor, who can help you evaluate your investments for a changing market environment.

For information about how the fund performed during the reporting period, as well as market perspectives, we have provided a Discussion of Fund Performance given by the funds Portfolio Manager.

Thank you for your continued confidence and support.




Thomas F. Eggers




Chief Executive Officer




The Dreyfus Corporation




November













DISCUSSION OF FUND PERFORMANCE

For the period of November through October as provided by John Jares, Portfolio Manager

Fund and Market Performance Overview

Despite bouts of heightened volatility stemming from a domestic credit crisis and a slowing U.S. economy, large-cap growth stocks posted respectable returns for the reporting period due to generally healthy corporate
earnings and robust global economic growth.The fund produced a higher return than that of its benchmark, primarily due to attractive results in the financials and information technology sectors.

For the period ended October Dreyfus Growth and Income Fund produced a total return of This compares with the
funds benchmark, the Standard &amp; Poors Composite Stock Price Index, which produced a total return of for the same The Russell Growth
Index, which more closely reflects the funds current composition, returned for the

The Funds Investment Approach

The fund seeks long-term capital growth, current income and growth of income consistent with reasonable investment risk by investing primarily in domestic and foreign stocks.When choosing stocks, we use a growth
style of investing, searching for companies whose fundamental strengths suggest the potential to provide superior earnings growth over time.We use a consistent,bottom-up approach that emphasizes individual stock selection, and we conduct
qualitative and quantitative in-house research to determine whether a stock meets our investment criteria. Income is primarily generated from dividend-paying stocks.

Corporate Earnings and International Growth Supported Stock Prices

Stocks generally advanced during the reporting period as strong corporate earnings growth more than offset the negative effects of rising energy prices, slower U.S. economic growth and a credit crisis that
originated

The Fund







DISCUSSION OF FUND PERFORMANCE (continued)

in the U.S. bond markets sub-prime mortgage sector. Numerous large-cap companies benefited from greater pricing power amid moderate inflation, and a weak dollar in a robust global economy created favorable business
conditions for large, U.S.-based multinational companies.

In this environment, an underweighted allocation to the financials sector produced the greatest contribution to the funds relative performance. Reduced exposure to the sector helped shelter the fund from the problems
many financial firms experienced in a credit crunch stemming from a deteriorating sub-prime mortgage market. In addition, the funds security selection strategy in this sector steered the fund away from companies directly exposed to the
mortgage industry. Instead, we focused on companies such as brokerage house Charles Schwab, which was better able to weather the financial sectors downturn. Schwab benefited from strong asset inflows, falling telecommunications prices and a
successful restructuring plan that reduced operational costs.

The funds relatively heavy exposure to the information technology sector bolstered its relative performance.Apples stock price was driven higher by the popularity of its products, such as the iPhone and the
iPod. Microsoft benefited from robust sales of its gaming platform and higher-margin software products during the reporting period.Shares of electronic storage specialist EMC/Massachusetts appreciated due to the spin-off of its virtualization
software company, VMware. Internet networking firm Cisco Systems benefited from sales of a new generation of Internet infrastructure equipment, while computer hardware manufacturer Hewlett-Packard captured a larger share of the personal computer
market.

Consumer staples companies such as Avon Products, which experienced upward pressure on its stock due to a successful restructuring and increasing exposure to emerging markets, also added to the funds relative
performance. Colgate-Palmolive benefited from robust growth in international markets, particularly emerging economies, as the falling value of the U.S. dollar made its products more attractive to overseas consumers.

Although energy stocks continued to gain value during the reporting period, the funds underweighted allocation to energy companies pre-









vented it from participating fully in the sectors strong performance. Favorable results from several of the funds energy investments, such as integrated oil giant Exxon Mobil, were not enough to offset
allocation-related weakness.The funds relative performance also was undermined by its relatively light exposure to and disappointing selections of materials and industrials stocks.

Our security selection strategy also constrained returns from consumer discretionary companies, erasing the benefits of slightly underweighted exposure to this weak-performing sector. Retailers such as big-box electronics
seller Best Buy Company, home improvement chain The Home Depot, mass merchandiser Wal-Mart Stores and home goods outfitter Bed Bath &amp; Beyond were hurt by slumping consumer spend-ing.The fund sold Bed Bath &amp; Beyond during the reporting
period.

Materials and Commodities Stocks Warrant Further Consideration

Surging global economic growth has persisted, suggesting to us that robust international demand is likely to continue. Consequently, we intend to investigate opportunities in the energy, materials and industrials areas,
perhaps bringing the funds underweighted allocations in these sectors closer to those of its benchmark.We also intend to look closely at companies with ample international exposure, as the falling value of the U.S. dollar and strong global
economic growth may benefit these firms. As always, we have continued to employ a bottom-up research process designed to identify companies that, in our judgment, have the potential to achieve superior earnings and revenue growth.

November








Total return includes reinvestment of dividends and any capital gains paid. Past performance is no
