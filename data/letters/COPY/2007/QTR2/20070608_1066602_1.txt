Dear Shareholder, 
 
The year is far from over but already investors have witnessed some remarkable events. There were
global market tremors that came on the heels of last Februarys single-day freefall in Chinese stocks. Widespread ripples were also felt following a downturn in the U.S. sub-prime mortgage industry. But, there have also been many positive
developments. The U.S. Federal Reserve Board appears confident about the future. The U.S. labor market continues to show strength and recently we have seen the Dow Jones Industrial Average
 
Meanwhile, investors seeking income should be especially pleased to know that U.S. bonds remain
popular among overseas investors and long-term inflation expectations continue to dwindle. Such news is typically a good sign for bonds. 
 
What do we make of all of these contrasting signals? When friends and colleagues in the industry voice concerns about such seemingly-divergent milestones, I remind them that such
events underscore the importance of a well-diversified investment strategy. Study after study shows that a portfolio allocated across a diverse group of asset classes and investment sectors may provide an investor with solid footing for the
long-term, despite the short-term commotions. 
 
We at ING Funds remain committed to
providing you, our investor, with a diverse and comprehensive line-up of innovative investment products, including a range of global investment solutions  all designed for the long-term. Whatever your investing goals, we look forward to
continuing to serve you. 
 
Sincerely, 


