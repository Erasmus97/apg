Dear Shareholder:

We are pleased to present this annual report for Dreyfus Growth Opportunity Fund, Inc., covering the period from March

through February

Despite a sudden bout of market volatility near the end of February the U.S. stock market fared relatively well over the course of the fiscal year. Stock prices generally were propelled higher by stabilizing
short-term rates, moderate inflationary pressures and robust corporate earnings. Although the U.S. economy has shown some signs of decelerating, our chief economist currently feels the risk of a full-blown recession is extremely small.

We remain optimistic about the sustainability of the economic expansion. Over the long term, productivity has increased as modern technologies and efficient business practices helped to limit cyclical inflation pressures
around the world. Over the more immediate term, a warm winter in the United States and historically low interest rates have helped to mitigate the risks a weakening U.S. housing sector have on business and consumer confidence.The overall result has
been to bolster investor sentiment and stock prices.As always, your financial consultant can help you identify the investments that may be most likely to help you profit from these trends.

For information about how the fund performed during the reporting period, as well as market perspectives, we have provided a Discussion of Fund Performance given by the funds portfolio managers.

Thank you for your continued confidence and support.











DISCUSSION OF FUND PERFORMANCE

Elizabeth Slover, Martin Stephan and Barry Mills, Portfolio Managers

How did Dreyfus Growth Opportunity Fund perform relative to its benchmark?

For the period ended February the fund produced a total return of In comparison, the funds benchmark, the
Russell Growth Index (the Index), produced a total return of for the same

Although investors were concerned during the first half of the reporting period about rising interest rates and surging energy prices, investor confidence improved over the second half as rates stabilized and inflationary
pressures subsided.

What is the funds investment approach?

In seeking long-term capital growth consistent with the preservation of capital, the fund invests at least of its assets in stocks issued primarily by U.S. companies.The fund also may invest up to of its assets in
foreign securities. Current income is a secondary goal. We employ both bottom-up and top-down approaches to create a portfolio of growth stocks. Our top-down analysis of economic, market or industry trends may
lead to overweight or underweight positions in certain market sectors. We then use bottom-up fundamental analysis within those sectors to identify companies with sound financial health, growing earnings or cash flows, strong competitive
positions and the presence of a catalyst that can trigger an increase in the companys stock price.

What other factors influenced the funds performance?

Concerns regarding a potentially overheated economy, intensifying inflationary pressures and rising interest rates dampened the performance of U.S. stocks during the first half of the reporting period. However, equity
markets rebounded sharply over the second half as economic growth moderated, energy prices fell and the Federal Reserve Board refrained from further rate hikes.

The Fund







DISCUSSION OF FUND PERFORMANCE (continued)

Our bottom-up analysis proved to be particularly successful in reading the business cycle of the industrials sector. Early in the reporting period, the fund was invested in a number of early-cycle industrials companies,
such as construction machinery manufacturer Caterpillar, which we had regarded as undervalued. However, as the reporting period progressed, we grew concerned that the industrials sectors cyclical strength was abating, and we shifted to
larger-cap multinationals, such as General Electric, that tend to do well under a variety of economic conditions. Aircraft manufacturer Airbus provided a trading opportunity as problems with production drove the stock down well below what we
believed was its fundamental value. We took advantage of this and received the benefit as the market brought the stock back close to its fundamental value.

Relatively low exposure to home improvement and consumer electronics retailers aided the funds results in the consumer discretionary sector, as weakness in housing markets and its potential impact on consumer spending
negatively affected those industries. Conversely, the fund held positions in higher-end retailers Coach and Polo Ralph Lauren, which benefited from strong earnings growth. Robust consumer travel trends supported lodging holdings, such as Marriott
International.

Several consumer staples companies contributed positively to the funds relative performance. Food and tobacco giant Altria Group gained value after announcing a spin-off of its Kraft Foods unit, while dairy and soy
foods producer Dean Foods and supermarket chain SUPERVALU achieved higher stock prices after successful corporate restructurings. Several multinational consumer staple holdings, including household goods provider Colgate-Palmolive, also advanced as
energy and other commodity prices declined, reducing overhead costs.

In the materials sector, aggregate stone manufacturer Vulcan Materials benefited from increased sales and earnings, and chemical producer Celanese took steps to strengthen its financial profile during the reporting period,
bolstering its share price.

On the other hand, the financial services and information technology sectors detracted from relative performance. Despite the overweight









position in the Investment Banks/Brokers by the fund, not owning Goldman Sachs hurt performance, as did the position in securities exchange NASDAQ, which failed to acquire the London Stock Exchange. However, the funds
investment in non-benchmark name JP Morgan contributed positively. Although Internet media companies generally outperformed traditional media sources over the past few years, the reporting period saw a reversal of this trend as Yahoo! and Google
fell in the wake of slowing growth rates. Semiconductor manufacturer Marvell Technology also faltered due to an options backdating scandal in which the company was implicated.

Other notable detractors from the funds relative performance during the reporting period included womens apparel retailer Chicos FAS, which fell on expansion miscues and weak same-store sales comparisons,
and auto retailer Advance Auto Parts, which reported lower-than-expected earnings.

What is the funds current strategy?

We have continued to employ our bottom-up and top-down approaches to assemble a portfolio of stocks, which are considered to have high growth potential.As of the reporting periods end, we have reduced the funds
position in smaller-cap industrials stocks while increasing exposure to large-cap industrials. In the consumer staples area, we have favored multinational companies and may consider adding other large, global firms to the portfolio.We also may look
for companies poised to benefit from corporate restructuring through the use of private equity.

March








Total return includes reinvestment of dividends and any capital gains paid. Past performance is no
