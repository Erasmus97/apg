DEAR PHOENIXFUNDS SHAREHOLDER:



We are pleased to provide this report for the fiscal year ended April
It includes valuable information about your Phoenix mutual fund(s)--such
as performance- and fee-related data and information about each fund's portfolio
holdings and transactions for the reporting period. The report also provides
commentary from your fund's management team with respect to the fund's
performance, its investment strategies, and how the fund performed against the
broader market.

At Phoenix, we strive to provide investors with choice. Our multi-manager
approach provides individual investors with access to a variety of investment
managers, including some they might otherwise not have access to--managers who
are usually available only to larger institutional investors. I am pleased that
our fund family, PhoenixFunds, can offer you the ability to invest in funds
managed by more than a dozen different management teams, including both Phoenix
affiliates and outside sub-advisers.

We also make diversification easy, with a wide array of investment
options--including numerous equity, fixed income and money market funds. For
those looking to simplify the investment selection process, we offer Phoenix
PHOLIOs(SM) (Phoenix Lifecycle Investment Options). Each PHOLIO is a broadly
diversified portfolio of mutual funds that enables investors to gain exposure to
a variety of investment options (such as equity, international/global, balanced,
alternative and fixed income). Phoenix PHOLIOs were designed to help investors
stay on track over time, with a targeted asset allocation mix that is rebalanced
regularly.

Because we offer such an extensive selection of investment options, it's
important that you consult an experienced financial professional for help
reviewing or rebalancing your portfolio on a regular basis. This can be an
effective way to help ensure that your investments are aligned with your
financial objectives.

For more information on the mutual funds including PHOLIOs that we currently
offer, I invite you to visit our Web site, at PhoenixFunds.com.

As the new president and chief operating officer of Phoenix Investment
Partners, Ltd., I would like to thank you for incorporating PhoenixFunds into
your financial strategy. It's our privilege to serve you.

Sincerely yours,
