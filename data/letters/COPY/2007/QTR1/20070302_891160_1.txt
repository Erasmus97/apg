Dear Fellow Shareowner,

The bull market continued during It was a good year for U.S. equity
investors, as most stock market indices recorded double-digit returns. The
investment strategy utilized was not a determining factor in domestic stock
performance last year. Nearly everything worked, as small-caps, mid-caps,
large-caps, growth stocks, and value stocks all rose pretty much in tandem.
Large-caps finally outperformed small-caps, but only by a narrow margin. The
Schwartz Value Fund ("Fund") had a total return of which modestly
exceeded the Fund's average.

Annual Rate of Return
For year ended
-------------------------------------------
SCHWARTZ VALUE FUND

Russell Index
NASDAQ Composite(a) +
S&P Index

Stocks that contributed positively to the Fund's performance included Fargo
Electronics, Inc., Thor Industries, Inc., Input/Output, Inc., Unico American
Corporation, and Kinetic Concepts, Inc. Stocks that detracted from performance
included ProQuest Company, National Dentex Corporation, TVI Corporation, and
Craftmade International, Inc. Despite trailing some of the stock market indices
in the Fund's compound annual rate of return continues to compare
favorably.

Compound Annual Rate of Return
For Years Ended
-------------------------------------------
SCHWARTZ VALUE FUND +

Russell Index +
NASDAQ Composite (a) +
S&P Index +






Although the Schwartz Value Fund has historically focused on investing in small
and mid-cap stocks (because that's where the best values were found),
increasingly we are finding attractive opportunities among larger-cap companies.
Small-cap stocks have been on a tear since and as a result investor
affections and valuations for these stocks are up. With the Russell
small-cap index near its all-time high, it is more difficult to find undervalued
small-cap ideas. In contrast, some large-cap companies have modest valuations
due to several years of underperformance. In fact, some senior growth stocks are
selling at prices which we believe represent real value. Accordingly, during
the Fund took positions in Caterpillar Inc., Johnson & Johnson, Wal-Mart
Stores, Inc., ConocoPhillips, EnCana Corporation, Legg Mason, Inc., and
Berkshire Hathaway, Inc.

On December the Fund paid a per share distribution, which was
composed of per share of short-term gains and per share of
long-term gains. After the distribution, the Fund ended the year with a NAV of
per share.

Best regards,

Dear Shareowner of:

Ave Maria Catholic Values Fund (AVEMX), started May
Ave Maria Growth Fund (AVEGX), started May
Ave Maria Bond Fund (AVEFX), started May
Ave Maria Rising Dividend Fund (AVEDX), started May
Ave Maria Opportunity Fund (AVESX), started May
Ave Maria Money Market Account, started July

The family of Ave Maria Mutual Funds continued to grow in with the
addition of the Ave Maria Opportunity Fund (AVESX), a small-cap focused mutual
fund, and the Ave Maria Money Market Account. With these two new investment
vehicles added to our list of offerings, pro-life and pro-family investors have
an even wider array of diversification options.

On July Schwartz Investment Counsel, Inc., the investment manager of
all the Ave Maria Mutual Funds, entered into a joint venture with Federated
Securities, Inc. to offer the Ave Maria Money Market Account. This money market
Fund is managed by Federated, and adheres to the same moral principles as all
the Ave Maria Mutual Funds. Federated Securities, Inc. is Pittsburgh-based and
is one of the largest and oldest managers of money market funds in the country.
We are blessed to be in partnership with this highly respected firm.

As portfolio managers of the Ave Maria Mutual Funds, we screen out companies
based on the guidelines established by our Catholic Advisory Board. This
eliminates from consideration companies related to abortion, pornography, and
companies which offer their employees non-marital partner benefits. Apparently
that approach has appeal; at least people, at last count, have invested
in the Ave Maria Mutual Funds.



Importantly, the investment performance of all the Ave Maria Funds has been
respectable. I'm happy to report that Morningstar* has recently rated each of
our three Funds which are more than three years old - Catholic Values, Growth,
and Bond - a rating (out of stars). Our investment research emphasis on
fundamentals with a long-term focus seems to be working.

Thanks for being a shareowner.
Sincerely,

Dear Fellow Shareowner:

The Ave Maria Catholic Values Fund had a total return in compared to
for the S&P Index and for the S&P MidCap Index. Since
inception on May the Fund has outperformed both benchmarks:

From (Inception) Through
Total Returns
-----------------------------------------
Cumulative Annualized
---------- ----------
Ave Maria Catholic Values Fund (AVEMX)
S&P Index
S&P MidCap Index

For the third consecutive year the stock market finished strong following a
lackluster first half. second half gain was fueled by double-digit
corporate profit growth, declining energy prices and the Federal Reserve's
decision to refrain from further rate hikes after July. We anticipate a
continuation of economic trends that should provide a favorable environment for
equities in not withstanding a slower rate of corporate profit growth.

The Fund's investment return in can be attributed to a broad range of
stocks across many industries, but notable out-performers included Input/Output,
Inc., Fargo Electronics, Inc,. Thor Industries, Inc., The Sherwin Williams
Company, Meadowbrook Insurance Group, Inc., and Dollar Tree Stores, Inc. Stocks
with the largest negative impact were ProQuest Company, Lifetime Brands, Inc.,
Mohawk Industries, Inc., and Pulte Homes, Inc. A number of the weaker performers
are related to residential construction, but notably many of them recovered
nicely late in the year.

In we sold shares of six companies as a result of their changed corporate
policies, which violated the Fund's moral screens. These holdings were
liquidated, as we were unable to persuade these companies to change their
offensive behavior.

Other significant eliminations from the Fund in the second half of this year
were: Lincare Holdings, Inc. because of concerns over reduced Medicare
reimbursement rates; Christopher & Banks Corporation, First Marblehead
Corporation and Core Laboratories N.V., which appreciated beyond our price
objectives, and SEMCO Energy, Inc. due to deteriorating fundamentals.

The Fund took advantage of opportunities to invest in a number of larger
capitalization companies this year including United Technologies Corporation,
Legg Mason, Inc., Johnson Controls, Inc., Stryker Corporation and ITT
Corporation. We are finding more attractive values in larger companies as




AVE MARIA CATHOLIC VALUES FUND
PORTFOLIO MANAGER COMMENTARY

Dear Fellow Shareholders:

For the year ended December the Ave Maria Growth Fund had a total
return of equaling the return of the S&P Index. For the three years
ended December the Fund's total return was annualized compared
with annualized for the S&P Index. Since inception (May the
Fund's total return has been annualized compared with annualized for
the S&P Index.

The top five performing issues in the Ave Maria Growth Fund for were:

SEI Investments Company (Asset Management)
Mettler-Toledo International Inc. (Electronic Weighing Equipment)
Harley-Davidson, Inc. (Motorcycle Manufacturer)
FactSet Research Systems, Inc. (Database Software)
Franklin Electric Company, Inc. (Submersible Pumps)

The bottom five performing issues were:

Frontier Oil Corporation (Oil & Gas Refining & Marketing)
Varian Medical Systems, Inc. (Healthcare)
Landstar System, Inc. (Trucking)
Brown & Brown, Inc. (Insurance Broker)
The Black & Decker Corporation (Household Appliances)

The Fund is diversified among eight out of eleven S&P economic sectors:
Consumer Staples Consumer Cyclicals Financial Transportation
Energy Capital Goods Technology and Healthcare

As of December the Ave Maria Growth Fund was rated by
Morningstar* (out of mid-cap growth funds). Overall Morningstar Rating(TM)
for a fund is derived from a weighted average of the performance figures
associated with its and (if applicable) rating metrics.

Respectfully,

Dear Fellow Shareholders:

The total return for the Ave Maria Rising Dividend Fund in was This
compares with for the S&P Index and for the S&P Dividend
Aristocrat Index. Important contributions to performance came from a variety of
industries, including FPL Group, Inc. (a Florida utility), The Sherwin-Williams
Company (paint), Diebold, Incorporated (automated teller machines), and Thor
Industries, Inc. (recreational vehicles). Performance was adversely effected by:
Pulte Homes, Inc. (homebuilder), Cato Corporation (retailer), and Automatic Data
Processing, Inc. (payroll processor). Over the course of the year, shares of six
companies were sold because of changes in their corporate policies, which
violated our pro-life/pro-family screens.

As of December the Ave Maria Rising Dividend Fund held shares of
companies across industries. Of these stocks, raised their dividend
during Investing in companies that have regularly raised their dividend is
a defining characteristic of this Fund. A company with a long history of rising
dividends is almost always indicative of balance sheet integrity and sound
underlying business fundamentals. It goes without saying that in order to
increase dividends over many years, companies must also generate growth in
earnings and cash flow. Importantly, when making investment decisions for the
Fund, we don't just look at historical results; we look forward, too, in an
effort to anticipate what the future may hold for these fine companies. Before
we buy shares of any company for the Fund, our research analysts have to show
the likelihood of continued earnings and dividend increases for that company. In
addition, valuation is a critical component of the portfolio management process.

Here is another benefit to this style of investing. One can take comfort in the
knowledge that the Boards of Directors of the companies in which we've invested
really seem to understand that the assets they are entrusted with managing,
belong to the shareholders. Companies represented in the Ave Maria Rising
Dividend Fund have Boards which have demonstrated respect for the interests of
their shareholders.

We appreciate your commitment and strive to merit your confidence.

With best regards,

Dear Fellow Shareholders:

The Ave Maria Opportunity Fund ended with a net asset value per share of
which along with a year-end distribution of per share, gave
shareholders an total return since the Fund's inception on May In
comparison, the Russell Index produced a total return of and the S&P
SmallCap Index a total return of during the same period.

Stocks which contributed notably to the positive performance during the year
included American Science and Engineering, Inc. (x-ray detection machines),
Champion Enterprises, Inc. (manufactured housing), Fargo Electronics, Inc.
(identification card printers), Gentex Corporation (automotive mirrors),
Input/Output, Inc. (energy), and Yak Communications Inc. (telecommunications).
Under-performers in the portfolio were Lifetime Brands, Inc. (consumer
products), Mine Safety Appliances Company (first responder & military
equipment), Stanley Furniture Company, Inc., and TVI Corporation
(decontamination & infection control equipment).

In managing this Fund, emphasis is placed on selecting well-managed, small and
mid-sized companies that have proprietary products and/or a unique industry
niche. We want companies with excellent business characteristics, including a
dominant market position, above average profit margins, excess free cash flow,
high returns on equity with little or no debt, and strong growth potential. At
the time of purchase, the company's stock price must be selling at a discount to
its intrinsic value. Recent additions to the portfolio that have some or all of
these characteristics include A.S.V., Inc. (earth moving equipment), Foot
Locker, Inc. (athletic retailing), Harris Corporation (defense communications),
MTS Systems Corporation (simulation systems), Raven Industries, Inc. (industrial
manufacturing), and Select Comfort Corporation (Sleep Number beds). Recently,
these companies' stock prices have been under pressure, owing to near-term
difficulties. This situation affords value investors like us an opportunity to
establish a position at an attractive price. When the temporary problems
subside, the share prices of these companies should resume an upward trajectory.

Your interest and investment in the Ave Maria Opportunity Fund is very much
appreciated.

With best regards,

Dear Fellow Shareholders:

In the Ave Maria Bond Fund (Retail Class) had a total return of This
compares favorably with the return on the Lehman Brothers Intermediate
Government/Credit Index of The average maturity of the bond portfolio was
shorter than that of the index for much of the year. This was additive to
performance in an environment of generally rising interest rates. Throughout the
year, we emphasized the purchase of very high quality bonds. Equities
represented of the portfolio at year end, and contributed positively to
performance with of those companies raising their dividends during the year.
One issue was sold because of a change in its corporate policies, which resulted
in the violation of our pro-life/pro-family screens.

Between June, and June, the Federal Reserve raised the Fed Funds rate
on seventeen different occasions, moving that rate from to where it
currently stands. We believe this marks the peak in the Fed Funds rate for the
current interest rate cycle. Last year, long-term interest rates moved up, much
less than short-term rates, which are more directly impacted by Fed policy. The
result is a negatively sloped yield curve with short-term rates higher than
long-term rates.

Much has been made of the fact that the investment world is awash in liquidity.
One of the effects of this phenomenon has been the low compensation investors
receive for assuming more risk. This has expressed itself in the bond market as
very small differences in yield for bonds with significant differences in credit
ratings. Another way to view this situation is that there is little "give-up" in
yield for owning high quality. Quality is cheap. Therefore, we have stressed
purchases of U.S. Treasuries and Agencies (governments), the highest quality
bonds available, and relatively short maturities which reduce volatility.

I'm happy to report that the Ave Maria Bond Fund (Retail Class) is rated
by Morningstar* (out of intermediate-term bond funds). Overall Morningstar
Rating(TM) for a fund is derived from a weighted average of the performance
figures associated with its and (if applicable) rating metrics.

Thanks for investing in the Ave Maria Bond Fund.

With best regards,
