Dear Shareholder:

For the calendar year the Epoch International Small Cap (EPIEX) Fund was
up at net asset value. In comparison, the Fund's benchmark, the
S&P/Citigroup EMI EPAC index was up The Fund significantly outperformed
its benchmark while displaying attractive risk characteristics as illustrated
by a high information ratio and a benign Beta vs. its benchmark.

Our focus on combining a top-down thematic approach with fundamental bottom-up
stock analysis and our preference for investing in quality businesses at
reasonable prices led us to build a diversified portfolio of stocks, the Fund's
portfolio, in aggregate, exhibited higher returns on equity, higher returns on
assets and faster growth in revenues and earnings when compared to the
benchmark, while trading at attractive valuations.

Superior stock selection across all major sectors (as defined by the benchmark)
helped generate strong absolute and relative returns. Stock picking was
particularly strong in the Consumer Staples, Materials, Healthcare, Financials
and Consumer Discretionary sectors. Several of our largest positions were also
among the strongest contributors. Among our best performers were C&C Group, an
Irish producer of cider and spirits, Lonmin, one of the world's largest
producers of platinum group metals, Phonak Holding, a maker of hearing aids
based in Switzerland, Marfin Financial Group, a Greek diversified financial
services company and the Osaka Securities Exchange, the largest
securities exchange in Japan.

From a sector perspective, the Fund benefited from our underweight positioning
in Consumer Discretionary sector and overweights in Healthcare, Industrials and
Consumer Staples. From a geographic perspective, the Fund benefited from an
active management of our Japan exposure and overweight positions in Ireland and
Greece. An underweight position in Australia detracted from performance. In
Japan, we started the year with an overweight position that we had held since
mid In the first quarter of we decided to underweight our exposure
in Japan relative to the benchmark. Among the reasons for that change were our
concerns regarding stock valuations after the very strong performance of the
Japanese market in the second half of as well as concerns regarding the
possible impact of several highly visible setbacks against prominent
shareholder activists.

As we begin we remain constructive on the international small cap asset
class and international equity markets in general. We believe that
globalization continues to be a powerful and positive force, enhancing global
economic growth, productivity and profits while constraining inflation. At the
same time, we remain vigilant regarding the impact that a possible deceleration
of global GDP growth, a potential rise in investor risk aversion and certain
geopolitical events could have on global equity markets. We believe the Fund is
well positioned and reflects our world views for

Daniel Geber
Portfolio Manager

Dear Shareholder,

Market Environment:

was indeed a good year for equity returns. What initially had looked like
a relatively good year for the stock market turned into an excellent one. The
market rally, which began in response to the Federal Reserve's August decision
to pause in its string of rate hikes, turned into a full-blown frenzy during
the fourth quarter. Indeed, the robust returns of the fourth quarter
represented more than half of the year's return.

We believe that by December the market had become overly optimistic about
a "Goldilocks" economic scenario, in which a mid-cycle slowing moderates both
GDP growth and inflation to a "just right" level allowing the Fed to begin to
make interest-rate cuts that re-ignite economic growth. This optimism drove
returns particularly in the materials, energy and financial sectors.

Portfolio Performance:

Because of strong stock selection, the Fund's holdings within the Consumer
Discretionary and Information Technology sectors had the largest positive
effect on results for the year. In Consumer Discretionary, we favored
leisure/entertainment companies in media, gaming and entertainment software
over retail/consumer goods companies. The Fund's position in Materials also
positively contributed to performance. Most of the stocks we hold in this
sector are "late cycle" cyclicals, which we selected in anticipation of good
performance at this point in the economic cycle.

In Technology, although each of our holdings has a different investment thesis,
the companies we have purchased for the Fund performed well, generally
benefiting from corporate spending increases or consumer product cycles. Most
of these companies own valuable intellectual property, which results in greater
profitability than their peers' more commoditized product offerings.

The Fund's economically sensitive stocks were well-positioned for the rally.
Still, the magnitude of the rally was difficult to match overall, given its
speculative nature. Our philosophy is to choose stocks that generate consistent
and growing free cash flow. Total return is a result of the intelligent use of
that cash by companies to either grow the business or return value to
shareholders over a market cycle. Given our preference for consistency over
financial and economic leverage, our investments tend to do well in most
environments - except for speculative rallies when expanding P/E ratios tend to
drive returns.
There were three areas in which our more conservative positioning in the Fund
did not keep pace with short term market results. In the volatile energy
sector, the Fund was positioned in less commodity price sensitive companies in
favor of those with more stable cash flows. In Health Care, the performance of
this traditionally defensive sector struggled with regulatory and political
uncertainties that have overwhelmed the strong financial performance of many
individual companies. Finally, our ongoing concerns about the multiple risks
confronting the Financial Sector including real estate lending, derivatives
exposure and system wide leverage have driven us to a low portfolio weight in
the sector, which was one of the stronger performing sectors once the Fed
paused and the market became convinced that a rate cut was on the horizon.





Market Outlook:

The "Goldilocks" scenario prevalent in the market seems unlikely to us.
Although we worry that the year's returns resulted from an overly optimistic
market outlook, we are confident that the fundamentals of our investment
holdings will yield a satisfactory total return given the current economic
scenario.

Due to our concern about economic prospects in we will continue
restructuring the portfolio with a focus toward defensive areas of the economy.
In the months ahead, we look to buy companies likely to thrive in a more
difficult global economic environment. We also believe that our emphasis on
consistent and sustainable free cash flow generation will act as a buffer in
market downturns and will provide positive returns through dividends, stock
buybacks and debt reduction over the long run.

The Fund continues to be invested consistent with our investment philosophy. We
look for companies that demonstrate consistent and growing Free Cash Flow. We
require that they are run by management who are committed to creating
shareholder value from this cash by reinvesting profitably in the business,
making accretive acquisitions, or returning the money to shareholders through
dividends, share buybacks, or debt reduction. We strive to pay low valuations
for these profitable businesses, which from a bottom-up stock selection process
helps drive our sector weightings. A great company run by great management for
the benefit of shareholders will be a great investment regardless of the type
of business it happens to be. Over time, we do expect this bottom-up
fundamental stock selection process to be the main driver of our returns.

David N. Pearl
Portfolio Manager

Dear Shareholder:

We are pleased to report the results for Global Equity Shareholder Yield Fund
(the "Fund") first full year. During the period the fund
produced total returns of Returns for the S&P/ Citigroup Broad Market
Index-Global Equity Index (the "BMI") were over the same period. We made
qualified dividend distributions of approximately well in excess of
those made by many other investment vehicles.

In addition to outperforming over the month period, we produced market
beating performance during the global liquidity crisis in May and June. Global
interest rate concerns led to the mid-May meltdown when many markets fell by
double digit amounts. Our expectation that the focus on high quality, dividend
paying companies would perform well in periods of stress proved well founded as
the Fund protected investors from approximately of the losses experienced
by the global markets during the correction (as measured relative to the BMI).

The financial markets more than bounced back as a slow down in the US economy
during the second quarter prompted the US Fed to leave interest rates unchanged
at their August meeting. This pause, the resolution of current tensions in
Lebanon and strong third quarter earnings gave the markets around the globe a
strong tailwind through year end. The Fund's participation in the recovery
exceeded our expectations, rising in the final quarter of

Other notable items included the significant increase in assets under
management in the Fund to from and the positive impact from
mergers and acquisition activities. In early September, AWG plc, a London-based
water utility received a takeover bid from a European buyout fund. The
company's solid execution and modest leverage proved more attractive to private
equity investors than public shareholders. This was an example of our belief
that hedge funds and private equity investors will drive value when markets
and/ or management can not.

As came to a close, the fundamentals for continued success looked good.
Cash balances were a significant portion of total corporate assets. By this
measure, the ability to return capital to shareholders through dividends has
never been greater. While the US economy appears to be slowing as a result of
the many interest rate increases over the past two years, foreign markets are
still growing. The addition of three billion workers and consumers from China,
India and other emerging economies to the global marketplace has had a very
positive impact on global economic growth.

Despite general optimism for we must acknowledge the fragile conditions
in the financial markets. As of the writing of this commentary, the markets
have had two serious tremors. First, in early January investors were
shocked by the realization the Fed might not be finished raising interest
rates. More recently, attempts by the Chinese to reign in speculation in local
shares and the increased scrutiny/ regulation in the US sub-prime markets has
reminded investors just how closely related markets and asset classes have
become. Goldilocks is a fairy tale, not an economic environment. Our response
to the possibility of more volatile financial markets is to re-double our focus
on high quality companies that return substantial cash to their owners on a
current basis. We are also increasing the Fund's regional and industry
diversification. Through





these efforts, we believe the Fund will be well positioned should the financial
markets prove difficult and less rewarding than in recent years.

William W. Priest
Portfolio Manager

Dear Shareholder:

We are pleased to submit to you the shareholder report for Dividend Capital
Realty Income Fund for the period October to December The
Board of Directors of the fund approved a change in the fund's fiscal and tax
year end from September to December We believe this change will
better facilitate the achievement of the fund's objectives.

Over this period, we believe the fund has performed according to its objectives
and in our opinion, offers a unique investment proposition in the universe of
real estate focused mutual funds. Against a backdrop of heightened market
volatility, a changing interest rate environment and shrinking investment
yields in the U.S. Equity REIT market, we believe the fund's structure, which
currently combines meaningful exposure to common stock of U.S. and Canadian
real estate companies as well as preferred stock of U.S. real estate companies,
offers a portfolio with the potential to deliver attractive current income and
capital appreciation potential with lower volatility as compared to real estate
common stock focused funds. As market conditions change, we believe this
diversified structure also enhances our ability to adjust the portfolio through
market cycles with an asset allocation and security selection process that
offers more strategic alternatives than your typical REIT fund.

Market Conditions

During the period September through December the real estate
securities market continued to perform strongly, as indicated by the
return of the MSCI U.S. REIT Index ("RMS") and return of the Merrill
Lynch REIT Preferred Index ("MLRP"). In our opinion, the main catalysts for the
performance were merger and acquisition activity and appreciation of private
real estate values. In addition, real estate fundamentals have continued to
improve, providing additional support to the values of publicly traded real
estate companies. Of the major property types, top performing sectors within
the universe of common stocks were Office- Central Business District and
Regional Malls. The Apartment sector, while delivering positive performance,
generally lagged the broader REIT market. Preferred stocks, as represented by
the MLRP, generally had less volatility during the period while delivering an
attractive income stream.

Performance Analysis

Consistent with the fund's structure and primary objective of income the fund
has a competitive trailing yield of (based on A-share
at NAV). On a total return basis, the fund increased (A-share at NAV)
over the period and annualized since the inception of the fund
A-share at NAV) while demonstrating significantly lower volatility
than common stock oriented benchmarks and competitive funds. This total return
and volatility profile is consistent with our expectations.

The fund's total return was most influenced by our allocation decision among
common and preferred stock. During the period, the fund was over weighted
towards real estate preferred stocks. The allocation decision, which supported
the fund's yield potential and lowered the volatility, decreased the overall
capital appreciation realized by the fund.

Within the common stock portion of our portfolio, positive performance was
contributed by our Hotel and Regional Mall investments. Performance was
hampered by our exposure to the Industrial property sector.



The preferred stock portion of our portfolio was focused on higher-yielding
non-investment grade and unrated issues in which our analysis of the underlying
company cash flows and the capital structure of the issuing company indicated
sufficient cash flow and common equity to support the coupon. We continue to
believe these securities offer compelling investment opportunities.

Outlook

To achieve the fund's objectives, we strongly believe the current market
environment underscores the importance of having a diversified, multi-asset
real estate strategy. Short-term market movements will offer opportunities to
adjust asset allocation between U.S. and Canadian REIT equity and preferred
stocks. The fund will aim to take advantage of compelling relative value
between asset classes and re-priced investment yields. We believe the ability
to make these adjustments from more than one asset class should help to create
both positive relative risk-adjusted performance and absolute performance. With
our focus on the quality, sustainability and (where relevant) growth potential
of underlying property cash flows, we remain confident in our continued ability
to identify and capitalize on attractive investment opportunities across the
real estate asset class. Thank you for your continued support.

Charles Song, Senior Portfolio Manager

Dear Shareholder:

The REMS Real Estate Value-Opportunity Fund (the "Fund") ended the fiscal and
calendar year with a period of strong positive performance, returning for
the six month period ending vs. for the S&P index. The
Fund's differentiated portfolio structure included at fiscal period end
common equity holdings of value-oriented REITs and real estate operating
companies; a position in REIT preferred equity securities that offer
stability and high current dividend yield; and a short position in real
estate equities intended to protect the portfolio against areas of over
valuation and credit risk.

While returns and fund flows to the public real estate sector remain strong,
risk levels are rising with valuations. The Fund's portfolio structure is
designed to offer above-average dividend yield with lower volatility than the
REIT indices while capturing real estate value creation upside. The Fund's real
estate equities recorded a total return for the six month period ending
compared to for the NAREIT Equity REIT Index. The REIT preferred
position contributed total return compared to for the Lehman
Government Corporate Bond Index, while the short position produced a
return. REIT share prices continue to be supported by strong fund flows into
the sector with high volatility and performance concentrated in the largest
capitalization names. The continued privitazation of public companies by
leveraged private buyers appears to be driving real estate pricing levels to
new highs, particularly in the office sector.

REMS believes maintaining its strict value, yield-advantage investment process
is more important than ever in today's public real estate market.

Edward W. Turville, CFA
