Dear Fellow Shareholders:
January 25, 2007
The gains in equities were broad-based in 2006, with leadership coming from the value, small capitalization and international sectors. The market, as measured by the S&P 500 Index, gained 15.78% during the year. The strongest sectors were telecommunications, medical and financial services while the retail sector fared the worst.
The fixed income market, as measured by the Lehman Brothers Gov't/Credit Bond Index, was up 3.78% in 2006. After a two-year period, which included 17 meetings of increasing the federal funds rate from 1.00% to 5.25%, the year 2006 finished with four consecutive meetings of "no-action." The pause was refreshing to the markets in that this action suggested confidence in the Federal Reserve's resolve to contain inflation.
The financial markets basically moved in three stages during the past year. In the first stage, from January through May, markets moved higher reflecting solid economic growth and a pause to the Federal Reserve tightening. Second stage, May through August, reflected a weakened market on investor fears of the growing risk of global inflation pressures as commodity prices surged. In the third stage, markets moved higher as inflation readings moderated and corporate profits exceeded expectations.
2007 Outlook
Energy prices appear to be a more important component to higher market prices in 2007. Further Federal Reserve rate cuts do not appear necessary for price/earning multiples to move higher. If the Federal Reserve does lower rates, this would contribute to an already bullish market outlook which includes moderate inflation, weaker energy prices and higher productivity growth that could boost corporate profits. However, on a short-term technical basis, the market appears to be over bought, suggesting that we may have to experience a consolidation phase before the markets move higher.
Conclusion
We expect equities to outperform other asset classes over the next year. Catalysts include solid fundamentals, moderate economic growth and higher business investment that should partially offset slower growth trends in personal consumption. The recent weakness in housing and autos is expected to moderate due to full employment and record levels of household net worth.
We believe the most influential macro variable in 2007 will be oil prices. It wasn't until oil peaked in mid-August that equity prices significantly rallied. Lower oil prices should add cash in the pockets of consumers, keeping a lid on inflationary expectations and removing the likelihood of further Federal Reserve rate hikes.
The risks?  World instability leading to higher energy prices and concerns about a possible economic recession.
I'd like to thank you for your investment in the Monetta Funds. We look forward to a prosperous 2007.
Sincerely,


