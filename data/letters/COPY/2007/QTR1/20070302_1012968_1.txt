Dear Investor
Investors enjoyed another good year in 2006 as stocks benefited from strong profit growth, a durable economic expansion, and, most notably, large pools of liquidity coursing through the world financial system. The latter had some
unusual effects on the character of the market’s advance, however. Many companies became the subject of takeover speculation, which sent their shares jumping, and others felt the impact of volatile commodity prices, which soared and then
plunged as speculators moved in and out of the markets. In our opinion, these factors took the spotlight off the highest-quality firms. Our fund enjoyed a gain in this environment, but mid-cap growth stocks lagged other classes, and our results
lagged our benchmarks.
The Institutional Mid-Cap Equity Growth Fund returned 5.58% during the past six months and 6.89% for the year. Our performance relative to the benchmarks reflected our emphasis on traditional growth sectors, such as technology and
health care. These fared poorly in relation to traditional value sectors, such as utilities, materials, and real estate investment trusts (REITs). The lattermost played an important role in boosting index returns, even in the growth benchmarks.

