Dear Shareholder:

The Berwyn Funds experienced another successful year in Our largest fund,
the Berwyn Income Fund (BIF) rose percent on a total return basis while
maintaining a portfolio heavily weighted with conservative fixed income
investments. The Berwyn Fund (BF), our small-cap fund, had a total return of
percent. The BF has had a series of excellent years in the recent past and,
after a sluggish start in finished the year with a strong fourth quarter.
The Berwyn Cornerstone Fund (BCF), which invests in mid and large-cap stocks,
advanced percent on a total return basis. The BCF was able to take
advantage of an increased interest in larger capitalization companies on the
part of institutional investors.

In general, the financial markets performed well during In spite of the
fact that interest rates rose, energy prices remained high and the war in Iraq
worsened, the U.S. economy continued to grow at a reasonable pace. The Standard
and Poor's Index rose percent reflecting the strength in many
large-cap stocks, including those in the Telecommunications and Energy
industries. However, the long-term boom in Residential Housing clearly came to
an end and companies in that industry saw their stock prices continue the
weakness that began in Amidst changing and more stringent government
regulations, the Health Care industry also struggled and many Consumer stocks
had a difficult time as higher gasoline prices and other energy related expenses
put financial pressure on middle and lower income families.

Despite the facts that the major indices did well last year and that the
problems cited above have not disappeared, I am optimistic about The
reason for my optimism is that in beneath the external performance of the
averages, divergences occurred that created investment values that did not exist
in I believe that our research team has taken advantage of the
opportunities offered by the marketplace and that The Berwyn Funds are well
positioned. If the economy remains healthy, these new additions to our
investment portfolios should provide the platform for a successful

On balance, the BIF remains conservatively postured as far as its fixed income
investments are concerned, but continues to invest in a number of attractive,
dividend paying common stocks that could bolster the more predictable returns
expected from the fixed income portion of the portfolio.

After several above average return years, the managers of the BF found it
necessary to make significant changes to reinvigorate the portfolio and I
believe that this Fund has the proper risk to reward characteristics to maintain
the momentum it experienced in last year's fourth quarter. As you may recall,
the BF was temporarily closed to new investors in early January of last year.
However, due to the availability of more attractive small-cap stocks, the Board
of Trustees voted to reopen the Fund this past December.





Last year your Board of Trustees voted to allow the BCF to invest a portion of
its assets in large-cap stocks as well as mid-cap stocks. This change gives the
Fund managers more flexibility and has permitted them to take advantage of what
proved to be a number of excellent values in the large-cap sector this past
year. These situations should continue to positively impact this year's
performance.

From an operational standpoint, The Berwyn Funds also performed well. Our
research staff remained unchanged, expense ratio goals were met, assets under
management remained at healthy levels and there were no accounting, or Trust
governance, issues. At the foundation of all our work is a strict adherence to a
"bottom up" fundamental approach that we believe results in long-term "growth
through value."

Finally, I thank you for your support and confidence.

Very truly yours,

Dear Berwyn Fund Shareholder:

Berwyn Fund's (BF) net asset value per share fell from to for the
year However, after accounting for a year-end distribution in the amount
of the BF registered a percent total return gain for the
twelve-month period. Most of the distribution, qualified as a long-term
capital gain, while only was categorized as ordinary income.

Last year's gain came after three consecutive years of double digit total
returns and after six years of outperforming our benchmark index, the Russell
Unfortunately, the need to reinvigorate the portfolio, the absence of
compelling values at the beginning of and some modest missteps prevented us
from outperforming either our benchmark index or the major stock market averages
during the first nine months of the year. This situation began to reverse in the
fourth quarter as the performance of a number of our undervalued holdings
started to improve. During the Russell Index had a total return of
percent.

During the entirety of this past year we focused on eliminating, or reducing,
our exposure to stocks where the risk to reward characteristics were no longer
in our favor. Stocks eliminated from the portfolio included: Cooper Tire and
Rubber Co., Drew Industries, Duquesne Light Holdings, EPIQ Systems, Inc.,
Esterline Corp., International Coal Group, Inc., Russell Corp. and Westmoreland
Coal Co. In addition, partial positions were sold at a profit in the following
stocks: COHU, Inc., Callon Petroleum Co., IHOP Corp., LaBranche & Co., Inc., MTS
Systems Corp., Neoware, Inc., Southwestern Energy Co. and Vital Signs, Inc. In
but particularly in the second half after the market suffered a sinking
spell, we began to become more aggressive buyers of stocks that we believed had
fallen into our range of good value. New purchases included: CFS Bancorp,
Chiquita Brands International, Gold Kist, Inc., Hooker Furniture Corp.,
Input/Output, Inc., Monaco Coach Corp., and Wabash National Corp.

==========================================

BERWYN FUND
TOTAL INVESTMENTS AND CASH
(% OF TOTAL INVESTMENTS)
DECEMBER (UNAUDITED)

Dear Berwyn Income Fund Shareholder:

The total return to shareholders of the Berwyn Income Fund (BIF) for was
percent. Net asset value per share increased from on December
to on December Quarterly dividends of were
distributed from net investment income, compared to in After a
lackluster first half, the U.S. financial markets rallied later in the year,
providing BIF shareholders with a solid return. Confidence returned that the
Federal Reserve Board could maintain a gradual economic expansion in spite of a
faltering housing market and that inflation would stay moderate.

In keeping with its objective to generate income while preserving capital in
real terms, BIF maintained a diversified portfolio of fixed income securities as
well as dividend-paying common stocks. The rally in the second half of the year
led to strong annual returns for equities and high yield bonds; however,
high-grade corporate and government bond prices were little changed for the
year. These results are reflected in our reference indices. The Lipper Income
Fund Index, our only reference index with an equity component, produced a total
return of percent, while the Merrill Lynch High Yield Master II Index and
the Citigroup Broad Investment Grade Bond Index produced total returns of
percent and percent, respectively. The Citigroup High Yield Composite
Index, another measure of the performance of lower rated bonds, was up
percent on a total return basis.

==========================================

BERWYN INCOME FUND
TOTAL INVESTMENTS AND CASH
(% OF TOTAL INVESTMENTS)
DECEMBER (UNAUDITED)

Dear Berwyn Cornerstone Fund Shareholder:

Berwyn Cornerstone Fund's (BCF) net asset value per share increased from
to for the year ended December Including a long-term capital
gain of per share, a short-term capital gain distribution of per
share and an income dividend of the Fund's total return for the year
was percent.

BCF's relative performance was strong for the year, outperforming its primary
benchmark, the S&P Midcap Index, as well as the S&P Index, which rose
percent and percent, respectively, on a total return basis. The Dow
Jones Industrial Average, which rose percent during the year, is the only
widely followed domestic stock index that outpaced BCF in

==========================================

BERWYN CORNERSTONE FUND
TOTAL INVESTMENTS AND CASH
(% OF TOTAL INVESTMENTS)
DECEMBER (UNAUDITED)
