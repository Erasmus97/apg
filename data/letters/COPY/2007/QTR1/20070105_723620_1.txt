Dear Fellow Shareholder,

The bond market has been nothing if not theatrical over the past months. The
pressures of the Federal Reserve's monetary tightening campaign, the mixed
messages sent by conflicting economic releases, and the changing of the guard at
the Fed all at times roiled the fixed income markets in dramatic fashion.
Finally, the Fed's decision to hold rates steady gave investors the
denouement--and price gains--they were hoping for.

Starting the fiscal year, the backdrop for bonds seemed less than supportive.
After all, bonds had already enjoyed a multi year boom, driven by ample
liquidity and the long coattails of loose monetary policy. In the wake of the
Fed's efforts to raise interest rates, which began in June investors in
long bonds had at times seemed indifferent to the pressure. However, by last
fall, the campaign was having an impact across maturities, and investors
wondered how long the Fed would need to keep raising interest rates to curb
inflation.

As such, the bond market spent the first half of the reporting period
focusing obsessively on economic data, which by turn seemed to suggest economic
softening and easing inflation, on the one hand, and economic rigor and rising
prices, on the other. Investors also faced the challenge of interpreting the new
Fed chairman, Ben Bernanke, which added to market volatility for a time.

Still, bond investors became relatively confident that the Fed's campaign would
have its expected slowing impact--a sentiment that was reflected in the bond
yield curve's flat or inverted shape for much of the fiscal year. (An inverted
yield curve is often seen as a harbinger of economic weakness.) In June, after
the Fed raised the Fed Funds rates for the consecutive time, the bond
market anticipated the central banks decision in August to freeze its long
tightening campaign, launching an impressive rally that lasted for the balance
of the fiscal year.

Of course, now the central question is whether investors have moved too far too
fast, since implied in all this excitement is confidence that the Fed will soon
begin CUTTING rates to shore up the economy.

As this drama has unfolded, our fixed income managers have taken a more




NEUBERGER BERMAN OCTOBER

level-headed view. Naturally, they are cognizant of the central goals of fixed
income investing: to provide stability to diversified portfolios, to preserve
capital, and to provide predictable income. For much of the past months, they
have focused on protecting portfolios from the effects of interest rate swings
through careful management of duration and a focus on credit quality. And
although they've taken advantage of the opportunities presented by the rally,
they are skeptical about its pace and intensity and are remaining cautious--a
sign of thoughtful perspective that should come as no surprise to our long-term
investors.

Sincerely,

Dear Shareholder,

We are pleased to present to you this annual report for the Lehman Brothers Core
Bond Fund for the period from November through October The
report includes portfolio commentary, a listing of the Fund's investments, and
their audited financial statements for the reporting period.

The Fund seeks to maximize total return through a combination of income and
capital appreciation. We endeavor to add value with a distinct process for
managing the portfolio's positioning with respect to interest rates, credit and
volatility.

The managers have remained cautious in the face of continued uncertainty in the
bond markets, but positioned the portfolio to take advantage of the bond rally
that occurred in the second half of the reporting period. In their efforts, the
goal of the managers has been to protect client principal and add
opportunistically to return.

Thank you for investing in the Lehman Brothers Core Bond Fund. We appreciate the
trust you have placed in us and we will do our very best to continue earning it.

Sincerely,

Dear Shareholder,

I am pleased to present to you this annual report for the Neuberger Berman
Strategic Income Fund for the period ended October The report includes
portfolio commentary, a listing of the Fund's investments, and its audited
financial statements for the reporting period.

The portfolio represents a carefully considered mix of income-producing
securities, which include real estate investment trusts (REITs), investment
grade debt, high yield securities, foreign securities, and other dividend-paying
equities. The Fund's return over the reporting period demonstrates the potential
benefits of investing in a diverse portfolio of income-producing securities
managed by investment professionals who are experts in their area of focus.
Oversight from an Asset Allocation Committee, whose primary responsibility is to
tactically and strategically adjust the portfolio's exposure to each of these
market sectors, offers additional flexibility to pursue long-term returns in
what we find to be the most attractive segments.

Over the past months, investors have paid special attention to economic
signals and how they might affect Federal Reserve interest rate policy. Early
on, this focus at times led to disappointment for stock and bond investors alike
as economic growth remained exceptionally strong and inflation trends,
threatening. However, by August the Fed was confident enough in the cumulative
effect of its tightening campaign to hold rates steady, which has provided fuel
to an impressive capital markets advance.

Of course, it's always worth remembering that markets can get ahead of
themselves, and our managers have, naturally, taken a level headed view of what
has unfolded. While taking advantage of the opportunities presented by the
rally, they have also been carefully managing duration and maintaining a focus
on quality to help protect shareholder capital.

Thank you for your confidence in Neuberger Berman. We will continue to do our
best to earn it.

Sincerely,

Dear Shareholder,

We are pleased to present to you the fiscal Annual Report for the Lehman
Brothers Municipal Money Fund and the Lehman Brothers New York Municipal Money
Fund for the period ended October The former of the two has a long and
distinguished track record, having been created in while the latter is
relatively new and was introduced on December Both, however, employ
the same research driven, value oriented investment methodology and are in the
capable hands of Portfolio Co-Managers Janet Fiorenza, William Furrer and Kelly
Landron.

The Funds' investment objectives are simple and straightforward--seeking the
highest available current income exempt from federal income tax (and for the
Lehman Brothers New York Municipal Money Fund, New York State and City income
taxes as well) through investment strategies consistent with the highest
standards for safety and liquidity. These strategies include active weighted
average maturity management (positioning the portfolio in the most
advantageous points on the yield curve), sector allocation (taking advantage
of opportunistic pricing in the different sectors of the municipal securities
markets) and, most importantly, security selection. For more than years we
have been investing in municipal money markets and we have developed an
extensive national network of regional brokers who provide considerable
assistance in identifying high quality attractively priced securities. We
also do our own internal credit analysis on every issue we invest in. We
believe our ongoing commitment to assessing the credit quality of potential
investments will continue to reward our shareholders.

During the fiscal year, both Funds enjoyed strong cash inflows. This is
certainly in part due to the significantly more attractive yields resulting from
rising short-term interest rates. However, we have also been winning over
individual and institutional investors who appreciate our experience and
expertise in municipal money markets. We welcome our many new shareholders and
reaffirm our commitment to getting the most out of municipal money markets while
adhering to conservative, "safety first" investment principles.

Sincerely,

Dear Shareholder,

We are pleased to present to you the fiscal Annual Report for the National
Municipal Money Fund and the Tax-Free Money Fund for the period ended October
Introduced on December both Funds are relatively new, but
their older sibling, the Lehman Brothers Municipal Money Fund, was one of the
first funds of its kind and has a distinguished track record. All Lehman
Brothers municipal money funds are in the capable hands of Portfolio Co-Managers
Janet Fiorenza, William Furrer and Kelly Landron and employ the same research
driven, value oriented investment methodology.

The Funds' investment objectives are simple and straightforward--seeking the
highest available current income exempt from federal income tax through
investment strategies consistent with the highest standards for safety and
liquidity. These strategies include active weighted average maturity management
(positioning the portfolio in the most advantageous points on the yield curve),
sector allocation (taking advantage of opportunistic pricing in the different
sectors of the municipal securities markets) and, most importantly, security
selection. Over the many years that we have been investing in municipal money
markets, we have developed an extensive national network of regional brokers who
provide considerable assistance in identifying high quality, attractively priced
securities. We also do our own internal credit analysis on every issue we invest
in. We believe that our ongoing commitment to assessing the credit quality of
potential investments will continue to reward our shareholders.

During the fiscal year, both Funds have enjoyed strong cash inflows. This is
certainly in part due to the significantly more attractive yields that have
resulted from rising short-term interest rates. However, we have also been
winning over investors who appreciate our experience and expertise in the
municipal money markets. We welcome our many new shareholders and reaffirm our
commitment to getting the most out of municipal money markets while adhering to
conservative, "safety first" investment principles.

Sincerely,
