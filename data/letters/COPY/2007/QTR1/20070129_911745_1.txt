Dear Shareholder:


We are pleased to present this annual report for Dreyfus Pennsylvania Intermediate Municipal Bond, covering the period from December through November

Although reports of declining housing prices have raised some economic concerns, we believe that neither a domestic recession nor a major shortfall in global growth is likely.A stubbornly low unemployment rate suggests that
labor market conditions remain strong, and stimulative monetary policies over the last several years have left a legacy of ample financial liquidity worldwide.These and other factors should continue to support further economic expansion, but at a
slower rate than we saw earlier this year.

The U.S. bond market also appears to be expecting a slower economy, as evidenced by an inverted yield curve at the end of November, in which yields of two-year U.S.Treasury securities were lower than the
overnight federal funds rate.This anomaly may indicate that short-term interest rates have peaked, while the Federal Reserve Board remains on hold as it assesses new releases of economic data. As always, we encourage you to discuss the
implications of these and other matters with your financial advisor.

For information about how the fund performed during the reporting period, as well as market perspectives, we have provided a Discussion of Fund Performance given by the funds portfolio manager.

Thank you for your continued confidence and support.









DISCUSSION OF FUND PERFORMANCE

Douglas Gaylor, Portfolio Manager

How did Dreyfus Pennsylvania Intermediate Municipal Bond Fund perform relative to its benchmark?

For the period ended November the fund achieved a total return of In comparison, the funds benchmark, the Lehman
Brothers Municipal Bond Index, achieved a total return of for the same In addition, the average total return for all funds reported in the Lipper
Pennsylvania Intermediate Municipal Debt Funds category was for the reporting

Despite heightened market volatility during the first half of the reporting period, stabilizing interest rates and robust investor demand generally supported higher municipal bond prices over the second half.The fund
produced a higher return than its Lipper category average, primarily due to its emphasis on bonds at the longer end of the intermediate-term range. However, the fund lagged its benchmark, which contains bonds from many states, not just Pennsylvania,
and does not reflect fund fees and expenses.
