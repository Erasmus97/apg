Dear Shareholder:
Performance Summary
11/01/05  10/31/06
 
Market Review: A Bumpy Upward Ride
The period ended October 31, 2006, was volatile. Early in the period, the Japanese stock market rose sharply, supported by good business results and a recovery in domestic economic indicators. The overall stock market continued upward until April despite the Bank of Japan ending their quantitative easing policy and a sharp drop in small and mid-cap stock prices due to the "Livedoor Shock" (the plunge in Japan's stock market caused by the raid of Internet company Livedoor's headquarters and the subsequent arrest of its president for securities law violations) early in 2006. However, toward the middle of June, the market fell following a sharp rise in the yen, surging crude oil prices, concerns of inflation, and increasing uncertainty regarding the U.S. economic outlook.
Subsequently, the Japanese market rose again as global stock markets stabilized under growing expectations of the Federal Reserve's pause in interest rate increases. Robust earnings from some of the larger caps drove the market even higher. Investors clearly felt that the strong corporate earnings of Japanese companies was reason enough to buy. Consequently, stock prices edged steadily upward with the Tokyo Stock Exchange Index (TOPIX), the Fund's benchmark, closing up at 1617.42 points.
Strategic Review and Outlook: Looking for Innovation
The Fund underperformed its benchmark for the year ended October 31, 2006. Contributing to performance was an overweight in non-ferrous metals, which rose 22.78% for the period. Additionally, the Fund avoided certain underperforming stocks, further aiding performance. These positive factors, however, were offset by the Fund's overweight in the negatively performing services and miscellaneous finance sectors, and its underweight in the positively performing real estate sector.
1
Credit Suisse Japan Equity Fund
Annual Investment Adviser's Report (continued)
October 31, 2006 (unaudited)


