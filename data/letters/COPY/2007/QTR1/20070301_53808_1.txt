Dear Shareholder:

We are pleased to present this annual report for Dreyfus Premier Core Value Fund, covering the period from January through December

proved to be a good year for the financial markets.Virtually all sectors and capitalization ranges of the U.S. equity markets generated strong returns, especially over the second half of the year.A number of positive
factors contributed to the markets gains in including an expanding domestic economy, subdued inflation, stabilizing interest rates, rising productivity and robust corporate profits.

In our analysis, provided an excellent reminder of the need for a long-term investment perspective. Adopting too short a time frame proved costly for some investors last year, as chasing recent winners often meant
buying the next months losers. Indeed, history shows that reacting to near-term developments with extreme shifts in strategy rarely is the right decision.We believe that a better course of action is to set a portfolio mix to meet future goals,
while attempting to ignore short term market fluctuations in favor of a longer-term view.

For information about how the fund performed during the reporting period, as well as market perspectives, we have provided a Discussion of Fund Performance given by the funds portfolio manager.

Thank you for your continued confidence and support.We wish you good health and prosperity in











DISCUSSION OF FUND PERFORMANCE

Brian Ferguson, Portfolio Manager

How did Dreyfus Premier Core Value Fund perform relative to its benchmark?

For the period ended December Dreyfus Premier Core Value Fund produced total returns of for its Class A shares, for its Class B shares, for its Class C shares, for its Class R
shares, for its Class T shares and for its Institutional In comparison, the funds benchmark, the Russell Value Index, produced a total
return of for the same The funds previous benchmark, the S&amp;P Value Index, achieved a total return of for the same


Stock prices generally rose in primarily due to strong corporate earnings in a growing U.S. economy.The funds returns were slightly lower than the Russell Value Index as a result of attractive results from
the consumer discretionary and information technology sectors. Furthermore, the funds returns were in line with the S&amp;P Value Index primarily due to strong performance in information technology and industrials
sectors.

What is the funds investment approach?

The fund invests primarily in large-cap companies that are considered undervalued based on traditional measures, such as price-to-earnings ratios. When choosing stocks, we use a bottom-up stock selection
approach, focusing on individual companies, rather than a top-down approach that forecasts market trends. We also focus on a companys relative value, financial strength, sales and earnings momentum and likely catalysts that could
ignite the stock price.

What other factors influenced the funds performance?

The U.S. stock market ended with double-digit gains, as investors remained hopeful regarding the strength of corporate earnings despite mixed economic signals, such as volatile energy prices and cooling hous-









ing markets.The market rallied particularly strongly over the second half of the year, when an uneventful hurricane season and warm weather in many parts of the United States contributed to a decline in energy prices,
giving relief to inflation worries. In addition, after more than two years of steady rate hikes, the Federal Reserve Board refrained from raising short-term interest rates over the second half of the year. In this environment, stock prices rose as
U.S. companies continued to report healthy earnings and mergers-and-acquisitions activity increased.

A successful stock selection strategy in the consumer discretionary sector contributed significantly to the funds relative performance during the reporting period, with particularly strong results coming from media
giants News Corp., which was sold during the reporting period, and Walt Disney. News Corp. prospered amid better results from Fox News, anticipation of a spin-off of its Sky Italia unit and the acquisition of MySpace.com.Walt Disney rose in the wake
of successful cost control measures, improved profit margins from its theme parks and stronger results from its television and film businesses. Conversely, the fund held no shares of Viacom, where results fell short of expectations. Finally, the
fund benefited from its relatively light holdings of homebuilders, which saw their financial results deteriorate in a slowing housing market.

The funds investments in the information technology sector also fared relatively well. Broadband equipment maker Cisco Systems gained value due to a general increase in broadband usage and expectations of greater
corporate demand for its products. Computer and printer maker Hewlett Packard, helmed by new management, gained market share in a variety of product areas, and consulting firm Accenture benefited from higher levels of outsourcing and corporate
spending.

On the other hand, some disappointments detracted from the funds relative performance. In the financials sector, the fund did not participate as fully as the benchmark in strong returns from real estate investment
trusts, and our emphasis on lagging insurance companies detracted from performance. For example, Genworth Financial was pressured by concerns regarding its long-term care and mortgage insurance businesses, as well as its former parents move to
divest its remaining stake in the com-









pany. In the health care sector, the funds results were hurt by relatively light exposure to drug developer Merck &amp; Co., which was sold during the reporting period, and which rose as legal concerns diminished and
research &amp; development prospects improved. Shares of medical products maker Boston Scientific, which were eventually sold during the reporting period, declined due to merger integration issues and concerns regarding low reimbursement policies by
Medicare and other insurers.

What is the funds current strategy?

We have continued to rely on our bottom-up stock selection process, as we believe it to be an effective method of identifying attractively valued stocks under a variety of market conditions.We have continued to find
attractive values in traditionally defensive areas, including the consumer staples sector and the homebuilding and retail industries within the consumer discretionary sector. Information technology stocks remain attractive to us, largely due to the
ongoing boom in Internet demand.While in the past we have found attractive opportunities in the energy area, we recently reduced our emphasis on the sector due to the possible effects of recent warmer weather and shifting supply-and-demand
influences on commodity prices.

January








Total return includes reinvestment of dividends and any capital gains paid, and does not take into

Dear Shareholder:

We are pleased to present this annual report for Dreyfus Premier Limited Term High Yield Fund, covering the period from January through December

proved to be a year of low volatility in the U.S. bond market.Yields of Treasury securities remained within a relatively narrow range of just basis points, making the third least volatile bond market
since a number of developments during the year might have suggested otherwise, including mounting economic uncertainty, volatile energy prices, softening real estate markets, a change in U.S. monetary policy and ongoing geopolitical
turmoil.

Why did fixed-income investors appear to shrug off some of the years more negative influences? In our analysis, investors disregarded near-term concerns in favor of a longer view, looking to broader trends that showed
moderately slower economic growth, subdued inflation, stabilizing short-term interest rates, a flat yield curve and persistently strong credit fundamentals. Indeed, confirmed that reacting to near-term influences with extreme shifts
in investment strategy rarely is the right decision.We believe that a better course is to set a portfolio mix to meet long-term goals, while attempting to ignore short term market fluctuations.

For information about how the fund performed during the reporting period, as well as market perspectives, we have provided a Discussion of Fund Performance given by the funds portfolio manager.

Thank you for your continued confidence and support.We wish you good health and prosperity in


Thomas F. Eggers Chief Executive Officer The Dreyfus Corporation January








DISCUSSION OF FUND PERFORMANCE

David Bowser, Portfolio Manager

Note to Shareholders: On October David Bowser replaced Jon Uhrig as the funds primary portfolio manager. Mr. Bowser has been a portfolio manager of the fund since July


How did Dreyfus Premier Limited Term High Yield Fund perform during the period?

For the period ended December the fund achieved total returns of for its Class A shares, for Class B shares, for Class C shares and for Class R shares.The fund generated aggregate
income dividends of for Class A shares, for Class B shares, for Class C shares and for Class R In comparison, the Merrill Lynch U.S. High
Yield Master II Constrained Index (the Merrill Lynch Constrained Index) achieved a total return of for the same

High yield bonds produced attractive returns in due to sound credit fundamentals and generally positive supply-and-demand factors. The fund produced lower returns than its benchmark, primarily due to our emphasis on
securities toward the higher end of the high yield markets credit-rating spectrum, which generally underperformed more speculative investments.

What is the funds investment approach?

The fund seeks to maximize total return, consisting of capital appreciation and current income.The average effective maturity of the fund is limited to a maximum of years.

At least of the funds assets are invested in fixed-income securities that are rated below investment grade (high yield or junk bonds) or are the unrated equivalent as determined by Dreyfus.
Individual issues are selected based on careful credit analysis.We thoroughly analyze the business, management and financial strength of each of the companies whose bonds we buy, then project each issuers ability to repay its debt.

The Fund







DISCUSSION OF FUND PERFORMANCE (continued)

What other factors influenced the funds performance?

The high yield market remained persistently strong in supported by domestic and international investors who maintained a relatively high tolerance for risk in their search for high levels of current income. Robust
demand was met during much of the year with a relatively light supply of newly issued securities, as a number of issuers turned instead to bank loans for financing. Several large leveraged buy-out transactions created a brief surge in supply at the
end of the year, enabling to set a new record for high yield bond issuance. However, even the spike in new supply near year-end was easily absorbed by the market.

In addition, the high yield market benefited from robust credit fundamentals, with defaults continuing to hover near historical lows. Although investors heightened inflation and interest-rate concerns sparked a
temporary correction in the spring, the market bounced back when it became apparent that cooling housing markets, falling energy prices and less robust employment gains were likely to produce a gradual economic slowdown, potentially keeping
inflation in check.The Federal Reserve Board lent credence to this view when it refrained from raising short-term interest rates over the second half of the year, its first pauses after more than two years of rate hikes that sent the overnight
federal funds rate to .

However, yield differences along the credit-rating spectrum began at below-average levels and continued to narrow as the market rallied, suggesting to us that a relatively defensive investment posture might be prudent
in a slowing economic environment. Accordingly, we focused on securities in the markets upper and middle rating tiers, including bonds issued by regulated industries, such as utilities, banks and real estate investment trusts.This stance
limited the funds participation in the rally among lower-tier credits, accounting for its lagging performance relative to its benchmark.

We also maintained an underweight position in the financially troubled automotive sector, which proved to be among the markets top performing areas in also held comparatively few bonds from cable and









media companies, some of which were distressed. Nonetheless, cable and media bonds generally performed well.

The fund achieved better results in other areas. In the wireline telecommunications industry, a number of the funds holdings received credit-rating upgrades during the fourth quarter as the industry consolidated.
Bonds from U.S. tobacco companies also fared well in an improving legal environment.

What is the funds current strategy?

While we have seen little evidence of an end to the positive supply-and-demand factors that have supported high yield bond prices, we remain cautious regarding the possibility that unexpected developments could trigger a
sharp sell-off, especially in the wake of the markets sustained rally in Indeed, new issues recently coming to market have tended to fall toward the lower end of the credit range, which may be a sign of frothiness in the
market.Therefore, we have continued to focus primarily on bonds from higher-quality issuers that, in our analysis, demonstrate relatively strong credit characteristics. At the same time, we have added opportunistically to some previously
underexposed areas including the wireless telecommunications, gaming and theater industries in an attempt to boost the funds yield without substantially reducing its credit profile.

January








Total return includes reinvestment of dividends and any capital gains paid, and does not take into

Dear Shareholder:

We are pleased to present this annual report for Dreyfus Premier Managed Income Fund, covering the period from January through December

proved to be a year of low volatility in the U.S. bond market.Yields of Treasury securities remained within a relatively narrow range of just basis points, making the third least volatile bond market
since a number of developments during the year might have suggested otherwise, including mounting economic uncertainty, volatile energy prices, softening real estate markets, a change in U.S. monetary policy and ongoing geopolitical
turmoil.

Why did fixed-income investors appear to shrug off some of the years more negative influences? In our analysis, investors disregarded near-term concerns in favor of a longer view, looking to broader trends that showed
moderately slower economic growth, subdued inflation, stabilizing short-term interest rates, a flat yield curve and persistently strong credit fundamentals. Indeed, confirmed that reacting to near-term influences with extreme shifts
in investment strategy rarely is the right decision.We believe that a better course is to set a portfolio mix to meet long-term goals, while attempting to ignore short term market fluctuations.

For information about how the fund performed during the reporting period, as well as market perspectives, we have provided a Discussion of Fund Performance given by the funds portfolio manager.

Thank you for your continued confidence and support.We wish you good health and prosperity in




Thomas F. Eggers




Chief Executive Officer




The Dreyfus Corporation




January











DISCUSSION OF FUND PERFORMANCE

Kent Wosepka, Portfolio Manager

How did Dreyfus Premier Managed Income Fund perform relative to its benchmark?

For the period ended December Dreyfus Premier Managed Income Fund produced total returns of for Class A shares, for Class B shares, for Class C shares and for Class R
In comparison, the Lehman Brothers U.S. Aggregate Index (the Index), the funds benchmark, produced a total return of for the same period.


After producing relatively lackluster returns over the first half of the year, the bond market rallied over the second half as short-term interest rates stabilized and economic growth slowed.The funds Class A and R
shares produced higher returns than its benchmark, which we attribute primarily to the performance of the funds high yield corporate bond, investment-grade corporate bond and asset-backed securities holdings. However, the fund is also subject
to fees and expenses to which the Index is not subject.

What is the funds investment approach?

The fund seeks high current income consistent with what is believed to be prudent risk of capital.The fund invests at least of its total assets in various types of U.S. government and corporate debt obligations rated
investment grade (or their unrated equivalent as determined by Dreyfus).The fund also normally invests at least of its total assets in debt obligations having effective maturities of years or less.We do not attempt to match the sector
percentages of any index, nor do we attempt to predict the direction of interest rates by substantially altering the funds sensitivity to changes in rates. Instead, the heart of our investment process is selecting individual securities that
possess a combination of superior fundamentals and attractive relative valuations.

The Fund







DISCUSSION OF FUND PERFORMANCE (continued)

What other factors influenced the funds performance?

Bond prices declined modestly over the first six months of due to intensifying inflation concerns in a generally robust economic environment. However, investor sentiment improved markedly over the second half of the
year, as U.S. economic growth moderated amid cooling housing markets and more modest employment gains. As a result, following four increases in short-term interest rates over the first half of the year, the Federal Reserve Board (the
Fed) held the overnight federal funds rate steady at at each of four meetings between August and December, its first pauses in more than two years. Investors first anticipated and then reacted favorably to the Feds shift in
policy, and the longer end of the bond market rallied.

Despite the economic slowdown, business fundamentals remained healthy in most industries, generally supporting prices of corporate bonds across the credit-rating spectrum. In the U.S. government securities market, low
levels of volatility helped mortgage-backed securities, asset-backed securities and other high quality, yield advantaged instruments outperform U.S.Treasury securities.

In this environment, the funds positions in high yield bonds helped it participate in the rally among lower-rated credits.At the same time, we attempted to manage the risks of lower-rated securities by focusing on
bonds with relatively short maturities, which we regarded as less likely to be affected by unexpected adverse developments.We also established shorter-maturity positions in the investment-grade corporate bond market, where we avoided issuers that we
believed might be vulnerable to activities that tend to be unfriendly to bondholders, such as leveraged buyouts. Instead, we favored regulated industries where such activities are less common.

The funds holdings of asset-backed securities provided higher yields than U.S.Treasury securities, contributing positively to returns. However, we maintained an underweight position in mortgage-backed securities, a
strategy that detracted modestly from the funds relative performance in the low volatility investment environment. In addition, tactical positions







in Treasury Inflation Protected Securities underperformed the averages when energy prices remained low during the fall, helping to keep a lid on inflation expectations.

The fund also benefited to a degree from our duration management strategy. A modestly short average duration over the first half of helped protect the fund from the potentially adverse effects of rising interest rates,
while a shift in mid-year to a slightly long position helped boost its participation in the market rally during the second half. Despite narrowing yield differences along the markets maturity spectrum, the funds bulleted
yield curve strategy had relatively little impact on performance. Finally, the funds derivative investments generally fared well, including tactical positions in credit default swaps designed to provide protection from declines in specific
markets or issuers.

What is the funds current strategy?

Although high yield and investment-grade corporate bonds have reached richer valuations overall, we have continued to uncover what we believe to be compelling opportunities among individual issuers. With the Fed appearing
to remain on hold for the foreseeable future, we have maintained the funds average duration in a range that is slightly longer than industry averages. Finally, we recently increased the funds positions in bonds from the emerging markets,
including securities from Brazil and Poland, that we believe may benefit as local inflation rates fall and currency exchange rates improve.

January








Total return includes reinvestment of dividends and any capital gains paid, and does not take into
