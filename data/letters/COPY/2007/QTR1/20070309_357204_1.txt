Dear Fellow Shareholders:
This letter and report on the
Westcore Funds provides a discussion of performance for the seven-month period ended
December 31, 2006. While we normally provide you with this information every six months, we
recently changed the Funds' fiscal year-end from May 31 to December 31. Therefore, to realign
our semiannual shareholder reports with the Funds' new fiscal year, we are
providing you with this seven-month update.
Our
decision to change the funds' fiscal year was based primarily on the benefit it
offers to
shareholders. We believe shareholders will find it easier to understand and follow investment performance if
it is discussed in a calendar year timeframe. In addition, we expect to realize
a number of administrate efficiencies as a result of this change.
Turning
to investment returns during this seven-month period, all 11 of the Westcore Funds produced gains,
benefiting from solid-performing stock and bond markets. Even into the early days of January, it
appeared as though this trend may extend through the first quarter of 2007: The S&amp;P 500
Index recently reached a six-year high,
and interest rates and inflation are low and stable.
We
continued to see growth in the Westcore Fund Family assets during the period. Assets were $1.8 billion at the
end of 2006, and they are currently approaching $2 billion. We believe this growth
is a reflection of good performance, good service and reasonable costs. And we
continue to work hard to improve each of these factors.
I
hope you find the following individual fund overviews from each of our
portfolio managers
helpful. Please note that, because it has been only one month since our previous report to you, these
commentaries have not changed substantially. Our next report to you will be for the six-months ending June
30, 2007.
Annual Report December 31,
		2006

