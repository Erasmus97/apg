Dear Shareholder,
In its recent meetings, the U.S. Federal Reserve Board (the "Fed") ceased what was a two-year trend. That trend  a string of 17 consecutive interest rate hikes that ended last fall  was seen by many analysts as a sign that the Fed was concerned about containing inflation.
Of course, the Fed's actions impact economies and market performance around the world. For instance, the current interest rate climate has been cited as one reason for a decline in the value of the dollar. Last year, that drop in the dollar meant that international stocks  which are often determined in foreign currencies  generally experienced strong performance as foreign currencies climbed and world stock markets continued to produce strong results.
At ING Funds, we are committed to providing you, the investor, with an array of investment choices that enables you to build a diversified portfolio across a variety of asset classes. We do so, with a view that the globalization of markets is an initial consideration in the products we offer.
We take the confidence you place in us with your investments very seriously  everyone in our firm is committed to renewing this trust each and every day.


