February Dear TARGET Shareholder:
We hope you find the annual report for The TARGET Portfolio Trust
informative and useful. Today many investors may be asking where they can find new growth opportunities. We believe that as a TARGET shareholder, you are uniquely positioned for domestic and global growth opportunities because you
already have a strategic investment plan in place. The TARGET
programs structured and professional approach to investing helps you tune out the noise of current market developments and allows you to concentrate on whats really importantyour long-term goals. It starts with a
personal plan that you and your financial professional construct based on your reasons for investing, the time you have to reach your goals, and the level of risk you are willing to assume. Your financial professional can work closely with you to
develop an appropriate asset allocation and select the corresponding TARGET portfolio for each asset class in your investment plan. The managers for each portfolio are carefully chosen, are monitored by our team of experienced
investment management analysts, and are among the leading institutional money managers available.
Your TARGET program also evolves with your changing needs. Your financial professional can help you track your plans progress, stay informed of important developments, and assist you in determining whether you need to
modify your portfolio. In these ways and more, the TARGET program seeks to make your investment goals a reality.
Thank you for your continued confidence.
Sincerely,
