Dear Shareholder,
 
Yields for municipal bonds generally increased during the first part of your funds fiscal year, then declined amid signs of a cooling economy and easing inflation concerns.
 
For the year, Vanguard Ohio Long-Term Tax-Exempt Fund returned 6.1%. As of November 30, the funds yield was 3.74%. For investors in the highest income tax bracket, the taxable-equivalent yield was 6.23%. 
Vanguard Ohio Tax-Exempt Money Market Fund returned 3.3%. The funds yield increased to 3.46% (from 2.89% a year ago); the taxable-equivalent yield would be 5.77% for investors in the highest income tax bracket. The fund maintained a net asset value of $1 per share, as is expected but not guaranteed.
Although the funds income distributions are expected to be exempt from federal and Ohio state income taxes, a portion of these distributions may be subject to the federal alternative minimum tax.
Weaker parts of the economy drew bond investors notice
In the first half of the fiscal year, the Federal Reserve Board continued to tighten monetary policy, raising its target for the federal funds rate five times through the end of June. Longer-term bond yields generally followed the upward trend until late summer, when they began
 
 
 
 
 
2


 
 
to backslide. Weakness in the housing and manufacturing sectors persuaded bond investors that inflation was not a threat, prompting a rally (bond prices rose and yields fell). 
The broad taxable bond market returned 5.9%. Municipal bonds did better still.
Economic uncertainty didnt stop a stock rally 
Despite being caught in a crosscurrent of opinions on the economy, stocks more than held their own. The housing market and automobile makers produced a drum-beat of bad news; however, corporate profits and job creation remained strong. Amid such mixed signals, stocks rose sharply beginning in mid-July. The broad market gained 14.7% for the year; in October, the narrower Dow Jones Industrial Average broke through its January 2000 highand kept going.
Small-capitalization stocks enjoyed an edge over large-cap stocks by a margin of roughly 3 percentage points. Among both small-cap and large-cap stocks, value-oriented stocks outperformed their growth-oriented counterparts by wide margins.
Investors in international stocks were rewarded by both a falling U.S. dollar and strong economic gains in Europe and emerging markets.
 
Market Barometer
 
 
 
 
 
Average Annual Total Returns
 
Periods Ended November 30, 2006
 
One Year
Three Years
Five Years
Bonds
 
 
 
Lehman Aggregate Bond Index (Broad taxable market)
5.9%
4.2%
5.0%
Lehman Municipal Bond Index
6.1
4.7
5.4
Citigroup 3-Month Treasury Bill Index
4.7
2.9
2.3
 
 
 
 
Stocks
 
 
 
Russell 1000 Index (Large-caps)
14.2%
12.2%
6.8%
Russell 2000 Index (Small-caps)
17.4
14.2
12.7
Dow Jones Wilshire 5000 Index (Entire market)
14.7
12.8
7.8
MSCI All Country World Index ex USA (International)
29.2
23.6
16.5
 
 
 
 
CPI
 
 
 
Consumer Price Index
2.0%
3.0%
2.6%
 
 
 
 
 
 
 
 
3
 
 
 
 
 
Advisor kept sharp focus on funds income and stability
During the funds fiscal year, interest rates moved in an upwardthen downwarddirection. These movements largely hinged upon investors outlook for inflation: In the first part of the fiscal year, many feared the economys brisk growth rate would trigger higher inflation. This drove interest rates higher. In the second part of the year, the economy cooled, inflation fears eased, and interest rates fell. In the municipal bond market, yields peaked in June, then decreased over the final five months of the fiscal year. For the full 12 months, short-term municipal yields increased slightly, and the yields for the longest-term securities fell. Throughout these ups and downs, the funds advisor remained focused on maintaining the funds income stability. 
 
The Ohio Long-Term Tax-Exempt Funds 6.1% return surpassed the average return of competing Ohio municipal bond funds, but slightly trailed the result of the benchmark index. The funds positioning represented a trade-off between interest rate sensitivity and income generation, and the fund experienced relatively stable income and an increase in net asset value for the fiscal year.
The Ohio Tax-Exempt Money Market Fund returned 3.3%, besting the average return of its peers. The fund benefited from the year-over-year increase in short-term interest rates as it quickly translated rising rates into higher income. In an arena where a few basis points of income can mean a great deal, the funds low expense ratio continued to serve its shareholders well.
 
 
 
 
 
 
Your fund compared with its peer group
 
 
 
 
Peer
Ohio Tax-Exempt Fund
Fund
Group
Money Market
0.13%
0.64%
Long-Term
0.14
1.12
 
 
 
 
 
 
 
 
 
 
1
Fund expense ratios reflect the 12 months ended November 30, 2006. Peer groups are: for the Ohio Tax-Exempt Money Market Fund, the Average Ohio Tax-Exempt Money Market Fund; and for the Ohio Long-Term Tax-Exempt Fund, the Average Ohio Municipal Debt Fund. Peer-group expense ratios are derived from data provided by Lipper Inc. and capture information through year-end 2005.
4


 
 
The funds performance advantage is clear over the long term
While a year of outperformance is certainly a worthy accomplishment, we believe its more meaningful to highlight the longer-term picture. The table below displays how hypothetical investments of $10,000 in the Ohio Tax-Exempt Funds would have grown over the past ten years, compared with the average performance among competing funds. As you can see, our funds advantage has been clear. 


