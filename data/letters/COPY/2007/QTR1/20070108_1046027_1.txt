Dear Shareholder:

We are pleased to provide the annual report for the Evergreen Global Large Cap Equity Fund, covering the twelve-month period ended October

Supported by the widespread, persistent global economic expansion, international equity markets generally delivered solid results for the fiscal year. Dynamic development in emerging market nations, led by China and India,
helped build demand for goods and services from markets throughout the world. At the same time, investors in foreign stocks were less influenced by factors such as high energy prices and rising interest rates, which, at times, held back results in
the domestic equity market. As a consequence, foreign equities generally outperformed domestic stocks, with the performance edge holding up across most regions of the world. Emerging markets provided superior returns compared to more developed
markets, although with greater volatility.

International fixed income markets, however, produced more modest results, as both bond markets and currencies experienced significant volatility during a period in which investors faced a variety of challenges and opportunities. In the face of the sustained global economic expansion, most major central banks tightened monetary policies, raising short-term interest rates to quell
inflationary pressures exacerbated by rapidly rising commodity prices, notably in energy. World oil and natural gas prices began receding off their highs in July, relieving inflationary pressures and allowing the U.S. Federal Reserve to leave the
influential Fed Funds rate unchanged from its level at the Boards meetings in both August and September. Other major central banks continued to raise short-term rates, but overall monetary policy remained far from restrictive, even if
less stimulative than earlier.

In managing Evergreens international and global funds, portfolio teams pursued strategies focusing on opportunities within









LETTER TO SHAREHOLDERS continued

specific markets and sectors. The team managing the Evergreen Global Large Cap Equity Fund, for example, invested in larger companies, both domestic and foreign. In contrast, the managers of the Evergreen International
Equity Fund concentrated on foreign large-cap equities, with some exposure to mid-cap and emerging market stocks. The teams managing portfolios of the Evergreen Global Opportunities Fund sought attractive mid-cap and small-cap growth stocks, both in
foreign markets and the United States, while the managers of the Evergreen Emerging Markets Growth Fund focused exclusively on developing economies. At the same time, the team managing the Evergreen Precious Metals Fund concentrated on gold-mining
stocks and other precious metals-related equities throughout the world. The managers of the Evergreen International Bond Fund, meanwhile, paid close attention to economic fundamentals, interest-rate expectations, and currency movements in investing in foreign debt securities.

As always, we encourage investors to maintain diversified investment portfolios, including allocations to international and global funds, in pursuit of their long-term investment goals.

Please visit our Web site, EvergreenInvestments.com, for more information about our funds and other investment products available to you. Thank you for your continued support of Evergreen Investments.

Sincerely,

Dear Shareholder:

We are pleased to provide the annual report for the Evergreen International Equity Fund, covering the twelvemonth period ended October

Supported by the widespread, persistent global economic expansion, international equity markets generally delivered solid results for the fiscal year. Dynamic development in emerging market nations, led by China and India,
helped build demand for goods and services from markets throughout the world. At the same time, investors in foreign stocks were less influenced by factors such as high energy prices and rising interest rates, which, at times, held back results in
the domestic equity market. As a consequence, foreign equities generally outperformed domestic stocks, with the performance edge holding up across most regions of the world. Emerging markets provided superior returns compared to more developed
markets, although with greater volatility.

International fixed income markets, however, produced more modest results, as both bond markets and currencies experienced significant volatility during a period in which investors faced a variety of challenges and opportunities. In the face of the sustained global economic expansion, most major central banks tightened monetary policies, raising short-term interest rates to quell inflationary pressures exacerbated by rapidly
rising commodity prices, notably in energy. World oil and natural gas prices began receding off their highs in July, relieving inflationary pressures and allowing the U.S. Federal Reserve to leave the influential Fed Funds rate unchanged from its
level at the Boards meetings in both August and September. Other major central banks continued to raise short-term rates, but overall monetary policy remained far from restrictive, even if less stimulative than earlier.

In managing Evergreens international and global funds, portfolio teams pursued strategies focusing on opportunities within specific markets and sectors. The









LETTER TO SHAREHOLDERS continued

team managing the Evergreen Global Large Cap Equity Fund, for example, invested in larger companies, both domestic and foreign. In contrast, the managers of the Evergreen International Equity Fund concentrated on foreign
large-cap equities, with some exposure to mid-cap and emerging market stocks. The teams managing portfolios of the Evergreen Global Opportunities Fund sought attractive mid-cap and small-cap growth stocks, both in foreign markets and the United
States, while the managers of the Evergreen Emerging Markets Growth Fund focused exclusively on developing economies. At the same time, the team managing the Evergreen Precious Metals Fund concentrated on gold-mining stocks and other precious
metals-related equities throughout the world. The managers of the Evergreen International Bond Fund, meanwhile, paid close attention to economic fundamentals, interest-rate expectations, and currency movements in investing in foreign debt securities.

As always, we encourage investors to maintain diversified investment portfolios, including allocations to international and global funds, in pursuit of their long-term investment goals.

Please visit our Web site, EvergreenInvestments.com, for more information about our funds and other investment products available to you. Thank you for your continued support of Evergreen Investments.

Sincerely,

Dear Shareholder:

We are pleased to provide the annual report for the Evergreen Emerging Markets Growth Fund, covering the twelve-month period ended October

Supported by the widespread, persistent global economic expansion, international equity markets generally delivered solid results for the fiscal year. Dynamic development in emerging market nations, led by China and India,
helped build demand for goods and services from markets throughout the world. At the same time, investors in foreign stocks were less influenced by factors such as high energy prices and rising interest rates, which, at times, held back results in
the domestic equity market. As a consequence, foreign equities generally outperformed domestic stocks, with the performance edge holding up across most regions of the world. Emerging markets provided superior returns compared to more developed
markets, although with greater volatility.

International fixed income markets, however, produced more modest results, as both bond markets and currencies experienced significant volatility during a period in which investors faced a variety of challenges and opportunities. In the face of the sustained global economic expansion, most major central banks tightened monetary policies, raising short-term interest rates to quell inflationary pressures exacerbated by rapidly
rising commodity prices, notably in energy. World oil and natural gas prices began receding off their highs in July, relieving inflationary pressures and allowing the U.S. Federal Reserve to leave the influential Fed Funds rate unchanged from its
level at the Boards meetings in both August and September. Other major central banks continued to raise short-term rates, but overall monetary policy remained far from restrictive, even if less stimulative than earlier.

In managing Evergreens international and global funds, portfolio teams pursued strategies focusing on opportunities within specific markets and sectors. The









LETTER TO SHAREHOLDERS continued

team managing the Evergreen Global Large Cap Equity Fund, for example, invested in larger companies, both domestic and foreign. In contrast, the managers of the Evergreen International Equity Fund concentrated on foreign
large-cap equities, with some exposure to mid-cap and emerging market stocks. The teams managing portfolios of the Evergreen Global Opportunities Fund sought attractive mid-cap and small-cap growth stocks, both in foreign markets and the United
States, while the managers of the Evergreen Emerging Markets Growth Fund focused exclusively on developing economies. At the same time, the team managing the Evergreen Precious Metals Fund concentrated on gold-mining stocks and other precious
metals-related equities throughout the world. The managers of the Evergreen International Bond Fund, meanwhile, paid close attention to economic fundamentals, interest-rate expectations, and currency movements in investing in foreign debt securities.

As always, we encourage investors to maintain diversified investment portfolios, including allocations to international and global funds, in pursuit of their long-term investment goals.

Please visit our Web site, EvergreenInvestments.com, for more information about our funds and other investment products available to you. Thank you for your continued support of Evergreen Investments.

Sincerely,

Dear Shareholder:

We are pleased to provide the annual report for the Evergreen Global Opportunities Fund, covering the twelve-month period ended October

Supported by the widespread, persistent global economic expansion, international equity markets generally delivered solid results for the fiscal year. Dynamic development in emerging market nations, led by China and India,
helped build demand for goods and services from markets throughout the world. At the same time, investors in foreign stocks were less influenced by factors such as high energy prices and rising interest rates, which, at times, held back results in
the domestic equity market. As a consequence, foreign equities generally outperformed domestic stocks, with the performance edge holding up across most regions of the world. Emerging markets provided superior returns compared to more developed
markets, although with greater volatility.

International fixed income markets, however, produced more modest results, as both bond markets and currencies experienced significant volatility during a period in which investors faced a variety of challenges and opportunities. In the face of the sustained global economic expansion, most major central banks tightened monetary policies, raising short-term interest rates to quell
inflationary pressures exacerbated by rapidly rising commodity prices, notably in energy. World oil and natural gas prices began receding off their highs in July, relieving inflationary pressures and allowing the U.S. Federal Reserve to leave the
influential Fed Funds rate unchanged from its level at the Boards meetings in both August and September. Other major central banks continued to raise short-term rates, but overall monetary policy remained far from restrictive, even if
less stimulative than earlier.

In managing Evergreens international and global funds, portfolio teams pursued strategies focusing on opportunities within









LETTER TO SHAREHOLDERS continued

specific markets and sectors. The team managing the Evergreen Global Large Cap Equity Fund, for example, invested in larger companies, both domestic and foreign. In contrast, the managers of the Evergreen International
Equity Fund concentrated on foreign large-cap equities, with some exposure to mid-cap and emerging market stocks. The teams managing portfolios of the Evergreen Global Opportunities Fund sought attractive mid-cap and small-cap growth stocks, both in
foreign markets and the United States, while the managers of the Evergreen Emerging Markets Growth Fund focused exclusively on developing economies. At the same time, the team managing the Evergreen Precious Metals Fund concentrated on gold-mining
stocks and other precious metals-related equities throughout the world. The managers of the Evergreen International Bond Fund, meanwhile, paid close attention to economic fundamentals, interest-rate expectations, and currency movements in investing in foreign debt securities.

As always, we encourage investors to maintain diversified investment portfolios, including allocations to international and global funds, in pursuit of their long-term investment goals.

Please visit our Web site, EvergreenInvestments.com, for more information about our funds and other investment products available to you. Thank you for your continued support of Evergreen Investments.

Sincerely,

Dear Shareholder:

We are pleased to provide the annual report for the Evergreen Precious Metals Fund, covering the twelvemonth period ended October

Supported by the widespread, persistent global economic expansion, international equity markets generally delivered solid results for the fiscal year. Dynamic development in emerging market nations, led by China and India,
helped build demand for goods and services from markets throughout the world. At the same time, investors in foreign stocks were less influenced by factors such as high energy prices and rising interest rates, which, at times, held back results in
the domestic equity market. As a consequence, foreign equities generally outperformed domestic stocks, with the performance edge holding up across most regions of the world. Emerging markets provided superior returns compared to more developed
markets, although with greater volatility.

International fixed income markets, however, produced more modest results, as both bond markets and currencies experienced significant volatility during a period in which investors faced a variety of challenges and opportunities. In the face of the sustained global economic expansion, most major central banks tightened monetary policies, raising short-term interest rates to quell inflationary pressures exacerbated by rapidly
rising commodity prices, notably in energy. World oil and natural gas prices began receding off their highs in July, relieving inflationary pressures and allowing the U.S. Federal Reserve to leave the influential Fed Funds rate unchanged from its
level at the Boards meetings in both August and September. Other major central banks continued to raise short-term rates, but overall monetary policy remained far from restrictive, even if less stimulative than earlier.

In managing Evergreens international and global funds, portfolio teams pursued strategies focusing on opportunities within specific markets and sectors. The









LETTER TO SHAREHOLDERS continued

team managing the Evergreen Global Large Cap Equity Fund, for example, invested in larger companies, both domestic and foreign. In contrast, the managers of the Evergreen International Equity Fund concentrated on foreign
large-cap equities, with some exposure to mid-cap and emerging market stocks. The teams managing portfolios of the Evergreen Global Opportunities Fund sought attractive mid-cap and small-cap growth stocks, both in foreign markets and the United
States, while the managers of the Evergreen Emerging Markets Growth Fund focused exclusively on developing economies. At the same time, the team managing the Evergreen Precious Metals Fund concentrated on gold-mining stocks and other precious
metals-related equities throughout the world. The managers of the Evergreen International Bond Fund, meanwhile, paid close attention to economic fundamentals, interest-rate expectations, and currency movements in investing in foreign debt securities.

As always, we encourage investors to maintain diversified investment portfolios, including allocations to international and global funds, in pursuit of their long-term investment goals.

Please visit our Web site, EvergreenInvestments.com, for more information about our funds and other investment products available to you. Thank you for your continued support of Evergreen Investments.

Sincerely,
