Dear Fellow Shareholder:

We have all seen and heard the words "tax-free" over and over in various
contexts. In fact, the name of the Fund contains the words "tax-free." But, what
does that really mean to you in black and white?

As you know, you have to pay federal, state, and/or local taxes on the
dividends distributed to you by a taxable mutual fund. Thus, a bond fund which
invests in tax-free municipal bonds, such as Churchill Tax-Free Fund of
Kentucky, may actually generate more net dividend income than a taxable bond
fund with a higher stated yield.

How is this possible? Let's use a simple example.

Suppose you invest in a hypothetical TAX-FREE municipal bond fund
with a yield of paying dividends that are EXEMPT FROM FEDERAL INCOME TAXES,
and you also invest in a hypothetical TAXABLE bond fund with a yield of
paying dividends that are TAXABLE AT THE FEDERAL LEVEL. The example below
shows the outcome for an investor in the federal income tax bracket. As you
can see, your investment in the tax-free municipal bond fund would have ended up
with more after-tax income even though the fund's yield was lower.

[BAR CHART]

Federal Income Interest Income
Taxes After Taxes

Tax-Free Municipal Bond
Fund Yielding (Assumes no taxes)

Taxable Bond Fund Yielding
Tax Rate

To help you compare taxable bond funds versus tax-free municipal bond
funds, a taxable-equivalent yield is often used. The taxable-equivalent yield
provides you with the rate that a taxable bond fund would have to yield to give
you the same after-tax yield as a tax-free municipal bond fund. (You should be
aware that the calculation does not take into account the impact of state income
taxes, or any alternative minimum taxes, to which some investors may be
subject.)

NOT A PART OF THE ANNUAL REPORT



In its simplest terms,

- --------------------------------------------------------------------------------
Yield of tax-free municipal bond fund
- ------------------------------------- = Your taxable-equivalent yield
- your federal income tax rate
- --------------------------------------------------------------------------------

Using the above tax-free example of a yield and a federal tax bracket
of the calculation would be as follows:

- --------------------------------------------------------------------------------

- ------------ =
-
- --------------------------------------------------------------------------------

In other words, a tax-free investment of and a taxable investment of
would provide the same yield after taxes.

EFFECT OF FEDERAL INCOME TAXES ON YIELDS OF TAX-EXEMPT AND TAXABLE
INSTRUMENTS


You should be aware that dividends paid by tax-free municipal bond funds,
such as Churchill Tax-Free Fund of Kentucky, are often also exempt from state
and/or local taxes. FACTORING IN THE EXEMPTION AT THE STATE AND/OR LOCAL LEVEL
WOULD HAVE THE EFFECT OF INCREASING THE TAXABLE EQUIVALENT YIELD EVEN FURTHER.

So, the next time that you see the phrase "tax-free," remember that in
black and white, it oftentimes means more money into your pocket.

Sincerely,
