Dear Shareowner:
- --------------------------------------------------------------------------------
As the global economy reacted to good and bad news of overall it managed
to keep chugging along with fairly stable growth. Looking back, the economic
stories included slowing economic growth bringing monetary tightening to an end
in the United States, price action in oil and other commodity markets
threatening to unsettle global growth and inflation, geopolitical uncertainties
and corporate earnings growth surpassing market expectations.

Global growth surpassed most analyst expectations in The International
Monetary Fund estimates world output increased by in up from in
and above the average annual pace of growth of the previous years.
Growth is estimated to slow modestly in but to remain well above the
long-term average.

U.S. economic growth slowed gradually over with estimated full-year growth
of GDP of modestly below growth but still strong enough to lower the
unemployment rate to While many observers were concerned that a weakening
housing sector could be a drag on consumer spending, the consumer has shown
resilience supported by a strong labor market and associated income growth and
by oil and gasoline prices falling from record highs in the second half of the
year. Corporate America has also proved resilient in the face of the slowing
housing sector, with business investment growing at its fastest rate since

The Federal Reserve stopped increasing interest rates in but has
retained its bias to raise rates, reflecting upside risks to inflation,
particularly from the tight labor market, and confidence that the economy
remained on solid ground. Consumers welcomed the relief from oil prices that
peaked at mid-year, and fell sharply towards year end. Likewise, they benefited
from core inflation (which excludes energy and food items) that has eased
modestly in the second half of the year, but remains above what is generally
considered the central bank's "comfort zone."

The European economy surpassed many economists' expectations in Eurozone
GDP grew at a pace, much stronger than its average annual growth pace
since Underlying this strength has been buoyant and broad-based strength
in business sentiment and investment and in exports. While household consumption
showed mixed results, the unemployment rate fell to by the end of The
European Central Bank raised interest rates gradually to over the course
of reflecting firm economic growth, steadily tightening labor market
conditions, and fast growing money supply. Core inflation in the Eurozone
remains reasonably benign, at roughly per annum. European stock markets
posted strong returns; appreciation of the Euro relative to both the dollar and
Yen made the Eurozone stock market the top performer of

The Japanese economy continued to grow firmly in although the pace of
expansion slowed in the second half of the year. Business investment was the
major driver of expansion as increasing capacity utilization and better
sentiment encouraged a new wave of investment. As deflation ended, the Bank of
Japan ended its zero interest rate policy, although it has raised rates to only
thus far. After a strong showing in the Japanese stock market posted
only muted gains in




Letter

Emerging economies enjoyed strong, relatively uninterrupted economic growth in
Good financing conditions, buoyant commodity prices, which boosted income
and investment in commodity exporting countries, and strong export demand from
developed economies drove this growth. The emerging market stock index was very
strong in early declined to near start-of-year levels, then rallied to
finish the year only marginally behind the Eurozone. We believe similar growth
can be expected in the emerging markets if current domestic economic and
political conditions prevail.

While global economic momentum has started to slow, we expect the global economy
to continue growing firmly in supported by business investment, rising
employment, and lower energy prices. A slowdown in the U.S. growth rate would be
welcome, since rapid growth could increase the risk of inflationary pressures,
which would force the Federal Reserve to raise interest rates. We expect Europe
and Japan to grow similarly, while emerging market economies are expected to
continue their strong growth. In this scenario, fixed-income assets are expected
to produce total returns generally in line with their current yields, while
equities are expected to produce returns generally in line with earnings growth,
which we forecast to be moderately above bond yields.

Our cautiously optimistic outlook on most regions of the world and asset classes
reinforces the importance of Pioneer Investments' message that investors should
remain diversified; take a long-term view rather than over-reacting to breaking
news, and base investment decisions on economic and market fundamentals rather
than on emotion. Our investment philosophy and approach continue to be based on
the use of fundamental research to identify a range of opportunities that offer
an attractive balance of risk and reward to help Fund shareowners work toward
their long-term goals.

Respectfully,
