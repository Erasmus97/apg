Dear Shareholders:

The year proved to be a very good one for many of the world's financial
markets. The U.S. stock market, as one notable example, posted strong gains as
the Dow Jones Industrial Average hit record highs and the Standard & Poor's
Stock Index rose to levels not seen since

While investors were undoubtedly encouraged by these positive developments, we
think it is important to maintain the proper perspective on what appears to be
happening with the world's financial markets. Around the globe, economies seem
to have reached a state of equilibrium. Both stock and bond markets have simply
been less volatile than they were in years past. While there are many factors
behind the low levels of volatility, one of the key reasons is that the U.S.
Federal Reserve Board and other central banks around the world seem to have
found the right formula for simultaneously promoting economic growth and
keeping inflation under control.

This new, calmer state of affairs is no doubt welcomed by many investors, but
with less volatility, the possibilities for earning exceptional returns also
diminish. Investors may have to lower their expectations about what are
reasonable returns from the markets. Those still looking for the types of
returns they had been accustomed to in the past may have to work much harder to
seek out those opportunities and perhaps also accept more risk than they
customarily did. In any market climate, even in a relatively benign one, we
think investors are still best served by spreading their holdings across a
variety of investments that will give them exposure to a range of market
opportunities and varying levels of risk.

Respectfully,

Dear Shareholders:

The year proved to be a very good one for many of the world's financial
markets. The U.S. stock market, as one notable example, posted strong gains as
the Dow Jones Industrial Average hit record highs and the Standard & Poor's
Stock Index rose to levels not seen since

While investors were undoubtedly encouraged by these positive developments, we
think it is important to maintain the proper perspective on what appears to be
happening with the world's financial markets. Around the globe, economies seem
to have reached a state of equilibrium. Both stock and bond markets have simply
been less volatile than they were in years past. While there are many factors
behind the low levels of volatility, one of the key reasons is that the U.S.
Federal Reserve Board and other central banks around the world seem to have
found the right formula for simultaneously promoting economic growth and
keeping inflation under control.

This new, calmer state of affairs is no doubt welcomed by many investors, but
with less volatility, the possibilities for earning exceptional returns also
diminish. Investors may have to lower their expectations about what are
reasonable returns from the markets. Those still looking for the types of
returns they had been accustomed to in the past may have to work much harder to
seek out those opportunities and perhaps also accept more risk than they
customarily did. In any market climate, even in a relatively benign one, we
think investors are still best served by spreading their holdings across a
variety of investments that will give them exposure to a range of market
opportunities and varying levels of risk.

Respectfully,

Dear Shareholders,

The year proved to be a very good one for many of the world's financial
markets. The U.S. stock market, as one notable example, posted strong gains as
the Dow Jones Industrial Average hit record highs and the Standard & Poor's
Stock Index rose to levels not seen since

While investors were undoubtedly encouraged by these positive developments, we
think it is important to maintain the proper perspective on what appears to be
happening with the world's financial markets. Around the globe, economies seem
to have reached a state of equilibrium. Both stock and bond markets have
simply been less volatile than they were in years past. While there are many
factors behind the low levels of volatility, one of the key reasons is that
the U.S. Federal Reserve Board and other central banks around the world seem
to have found the right formula for simultaneously promoting economic growth
and keeping inflation under control.

This new, calmer state of affairs is no doubt welcomed by many investors, but
with less volatility, the possibilities for earning exceptional returns also
diminish. Investors may have to lower their expectations about what are
reasonable returns from the markets. Those still looking for the types of
returns they had been accustomed to in the past may have to work much harder
to seek out those opportunities and perhaps also accept more risk than they
customarily did. In any market climate, even in a relatively benign one, we
think investors are still best served by spreading their holdings across a
variety of investments that will give them exposure to a range of market
opportunities and varying levels of risk.

Respectfully,
