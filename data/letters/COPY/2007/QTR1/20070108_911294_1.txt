Dear Shareholder,
In its recent meetings, the Federal Reserve Board (the "Fed") has ceased its two-year trend of raising interest rates. That trend  a string of 17 consecutive rate hikes  was seen by many analysts as a sign that the Fed was concerned about containing inflation.
Of course, the Fed's actions have also impacted economies and market performance around the world. For instance, the current interest rate climate has been cited as one reason for a recent decline in the value of the dollar. That drop in the dollar has meant that international stocks  which deal in foreign currencies  have generally experienced strong performance in recent months as their currencies have climbed. Meanwhile other factors have triggered robust international stock performance as well: business confidence overseas is up, foreign merger and acquisition activity has been positive and Japan, England and the Eurozone have all reported impressive earnings in 2006.
Whatever the future holds  here or abroad  we at ING Funds continue to work hard to provide you, the investor, with an array of investment choices that enable you build a smart and diversified portfolio. We also continue to expand and improve our customer service department to ensure that your needs are met promptly and that we indeed continue to make attaining your future goals easier.
On behalf of everyone here at ING Funds, I thank you for your continued support.
Sincerely,


