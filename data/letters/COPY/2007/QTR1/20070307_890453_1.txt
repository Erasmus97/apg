Dear Wilshire Mutual Funds Shareholder, We are pleased to present this Annual report to all shareholders of the Wilshire Mutual
Funds, Inc. This report covers the year ended (the Period), for all share classes of the Large Company Growth, Large Company Value, Small Company Growth, Small Company Value and Dow Jones Wilshire Index
Portfolio. MARKET ENVIRONMENT U.S. GDP growth slowed
progressively during the year. The latest GDP figure showed annualized growth in GDP of for the quarter of lower than the and annualized growth figures recorded for the and quarter of the year, respectively. Data
from the U.S. Department of Labor suggested further strengthening of the labor market. Non-farm payrolls increased by in December; and the unemployment rate fell from to over the year. The Consumer Price Index (CPI)
climbed by for the year, the best showing since and nearly a full percentage point lower than the jump in The CPI report showed that core inflation, which excludes energy and food costs, rose last year, compared with
gains in both and It was the highest increase since a jump in The U.S. stock market posted its fourth consecutive annual advance in
with the Dow Jones Wilshire Index up A fifth straight year of double-digit corporate earnings growth and an active mergers and acquisitions environment, fueled in large part by record levels of private equity buyouts, encouraged
equity investors to push stocks higher. Despite concerns of volatile energy prices, higher short-term interest rates, and declining home building
activities, investors benefited from a strong quarter rally in the broad U.S. equity market. The market rose in the last three months of the year, the strongest quarterly return in two years.
The U.S. equity market advance was led by small cap stocks, whose outperforming trend against large cap stocks was extended to eight consecutive years. The Dow Jones
Wilshire Small Cap Index returned for the year, outperforming the Dow Jones Wilshire Large Cap Index by for the same period. The investment environment for growth investors remained challenging. Healthcare and technology, the
traditional growth sectors of the economy lagged behind the more cyclical sectors of Energy and Materials. As a result, the return of the Dow Jones Wilshire Top Growth Index trailed the Dow Jones Wilshire Top Value Index by
for the year. The bond market returns improved in returning as compared to the return from the previous year. Bond investors experienced
positive returns during the second half of the year as the Fed paused from its interest rate tightening campaign and left short-term interest rate unchanged. After raising the key Fed Funds rate four times in the first six months of the year by a
total of from to the Fed refrained from further rate hikes for the rest of the year on concerns of rapidly slowing economy. The stability in short-term interest rates served as catalyst for positive bond returns in the latter part of
the year. International equities continued the out-performance against U.S. equities for the fourth consecutive year. Measured in U.S. dollar terms, the
MSCI AC World ex. U.S. Index returned for the Period, handily outpacing the return for the Dow Jones Wilshire Index. Strong economic growth and higher energy prices boosted returns of emerging markets around the globe. Emerging
markets, as measured by the MSCI Emerging Market Index, advanced for the Period.





Table of Contents

FUND PERFORMANCE OVERVIEW*
Wilshire is pleased to discuss the results with our shareholders. With the exception of the Large Company Growth Portfolio, the remaining four funds posted double-digit returns for the Period. The Large Company Value and the Small Company
Value portfolios returned and respectively, for the Period. Collectively, value stocks provided the most robust returns for equity investors as value stocks continued their dominance over growth stocks. The Small Company Growth
Portfolio returned for the Period. Despite general optimism concerning a market rebound by growth stocks in growth stocks lagged value stocks for the seventh consecutive year. The Large Company Growth Portfolio gained for the
Period, in a challenging environment for large growth stocks. The Dow Jones Wilshire Index Portfolio, which is designed to provide investors with the broadest U.S. equity market exposure gained for the Period.
Sincerely,
