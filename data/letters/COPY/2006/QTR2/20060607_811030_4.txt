Dear Shareholders:

During the first quarter of Pax World Funds and Walden Asset
Management joined Women's Equity Fund in sending letters to companies in our
portfolios that have a disproportionately low level of women and minority
representation on corporate boards. It is our intention to do more to bring to
the attention of the corporations the negative impact that such a low level of
representation has on shareholder value. Shareholder activism over the last
years has been a catalyst for many positive changes for working women. We know
our voice can be louder when our assets increase.

The Women's Equity Fund continues to focus on long-term appreciation. We
believe this consistent approach makes the Fund a solid core investment. The
Fund's performance as of March is as follows:


Performance data quoted represents past performance; past performance does
not guarantee future results. The investment return and principal value of an
investment will fluctuate so that an investor's shares, when redeemed, may be
worth more or less than their original cost. Current performance of the fund
may be lower or higher than the performance quoted. Performance data current to
the most recent month end may be obtained by calling or visiting
www.womens-equity.com.
- ---------------------

The Fund imposes a redemption fee on shares held for less than two
months.

MARKET AND PERFORMANCE SUMMARY

For the second consecutive quarter the stock market produced good returns.
In contrast, bond market returns were generally negative as the strength of the
economy, along with continued tightening by the Fed, pushed yields higher. A
positive start to the New Year is always welcome. However, many challenges
continued in the first quarter of Energy prices, which had moderated
during the last quarter of ended the quarter near the highs of last
summer, hindering returns.

We recognize that a single quarter is not necessarily indicative of the
prospective full year return, and certainly not of much importance in the
achievement of attractive performance in the long run. Underlying trends in
fundamental business conditions are of far more significance to both. Nothing
occurred during the past quarter that alters the positive economic and
investment views.

ECONOMIC SUMMARY AND OUTLOOK

The current economic expansion, both in the United States and globally, has
been driven by stimulative fiscal and monetary policies within most
industrialized and emerging markets. Growth has also been supported through
this period by greater political co-operation, increased trade, and technology-
based enhancements to labor productivity.

In the process, global employment and incomes have risen, while the
availability of lower cost manufactured goods and services has kept inflation
comparatively low despite increases in the prices of most raw materials. Global
capital markets have recognized these favorable economic trends, with most
markets rising steadily over the past several years.

Our consistently positive assessment of the domestic economy since the
summer of has relied on a continuation of these global business and
political trends. In recent months, financial headlines have focused on reports
of a decline in home sales. Together with the rise in interest rates, these
reports have raised concern that home values are about to decline, with the
natural consequence being an adverse impact on consumer spending and economic
activity generally. All of these concerns have merit. Yet broader economic
evidence suggests that continued, though perhaps more moderate, growth in GDP
over the next year or two is likely even in the face of these cross currents.
Fiscal policy, business capital spending, and rising employment are among the
key positive forces.

The still high price of oil has a dual impact on economic activity. In
part, high energy prices usurp general consumer spending and, to the extent we
import energy, serve as a restrictive tax on the domestic economy. On the
positive side, we have begun to see much greater capital investment to explore
and develop additional energy resources. At current price levels, many more
energy reserves have become economically viable, as has additional investment
related to energy conservation. Both types of expenditures will stimulate
industrial production through more demand for machinery and related equipment.
An added benefit is that the increase in energy production will tend to restrain
prices even while global economic activity remains strong. Our view is that the
probability of a sudden slowdown in economic growth over the next year or so
remains low.

Since the summer of the increase in corporate profits and cash flow
has been greater than the change in stock prices. The net result is that stocks
generally are still reasonably valued today despite a price recovery of roughly
in the S&P index over the past three years. Values are especially
attractive among stocks of larger, higher quality companies, many of which are
available at to times prospective earnings, in many cases the lowest
levels since the Unless the economy and corporate profits falter,
or long-term interest rates rise well above current levels, we expect a good
stock market.

NEW INSTITUTIONAL SHARE CLASS

The Women's Equity Fund now has available a new institutional class of
shares. For minimum initial investments of the expense ratio is
and the existing retail shares' total expense ratio has been reduced to
We hope you will tell your friends and companies about the Fund and help our
assets grow so that we can have a louder voice on behalf of women.

We appreciate your investment in the Women's Equity Fund and always welcome
your comments and suggestions.

Sincerely,
