Dear Shareowners,
- --------------------------------------------------------------------------------
There's a conundrum seen when observing the U.S. economy. In spite of sky high
oil prices and real estate prices starting to soften, the U.S. economy was on a
tear in the first quarter of Sizzling, roaring, and surging are terms
used recently in headlines to describe the current state of our economy.

Our nation's gross domestic product (GDP), the broadest measure of the
economy's strength, rose at an annual rate of in the first quarter, the
fastest growth rate since the third quarter of Part of this growth is a
rebound from a soft fourth quarter, but there is also real underlying growth.
Business spending is up as the caution of previous years is being
overcome by the need for equipment and technology. Consumers are doing their
part by pushing the sales of consumer durables up through the purchase of
new household goods and luxury items.

It is difficult to project how long this growth will last, but most economists
tend to agree that it can do so as long as business and consumer demand
continues and inflation remains low.

Foreign markets are also faring well, benefiting from growth-oriented economic
policies, and world economic growth is becoming more broadly based. The
Japanese economy is expanding, and there are signs of a sustained recovery in
Europe. Growth in the emerging economies and developing nations remains solid,
with tremendous strength in China, India and Russia. Looking forward, we expect
strong growth to continue, yet we remain cautious.

Investor confidence, a favorable economic climate and healthy corporate
profitability and cash flow have helped global stock markets continue their
strong performance in

The broad U.S. stock market is doing extremely well thus far in Investors
seem to have grown accustomed to companies generally delivering on their
earnings promises, lessening anxiety in the marketplace. Investors are feeling
confident with stocks, especially those of mid-sized and small companies, which
have far outpaced large-cap stocks.

Yet, the Federal Reserve response to the strong economy in the first quarter
has been cautious, weighing whether further interest hikes are necessary.





Letter

This concerns some in the financial markets, who have grown accustomed to the
Fed confidently determining the movement of rates in the past.

The Treasury-bond market ended the first quarter with short- and long-term bond
yields almost equal - a flat yield curve. Although municipal-bond yields are
not quite flat, the difference between short- and long-term interest rates is
the smallest it has been since Even if the Fed's interest rate hikes end
soon, intermediate and long interest rates are still low relative to inflation.

In summary, the economy and financial markets in the U.S. are prospering and
the fundamentals remain, in our view, healthy going forward. However, there are
no guarantees in investing: we know from a long-view of history that sudden
shifts can occur with little warning. We need only look to the natural
disasters and political upheavals of As such, we continue to pursue our
philosophy in stock and bond portfolio management: global resources dedicated
to fundamental research in the pursuit of opportunities that offer an
attractive balance of risk and reward to help our shareowners grow their
assets.

Respectfully,
