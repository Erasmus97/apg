Dear Shareholder:

We are pleased to present this semiannual report for Dreyfus BASIC S&amp;P Stock Index Fund, covering the six-month period from November through April

Since June the Federal Reserve Board has attempted to manage U.S. economic growth and forestall potential inflation by gradually raising short-term interest rates. Recently, Fed Chairman Ben Bernanke suggested that
the Fed may soon pause to assess current economic data and evaluate the impact of its credit tightening campaign. In our view, the Fed's efforts so far have largely been successful: the economy has grown at a moderate pace, the unemployment
rate has dropped to multi-year lows, corporate profits have risen, and inflation has remained low despite volatile energy prices.

However, the financial markets are more likely to be influenced not by what the Fed already has accomplished, but by investors' expectations of what is to come, including the Fed's decision to increase rates
further, maintain them at current levels or reduce them to stimulate future growth.We believe that this decision will depend largely on the outlook for core inflation in Fed probably can stand pat as long as it expects inflation to remain
subdued. But if inflationary pressures build in an expanding economy, the Fed may choose to resume tightening later this year.As always, we urge you to discuss with your financial advisor the potential implications of these possibilities on your
investments.

For information about how the fund performed, as well as market perspectives, we have provided a Discussion of Fund Performance given by the fund's portfolio manager.

Thank you for your continued confidence and support.

Sincerely,

Dear Shareholder:

We are pleased to present this semiannual report for Dreyfus Bond Market Index Fund, covering the six-month period from November through April

Since June the Federal Reserve Board (the "Fed") has attempted to manage U.S. economic growth and forestall potential inflation by gradually raising short-term interest rates. Recently, Fed Chairman Ben
Bernanke suggested that the Fed may soon pause to assess current economic data and evaluate the impact of its credit tightening campaign. In our view, the Fed's efforts so far have largely been successful: the economy has grown at a moderate
pace, the unemployment rate has dropped to multi-year lows, corporate profits have risen, and inflation has remained low despite volatile energy prices.

However, the bond market is more likely to be influenced not by what the Fed already has accomplished, but by investors' expectations of what is to come, including the Fed's decision to increase interest rates
further, maintain them at current levels or reduce them to stimulate future growth.We believe that this decision will depend largely on the outlook for core inflation in Fed probably can stand pat as long as it expects inflation to remain
subdued. But if inflationary pressures build in an expanding economy, the Fed may choose to resume tightening later this year. As always, we urge you to discuss with your financial advisor the potential implications of these possibilities on your
investments.

For information about how the fund performed, as well as market perspectives, we have provided a Discussion of Fund Performance given by the fund's portfolio manager.

Thank you for your continued confidence and support.











DISCUSSION OF FUND PERFORMANCE

Laurie Carroll, Portfolio Manager

How did Dreyfus Bond Market Index Fund perform relative to its benchmark?

For the six-month period ended April the fund achieved total returns of for its Investor shares and for its BASIC
The fund's benchmark, the Lehman Brothers U.S.Aggregate Index (the "Index"), achieved a total return of for the same

During the reporting period, most sectors of the U.S. bond market were challenged by strong economic growth, rising interest rates and renewed inflation concerns. However, these factors were partly offset by the positive
effects of robust overseas demand for U.S. government securities, low volatility in the mortgage-backed securities market and a generally favorable business environment for corporate bond issuers. We attribute the difference between the fund's
and benchmark's returns to fees and expenses that are not reflected in the Index.

What is the fund's investment approach?

The fund seeks to match the total return of the Index.To pursue this goal, the fund normally invests at least of its assets in bonds that are included in the Index. To maintain liquidity, the fund may invest up to
of its assets in various short-term, fixed-income securities and money market instruments.

While the fund seeks to mirror the returns of the Index, it does not hold the same number of bonds. Instead, the fund holds approximately securities as compared to securities in the Index. The fund's average
duration a measure of sensitivity to changing interest rates generally remains neutral to the Index.As of April the average duration of the fund was approximately years.

What other factors influenced the fund's performance?

The bond market's performance was influenced during the reporting period by sustained economic expansion, rising short-term interest

The Fund







DISCUSSION OF FUND PERFORMANCE (continued)

rates and strong corporate earnings. While GDP growth during the fourth quarter of proved to be somewhat sluggish, the economy rebounded sharply in the first quarter of fueled largely by a rebound in corporate
spending, employment gains and rising consumer confidence. Because fixed-income investors grew concerned that a strong economy might cause the Federal Reserve Board (the "Fed") to raise short-term interest rates more than previously
expected, they grew more cautious, and bond prices fell. Indeed, the Fed raised the overnight federal funds rate during each of four meetings of the Federal Open Market Committee over the reporting period, driving it from to .The Fed
acted again soon after the end of the reporting period, implementing its sixteenth consecutive increase since June hiking the federal funds rate to

In addition, while most measures of inflation generally have remained low, investors appeared to become more concerned about a possible reacceleration of inflation when energy and other commodity prices soared to new highs
in the spring of As a result, longer-term bond yields began to rise faster than shorter-term bond yields, eroding prices at the longer end of the market's maturity range. U.S. government securities and other interest rate-sensitive areas
of the bond market were particularly vulnerable to these adverse influences. However, robust demand from overseas investors helped limit price declines, and higher yields among U.S. government agency securities helped them outperform U.S.Treasury
securities.

Mortgage-backed securities, which comprise approximately one-third of the Index, produced some of the overall bond market's better returns due to persistently low levels of market volatility. An orderly slowdown in the
U.S. housing market, lower mortgage refinancing volumes and robust demand from banks and overseas investors helped contain market volatility in the rising interest-rate environment. Commercial mortgage-backed securities also performed well,
primarily due to low default rates, strong corporate earnings and sound business fundamentals in most industries. Rising corporate earnings and









low default rates, as well as greater mergers-and-acquisitions activity, also helped support bond prices in the investment-grade corporate bond market, which generally produced higher returns than U.S. Treasury
securities.

Another noteworthy development during the reporting period was the reintroduction of the bond by the U.S. Treasury in February Treasury bonds were last issued in when they were discontinued in
light of the then-prevailing federal budget surplus.While the results of early trading have been mixed, the longer-term influence of securities on the overall bond market has not yet been determined.

What is the fund's current strategy?

As always, we intend to continue to employ our strategy of closely monitoring the Index in an attempt to replicate its return.Accordingly, as of April approximately of the fund's assets were invested in
mortgage-backed securities, were allocated to U.S. Treasury securities, to corporate bonds and asset-backed securities, to U.S. government agency bonds and to securitized assets. In addition, the majority of the fund's corporate
securities were BBB-rated as of the reporting period's end, which is closely aligned with the overall credit quality of the Index.

May










DISCUSSION OF FUND PERFORMANCE

L. Emerson Tuttle and Catherine Powers, Portfolio Managers

How did Dreyfus Premier Balanced Fund perform relative to its benchmark?

For the six-month period ended April the fund produced total returns of for Class A shares, for Class B shares, for Class C shares, for Class R shares and for Class T
In comparison, the fund's benchmark, a hybrid index composed of Standard &amp; Poor's Composite Stock Price Index ("S&amp;P Index")
and Lehman Brothers U.S. Aggregate Index ("Lehman Aggregate Index"), provided a total return of for the same period. Separately, the S&amp;P Index and the Lehman Aggregate Index provided total returns of and
respectively, for the same

While stocks advanced in response to continued economic growth, rising interest rates generally held bonds in check. The fund roughly matched the benchmark's performance, with gains driven primarily by stocks in the
financial, energy and industrial sectors.

What is the fund's investment approach?

The fund seeks to outperform an unmanaged hybrid of which is the S&amp;P Index and of which is the Lehman Aggregate Index.

The fund is a balanced fund, with an allocation under normal circumstances of approximately stocks and bonds, corresponding to the fund's benchmark. However, the fund is permitted to invest up to and as
little as of its total assets in stocks, and up to and as little as of its total assets in bonds.

When allocating assets between stocks and bonds, we assess the relative returns and risks of each asset class, using a model that analyzes several factors, including interest-rate-adjusted price-to-earnings ratios, the
valuation and volatility levels of stocks relative to bonds, and economic factors such as interest rates.

We use a valuation model and fundamental analysis to select stocks based on: value, or how a stock is priced relative to its perceived intrinsic
worth;

The Fund







DISCUSSION OF FUND PERFORMANCE (continued)

growth, in this case the sustainability or growth of earnings or cash flow; and financial profile, which
measures the financial health of the company.

In choosing bonds, we review economic, market and other factors, leading to valuations by sector, maturity and quality.The fund invests primarily in U.S. government securities, corporate bonds, mortgage-backed securities
and asset-backed securities for its fixed-income portfolio. The average effective maturity of the fund's fixed-income portfolio normally will not exceed years.

What other factors influenced the fund's performance?

Moderate economic growth and rising interest rates produced a more positive environment for stocks than bonds during the reporting period.While the fund maintained greater exposure to stocks than its benchmark, weakness in
a few holdings kept the fund's total returns in line with the benchmark. Specifically, in the health care sector, weakness among biotechnology and medical device stocks hurt holdings such as Genzyme and Alcon. Disappointments in the technology
sector, such as Dell and Altera, which were sold during the reporting period, further undermined returns, as did light exposure to metals and mining stocks in the materials sector.

However, the fund delivered relatively strong returns in the financials area, where top performers included commercial banks, such as Bank of America and Wachovia; diversified financial services providers, such as JPMorgan
Chase &amp; Co.; and mortgage lenders, such as Countrywide Financial. An overweighted position and good stock selections in the energy sector, such as Weatherford International and Grant Prideco, enhanced returns. Industrial holdings further boosted
relative performance, including electrical equipment maker Emerson Electric and conglomerate Textron, which was sold during the reporting period.

Bond prices began to fall during the second half of the reporting period due to mounting inflation concerns in the strong economy, erasing earlier gains. U.S.Treasury securities were especially sensitive to rising interest
rates, while mortgage-backed securities and corporate









bonds fared somewhat better. Our emphasis on corporate bonds, especially those with "triple-B" credit ratings, performed well due to investors' ample appetite for risk. However, we avoided issuers likely to
weaken their balance sheets due to increased share repurchases or potentially, a leveraged buyout.The fund's relatively light exposure to mortgage-backed securities detracted modestly from performance. Although we expected mortgage-backed
securities to suffer as interest rates rose, they instead benefited from unusually low market volatility.

What is the fund's current strategy?

In light of continuing economic growth and steadily increasing interest rates, we have maintained the fund's emphasis on stocks over bonds.The fund currently holds overweighted exposure to energy and health care
stocks, and underweighted exposure to the consumer discretionary, technology, communications and utility sectors. We have begun to position the bond portfolio for the next phase of the credit cycle,including adopting a more index-like yield curve
strategy. We have continued to emphasize corporate bonds, which we expect to benefit from strong business conditions, and de-emphasize mortgage-backed securities, which began to encounter heightened volatility late in the reporting
period.

May

May










DISCUSSION OF FUND PERFORMANCE

Sean P. Fitzgibbon, Portfolio Manager

How did Dreyfus Premier Large Company Stock Fund perform relative to its benchmark?

For the six-month period ended April the fund produced total returns of for Class A shares, for Class B shares, for Class C shares, for Class R shares and for Class T
For the same period, the total return of the Standard &amp; Poor's Composite Stock Price Index ("S&amp;P Index"), the fund's benchmark,
was

Stocks rose during the reporting period, primarily due to better-than-expected corporate earnings in a growing economy.The fund produced higher returns than its benchmark, mainly as a result of strong individual stock
selections in the financials, industrials and energy sectors.

What is the fund's investment approach?

The fund seeks capital appreciation.To pursue its goal, the fund normally invests at least of its assets in stocks of large-cap companies.

The fund invests in a diversified portfolio of large companies that we believe meet our strict standards for value and growth.The fund invests in growth and value stocks, which are chosen through a disciplined investment
process that combines computer-modeling techniques, fundamental analysis and risk management.The fund's investment process is designed to provide investors with investment exposure to sector weightings and risk characteristics generally similar
to those of the S&amp;P Index.

In addition to identifying what we believe are attractive investment opportunities, our approach has been designed to manage the risks associated with modifying the fund's sector and industry exposure often in an
effort to capitalize on those sectors and industries currently in favor.We do not believe that the advantages of attempting to rotate in and out of various industry sectors outweigh the risks of such moves.

The Fund







DISCUSSION OF FUND PERFORMANCE (continued)

Instead, our goal is to minimize these risks by being fully invested and remaining industry and sector neutral in relation to the S&amp;P Index.

The result is a broadly diversified portfolio of carefully selected stocks. At the end of the reporting period, the fund held positions in approximately stocks across economic sectors. Our largest holdings
accounted for approximately of the portfolio, so that the fund's performance was not overly dependent on any one stock, but was determined by a large number of securities.

What other factors influenced the fund's performance?

Broad-based economic growth supported most of the stock market's industry groups, but produced particularly strong returns in the financials, industrials and energy areas.Among financial stocks, a number of companies
benefited from greater activity in the capital markets. Industrial stocks generally benefited from the development of industrial infrastructures in nations formerly considered third-world. Energy companies boosted profits as commodity prices soared
due to strong global demand for a limited supply of oil and gas.

The fund's returns in each of these sectors exceeded those of its benchmark. In the financials sector, the fund emphasized brokerage and asset management companies, such as E
*TRADE Financial, Goldman Sachs Group and Lehman Brothers Holdings, while de-emphasizing banks and insurers that tend to be more sensitive to changing interest rates. In the industrials area, the
fund focused on companies, including Caterpillar, Eaton and Emerson Electric, that are positioned to benefit from industrialization in the emerging markets.The fund's mild emphasis on energy stocks further bolstered its returns, as did our
focus on oilfield service companies, such as Weatherford International and GlobalSantaFe. Finally, among consumer cyclical stocks, the fund outperformed its benchmark by avoiding the troubled automotive industry.

In other industry groups, performance compared to the benchmark proved to be mixed. Individual stock selections in the technology sector, such as communications semiconductor maker Broadcom, added









modestly to the fund's relative performance. Gains in health care companies providing specialized laboratory products and services, such as Thermo Electron and Fisher Scientific International, helped offset declines in
other health care holdings, such as biotechnology firm Amgen. On the other hand, the fund's relatively light exposure to telecommunications services companies prevented it from participating fully in the sector's recent gains, and a
pullback in Altria Group, one of the prior reporting period's better performers, undermined returns in the consumer staples area.

What is the fund's current strategy?

We have adopted a more cautious posture regarding the potential impact of high oil prices and rising interest rates on consumer spending. As a result, we have reduced the fund's exposure to retailers and placed greater
emphasis on more defensive consumer staples producers. Because of uncertainties concerning the sustainability of high oil prices, we have trimmed the fund's energy exposure to a position that is in line with the benchmark. In the financials
sector, we have continued to focus on capital markets businesses while de-emphasizing more interest rate-sensitive banks and insurers. Finally, we have allocated relatively few assets to telecommunications services companies, which face an
increasingly competitive environment.

May
