DEAR PHOENIXFUNDS SHAREHOLDER:


The enclosed annual report addresses the performance of your Phoenix
mutual fund for the fiscal year ended February The report also
provides a commentary from your fund's management team on how the fund
performed, the investment strategies used, and how the fund's results compared
to the broader market.
At Phoenix, our focus is on investment performance and serving the best
interests of our shareholders. We believe that mutual funds are among the most
effective vehicles for individual investors to gain access to a variety of
financial markets and for building diversified portfolios.
I am especially proud of how we have expanded our fund family over the
last year to offer access to even more money managers. Today, the PhoenixFunds
draw from the vast expertise of different management teams--seven Phoenix
affiliates and nine outside subadvisers chosen for their complementary
investment capabilities.
These fund teams operate independently, conducting their research,
identifying opportunities in the markets they know best, and applying their
disciplined strategies to the portfolios they manage. We are confident in their
ability to navigate their funds through whatever market and economic changes lie
ahead.
When it comes to financial decisions, we recommend working with an
experienced financial advisor. If you haven't reviewed or rebalanced your
portfolio lately, this may be a good time to meet with your advisor and make
sure that your investments are still aligned with your financial goals.
Thank you for choosing PhoenixFunds to be part of your financial plan.

Sincerely yours,
