Dear Shareholder:

The fiscal year ended March was a positive one for Old Mutual Advisor
Funds II and ultimately the financial markets. Ongoing speculation regarding the
direction of oil prices and interest rates led to a difficult, event-driven
environment, as investors grappled with conflicting fears of higher inflation
and economic slowdown. These concerns were further exacerbated amid devastating
natural disasters, among the costliest in U.S. history, as well as persistent
geopolitical tensions, protracted monetary tightening and the appointment of a
new Federal Reserve chairman.

Despite these challenges, economic expansion and corporate earnings proved
resilient, as evidenced by strong fiscal-year gains across all the major market
averages. While energy sector performance was a clear standout, other areas
recorded impressive results, including industrials, materials and technology
issues, in addition to REITs (Real Estate Investment Trusts) and growth-oriented
investments.

We are pleased to report that the performance of the Old Mutual Advisor Funds II
portfolios reflected this strength. For more complete information, we invite you
to refer to the following pages, which discuss the Funds' individual activities
and returns in greater detail.

We are also pleased to announce the implementation of a series of strategic
improvements aimed at enhancing Fund management and lowering overall expenses.
In doing so, we are committed to providing a beneficial shareholder experience
that is consistent with your investment goals.

These changes included Old Mutual Capital, Inc. being appointed as investment
advisor effective January Fund names were changed to reflect this new
relationship and more closely align the Funds with Old Mutual Capital and its
parent, Old Mutual Asset Management.

At the same time, the Board and Old Mutual Capital identified and shareholders
have since approved several new sub-advisors to assume portfolio management
roles for certain Funds. Outstanding managers known for their consistency, depth
and stability were selected, many of which otherwise would be available only to
institutional or high net worth investors. Additionally, certain Funds now
employ a multi-manager approach, creating, in our view, unique, risk-managed
offerings for our shareholders.

With the integration of these new sub-advisors, we look forward to the coming
fiscal year with optimism and confidence. Although none of us has a crystal
ball, we believe these improvements make the Funds more competitive and position
them to better navigate the environment ahead. As always, we thank you for your
continued investment.

Sincerely,
