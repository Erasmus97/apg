Dear Shareholder:

For the months ended March most global economies experienced modest
to robust growth despite high energy prices and concerns about inflation. U.S.
gross domestic product (GDP) experienced solid growth in the first half of the
period before growth slowed in fourth quarter Many analysts expected first
quarter GDP growth to rebound. In Asia, China's GDP was strong amid some
easing inflationary pressures, and Japan's economy benefited from ongoing
banking and political reforms. Euro-zone countries generally reported weak
consumer and business sentiment, partly attributable to slow economic growth,
high unemployment in some countries, and the euro's decline against the U.S.
dollar. In this environment, global equity markets posted strong results during
the period, as measured by the return of the Morgan Stanley Capital
International (MSCI) Europe, Australasia, Far East (EAFE) Index and the
return of the MSCI World Index. Strength was geographically broad based with
the financials and materials sectors among the leaders.

Source: Standard & Poor's Micropal. The MSCI EAFE Index is a free
float-adjusted, market capitalization-weighted index designed to measure equity
market performance in global developed markets excluding the U.S. and Canada.
The MSCI World Index is a free float-adjusted, market capitalization-weighted
index designed to measure equity market performance in global developed markets.

- --------------------------------------------------------------------------------

EDELIVERY DETAILS

Log in at franklintempleton.com and click on eDelivery. Shareholders who are
registered at franklintempleton.com can receive these reports via email. Not all
accounts are eligible for eDelivery.

- -----------------------------------------------------
NOT FDIC INSURED | MAY LOSE VALUE | NO BANK GUARANTEE
- -----------------------------------------------------


Not part of the annual report |



Historically, patient investors have achieved rewarding results by evaluating
their goals, diversifying their assets and maintaining a disciplined investment
program. A long-term investment strategy may offer investors an opportunity to
take advantage of regularly scheduled investments, also called dollar-cost
averaging. By investing a certain amount of money monthly or quarterly, you can
take advantage of market dips without worrying about when they will occur. Your
money buys more shares when the price is low and fewer when the price is high,
which can mean a lower average cost per share over time. Remember, however, to
consider your financial ability to continue purchases through times of low price
levels or changing economic conditions before committing to such a strategy.
Dollar-cost averaging does not guarantee a profit or eliminate risk, and it will
not protect you from a loss if you sell shares at a market low. As always, we
encourage you to discuss your goals with your financial advisor who can address
concerns about volatility and diversification, periodically review your overall
portfolio and help you stay focused on the long term. We firmly believe that
most people benefit from professional advice, and that advice is never more
valuable than during a volatile market.

The enclosed annual report for Templeton International (Ex EM) Fund includes a
discussion of market conditions, investment management decisions and Fund
performance during the period under review. You will also find performance data
and financial information. Please remember that all securities markets
fluctuate, as do mutual fund share prices.

If you would like more frequent updates, FRANKLINTEMPLETON.COM provides daily
prices, monthly performance figures, portfolio holdings and other information.
You can also access your account, buy and sell shares, read timely commentary
from portfolio managers, and find helpful financial planning tools. We hope you
will take advantage of these online services.

We thank you for investing with Franklin Templeton, welcome your questions and
comments, and look forward to serving your investment needs in the years ahead.

Sincerely,
