Dear Shareholders:

THE FUND
- --------

For the fiscal year ending March your Fund returned
Equities only, excluding any cash reserves, were up underperforming the
Standard & Poor's Composite Index which returned Total assets in
the Fund were Approximately of the assets were invested in
equities, with in short-term investments and other assets less
liabilities.

Sector and stock-specific issues impacted returns. On a positive note, the
Fund experienced significant outperformance in the Energy, Financials,
Information Technology and Utility sectors of the portfolio. Sectors that
underperformed included Materials, Industrials, Consumer and Healthcare. An
over-weighting in Industrials combined with poor performance by the shares of
Cendant Corp. and Tyco International Ltd. negatively impacted returns. In the
Consumer area, an over-weighting in discretionary shares along with
underperformance in such companies as Brunswick Corp. and Comcast Corp. were
also important factors on returns. We have eliminated several securities from
the portfolio that were not performing well and believe that we are
appropriately structured for the current market environment.

MARKET REVIEW
- -------------

For the past year, the U.S. financial markets have battled the headwinds of
high and rising energy prices and steadily climbing interest rates.
Impressively, stock prices have continued to advance with each of the past four
quarters recording positive rates of return. In fact, the March quarter was the
best first quarter for the S&P since Value stocks have outperformed
growth, and smaller capitalization companies continue to lead larger stocks with
mid-cap a strong second place finisher. Fixed-income returns remain anemic
thanks to tighter monetary policy, strong economic growth and rising inflation.

MARKET OUTLOOK
- -------------------

In terms of the economy, we believe that the expansion remains on solid
ground; however, our belief is that three factors remain as possible drags on
future economic performance. These include a global tightening of liquidity by
many of the major economic powers (including the United States), rising interest
rates and an unsustainable current account deficit. The Federal Reserve has
raised rates at consecutive meetings by a quarter of a point from to the
current level of Based on declining unemployment claims and continued
strong loan demand, we expect short-term rates to rise from current levels.
Long-term rates have pushed higher with the Treasury recently
penetrating the yield level and the Treasury also above

As for the financial markets, these factors are likely to result in continued
lackluster returns for the intermediate term. We believe the U.S. equity market
will remain range bound until the Federal Open Market Committee's (FOMC) job is
complete and liquidity levels can return to an expansion phase. The market is
currently selling at approximately times forward earnings, down significantly
from the levels of months ago and at the top of the bull market of the
and Company performance confirms that global economic conditions overall
have been favorable despite the negatives, and that many companies are
demonstrating strong financial performance. Corporate balance sheets in general
should remain solid, and we will continue our focus on stock selection. While
we believe that there remains more risk than upside potential at today's
valuation, we will remain a careful shopper, looking for good long-term
investment opportunities as they arise. At this point in time, we have no plans
to establish an allocation to fixed income securities. We will continue to be
opportunistic in our search for companies exhibiting good fundamental
improvement combined with a reasonable valuation.

We thank you for the opportunity to serve as investment advisor to the Hester
Total Return Fund and look forward to the new year.

Sincerely,
