Dear Fellow Shareholder,

It is our pleasure to provide you with the American Trust Allegiance Fund Annual
Report for the twelve months ending February

During these twelve months the Fund grew nicely, realizing a total return of
essentially in line with its benchmark, the Standard & Poor's Index
which had a total return of during the same period.

The Fund's overweighting in utilities certainly helped performance as longer
term interest rates stayed surprisingly steady despite the Federal Reserve
Board's constant increase of short-term rates. The overweighting in energy
stocks aided performance as well. The price per barrel rose steadily and with
other commodities threatened to push inflation higher. However, with
significant productivity gains (alluded to below) inflation remained quiescent
which helped keep stock prices on track. The focus on industrial and financial
services companies also helped performance as economic growth aided the earnings
of these companies. The two major sectors that were particularly weak during
this period were consumer discretion and consumer staples which
underperformed the S&P Index and dragged down Fund performance.

The last months have not only seen continued economic growth in the U.S but
also worldwide. Though fair trade practices of the U.S. have resulted in a
large trade deficit, it has also generated the beginnings of a middle class in
many nations and economic dependency with China and many other nations that have
never before had a stake in protecting the viability of our economy. Though
foreign governments have financed much of our public debt, they also can no
longer afford to hurt their major trading partner by suddenly withdrawing that
support. This world interdependency is a healthy development for world economic
growth as well as for better working relationships between disparate countries
and cultures. These developments have fostered strong economic and stock market
growth in overseas markets over the past months relative to the U.S. market.
This trend is also widely expected to continue. However, the International
Monetary Fund (IMF) predicts that the U.S. economy will enjoy the fastest growth
of the advanced economies in Couple that with the fact that valuations of
quality large cap growth stocks including many multinationals are near the
bottom of their range and one can reasonably argue that U.S. stocks will
provide stronger performance relative to international stocks in

Another prime determinant of stock performance is Federal Reserve Board (Fed)
monetary policy. No one knows when the Fed will stop raising interest rates.
They have now raised rates of a point consecutive times. We do know
however, that the Fed is closer to the end of the tightening process than to the
beginning. Most economists believe the Fed will stop raising rates near the
middle of Historically, this has resulted in a stock market rally. In
any case, the advent of a more accommodative Federal Reserve policy should help
U.S. stocks.

One nagging concern is the price of oil and other commodities adding to
inflation. The Federal Reserve pointed out, however, in its March
monetary report, that strong productivity gains in the U.S. have effectively
mitigated the effects of higher raw material costs. Not surprisingly, and
somewhat related, spending in the U.S. on technology has been very strong and
has now even surpassed the peak before the last recession. Despite strong
technology sales, many of these leading companies are still selling near their
lowest valuations in years. We expect that, with the moderate strength of the
U.S. economy in stock valuation levels will improve. However, if the
economy should slow unexpectedly, the Federal Reserve should provide supportive
monetary policy to strengthen the capital markets.

We are grateful to you for your support of the American Trust Allegiance Fund
and we hope that, in return, we can help you meet your financial goals.

Sincerely,
