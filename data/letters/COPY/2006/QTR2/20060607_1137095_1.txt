Dear Fellow Shareholders,

We are pleased to share with you that it has been another great year for the
Generation Wave Growth Fund (the "Fund"). As of March Morningstar
ranked the Fund in the top of large-cap growth funds for the three year
period then ended and in the top among large-cap growth funds for the
year period. The Fund received a FIVE-STAR OVERALL MORNINGSTAR RATING FOR THE
THREE YEAR PERIOD AMONG LARGE-CAP GROWTH FUNDS (DERIVED FROM A WEIGHTED
AVERAGE OF THE FUND'S THREE-YEAR MORNINGSTAR RATINGS METRICS, WHICH ARE BASED ON
RISK-ADJUSTED RETURN PERFORMANCE.). In fact, through March the Fund
has outperformed the S&P Index over the past months, months, year,
years, and since its inception in June (See page for complete
performance information).

As you know, the overall investment philosophy of the Generation Wave Growth
Fund is to invest in sectors of the market that will most likely be impacted by
the spending habits of the massive Baby Boomer population. That means the Fund
is largely concentrated in the technology, healthcare and financial services
sectors, which will all be greatly influenced by the demographic shift already
underway.

For the most part, the technology and financial sectors have been a good place
to focus. Over the past year, the financial and technology sectors have been
leading areas of the market, both of which outperformed the S&P Index over
the twelve month period ending March which greatly contributed to the
performance of the Fund. Healthcare lagged the other sectors this past year,
underperforming the S&P Index, but we still remain optimistic about
healthcare over the long-haul. Baby Boomers will continue to need more
healthcare as they age, and people are living longer these days than ever
before.

On a separate note, we would encourage you to look up our underlying fund-of-
fund holdings to see that many of them are either institutional class shares or
are closed to new investors, which make them difficult if not impossible for
individual investors to purchase.

Remember that as a shareholder you can electronically invest into your account
on a monthly basis through your checking or savings account via an ACH. For
more information please call our shareholder service department at
As always your suggestions for additional improvements are always welcome
and can be submitted via e-mail at our website.

PLEASE REMEMBER THAT DETAILED FUND INFORMATION INCLUDING HOLDINGS AND
PERFORMANCE, UPDATED MONTHLY, ARE ALWAYS AVAILABLE AT WWW.GENWAVEFUNDS.COM. IT
IS ALSO AVAILABLE THROUGH WWW.MUTUALS.COM, UNDER THE LINK FOR GENERATION WAVE
FUND.

Thank you for being a valued shareholder of the Generation Wave Growth Fund.

/s/Charles L. Norton /s/Michael J. Henry

Charles L. Norton, CFA Michael J. Henry
Portfolio Manager Portfolio Manager

Dear Fellow Shareholders,

The past year was another good one for the Vice Fund (the "Fund"). In the twelve
month period ending March the policy-setting Federal Open Market
Committee raised rates eight times; commodity prices soared - to nominal all-
time highs, in some cases; the yield curve flattened dramatically; and
geopolitical tensions heated up, particularly in the Middle East. Yet, none of
these factors have had any meaningful impact on people's desire to drink, smoke
and gamble - or our nation's need to protect itself.

As a result, many of the companies in our portfolio, such as Altria Group, MGM
Mirage and Constellation Brands, reported record earnings.

For the one-year period represented by this report, the Fund returned
while the Standard & Poor's Index gained only even including
dividend income. This strong performance builds on the successes we have had
since the Fund's inception in August In fact, ACCORDING TO LIPPER, THE
FUND'S THREE-YEAR RETURN AS OF MARCH RANKS IT IN THE TOP AMONG
FUNDS IN THE MULTI-CAP CORE CATEGORY. THE FUND ALSO RANKS IN THE TOP AMONG
FUNDS, ACCORDING TO LIPPER, FOR THE ONE-YEAR PERIOD ENDING MARCH
More recently, derived from a weighted average of the Fund's three-year
Morningstar Ratings metrics, which are based on risk-adjusted return performance
through April the Fund received a five-star Overall Morningstar rating
among Mid-Cap Blend funds for the first time in its history, and assets in
the Fund have never been higher.

In periods characterized by relatively unexciting equity returns and a tepid
economy, the industries that the Fund focuses on, because of their defensive
nature, have historically beaten the broad market. Yet, despite solid economic
growth and all of the major stock indices nearing multi-year highs, the Vice
Fund has still managed to greatly outperform the market - a testament to the
complementary mix of industries that comprise this Fund. In bad times, the
value-oriented tobacco and alcohol sectors provide our anchor, while the fast-
growing gaming sector and select defense companies give the Fund a boost when
the economy and markets are strong.

The end result is what we hope will prove to be a Fund that performs solidly in
all seasons.

PLEASE REMEMBER THAT DETAILED FUND INFORMATION INCLUDING HOLDINGS AND
PERFORMANCE, UPDATED MONTHLY, IS ALWAYS AVAILABLE AT WWW.VICEFUND.COM.

Thank you for being a valued shareholder of the Vice Fund.

/s/Charles L. Norton /s/Michael J. Henry

Charles L. Norton, CFA Michael J. Henry
Co-Portfolio Manager Co-Portfolio Manager
