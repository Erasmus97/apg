Dear Fellow Shareholders:

For the fiscal year ended February The Turnaround Fund had a total
annual return of This annual return was below the S&P annual return
of For The Turnaround Fund's most up-to-date performance information, see
our website at www.theturnaroundfund.com.


Management Discussion

While we are disappointed by the overall performance of the Fund during the
year, we continue to be confident about the long-term value of the holdings in
the Fund's portfolio. We buy "turnaround companies" - companies that are or have
been struggling with one or more business issues, such as management issues
(e.g., mismanagement or management changes), operational issues (e.g., product,
services or customer issues) or financial issues (e.g., balance sheet issues).
We buy stocks that have been beaten down by the market, where our calculation of
value greatly exceeds the stock quote.


Portfolio Activity

During the fiscal year one of our largest positions, E-Loan, was taken over at a
premium by Popular, Inc. We eliminated a position in Doral Financial (DRL)
at a substantial loss. This position was a mistake, in retrospect. While the
business appeared strong based on their financial statements, those statements
turned out not to be reliable.

In addition, we took advantage of excess pessimism surrounding several companies
to add to, or initiate positions: Overstock (OSTK), Wal-Mart (WMT), Home Depot
(HD), and JetBlue (JBLU). Worry about short-term business conditions and/or
macro variables allowed us to purchase these high-quality assets at a fraction
of our fair value calculations.

We continue to have a high degree of confidence in positions initiated in the
prior fiscal year. For example, it was announced recently that North Fork
Bancorporation is going to be taken over by Capital One. If Commerce Bank, our
largest position, was valued similar to North Fork's takeout price, it would be
worth about per share, based on our comparable book value and deposit
premium calculation. At our fiscal year end, our Commerce position was quoted at


If we could magically control the timing of stock quotes, we would have a simple
request. We'd like to have our holdings priced at levels consistent with our
fair value calculations once per year, as of the end of February, our fiscal
year end. Then our shareholders would be able to see the real underlying value
of their holdings. While this would make us feel good, it's not necessary. We
know what our assets are worth. We know that, given enough time, the market
recognizes value.


The Turnaround Fund

As of our fiscal year-end, we owned shares in different businesses, and we
expect to continue to maintain a concentrated portfolio. We don't expect that
number to change appreciably in the near future. Because of this level of
concentration, we expect our performance will be irregular and volatile.
Generally, you should expect our performance to be significantly above or below
the S&P over any given year.



Arne Alsin Glenn Surowiec
arne@theturnaroundfund.com glenn@theturnaroundfund.com
- -------------------------- ---------------------------




The Turnaround Fund(TM)

Performance Update - Investment (Unaudited)
