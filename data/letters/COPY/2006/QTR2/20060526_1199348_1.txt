Dear Shareholder:


We are pleased to present this semiannual report for Dreyfus Premier International Equity Fund, covering the six-month period from October through March

International stocks have continued to advance strongly,substantially outpacing their U.S. counterparts, on average, over the past six months. European mergers-and-acquisitions activity has been a key driver of
international equity performance as merger frenzy has taken hold among utilities and financial companies in the region. The rallying Japanese market has been fueled by a revitalized domestic economy in the wake of long-awaited banking
reforms. Finally, the emerging markets have continued to benefit from the adoption of sound fiscal policies and robust global demand for the natural resources they produce.

Our chief economist, Richard Hoey, currently expects continued global economic growth, which could support business fundamentals for many international companies. However, the global economic expansion may become more
balanced as domestic demand expands and local economies rely less on exports to the United States. Clearly, changes in the global economic climate may benefit some areas of the international stock markets more than others. As always, we encourage
you to talk with your financial advisor to discuss investment options and portfolio allocations that may be suitable for you in this environment.

For more information about how the fund performed, as well as information on market perspectives, we have provided a Discussion of Fund Performance given by the funds portfolio managers.




Stephen E. Canter




Chairman and Chief Executive Officer




The Dreyfus Corporation




April











DISCUSSION OF FUND PERFORMANCE

Remi J. Browne, CFA and Peter S. Carpenter, CFA, Portfolio Managers

The Boston Company Asset Management, LLC, Investment Adviser

How did Dreyfus Premier International Equity Fund perform relative to its benchmark?

For the six-month period ended March the fund produced total returns of for its Class A shares, for its Class B shares, for its Class C shares, for its Class R shares and for its
Class T In comparison, the funds benchmark, the Morgan Stanley Capital International Europe, Australasia and Far East Index (MSCI EAFE Index),
produced a total return of for the

International stocks generally gained value during the reporting period, as economic conditions in Europe and Japan continued to exceed many analysts expectations, and most companies reported rising profits. However,
the funds returns slightly lagged that of the MSCI EAFE Index, partly because investors in Japan favored more expensive growth-oriented companies over stocks with attractively valued business momentum.

What is the funds investment approach?

The fund seeks long-term growth of capital. The fund is a feeder fund that invests all of its assets in a master portfolio known as The Boston Company International Core Equity Portfolio, which is a
series of the Mellon Institutional Funds Master Portfolio.The fund (as used herein, fund refers to the Dreyfus fund and the Master Portfolio) invests, under normal circumstances, at least of its assets in shares of companies located
in the foreign countries represented in the MSCI EAFE Index and Canada. Although we generally seek to allocate assets among countries in accordance with the MSCI EAFE Index, deviations may occur. Similarly, we use the industry group allocations of
the MSCI EAFE Index as a guide, but allocations may differ from those of the MSCI EAFE Index.The fund also may invest

The Fund







up to of its assets in securities of issuers located in emerging market countries, but no more than of its assets may be invested in issuers located in any one emerging market country.

The fund invests in stocks that appear to be undervalued (as measured by their price/earnings ratios) and that may have value and/or growth characteristics. We employ a bottom-up investment approach, which emphasizes
individual stock selection.

The stock selection process is designed to produce a diversified portfolio that, relative to the MSCI EAFE Index, has a below-average price/earnings ratio and an above-average earnings growth rate.

What other factors influenced the funds performance?

Stocks throughout the developed world posted attractive gains during the reporting period, led by markets in Portugal, Finland and Germany. As economic conditions in Europe improved, corporate earnings growth accelerated.
In addition, mergers-and-acquisitions activity rebounded, government regulations on businesses were relaxed and many companies renegotiated labor agreements. Positive macroeconomic factors also helped support stock prices in Japan, which appears
finally to have emerged from longstanding economic doldrums after implementing reforms to its banking system. As a result, Japans domestic economy benefited from more robust consumer spending, complementing ongoing strength among
export-oriented businesses.

Basic materials and industrial companies generally demonstrated that they had the pricing power to prosper despite volatile energy and commodity prices. For example, shares of German tire and automobile parts maker
Continental AG rose sharply when the company demonstrated that it could pass on rising raw materials prices to customers. Rautaruukki Oyj, a Finnish steel maker, benefited from robust industrial demand in the emerging markets, which enabled it to
implement relatively large price increases.

In other market sectors, Frances financial services giant Societe Generale posted earnings that exceeded investors expectations due to greater trading and investment banking activity in global capital
markets.







Increased mergers-and-acquisitions activity also benefited the fund, as German pharmaceutical company Schering KGA received a takeover bid from Merck &amp; Co, only to have that bid topped by Bayer AG.

Although the funds Japan investments participated in the markets rise to a significant degree, their returns generally lagged that of the benchmarks Japan component. The value component of our investment
approach proved to be out of favor among Japanese investors, who preferred companies selling at higher valuations. In addition, some of the funds holdings in Japan posted disappointing earnings. Shipping company Mitsui O.S.K. Lines Ltd., tire
and sporting goods manufacturer Sumitomo Rubber Industries and consumer finance provider Sanyo Shinpan Finance Co. generally were not able to pass along higher costs for energy, raw materials and capital, respectively, to their customers.

What is the funds current strategy?

We have continued to allocate the funds assets to geographic regions and industry groups in proportions similar to those of the MSCI EAFE Index. We believe that this approach enables us to focus more intently on
adding value through our security selection strategy.








The investment adviser to the Master Portfolio is The Boston Company Asset Management,









LLC, an affiliate of Dreyfus.









Total return includes reinvestment of dividends and any capital gains paid, and does not take into

Dear Shareholder:


We are pleased to present this semiannual report for Dreyfus Premier International Small Cap Fund, covering the six-month period from October through March

International stocks have continued to advance strongly, substantially outpacing their U.S. counterparts, on average, over the past six months. European mergers-and-acquisitions activity has been a key driver of
international equity performance as merger frenzy has taken hold among utilities and financial companies in the region. The rallying Japanese market has been fueled by a revitalized domestic economy in the wake of long-awaited banking
reforms. Finally, the emerging markets have continued to benefit from the adoption of sound fiscal policies and robust global demand for the natural resources they produce.

Our chief economist, Richard Hoey, currently expects continued global economic growth, which could support business fundamentals for many international companies. However, the global economic expansion may become more
balanced as domestic demand expands and local economies rely less on exports to the United States. Clearly, changes in the global economic climate may benefit some areas of the international stock markets more than others. As always, we encourage
you to talk with your financial advisor to discuss investment options and portfolio allocations that may be suitable for you in this environment.

For more information about how the fund performed, as well as information on market perspectives, we have provided a Discussion of Fund Performance given by the funds portfolio manager.




Stephen E. Canter




Chairman and Chief Executive Officer




The Dreyfus Corporation




April











DISCUSSION OF FUND PERFORMANCE

Daniel LeVan, CFA, Portfolio Manager

The Boston Company Asset Management, LLC, Investment Adviser

How did Dreyfus Premier International Small Cap Fund perform relative to its benchmark?

For the six-month period ended March the fund produced total returns of for its Class A Shares, for its Class B Shares, for its Class C shares, for its Class R shares and for its
Class T In comparison, the funds benchmark, the S&amp;P/Citigroup Extended Market Ex-U.S. Index (EM Ex-U.S. Index), produced a total return of
for the same

Despite rising interest rates and concerns about high energy prices, international small-cap stocks posted attractive results over the reporting period, as investor demand for smaller, non-U.S. companies continued to grow.
Nearly every country represented in the EM Ex-U.S. Index posted positive returns, and all of the benchmarks industry groups gained value.The fund was able to participate in this favorable environment, and outperformed its benchmark mainly due
to strong stock selections in the industrials and materials sectors.

What is the funds investment approach?

The fund seeks long-term growth of capital. The fund is a feeder fund that invests all of its assets in a master portfolio known as The Boston Company International Small Cap Portfolio, which is a
series of the Mellon Institutional Funds Master Portfolio.The fund (as used herein, fund refers to the Dreyfus fund and the Master Portfolio) invests, under normal circumstances, at least of its assets in stocks that are located in
foreign countries represented in the EM Ex-U.S. Index. The fund normally invests at least of its assets in companies that comprise the EM Ex-U.S. Index.Although we generally seek to allocate assets among countries in accordance with the EM
Ex-U.S. Index, deviations may occur. Similarly, we use the sector group allocations of the EM Ex-U.S. Index as a guide, but allocations may differ

The Fund







from those of the EM Ex-U.S. Index.The fund also may invest up to of its assets in securities of issuers located in emerging market countries, but no more than of its assets may be invested in any one emerging market
country.

Our approach is to employ a bottom-up stock selection process that emphasizes individual stock selection. The portfolio manager uses quantitative models and traditional qualitative analysis to construct a
diversified portfolio of stocks with low relative price multiples and positive trends in earnings forecasts.

What other factors influenced the funds performance?

A healthy global economy, growing corporate profits and greater mergers-and-acquisitions activity helped fuel sharp gains among international small-cap stocks during the reporting period. The European economy has
experienced particular improvement in recent months, and companies doing business there have reported higher profits and an increase in takeover offers. These positive factors more than offset the potentially eroding influences of ongoing
geopolitical tensions in the Middle East, volatile energy prices and, in many regions of the world, rising interest rates.

The fund outperformed its benchmark primarily in two sectors: materials and industrials. Although they faced higher energy prices, materials and industrials companies generally were able to pass these costs along to their
customers. For example, French specialty steel products manufacturer Vallourec was able to raise prices amid robust demand for seamless steel tubes used to drill for oil and natural gas. Tandberg Television ASA, a Norwegian manufacturer of high
definition broadcasting equipment, also benefited from pricing power in a booming market for its products.

While higher energy costs represented a challenge for many companies, they benefited others. For example, Japans Mitsubishi Gas &amp; Chemical Co., Inc. reported strong earnings due to rising demand for methanol,
which is considered by many to be a viable alternative energy source.







Although we encountered some disappointments over the reporting period, they were not enough to prevent the fund from outperforming its benchmark. Our investments in Japan generally lagged the averages, as the
countrys stock market proved to be relatively choppy in early after posting impressive gains in The fund received especially weak contributions from Japanese real estate investment companies such as Urban and Kenedix.
Disappointments in the health care sector included iSOFT Group PLC, a United Kingdom-based medical software company.

What is the funds current strategy?

We have continued to employ our strategy of roughly matching the benchmarks allocations to various geographical regions and market sectors, which we believe enables us to focus more intently on adding value through
our bottom-up security selection strategy.Although valuations have become more expensive after the markets recent advance, we continue to believe that international small-cap stocks remain attractive due to robust demand from investors
worldwide. In addition, as of the end of the reporting period, international small-cap stocks generally have traded at a discount to their U.S. counterparts, which suggests to us that there is room for further gains.








The fund invests in a Master Portfolio that has the same investment objective and policies as









the fund.This is known as a master/feeder arrangement.The investment adviser to the Master









Portfolio is The Boston Company Asset Management, LLC, an affiliate of Dreyfus. References to









the fund in this report generally mean the fund and the Master Portfolio in which it invests.









Total return includes reinvestment of dividends and any capital gains paid, and does not take into

Dear Shareholder:


We are pleased to present this semiannual report for Dreyfus Premier Small Cap Equity Fund, covering the six-month period from October through March

After posting relatively mild gains during much of U.S. stocks began to show signs of renewed vigor in the first quarter of As they have for some time, smaller-cap stocks rose more sharply than large-cap shares
in this current market environment in which investors have responded positively to reports of continued economic growth and robust business fundamentals in most industry groups. In fact, it recently was announced that, over the final three months of
last year, pretax corporate profits rose at the fastest pace since

While our chief economist, Richard Hoey, currently expects U.S. economic growth to continue, the economic expansion may begin to rely less on consumer spending and more on corporate capital investment, exports and
non-residential construction. In addition, inflationary pressures may increase moderately due to tighter labor markets and robust demand for goods and services. Clearly, changes in the economic climate might benefit some types of companies and
industries more than others.As always, we encourage you to talk with your financial advisor to discuss investment options that may be suitable for you in this environment.

For more information about how the fund performed, as well as information on market perspectives, we have provided a Discussion of Fund Performance given by the funds portfolio managers.

Thank you for your continued confidence and support.




Stephen E. Canter




Chairman and Chief Executive Officer




The Dreyfus Corporation




April











D I S C U S S I O N O F F U N D P E R F O R M A N C E

Joseph M. Corrado, CFA, and Stephanie K. Brandaleone, CFA, Portfolio Managers The Boston Company Asset Management, LLC, Investment Adviser

How did Dreyfus Premier Small Cap Equity Fund perform relative to its benchmark?

For the six-month period ended March the fund produced total returns of for its Class A Shares, for its Class B Shares, for its Class C shares, for its Class R shares and for its
Class T In comparison, the funds benchmark, the Russell Value Index, produced a total return of for the same period.


As they have for some time, small-cap stocks continued to outperform their large-cap counterparts, as strong economic growth and rising corporate earnings whetted investors appetite for risk. However, investors
preferred lower-quality companies over the higher-quality businesses we favor. As a result, despite strong absolute returns for the period, the fund produced lower returns than its benchmark.

What is the funds investment approach?

The fund seeks long-term growth of capital.The fund is a feeder fund that invests all of its assets in a master portfolio known as The Boston Company Small Cap Value Portfolio, which is a series of
the Mellon Institutional Funds Master Portfolio. The fund (as used herein, fund refers to the Dreyfus fund and the Master Portfolio) invests, under normal circumstances, at least of its assets in the stocks of small-cap U.S.
companies, and may invest up to of its assets in foreign companies.

We employ a value-based investment style, in which we seek to identify companies with stocks trading at prices below what we believe to be their intrinsic values.We measure value by evaluating each companys valuation
multiples (price/earnings, price/sales, price/cash flow), current competitive position and expected business growth relative to its industry.

We focus primarily on individual stock selection instead of trying to predict which industries or sectors will perform best.The stock selection

The Fund







process is designed to produce a diversified portfolio of companies that we believe are undervalued relative to expected business growth, with the presence of a catalyst (such as a corporate restructuring, change in
management or spin-off) that will trigger a price increase.

What other factors influenced the funds performance?

Despite consecutive increases of short-term interest rates since June the rates at which companies borrow have remained relatively low by historical standards, and even marginal companies have had ample access to
capital.As a result, investors apparently have remained comfortable investing in riskier stocks that do not meet our earnings, return-on-equity and valuation criteria. As a result, the stocks of lower-quality small-cap issuers outperformed their
higher-quality counterparts during the reporting period, causing the funds returns to lag its benchmark.

Still, the fund participated to a substantial degree in the small-cap markets gains, achieving above-average returns in a number of market sectors. In the financials area, the fund benefited both from relatively light
exposure to the sector overall and our security selection strategy. We generally avoided consumer finance companies, focusing instead on firms that we believed would prosper due to greater activity in the capital markets, including Raymond James
Financial Inc., Piper Jaffray Companies, Inc. and Knight Capital Group Inc.The fund also received strong contributions from diversified commercial banks Sterling Financial Corp. and Sterling Bancshares Inc., which are not related, and real estate
investment trusts Jones Lang Lasalle, Strategic Hotels &amp; Resorts and Education Realty Trust.

The funds overweighted position in the energy area helped it participate more fully in the sectors gains. Successful stock selections benefiting from favorable supply-and-demand factors in the energy market
included Foundation Coal Holdings, a relatively recent purchase, as well as equipment and services providers Tetra Technologies and Dril-Quip. In addition, industrial company United Rentals Inc., the largest equipment rental company in the United
States, rose on the strength of the expanding U.S. economy.

Although the funds information technology and health care holdings produced positive absolute returns for the reporting period, both areas







lagged their respective components in the benchmark due to our emphasis on higher-quality companies. In addition, in the technology area, encryption software developer SafeNet Inc. was hurt by disappointing earnings and
their CFO resignation, RFID solutions provider Comtech Telecommunications was undermined by a lower-than-expected earnings forecast, and broadband account management software supplier Motive Inc. suffered from weaker-than-expected earnings and a
restatement. Disappointments among health care companies included home health care providers Apria Healthcare Group Inc., which encountered unstable business conditions, and Amedisys Inc., where earnings fell short of expectations related to an
acquisition.

What is the funds current strategy?

As of the end of the reporting period, we have found a number of opportunities in the energy and health care sectors. Smaller energy companies have continued to benefit from favorable supply-and-demand dynamics, while
certain managed health care providers appear poised to benefit from greater outsourcing of specialty services, save the government or commercial payers money, and have direct contact with consumers. In addition, we believe that investors
preference for lower-quality companies may diminish when the U.S. economy moves to the next phase of its cycle, and we expect attention to turn once again to companies with higher-quality earnings and improving returns-on-equity.








The investment adviser to the master portfolio is The Boston Company Asset Management, LLC, an









affiliate of Dreyfus.









Total return includes reinvestment of dividends and any capital gains paid, and does not take into
