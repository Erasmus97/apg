Dear Fellow Shareholder

In recent months, we have witnessed the continuing vibrancy of the current economic expansion, now in its fifth year. U.S. businesses have seized opportunities available both at home and abroad to generate some of the most
impressive profit margins in history, by some measures. During your funds reporting period, common stocks have traded at higher levels to reflect improving corporate profits. However, the gains have not come without concerns in some quarters
of the market about the risks facing the economy. These risks include high energy prices, inflation, and a potential pullback in consumer spending, as well as the potential adverse effects of the Federal Reserves (the Feds) series of
interest-rate increases. Concerns about inflation, in particular, have been reflected in falling bond prices and rising bond yields, and worries about consumer spending have clouded the outlook for stocks.

You can be assured that the investment professionals managing your fund are closely monitoring the factors that are influencing the performance of the securities in which your fund invests. Moreover, Putnam Investments
management team, under the leadership of Chief Executive Officer Ed Haldeman, continues to focus on investment performance and remains committed to putting the interests of shareholders first.











In the following pages, members of your funds management team discuss the funds performance and strategies for the fiscal period ended April and provide their outlook for the months ahead. As always, we thank
you for your support of the Putnam funds.






Putnam Income Fund: seeking high
current income across a broad range of fixed-income
securities
Over Putnam Income Funds
history, the bond landscape has undergone a dramatic transformation. One-third
of the U.S. investment-grade sector, the funds primary focus, is now composed of securitized debt instruments,
including mortgage- and asset-backed securities. The high-yield corporate bond
sector, which was established in the late has also grown significantly
and is now considered a mature asset class. And outside the United States, there
are new opportunities in the debt of developed and emerging-market
countries.
Amid this evolution of the
fixed-income markets, the investment objective of Putnam Income Fund has remained constant. In a letter to Putnam
shareholders in George Putnam, Jr. (who is currently Chairman Emeritus of
the Putnam Funds), expressed it this way: We have in mind those people who need
a liberal current return Mr. Putnams choice of current return rather than
current income captures the investment philosophy of the team managing your
fund today: that high current income should be pursued within a total return
context and that risk management is as important as yield in maintaining a high
current income stream.
Successful investing in todays
global bond market requires broad expertise. Putnams fixed-income
group is divided into teams of specialists who focus on varied investment
opportunities. Each team identifies compelling opportunities within its area

Optimizing the risk/return trade-off
across multiple sectors
Putnam believes that building a
diversified funds objectives. The funds portfolio is portfolio with multiple
income-generating composed of a broad spectrum of government, strategies is the
best way to pursue your credit, and securitized debt instruments.


of expertise. Your funds management
team selects from among these opportunities, systematically building a
diversified portfolio that carefully balances risk and return.
Lower-rated bonds may offer higher
yields in return for more risk. Mutual funds that invest in government
securities are not guaranteed. Mortgage-backed securities are subject to
prepayment risk. Mutual funds that invest in bonds are subject to certain risks,
including interest-rate risk, credit risk, and inflation risk. As interest rates
rise, the prices of bonds fall. Long-term bonds are more exposed to
interest-rate risk than short-term bonds. Unlike bonds, bond funds have ongoing
fees and expenses. The use of derivatives involves special risks and may result
in losses.
Key fixed-income return sources

Government: Interest-rate levels are a primary driver of government
bond performance. Generally, bond prices decline when interest rates rise, and
rise when interest rates fall. Interest rates and bond yields rise and fall
according to investor expectations about the overall health of the
economy. Credit: Corporate bond performance tends to track the health of
the overall economy more closely than other bonds. These bonds are less
sensitive to interest-rate movements; they tend to perform well when the economy
strengthens, often in spite of the higher rates that accompany stronger
growth.
Securitized: Interest-rate cycles also affect mortgage- and
asset-backed securities (MBSs/ABSs). Because MBSs are the securitized cash flows
of mortgages, prepayment rates are another consideration. For ABSs, managers
monitor the credit quality of the underlying assets, which comprise the
securitized cash flow of anything from credit card debt to manufactured housing
debt.



Putnam Income Fund
seeks high current income consistent with what
Putnam Management believes to be a prudent level of risk. The fund invests in a
diversified portfolio composed mainly of corporate investment-grade bonds, U.S.
government and agency bonds, and collateralized mortgage obligations.




Highlights

*
During the semiannual period ended
April Putnam Income Funds class A shares


had a total return of without
sales charges.

*
The funds benchmark, the Lehman
Aggregate Bond Index, returned

*
The average return for the funds
Lipper category, Corporate Debt Funds A Rated,


was

*
Additional fund performance,
comparative performance, and Lipper data can be found in
the


performance section beginning on page

Performance
Total return for class A shares for
periods ended



Since the fund's
inception average annual return is at NAV and at
POP.







Average annual
return
Cumulative
return


NAV
POP
NAV

POP




years









years









years









year









months








Data is historical. Past
performance does not guarantee future results. More recent returns may be less
or more than those shown. Investment return and principal value will fluctuate,
and you may have a gain or a loss when you sell your shares. Performance assumes
reinvestment of distributions and does not account for taxes. Returns at NAV do
not reflect a sales charge of . For the most recent month-end performance,
visit www.putnam.com. For a portion of the period, this fund limited expenses,
without which returns would have been lower. A short-term trading fee of up to
may apply.




The period in review
Continuing indications of solid economic growth,
and the desire to curb the potential inflation that frequently accompanies such
growth, prompted the Fed to continue to raise interest rates during the first
half of your funds fiscal year. As interest rates and bond yields rose,
bond prices generally fell. However, interest income from the bonds in your
funds portfolio helped to offset these falling bond prices and resulted in
modestly positive returns, based on net asset value (NAV, or without sales
charges). These results were in line with the average for the funds Lipper
group, but slightly behind those of its benchmark. We attribute the
underperformance relative to the benchmark to an underweight position in
corporate bonds, which strengthened due to continued strong corporate profits
and solid economic growth. Performance benefited from a defensive, or short,
duration strategy amid rising interest rates and a positioning strategy that we
shifted to take advantage of changes in the yield curve. Also, the funds
strategy in the securitized bond sector contributed positively to returns while
its underweight position in dollar-based emerging-market debt detracted from
returns.
Market overview
The market environment over the past six
months has been challenging for bonds. Interest rates have continued to rise
amid signs of stronger-than-expected economic growth. Bond investors closely
watch the pace of economic growth, as it can lead to rising inflation, which
erodes the value of fixed-income investments. Growth in the United States
remained robust, with a surprisingly high first-quarter gross domestic product (GDP) annualized growth rate of
continued strong corporate profits, and relatively low unemployment. As a
consequence, the Fed raised the federal funds rate four times during the period,
pushing up yields on U.S. bonds, which caused their prices to fall.
Economic growth outside the United States
is also having an impact on



yields and prices of bonds issued here.
Demand from non-U.S. investors for higher-yielding U.S. issues has been a key
factor in keeping yields relatively low, particularly among longer-maturity
bonds. Most noteworthy is the shift of Japans economy into a growth mode after
a long slump. This shift prompted the Bank of Japan to abandon its extremely
loose monetary policy, causing yields on Japanese government bonds to rise.
Higher yields in Japan could result in higher yields worldwide, because as
Japanese investors begin to divert some of their capital out of the
international markets and back to their domestic markets, bond issuers worldwide
could be challenged to raise yields to attract these investors.
Performance among credit-sensitive issues,
such as corporate bonds and emerging-market debt, benefited from improving
economies and rising corporate earnings. In general, the lower its credit
rating, the stronger a bonds performance was during the period, as strong
demand from buyers in search of higher yields drove prices higher.
Strategy overview
Your fund employs multiple
income-generating strategies across the different U.S. investment-grade bond
sectors in pursuit of its objectives. We believe that having diversified return
sources contributes to more consistent results over time and helps to manage
risk. Generally, our investment decisions



Market sector
performance


These indexes provide an
overview of performance in different market sectors for the



six months ended









Bonds





Lehman Aggregate Bond Index
(broad bond market)






Lehman Global Aggregate Bond
Index (international bonds)






Lehman Government Bond Index
(U.S. Treasury and agency securities)






Lehman GNMA Index (Government
National Mortgage Association bonds)






Equities





S&amp;P Index (broad stock
market)






Russell Growth Index
(small-company growth stocks)






Russell Value Index
(small-company value stocks)








involve the following considerations:
duration management, yield curve positioning, sector allocation, and security
selection.
In anticipation of continued rising
interest rates, we maintained a short (defensive) duration profile for the fund
a strategy that helped performance. Duration, which is measured in years, is
the primary indicator of interest-rate sensitivity. The shorter a bonds
duration, the less sensitive its price will be to interest-rate
changes.
The fund also benefited from its yield
curve positioning. The yield curve is a graphical representation of bond yields
with the same quality plotted from the shortest to the longest maturity. As the
fiscal year began, the fund was positioned to
take advantage of yield-curve flat-tening. Flattening occurs when yields on
short- and longer-term securities converge, as was the case early in the period
when short-term rates rose faster than long-term rates. However, based on our
conviction that conditions were in place for long-term rates to rise, we shifted
our strategy to position the fund to benefit from expected yield-curve
steepening. This shift helped results as long-term rates did rise later in the
period.
In terms of sector and security
considerations, we have sought to reduce the level of credit risk in the
portfolio over the past two years by reducing the funds exposure to corporate
and emerging-market debt. (Credit risk is the risk that a bond issuer could
default
* Cash exposure includes various
derivative investments.



and fail to pay interest and repay
principal in a timely manner.) As interest rates rise, it becomes more expensive
for issuers to borrow money. Increased interest-rate expenses are particularly
burdensome for highly leveraged companies or economies. While we expect this
strategy to prove rewarding over the long term, it detracted from short-term
results as these sectors outperformed during the semiannual period.
Your funds holdings
The securitized bond sector remained a key
area of focus for the fund. This growing sector is now among the largest within
the investment-grade bond universe. The most common securitized bonds are
mortgage-backed securities (MBSs) issued by the Federal
National Mortgage Association (Fannie Mae)
and the Government National Mortgage
Association (Ginnie Mae). Other types of
securitized bonds include asset-backed
securities (ABSs), which are typically backed
by car loans and credit card payments, and commercial mortgage-backed securities (CMBSs), which are backed by loans on large commercial real estate
projects, such as office parks or shopping malls.
CMBSs were an area of particular focus as we sought to replace the credit risk
of corporate bonds with more diversified credit exposure. Securitized bonds
typically offer higher income than corporate bonds of comparable credit quality.
Another reason for
Credit quality overview
Credit qualities shown as a
percentage of portfolio value as of A bond rated Baa or higher is
considered investment grade. The chart reflects Moodys ratings; percentages may
include bonds not rated by Moody's but considered by Putnam Management to be of
comparable quality. Ratings will vary over time.




favoring CMBSs versus corporates was their
valuations. The yield spreads on investment-grade corporate bonds versus
Treasuries one means of evaluating the price of corporates on a relative basis
have been at historically low levels. This means that corporates have been
very expensive relative to other investment-grade sectors. Based on this
measure, we believed that there was greater downside risk in owning corporate
bonds. However, yields on corporate bonds continued to tighten against these
odds and your funds underweight position (relative to the benchmark) hurt
results. Strong performance from CMBS holdings helped to offset this somewhat,
as did the performance of select corporate bonds the fund held, particularly
shorter-duration Ba-rated corporate bonds
within the automotive sector.
Another bright spot in our security
selection within the securitized bond sector was an emphasis on ABSs backed by
manufactured housing and home equity loans.
ABSs carry short maturities, which provide us
with the flexibility to shift to other fixed-income securities should interest
rates rise. In addition, the funds collateralized mortgage obligations (CMOs) performed well on a relative basis. CMOs can be further divided
into interest-only (IO) and principal-only
(PO) securities, which separate the interest
and principal payments of a bond into separate cash flows. Although these types
of securities exhibit much greater prepayment sensitivity than MBSs, they compensate investors by offering much higher
yields. The fund has been combining IOs and POs to create a cash flow that is
similar to that of a standard MBSs, but which can be obtained at a lower
cost.
Hybrid Adjustable Rate
Mortgages
(ARMs) contributed positively to fund
results. Hybrid ARMs typically offer borrowers three or five years of payments
at a fixed interest rate, after which they become adjustable interest-rate
mortgages for which the interest rate is adjusted yearly and ARMs).
According to our analysis, many of these securities have been offering yields
that more than compensate for the level of risk they represent. Therefore, they
contribute to an attractive risk/return profile for the fund.
The funds underweight position in
dollar-denominated emerging-market debt
detracted from relative results as this
segment of the market continued to perform well, and it comprises a small
portion of the funds benchmark. Rising commodity prices are helping to create
positive fundamentals for issuers of emerging-market debt. However, after a
prolonged period of strong performance, we have taken a conservative approach
with regard to this segment of the market, so the fund did not benefit from its
strength to the same extent as many of its peers.
Please note that the holdings
discussed in this report may not have been held by the fund for the entire
period. Portfolio composition is subject to review in accordance with the funds
investment strategy and may vary in the future.










The following commentary reflects anticipated developments that could affect your fund over the next six months, as well as your management teams plans for responding to them.

With the key federal funds rate at we believe that the Fed may be nearing the end of its tightening cycle. However, continued solid growth in the United States and Asia (particularly Japan), means that the
possibility of globally rising inflation and real interest rates, especially in long-term bonds, will be a major consideration in our strategies for the remainder of the fiscal year and beyond.

Over the near term, we will continue to maintain a cautious stance, reflected in a portfolio with higher credit quality than that of the funds benchmark. Currently, we do not believe there is enough reward
available in the form of higher interest rates to make it worthwhile for the fund to take on additional credit risk. We have moved to a more neutral stance with regard to the funds portfolio duration, as we believe that much of the
interest-rate increases have already been priced into the market. Structured securities remain an area of opportunity, particularly CMBSs, ABSs backed by home-equity loans, and CMOs.

We will continue to remain vigilant regarding any possible disruptions to the economy and fixed-income markets, seeking to keep the fund positioned to benefit from opportunities while also avoiding unnecessary risk. We
will continue to pursue the funds objectives through multiple income-generating strategies across U.S. investment-grade fixed-income sectors and securities.

The views expressed in this report are exclusively those of Putnam Management. They are not meant as investment advice.

Lower-rated bonds may offer higher yields in return for more risk. Mutual funds that invest in government securities are not guaranteed. Mortgage-backed securities are subject to prepayment risk. Mutual funds that
invest in bonds are subject to certain risks, including interest-rate risk, credit risk, and inflation risk. As interest rates rise, the prices of bonds fall. Long-term bonds are more exposed to interest-rate risk than short-term bonds. Unlike
bonds, bond funds have ongoing fees and expenses. The use of derivatives involves special risks and may result in losses.






Your funds
performance
This section shows your funds
performance for periods ended April the end of the first half of its
current fiscal year. In accordance with regulatory requirements for mutual
funds, we also include performance for the most recent calendar quarter-end.
Performance should always be considered in light of a funds investment
strategy. Data represents past performance. Past performance does not guarantee
future results. More recent returns may be less or more than those shown.
Investment return and principal value will fluctuate, and you may have a gain or
a loss when you sell your shares. For the most recent month-end performance,
please visit www.putnam.com or call Putnam at Class Y shares are
generally only available to corporate and institutional clients. See the Terms
and Definitions section in this report for definitions of the share classes
offered by your fund.





Class A

Class B

Class C

Class M

Class R
Class Y

(inception dates)













NAV
POP
NAV
CDSC
NAV
CDSC
NAV
POP
NAV
NAV




Annual average











(life of fund)















years











Annual average















years











Annual average















years











Annual average















year















months














Performance assumes reinvestment
of distributions and does not account for taxes. Returns at public offering
price (POP) for class A and M shares reflect a sales charge of and
respectively. Class B share returns reflect the applicable contingent deferred
sales charge (CDSC), which is in the first year, declining to in the sixth
year, and is eliminated thereafter. Class C shares reflect a CDSC the first
year that is eliminated thereafter. Class R and Y shares have no initial sales
charge or CDSC. Performance for class B, C, M, R, and Y shares before their
inception is derived from the historical performance of class A shares, adjusted
for the applicable sales charge (or CDSC) and, except for class Y shares, the
higher operating expenses for such shares.
For a portion of the period, this
fund limited expenses, without which returns would have been lower. A
short-term trading fee may be applied to shares exchanged or sold within days
of purchase.






Comparative index
returns



For periods ended










Lipper Corporate



Lehman Aggregate

Debt Funds A Rated



Bond Index
category average





Annual average



(life of fund)
*
*





years



Annual average







years



Annual average







years



Annual average







year







months






Index and Lipper results should be
compared to fund performance at net asset value.
* The benchmark and the Lipper
category were not in existence at the time of the fund's inception. The Lehman
Aggregate Bond Index commenced The Lipper category commenced

Over the and
and periods ended , there were and funds,
respectively, in this Lipper category.



Fund price and distribution
information
For the six-month period ended




Distributions*
Class A

Class B
Class C
Class M

Class R
Class Y





Number













Income













Capital gains













Total













Share
value:

NAV

POP

NAV

NAV

NAV
POP
NAV
NAV




























Current yield









(end of period)









Current









dividend













Current









SEC









(with expense









limitation)













Current









SEC









(without









expense









limitation)












* Dividend sources are estimated
and may vary based on final tax calculations after the fund's fiscal year-end.
Most recent distribution, excluding capital gains,
annualized and divided by NAV or POP at end of period.
For a portion of the period, this fund limited expenses,
without which yields would have been lower.
Based only on investment income, calculated using SEC
guidelines.






Fund performance for
most recent calendar quarter




Total return for
periods ended















Class A

Class B

Class C

Class M

Class R
Class Y

(inception dates)













NAV
POP
NAV
CDSC
NAV
CDSC
NAV
POP
NAV
NAV




Annual average











(life of fund)















years











Annual average















years











Annual average















years











Annual average















year















months






















Your funds expenses

As a mutual fund investor, you pay ongoing expenses, such as management fees, distribution fees fees), and other expenses. In the most recent six-month period, your fund limited these expenses; had it not done
so, expenses would have been higher. Using the information below, you can estimate how these expenses affect your investment and compare them with the expenses of other funds. You may also pay one-time transaction expenses, including sales charges
(loads) and redemption fees, which are not shown in this section and would have resulted in higher total expenses. For more information, see your funds prospectus or talk to your financial advisor.

Review your funds expenses

The table below shows the expenses you would have paid on a investment in Putnam Income Fund from November to April It also shows how much a investment would be worth at the
close of the period, assuming actual returns and expenses.






Class A


Class B


Class C


Class M


Class R


Class Y









Expenses paid per



























Ending value (after expenses)



























* Expenses for each share class are calculated using the funds annualized expense ratio for each class, which represents the ongoing expenses as a percentage of net assets for the six months ended The
expense ratio may differ for each share class (see the table at the bottom of the next page). Expenses are calculated by multiplying the expense ratio by the average account value for the period; then multiplying the result by the number of days in
the period; and then dividing that result by the number of days in the year. Does not reflect the effect of a non-recurring reimbursement by Putnam. If this amount had been reflected in the table above, expenses for each share class would have been
lower.

Estimate the expenses you paid

To estimate the ongoing expenses you paid for the six months ended April use the calculation method below. To find the value of your investment on November go to www.putnam.com and log on to your
account. Click on the Transaction History tab in your Daily Statement and enter in both the from and to fields. Alternatively, call Putnam at








Compare expenses using the SECs
method
The Securities and Exchange
Commission (SEC) has established guidelines to help investors assess fund
expenses. Per these guidelines, the table below shows your funds expenses based
on a investment, assuming a hypothetical annualized return. You can use this information to compare the ongoing expenses (but not
transaction expenses or total costs) of investing in the fund with those of
other funds. All mutual fund shareholder reports will provide this information
to help you make this comparison. Please note that you cannot use this
information to estimate your actual ending account balance and expenses paid
during the period.




Class A
Class B
Class C
Class M
Class R
Class Y





Expenses paid per

$
$
$
$
$
$





Ending value (after
expenses)










* Expenses for each share class
are calculated using the funds annualized expense ratio for each class, which
represents the ongoing expenses as a percentage of net assets for the six months
ended The expense ratio may differ for each share class (see the table
at the bottom of this page). Expenses are calculated by multiplying the expense
ratio by the average account value for the period; then multiplying the result
by the number of days in the period; and then dividing that result by the number
of days in the year. Does not reflect the effect of a non-recurring
reimbursement by Putnam. If this amount had been reflected in the table above,
expenses for each share class would have been lower.
Compare expenses using industry
averages
You can also compare your funds
expenses with the average of its peer group, as defined by Lipper, an
independent fund-rating agency that ranks funds relative to others that Lipper
considers to have similar investment styles or objectives. The expense ratio for
each share class shown below indicates how much of your funds net assets have
been used to pay ongoing expenses during the period.




Class A
Class B
Class C
Class M
Class R
Class Y




Your funds
annualized







expense ratio*











Average annualized
expense







ratio for Lipper peer
group










* Does not reflect the effect of a
non-recurring reimbursement by Putnam. If this amount had been reflected in the
table above, the expense ratio for each share class would have been
lower.
Simple average of the expenses
of all front-end load funds in the funds Lipper peer group, calculated in
accordance with Lippers standard method for comparing fund expenses (excluding
fees and without giving effect to any expense offset and brokerage service
arrangements that may reduce fund expenses). This average reflects each funds
expenses for its most recent fiscal year available to Lipper as of To
facilitate comparison, Putnam has adjusted this average to reflect the
fees carried by each class of shares other than class Y shares, which do not
incur fees. The peer group may include funds that are significantly
smaller or larger than the fund, which may limit the comparability of the funds
expenses to the simple average, which typically is higher than the
asset-weighted average.




Putnam funds are actively managed by
teams of experts who buy and sell securities based on intensive analysis of
companies, industries, economies, and markets. Portfolio turnover is a measure
of how often a funds managers buy and sell securities for your fund. A
portfolio turnover of for example, means that the managers sold and
replaced securities valued at of a funds assets within a one-year period.
Funds with high turnover may be more likely to generate capital gains and
dividends that must be distributed to shareholders as taxable income. High
turnover may also cause a fund to pay more brokerage commissions and other
transaction costs, which may detract from performance.
Funds that invest in bonds or other
fixed-income instruments may have higher turnover than funds that invest only in
stocks. Short-term bond funds tend to have higher turnover than longer-term bond
funds, because shorter-term bonds will mature or be sold more frequently than
longer-term bonds. You can use the table below to compare your funds turnover
with the average turnover for funds in its Lipper category.














Putnam Income Fund











Lipper Corporate Debt
Funds






A Rated category
average









Turnover data for the fund is
calculated based on the fund's fiscal-year period, which ends on October
Turnover data for the fund's Lipper category is calculated based on the average
of the turnover of each fund in the category for its fiscal year ended during
the indicated year. Fiscal years vary across funds in the Lipper category, which
may limit the comparability of the fund's portfolio turnover rate to the Lipper
average. Comparative data for is based on information available as of

* Portfolio turnover excludes
dollar roll transactions.
Portfolio turnover excludes
certain Treasury note transactions executed in connection with a short-term
trading strategy.



Your funds risk
This risk comparison is designed to
help you understand how your fund compares with other funds. The comparison
utilizes a risk measure developed by Morningstar, an independent fund-rating
agency. This risk measure is referred to as the funds Overall Morningstar
Risk.
Your funds Overall
Morningstar Risk
Your funds Overall Morningstar Risk
is shown alongside that of the average fund in its broad asset class, as
determined by Morningstar. The risk bar broadens the comparison by translating
the funds Overall Morningstar Risk into a percentile, which is based on the
funds ranking among all funds rated by Morningstar as of March A
higher Overall Morningstar Risk generally indicates that a funds monthly
returns have varied more widely.
Morningstar determines a funds
Overall Morningstar Risk by assessing variations in the funds monthly returns
with an emphasis on downside variations over and periods, if
available. Those measures are weighted and averaged to produce the funds
Overall Morningstar Risk. The information shown is provided for the funds class
A shares only; information for other classes may vary. Overall Morningstar Risk
is based on historical data and does not indicate future results.
Morningstar does not purport to
measure the risk associated with a current investment in a fund, either on an
absolute basis or on a relative basis. Low Overall Morningstar Risk does not
mean that you cannot lose money on an investment in a fund. Copyright
Morningstar, Inc. All Rights Reserved. The information contained herein is
proprietary to Morningstar and/or its content providers; may not be copied
or distributed; and is not warranted to be accurate, complete, or timely.
Neither Morningstar nor its content providers are responsible for any damages or
losses arising from any use of this information.



















Year







and over





Kevin Cronin




*









Portfolio
Leader




*








Rob Bloemker




*









Portfolio
Member




*








Kevin Murphy

*












Portfolio
Member

*











Raman Srivastava

*












Portfolio
Member

*














