Dear Fellow Shareholder,

After posting solid returns in the last three quarters of small cap growth
stocks carried their momentum into In the first quarter, the Russell
Growth Index rose more than far outpacing both the Dow Jones Industrial
Average, as well as the S&P Here at Greenville Capital Management, we
participated commensurately in this positive price appreciation. For the
trailing six and twelve months ending April your fund returned
and respectively. This places your fund in the top out of
for the trailing twelve-month period as ranked by the Morningstar Small Cap
Growth Universe.

The fund's performance was led by strong gains in Industrials and Information
Technology; these sectors, per Russell, returned and respectively.
The Energy Sector continued its strong move upward, returning during the
same period. Your portfolio was over weight Industrials and Information
Technology and equal weight Energy compared to the benchmark by the fiscal year
end. Currently, your fund is underweight relative to the benchmark in Health
Care.

The US economy continued to exhibit solid GDP growth while, in our opinion,
keeping inflation largely in check. In Real GDP increased by
followed by robust growth of in the first quarter of The Consumer
Price Index (CPI) grew at in but taking out energy and food, the Core
CPI grew at a relatively modest That level of inflation has continued in
While core inflation remains at the high end of the Fed's target range,
recent data, in our view, does not appear alarming or accelerating at this time.
Additionally, the unemployment rate remained low, hovering slightly below
level for the trailing months. However, the low unemployment rate has placed
some upward pressure on wages, with the average hourly earnings up over the
past twelve months.

As we look forward, many of the same macroeconomic factors that played a role in
shaping the investment landscape should continue to do so in the year ahead.
Obviously, the Federal Reserve's hawkish stance on inflation remains one of the
focal points of the markets. The markets ran hard in in anticipation of a
halt in the Fed's measured point increases. However, recent comments
by the newly appointed Fed Chairman suggested that further rate increases would
be predicated on forthcoming data. Speculation remains as to what the Fed's
course of policy will be going forward, thus causing some apprehension on the
part of investors. An end to rate increases would likely, in our view, be a
positive catalyst for the equity markets and specifically the small cap growth
asset class.

Energy is another key variable that played a prominent role in and should
for the foreseeable future. The price of crude remains perched around
with prices at the pump hovering around ahead of the busy
summer driving season and a much anticipated hurricane season. The U.S.
Consumer has proven resilient with the higher energy prices in the recent past;
however, this could change if prices keep climbing through the summer
months.

Corporations continue to deliver solid earnings results and have begun to
deploy cash on the balance sheet to enhance productivity and add some
incremental capacity. Enterprise spending remains healthy and could continue to
benefit the Industrial and IT sectors. We believe we have been seeing early
signs of this during the first quarter as Institutional Investors have begun the
rotation away from Energy and into Industrial and IT sectors. Accordingly, we
believe your portfolio is well positioned to take advantage of any such shift.

In closing, as we have seen during the past three quarters, we believe that
investors will migrate towards growth stocks as we enter the later stages of
this economic cycle. In our opinion investors will likely pay a higher earnings
multiple for stocks exhibiting continued and consistent profit growth in a
moderating economic cycle. We believe investors could carry on the move from
value to growth, and thus small cap growth stocks could continue outperforming
the other indices in and beyond. While concerns over energy costs and the
Fed's course of tightening may present near term volatility in the markets, the
fundamentals of a growing economy with benign or decelerating inflation, and
reasonable price earnings multiples have historically provided strong fuel to a
favorable investment environment. In anticipation, we look forward to the
remainder of the year.

Once again, we appreciate your support and if you have any comments and or
questions, please do not hesitate to contact us.

Sincerely,
