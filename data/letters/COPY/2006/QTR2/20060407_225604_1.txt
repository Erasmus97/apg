Dear
Shareholders,
It has been said
that change is the only constant in life. As investors have seen, that theme is
still accurate today as we recently have experienced shifting economic cycles
because of natural disasters and political instability around the globe. Markets
worldwide have fluctuated in the past year as devastating hurricanes had a
dramatic effect on the international economy, particularly on oil prices. We
witnessed political unrest in the Middle East, highlighted by instability in
Iraq, and in Africa, the usually stable Nigeria also experienced violence. As a
result, energy prices have bounced up and down, with crude oil prices at one
point topping a record $70 per
barrel.
Such cycles are not
uncommon and in fact have almost become the norm in our everyday lives. What
does all of this mean to you as an investor? In times like these, it helps to
know that youre working with a seasoned investment professional who has
experience to guide you through difficult times. At MFS®, we believe our
investment management team has the knowledge and confidence to navigate through
difficult cycles and at the same time see through adversity to find investment
opportunities for our clients and
shareholders.

Dear Shareholders,
It has been said that change is the only
constant in life. As investors have seen, that theme is still accurate today as
we recently have experienced shifting economic cycles because of natural
disasters and political instability around the globe. 
Markets worldwide have fluctuated in the
past year as devastating hurricanes had a dramatic effect on the international
economy, particularly on oil prices. We 
Such cycles are not uncommon and in fact
have almost become the norm in our everyday lives. What does all of this mean to
you as an investor? In times like these, it helps to know that youre working
with a seasoned investment professional who has experience to guide you through
difficult times. At MFS®
Our investment management process, honed
over 80 years, combines a unique concept of teamwork with our unwavering focus
on the long term. We firmly believe that the best way to realize long-term
financial goals  be it a college education, a comfortable retirement, or a
secure family legacy  is to follow a three-pronged approach that focuses on
longer time horizons. Allocate holdings across the major asset classes 
including stocks, bonds, and cash. Diversify within each class to take advantage
of different market segments and investing styles. Rebalance assets regularly to
maintain a desired asset allocation. Of course, these strategies cannot
guarantee a profit or protect against a loss. This long-term approach requires
diligence and patience, two traits that in our experience are essential to
capitalizing on the many opportunities the financial markets can offer  through
both up and down economic cycles.
Respectfully,


Dear Shareholders,
It has been said that change is the only
constant in life. As investors have seen, that theme is still accurate today as
we recently have experienced shifting economic cycles because of natural
disasters and political instability around the globe.
Markets worldwide have fluctuated in the
past year as devastating hurricanes had a dramatic effect on the international
economy, particularly on oil prices. We
Such cycles are not uncommon and in fact
have almost become the norm in our everyday lives. What does all of this mean to
you as an investor? In times like these, it helps to know that youre working
with a seasoned investment professional who has experience to guide you through
difficult times. At MFS®
Our investment management process, honed
over 80 years, combines a unique concept of teamwork with our unwavering focus
on the long term. We firmly believe that the best way to realize long-term
financial goals  be it a college education, a comfortable retirement, or a
secure family legacy  is to follow a three-pronged approach that focuses on
longer time horizons. Allocate holdings across the major asset classes 
including stocks, bonds, and cash. Diversify within each class to take advantage
of different market segments and investing styles. Rebalance assets regularly to
maintain a desired asset allocation. Of course, these strategies cannot
guarantee a profit or protect against a loss. This long-term approach requires
diligence and patience, two traits that in our experience are essential to
capitalizing on the many opportunities the financial markets can offer  through
both up and down economic cycles.
Respectfully,


