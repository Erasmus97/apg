Dear Shareholder:


We are pleased to present this semiannual report for Dreyfus Premier New Jersey Municipal Bond Fund, Inc., covering the six-month period from January through June

After a long period of remarkable resilience, municipal bonds and other fixed-income securities encountered heightened volatility during the first half of as investors reacted to each new release of economic data or
comment from members of the Federal Reserve Board (the Fed). The economic data often painted a contradictory picture, sometimes suggesting that inflationary pressures were increasing and, at other times, seeming to point to milder
economic growth. The Fed also sent mixed signals as investors attempted to determine whether the U.S. central bank might pause in its tightening campaign after seventeen consecutive interest-rate hikes since June

In the judgment of our Chief Economist, Richard Hoey, the U.S. economy may be moving into a more mature, slower-growth phase. However, a number of economic uncertainties remain. Indicators to watch in the months ahead
include the outlook for inflation, the extent of softness in the housing market, the impact of slower economic growth on consumer spending, additional changes in interest rates from the Fed, and the strength of the U.S. dollar relative to other
major currencies. As always, we encourage you to discuss these and other investment-related issues with your financial advisor, who can help you prepare for the challenges and opportunities that lie ahead.

For information about how the fund performed during the reporting period, as well as market perspectives, we have provided a Discussion of Fund Performance given by the funds portfolio manager.

Thank you for your continued confidence and support.









DISCUSSION OF FUND PERFORMANCE

W. Michael Petty, Portfolio Manager

How did Dreyfus Premier New Jersey Municipal Bond Fund perform relative to its benchmark?

For the six-month period ended June the fund achieved total returns of for Class A shares, for Class B shares and for Class C
In comparison, the Lehman Brothers Municipal Bond Index (the Index), the funds benchmark, achieved a total return of for the reporting period.


Rising short-term interest rates and intensifying inflation concerns began to erode municipal bond prices during the second half of the reporting period.The funds Class A shares produced a return that was in line with
that of the benchmark, primarily as a result of its higher exposure to securities that are relatively sensitive to changes in yields. In addition, the fund is subject to fees and expenses to which the Index is not.

What is the funds investment approach?

The fund seeks as high a level of current income exempt from federal and New Jersey income taxes as is consistent with the preservation of capital.

To pursue this goal, the fund normally invests at least of its assets in municipal bonds that provide income exempt from federal and New Jersey personal income taxes.The fund invests at least of its assets in
investment-grade municipal bonds or the unrated equivalent as determined by Dreyfus.The fund may invest up to of its assets in municipal bonds rated below investment grade (high yield or junk bonds) or the unrated
equivalent as determined by Dreyfus. The dollar-weighted average maturity of the funds portfolio normally exceeds years.

We may buy and sell bonds based on credit quality, market outlook and yield potential. In selecting municipal bonds for investment, we may assess the current interest-rate environment and the municipal

The Fund







bonds potential volatility in different rate environments.We focus on bonds with the potential to offer attractive current income, typically looking for bonds that can provide consistently attractive current yields or
that are trading at competitive market prices.A portion of the funds assets may be allocated to discount bonds, which are bonds that sell at a price below their face value, or to premium bonds, which are bonds that sell
at a price above their face value.The funds allocation to either discount bonds or to premium bonds will change along with our changing views of the current interest-rate and market environment.We also may look to select bonds that are most
likely to obtain attractive prices when sold.

What other factors influenced the funds performance?

The Federal Reserve Board (the Fed) implemented four more increases in the overnight federal funds rate over the first half of driving it to .While longer-term bond yields remained relatively stable
during the first quarter of the year, they began to rise more steeply in the second quarter, eroding bond prices, as mounting inflationary pressures led to concerns that the Fed might raise short-term rates more than previously expected. However,
for the reporting period overall, short-term rates climbed somewhat more sharply than long-term yields, causing yield differences to narrow along the municipal bond markets maturity spectrum. As of June only basis points separated
yields of two-year and AAA-rated municipal bonds.

The funds results also were influenced by supply-and-demand factors. Although New Jersey has continued to face fiscal challenges, the growing U.S. economy benefited most states and municipalities, helping to boost
corporate and personal income tax receipts. As a result, the national supply of newly issued bonds moderated while investor demand remained robust, supporting bond prices.

In this environment, the fund continued to receive strong income contributions from its core holdings of seasoned bonds, which were purchased with significantly higher yields than are available today.The







fund also benefited from its corporate-backed holdings, including bonds issued on behalf of airlines as well as securities backed by the states settlement of litigation with U.S. tobacco companies.

However, these favorable factors were offset by relative weakness among holdings whose prices are particularly sensitive to changes in yields, including zero-coupon and non-callable bonds. In addition, the fund maintained
less exposure than the benchmark to securities at the long end of the maturity range, constraining its relative performance.

What is the funds current strategy?

The economic outlook currently appears to be cloudy. On one hand, cooling housing markets and moderating consumer spending suggest that U.S. economic growth may slow. On the other, soaring energy prices and other
inflationary pressures may lead to additional rate hikes from the Fed.Therefore, we expect heightened market volatility over the second half of which may create opportunities to purchase municipal bonds at attractive prices.








Total return includes reinvestment of dividends and any capital gains paid and does not take into
