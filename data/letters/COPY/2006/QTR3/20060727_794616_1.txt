Dear Fellow Shareholder

Investors continue to keep a close watch on the course of the economy. Globally, it appears that, assuming economic growth exceeds in we will have seen the strongest economic performance over a four-year period in over
thirty years. Corporate profits have boomed around the world, business capacity utilization rates have moved up, and unemployment rates have come down. Given such a sustained period of robust growth, it is not surprising that prices have begun to
rise, inflation rates have crept up, and central banks in many countries, particularly the Federal Reserve (the Fed) in the United States, have pushed interest rates higher.

In recent weeks, investors have worried that these higher rates could threaten the fundamentals of the global economy, prompting a widespread sell-off. However, we believe that todays higher interest rates, far from being a
threat to global economic fundamentals, are in fact an integral part of them. Higher interest rates are bringing business borrowing costs closer to the rate of return available from investments. In our view, this should help provide the basis for a
longer and more durable business expansion and a continued healthy investment environment.

You can be assured that the investment professionals managing your fund are closely monitoring the factors that influence the performance of the securities in which your fund invests. Moreover, Putnam Investments management
team, under the leadership of Chief Executive Officer Ed Haldeman, continues to focus on investment performance and remains committed to putting the interests of shareholders first.











In the following pages, members of your funds management team discuss the funds performance and strategies for the fiscal period ended May and provide their outlook for the months ahead. As always, we thank
you for your support of the Putnam funds.







Municipal bonds can help investors
keep more of their investment income while also financing important public
projects such as schools, roads, and hospitals. Municipal bonds are typically
issued by states and local municipalities to raise funds for building and
maintaining public facilities, and they offer income that is generally exempt
from federal income tax.
For residents of the state where the
bond is issued, income is typically exempt from state and any local income taxes
as well. While the stated yields on municipal bonds are usually lower than those
on taxable bonds, tax exemption is an especially powerful advantage in Ohio
because the states top income tax rate is among the highest in the United
States. And the sheer size of the Ohio municipal bond market provides a wide
array of investment opportunities.
Putnam Ohio Tax Exempt Income Fund
seeks to capitalize on investment opportunities in Ohio by investing in bonds across a range of market sectors. The fund
also combines bonds of differing credit quality to increase income potential. In
addition to investing in high-quality bonds, the funds management team
allocates a smaller portion of the portfolio to lower-rated bonds, which may
offer higher income in return for more risk.
When deciding whether to invest in a
bond, the funds management team considers the risks involved including credit
risk, interest-rate risk, and the risk that the bond will be prepaid. The
management team is backed by the resources of Putnams fixed-income
organization, one of the largest in the investment industry, in which municipal
bond analysts are grouped into sector teams and conduct ongoing, rigorous
research. Once a bond has been purchased, the team continues to monitor
developments that affect the bond market, the sector, and the issuer of the
bond.
Municipal bonds may finance a range
of projects in your community and thus play a key role in its
development.







The goal of the funds in-depth research and active management is to stay a step ahead of the industry and pinpoint opportunities to adjust the funds holdings for the benefit of the fund and its
shareholders.

The fund concentrates its investments by region and involves more risk than a fund that invests more broadly. Capital gains, if any, are taxable for federal and, in most cases, state purposes. For some investors,
investment income may be subject to the federal alternative minimum tax. Tax-free funds may not be suitable for IRAs and other non-taxable accounts. Please consult with your tax advisor for more information. Mutual funds that invest in bonds are
subject to certain risks, including interest-rate risk, credit risk, and inflation risk. As interest rates rise, the prices of bonds fall. Long-term bonds are more exposed to interest-rate risk than short-term bonds. Unlike bonds, bond funds have
ongoing fees and expenses.

Understanding tax-equivalent yield

To understand the value of tax-free income, it is helpful to compare a municipal bonds yield with the tax-equivalent yield the before-tax yield that must be offered by a taxable bond in order
to equal the municipal bonds yield after taxes.

How to calculate tax-equivalent yield:

The tax-equivalent yield equals the municipal bonds yield divided by one minus the tax rate. For example, if a municipal bonds yield is then its tax-equivalent yield is assuming the
maximum federal tax rate for



Results for investors subject to lower tax rates would not be as advantageous.






Putnam Ohio Tax Exempt Income Fund
seeks to provide as high a level of current income free from federal and state of
Ohio personal income taxes as we believe to be consistent with the preservation
of capital. It may be suitable for Ohio investors seeking tax-free income
through a diversified portfolio of municipal bonds primarily issued in
Ohio.



Highlights

*
For the months ended May
Putnam Ohio Tax Exempt Income Funds class A


shares returned without
sales charges.

*
The funds benchmark, the Lehman
Municipal Bond Index, returned

*
The average return for the
funds Lipper category, Ohio Municipal Debt Funds, was


*
Additional fund performance,
comparative performance, and Lipper data can be found in the



performance section beginning on
page




Since the funds
inception average annual return is at NAV and at
POP.







Average annual
return
Cumulative
return


NAV
POP
NAV

POP





years









years









years









year








Data is historical. Past
performance does not guarantee future results. More recent returns may be less
or more than those shown. Investment return and principal value will fluctuate,
and you may have a gain or a loss when you sell your shares. Performance assumes
reinvestment of distributions and does not account for taxes. Returns at NAV do
not reflect a sales charge of . For the most recent month-end performance,
visit www.putnam.com. For a portion of the period, this fund limited expenses,
without which returns would have been lower. A short-term trading fee of up to
may apply.




Strong demand from yield-hungry investors pushed
bond prices in the more credit-sensitive sectors of the municipal bond market
higher during your funds fiscal year. Many of the funds investments in the
tobacco settlement, health-care, and utility sectors performed well as a result.
The fund also benefited from its relatively short duration positioning, which
was designed to reduce the portfolios sensitivity to rising interest rates. The
pre-refunding of two holdings also contributed favorably to results. Fund
returns at net asset value (NAV, or without sales charges) surpassed the average
return for its Lipper category for the months ended May However,
the fund underperformed its nationally diversified benchmark because of its
single-state focus. The portfolios yield curve positioning which emphasized
intermediate-maturity bonds while limiting exposure to longer-maturity bonds
was also a factor in this underperformance.
Market overview
Amid continuing signs of solid economic
growth, and the potential for inflation that often accompanies such growth, the
Fed increased the federal funds rate eight times during the funds fiscal year,
lifting this benchmark for overnight loans between banks from to .
Bond yields rose across the maturity spectrum, leading to a convergence of
shorter- and longer-term rates. As rates converged, the yield curve a
graphical representation of yields for bonds of comparable quality plotted from the shortest to the longest maturity flattened.

During the period, tax-exempt bonds
generally outperformed comparable Treasury bonds, as prices of tax-exempt bonds
declined less than Treasury prices across all maturities. Municipal bonds
typically perform better than Treasuries when interest rates are rising.
However, the degree to which they outperformed Treasuries was greater than we
expected.



A robust economy and rising corporate
earnings contributed to the strong relative performance of lower-rated bonds.
Among uninsured bonds in general and especially bonds rated Baa and below, yield
spreads tightened as lower-rated bonds performed better than higher-rated bonds.
The superior performance of lower-rated bonds was primarily the result of strong
demand by buyers searching for higher yields. Non-rated bonds also rallied. In
addition, as a result of favorable legal rulings, tobacco settlement bonds
generally outperformed the broader market. Also, airline-related industrial
development bonds (IDBs) performed exceptionally well, in our view.
Strategy overview
Given our expectation for rising interest
rates, we maintained a short (defensive) portfolio duration relative to the
funds Lipper peer group, a strategy that contributed positively to results for
the period as rates rose. Duration is a measure of a funds sensitivity to
changes in interest rates. Having a shorter-duration portfolio may help protect
principal when interest rates rise, but it can reduce appreciation potential
when rates fall.
The funds yield curve positioning, or the
maturity profile of its holdings, detracted from performance during the period
but did not negate the positive contribution provided by our duration strategy.
In order to keep the funds duration short, we limited exposure to



Market sector
performance


These indexes provide an
overview of performance in different market sectors for the



months ended









Bonds





Lehman Municipal Bond Index
(tax-exempt bonds)






Lehman Aggregate Bond Index
(broad bond market)






Lehman Intermediate Government
Bond Index


(intermediate-maturity U.S.
Treasury and agency securities)






JP Morgan Global High Yield
Index (global high-yield corporate bonds)






Equities





S&amp;P Index (broad stock
market)






Russell Index
(small-company stocks)






MSCI EAFE Index (international
stocks)








longer-maturity bonds, favoring
intermediate-maturity securities instead. However, as the yield curve flattened
and the yield differences between shorter- and longer-term bonds converged,
bonds with longer maturities performed better than their intermediate-maturity
counterparts.
The funds overweight position in
Baa-rated bonds, compared to other funds in its peer group, boosted results as
securities in this area of the market continued to perform well. In contrast,
the funds underweight position in the lowest-rated bonds (i.e., those rated Ba
and lower), compared to other funds in its peer group, detracted from results as
securities in this area of the market rallied substantially. We maintained this
underweighting because we believed that better
relative value existed in bonds rated Baa and higher at this point in the credit
cycle, when the narrowing of credit spreads or the reduced premium investors
stand to receive by investing in lower-rated issues appears to be unsupported
by credit fundamentals.
During the period, we increased the funds
holdings in the single-family housing sector, a strategy that proved helpful to
results as rising interest rates and declining mortgage prepayments helped this
sector perform exceptionally well.
Your funds holdings
Lower-rated municipal bonds
delivered some of the best
performance
Comparison of the funds maturity and
duration
This chart compares changes in the
funds average effective maturity (a weighted average of
Average effective duration and
average effective maturity take into account put and call features, where
applicable, and reflect prepayments for mortgage-backed securities. Duration is
usually shorter than maturity because it reflects interest payments on a bond
prior to its maturity.



in the municipal bond market during the
reporting period, helped in large part by strong economic growth, relatively low
inflation, and record demand. After such strong appreciation, we have become
more cautious regarding this sector, since we dont think investors are being
adequately compensated for the increased risk associated with investing in these
lower-quality investments. While this decision curbed the funds participation
in the rally, we think it may help protect the value of the portfolio when
credit spreads begin to widen. The funds investments in Miami County Ohio revenue bonds for Upper Valley Medical Center
exemplify our strategy in this sector. These
bonds are financing improvements tothe hospital,
which has dominant market share in the community it serves and has demonstrated
strong profit margins and favorable cash flow. Moodys changed its outlook for
this bond from stable to positive in September
The funds overweight position in
tobacco settlement bonds contributed positively to results, as these bonds continued to
strengthen during the period. While tobacco settlement bonds generally carry
investment-grade ratings of Baa, they are secured by the income stream from
tobacco companies settlement obligations to the states and generally offer
higher yields than bonds of comparable quality.
Furthermore, we think tobacco
bonds

Credit qualities shown as a
percentage of portfolio value as of A bond rated Baa or higher is
considered investment grade. The chart reflects Moodys ratings; percentages may
include bonds not rated by Moodys but considered by Putnam Management to be of
comparable quality. Ratings will vary over time.




provide valuable diversification since
their performance is not as closely tied to the direction of economic growth as
other, more economically sensitive, holdings. The fund holds tobacco settlement
bonds issued by the Childrens Trust Fund
Tobacco Settlement of Puerto Rico as well as
bonds issued by the Tobacco Settlement
Financial Corporation of the Virgin Islands.
Puerto Rico municipal bonds
offer tax-exempt income to investors in all
states and have, over time, proven useful in diversifying the funds
portfolio. However, general obligation bonds
issued in Puerto Rico were downgraded near
the end of the period due to financial turmoil that led to a partial government
shutdown. Prices of uninsured Puerto Rico bonds declined, providing the fund
with an opportunity to add to its holdings at favorable levels. By the end of
the period, the fund had increased its Puerto Rico holdings from an underweight
position relative to the Lipper peer group to an allocation that is
approximately in line with the peer group average.
Two of the funds investment-grade
holdings rallied during the fiscal year when the bonds were pre-refunded.
Montgomery County Hospital revenue bonds
for Kettering Medical Center and
Franklin County Health Care Facility revenue
bonds for Ohio Presbyterian Services were
pre-refunded this year in January and May, respectively. Pre-refundings occur
when a municipality issues new bonds to raise
funds to pay off an older issue. This money is then invested in a secure
investment usually U.S. Treasury securities that matures at the older bonds
first call date, effectively raising the bonds perceived rating and frequently
its market value.
Please note that the holdings
discussed in this report may not have been held by the fund for the entire
period. Portfolio composition is subject to review in accordance with the funds
investment strategy and may vary in the future.










The following commentary reflects anticipated developments that could affect your fund over the next six months, as well as your management teams plans for responding to them.

We believe that the Feds tightening cycle is nearing an end but expect that rates will continue to rise over the near term. Consequently, we currently plan to maintain the funds defensive duration because
we believe that the municipal bond market may be susceptible to weaker returns in the coming months. A key reason for this belief is the markets unusually strong performance versus taxable equivalents for the fiscal period. In our view, the
municipal markets exceptional results can be attributed primarily to a combination of robust demand and limited new-issue supply.

It appears to us that the extended rally among lower-rated, higher-yielding bonds may be in its final stages. We base this view, in part, on the fact that the difference in yields between Aaa-rated bonds and Baa-rated
bonds the lowest investment-grade rating is at its narrowest point since late In other words, the reward in terms of higher yields for assuming additional credit risk has diminished substantially. Therefore, we remain cautious
with respect to securities at the lower end of the credit spectrum.

We continue to have a positive view of defensive sectors such as single-family housing bonds, which performed well over most of the fiscal period. Finally, our view on tobacco settlement bonds remains positive as we
believe that they represent good investment opportunities relative to the inherent risks associated with the sector.

The views expressed in this report are exclusively those of Putnam Management. They are not meant as investment advice.

This fund concentrates its investments by region and involves more risk than a fund that invests more broadly. Capital gains, if any, are taxable for federal and, in most cases, state purposes. For some investors,
investment income may be subject to the federal alternative minimum tax. Mutual funds that invest in bonds are subject to certain risks, including interest-rate risk, credit risk, and inflation risk. As interest rates rise, the prices of bonds fall.
Long-term bonds are more exposed to interest-rate risk than short-term bonds. Unlike bonds, bond funds have ongoing fees and expenses. Tax-free funds may not be suitable for IRAs and other non-taxable accounts.







This section shows your funds
performance for periods ended May the end of its fiscal year. In
accordance with regulatory requirements for mutual funds, we also include
performance as of the most recent calendar quarter-end. Performance should
always be considered in light of a funds investment strategy. Data represents
past performance. Past performance does not guarantee future results. More
recent returns may be less or more than those shown. Investment return and
principal value will fluctuate, and you may have a gain or a loss when you sell
your shares. For the most recent month-end performance, please visit
www.putnam.com or call Putnam at



Fund
performance







Total return for periods ended















Class A

Class B

Class M


(inception dates)









NAV
POP
NAV
CDSC
NAV
POP




Annual average







(life of fund)











years







Annual average











years







Annual average











years







Annual average











year










Performance assumes reinvestment
of distributions and does not account for taxes. Returns at public offering
price (POP) for class A and M shares reflect a sales charge of and
respectively. Class B share returns reflect the applicable contingent deferred
sales charge (CDSC), which is in the first year, declining to in the sixth
year, and is eliminated thereafter. Performance for class B and M shares before
their inception is derived from the historical performance of class A shares,
adjusted for the applicable sales charge (or CDSC).
For a portion of the period, this
fund limited expenses, without which returns would have been lower. A
short-term trading fee may be applied to shares exchanged or sold within days
of purchase.





Past performance does not indicate
future results. At the end of the same time period, a investment in the
funds class B shares would have been valued at and no contingent
deferred sales charges would apply. A investment in the funds class M
shares would have been valued at at public offering price). See
first page of performance section for performance calculation
method.
Comparative index returns
For periods ended





Lipper Ohio


Lehman Municipal

Municipal Debt
Funds


Bond Index
category average*





Annual average



(life of fund)







years



Annual average







years



Annual average







years



Annual average







year






Index and Lipper results should be
compared to fund performance at net asset value.
* Over the and
periods ended there were and funds, respectively, in
this Lipper category.







Distributions
Class A


Class B

Class M






Number























Capital












Total










Share value:
NAV
POP

NAV


NAV

POP



























Current yield (end of period)








Current dividend












Taxable












Current SEC












Taxable











For some investors, investment income may be subject to
the federal alternative minimum tax.
Capital gains, if any, are taxable for federal and, in
most cases, state purposes.
Most recent distribution, excluding capital gains,
annualized and divided by NAV or POP at end of period.
Assumes maximum federal and state combined tax
rate for Results for investors subject to lower tax rates would not be as
advantageous.
Based only on investment income, calculated using SEC
guidelines.
Fund performance for most recent calendar
quarter
Total return for periods ended





Class A

Class B

Class M






(inception dates)













NAV
POP
NAV
CDSC
NAV
POP

Annual average







(life of fund)











years







Annual
average

















years







Annual average













years







Annual average











year













Your funds
expenses
As a mutual fund investor, you pay
ongoing expenses, such as management fees, distribution fees fees), and
other expenses. In the most recent six-month period, your fund limited these
expenses; had it not done so, expenses would have been higher. Using the
information below, you can estimate how these expenses affect your investment
and compare them with the expenses of other funds. You may also pay one-time
transaction expenses, including sales charges (loads) and redemption fees, which
are not shown in this section and would have resulted in higher total expenses.
For more information, see your funds prospectus or talk to your financial
advisor.
Review your funds
expenses
The table below shows the expenses
you would have paid on a investment in Putnam Ohio Tax Exempt Income Fund
from December to May It also shows how much a
investment would be worth at the close of the period, assuming actual returns and expenses.




Class A
Class
B

Class
M





Expenses paid per

$
$
$





Ending value (after
expenses)







* Expenses for each share class
are calculated using the funds annualized expense ratio for each class, which
represents the ongoing expenses as a percentage of net assets for the six months
ended The expense ratio may differ for each share class (see the table
at the bottom of the next page). Expenses are calculated by multiplying the
expense ratio by the average account value for the period; then multiplying the
result by the number of days in the period; and then dividing that result by the
number of days in the year.
Estimate the expenses you
paid
To estimate the ongoing expenses you
paid for the six months ended May use the calculation method below. To
find the value of your investment on December go to www.putnam.com and
log on to your account. Click on the Transaction History tab in your Daily
Statement and enter in both the from and to fields.
Alternatively, call Putnam at



Compare expenses using the SECs
method
The Securities and Exchange
Commission (SEC) has established guidelines to help investors assess fund
expenses. Per these guidelines, the table below shows your funds expenses based
on a investment, assuming a hypothetical annualized return. You can use this information to compare the ongoing expenses (but not
transaction expenses or total costs) of investing in the fund with those of
other funds. All mutual fund shareholder reports will provide this information
to help you make this comparison. Please note that you cannot use this
information to estimate your actual ending account balance and expenses paid
during the period.




Class A
Class
B

Class
M





Expenses paid per

$
$
$





Ending value (after
expenses)







* Expenses for each share class
are calculated using the funds annualized expense ratio for each class, which
represents the ongoing expenses as a percentage of net assets for the six months
ended The expense ratio may differ for each share class (see the table
at the bottom of this page). Expenses are calculated by multiplying the expense
ratio by the average account value for the period; then multiplying the result
by the number of days in the period; and then dividing that result by the number
of days in the year.
Compare expenses using industry
averages
You can also compare your funds
expenses with the average of its peer group, as defined by Lipper, an
independent fund-rating agency that ranks funds relative to others that Lipper
considers to have similar investment styles or objectives. The expense ratio for
each share class shown below indicates how much of your funds net assets have
been used to pay ongoing expenses during the period.




Class A
Class B
Class M




Your funds annualized expense
ratio*








Average annualized expense ratio
for Lipper peer group







* For the funds most recent
fiscal half year; may differ from expense ratios based on one-year data in the
financial highlights.
Simple average of the expenses
of all front-end load funds in the funds Lipper peer group, calculated in
accordance with Lippers standard method for comparing fund expenses (excluding
fees and without giving effect to any expense offset and brokerage service
arrangements that may reduce fund expenses). This average reflects each funds
expenses for its most recent fiscal year available to Lipper as of To
facilitate comparison, Putnam has adjusted this average to reflect the
fees carried by each class of shares. The peer group may include funds that are
significantly smaller or larger than the fund, which may limit the comparability
of the funds expenses to the simple average, which typically is higher than the
asset-weighted average.




Putnam funds are actively managed by
teams of experts who buy and sell securities based on intensive analysis of
companies, industries, economies, and markets. Portfolio turnover is a measure
of how often a funds managers buy and sell securities for your fund. A
portfolio turnover of for example, means that the managers sold and
replaced securities valued at of a funds assets within a one-year period.
Funds with high turnover may be more likely to generate capital gains and
dividends that must be distributed to shareholders as taxable income. High
turnover may also cause a fund to pay more brokerage commissions and other
transaction costs, which may detract from performance.
Funds that invest in bonds or other
fixed-income instruments may have higher turnover than funds that invest only in
stocks. Short-term bond funds tend to have higher turnover than longer-term bond
funds, because shorter-term bonds will mature or be sold more frequently than
longer-term bonds. You can use the table below to compare your funds turnover
with the average turnover for funds in its Lipper category.



Turnover
comparisons






Percentage of
holdings that change every year


















Putnam Ohio Tax
Exempt







Income Fund










Lipper Ohio Municipal
Debt






Funds category
average









Turnover data for the fund is
calculated based on the funds fiscal-year period, which ends on May
Turnover data for the funds Lipper category is calculated based on the average
of the turnover of each fund in the category for its fiscal year ended during
the indicated year. Fiscal years vary across funds in the Lipper category, which
may limit the comparability of the funds portfolio turnover rate to the Lipper
average. Comparative data for is based on information available as of









Your funds risk

This risk comparison is designed to help you understand how your fund compares with other funds. The comparison utilizes a risk measure developed by Morningstar, an independent fund-rating agency. This risk measure is
referred to as the funds Overall Morningstar Risk.

Your funds Overall Morningstar&reg; Risk



Your funds Overall Morningstar Risk is shown alongside that of the average fund in its broad asset class, as determined by Morningstar. The risk bar broadens the comparison by translating the funds Overall
Morningstar Risk into a percentile, which is based on the funds ranking among all funds rated by Morningstar as of June A higher Overall Morningstar Risk generally indicates that a funds monthly returns have varied more widely.


Morningstar determines a funds Overall Morningstar Risk by assessing variations in the funds monthly returns with an emphasis on downside variations over and periods, if
available. Those measures are weighted and averaged to produce the funds Overall Morningstar Risk. The information shown is provided for the funds class A shares only; information for other classes may vary. Overall Morningstar Risk is
based on historical data and does not indicate future results. Morningstar does not purport to measure the risk associated with a current investment in a fund, either on an absolute basis or on a relative basis. Low Overall Morningstar Risk does not
mean that you cannot lose money on an investment in a fund. Copyright Morningstar, Inc. All Rights Reserved. The information contained herein is proprietary to Morningstar and/or its content providers; may not be copied or distributed;
and is not warranted to be accurate, complete, or timely. Neither Morningstar nor its content providers are responsible for any damages or losses arising from any use of this information.






Your funds
management
Your fund is managed by the members
of the Putnam Tax Exempt Fixed-Income Team. David Hamlin is the Portfolio
Leader, and Paul Drury, Susan McCormack, and James St. John are Portfolio
Members of your fund. The Portfolio Leader and Portfolio Members coordinate the
teams management of the fund.
For a complete listing of the members
of the Putnam Tax Exempt Fixed-Income Team, including those who are not
Portfolio Leaders or Portfolio Members of your fund, visit Putnams Individual
Investor Web site at www.putnam.com.
Fund ownership by the Portfolio Leader
and Portfolio Members
The table below shows how much the
funds current Portfolio Leader and Portfolio Members have invested in the fund
(in dollar ranges). Information shown is as of May and May















Year






and over





David Hamlin

*












Portfolio
Leader

*











Paul Drury

*












Portfolio
Member

*











Susan McCormack

*












Portfolio
Member

*











James St. John

*












Portfolio
Member

*


















