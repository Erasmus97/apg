Dear Shareholder:

We are pleased to present this annual report for Dreyfus Intermediate Municipal Bond Fund, Inc., covering the period from June through May

Since June the Federal Reserve Board (the Fed) has attempted to manage U.S. economic growth and forestall potential inflation by gradually raising short-term interest rates. In our view, the Feds
shift from a stimulative monetary policy to a neutral one has so far been successful: the economy has grown at a moderate pace, the unemployment rate has dropped, corporate profits have risen, and inflation has generally remained in check despite
recent cost pressures stemming from higher energy and import prices.

As we near the second half of the year, the financial markets appear more likely to be influenced not by what the Fed already has accomplished, but by investors expectations of what is to come, including the
Feds decision to increase rates further, maintain them at current levels or reduce them to stimulate future growth. We believe that this decision will depend largely on the outlook for core inflation in Fed probably can stand pat as
long as it expects inflation to remain subdued. But if inflationary pressures continue to build, the Fed may choose to tighten monetary policy further.As always, we urge you to discuss with your financial advisor the potential implications of these
possibilities on your investments.

For information about how the fund performed during the reporting period, as well as market perspectives, we have provided a Discussion of Fund Performance given by the funds portfolio manager.

Thank you for your continued confidence and support.









DISCUSSION OF FUND PERFORMANCE

Monica S. Wieboldt, Portfolio Manager

How did Dreyfus Intermediate Municipal Bond Fund perform relative to its benchmark?

For the period ended May the fund achieved a total return of The Lehman Brothers Municipal Bond Index (the
Index), the funds benchmark, achieved a total return of for the same In addition, the average total return for all funds reported in the
Lipper Intermediate Municipal Debt Funds category was

Municipal bonds held up relatively well amid rising interest rates as a result of supply-and-demand factors. The fund produced a higher return than the Index and its Lipper category average, due in part to strong income
contributions from its core holdings and investments in higher-yielding corporate-backed bonds.

What is the funds investment approach?

The fund seeks the maximum amount of current income exempt from federal income tax as is consistent with the preservation of capital.To pursue this goal, the fund normally invests substantially all of its assets in
municipal bonds that provide income exempt from federal personal income tax.

The fund invests at least of its assets in municipal bonds rated A or higher, or the unrated equivalent as determined by Dreyfus. The fund may invest up to of its assets in municipal bonds rated below A, including
bonds rated below investment grade (high yield or junk bonds) or the unrated equivalent as determined by Dreyfus. The dollar-weighted average maturity of the funds portfolio ranges between three and years.

We may buy and sell bonds based on credit quality, market outlook and yield potential. In selecting municipal bonds for investment, we may assess the current interest-rate environment and the municipal bonds









potential volatility in different rate environments. We focus on bonds with the potential to offer attractive current income, typically looking for bonds that can provide consistently attractive current yields or that are
trading at competitive market prices. A portion of the funds assets may be allocated to discount bonds, which are bonds that sell at a price below their face value, or to premium bonds, which are bonds that sell at a
price above their face value.The funds allocation to either discount bonds or to premium bonds will change along with our changing views of the current interest-rate and market environment.We also may look to select bonds that are most likely
to obtain attractive prices when sold.

What other factors influenced the funds performance?

The Federal Reserve Board (the Fed) raised short-term interest rates eight times during the reporting period, driving the overnight federal funds rate to Short-term municipal bond yields climbed along with
interest rates, but intermediate-term bonds rose less steeply, contributing to a further narrowing of yield differences (known as spreads) across the maturity range.The spread between two-year and bonds flattened from basis
points at the start of the reporting period to less than basis points at the end.When intermediate-and longer-term bond yields began to rise more steeply, and prices fell, in the spring, it was not enough to erase the markets positive
absolute returns for the reporting period overall.

The funds results also were positively influenced by supply-and-demand factors. The steadily growing U.S. economy helped to boost corporate and personal income tax receipts for most states and municipalities. A
correspondingly reduced supply of newly issued securities was met with robust investor demand, supporting prices.

The fund received strong contributions from its core holdings of seasoned bonds.These are bonds that were purchased at higher yields than are available today, and typically retain more of their value.The fund
also benefited from its corporate-backed securities bonds back by corporations or other institutions that are eligible for federal tax-exempt status including those backed by airlines, health care facilities and the







states settlement of litigation with U.S. tobacco companies.While high-yield municipal bonds represented one of the better performing sectors over the past year, credit spreads have tightened significantly, making
these issues current risk-reward characteristics less attractive.

During the first half of the reporting period, we generally focused on bonds with maturities toward the longer end of the intermediate range, while de-emphasizing the two- to five-year area.This strategy enabled the fund to
participate more fully in strength among longer-term bonds. We began to shift our focus toward shorter maturities in the spring as the yield curve continued to flatten, providing opportunities to roll down the yield curve without
significantly hampering current income.

What is the funds current strategy?

Recent economic data suggests that more rate hikes from the Fed are likely. If so, we anticipate a greater degree of volatility than the market has experienced over the past year. As we move further into the year and if it
appears that Fed tightening is coming to an end, we would expect the yield curve to steepen, which may benefit short- to intermediate-term bonds and potentially offer opportunities to reduce the funds weighted average maturity without
significantly affecting its current income profile.However,we are watching the economy carefully, and we are prepared to adjust our strategies as circumstances change.

June








Total return includes reinvestment of dividends and any capital gains paid. Past performance is no
