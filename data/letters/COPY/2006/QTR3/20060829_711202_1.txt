Dear Shareholder,

I am pleased to report that Citizens Funds had a strong positive year in fiscal
All of our equity funds are now managed by our in-house equity team. We
have built, what I believe, is the best investment group currently available
dedicated to managing a family of socially responsible mutual funds across a
range of asset classes. From growth to value, from small-cap to large-cap funds,
domestic and global, we have what I believe are strong effective strategies.

In fiscal year (FY) our small- and mid-cap funds, Citizens Small Cap Core
Growth and Citizens Emerging Growth had the highest total returns among the
domestic funds we manage. I believe these returns came from strong stock picking
and from the fact that in periods of economic expansion smaller companies are
often best able to capitalize on growth opportunities. By positioning these
funds in companies experiencing accelerated growth we were able to deliver these
returns for shareholders.

Within the large-cap universe, in FY we continued our strategy of
emphasizing "active management" by closing the Citizens Fund. Most
shareholders elected to exchange their Fund assets into another large cap
fund within the Citizens family. Even though the large-cap universe is well
understood by investors we continue to believe that we can actively add value by
stock selection as we seek to discriminate between corporate winners and losers.
Our Citizens Value Fund is a good example of how active stock selection can
provide positive returns ahead of the market. The Value Fund's standard shares
gained for the fiscal year, beating the S&P which was up

"FROM GROWTH TO VALUE, FROM SMALL- CAP TO LARGE-CAP FUNDS, DOMESTIC AND GLOBAL,
WE HAVE WHAT I BELIEVE ARE STRONG EFFECTIVE STRATEGIES."

-- SOPHIA COLLIER, PRESIDENT
