Dear Shareholder,
 
As you may recall in my last
letter, I described the enthusiasm that we were experiencing here at ING Funds
as we worked to bring more of the worlds investment opportunities to you, the
investor.
 
I am happy to report that
enthusiasm is continuing to thrive. We have launched a series of new
international mutual funds, each created to bring more of the worlds
opportunities to you.
 
SM
 
Those two objectives 
bringing you more of the worlds opportunities and doing it in a way that is
easier for you  are behind the development of the ING Diversified
International Fund. This new Fund is among those that we launched in January,
but it is unique in that it is a fund-of-funds. It is also, we believe, simply
an easier way to invest internationally.
 
The ING Diversified
International Fund brings together six distinct, international mutual funds,
each managed by well-known asset managers who specialize in key international
sub-asset classes. Whats more, the Fund is periodically reviewed by a seasoned
team of ING asset allocation experts who re-adjust the Funds allocation based
on prevailing market conditions.
 
Best of all: weve made it
easy. With just one investment, investors can now acquire a broadly
diversified, actively managed international equity portfolio.
 
The ING Diversified
International Fund marks one more way that we at ING Funds are continuing to
offer you the global expertise, product innovation and world-class service that
you have come to expect from us.
 
On behalf of everyone at ING
Funds, I thank you for your continued support and loyalty. We look forward to
serving you in the future.
 
Sincerely,


Dear
Shareholder,
 
As
you may recall in my last letter, I described the enthusiasm that we were
experiencing here at ING Funds as we worked to bring more of the worlds
investment opportunities to you, the investor.
 
I
am happy to report that enthusiasm is continuing to thrive. We have launched a
series of new international mutual funds, each created to bring more of the
worlds opportunities to you.
 
Meanwhile,
we have also heard you loud and clear. Our research tells us that many
investors report that they find investing an intimidating and overly-complex
endeavor. That is why ING is committed to helping investors across the country
cut through the confusion and clutter. Your future. Made easier.
 
Those
two objectives  bringing you more of the worlds opportunities and doing it in
a way that is easier for you  are behind the development of the ING
Diversified International Fund. This new Fund is among those that we launched
in January, but it is unique in that it is a fund-of-funds. It is also, we
believe, simply an easier way to invest internationally.
 
The
ING Diversified International Fund brings together six distinct, international
mutual funds, each managed by well-known asset managers who specialize in key
international sub-asset classes. Whats more, the Fund is periodically reviewed
by a seasoned team of ING asset allocation experts who re-adjust the Funds
allocation based on prevailing market conditions.
 
Best
of all: weve made it easy. With just one investment, investors can now acquire
a broadly diversified, actively managed international equity portfolio.
 
The
ING Diversified International Fund marks one more way that we at ING Funds are
continuing to offer you the global expertise, product innovation and
world-class service that you have come to expect from us.
 
On
behalf of everyone at ING Funds, I thank you for your continued support and
loyalty. We look forward to serving you in the future.
 
Sincerely,


