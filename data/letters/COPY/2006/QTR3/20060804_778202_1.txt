Dear Shareholders:

We are pleased to provide you with important annual information about Allegiant
Funds, as well as a review of the financial markets and the events shaping the
global markets. Overall the Board is very satisfied with the progress made this
year by our portfolio management teams. During the year, total net assets of
the Allegiant Funds decreased by million to billion due to net
shareholder redemptions. We encourage you to review the audited financial
information contained in this report to stay informed about your investments.

We also are pleased to welcome three new Trustees - Tim Swanson, President and
CEO, Dorothy Berry, and Kelley Brennan - whose industry expertise will be very
helpful to our Board. They replace three retiring Trustees - Herb Martens,
President and CEO, Bob Farling, and Bill Pullen - who made valuable
contributions to the Funds over many years.

If you have questions regarding the Funds or your investments, we encourage you
to speak with your investment professional or call an Allegiant Funds
representative at More information about Allegiant Funds
is available on our website, www.allegiantfunds.com.

Thank you for making Allegiant Funds part of your investment portfolio and we
look forward to helping you achieve your financial goals in the year ahead.

Sincerely,

Dear Shareholders:

We have experienced a transformational year at Allegiant Asset Management
Company and it makes writing this letter so rewarding. I'm confident we will
reach our strategic objective to deliver strong investment performance and
unparalleled service because we have highly engaged, motivated professionals,
who deliver on their commitments. As adviser to the fund complex, I'd like to
highlight several themes that illustrate our approach to become your investment
manager of choice:

. Driving investment excellence: In the past two years, Allegiant took
decisive steps to solidify the investment processes and strategies
implemented by our portfolio management teams. In late we added
Polaris Capital Management, Inc. as a sub-advisor for the
International Equity Fund. Likewise, the structured equity team has
assumed responsibility for the Multi-Factor Small Cap Value Fund. The
team is using the same quantitative investment process in managing
Allegiant's three newest small cap offerings that were introduced in
September

. Strengthening our platform for future growth: Allegiant marked the
beginning of with completion of significant systems conversions
that are critical to expanding our existing infrastructure, creating
greater efficiencies and streamlining reporting and compliance
monitoring.

. Creating a dynamic culture within our organization: With the input and
feedback from within, Allegiant supports an investment management firm
where our team members are valued and empowered. Simultaneously,
Allegiant continues to attract established talent to build top-tier
investment management teams.

Dear Shareholders:

We are pleased to provide you with important annual information about Allegiant
Funds, as well as a review of the financial markets and the events shaping the
global markets. Overall, the Board is very satisfied with the progress made
this year by our portfolio management teams. During the year, total net assets
of the Allegiant Funds decreased by million to billion due to net
shareholder redemptions. We encourage you to review the audited financial
information contained in this report to stay informed about your investments.

We also are pleased to welcome three new Trustees - Tim Swanson, President and
CEO, Dorothy Berry, and Kelley Brennan - whose industry expertise will be very
helpful to our Board. They replace three retiring Trustees - Herb Martens,
President and CEO, Bob Farling, and Bill Pullen - who made valuable
contributions to the Funds over many years.

If you have questions regarding the Funds or your investments, we encourage you
to speak with your investment professional or call an Allegiant Funds
representative at More information about Allegiant Funds
is available on our website, www.allegiantfunds.com.

Thank you for making Allegiant Funds part of your investment portfolio and we
look forward to helping you achieve your financial goals in the year ahead.

Sincerely,

Dear Shareholders:

We have experienced a transformational year at Allegiant Asset Management
Company and it makes writing this letter so rewarding. I am confident we will
reach our strategic objective to deliver strong investment performance and
unparalleled service because we have highly engaged, motivated professionals,
who deliver on their commitments. As adviser to the fund complex, I would like
to highlight several themes that illustrate our approach to become your
investment manager of choice:

o Driving investment excellence: In the past two years, Allegiant took
decisive steps to solidify the investment processes and strategies
implemented by our portfolio management teams.

o Strengthening our platform for future growth: Allegiant marked the
beginning of with completion of significant systems conversions
that are critical to expanding our existing infrastructure, creating
greater efficiencies and streamlining reporting and compliance
monitoring.

o Creating a dynamic culture within our organization: With the input and
feedback from within, Allegiant supports an investment management firm
where our team members are valued and empowered. Simultaneously,
Allegiant continues to attract established talent to build top-tier
investment management teams.

Dear Shareholders:

We are pleased to provide you with important annual information about Allegiant
Funds, as well as a review of the financial markets and the events shaping the
global markets. Overall, the Board is very satisfied with the progress made
this year by our portfolio management teams. During the year, total net assets
of the Allegiant Funds decreased by million to billion due to net
shareholder redemptions. We encourage you to review the audited financial
information contained in this report to stay informed about your investments.

We also are pleased to welcome three new Trustees - Tim Swanson, President and
CEO, Dorothy Berry, and Kelley Brennan - whose industry expertise will be very
helpful to our Board. They replace three retiring Trustees - Herb Martens,
President and CEO, Bob Farling, and Bill Pullen - who made valuable
contributions to the Funds over many years.

If you have questions regarding the Funds or your investments, we encourage you
to speak with your investment professional or call an Allegiant Funds
representative at More information about Allegiant Funds
is available on our website, www.allegiantfunds.com.

Thank you for making Allegiant Funds part of your investment portfolio and we
look forward to helping you achieve your financial goals in the year ahead.

Sincerely,

Dear Shareholders:

We have experienced a transformational year at Allegiant Asset Management
Company and it makes writing this letter so rewarding. I am confident we will
reach our strategic objective to deliver strong investment performance and
unparalleled service because we have highly engaged, motivated professionals,
who deliver on their commitments. As advisor to the fund complex, I would like
to highlight several themes that illustrate our approach to become your
investment manager of choice:

o Driving investment excellence: In the past two years, Allegiant took
decisive steps to solidify the investment processes and strategies
implemented by our portfolio management teams.

o Strengthening our platform for future growth: Allegiant marked the
beginning of with completion of significant systems conversions
that are critical to expanding our existing infrastructure, creating
greater efficiencies, and streamlining reporting and compliance
monitoring.

o Creating a dynamic culture within our organization: With the input and
feedback from within, Allegiant supports an investment management firm
where our team members are valued and empowered. Simultaneously,
Allegiant continues to attract established talent to build top-tier
investment management teams.
