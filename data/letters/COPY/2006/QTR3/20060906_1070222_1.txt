Dear Shareholders:


The economy generated healthy growth during the period between and
Stocks posted solid returns for the year, although the market became increasingly volatile
late in the period, and bonds generally retreated.


Economic growth fluctuated throughout the fiscal year The economy expanded briskly during the
summer of supported by increases in corporate spending. The after-effects of Gulf Coast
hurricanes, including a spike in already-high energy prices, combined with rising interest rates to
restrain consumer and business spending for the rest of During the first three months of
the economy expanded rapidly as energy prices retreated and a warm winter boosted home
construction. Growth then slowed during the spring, due to the effects of higher interest rates and
energy prices.


Oil and gasoline prices were generally high throughout the period. Economic strength around the
world, particularly in the fast growing, oil-thirsty Chinese economy, increased demand for energy.
Meanwhile, fears of supply disruptions, due largely to conflicts in the Middle East and Africa,
caused speculators to bid up the price of crude oil. Steep oil and gasoline prices slowed the
economy and led to higher prices on energy-intensive products such as food and transportation.


The Federal Reserve Board raised short-term interest rates nine times during the through
June, each time by points, pushing the Federal Funds rate to by the end of
the period. Those actions continued a series of quarter-point interest-rate hikes at every Federal
Reserve meeting since June of That policy was designed to combat the inflationary pressures
resulting from strong economic growth and high energy prices. A new Chairman, Ben Bernanke, assumed
leadership of the Federal Reserve in early


The economic environment during the period proved difficult for consumers, particularly
those with lower incomes, as higher energy costs reduced disposable income and higher interest
rates increased borrowing costs. Reduced consumer spending was offset by increased corporate
spending. Corporations enjoyed strong balance sheets and good free cash flows, and increasingly
sought ways to invest that moneyin many cases by engaging in mergers and acquisitions.


Stocks posted strong gains through early May, and then pulled back amid volatile trading. The
Standard Poors Composite Index (the SP or the SP returned
for the period as a whole, slightly lower than long-term averages.


The broad market generated gains early in the period. Stocks pulled back in early fall as investors
worried that dramatically higher energy prices would weaken consumer spending, thereby undercutting
economic growth and corporate profits. But the economy and profits proved resilient, and the market
rallied between October and early May. The market sold off again in late spring, as investors
worried that rising inflation would persuade the Federal Reserve to raise interest rates enough to
slow the economy to a standstill. Stock prices were very volatile through the remainder of the
period, as investors tried to weigh economic data and comments from the new Federal Reserve
chairman. Stocks recovered somewhat by the end of June, as fears about inflation moderated.


Small- and mid-cap stocks and foreign equities led the way during the markets rally, with
emerging-markets stocks posting some of the strongest gains. Those leading stock categories
generally fell the furthest when the market declined in late spring. Stocks in emerging markets
especially suffered from the reversal of Japans interest rate policy, which threatened to
reduce growth in many Asian markets. Nevertheless, smaller stocks and foreign shares outperformed
their larger, domestic counterparts for the period as a whole. Energy was the best-performing
sector, as higher prices for oil and gasoline improved profits at energy firms. That trend helped
value indices outperform growth indices.


Bonds declined slightly during the period. The Federal Reserves interest-rate increases
contributed to higher yields on fixed-income securities, which led to lower prices on existing
bonds. The bond market experienced a slightly inverted yield curve early in late an uncommon
condition in which short-term bonds offer higher yields than longer-term
bonds. That development reflected the markets uncertainty about how the new Federal Reserve
chairman would handle a difficult environment of rising inflation and potentially slower economic
growth. But the yield curve subsequently flattened, and then yields rose on bonds of all term
lengths. Corporate bonds and mortgage-backed securities outperformed government bonds during the
period.



The New Covenant Growth Fund


The New Covenant Growth Fund gained during the period ended That
compared to an return for the Funds benchmark, the SP Index.


This Fund invests the majority of its assets in a core portfolio, and adds satellite portfolios of
value, growth and international stocks to provide shareholders with greater diversification. The
Fund also spreads its assets among small-, mid- and large-cap shares to gain exposure to various
sectors of the equity


The Funds exposure to international stocks, including emerging-markets shares, boosted returns
relative to the benchmark during the period. The Fund generally held between and
of assets in foreign stocks, including to of assets in emerging markets. Those allocations
helped shareholders benefit from the strong run-up in foreign shares that occurred during much of
the fiscal year. The Funds roughly allocation to small- and mid-cap stocks also improved
returns against the SP as smaller stocks outperformed larger stocks. The declines in foreign
shares and smaller stocks toward the end of the period decreased the magnitude of the Funds
superior performance relative to the benchmark.



The New Covenant Income Fund


The New Covenant Income Fund returned during the period ended That
net return nearly matched the return of the Funds benchmark, the Lehman Brothers Aggregate
Bond


The Funds Sub-Advisor held the Funds duration shorter than that of its benchmark throughout the
period, but gradually reduced the duration gap to capture higher yields as interest rates rose.
That strategy helped relative returns. The Funds relative performance also benefited early in the
period from employing a structure known as a barbell, in which the portfolio emphasized bonds on
either end of








Table of Contents






to our shareholders



NEW COVENANT FUNDS



the yield curve and held few intermediate-term securities. The Sub-Advisor gradually unwound
that barbell structure during the period by adding intermediate-term securities. Finally, effective
selection among mortgage-backed securities improved performance relative to the




The New Covenant Balanced Growth Fund


The New Covenant Balanced Growth Fund gained during the period ended
That compared to a return for its benchmark, a composite index that is comprised of a
weighting in the SP and a weighting in the Lehman Brothers Aggregate Bond Index.


The Fund held a slightly overweight stock allocation throughout the period. That positioning helped
the Fund outperform its composite benchmark, as stocks outperformed bonds and the Funds equity
allocation out-gained the SP As of the Fund held of its assets in the
New Covenant Growth Fund and in the New Covenant Income Fund.



The New Covenant Balanced Income Fund


The New Covenant Balanced Income Fund gained during the period ended
That compared to a return for its benchmark, a composite index with a allocation to the
SP Index and a allocation to the Lehman Brothers Aggregate Bond Index.


Like the Balanced Growth Fund, the Balanced Income Fund held a slightly overweight allocation to
stocks during the period. The Fund, as of held of its assets in the
New Covenant Growth Fund, with of assets in the New Covenant Income Fund. That overweight
stake in equities helped the Fund outperform its benchmark for the period, as did the strong
relative performance of the New Covenant Growth Fund.


Thank you for your confidence in the New Covenant Funds.




George W. Rue III
Senior Vice President and Chief Investment Officer
NCF Investment Department of New Covenant Trust Company,


Past performance does not guarantee future results. The performance data quoted represents past
performance and current returns may be lower or higher. Total return figures include change in
share price, reinvestment of dividends and capital gains. The investment return and principal value
will fluctuate so that an investors shares, when redeemed, may be worth more or less than the
original cost. Returns do not reflect the deduction of taxes that a shareholder would pay on Fund
distributions or redemptions of fund shares. To obtain performance information current to the most
recent month end, please call or visit our website at www.NewCovenantFunds.com.









The Standard Poors Composite Index of stocks is an unmanaged, capitalization weighted
index that measures the performance of large-capitalization stocks representing all major
industries. It is not possible to invest directly in any index.







Portfolio composition is subject to change.







The Lehman Brothers Aggregate Bond Index is an unmanaged index of U.S. bonds, which includes
reinvestment of any earnings. It is widely used to measure the overall performance of the U.S.
bond market. It is not possible to invest directly in any index.







A subsidiary of the Presbyterian Foundation.





Portfolio Allocation (unaudited) (subject to change)
