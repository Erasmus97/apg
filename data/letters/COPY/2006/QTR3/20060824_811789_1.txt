Dear Shareholder:

We present this semiannual report for Dreyfus BASIC U.S. Mortgage Securities Fund, covering the six-month period from January through June

After a long period of remarkable resilience, the U.S. bond market encountered heightened volatility during the first half of as investors reacted to each new release of economic data or comment from members of the
Federal Reserve Board (the Fed). The economic data often painted a contradictory picture, sometimes suggesting that inflationary pressures were increasing and, at other times, seeming to point to milder economic growth.The Fed also sent
mixed signals as investors attempted to determine whether the U.S. central bank might pause in its tightening campaign after seventeen consecutive rate hikes since June

In the judgment of our Chief Economist, Richard Hoey, the U.S. economy may be moving into a more mature, slower-growth phase. However, a number of economic uncertainties remain. Indicators to watch in the months ahead
include the outlook for inflation, the extent of softness in the U.S. housing market, the impact of slower economic growth on consumer spending, additional changes in interest rates from the Fed and other central banks, and the strength of the U.S.
dollar relative to other major currencies.As always, we encourage you to discuss these and other investment-related issues with your financial advisor, who can help you prepare for the challenges and opportunities that lie ahead.

For information about how the fund performed during the reporting period, as well as market perspectives, we have provided a Discussion of Fund Performance given by the portfolio manager.

Thank you for your continued confidence and support.









DISCUSSION OF FUND PERFORMANCE

Robert Bayston, Portfolio Manager

How did Dreyfus BASIC U.S. Mortgage Securities Fund perform relative to its benchmark?

For the six-month period ended June the fund achieved a total return of and provided aggregate income dividends of approximately
In comparison, the funds benchmark, the Lehman Brothers GNMA Index (the Index), achieved a total return of

After demonstrating remarkable resilience during most of the Federal Reserve Boards (the Fed) credit-tightening campaign, bond prices began to fall, and their yield rose, as investors became more concerned
about potential inflationary pressures. The fund produced lower returns than the Index, which does not reflect fund fees and expenses.
