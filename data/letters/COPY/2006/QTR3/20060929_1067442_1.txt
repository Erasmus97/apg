Dear Shareholder: 
 
We hope that you find the annual report for the Target Conservative Allocation Fund (formerly Strategic
Partners Conservative Allocation Fund) informative and useful. 
 
Target Asset Allocation
Funds will be managed exactly as they have been in the past, with institutional-quality asset managers selected, matched, and monitored by the same research team as before. Portions of the Funds assets are assigned to carefully chosen asset
managers, with the allocations actively managed on the basis of our projections for the financial markets and the managers individual strengths. This approach has led to competitive long-term performance. 
 
You also will retain exchange privileges with any fund in Prudentials JennisonDryden mutual fund
family. 
 
We believe your Target Conservative Allocation Fund will remain an excellent way
for you to achieve broad, actively managed diversification at a targeted risk/return balance with a single investment purchase. We appreciate your continued confidence in us. 
 
Sincerely, 


Dear Shareholder: 
 
We hope that you find the annual report for the Target Moderate Allocation Fund (formerly Strategic
Partners Moderate Allocation Fund) informative and useful. 
 
Target Asset Allocation Funds
will be managed exactly as they have been in the past, with institutional-quality asset managers selected, matched, and monitored by the same research team as before. Portions of the Funds assets are assigned to carefully chosen asset
managers, with the allocations actively managed on the basis of our projections for the financial markets and the managers individual strengths. This approach has led to competitive long-term performance. 
 
You also will retain exchange privileges with any fund in Prudentials JennisonDryden mutual fund
family. 
 
We believe your Target Moderate Allocation Fund will remain an excellent way for
you to achieve broad, actively managed diversification at a targeted risk/return balance with a single investment purchase. We appreciate your continued confidence in us. 
 
Sincerely, 


Dear Shareholder: 
 
We hope that you find the annual report for the Target Growth Allocation Fund (formerly Strategic
Partners Growth Allocation Fund) informative and useful. 
 
Target Asset Allocation Funds
will be managed exactly as they have been in the past, with institutional-quality asset managers selected, matched, and monitored by the same research team as before. Portions of the Funds assets are assigned to carefully chosen asset
managers, with the allocations actively managed on the basis of our projections for the financial markets and the managers individual strengths. This approach has led to competitive long-term performance. 
 
You also will retain exchange privileges with any fund in Prudentials JennisonDryden mutual fund
family. 
 
We believe your Target Growth Allocation Fund will remain an excellent way for
you to achieve broad, actively managed diversification at a targeted risk/return balance with a single investment purchase. We appreciate your continued confidence in us. 
 
Sincerely, 


