Dear Fellow Shareholders:



We are pleased to provide you with an update on the Westcore Funds. The past year has been a good one for our Fund Family, and we have positive news to share with you on a number of fronts,
including performance and research team enhancements.



Fund Update



During the past months, assets in the Westcore Funds increased from million to over billion, with growth coming from both positive returns across all Funds and new investors
joining our Fund Family. We are also pleased to announce that the Westcore Plus Bond Fund was recognized by Lipper for its excellence in delivering consistently strong risk-adjusted performance relative to its peers. The Fund was a Lipper Fund
Award Winner, ranking first in the Intermediate Investment Grade Debt category for the three years ended December We are proud of this award and of the team that earned it by staying true to our disciplined, research-intensive approach to
fixed-income investing.


Likewise, as of May eight Westcore Funds were awarded Lipper Leader designations. Please see pages through for more detailed information and pages through for important
disclosures on these designations. Overall Fund performance and expense examples can be viewed on pages and
