Dear Shareholder:

I am pleased to present the enclosed Franklin Global Trust Funds annual report
covering the fiscal year ended July In each Fund report, the Fund's
portfolio managers discuss market conditions and Fund performance. A complete
list of each Fund's holdings, as well as the financial statements, is included
in the annual report.


/s/ Rupert H. Johnson, Jr.

Rupert H. Johnson, Jr.
President and Chief Executive Officer - Investment Management
Franklin Global Trust

-----------------------------------------------------
NOT FDIC INSURED | MAY LOSE VALUE | NO BANK GUARANTEE
-----------------------------------------------------


Annual Report |


Economic and Market Overview

During the months ended July the U.S. economy advanced at a
moderate pace. After a slow fourth quarter gross domestic product grew an
annualized in first quarter and moderated to an estimated
annualized rate in the second quarter. Growth was driven by consumer, business
and government spending. Productivity gains and corporate profits also
contributed. Export growth picked up some momentum, but a wide trade deficit
remained. The labor market firmed as employment generally increased and the
unemployment rate fell from to Although labor costs rose during the
reporting period, hiring increased in many industries and personal income grew.

Elevated energy and other commodity prices were a primary economic concern. Oil
prices remained high, largely due to potential supply disruptions and strong
growth in global demand, especially from China and India. Gasoline, medical and
pharmacy costs climbed substantially. Consumer confidence and spending remained
surprisingly strong but could weaken with the impact of higher prices. As home
price appreciation slowed, borrowing against home equity flattened and could
have a gradual effect on consumer spending. Inflation was also a concern, as the
core Consumer Price Index (CPI) rose for the months ended July
which was higher than the average.

The Federal Reserve Board (Fed) raised the federal funds target rate from
to in eight quarter-point steps. The Fed's credit-tightening campaign
contributed to cooler housing and real estate markets as the cost of credit grew
less attractive to most consumers who were already dealing with near-record
consumer debt burdens, low personal savings and historically high gas prices. By
period-end, most market observers expected the Fed to pause its rate tightening
in August.

In this environment, equity markets experienced dramatic swings late in the
period. Overall, the blue chip stocks of the Dow Jones Industrial Average posted
a total return of while the broader Standard & Poor's Index
(S&P and the technology-heavy NASDAQ Composite Index returned and
- Energy, telecommunications and financials stocks performed
particularly well.

Source: Bureau of Labor Statistics.

Source: Bureau of Labor Statistics. Core CPI excludes food and energy
costs.

Source: Standard & Poor's Micropal. The Dow Jones Industrial Average is
price weighted based on the average market price of blue chip stocks of
companies that are generally industry leaders. The S&P consists of
stocks chosen for market size, liquidity and industry group
representation. Each stock's weight in the index is proportionate to its
market value. The S&P is one of the most widely used benchmarks of
U.S. equity performance. The NASDAQ Composite Index measures all NASDAQ
domestic and international based common type stocks listed on The NASDAQ
Stock Market. The index is market value weighted and includes more than
companies.

THE FOREGOING INFORMATION REFLECTS OUR ANALYSIS AND OPINIONS AS OF JULY
THE INFORMATION IS NOT A COMPLETE ANALYSIS OF EVERY ASPECT OF ANY MARKET,
COUNTRY, INDUSTRY, SECURITY OR FUND. STATEMENTS OF FACT ARE FROM SOURCES
CONSIDERED RELIABLE.


| Annual Report


Fiduciary Large Capitalization
Growth and Income Fund

YOUR FUND'S GOAL AND MAIN INVESTMENTS: Fiduciary Large Capitalization Growth and
Income Fund seeks long-term growth of principal and income through investing at
least of its net assets in large capitalization companies with market
capitalizations of more than billion, or that are within the top of the
Russell Index, at the time of purchase. The Fund invests mainly in
dividend-paying stocks the manager believes will approximate the dividend yield
of companies in the Standard & Poor's Index (S&P

- --------------------------------------------------------------------------------
PERFORMANCE DATA REPRESENT PAST PERFORMANCE, WHICH DOES NOT GUARANTEE FUTURE
RESULTS. INVESTMENT RETURN AND PRINCIPAL VALUE WILL FLUCTUATE, AND YOU MAY HAVE
A GAIN OR LOSS WHEN YOU SELL YOUR SHARES. CURRENT PERFORMANCE MAY DIFFER FROM
FIGURES SHOWN.
- --------------------------------------------------------------------------------

This annual report for Fiduciary Large Capitalization Growth and Income Fund
covers the fiscal year ended July

PERFORMANCE OVERVIEW

Fiduciary Large Capitalization Growth and Income Fund delivered a total
return for the months ended July The Fund underperformed its
benchmark, the S&P which returned during the same period.

INVESTMENT STRATEGY

We are research driven, fundamental investors, pursuing a blend of growth and
value strategies. We use a top-down analysis of macroeconomic trends, market
sectors (with some attention to the sector weightings in the Fund's comparative
index) and industries combined with a bottom-up analysis of individual
securities. In selecting investments for the Fund, we look for companies we
believe are positioned for growth in revenues, earnings or assets, and are
selling at reasonable prices. We employ a thematic approach to identify sectors
that may

The Russell Index is market capitalization weighted and measures
performance of the largest companies in the Russell Index,
which represent approximately of total market capitalization of the
Russell Index. The S&P consists of stocks chosen for market
size, liquidity and industry group representation. Each stock's weight in
the index is proportionate to its market value. The S&P is one of the
most widely used benchmarks of U.S. equity performance.

Source: Standard & Poor's Micropal. See footnote for a description of
the S&P The index is unmanaged and includes reinvested dividends. One
cannot invest directly in an index, nor is an index representative of the
Fund's portfolio.

THE DOLLAR VALUE, NUMBER OF SHARES OR PRINCIPAL AMOUNT, AND NAMES OF ALL
PORTFOLIO HOLDINGS ARE LISTED IN THE FUND'S STATEMENT OF INVESTMENTS (SOI). THE
SOI BEGINS ON PAGE


Annual Report |


PORTFOLIO BREAKDOWN
Fiduciary Large Capitalization
Growth and Income Fund


- --------------------------------------------------------------------------------
% OF TOTAL
SECTOR/INDUSTRY NET ASSETS
- --------------------------------------------------------------------------------
Energy Equipment & Services
- --------------------------------------------------------------------------------
Oil, Gas & Consumable Fuels
- --------------------------------------------------------------------------------
Insurance
- --------------------------------------------------------------------------------
Aerospace & Defense
- --------------------------------------------------------------------------------
Communications Equipment
- --------------------------------------------------------------------------------
Machinery
- --------------------------------------------------------------------------------
IT Services
- --------------------------------------------------------------------------------
Health Care Equipment & Supplies
- --------------------------------------------------------------------------------
Food Products
- --------------------------------------------------------------------------------
Food & Staples Retailing
- --------------------------------------------------------------------------------
Health Care Providers & Services
- --------------------------------------------------------------------------------
Media
- --------------------------------------------------------------------------------
Diversified Financial Services
- --------------------------------------------------------------------------------
Hotels, Restaurants & Leisure
- --------------------------------------------------------------------------------
Biotechnology
- --------------------------------------------------------------------------------
Industrial Conglomerates
- --------------------------------------------------------------------------------
Semiconductors & Semiconductor Equipment
- --------------------------------------------------------------------------------
Pharmaceuticals
- --------------------------------------------------------------------------------
Life Sciences Tools & Services
- --------------------------------------------------------------------------------
Capital Markets
- --------------------------------------------------------------------------------
Software
- --------------------------------------------------------------------------------
Commercial Banks
- --------------------------------------------------------------------------------
Computers & Peripherals
- --------------------------------------------------------------------------------
Diversified Telecommunication Services
- --------------------------------------------------------------------------------
Short-Term Investments & Other Net Assets
- --------------------------------------------------------------------------------

benefit from longer dynamic growth. Within these sectors, we consider the basic
financial and operating strength and quality of a company as well as company
management. The Fund, from time to time, may have significant positions in
particular sectors such as technology or industrials. We also seek to identify
companies that we believe are temporarily out of favor with investors, but have
a good intermediate- or long-term outlook.

MANAGER'S DISCUSSION

For the fiscal year ended July the top contributor to Fund performance
was Australian biopharmaceutical giant CSL (sold during the review period).
Shares of the company rose mostly in response to strong demand for its plasma
products and its planned entrance into the U.S. flu vaccine market. In July, CSL
also announced its agreement to purchase Zenyth Therapeutics.

On a sector basis, the Fund benefited from its overweighted energy and
industrials exposure as oil and natural gas prices rose during the review period
and the global economy continued to expand. Notably, three of the significant
contributors to performance were energy-related stocks SBM Offshore, BJ Services
and Exxon Mobil. Shares of industrials sector companies United Technologies and
Danaher also increased in value, benefiting from a general trend of increased
capital spending. Japanese machinery maker Komatsu benefited from the same trend
in addition to Japan's market recovery.

International stocks, which comprised nearly of the Fund's net assets at
period-end, generally outperformed their domestic counterparts during the review
period. As a result, several of the Fund's investments in Europe, Canada, Japan
and Australia contributed to performance. We invested in foreign equities in
their respective local currencies, which added to the Fund's return because of
the gradual weakening of the U.S. dollar.

Offsetting the Fund's positive performance were some of the Fund's information
technology and health care holdings. The technology sector was one of the
worst performing sectors during the fiscal year, and Intel and Juniper Networks
were among the Fund's largest detractors. Intel struggled in a difficult market
environment as it faced increased competition from Advanced Micro Devices.
Juniper was the subject of an SEC probe into its accounting practices. Shares of

The energy sector comprises energy equipment and services; and oil, gas
and consumable fuels in the SOI. The industrials sector comprises
aerospace and defense, industrial conglomerates and machinery in the SOI.

The information technology sector comprises communications equipment,
computers and peripherals, IT services, semiconductors and semiconductor
equipment, and software in the SOI. The health care sector comprises
biotechnology, health care equipment and supplies, health care providers
and services, and pharmaceuticals in the SOI.


| Annual Report


Boston Scientific declined as negative developments surrounding the company's
medical device business surfaced during the review period, including product
recalls from recent acquisition Guidant. Although we believe these companies
will rebound based on their positions in their respective markets and quality of
management, near-term performance has been disappointing.

Thank you for your continued participation in Fiduciary Large Capitalization
Growth and Income Fund. We look forward to serving your future investment needs.


/s/ S. Mackintosh Pulsifer

S. Mackintosh Pulsifer
Vice President of Fiduciary International, Inc. (Fiduciary)
Senior Vice President of Fiduciary Trust Company International
(Fiduciary Trust)


/s/ Kenneth J. Siegel

Kenneth J. Siegel
Vice President of Fiduciary

Dear Shareholder:

We are pleased to bring you Franklin Global Real Estate Fund's inaugural annual
report. During the period from the Fund's inception on June through
July global economic growth began to moderate. Many central banks
tightened monetary policy, thereby reducing the immense liquidity that helped
fuel the recent recovery. The possibility of future rate increases in many
countries remained, although the market consensus was that an end to the current
U.S. tightening cycle might be near. Economic growth sustained strong demand for
oil and other commodities, which elevated prices. Oil prices reached a
historical high during the period. The U.S. housing market showed evidence of
cooling. In this short review period, global equity markets were generally
higher. From June through July the Morgan Stanley Capital
International (MSCI) Europe, Australasia, Far East (EAFE) Index returned


In the enclosed annual report for Franklin Global Real Estate Fund, the
portfolio managers discuss market conditions, investment management decisions
and Fund performance during the period under review. You will also find
performance data and financial information. Please remember that all securities
markets fluctuate, as do mutual fund share prices.

Source: Standard & Poor's Micropal. The MSCI EAFE Index is a free
float-adjusted, market capitalization-weighted index designed to measure
equity market performance in global developed markets excluding the U.S.
and Canada.

EDELIVERY DETAILS

Log in at franklintempleton.com and click on eDelivery. Shareholders who are
registered at franklintempleton.com can receive these reports via email. Not all
accounts are eligible for eDelivery.

- -----------------------------------------------------
NOT FDIC INSURED | MAY LOSE VALUE | NO BANK GUARANTEE
- -----------------------------------------------------


Not part of the annual report |


If you would like more frequent updates, FRANKLINTEMPLETON.COM provides daily
prices, monthly performance figures, portfolio holdings and other information.
You can also access your account, buy and sell shares, read timely articles, and
find helpful financial planning tools. We hope you will take advantage of these
online services.

As always, we recommend investors consult their financial advisors and review
their portfolios to design a long-term strategy and portfolio allocation that
meet their individual needs, goals and risk tolerance. We firmly believe that
most people benefit from professional advice, and that advice is invaluable as
investors navigate changing market environments.

We thank you for investing with Franklin Templeton, welcome your questions and
comments, and look forward to serving your investment needs in the years ahead.

Sincerely,
