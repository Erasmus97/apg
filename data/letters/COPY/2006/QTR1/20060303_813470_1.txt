DEAR SHAREHOLDERS,

YEAR IN REVIEW
Every year the capital markets are confronted with events that shape the
attitudes of investors and the direction of market prices for the year.
The year proved no different and the laundry list of concerns was
apparent from the start of the year. First, on the domestic front,
investors were anxious about the magnitude and extent to which the Federal
Reserve would continue to raise short-term interest rates. The Fed had
already raised short-term interest rates from a low of percent in mid
to percent by the end of that year. As began, policy makers
sent notice to the capital markets that several more interest rate hikes were
necessary to protect against anticipated higher inflation as economic growth
accelerated. By June, the Fed raised short-term interest rates another
percent and continued to telegraph that more were in the cards. At the same
time, crude oil prices surged from about per barrel in January to a high
of almost per barrel in late August. In conjunction but with fewer
headlines, natural gas prices more than doubled from December through
September. The cause was textbook economics; strong domestic and foreign
demand outpaced worldwide supplies of both oil and natural gas. This supply
and demand imbalance was further compounded by hurricanes Katrina and Rita.
These massive storms battered the Gulf Coast region and severely crippled the
nations ability to drill for and refine petroleum products. After weeks of
repair, these facilities came back online and oil prices retreated by years
end. Although the combination of rising interest rates and dramatically higher
energy prices dampened consumers spending habits a bit, easy credit options
such as adjustable rate mortgage products and low interest loans kept spending
elevated and economic growth on high ground.

CAPITAL MARKET PERFORMANCE
With this backdrop, stocks and bonds reacted accordingly. Higher energy
prices, rising core inflation and an aggressive Federal Reserve kept fixed
income investors on the defensive most of the year. While yields on short
maturing Treasury notes rose considerably, yields on longer dated Treasury
bonds declined slightly. For the year, the Treasury note declined
about percent in price while the Treasury bond increased about
percent in price. Bonds rated below investment grade known as junk bonds,
had a total return of only about percent for the year. After stellar
returns over the past two years, investors concluded high yield bonds were
fully valued with limited upside potential. In the equity markets, returns
were generally positive with certain industries and stocks performing well
above average. The energy sector surged between and percent amidst
significantly higher gas prices and constrained supplies. Internet content
provider Google and computer maker Apple Computer proved standouts in the
technology sector. The explosion of Internet advertising and frenzy
surrounding the iPod music player led to very strong revenue growth at each
company and much higher stock prices. Across the market capitalization
spectrum, mid cap stocks handily beat both small and large stocks for the year.
Large growth stocks, which have lagged large value since finally posted
results better than their large value counterpart. In the broader markets,
the S&P Index returned percent while the Nasdaq Composite Index posted
a percent return.

Most of the Advance Capital I Funds posted results generally in-line or better
than their respective benchmarks for the year. The Equity Growth Fund returned
percent as compared to the Lipper Mid Cap Growth Index which returned
percent. The Balanced Fund, with its mix





of stocks and bonds returned percent, compared to the Lipper Balanced
Index which returned percent. The Retirement Income Fund increased
percent, compared to the Lipper BBB Index which returned percent and the
Lehman Aggregate Index which returned percent. The Cornerstone Stock Fund
returned percent as compared to the Lipper Large Growth Index which
returned percent. The under performance in the Cornerstone Stock Fund was
caused by an under weighting to the energy sector along with stellar returns in
a few high growth stocks which the Fund was either under weight or did not own.
The more traditional large growth stocks produced below average results for the
year.

YEAR AHEAD
Looking ahead to economic growth will likely cool as consumers feel the
pinch of elevated energy prices, higher interest rates and a softening housing
sector. Although the unemployment rate is well below the historical average
and income levels have risen during the economic expansion, consumers will
likely remain a bit cautious in their spending decisions. Combined with high
household debt levels and general employment uncertainty in the steel,
automotive and airline industries, consumer spending is estimated to slow in
the year ahead. On the other hand, after fifteen consecutive quarters of
double-digit earnings growth, many businesses are flush with cash and prepared
to spend it on new expansion plans, acquisitions or dividends to shareholders.
At the federal level, deficit spending for the war in Iraq and federal income
tax relief is expected to continue at the detriment of private investments.
Also, the Treasury has reissued the Treasury bond to finance these
projects which could push long-term interest rates higher in Within
this economic environment, fixed income securities will likely struggle to
produce more than coupon type returns in Equity securities, however,
should benefit from subdued inflation, modest economic growth, shareholder
friendly deals and strong corporate balance sheets. However, the streak of
fifteen quarters of double-digit corporate earnings growth could be at risk as
input costs continue to rise and global competition remains intense. Still,
high single digit returns for equities is a strong possibility in As
always, forecasting capital market returns is tricky and subject to change
based on new information and dynamic economic data. Through it all, we remind
clients to stay well diversified and to focus on long term investing.

At December the four Advance Capital I, Inc. Funds held about
billion in total net assets. This is about percent more than the same time
last year. Intelligent investment decisions, broad diversification and control
of costs remain the building blocks of our philosophy. We fully expect this
approach will continue to serve our investors well over time. We thank you for
your continued confidence and look forward to providing you with service and
results designed to meet or exceed your long term investment objectives. If
you have questions or if we may be of service, please call us. Our toll-free
number is

Sincerely,
