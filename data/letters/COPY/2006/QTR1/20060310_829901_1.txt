Dear Shareholders: We are pleased to provide you with this overview of the Lord
Abbett Global Equity Fund's and Lord Abbett Global Income Fund's strategies and
performance for the year ended December On this and the following
pages, we discuss the major factors that influenced performance.

Thank you for investing in Lord Abbett mutual funds. We value the trust that
you place in us and look forward to serving your investment needs in the years
to come.

BEST REGARDS,
