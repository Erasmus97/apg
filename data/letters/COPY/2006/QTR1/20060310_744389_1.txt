Dear Shareholder,
 
 
 
i
  
 
 
 
ii 
 
 
 
 
 
iv 
 
 
 
 
 
          Despite
  relatively weaker economies, the international stock markets generally
  outperformed their U.S. counterparts during the fiscal year. Japan was the
  best performing region for the period. Its market began to rally sharply
  during the summer of 2005 when the Liberal Democratic Party secured a
  landslide election victory and overseas investors increased their purchases
  of Japanese equities. The stock markets in Europe were also strong. The
  decline in the value of the euro
 
 
Smith Barney
  International Large Cap Fund
I
 
helped the regions
  exporters, while high oil prices led to a rise in many energy-related stocks.
  The steady improvement in macroeconomic data, combined with an increase in
  merger and acquisition activity, also supported European equities.
 
 
 
          Please
  read on for a more detailed look at prevailing economic and market conditions
  during the Funds fiscal year and to learn how those conditions have affected
  Fund performance.
 
 
 
Special Shareholder Notices
 
 
 
On December 1, 2005,
  Citigroup Inc. (Citigroup) completed the sale of substantially all of its
  asset management business, Citigroup Asset Management (CAM), to Legg Mason,
  Inc. (Legg Mason). As a result, the Funds investment adviser (the
  Manager) and Sub-Adviser, previously indirect wholly-owned subsidiary of
  Citigroup, has become a wholly-owned subsidiary of Legg Mason.
  Completion of the sale
  caused the Funds existing investment management contract and subadvisory
  contract to terminate. The Funds shareholders approved a new investment
  management contract between the Fund and the Manager and a new sub-advisory
  contract which became effective on December 1, 2005.
 
 
 
          Effective
  February 1, 2006, Smith Barney Fund Management LLC (the Manager), the Funds
  investment manager, assumed portfolio management responsibilities for the
  Fund. Charles Lovejoy, Guy Bennett, Christopher Floyd and John Vietz,
  investment officers of the manager, became the portfolio managers of the
  Fund. The sub-advisory contract with Citigroup Asset Management Limited has
  been terminated. Mr. Lovejoy is a Director and Senior Portfolio Manager of
  Batterymarch Financial Management, Inc. (Batterymarch), and Mr. Bennett,
  Mr. Floyd and Mr. Vietz are each Portfolio Managers of Batterymarch, which,
  like the manager, is a subsidiary of Legg Mason, Inc.
 
 
 
          On
  or about April 7, 2006, the Smith Barney International Large Cap Fund will be
  renamed Legg Mason Partners International Large Cap Fund.
 
 
 
Information About Your Fund
 
 
 
As you may be aware,
  several issues in the mutual fund industry have recently come under the
  scrutiny of federal and state regulators. The Funds Manager and some of its
  affiliates have received requests for information from various government
 
 
II
Smith Barney International Large Cap Fund
 
 
 
regulators regarding
  market timing, late trading, fees, and other mutual fund issues in connection
  with various investigations. The regulators appear to be examining, among
  other things, the Funds response to market timing and shareholder exchange
  activity, including compliance with prospectus disclosure related to these
  subjects. The Fund has been informed that the Manager and its affiliates are
  responding to those information requests, but are not in a position to
  predict the outcome of these requests and investigations.
 
 
 
          Important
  information concerning the Fund and its Manager with regard to recent
  regulatory developments is contained in the Notes to Financial Statements
  included in this report.
 
 
 
          As
  always, thank you for your confidence in our stewardship of your assets. We
  look forward to helping you continue to meet your financial goals.
 
 
 
Sincerely,


