Dear Fellow Shareholder:

As the name of your fund clearly states, Churchill Tax-Free Fund of
Kentucky is tax-free* - free from Kentucky state and regular Federal income
taxes.

This tax-free status serves many purposes. It obviously provides you, and
our other shareholders, with tax-free income. But, it also provides
municipalities with an attractive motivator to help them raise revenue.
Additionally, the projects funded by these tax-free securities enhance the
quality of life for all community residents.

Many individuals pay as much as to of their income in Federal and
state taxes. Obviously, if you have to pay that much in taxes, it significantly
reduces what you get to keep in your pocket.

BENEFITS TO INVESTORS

As an investor seeking tax-free income, do you have to forego higher
yields? Not necessarily.

[BAR CHART]

Hypothetical Tax-Free Yield
Hypothetical Taxable Equivalent Yield

Hypothetical Tax-Free Yield
Hypothetical Taxable Equivalent Yield

Hypothetical Tax-Free Yield
Hypothetical Taxable Equivalent Yield

This chart assumes a federal and state tax-rate
and is for illustration purposes only; it does not
represent past or future performance of any investment.

The chart above shows that you would have to earn significantly more from
a taxable investment in order to be equal to what you get to keep from a
tax-free investment.

As you can clearly see from the chart, you would have to earn on a
taxable investment in order to equal the tax-free level of

Keep this illustration in mind the next time you examine the yield that
Churchill Tax-Free Fund of Kentucky offers you. You will find that a or
tax-free yield looks considerably more attractive to you when you consider
the implications of taxes.

NOT A PART OF THE ANNUAL REPORT


BENEFITS TO MUNICIPALITIES

As you are most likely aware, states, counties, cities, towns, and other
forms of municipalities issue tax-free bonds to raise monies. These issues
generally take two forms - general obligation securities and revenue securities.

General obligation municipal securities are primarily those securities
used to finance the general needs of such municipalities. General obligation
securities are secured by the tax-raising power of the specific municipality in
terms of its ability to pay interest and repay principal on a timely basis.
Thus, these securities are considered to be "backed by the full faith and
credit" of the issuer.

Revenue securities are issued to finance specific projects, such as a
hospital or airport. In this instance, the municipal issuer pledges the
operating revenues derived from the specific project to pay the interest and
repay the principal when due.

In many cases, smaller municipalities would have difficulty selling these
securities to the marketplace were it not for the added attractiveness of
tax-free status.

BENEFITS TO QUALITY OF LIFE

The benefit you may not have considered - when you made your investment in
the Fund - was that in the process of having the Fund provide you with tax-free
income, it also provides help to a variety of others within your community and
Kentucky. This is a benefit in which you can take real pride.

Municipal bonds, such as those in which Churchill Tax-Free Fund of
Kentucky invests, are the primary way infrastructure is financed. Infrastructure
is a relatively obscure word that by itself doesn't mean much to most people.
But, when you translate the word "infrastructure" into projects such as schools,
roads, bridges, water facilities, pollution control, airports, hospitals, and
fire and police stations, then you are speaking in terms that people more easily
understand.

As the economy of Kentucky grows, new and additional municipal projects
are needed for the benefit of the citizens of Kentucky and the various
communities throughout the Commonwealth. In essence, your money invested in the
Fund helps pay for that new school, road, airport, etc. that you and your
neighbors now enjoy. So, as an investor in Churchill Tax-Free Fund of Kentucky,
you can take pride in knowing that you are playing a vital role and a very real
part in enhancing the quality of life for your family, friends, neighbors and
future generations of Kentuckians.

SUMMARY

So the next time that you receive your statement from Churchill Tax-Free
Fund of Kentucky, remember that the benefits that are reaped from your
investment are more than just what is evident on that piece of paper.

Sincerely,
