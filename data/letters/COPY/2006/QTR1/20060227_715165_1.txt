Dear Shareholders:

U.S. stocks and bonds limped in with modest returns for the fourth
quarter of for the S&P and for the Lehman Aggregate), no big
departure from the prior three quarters' average returns S&P
Lehman Aggregate). For the year, stock returns were modestly ahead of inflation,
bond returns modestly behind. In contrast to this low-volatility, low-return
environment for the markets, was marked by the worst natural disasters in
U.S. history, disillusionment over the slow progress in Iraq, record energy
prices, and Federal Reserve interest rate hikes totaling two percentage points.
Still, the U.S. economy did a lot better than limp home in GDP growth
reached and possibly exceeded the nation's long-term trend growth rate of
and corporate profits are estimated to have increased to record levels. At
the same time, the general level of consumer prices - outside of energy and
other commodities - remained under control, increasing only around in
the straight year that the core inflation rate came in under

So, with corporate profits up in but stock prices only (S&P
what accounts for this roughly decline in stock P/E multiples? For one
thing, it is not unusual following market bubbles for stock
price/value multiples to continue declining well after stock prices themselves
have bottomed (October in the current cycle). Federal Reserve monetary
tightening of the magnitude seen in the past months also raises questions
about the sustainability of today's record profitability, undermining P/Es. High
oil prices also threaten to push inflation and interest rates higher and P/Es
lower. In any event, at year-end the S&P was priced at times
forecast year-ahead earnings, a lot closer to its October low than to
the early high - and a good P/E points below the peak P/E multiples
seen in the bubble years of

Considering that the Fed raised short-term interest rates by two
percentage points in the U.S. bond market had a respectable enough
performance, managing a small positive return for the year. Despite
energy- and hurricane-related pressures on prices, market estimates of inflation
decreased roughly one-third of a percentage point last year. We wouldn't be
surprised to see some uptick in inflation expectations this year, but we don't
expect more than a modest rise. The Federal Reserve's program of measured
interest rate increases is expected to come to a close during the first half of
with the fed funds rate topping out at to and longer Treasury
yields staying close to their year-end levels around

In contrast to subdued stock market environment, has begun
with five straight advances and the first reading above on the Dow Jones
Industrial Average since June What's more, market breadth continued the
improvement begun in November, with the NYSE cumulative advance/decline line
approaching last summer's highs in the first week of January. While
such early momentum has often been a positive market indicator in years past, we
would be even more encouraged if there were to be a transition in market
leadership away from the energy sector, which was the strongest market sector in
and That is not to say that there weren't other strong performers
last year; in fact, mid- and small-cap stocks easily outperformed big-cap U.S.
stocks in as did foreign equities. As begins, we believe that
investors can take encouragement from mounting evidence that the current cycle
of Federal Reserve interest rate hikes is coming to an end. That prospect, along
with our forecast of a increase in corporate profits, forms the basis for
expecting stock and bond returns to reach more normal levels in the year to
come.

Finally, I would like to express my continuing appreciation for your
confidence in Wright Investors' Service and to wish you a happy, healthy and
prosperous As always, I invite your questions as well as suggestions on
how we can better serve your investment needs.

Sincerely,
