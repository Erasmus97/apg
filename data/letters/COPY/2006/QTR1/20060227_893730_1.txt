Dear
Shareholder:
Enclosed is the report of
the operations for the Integrity Value Fund (the
“Fund”) for the year ended December 30, 2005. The
Fund’s portfolio and related financial statements are
presented within for your review.
The second leg
run up in the stock market that began in August, 2004 paused for
a breather in early 2005. This consolidation period continued in
a downward pattern, reaching an extremely oversold condition on
April 20. A strong rally ensued. The S&amp;P 500 subsequently
established another meaningful bottom (but at a higher level) on
October 13, 2005, reaching a significantly oversold condition,
affording investors the last opportunity to “Buy the
Dips” in 2005. Sluggish market conditions in December, 2005
set the stage for a strong market in 2006 and beyond.
2005 brought us
concerns over high oil prices, inflation, and declining consumer
confidence. However, recent market resilience hints that business
and consumers will muddle through somehow, in spite of higher oil
prices. Contrary to conventional wisdom in the fall of 2004 and
at the outset of 2005, we strongly suggested technology and
healthcare as areas providing attractive valuations. Healthcare
and technology have both led the way for much of 2005. As
predicted, the economy continues down a path of solid, steady
growth.
The Value Fund
(+6.70%*) outperformed the benchmark and the Morningstar large
value category over the 2005 calendar year. This was due to
several factors: we were overweight in the Semiconductor and
Semiconductor Equipment areas. Late in 2004, we purchased shares
of Texas Instruments, Intel, and Lam Research based on attractive
valuations, competitive advantage, and anticipation of increased
revenues. Texas Instruments and Lam Research have appreciated
nicely. Intel’s performance was slightly disappointing. We
continue to believe it has good upside potential as it overcomes
constraints that have limited manufacturing capacity.
E-Trade’s (Diversified Financial) share price benefited
from consolidation amongst electronic trading platforms. Also
contributing positively to performance were overweight positions
in select biotechnology issues. Genzyme has capitalized on the
strength of its genetic disorder products. Kos Pharmaceutical,
which specializes in time-release cholesterol lowering drugs,
appreciated significantly during the year, but recently suffered
a significant decline upon announcement that Merck had a similar
product targeted for sale in 2008. We feel the price drop in Kos
Pharmaceutical shares was unwarranted. The strength of
Merck’s presentation rested on using Kos’ data, and
if anything, should strengthen Kos Pharmaceutical’s
franchise over the next few years. Eon Labs (sold) profited
handsomely, becoming the acquisition target of Novartis. Our
overweight position in insurance stocks versus banks contributed
positively to performance, with W. R. Berkley being a big winner.
Among the other winners were energy players Conoco Phillips
(Integrated Oil &amp; Gas) and Maverick Tube (Oil &amp; Gas
Equipment). When viewed versus benchmark, the Fund’s
performance was negatively affected by its underweight exposure
to high yield utilities, health care providers, and multiline
retail. Individual securities hurting Fund performance were
positions in Andrx (Pharmaceuticals), Angiotech Pharmaceuticals
(Pharmaceuticals), and Boston Scientific (Health Care Equipment);
Andrx received notice from the FDA concerning compliance of its
Davie Florida production facility. Management’s response
appears to have allayed investor’s fears as the stock
rebounded nicely in the fourth quarter. Boston Scientific and
Angiotech underperformed despite strong revenues and solid market
share in drug eluting stents.
We manage the
Value Fund using our quantitative intelligent expert system for
stock selection. The portfolio is focused on profitable value
stocks that areexpected increased unit volume beneficiaries.
These are typically enterprises, cyclical or otherwise, that are
experiencing an improved outlook. Such usually leads to upward
revisions in earnings estimates and superior investment
performance. We do not own speculative “Red Ink Turn
Arounds” with an unattractive business franchise. We like
to think of ourselves as investors in profitable companies, not
speculators.
We are currently
underweight in utilities, retail apparel, banks, automobiles and
components, multiline retail, materials, consumer staples, media,
and integrated oil &amp; gas and oil &amp; gas exploration. We
are overweight in semiconductor, semiconductor equipment,
wireless telecom issues, biotechnology, pharmaceuticals,
construction &amp; farm equipment, insurance, and homebuilding.
We continue to focus on companies that are experiencing
“positive change” while still selling at reasonable
valuations as opposed to high-yielding, low-growth defensive
stocks such as utilities and consumer staples. We do not invest
in high yield issues without regard to the viability and
attractiveness of the underlying business franchise. At the end
of December, 2005, the portfolio had an average forward P/E ratio
of approximately 14, a consensus future earnings growth rate of
17% and an average ROE of 19%.
While value
investing is generally considered to be a defensive style, often
underperforming in a bull market, we nevertheless believe our
Value Fund is well positioned to take advantage of what we expect
to be a vibrant economic expansion in the years ahead.
If you would like
more frequent updates, visit our website at
www.integrityfunds.com for daily prices along with pertinent Fund
information.
Sincerely,


Dear
Shareholder:
Enclosed is the
report of the operations for the Integrity Small Cap Growth Fund
(the "Fund") for the year ended December 30, 2005. The Fund's
portfolio and related financial statements are presented within
for your review.
The second leg
run up in the stock market that began in August 2004 paused for a
breather in early 2005. This consolidation period continued in a
downward pattern, reaching an extremely oversold condition on
April 20. A strong rally ensued. The S&amp;P 500 subsequently
established another meaningful bottom (but at a higher level) on
October 13, 2005, reaching a significantly oversold condition,
affording investors the last opportunity to “Buy the
Dips” in 2005. Sluggish market conditions in December 2005
set the stage for a strong market in 2006 and beyond.
2005 brought us
concerns over high oil prices, inflation, and declining consumer
confidence. However, recent market resilience hints that business
and consumers will muddle through somehow, in spite of higher oil
prices. Contrary to conventional wisdom in the fall of 2004 and
at the outset of 2005, we strongly suggested technology and
healthcare as areas providing attractive valuations. Healthcare
and technology have both led the way for much of 2005. As
predicted, the economy continues down a path of solid, steady
growth.
The Small Cap
Growth Fund (+11.65%*) outperformed the Morningstar midcap growth
category but fell short of the benchmark over the 2005 calendar
year. Assisting performance was our overweight position in
healthcare. Resolution to Medicare / Medicaid reimbursement rate
issues lifted some of the uncertainty surrounding our holdings in
the healthcare services and healthcare facilities areas,
resulting in outstanding performance by Express Scripts and
Community Health Systems. Kos Pharmaceutical (Biotechnology),
which specializes in time-release cholesterol lowering drugs,
appreciated significantly during the year, recently suffered a
significant decline upon announcement that Merck had a similar
product targeted for sale in 2008. We feel this to be a severe
over-reaction. The strength of Merck’s presentation rested
on using Kos Pharma’s data, and if anything, should
strengthen Kos Pharma’s franchise over the next few years.
Our position in Eon Labs (sold) profited handsomely, becoming the
acquisition target of Novartis. We were also overweight in oil /
gas drilling and equipment concerns (Precision Drilling &amp;
Maverick Tube) that performed quite well. Among financials, we
were overweight Diversified Financials and Insurance, with
E-Trade (Diversified Financial) and W.R. Berkley being our best
performers in this group. Among other bright spots were: Cerner
(Health Care Services – recently sold), Lam Research
(Semiconductor Equipment), and Hyperion Solutions (Application
Software). Transportation issues were a drag on performance.
Fears of an economic slowdown and higher fuel prices had a
negative impact on J.B. Hunt, Arkansas Best, and Yellow Corp.
Other securities hurting fund performance were Angiotech
Pharmaceuticals and Andrx, (Pharmaceuticals). Andrx received
notice from the FDA concerning compliance of its Davie, Florida
production facility. Management’s response appears to have
allayed investor’s fears as the stock rebounded nicely in
the fourth quarter. When viewed versus the benchmark, underweight
positions in Asset Management, Diversified Metals, Consumer
Staples, Utilities, Specialty Stores, Apparel Retail, and Casinos
&amp; Gaming stocks had a negative impact on the fund.
We manage the
Fund using our quantitative intelligent expert system for stock
selection. The portfolio is focused on profitable small cap
growth stocks that areexpected increased unit volume
beneficiaries. These are typicallyenterprises that are in the
process of launching high demand proprietary products, or other
firms, cyclical or otherwise, that are experiencing an improved
outlook. Such usually leads to upward revisions in earnings
estimates and superior investment performance. We do not own
speculative micro cap “Story Stocks”. We like to
think of ourselves as investors in profitable companies, not
speculators.
We focus on
rapidly growing companies that were selling at reasonable P/E
ratios. Relative to benchmark, we are currently underweight in
utilities, integrated telecomm, diversified metals, consumer
staples, consumer discretion, diversified commercial, machinery,
regional banks, asset management, and overweight in wireless
telecom, healthcare services and facilities, biotechnology,
pharmaceuticals, diversified financial, insurance, technology
distribution, and trucking. We continue to focus on companies
that are experiencing “positive change”. At the end
of December 2005, the portfolio had an average forward P/E ratio
of approximately 15, a consensus future earnings growth rate of
18% and an average ROE of 22%.

Smaller companies generally have much higher growth prospects
than their larger competitors. For this and other reasons, we
believe our portfolio is well positioned to take advantage of the
expanding economy and current market conditions.
If you would like
more frequent updates, visit our website at
www.integrityfunds.com for daily prices along with pertinent Fund
information.
Sincerely,


Dear
Shareholder:
Enclosed is the
report of the operations for the Integrity Technology Fund (the
"Fund") for the year ended December 30, 2005. The Fund's
portfolio and related financial statements are presented within
for your review.
The second leg
run up in the stock market that began in August 2004 paused for a
breather in early 2005. This consolidation period continued in a
downward pattern, reaching an extremely oversold condition on
April 20. A strong rally ensued. The S&amp;P 500 subsequently
established another meaningful bottom (but at a higher level) on
October 13, 2005, reaching a significantly oversold condition,
affording investors the last opportunity to “Buy the
Dips” in 2005. Sluggish market conditions in December 2005
set the stage for a strong market in 2006 and beyond.
2005 brought us
concerns over high oil prices, inflation, and declining consumer
confidence. However, recent market resilience hints that business
and consumers will muddle through somehow, in spite of higher oil
prices. Contrary to conventional wisdom in the fall of 2004 and
at the outset of 2005, we strongly suggested technology and
healthcare as areas providing attractive valuations. Healthcare
and technology have both led the way for much of 2005. As
predicted, the economy continues down a path of solid, steady
growth.
The Technology
Fund (+7.39%*) has outperformed both the benchmark and the
Morningstar technology category, largely due to stock selection
and significant underweight and overweight (by market cap &amp;
in key sub industries) of the technology sector. We are
underweight many of the large, more mature technology issues. We
are also underweight Semiconductors, Computer Hardware and
Communications Equipment sub industries. However, we did add
Texas Instruments and Intel to the portfolio in December 2004
based on attractive valuations, competitive advantage, and
anticipation of increase revenues. Texas Instruments, the
world’s largest provider of cell phone chips, has performed
extremely well while Intel has been slightly disappointing. We
continue to believe Intel has some good upside potential as it
overcomes constraints that have limited manufacturing capacity.
Small and Mid-Cap Biotechnology and Pharmaceutical companies,
with their proprietary technologies, continue to play an
important role in the portfolio. Kos Pharmaceutical, which
specializes in time-release cholesterol lowering drugs,
appreciated significantly during the year, but recently suffered
a significant decline upon announcement that Merck had a similar
product targeted for sale in 2008. We feel the price drop in Kos
Pharma shares was a severe over-reaction. The strength of
Merck’s presentation rested on using Kos Pharma’s
data, and if anything, should strengthen Kos Pharma’s
franchise over the next few years. Our position in Eon Labs
(sold) profited handsomely, becoming the acquisition target of
Novartis. Our better performers include: America Movil (Wireless
Telecom), which continues to grow its subscriber base in Latin
America, Harris Corp, a company specializing in state-of-the-art
satellite communications equipment provided primarily to
government, and E-Trade (Diversified Financial), which is
benefiting from consolidation amongst electronic trading
platforms. Individual securities hurting fund performance were
Boston Scientific (Health Care Equipment), Angiotech
Pharmaceuticals and Andrx (Pharmaceuticals).
We manage the
Technology Fund using our quantitative intelligent expert system
for stock selection. The portfolio is focused on profitable
technology stocks that are expected increased unit volume
beneficiaries. These are typically enterprises that are in the
process of launching high demand proprietary products, or other
firms, cyclical or otherwise, that are experiencing an improved
outlook. Such usually leads to upward revisions in earnings
estimates and superior investment performance. We do not own
speculative micro cap “Story Stocks” and “Hot
New Issues” that don’t have a real business
franchise, reminiscing of the “Dot Com” era of 1999
and early 2000. We like to think of ourselves as investors in
profitable companies, not speculators.
We focus on
investment in rapidly growing companies with good price momentum
selling at reasonable P/E ratios. Relative to benchmark, we are
currently underweight in data processing, systems software,
communications equipment, computer hardware, integrated telecom,
and overweight in aerospace &amp; defense, healthcare equipment,
biotechnology, internet software, IT consulting, applications
software, electronic manufacturing, and wireless telecom issues.
We continue to evaluate the portfolio and make adjustments as
market conditions warrant, focusing on companies that are
experiencing “positive change”. At the end of
December 2005, the portfolio had an average forward P/E ratio of
approximately 18, a consensus future earnings growth rate of 19%
and an average ROE of 22%.
Contrary to
popular belief, the tech’s are likely to be the driving
force behind the current economic expansion and for decades to
come. Participants in the Technology Fund should continue to
benefit from these trends.
Sincerely,


Dear Shareholder:
Enclosed is the
report of the operations of the Integrity Growth &amp; Income
Fund (the "Fund") for the period from December 1, 2005, through
December 30, 2005. The Fund's portfolio and related financial
statements are presented within for your review.
Integrity Growth
&amp; Income Fund (IGIAX) outperformed the S&amp;P 500 on a raw
numbers basis for the 12 months ended December 30, 2005. Our
12-month return was 9.32%*, compared with a total return of 4.91%
for the S&amp;P 500, despite our conservative holdings in
dividend-paying stocks and a cash position ranging on a month-end
basis from 20% to 52% during the year. We ended 2005 with
approximately 25% in cash. If you've been an investor with us for
long, then you are aware that we feel mutual funds and the entire
investment community should provide more disclosure regarding the
amount of risk their managers take with their investors’
money.
Our turnover has
been moderately high in 2005. The primary reason is our sell
discipline. Our definition of ‘value’ is the amount
by which a stock is selling for less than fair value. In most
cases, stocks selling for above fair value are, by definition,
over valued, or have ‘negative value’. A major part
of our sell discipline is that we sell whenever a stock reaches
approximately fair value. Let someone else take the high risk
gains when it’s more than that. Since most of our stocks,
with the exception of the telecoms, reached fair value this year,
we sold them.
It is this
disciplined process of keeping risk low that we believe
differentiates us from most other mutual funds. We buy in most
cases only when a stock is selling at a large discount to fair
value, and sell when it returns to fair value. Stocks (and stock
markets) oscillate around some central measure of value. For some
people it’s P/E ratios, P/Book or P/Sales ratios. For us
it’s fair value based on discounted free cash flows. Buying
below fair value is a way of building in Benjamin Graham’s
‘margin of safety,’ and is what enables us to keep
our risk level below that of the overall stock market.
Risk is central
to the investment decision-making process. The most important
decision any investor makes is not what level of return to aim
for, but what level of risk, because it is the risk we take that
largely determines our return. It's not enough to beat the market
on a raw numbers basis; managers should strive for a greater
return than the overall market on a risk-adjusted basis. How does
an investor decide between two managers who may have similar or
even very different returns? Is the one with the highest returns
always the best one, or the one with the greatest risk?
There has to be a
better way to define good managers, other than simply on the
basis of raw returns. Fortunately, there is. It’s much like
calculating miles per gallon (MPG) by dividing the number of
miles we drive by the number of gallons of gas we consume.
Similarly, we calculate RAR by dividing the return we earn by the
amount of risk we take. Risk is a simple statistical measure of
how much a stock’s price gyrates above and below its mean
value.
How do we know if
a manager has obtained her return recklessly, or with a great
deal of care with your money and attention to risk? You can only
tell by adjusting those returns for the amount of risk taken. Our
primary goal in managing your fund is to earn a higher
risk-adjusted return than the broad market, not necessarily a
higher raw number return. The reason is simple: unusually high
returns are almost inevitably paired with unusually low returns
during bad times. If you lose 50%, you have to make 100% just to
get back to even. That’s highly unlikely, so high risk
tends to dig holes from which investors cannot recover for many
years, if ever.
With a strategy
emphasizing the highest possible risk-adjusted return, you know
whether or not the level of risk your manager is taking is paying
off, simply by comparing it with a broad market risk-adjusted
return, as we have done below. Integrity Growth &amp; Income Fund
over the 12 months earned 4.24% of return per unit of risk taken,
compared with 2.21% per unit of risk for the S&amp;P 500. Risk in
this context is defined as standard deviation, a broad,
all-inclusive statistical measure of risk generally considered
superior to beta.
Risk-Adjusted Return Comparison
IGIAX vs. S&amp;P 500, 12 mos. Ended
12/30/05
 
Raw Return
Standard Deviation
Risk-Adjusted Return
IGIAX
9.32%
2.20%
4.24%
S&P 500
4.91%
2.22%
2.21%
Note:
If you would like
more frequent updates, visit our website at
www.integrityfunds.com for daily prices along with pertinent Fund
information.
Sincerely,


Dear Shareholders:
Enclosed is the report of the operations for the Integrity All Season Fund (the "Fund") for the period since inception (August 8, 2005) through December 30, 2005. The Fund's portfolio and related financial statements are presented within for your review.
As you may know, the design and objective of the All Season Fund is quite different from the vast majority of mutual funds currently available in the industry. Specifically, the All Season Fund is a risk managed fund that equally weights the impact of market risk and potential returns in the decisions of what to own and when. The Fund has the latitude to hedge our own equity positions with derivatives using futures contracts, owning stocks and bonds or even moving entirely out to cash (T-Bills) if necessary. The Fund can also own substantial international positions through foreign exchange traded funds or ADRs. This new level of flexibility, not commonly seen in the mutual fund space, gives us the advantage and objective of limiting losses and periodic drawdowns to less than 5% while pursuing reasonable returns in all market conditions. The greatest value of the Fund will emerge during the next significant decline in the U.S. stock market as the Fund is designed to capture gains and 
avoid significant losses.
The current asset allocation model in the Fund is approximately 42% equities, 31% bonds and 27% cash. Of the 31% commitment to bonds, our High Yield partner, SMH Capital Advisors invests 20% in lower grade corporate bonds with the remainder invested in a spectrum of Treasury bond ETFs. Our internal investment model calls for a 40% bond allocation and we are looking to add to our bond positions as conditions warrant. Equity positions are fully invested and spread between the financial, healthcare, technology, utilities, natural resources, and metals sectors. One of our largest holdings is EWJ (I Shares Japan Exchange Traded fund) as the fundamental opportunities and technical support for Japan suggests it will continue its strength well into 2006 and beyond. We agree with the widely publicized strength and promise of Asia and emerging countries as well and will add positions when a low risk opportunity presents itself.
Our internal market health model is currently on a short-term buy, however, the intermediate term picture is still overbought and very much on the edge of a significant sell signal. Unfortunately most investors are not even aware of market risk and do not realize the risks they are taking by putting new money into the markets under current conditions. We expect a “deep discount” in the U.S. stock market sometime in 2006, probably by this summer. A deep discount, by our definition is a decline of 20% or more in the broad market averages. While the event by itself will be enough to cause significant damage to most passively held investments and portfolios, we view it as a significant buying opportunity and expect to show excellent absolute and relative performance numbers by the first quarter of 2007.
Investors in the All Season Fund are reminded that as a risk managed strategy, upside gains are likely to marginally under perform broad market benchmarks during up trends. However, investors can also expect to significantly outperform the same during broad market downtrends. The All Season Fund is best described as a “core” position in an investor’s portfolio and we do encourage investors to carry “satellite” investments around this position to take advantage of especially strong cycles in the financial markets. The maximum benefit of owning shares in the All Season Fund will be realized in a full, four-year, economic cycle. Thank you for your continued trust and confidence in the All Season Fund.
If you would like more updates, visit our website at www.integrityfunds.com for daily prices along with pertinent Fund information.
Sincerely,


Dear Shareholder:
Enclosed is the report of the operations for the Integrity High Income Fund (the “Fund”) for the year ended December 30, 2005. The Fund’s portfolio and related financial statements are presented within for your review.


As we historically do not decline as much as the index, we took advantage of the situation to turn our defensive positions into longer and high yielding names that had been beaten down in the correction. We captured some nice trading gains on positions like GMAC that lead to above market returns.
 
Fund Return
Category Return
Lehman Brothers
Integrity High Income A
7.48%*
2.14%
2.74%
Integrity High Income C
6.59%*
2.14%
2.74%

						We never like the question, “Is now a good time for high yield?” Although high yield does offer attractive yields at this time, we feel high yield should be part of an asset allocation that reflects the clients point in time and financial picture, not a “timing” decision.
With much of the Feds interest rate hikes temporarily behind us, high yield should have an easier year. As in last year, we do not focus on the “market” anyway and look at individual companies on their own merits [bottom up].


Sincerely,


