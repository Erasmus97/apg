Dear Fellow Shareholders:

probably turned out to be a better than expected year for the economy, but
perhaps a slightly disappointing year for the market averages. Despite the Gulf
Coast hurricanes and the sharp boost in energy prices, the U.S. economy
continued to exhibit solid growth, and economic activity outside the U.S. was
healthy and improving throughout the year. However, in the U.S., the S&P
returned only about for the year, and this return would be reduced to only
about if we excluded energy and utility stocks. Overseas stock markets, such
as Japan, France, Germany and South Korea, were generally much stronger in their
own currencies.

As we move into we would expect the landscape to continue to evolve. The
huge amount of fiscal and monetary thrust that was put in place from
through will still be exerting a positive push as we enter but by the
second half those forces will have been spent. In particular, despite rate
hikes by the Federal Reserve, we think it is unlikely they will pause after just
one or two more hikes, with short-term interest rates still below From
through short-term interest rates moved in a tight band around and
we still enjoyed excellent economic growth. Moreover, despite the rate hikes
since June and a flat/inverted yield curve, money is just not tight. Yield
spreads on high-yield debt and emerging market instruments are very low, and
loans for LBOs and private equity backed takeovers are available on easy terms.
Consequently, we expect the U.S. economy to remain fairly vibrant, and we don't
expect a near-term pause in the trend of Fed rate hikes, nor a second-half cut
in rates.

Eventually, tighter monetary policy will back up to the housing and auto market
causing some slowdown in U.S. economic activity. However, apocalyptic views seem
overdone. With employment conditions solid and household net worth at record
levels any consumer slowdown will likely be modest. Moreover, in the U.S.
capital spending is likely to pick up, and the lagged impact of hurricane
rebuilding will provide stimulus in that region. In addition, growth in China
appears to continue to be quite robust, and this is driving strength throughout
southern Asia. Japan is in the beginning of what looks like a self-sustaining
expansion, and Europe is stable, if not slightly improving. In addition, other
dynamic economies such as India are likely to grow at - rates, and
countries including Russia and Brazil appear to be set up for another good year
in

For the U.S. stock market, healthy global growth should produce a profit
expansion in the mid-single digits. Profit growth will be mitigated by the
impact of a higher dollar, options expensing, and some likely inability to pass
through raw material cost increases. With our expectation of higher short-term
interest rates, P/Es will likely be range-bound, and the total return for the
U.S. averages will likely be positive, but muted.





Background news for the market in is also likely to be a slight depressant.
During we will experience a pivotal mid-year election, and continue to see
a great deal of jockeying for the White House in In some ways the country
is evenly divided, yet on the other hand there is almost a universal
dissatisfaction with all politicians as polls are equally depressing for
Democrats as well as Republicans. The one thing you can count on is that the
"blame game" will be on in full force. One casualty of all of this high-pitched
negative rhetoric for the stock market will likely be any further
investor-friendly tax legislation. Over the past couple of years, investors have
benefited from the temporary cut in dividend and capital gains rates. These
investor tax cuts are due to expire at the end of They are likely to be
extended to the end of but in dividends may very well be taxed at
the previous rate of instead of For individual investors this is not
a plus.

However, despite this backdrop, it is important to focus on the "biggest
picture." As we wrote last year, "...with fits and starts, the global pie should
continue to grow nicely for many years as the billions of people in Asia, parts
of Latin America, Eastern Europe and Southern Africa become empowered and enter
a self-reinforcing loop characterized by strong liquidity, less government
interference, a strong work ethic, free trade and easy access to "standardized
digital technology." Global opportunity has never been richer. Really low
nominal interest rates, and a continued specialization of labor that results in
lower prices for both goods and services, are healthy conditions for global
growth. Capitalism is alive and well in many parts of the world that were
previously bottled up by anti-growth policies. This creates both opportunity and
challenges for companies, but most importantly the global pie is growing.

As we enter we would expect a continuation of strong merger and
acquisition activity. There is a long list of preconditions that seem to exist
for this: reasonable valuations, reduced leverage, large balances in private
equity funds, slack demand for traditional commercial and industrial loans, etc.
However, perhaps just as important is that many U.S. companies see a difficult
path toward achieving double-digit organic top-line growth. Moreover, many
sectors have already been well consolidated, leaving limited pockets of "prime
real estate." While we don't buy stocks purely for M&A reasons, companies that
meet our normal criteria and where this is an extra kicker are likely to be
emphasized. In addition, financial institutions that are well positioned in
investment banking are an area of emphasis for our portfolios.

In addition, we expect corporations -- after a long period of subdued capital
spending -- to increase budgets next year. While this change may be modest by
historical standards, it will look favorable in comparison to the likely
deceleration in consumer discretionary spending. Consequently, we have
positioned the portfolio with exposure to spending on aerospace, automation,
electrical equipment and infrastructure spending (including oil service), while
having no exposure to autos and housing, and limited exposure to large ticket
retailing.

Finally, we continue to focus on economic sectors that have been starved for
capital in recent decades, but where recent demand trends have been healthy. In
particular, we remain focused on energy (including nuclear) production, refining
and marketing, as well as rail transportation and agriculture. In these areas it
is difficult to ratchet up supply in the short run, so with healthy demand,
pricing remains robust. Meanwhile, managements are being disciplined about
introducing new capacity, and competitive products (ranging from wind and solar
power to trucking) are also constrained for the intermediate future.

Energy prices specifically are likely to remain elevated again in It would
not be surprising if the highest prices seen in are not exceeded, however,
it does seem as if the floor underneath the energy complex has been lifted. The
reality is that there are lots of new consumers outside the traditional
countries adding to demand, while incremental reserves and production capacity
is almost exclusively in the hands of OPEC members in the Gulf Region -- an area
that is not necessarily becoming less volatile. In addition, the fact that the
U.S. economy did not fold (at all) during the fourth quarter energy spike
indicates we can cope better than we expected with such high prices.





On the other side of the ledger, in a world of low nominal returns, it will be
vital to minimize losing investments in Despite a growing global pie,
there will be industries and companies that lose share. Moreover, as we pointed
out in last year's letter, "...companies with large pension and health care
legacy costs, companies with weak balance sheets and a lack of competitive edge
should not be expected to do well." This pressure is not going to let up in
and may in fact be exacerbated by new FASB rules regarding pensions.
Moreover, challenges to companies do not have to emanate from abroad. One has to
look no further than the damage being done by newcomers such as Google and
craigslist to the seemingly entrenched media giants to see how competitive the
world is becoming. Consequently, minimizing the inevitable errors that occur in
investing will be as important as ever in

For the ICAP Equity Fund returned and the ICAP Select Fund returned
versus for the S&P These strong results were achieved primarily
from stock selection, although sector weightings were also additive. In
particular, stock selection in energy and transportation was strong. Both
sectors are benefiting from the cyclical expansion in the global economy and
international trade, as well as from secular growth in the developing economies
in Asia. Pricing is very strong in these industries because the supply
infrastructure is unable to handle current demand; years of underinvestment
created this situation, and it will not be easily or quickly remedied.
Halliburton and Marathon Oil were particularly noteworthy in the energy sector,
while Norfolk Southern and CSX were strong rail names. In the technology sector,
stock selection was led by strong results from Hewlett-Packard where new
management has started making progress on margin improvements in their
enterprise and software businesses. Although stock selection was strong overall,
Cendant detracted from performance when it declined due to lowered expectations
in its travel business.

ICAP INTERNATIONAL FUND

In we transitioned the ICAP Euro Select Equity Portfolio to the ICAP
International Fund. In March, Jerry Senser, our Co-Chief Investment Officer,
strategist and economist, presented an analysis which concluded that the
preconditions for improvement in Japan were falling into place. Consumer and
land prices were bottoming out, the banking system had stabilized, and the
employment picture was improving. Moreover, the corporate culture for Japanese
companies was also changing; share cross-ownerships were down, cash balances
were up, leverage had been reduced and, importantly, ROEs were improving. In the
stock market the price/book value ratio was still low by international
standards, and dividends were beginning to mount. In addition to voluntary
improvement, some Japanese companies were beginning to feel pressure, both
internally and externally, to boost returns.

Consequently, we began to increase our universe of large cap European companies
by adding large cap stocks from Japan, Australia, Hong Kong and Singapore, as
well as from South Korea, Taiwan, etc. Our process stayed the same as the one
employed domestically, and used in Europe since the

Interestingly, while certainly not pioneers in international investing, we don't
feel as though we have missed much! When I started working at a large bank trust
department in the early it was virtually impossible to entice even
multinational corporations to invest a fraction of their pension assets outside
the U.S. In the excluding the big bull market in Japan (which was largely
homegrown), international markets were generally small, and not that
interesting. By the there was a big run in emerging markets that began in
but ended very badly in Since markets have improved again,
albeit only in Japan for the past six months. Given our view of the potential
growth in the global pie, we would still consider this to be "early days." While
there will always be cycles and interruptions, in our view the key trends are
favorable.





In Europe we still see a positive story. Valuations generally remain low,
although not as destitute as they were in March, More importantly, despite
some backtracking at the government level, many European companies are quietly
boosting returns by relocating production, streamlining their operations by
divesting non-core assets, and even buying in their own stock. Private-equity
funds are also hunting for targets, and M&A is alive and well. While some
governments are fighting a rear-guard action to protect uneconomic social
welfare states, the successes a couple of time zones to the east of states
employing pro-growth tax policies and with eager, lower wage work forces is
getting harder to ignore. Within the EU the Irish economy also serves to
highlight the success of forward-thinking pro-growth administrations.

In Asia, again, knowing cycles will come and go, we believe that the pie is
going to grow very large. China still has million people living in deep
poverty in rural areas. India's impressive growth in recent years has just
reached a thin veneer of their population. Moreover, countries such as
Indonesia, the Philippines, Vietnam, Malaysia and Thailand have a combined
population greater than that of the U.S., and approaching that of the EU. Those
more fragile areas are moving in the right direction, and are being pulled along
as satellites of China, Taiwan, Hong Kong and Singapore. From a stock market
perspective valuations are relatively low compared to earnings growth rates, and
although risks are higher, opportunities exist. Moreover, for Western investors
the infrastructure for investing in these companies has improved -- the quality
and availability of information is dramatically better than even five years ago.
Our internal resources are growing daily, and ultimately our platform can be
expanded to the "BRIC" countries (Brazil, Russia, India, China) as larger cap
value oriented opportunities arise and as the rule of law and transparency
gravitate toward those standards necessary to attract capital on terms similar
to those formed in traditional EAFE markets. We see wealth creation abroad
particularly favorable for the Fund's holdings of UBS and Credit Suisse -- the
two largest private banks in the world.

In the ICAP International Fund returned following a performance of
in and in We were honored that Morningstar, which has
followed our Fund's progress since named our team International-Stock Fund
Manager of the Year!

During our performance was driven by strong stock selection, although
sector weightings were also additive. Increased mergers and acquisitions
activity was a dominant theme. Key contributors were Allied Domecq and Scottish
Power. In addition, ICAP's belief in Japan's economic improvement as mentioned
above led to positions in Mitsubishi Corp., Mitsui Fudosan, and Mizuho Financial
Group, all of which performed strongly. Finally, ICAP's energy theme supported
an overweight position in oil and gas companies as well as an overweight in
energy-sensitive utilities like Fortum. Detractors were few. Telecom Italia
under-performed due to very difficult market conditions and Siemens suffered as
their restructuring plan took longer than the market anticipated at the start of
the year.

Thank you for your continued support.

Sincerely,
