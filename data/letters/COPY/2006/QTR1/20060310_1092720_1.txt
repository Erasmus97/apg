DEAR SHAREHOLDER:

Many will remember as a year of unfulfilled promise. The underpinnings were
in place for a robust year of stock market gains. Corporate earnings growth
continued to set new records, and interest rates, inflation and unemployment
remained low. Corporations had massive amounts of cash on hand to grow their
businesses and bolster stock prices through buybacks and dividend hikes. The
appetite for risk bounced back as well, judging by a near record volume of
merger and acquisition activity.

In spite of the positive fundamentals, the major equity indices remained
range-bound for much of the year with the bellwether S&P Index trading
within a narrow point range of to Investors were hoping
for a replay of year-end rally, but it was not to be, as a strong
November rally was unable to overcome the prior month's sell-off. As a result,
the Dow Jones Industrial Average(SM) Index closed the year with fractional gains
of while the S&P Index and Nasdaq Index ended the year with
disappointing and gains, respectively.

For the first time in seven years, large-cap stocks outperformed small caps as
the Russell Index gained compared to for the Russell
Index. Mid-cap stocks outperformed both large and small-cap stocks as
the Russell Midcap(TM) Index gained Although value stocks slightly
outperformed their growth counterparts, it was a statistical dead heat between
the two.

For equity investors, the real action in was in international stocks. The
MSCI EAFE Index gained in dollar terms. Attractive valuations in European
stocks and what looked to be a sustainable economic recovery in Japan attracted
investor interest. The double-digit returns in the developed markets paled in
comparison to returns in emerging market stocks. The benchmark for emerging
market stocks, the MSCI EM Index, returned a whopping during the year.
This volatile corner of the equity universe was driven by strong returns in
Latin American and Middle Eastern markets, among others.

Stock prices were buffeted by strong crosscurrents throughout the year. Although
long-term interest rates remained low, the specter of the Federal Reserve's
("Fed") interest rate tightening campaign weighed heavily on the market. Despite
the fact that job growth remained healthy and consumers used rising housing
prices as an excuse to turn their houses into ATMs to the tune of approximately
billion, consumer confidence suffered its largest decline in years in
the wake of hurricane Katrina and per barrel oil. Neither the subsequent
strong rebound in consumer confidence as energy prices moderated, nor the four
consecutive quarters of double-digit earnings growth was enough to kick stock
prices higher.

The rise in short-term interest rates held stocks back in During the
course of the year, the Fed increased interest rates eight times, bringing the
closely watched Fed Funds Target Rate to It was widely assumed that
long-term interest rates would follow suit--an expectation that held stocks in
check. Long-term interest rates ended the year little changed. While the yield
on the Treasury Bond fell nearly basis points to the yield on
the benchmark Treasury Note ended the year at basis
points higher than its starting point. A combination of foreign buying, low
inflation and demand from both public and private pensions for long maturity
assets conspired to keep long-term rates low throughout the year.

By year-end, the potent combination of steadily rising short-term interest rates
and static long-term rates gave rise to the dreaded inverted yield curve as the
two-year Treasury Note yielded one basis point higher than the
Note. Traditionally, an inverted yield curve portends an economic slowdown.


| THE RYDEX DYNAMIC FUNDS ANNUAL REPORT



LETTER TO OUR SHAREHOLDERS (CONCLUDED)
- --------------------------------------------------------------------------------

We base our outlook on two factors--the economy and interest rates. But if
the threat of rising interest rates keeps the market range bound, we believe
opportunities exist in three areas during energy, technology and health
care. Although energy prices experienced a run-up of historic proportions in
the sector still exhibits favorable supply/demand characteristics. There
is too much demand and not enough supply--a situation that is likely to be in
place for the foreseeable future. Technology's relatively low valuations and the
possibility of an earnings-driven technology replacement cycle may provide
opportunity for the technology sector. And lastly, the defensive nature of
health care stocks will serve investors well should economic growth slow
appreciably.

We appreciate the trust you have placed in our firm by investing with us.

Sincerely,
