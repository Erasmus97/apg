Dear Fellow Shareholders: January

The year turned out to be a much better year for the U.S. economy
than many had initially expected, especially with major negative events
ranging from the rise in oil prices, the Iraq War and deadly hurricanes.
The confluence of good and bad news resulted in the major market indices
staying within a fairly tight trading band, with the S&P Index,
including dividends, up while the smaller capitalization NASDAQ
Index posted a moderate gain of

Energy, utilities and healthcare sectors posted the strongest gains,
while telecommunication services and consumer discretionary sectors
were the worst performers. Masking an overall lackluster stock market
were the gains in the energy sectors, which advanced approximately
during

Putting downward pressure on the financial markets were spiking energy
costs, rising short-term interest rates, concerns about a "housing bubble"
and the effects of Hurricane Katrina. On the positive side, U.S.
corporate profits generally have continued to surpass estimates
resulting in improved corporate balance sheets and dividend payouts.

The bond market, as measured by the Lehman Brothers Gov't/Credit
Intermediate Bond Index, was up a moderate in In spite
of the Federal Reserve's aggressive monetary policy, inflationary
pressures remain subdued providing a floor or flating yield curve
under the bond market.

The market outlook for includes a widespread belief that the
market will rally following confirmation of a final Federal Reserve
rate hike. We believe that stock prices already reflect the end of the
Federal Reserve's tightening cycle and that the key drivers of
performance will be a reacceleration in corporate earnings. Typically,
it takes about six months after the last rate hike before market
multiples begin to expand which suggests the latter half of providing
greater capital appreciation potential. The first half of the year is
expected to be mildly inflationary, as the Federal Reserve takes it foot
off the interest rate peddle. A key economic factor in will be the
extent that consumer spending slows under the weight of higher energy costs,
increased debt levels and moderate wage increases.

Current market momentum suggests favoring the energy, healthcare and
technology sectors while underweighting the consumer discretionary,
financial and housing sectors.

Please visit our website to enroll in the Monetta/Sage College
Tuition Rewards(tm) Program*. Since inception, the program has accumulated
the equivalent of in tuition credits for shareholders.
Shareholders receive in tuition credits upon enrolling in the
program and earn tuition credits annually, at a rate of based
on each shareholder's account balance.

I'd like to thank you for your continued support and the confidence
you've placed in Monetta. We look forward to a happy and prosperous

Sincerely,
