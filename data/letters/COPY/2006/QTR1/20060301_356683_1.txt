Dear Shareholder: 
 
We hope you find the annual report for the Jennison Blend Fund, Inc. informative and useful. In March
2005, Jennison Associates assumed full management of the Funds assets and the Fund became part of the JennisonDryden Fund family. As a JennisonDryden mutual fund shareholder, you may be thinking about where you can find additional growth
opportunities. You could invest in last years top-performing asset class and hope history repeats itself or you could stay in cash while waiting for the right moment to invest. 
 
Instead we believe it is better to take advantage of developing domestic and global investment
opportunities through a diversified portfolio of stock and bond mutual funds. A diversified asset allocation offers two potential advantages. It helps you manage downside risk by not being overly exposed to any particular asset class, plus it gives
you a better opportunity to have at least some of your assets in the right place at the right time. Your financial professional can help you create a diversified investment plan that may include mutual funds covering all the basic asset classes and
that reflects your personal investor profile and tolerance for risk. 
 
JennisonDryden
Mutual Funds gives you a wide range of choices that can help you make progress toward your financial goals. Our funds offer the experience, resources, and professional discipline of three leading asset managers. They are recognized and respected in
the institutional market and by discerning investors for excellence in their respective strategies. JennisonDryden equity funds are advised by Jennison Associates LLC or Quantitative Management Associates LLC (QMA). Prudential Investment Management,
Inc. (PIM) advises the JennisonDryden fixed income and money market funds. Jennison Associates, QMA, and PIM are registered investment advisors and Prudential Financial companies. 
 
Thank you for choosing JennisonDryden Mutual Funds. 
 
Sincerely, 


