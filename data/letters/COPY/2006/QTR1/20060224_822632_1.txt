Dear Shareholders: 
 

 
After the bursting of the technology bubble in 2000-2002, the market rebounded sharply in 2003, then began to slow down to low double-digit
growth in 2004, and in 2005 leveled off further to a modest single-digit growth rate. The economy is growing quite nicely, but growth in stock prices has been dampened by high energy prices and the continuing efforts by the Fed to keep the economy
from overheating by raising interest rates. 
 
The big story was the
out-performance of mid-cap stocks, which did better than both small and large-caps. Beginning in 2003, small caps had done much better than mid- and large-caps, and it had been widely expected that eventually this trend would reverse. This is
beginning to happen, as leadership shifted from small-caps to mid-caps. It remains to be seen whether, as expected, this trend will continue and result in large-caps having their day in the sun in the coming year. 
 
The differences between value and growth were very modest, with value just edging out growth
one more year, continuing a five-year winning streak. This trend is also widely expected to reverse, and the fact that it didnt happen in 2005 was a bit of a surprise. 
 
Total Returns by Russell Style Index 
January 1 through December 31, 2005 
 
Large Cap (Russell 1000)
Mid Cap (Russell MidCap)
Small Cap (Russell 2000)
 
Emerging Markets Growth Fund
 
As always, thank you for investing with us! 
 
 
