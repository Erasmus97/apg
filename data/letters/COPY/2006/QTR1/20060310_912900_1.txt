Dear MMA Praxis Shareholder:

The lessons of reflect the importance of patience and discipline. It was a
year with lots of macro-level worries. For much of the year, markets were flat
or falling. However, there were also short periods when returns came in bunches.
Timing these short bursts of performance would have been difficult. It was also
a year when diversification away from the most mainstream asset classes paid
off. Committing to a fundamentals-based asset allocation also rewarded investors
adhering to this strategy. So, in the end, even though didn't feel like a
particularly good year for investors, it wasn't that bad.

Stocks, as measured by the Standard & Poor's returned percent for the
year, well below their long-term historical average. However, foreign stocks
delivered stellar returns, advancing percent, as measured by the EAFE
Index. Overall, investors with global equity market exposure in their portfolio
had an opportunity to earn reasonably good returns. Domestically, based on most
benchmarks, value-type stocks outperformed their growth counterparts for the
sixth consecutive year (though it should be noted that in the last half of the
year, growth began to outperform value). Value's strong performance was due
largely to energy stocks, which were far and away the year's strongest
performing sector. In the fixed income markets, bonds offered only small,
nominal returns and on an inflation-adjusted basis, delivered negative returns.

First Half Second Half Annual Returns

- --------------------------------------------------------------------------------
S&P Index
- --------------------------------------------------------------------------------
Lehman Brothers Aggregate
Bond Index
- --------------------------------------------------------------------------------
S&P Value Index
- --------------------------------------------------------------------------------
MSCI EAFE Index
- --------------------------------------------------------------------------------

The dangers of market timing

As noted in the opening paragraph, attempts at timing the market can prove to be
difficult. (Market timing is defined as the attempt to anticipate the direction
of securities markets and reposition an investment portfolio in an effort to
capture these anticipated changes in the market). Investors who attempt to time
the market run the risk of missing periods of exceptional returns. This practice
may have a negative effect on a sound investment strategy. The chart below
illustrates the risk of attempting to time the stock market over the past
years. A hypothetical investment in stocks invested at year-end grew to
by year-end However, that same investment would have only grown
to had it missed the best months of stock returns. One dollar invested
in Treasury bills over the period resulted in an ending wealth value of
An unsuccessful market timer, missing the best months of stock
returns, would have received a return that was lower than Treasury bills!
Although successful market timing may improve portfolio performance, it is very
difficult to time the market consistently. In addition, unsuccessful market
timing can lead to a significant opportunity loss.





[The following table was represented as a bar chart in the printed material.]

S&P ...................................
S&P minus best months .............. $
Treasury bills ............................ $

Manager change in the MMA Praxis Core Stock Fund

MMA is very pleased to announce the appointment of Davis Advisers, LLP, as the
new sub-advisor of the MMA Praxis Core Stock Fund. After a thorough review of
the Fund, management, in consultation with the Independent Trustees of MMA
Praxis Mutual Funds, made a decision to hire a new manager to steward the assets
of this Fund. Employing an in-depth, rigorous screening process, we identified
approximately a dozen firms that embodied the right people, investment process,
discipline, and long-term performance record. From this list, we narrowed the
candidates down to three, from which the final selection was made. The MMA
Praxis Trustees approved Davis Advisers as the manager at a specially called
meeting in October of The shareholders of the Core Stock Fund also
approved this change at a specially called shareholder meeting in late December.
Davis Advisers is a well-respected, third generation investment advisory firm
that manages over billion of assets. Davis Advisers has developed a culture
built on stewardship principles, championing shareholders' interests in the
money management industry. Morningstar recently named Chris Davis and Ken
Feinberg (co-managers of the MMA Praxis Core Stock Fund) as Domestic Equity
Investment Manager of the year. We are very excited about the new partnership
between Davis and MMA that links one of the premier investment management firms
in the United States with an organization in business more than years, that
has a deep history of socially responsible investing experience.

Portfolio Performance

MMA Praxis Intermediate Income Fund

The Class A Share (NAV) posted gains of percent in These returns
lagged the benchmark established for the MMA Praxis Intermediate Income Fund
(Lehman Aggregate Bond Index), which returned percent over the same time
period. Compared to its Lipper peer group, the A Shares exceeded the average
intermediate term bond fund category average funds), which gained
percent. For an in-depth analysis of this Fund's strategy, please read the
co-portfolio managers' commentary found later in this report.

MMA Praxis Core Stock Fund

The MMA Praxis Core Stock Fund Class A Share (NAV) returned percent for the
period ended December These returns were less than the
established benchmark (S&P for the MMA Praxis Core Stock Fund, which
returned percent. According to Lipper, the Core Stock Fund's
performance landed it in the percentile among its Large-Cap Core peer group
funds). On a longer-term basis, the Fund (A Share NAV) has delivered an
average annual total return of percent over three years and percent
average annual returns over the past five years. Lipper reports that the A
Shares of the Core Stock Fund was ranked in the and percentile among
its peer category group based on the three and five year numbers cited above,
respectively. The portfolio managers' report will provide you with an in-depth
analysis of those factors that contributed to the Fund's relative performance.





MMA Praxis Value Index Fund

As noted in the introductory comments, value once again outperformed a growth
style of investing in For the year, the MMA Praxis Value Index Fund A
Shares (NAV) were up percent. The benchmark index for this Fund, the Barra
Value Index, gained percent. (Note: it is not possible to invest directly
in an index, which does not incur expenses like an index fund.) Among its Lipper
peer universe, the A Share performance placed the Fund in the
percentile funds). Over the past three years, A Shares have delivered
average annualized returns of percent percentile).

MMA Praxis International

Fund Foreign stock markets enjoyed one of their broadest rallies in recent
memory, beating their United States counterparts for a third straight year.
Global conditions in were close to ideal for international stocks. Low
interest rates worldwide generated a wave of borrowing and investment. European
and Japanese companies continued to restructure, squeezing out larger profits
and boosting their share prices. Against this backdrop, the MMA Praxis
International Fund Class A Share (NAV) returned percent. The Fund
underperformed its benchmark, the Morgan Stanley EAFE Index, which returned
percent. Among its Lipper peer category group funds), the Class A
shares ranked in the percentile.

During the past two years, socially responsible investors (SRI) in international
funds have struggled to compete with their non-SRI competitors and the benchmark
EAFE index. Our screening process excludes many large energy companies, mining
firms (commodities) and emerging companies in countries like China and Russia,
where it is difficult to obtain data on corporate behavior related to
environmental and human rights issues. Our research over the years reveals that
we will encounter periods of time (some lasting several years) where our
commitment to stewardship investing principles will result in lagging investment
performance. However, over a longer period of time, we believe that the
application of social screens will not inevitably lead to poor performance. We
appreciate your patience as we wait for improved relative performance. Please
read the commentary from the portfolio managers of the International Fund for an
in-depth analysis of the Fund's performance and an understanding of the current
investment strategy.

Closing thoughts

The year saw several major developments in MMA's stewardship investing
activities. Please be sure to read Mark Regier's report for a thorough
discussion of these important initiatives.

We remain grateful for the trust and confidence you have demonstrated in MMA
Praxis Mutual Funds. We take our stewardship responsibilities seriously and will
endeavor to generate both financial and social returns on your investments.

Thank you for allowing us to partner with you in meeting your financial planning
goals.

Sincerely,
