DEAR FELLOW SHAREHOLDERS:
We are pleased to report the following performance information for the LKCM
Funds:
Performance data quoted represents past performance and does not guarantee
future results. The investment return and principal value of an investment
will fluctuate so that an investor's shares, when redeemed, may be worth more
or less that the original cost. Current performance of the fund may be lower
or higher that the performance quoted. Performance data current to the most
recent month end may be obtained by calling Returns shown
reflect voluntary fee waivers in effect. In the absence of such waivers,
performance would be reduced.

The S&P Index consists of stocks chosen for market size, liquidity
and industry group representation. It is a market-value weighted index (stock
price times number of shares outstanding), with each stock's weight in the
Index proportionate to its market value. The is one of the most widely
used benchmarks of U.S. equity performance.

The Russell Index is an unmanaged index consisting of the smallest
of the largest stocks. The average market capitalization was
approximately million.

The Morgan Stanley Capital International Europe, Australasia, Far East Index
("MSCI/EAFE") is an unmanaged index composed of European and Pacific Basin
countries. The MSCI/EAFE Index is the most recognized international index and
is weighted by market capitalization.

The Lehman Brothers Intermediate Government/Credit Bond Index is an unmanaged
market value weighted index measuring both the principal price changes of,
and income provided by, the underlying universe of securities that comprise
the index. Securities included in the index must meet the following criteria:
fixed as opposed to variable rate; remaining maturity of one to ten years;
minimum outstanding par value of million; rated investment grade or
higher by Moody's Investors Service or equivalent; must be dollar denominated
and non-convertible; and must be publicly issued.


The stock market provided investors with a third year of positive returns,
although the gains were modest. The Standard & Poor's Index, the Dow Jones
Industrial Average, and the Nasdaq Composite Index all advanced on a total
return basis. The positive influence of stronger than expected earnings growth,
stable bond prices, a robust global economy, continued strong housing and
consumer trends, improved corporate balance sheets, increased capital spending,
and the rising dollar were offset by concerns over high energy costs, rising
short-term interest rates, the overhang of the war in Iraq, worry over a housing
bubble, and a general hesitation by corporate management toward the reinvestment
of cash flows. As a result, the market's volatility was low and showed little
movement for the year. The energy area was by far the most rewarding and, to the
extent investors did well in it is likely that investments in that sector
of the market played an important role.

We believe the U.S. economy will continue to grow in The rate of gain is
likely to slow however as the impact of a weakening rate of growth in the
housing sector should cause less robust consumer activity than in This
slowdown will likely be offset by greater capital spending in the corporate
sector, where business is good and balance sheets are healthy. Increased capital
spending, in turn, should stimulate the hiring of new employees.

The Federal Reserve is likely to end the move to higher short-term interest
rates during the first half of at a level of or perhaps lower. We
believe the Federal Reserve is unlikely to implement a highly restrictive policy
unless inflation moves higher than most economists are forecasting. Much higher
rates could cause significant weakness in housing as well as the overall
economy, a situation the Federal Reserve would like to avoid.

The global economy for looks positive and should offset the expected
slowdown in U.S. consumer activity. The global economic and liquidity conditions
look healthy and opportunities for U.S. companies to participate in this
environment are good.

We continue to forecast that bond yields will rise across the curve. However,
the structural forces which have been a factor in holding down long-term yields
are likely to persist, given excess global liquidity, low inflation, and, in
some cases, the expectations of a mid-cycle economic slowdown in the U.S.

The value of the dollar, while stronger in is likely to weaken in
However, we would not anticipate significant deterioration unless the U.S.
economy is much weaker than we expect.

Some of the risks to our forecast would be a sharp increase in the price of
oil, significant weakness in the value of the U.S. dollar, a discernible
increase in the rate of inflation or a collapse in real estate values. While
these conditions could occur they are not in our forecast.

We believe the stock market will generate positive returns again in While
the rate of gain in corporate profits is expected to slow to normal historical
levels of to we believe the peaking of short-term interest rates
coupled with low inflation and a strong global economy will produce a rising
stock market. Corporate merger activity should remain strong. Increasing
dividends and share repurchases will be an additional positive for the market.
The valuation of share prices based on a forward P/E and historically low bond
yield creates an attractive environment for investing in the public stock
markets.






The LKCM Equity Fund is managed to provide long-term capital appreciation and is
primarily invested in common stocks of mid to large sized companies. The Fund
focuses its investments in quality companies with above-average profitability
and reasonable valuations. For the Fund returned vs. the S&P
return. As of December the total net assets in the portfolio were
million with of the net assets invested in common stocks and in
cash reserves, net of liabilities. During the year, our performance benefited
from stock selection and an overweight position in the Energy sector while our
Technology investments were a drag to our investments. The Fund remains well
positioned with a broadly diversified portfolio of quality companies.

The LKCM Small Cap Equity Fund is managed to maximize capital appreciation
through investment primarily in the common stocks of smaller companies. The
performance of the Fund's Institutional Class during the year was vs. the
Russell return. The Fund's strategy focuses on investing in shares
of reasonably valued niche companies with above average growth and return
prospects. Stock selection and a focus on valuation remain an important
component of our success. As of December the total net assets of the
portfolio were million with of the net assets invested in common
and preferred stocks and in cash reserves, net of liabilities. We generated
positive returns for our investors in the year as we added value from both our
stock selection as well as from our sector allocation decisions. Our stock
selection in Technology and our decision to overweight the Energy sector was
beneficial to our results. Stock selection in the Industrial sector and our
overweight in the Consumer sectors were slightly negative to our performance.

The LKCM Balanced Fund emphasizes current income and long-term capital
appreciation. In order to attain the desired reward/risk profile, the Fund
invests in a blend of common stocks, convertible securities, government and
corporate bonds and cash. We believe our diversified, total return approach
proved successful during the year as the Fund returned vs. the return
in the S&P and the return in the Lehman Index. As of December
total net assets were million and the asset mix contained common
stocks, corporate bonds, in government bonds, and in cash
reserves, net of liabilities. The equity sector of the Fund contributed the
majority of the Fund's returns for the year. Within the equity sector, our
Energy exposure added significantly to our performance while Technology was a
slight drag. We believe the "total return" philosophy of controlling risk via a
blend of asset classes remains an attractive investment strategy for long-term
investors.

The LKCM Fixed Income Fund's main objective is current income. The Fund's
strategy is to invest in a combination of non-callable bonds for their offensive
characteristics and callable bonds as defensive investments in order to create a
high quality, low volatility, short to intermediate maturity portfolio. Our
primary focus is to identify corporate bonds with strong credit profiles and
attractive yields. The Fund had an average duration at December of
years and an average quality rating by Standard &Poor's Corporation of Single A.
Relative to the comparable benchmark, the portfolio had a higher percentage of
its assets invested in corporate bonds and had a shorter average duration. The
Fund's shorter duration focus on higher credit quality issues was additive to
our results. During the year, the Fund returned vs. for the Lehman
Government Credit Intermediate Index. As of December total net assets
were million and the asset mix was in corporate bonds and first
mortgage bonds, in preferred stocks, in U.S. Government and Agency
Issues, and in cash and cash equivalents, net of liabilities.

At LKCM, our investment objective is to utilize our proprietary research
capabilities in order to achieve superior returns over the market cycle in
accordance with the specific objectives of each of the Funds. Our investment
strategy is focused on our fundamental research effort combined with adequate
diversification and a keen eye on valuation. As always, we focus on attractively
valued, competitively advantaged companies with business models supporting high
and/or rising returns on invested capital, strong and growing cash flows that
can be used to reinvest back into the business, and solid balance sheets. This
investment process is consistent in all of our Fund offerings and should keep us
well positioned for the future. We appreciate the opportunity to exercise our
investment talents on your behalf and the trust you have placed in LKCM through
your investment in these Funds.

/s/ J. Luther King, Jr.

J. Luther King, Jr., CFA

DEAR FELLOW SHAREHOLDERS:
We are pleased to report the following performance information for the LKCM
Aquinas Funds:
TOTAL
RETURN
INCEPTION NAV @ SINCE
FUNDS DATES INCEPT.*

LKCM Aquinas Value Fund
Russell Value

LKCM Aquinas Growth Fund
Russell Growth

LKCM Aquinas Small Cap Fund
Russell

LKCM Aquinas Fixed Income Fund
Lehman Bond

Performance data quoted represents past performance and does not guarantee
future results. The investment return and principal value of an investment
will fluctuate so that an investor's shares, when redeemed, may be worth more
or less that the original cost. Current performance of the fund may be lower
or higher than the performance quoted. Performance data current to the most
recent month end may be obtained by calling Returns shown
reflect voluntary fee waivers in effect. In the absence of such waivers,
performance would be reduced.

* On July the Aquinas Funds merged into the LKCM Aquinas Funds. Due
to the change in adviser and investment technique, performance is being
quoted for the period after the merger.

The Russell Value Index is an unmanaged index which measures the
performance of those Russell companies with lower price-to-book ratios
and lower forecasted growth values.

The Russell Growth Index is an unmanaged index which measures the
performance of those Russell companies with higher price-to-book ratios
and higher forecasted growth values.

The Russell Index is an unmanaged index which measures the performance
of the smallest companies in the Russell Index. The average market
capitalization was approximately million.

The Lehman Brothers Intermediate Government/Credit Bond Index is an unmanaged
market value weighted index measuring both the principal price changes of,
and income provided by, the underlying universe of securities that comprise
the index. Securities included in the index must meet the following criteria:
fixed as opposed to variable rate; remaining maturity of one to ten years;
minimum outstanding par value of million; rated investment grade or
higher by Moody's Investors Service or equivalent; must be dollar denominated
and non-convertible; and must be publicly issued.


The stock market provided investors with a third year of positive returns,
although the gains were modest. The Standard & Poor's Index, the Dow Jones
Industrial Average, and the Nasdaq Composite Index all advanced on a total
return basis. The positive influence of stronger than expected earnings growth,
stable bond prices, a robust global economy, continued strong housing and
consumer trends, improved corporate balance sheets, increased capital spending,
and the rising dollar were offset by concerns over high energy costs, rising
short-term interest rates, the overhang of the war in Iraq, worry over a housing
bubble, and a general hesitation by corporate management toward the reinvestment
of cash flows. As a result, the market's volatility was low and showed little
movement for the year. The energy area was by far the most rewarding and, to the
extent investors did well in it is likely that investments in that sector
of the market played an important role.

We believe the U.S. economy will continue to grow in The rate of gain is
likely to slow however as the impact of a weakening rate of growth in the
housing sector should cause less robust consumer activity than in This
slowdown will likely be offset by greater capital spending in the corporate
sector, where business is good and balance sheets are healthy. Increased capital
spending, in turn, should stimulate the hiring of new employees.

The Federal Reserve is likely to end the move to higher short-term interest
rates during the first half of at a level of or perhaps lower. We
believe the Federal Reserve is unlikely to implement a highly restrictive policy
unless inflation moves higher than most economists are forecasting. Much higher
rates could cause significant weakness in housing as well as the overall
economy, a situation the Federal Reserve would like to avoid.

The global economy for looks positive and should offset the expected
slowdown in U.S. consumer activity. The global economic and liquidity conditions
look healthy and opportunities for U.S. companies to participate in this
environment are good.

We continue to forecast that bond yields will rise across the curve. However,
the structural forces which have been a factor in holding down long-term yields
are likely to persist, given excess global liquidity, low inflation, and, in
some cases, the expectations of a mid-cycle economic slowdown in the U.S.

The value of the dollar, while stronger in is likely to weaken in
However, we would not anticipate significant deterioration unless the U.S.
economy is much weaker than we expect.

Some of the risks to our forecast would be a sharp increase in the price of
oil, significant weakness in the value of the U.S. dollar, a discernible
increase in the rate of inflation or a collapse in real estate values. While
these conditions could occur they are not in our forecast.

We believe the stock market will generate positive returns again in While
the rate of gain in corporate profits is expected to slow to normal historical
levels of to we believe the peaking of short-term interest rates
coupled with low inflation and a strong global economy will produce a rising
stock market. Corporate merger activity should remain strong. Increasing
dividends and share repurchases will be an additional positive for the market.
The valuation of share prices based on a forward P/E and historically low bond
yield creates an attractive environment for investing in the public stock
markets.

The LKCM Aquinas Small Cap Fund is managed to maximize capital appreciation
through investment primarily in the common stocks of smaller companies. The
performance of the Fund since inception was vs. the Russell
return. The Fund's strategy focuses on investing in shares of reasonably valued
niche companies with above average growth and return prospects. Stock selection
and a focus on valuation remain an important component of our success. As of
December the total net assets of the portfolio were million with
of the net assets invested in common stocks and





in cash reserves, net of liabilities. We generated positive returns for our
investors since the Fund's inception as we added value from both our stock
selection as well as from our sector allocation decisions. Our stock selection
in Technology and our decision to overweight the Energy sector was beneficial to
our results. Stock selection in the Industrial sector and our overweight in the
Consumer sectors were slightly negative to our performance.

The LKCM Aquinas Growth Fund is managed to provide long-term capital
appreciation by choosing investments that generally have above-average growth in
revenue and/or earnings, above average return on equity, and under-leveraged
balance sheets. The Fund's strategy is to identify quality companies of any
market capitalization size based on various financial and fundamental criteria,
which are constantly updated. The Fund is in the process of transitioning from
the previous manager. As of December the total net assets in the
portfolio were million with of the net assets invested in common
stocks and invested in cash reserves, net of liabilities. Since the Fund's
inception, the Fund advanced versus for the Russell Growth
Index. The portfolio maintained an over-weight position in the Energy Sector and
returns were enhanced by good stock selection in both the Financial and
Technology sectors of the market. The Fund maintains a broadly diversified
portfolio and is well positioned for the market conditions the firm anticipates
in

The LKCM Aquinas Value Fund is managed to provide long-term capital appreciation
through investment in mid to large sized companies. These holdings typically
sell at a discount to their growth rate, have low price to cash flow
characteristics, an attractive dividend yield, or a lower price to book value
than the overall market. As of December the total net assets in the
portfolio were million, with invested in common stocks and
invested in cash reserves, net of liabilities. Since the transition to our
management in mid-July, the Fund advanced versus for the Russell
Value Index. The portfolio benefited from its overweighting in Industrials
and Materials while the underweight versus the Financials hindered our
performance somewhat. The Fund remains well positioned with a broadly
diversified portfolio of quality companies.

The LKCM Aquinas Fixed Income Fund's primary objective is to generate current
income consistent with the preservation of capital and Catholic values. The
Fund's principal strategy is to invest in a combination of non-callable bonds
for their offensive characteristics and callable bonds and floating rate issues
as defensive investments in order to create a high quality, low volatility,
short-to-intermediate duration portfolio. Our main focus is to identify
corporate bonds with strong credit profiles and attractive yields. The Fund had
an average duration of years on December versus years for its
benchmark, the Lehman Intermediate Government/Credit Index, and an average
quality rating of single A+* versus the benchmark quality rating of double A*.
The Fund's higher exposure to credit, specifically the single A* sector as well
as the avoidance of the auto area, were additive to our results, while the
shorter duration and portfolio restructuring out of less liquid mortgage and
asset backed issues detracted from results. The Fund's total return during the
transitional period beginning July through year-end and during the
fourth quarter were and respectively, versus and
respectively, for the Lehman Intermediate Government/Credit Index over the same
periods. As of December the Fund's total net assets were million
and the asset mix was as follows: in corporate bonds, in U. S.
Treasury and government agency issues, in municipal bonds, in mortgage
and asset-backed issues, in preferred stock and in cash equivalents
(net of liabilities).

At LKCM, our investment objective is to utilize our proprietary research
capabilities in order to achieve superior returns over the market cycle in
accordance with the specific objectives of each of the Funds. Our investment
strategy is focused on our fundamental research effort combined with adequate
diversification and a keen eye on valuation. As always, we focus on attractively
valued, competitively advantaged companies with business models supporting high
and/or rising returns on invested capital, strong and growing cash flows that
can be used to reinvest back into the business, and solid balance sheets. This
investment process is consistent in all of our Fund offerings and should keep us
well positioned for the future.

The LKCM Aquinas Funds have a mission to generally follow the investment
guidelines as outlined by the United States Conference of Catholic Bishops and
to achieve its investment objective of superior returns with lower volatility.
By prudently applying traditional moral teachings and employing traditional
principles on cooperation and toleration, as well as the duty to avoid scandal,
we believe that we can carry out our mission and exercise faithful, competent
and socially responsible stewardship of the Fund assets. We appreciate the
opportunity to exercise our investment talents on your behalf and trust you have
placed in LKCM Aquinas through your investment in these Funds.

*Ratings provided by Standard &Poor's Corporation. AAA: highest grade
obligations; possess the ultimate degree of protection as to principal and
interest; AA: also qualify as high grade obligations, and in the majority of
instances differs from AAA issues only in small degree; A: regarded as upper
medium grade; have considerable investment strength but are not entirely free
from adverse effects of changes in economic and trade conditions. Interest and
principal are regarded as safe; BBB: regarded as borderline between definitely
sound obligations and those where the speculative element begins to predominate;
this group is the lowest which qualifies for commercial bank investments.

/s/ J. Luther King, Jr.

J. Luther King, Jr., CFA
