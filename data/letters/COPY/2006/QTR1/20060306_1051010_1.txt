Dear Shareowner,
- --------------------------------------------------------------------------------
As came to a close, U.S. investors looked back on a year of major
challenges, though without much change in the market indices. The war in Iraq
continued, oil prices soared, then dropped, while short-term interest rates
ratcheted steadily higher and intermediate and long-term rates stayed about the
same. Natural disasters also threatened economic expansion. Still, the economy
moved forward as corporate earnings grew. The hope of a growing economy was not
reflected by the small gains or losses in the major U.S. market indices. Among
capitalization ranges, midcap issues made the most headway. Bond prices held
firm and yields remained low, perhaps a sign that the Federal Reserve Board's
interest rate hikes would temper the inflationary pressures induced by a growing
economy.

Among the nagging issues facing the U.S. economy in is the potential impact
of high energy prices on consumer spending and corporate profits. Also unknown
at this time is whether the Federal Reserve Board will continue to raise
interest rates under its new chairman, Ben Bernanke, who stated his top priority
will be to maintain continuity.

Rising interest rates and improving business conditions made U.S. holdings more
attractive to foreign investors, helping to strengthen the dollar versus the
euro and other key currencies. Investors in many foreign markets enjoyed stellar
returns. Double-digit gains were widespread in Europe, Asia and Latin America.
Even the long-dormant Japanese economy began to stir, while emerging markets,
especially those rich in natural resources, fed global economic growth.

The disparity of returns among countries and sectors underscores the importance
for investors to maintain a well-diversified portfolio. We believe this may be a
good time for investors to review their holdings with their advisor and
determine if they reflect the wide range of opportunities that exist across many
asset classes, as last year's results make clear.

Investing for income with Pioneer

Adding one or more of Pioneer's income-oriented funds to your investment program
may help improve your portfolio's overall balance. As a premier provider of
fixed-income investments, Pioneer offers you a broad selection of actively
managed bond funds to help meet a variety of investment needs. Pioneer also
offers income-oriented equity funds, each managed using a value-oriented, total
return investment philosophy that seeks enhanced return potential and lower
volatility through active diversification. Your financial advisor can help you
select among Pioneer's fixed-income choices.


Respectfully,
