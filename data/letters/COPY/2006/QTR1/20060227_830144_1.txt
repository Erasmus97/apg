Dear Atlas Shareholder:
Investment markets held their ground throughout 2005, despite the Federal Reserve's Open Market Committee (FOMC) raising interest rates eight times and consumer confidence remaining low for most of the year. For a significant portion of 2005, natural disastersmost notably Hurricane Katrinaand subsequent hikes in energy costs dominated headlines. Energy stocks were the obvious market leaders, as oil prices surged due to higher global demand and constricted supplies. Despite these pressures, the U.S. economy remained relatively stable through the first three quarters of the year and showed resilience in the fourth quarter. By yearend, the employment rate was up, and gas prices were somewhat lower than they had been before hurricane season.
Overall consumer spending was higher than anticipated in the fourth quarter, accompanied by positive indicators such as continued strength in the real estate market and domestic households' surplus of disposable income. Stocks rebounded well toward the end of the year, due in part to signs that the FOMC will likely ease up in 2006 on interest rate increases. The bond market managed to post a positive return, although the combination of foreign demand for long-term bonds and the FOMC's ongoing interest rate hikes resulted in a flattening yield curve. (A yield curve is considered flat when there is no difference between short- and long-term interest rates.)
Atlas Funds performed well in 2005
Atlas Funds continued to perform well in 2005 and every Atlas portfolio delivered a positive return for the 12-month period. While bond funds continued to provide attractive yields, both stock and bond funds generated solid gains for the year. Following this letter, I encourage you to read detailed market and economic reviews, as well as portfolio managers' in-depth discussions of Atlas Funds' performance.
Atlas launches the Independence Portfolios
On November 30, 2005, Atlas introduced the Atlas Independence Portfolios, three distinct funds of funds:
  Atlas Independence Star Spangled Fund, which invests primarily in Atlas stock funds,
  Atlas Independence Flagship Fund, which invests in a combination of Atlas stock, bond and money funds, and
2
  Atlas Independence Eagle Bond Fund, which invests primarily in Atlas bond funds.
We are pleased to offer these new investment choices and believe the Atlas Independence Portfolios expands your investment options and provides you with worthwhile diversification opportunities.
The continued importance of an asset allocation strategy
The year's natural disasters should remind all investors of the importance of a consistent, well-thought-out, long-term investment strategy. Such a strategy increases your chances of reaching your financial goals regardless of unforeseen world events or an uncertain interest rate climate. Trying to time the market is never a smart idea. Effective asset allocationspreading your investment dollars across stocks, bonds, and cash in a way that is consistent to help you meet your long-term goalsis essential to successful investing. Experienced investors know that broad diversification can reduce the impact that one underperforming asset class can have on an investment portfolio in the short term. Ask your Atlas Representative to review your portfolio with you on a regular basis for appropriate asset allocation as your needs, goals and tolerance for risk evolve.
Thank you for investing in Atlas Funds.
Thank you for entrusting your valuable assets to Atlas. As always, if you have any questions, don't hesitate to call us at 1-800-933-ATLAS (1-800-933-2852). In closing, please accept our best wishes for a prosperous 2006.
Sincerely,


