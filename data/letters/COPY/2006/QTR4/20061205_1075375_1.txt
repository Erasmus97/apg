Dear Shareholders:

The past twelve months has been very exciting for The Catholic Funds, Inc.
We've extended the depth of our Advisory Committee, changed our sub-adviser and
achieved performance that follows our philosophy.

The first and most gratifying change is that we now have The Most Reverend
Timothy M. Dolan, Archbishop of Milwaukee on our Advocacy Advisory Board. His
presence brings reassurances that our investment policies are in fact following
the United States Conference of Catholic Bishops investment guidelines!

On February we replaced our sub-advisor with Ziegler Capital
Management. The strategy that Ziegler has been spearheading is intended to
increase assets and attract more investors. We will continue to exclude
companies that aid in the practice of abortion and then try to obtain a total
return over time from dividends and capital gains that exceeds, before deducting
operating expenses, the total return of the S&P Index.

Below are details on how your investment has put your Catholic values into
action.

DIALOGUE AND RESOLUTIONS

In the advocacy year, we addressed three main issues; Human Rights,
Treatment of Workers and Excessive CEO Compensation. In collaboration with
other faith-based institutions, we attempted dialogue with all companies to
discuss issues pertaining to Catholic values prior to filing resolutions.

First of all, we would like to thank management at Caterpillar for committing to
give us the opportunity to participate in the writing of the company's
sustainability report. Because of this commitment, we withdrew from filing a
resolution.

HUMAN RIGHTS - Chevron has been linked to human rights abuse accusations
including: Pollution Resulting In Health Problems, History of Violence Against
Non-Violent Opposition and Execution, Torture, Rape and Forced Labor. After two
years of dialog with Chevron, The Catholic Equity Fund co-filed with advocacy
partners on a resolution asking that Chevron adopt a comprehensive, transparent,
verifiable human rights policy and report to shareholders on the plan for
implementation by October

Halliburton's Code of Business Conduct does not address major human rights areas
such as freedom of association, forced labor, and equal pay for equal work.
After dialog with Halliburton stalled, The Catholic Equity Fund co-filed with
advocacy partners on a resolution asking that Halliburton review its policies
related to human rights, assess where additional policies should be adopted, and
report its finding by December

TREATMENT OF WORKERS - After dialogue failed, we co-filed resolutions for the
following companies, Apple Computer, Dell Computer, Hewlett-Packard, Motorola
and Wal-Mart.

EXCESSIVE CEO COMPENSATION - After dialogue failed, we filed resolutions with
the following companies, Cendant, Exxon Mobil, Honeywell, Merrill Lynch, AT&T
and Wells Fargo.

As always, you can find more information about Proxy Voting and Resolutions on
our website - www.CatholicFunds.com.

/s/Daniel J. Steininger

Daniel J. Steininger
Chairman and President, The Catholic Funds

The Catholic Church has not sponsored or endorsed The Catholic Equity fund nor
approved or disapproved of the Fund as an investment.

The Catholic Equity Fund is distributed through Catholic Financial Services
Corporation, W. Wells St., Milwaukee, WI Member
NASD and SIPC.

"S&P is a trademark of The McGraw-Hill Companies, Inc., and has been
licensed for use by The Catholic Equity Fund. The Catholic Equity Fund is not
sponsored, endorsed, sold or promoted by Standard and Poor's and Standard and
Poor's makes no representation regarding the advisability of investing in the
Fund. An investment cannot be made directly in the index.

The Catholic Equity Fund

MANAGEMENT:

The Fund is managed by a team of professionals employed by its subadviser,
Ziegler Capital Management, LLC. Those professionals include, among others,
Brian K. Andrew, Donald J. Nesbitt, and Mikhail I. Alkhazov.

Mr. Brian K. Andrew joined Ziegler in after spending seven years working as
an analyst and portfolio manager for bank trust and investment advisory firms.
He was also a managing director and partner in a private investment advisory
firm. Mr. Andrew is currently the president of Ziegler Capital Management and
serves on Ziegler's management committee, as well as Ziegler Wealth Management's
equity and fixed income committees.

Prior to joining Ziegler, he created an investment program to manage indentured
funds, managed municipal funds and monies for organizations in the health care
and senior living industry. He has experience crafting investment policies and
educating organizational management and boards regarding those policies and
their implementation.

Mr. Andrew received his B.S. in Business/Finance from the University of
Minnesota. He has also earned his Chartered Financial Analyst (CFA), and
Certified Cash Manager (CCM) designations. He is a member of the Milwaukee
Investment Analyst Society, the CFA Institute and the Treasury Management
Association. Mr. Andrew currently serves as the President for the Great Lakes
Hemophilia Foundation. He has also been active in the establishment of a
Community Development Authority board for a local Milwaukee municipality where
he most recently served as the Board's Vice Chair. He has also been active in
adult education teaching investment topics through the University of Wisconsin,
Milwaukee, Center for Financial Training and the American Bankers Association.
He has also taught undergraduate courses in finance at Concordia University.

Mr. Donald J. Nesbitt, Chief Investment Officer of Ziegler Capital Management,
joined Ziegler in early after having spent nearly four years at Qwest
Communication's pension plan in Denver, Colorado, where he managed billion of
equities, using research-enhanced, quantitative approaches. Prior to joining
Qwest, Mr. Nesbitt spent nine years at the Illinois Teachers' Retirement System
where, as Director of Investments, he was responsible for the management of
billion across various asset classes.

Mr. Nesbitt holds a B.S. in economics from Saint Cloud State University, St.
Cloud, Minnesota, and a M.S. in financial analysis from the University of
Wisconsin-Milwaukee. He holds a Chartered Financial Analyst (CFA) designation
and has been actively involved with the CFA Institute. Mr. Nesbitt has also
instructed investment courses at the University of Illinois-Springfield and has
spoken at numerous industry conferences on the topics of enhanced equity
management and derivative investment strategies.

Mikhail I. Alkhazov, CFA, is an Assistant Vice President and Portfolio Manager
for Ziegler Capital Management. He joined Ziegler Investment Services Group in
having served as a research analyst at B.C. Ziegler and Company for the
two previous years. He currently serves as a Portfolio Manager and Equity
Analyst, supporting the quantitative investment research process. Mr. Alkhazov
graduated magna cum laude from the University of Wisconsin-Milwaukee with
undergraduate degrees in Accounting and in Finance. He holds a Chartered
Financial Analyst (CFA) designation and additionally holds Series and
licenses.

MARKET COMMENTARY

For the twelve months ended September the Catholic Equity Fund (the
"Fund") produced a total return of (Class A without sales load), compared
to a return of the S&P Composite Stock Price Index ("Index") of The
primary differences between the Fund's performance and the Index's return were
due to expenses and certain securities that were in the Index, but excluded from
the Fund, due to the sanctity of life screen. The management approach for the
Fund was modified from a passive to an enhanced-index approach in February
in an effort to offset performance shortfall from the exclusionary constraints
and the Fund's expenses. Ziegler Capital Management was retained with that
