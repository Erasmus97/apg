Dear Shareholder,
 
Strong earnings, declining oil prices, and robust economic growth have provided a perfect environment for international markets, where stocks are on track to beat the U.S. market for the fifth straight calendar year. During Vanguard International Value Funds fiscal year ended October 31, 2006, the fund soared 31.2%, significantly ahead of its comparative benchmarks. The unmanaged Morgan Stanley Capital International Europe, Australasia, Far East (MSCI EAFE) Index, which tracks equity performance in 21 developed markets, gained 27.5% in U.S. dollars. The performance was pumped up by a 5% decline in the dollar relative to local currencies; the index gained 22.4% without the weaker-dollar effect. 
 
Approximately one-third of the funds gain came in the financial services sector; materials and consumer discretionary stocks also contributed significantly. All three sectors benefited from improved economic growth across the Eurozone and continued supercharged growth within emerging markets. If you own the International Value Fund in a taxable account, please see page 28 for a report on its after-tax returns.
 
International markets served up stronger returns than the U.S. market
International stocks turned in excellent results during the past 12 months. European and emerging markets led the rally. In Europe, a surge in corporate 
 
 
 
 
 
 
 
 
 
 
 
 
2
 
 


mergers and acquisitions drove markets higher in many nations. Emerging economies continued to benefit both from outsourcing by developed countries and from increasingly strong domestic consumer markets. The Japanese market was a somewhat weak performer over the year. 
 
In the United States, stock prices advanced in fits and starts during the 12 months, reflecting the uncertainty that pervaded the market for much of the period. The broad market rallied at the start of the fiscal year, buoyed by strong corporate earnings growth and vigorous economic expansion. In mid-May, inflation fears moved to the fore, and market indexes pulled back sharply.
Optimism regained the upper hand in  late summer, when the broad market rallied to post a 12-month return of 16.6%. As has been the case for much of the past five years, smaller-capitalization stocks outperformed large-caps, and value-oriented stocks bested their growth-oriented counterparts.
 
Rate hikes and inflation concerns drove the U.S. bond market
The U.S. fixed income markets reflected some of these same uncertainties, with the back-and-forth pattern most pronounced among the longest-maturity bonds. The Federal Reserve Board tightened monetary policy by raising its target for the federal funds rate six times during the fiscal year, to 5.25%, and the yields of short-term issues followed closely behind. The yields
 
 
Market Barometer
 
 
 
 
 
Average Annual Total Returns
 
 
Periods Ended October 31, 2006
 
One Year
Three Years
Five Years
Stocks
 
 
 
MSCI All Country World Index ex USA (International)
28.9%
23.0%
16.7%
Russell 1000 Index (Large-caps)
16.0
11.9
7.9
Russell 2000 Index (Small-caps)
20.0
14.5
13.8
Dow Jones Wilshire 5000 Index (Entire market)
16.6
12.4
8.9
 
 
 
 
Bonds
 
 
 
Lehman Aggregate Bond Index (Broad taxable market)
5.2%
3.9%
4.5%
Lehman Municipal Bond Index
5.7
4.8
5.1
Citigroup 3-Month Treasury Bill Index
4.5
2.8
2.3
 
 
 
 
CPI
 
 
 
Consumer Price Index
1.3%
2.9%
2.6%
 
 
 
 
 
 
 
 
 
3
 
 


of longer-term securities, by contrast, dipped early in the year, rose sharply on inflation worries in May, then finished the period a bit above their starting point. The broad taxable bond market returned 5.2%. Corporate bonds generally outperformed government issues. Municipal bonds did better still.
 
Stock selection added value across industry sectors
During the past year, Vanguard International Value Fund benefited from a sizable stake in red-hot emerging markets and the excellent stock selection of its advisors: Hansberger Global Investors, AllianceBernstein L.P., and Lazard Asset Management LLC, the latter of which joined the team in July 2006. Hansberger and Lazard rely on traditional fundamental research, although each  stresses different measures when selecting stocks, while AllianceBernstein combines fundamental research with quantitative tools.
 
The trios blended investment approach generated strong gains across industry sectors and geographic regions. For example, at the end of the fiscal year, the fund was weighted roughly 18% in emerging markets, to which the MSCI EAFE Index has no exposure. Returns for emerging markets topped the already stellar gains of the markets represented by the EAFE index, giving the funds investors an edge. The difference was most noticeable in the energy sector. In the EAFE Index, energy stocks were among the weakest performers, gaining only 13% during the year, but the funds energy holdings soared
 
 
 
 
Your fund compared with its peer group
 
 
 
 
Average
 
 
International
 
Fund
Fund
International Value Fund
0.46%
1.65%
 
 
 
 
 
 
 
 
 
 
1 Fund expense ratio reflects the 12 months ended October 31, 2006. Peer-group expense ratio is derived from data provided by Lipper Inc. and captures information through year-end 2005.
 
 
 
4
 
 


26% on the strength of emerging markets companies Petróleo Brasileiro (Brazil) and OAO Lukoil Holding (Russia). 
 
The funds advisors also chose to maintain a weighting in Japan that was much lower than that of the benchmark (on average, roughly 19% for the fund versus 25% for the index). The Japanese market was the weakest among developed countries as its economy, which caught fire in 2005, appeared to cool. Japanese banks were among the funds weakest performers. However, even in Japans more-challenging environment, your funds advisors spotted winners. Canon and Nintendo were among the funds top performers, benefiting from global demand for digital cameras and video games, respectively.
 
Finally, the funds holdings in the all-important European market turned in outstanding performances, led by banking and insurance companies that have increased lending, investment banking, and other business lines. Top contributors included ING Groep (Netherlands), Credit Suisse Group (Switzerland), and HBOS (United Kingdom). 
 
For more information about each advisors portfolio positioning during the period, see the Advisors Report on page 7.
 
The fund is well ahead of peers over the long term
Over the past ten years, your fund has produced an average annual return that is nearly 2 percentage points ahead of the MSCI EAFE Index and the average return of international funds. The fund benefits both
 
 
Total Returns
 
 
Ten Years Ended October 31, 2006
 
 
 
Average
Final Value of a $10,000
 
Annual Return
Initial Investment
International Value Fund
9.1%
$23,977
MSCI EAFE Index
7.3
20,301
Average International Fund
7.2
20,115
MSCI All Country World Index ex USA
8.2
21,896
 
The figures shown represent past performance, which is not a guarantee of future results. (Current performance may be lower or higher than the performance data cited. For performance data current to the most recent month-end, visit our website at www.vanguard.com.) Note, too, that both investment returns and principal value can fluctuate widely, so an investors shares, when sold, could be worth more or less than their original cost.
 
 
 
 
 
 
 
 
5
 
 


from its advisors records of strong stock selection and from the funds expense ratio, which is less than one-third of the industry average.
 
Translating the funds long-term performance into dollars, a hypothetical investment of $10,000 in the fund on October 31, 1996, would have grown to $23,977 over the ensuing decade. An investor making the same investment in a fund achieving the average return among international funds would have ended up with $3,862 less.


Dear Shareholder,
 
Vanguard Diversified Equity Fund returned 14.7% for the 12 months ended October 31, 2006the funds first complete fiscal year. The return lagged the 16.5% result of the broad U.S. stock market (the funds benchmark), but bested the average result of competing multi-cap core funds.
 
Diversified Equity, a fund of funds, invests in eight underlying Vanguard domestic stock funds. As its name suggests, Diversified Equity encompasses a wide array of investment styles, company sizes, and portfolio management strategies.
 
Stocks produced fitful rallies and familiar patterns
U.S. stock prices advanced in fits and starts during the past 12 months, reflecting the uncertainty that pervaded the market for much of the period. The broad market rallied at the start of the fiscal year, buoyed by strong corporate earnings growth and vigorous economic expansion. In mid-May, as investors responded to increasingly pungent whiffs of inflation, anxiety moved to the fore, and market indexes pulled back sharply. In late summer, optimism regained the upper hand. The broad market staged a powerful rally to post a 12-month return of 16.6%, based on the Dow Jones Wilshire 5000 Composite Index.
 
As has been the case for much of the past five years, smaller-capitalization stocks outperformed large-caps, and value-oriented 
 
 
 
 
 
 
 
 
 
 
2
 
 


stocks bested their growth-oriented counterparts. International stocks were especially strong performers; European and emerging-markets stocks led the way.
 
Rate hikes and inflation concerns drove the bond market
The fixed income markets reflected some of these same uncertainties, with the back-and-forth pattern most pronounced among the longest-maturity bonds. The Federal Reserve Board tightened monetary policy by raising its target for the federal funds rate six times during the fiscal year, to 5.25%, and the yields of short-term issues followed closely behind. The yields of longer-term securities, by contrast, dipped early in the year, rose sharply on inflation worries in May, then finished the period a bit above their starting point. The broad taxable bond market returned 5.2%. Corporate bonds generally outperformed government issues. Municipal bonds did better still.
 
Your fund reflected diverse segments of the U.S. market
The stock market made healthy advances during your funds fiscal yeara period in which investors continued to favor value stocks over growth stocks. Vanguard Diversified Equity Funds 14.7% returnand the results of its component fundsreflected these dynamics.
 
 
 
Market Barometer
 
 
 
 
 
Average Annual Total Returns
 
 
Periods Ended October 31, 2006
 
One Year
Three Years
Five Years
Stocks
 
 
 
Russell 1000 Index (Large-caps)
16.0%
11.9%
7.9%
Russell 2000 Index (Small-caps)
20.0
14.5
13.8
Dow Jones Wilshire 5000 Index (Entire market)
16.6
12.4
8.9
MSCI All Country World Index ex USA (International)
28.9
23.0
16.7
 
 
 
 
Bonds
 
 
 
Lehman Aggregate Bond Index (Broad taxable market)
5.2%
3.9%
4.5%
Lehman Municipal Bond Index
5.7
4.8
5.1
Citigroup 3-Month Treasury Bill Index
4.5
2.8
2.3
 
 
 
 
CPI
 
 
 
Consumer Price Index
1.3%
2.9%
2.6%
 
 
 
 
 
 
 
 
3
 
 


 
Diversified Equitys largest individual holding, Vanguard Growth and Income Fund, blends characteristics of growth and value stocks. It returned 15.9%, nearly keeping pace with the 16.3% return of its benchmark, the Standard & Poors 500 Index.
 
 
Diversified Equitys youth belies decades of fund experience
Since your funds inception in June 2005, it has produced an average annual return of 12.1%, lagging the 13.3% return of the broad stock market and slightly trailing the average return of its peers. If you hold shares of Diversified Equity Fund in a taxable account, see page 17 for a report on the funds after-tax returns. A hypothetical investment of $10,000 made in Diversified Equity at the funds inception would have grown to $11,724 by October 31, 2006.


