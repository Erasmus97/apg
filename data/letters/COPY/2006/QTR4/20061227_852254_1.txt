Dear Investor
Emerging markets stocks and your fund posted strong gains for the year ended October 31, 2006, despite the underwhelming returns generated in the past six months. After a sharp correction in May and June, investors in emerging market countries apparently regained their appetites for risk, sparking a late-period rally. 
