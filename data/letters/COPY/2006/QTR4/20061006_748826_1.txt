Dear Shareholder, 
 
The U.S. economy was mixed during the 12-month reporting period. After expanding 3.3% in the second quarter of 2005, third quarter gross domestic product
(GDP)
The Federal Reserve Board (Fed)
Both short- and long-term yields rose over the reporting period, causing the overall bond market to post only a 
 
 
 
modest gain. During the 12 months ended July 31, 2006, two-year Treasury yields increased from 4.02% to 4.97%. Over the same period, 10-year Treasury
yields moved from 4.28% to 4.99%. Short-term rates rose in concert with the Feds repeated rate hikes, while long-term rates rose on fears of mounting inflationary pressures. Looking at the 12-month period as a whole, the overall bond market,
as measured by the Lehman Brothers U.S. Aggregate Index
The high yield market generated positive returns during the reporting period, supported by strong corporate profits and low default rates. These factors
tended to overshadow several company specific issues, mostly in the automobile industry. During the 12-month period ended July 31, 2006, the Citigroup High Yield Market Index
Despite weakness late in the
reporting period, emerging markets debt produced positive results over the 12-month period, as the JPMorgan Emerging Markets Bond Index Global (EMBI Global)
Please read on for a more detailed look
at prevailing economic and market conditions during the Funds fiscal year and to learn how those conditions have affected Fund performance. 
 
Information About Your Fund 
As you may be aware, several issues
in the mutual fund industry have come under the scrutiny of federal and state regulators. Affiliates of the Funds Manager have, in recent years, received requests for information from various government regulators regarding market timing, late
trading, fees, and other mutual fund issues in connection with various investigations. The regulators appear to be examining, among other things, the Funds response to market timing and shareholder exchange activity, including compliance with
prospectus disclosure related to these subjects. The Fund is not in a position to predict the outcome of these requests and investigations. 
 
 
 
Important information with regard to recent regulatory developments that may affect the Fund is
contained in the Notes to Financial Statements included in this report. 
As always, thank you for your confidence in our stewardship of your
assets. We look forward to helping you meet your financial goals. 
 
Sincerely,



