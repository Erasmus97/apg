Dear Shareholder,

We are pleased to provide the annual report for the Evergreen Small-Mid Growth Fund, covering the period ended September

While the U.S. economy demonstrated remarkable resilience throughout the fiscal year, the equity markets fought through some volatile and uncertain periods before finally finishing on a strong note in the closing months of
the period. During the year, investors took encouragement from two developments: the relaxation of oil and gas prices, which had reached record heights by early July; and the decision of the U.S. Federal Reserve Board (Fed) to pause in
its cycle of rate hikes that had seen short-term interest rates rise from to since June Led by large-company shares, the stock market rallied sharply in the summer months, lifting the Dow Jones Industrial Average to a near all-time
high by the end of the period, although the broader-based Standard &amp; Poors Index remained well below its record level.

The domestic economy frequently showed its strength and flexibility throughout the fiscal year. Demand growth was particularly robust in early with Gross Domestic Product surging by more than in the first quarter
of the period. Growth then decelerated to an annualized rate of approximately in the second quarter of the period, as speculative activity diminished in the nations housing market. Yet healthy levels of growth in personal consumption and business investment enabled demand to grow at more
historically average rates, enabling the overall economy to absorb the comparative weakness in housing from prior year levels. In addition, energy prices fell dramatically in the waning months of the fiscal period, boosting consumer confidence and
brightening the prospects for lower inflation in the months ahead. Indeed, monetary officials at the Fed seemed to agree, as the moderation in housing and energy highlighted their decisions to hold interest rates steady at their most recent policy
meetings.

Equities generally delivered positive returns for the entire twelve month period, albeit with considerable interim volatility. Driven by attractive valuations, large-cap stocks gained the performance edge over mid-cap and
small-cap stocks









LETTER TO SHAREHOLDERS continued

during the investment period, while value continued to outpace growth. Stock performance, however, was very uneven from sector to sector within the domestic market, which generally fell short of the returns generated
internationally.

In this environment, managers of Evergreens domestic equity funds tended to emphasize higher-quality companies with solid balance sheets, healthy cash flows and more consistent profitability. The management team for
the Evergreen Large Company Growth Fund and the Evergreen Omega Fund, for example, continued to position their portfolios based upon bottom-up, fundamental analysis. The Evergreen Mid Cap Growth Fund and the Evergreen Small-Mid Growth Fund sought
companies that were best positioned to grow in a decelerating economic environment. The team supervising the Evergreen Growth Fund maintained their focus on reasonably priced small-cap growth companies with above-average earnings expectations, while
the Evergreen Large Cap Equity Fund employed a diversified core strategy combining both value and growth styles of investing.

While challenges and concerns may periodically surface to weigh on investor sentiment, we believe the fundamental backdrop for long-term investors remains solid. As always, we encourage investors to maintain diversified
investment strategies, including allocations to equity funds, for their long-term portfolios.

Please visit our Web site, EvergreenInvestments.com, for more information about our funds and other investment products available to you. Thank you for your continued support of Evergreen Investments.

Sincerely,

Dear Shareholder,

We are pleased to provide the annual report for the Evergreen Growth Fund, covering the twelve-month period ended September

While the U.S. economy demonstrated remarkable resilience throughout the fiscal year, the equity markets fought through some volatile and uncertain periods before finally finishing on a strong note in the closing months of
the period. During the year, investors took encouragement from two developments: the relaxation of oil and gas prices, which had reached record heights by early July; and the decision of the U.S. Federal Reserve Board (Fed) to pause in
its cycle of rate hikes that had seen short-term interest rates rise from to since June Led by large-company shares, the stock market rallied sharply in the summer months, lifting the Dow Jones Industrial Average to a near all-time
high by the end of the period, although the broader-based Standard &amp; Poors Index remained well below its record level.

The domestic economy frequently showed its strength and flexibility throughout the fiscal year. Demand growth was particularly robust in early with Gross Domestic Product surging by more than in the first quarter
of the period. Growth then decelerated to an annualized rate of approximately in the second quarter of the period, as speculative activity diminished in the nations housing market. Yet healthy levels of growth in personal consumption and business investment enabled demand to grow at more
historically average rates, enabling the overall economy to absorb the comparative weakness in housing from prior year levels. In addition, energy prices fell dramatically in the waning months of the fiscal period, boosting consumer confidence and
brightening the prospects for lower inflation in the months ahead. Indeed, monetary officials at the Fed seemed to agree, as the moderation in housing and energy highlighted their decisions to hold interest rates steady at their most recent policy
meetings.

Equities generally delivered positive returns for the entire twelve month period, albeit with considerable interim volatility. Driven by attractive valuations, large-cap stocks gained the performance edge over mid-cap and
small-cap stocks









LETTER TO SHAREHOLDERS continued

during the investment period, while value continued to outpace growth. Stock performance, however, was very uneven from sector to sector within the domestic market, which generally fell short of the returns generated
internationally.

In this environment, managers of Evergreens domestic equity funds tended to emphasize higher-quality companies with solid balance sheets, healthy cash flows and more consistent profitability. The management team for
the Evergreen Large Company Growth Fund and the Evergreen Omega Fund, for example, continued to position their portfolios based upon bottom-up, fundamental analysis. The Evergreen Mid Cap Growth Fund and the Evergreen Small-Mid Growth Fund sought
companies that were best positioned to grow in a decelerating economic environment. The team supervising the Evergreen Growth Fund maintained their focus on reasonably priced small-cap growth companies with above-average earnings expectations, while
the Evergreen Large Cap Equity Fund employed a diversified core strategy combining both value and growth styles of investing.

While challenges and concerns may periodically surface to weigh on investor sentiment, we believe the fundamental backdrop for long-term investors remains solid. As always, we encourage investors to maintain diversified
investment strategies, including allocations to equity funds, for their long-term portfolios.

Please visit our Web site, EvergreenInvestments.com, for more information about our funds and other investment products available to you. Thank you for your continued support of Evergreen Investments.

Sincerely,

Dear Shareholder,

We are pleased to provide the annual report for the Evergreen Large Cap Equity Fund, covering the twelvemonth period ended September

While the U.S. economy demonstrated remarkable resilience throughout the fiscal year, the equity markets fought through some volatile and uncertain periods before finally finishing on a strong note in the closing months of
the period. During the year, investors took encouragement from two developments: the relaxation of oil and gas prices, which had reached record heights by early July; and the decision of the U.S. Federal Reserve Board (Fed) to pause in
its cycle of rate hikes that had seen short-term interest rates rise from to since June Led by large-company shares, the stock market rallied sharply in the summer months, lifting the Dow Jones Industrial Average to a near all-time
high by the end of the period, although the broader-based Standard &amp; Poors Index remained well below its record level.

The domestic economy frequently showed its strength and flexibility throughout the fiscal year. Demand growth was particularly robust in early with Gross Domestic Product surging by more than in the first quarter
of the period. Growth then decelerated to an annualized rate of approximately in the second quarter of the period, as speculative activity diminished in the nations housing market. Yet healthy levels of growth in personal consumption and business investment
enabled demand to grow at more historically average rates, enabling the overall economy to absorb the comparative weakness in housing from prior year levels. In addition, energy prices fell dramatically in the waning months of the fiscal period,
boosting consumer confidence and brightening the prospects for lower inflation in the months ahead. Indeed, monetary officials at the Fed seemed to agree, as the moderation in housing and energy highlighted their decisions to hold interest rates
steady at their most recent policy meetings.

Equities generally delivered positive returns for the entire twelve month period, albeit with considerable interim volatility. Driven by attractive valuations, large-cap stocks gained the performance edge









LETTER TO SHAREHOLDERS continued

over mid-cap and small-cap stocks during the investment period, while value continued to outpace growth. Stock performance, however, was very uneven from sector to sector within the domestic market, which generally fell
short of the returns generated internationally.

In this environment, managers of Evergreens domestic equity funds tended to emphasize higher-quality companies with solid balance sheets, healthy cash flows and more consistent profitability. The management team for
the Evergreen Large Company Growth Fund and the Evergreen Omega Fund, for example, continued to position their portfolios based upon bottom-up, fundamental analysis. The Evergreen Mid Cap Growth Fund and the Evergreen Small-Mid Growth Fund sought
companies that were best positioned to grow in a decelerating economic environment. The team supervising the Evergreen Growth Fund maintained their focus on reasonably priced small-cap growth companies with above-average earnings expectations, while
the Evergreen Large Cap Equity Fund employed a diversified core strategy combining both value and growth styles of investing.

While challenges and concerns may periodically surface to weigh on investor sentiment, we believe the fundamental backdrop for long-term investors remains solid. As always, we encourage investors to maintain diversified
investment strategies, including allocations to equity funds, for their long-term portfolios.

Please visit our Web site, EvergreenInvestments.com, for more information about our funds and other investment products available to you. Thank you for your continued support of Evergreen Investments.

Sincerely,

Dear Shareholder,

We are pleased to provide the annual report for the Evergreen Large Company Growth Fund, covering the twelve-month period ended September

While the U.S. economy demonstrated remarkable resilience throughout the fiscal year, the equity markets fought through some volatile and uncertain periods before finally finishing on a strong note in the closing months of
the period. During the year, investors took encouragement from two developments: the relaxation of oil and gas prices, which had reached record heights by early July; and the decision of the U.S. Federal Reserve Board (Fed) to pause in
its cycle of rate hikes that had seen short-term interest rates rise from to since June Led by large-company shares, the stock market rallied sharply in the summer months, lifting the Dow Jones Industrial Average to a near all-time
high by the end of the period, although the broader-based Standard &amp; Poors Index remained well below its record level.

The domestic economy frequently showed its strength and flexibility throughout the fiscal year. Demand growth was particularly robust in early with Gross Domestic Product surging by more than in the first quarter
of the period. Growth then decelerated to an annualized rate of approximately in the second quarter of the period, as speculative activity diminished in the nations housing market. Yet healthy levels of growth in personal consumption and business investment
enabled demand to grow at more historically average rates, enabling the overall economy to absorb the comparative weakness in housing from prior year levels. In addition, energy prices fell dramatically in the waning months of the fiscal period,
boosting consumer confidence and brightening the prospects for lower inflation in the months ahead. Indeed, monetary officials at the Fed seemed to agree, as the moderation in housing and energy highlighted their decisions to hold interest rates
steady at their most recent policy meetings.

Equities generally delivered positive returns for the entire twelve month period, albeit with considerable interim volatility. Driven by attractive valuations, large-cap stocks gained the performance edge









LETTER TO SHAREHOLDERS continued

over mid-cap and small-cap stocks during the investment period, while value continued to outpace growth. Stock performance, however, was very uneven from sector to sector within the domestic market, which generally fell
short of the returns generated internationally.

In this environment, managers of Evergreens domestic equity funds tended to emphasize higher-quality companies with solid balance sheets, healthy cash flows and more consistent profitability. The management team for
the Evergreen Large Company Growth Fund and the Evergreen Omega Fund, for example, continued to position their portfolios based upon bottom-up, fundamental analysis. The Evergreen Mid Cap Growth Fund and the Evergreen Small-Mid Growth Fund sought
companies that were best positioned to grow in a decelerating economic environment. The team supervising the Evergreen Growth Fund maintained their focus on reasonably priced small-cap growth companies with above-average earnings expectations, while
the Evergreen Large Cap Equity Fund employed a diversified core strategy combining both value and growth styles of investing.

While challenges and concerns may periodically surface to weigh on investor sentiment, we believe the fundamental backdrop for long-term investors remains solid. As always, we encourage investors to maintain diversified
investment strategies, including allocations to equity funds, for their long-term portfolios.

Please visit our Web site, EvergreenInvestments.com, for more information about our funds and other investment products available to you. Thank you for your continued support of Evergreen Investments.

Sincerely,

Dear Shareholder,

We are pleased to provide the annual report for the Evergreen Mid Cap Growth Fund, covering the twelve-month period ended September

While the U.S. economy demonstrated remarkable resilience throughout the fiscal year, the equity markets fought through some volatile and uncertain periods before finally finishing on a strong note in the closing months of
the period. During the year, investors took encouragement from two developments: the relaxation of oil and gas prices, which had reached record heights by early July; and the decision of the U.S. Federal Reserve Board (Fed) to pause in
its cycle of rate hikes that had seen short-term interest rates rise from to since June Led by large-company shares, the stock market rallied sharply in the summer months, lifting the Dow Jones Industrial Average to a near all-time
high by the end of the period, although the broader-based Standard &amp; Poors Index remained well below its record level.

The domestic economy frequently showed its strength and flexibility throughout the fiscal year. Demand growth was particularly robust in early with Gross Domestic Product surging by more than in the first quarter
of the period. Growth then decelerated to an annualized rate of approximately in the second quarter of the period, as speculative activity diminished in the nations housing market. Yet healthy levels of growth in personal consumption and business investment
enabled demand to grow at more historically average rates, enabling the overall economy to absorb the comparative weakness in housing from prior year levels. In addition, energy prices fell dramatically in the waning months of the fiscal period,
boosting consumer confidence and brightening the prospects for lower inflation in the months ahead. Indeed, monetary officials at the Fed seemed to agree, as the moderation in housing and energy highlighted their decisions to hold interest rates
steady at their most recent policy meetings.

Equities generally delivered positive returns for the entire twelve month period, albeit with considerable interim volatility. Driven by attractive valuations, large-cap stocks gained the performance edge









LETTER TO SHAREHOLDERS continued

over mid-cap and small-cap stocks during the investment period, while value continued to outpace growth. Stock performance, however, was very uneven from sector to sector within the domestic market, which generally fell
short of the returns generated internationally.

In this environment, managers of Evergreens domestic equity funds tended to emphasize higher-quality companies with solid balance sheets, healthy cash flows and more consistent profitability. The management team for
the Evergreen Large Company Growth Fund and the Evergreen Omega Fund, for example, continued to position their portfolios based upon bottom-up, fundamental analysis. The Evergreen Mid Cap Growth Fund and the Evergreen Small-Mid Growth Fund sought
companies that were best positioned to grow in a decelerating economic environment. The team supervising the Evergreen Growth Fund maintained their focus on reasonably priced small-cap growth companies with above-average earnings expectations, while
the Evergreen Large Cap Equity Fund employed a diversified core strategy combining both value and growth styles of investing.

While challenges and concerns may periodically surface to weigh on investor sentiment, we believe the fundamental backdrop for long-term investors remains solid. As always, we encourage investors to maintain diversified
investment strategies, including allocations to equity funds, for their long-term portfolios.

Please visit our Web site, EvergreenInvestments.com, for more information about our funds and other investment products available to you. Thank you for your continued support of Evergreen Investments.

Sincerely,

Dear Shareholder,

We are pleased to provide the annual report for the Evergreen Omega Fund, covering the twelve-month period ended September

While the U.S. economy demonstrated remarkable resilience throughout the fiscal year, the equity markets fought through some volatile and uncertain periods before finally finishing on a strong note in the closing months of
the period. During the year, investors took encouragement from two developments: the relaxation of oil and gas prices, which had reached record heights by early July; and the decision of the U.S. Federal Reserve Board (Fed) to pause in
its cycle of rate hikes that had seen short-term interest rates rise from to since June Led by large-company shares, the stock market rallied sharply in the summer months, lifting the Dow Jones Industrial Average to a near all-time
high by the end of the period, although the broader-based Standard &amp; Poors Index remained well below its record level.

The domestic economy frequently showed its strength and flexibility throughout the fiscal year. Demand growth was particularly robust in early with Gross Domestic Product surging by more than in the first quarter
of the period. Growth then decelerated to an annualized rate of approximately in the second quarter of the period, as speculative activity diminished in the nations housing market. Yet healthy levels of growth in personal consumption and business investment enabled demand to grow at more
historically average rates, enabling the overall economy to absorb the comparative weakness in housing from prior year levels. In addition, energy prices fell dramatically in the waning months of the fiscal period, boosting consumer confidence and
brightening the prospects for lower inflation in the months ahead. Indeed, monetary officials at the Fed seemed to agree, as the moderation in housing and energy highlighted their decisions to hold interest rates steady at their most recent policy
meetings.

Equities generally delivered positive returns for the entire twelve month period, albeit with considerable interim volatility. Driven by attractive valuations, large-cap stocks gained the performance edge over mid-cap and
small-cap stocks









LETTER TO SHAREHOLDERS continued

during the investment period, while value continued to outpace growth. Stock performance, however, was very uneven from sector to sector within the domestic market, which generally fell short of the returns generated
internationally.

In this environment, managers of Evergreens domestic equity funds tended to emphasize higher-quality companies with solid balance sheets, healthy cash flows and more consistent profitability. The management team for
the Evergreen Large Company Growth Fund and the Evergreen Omega Fund, for example, continued to position their portfolios based upon bottom-up, fundamental analysis. The Evergreen Mid Cap Growth Fund and the Evergreen Small-Mid Growth Fund sought
companies that were best positioned to grow in a decelerating economic environment. The team supervising the Evergreen Growth Fund maintained their focus on reasonably priced small-cap growth companies with above-average earnings expectations, while
the Evergreen Large Cap Equity Fund employed a diversified core strategy combining both value and growth styles of investing.

While challenges and concerns may periodically surface to weigh on investor sentiment, we believe the fundamental backdrop for long-term investors remains solid. As always, we encourage investors to maintain diversified
investment strategies, including allocations to equity funds, for their long-term portfolios.

Please visit our Web site, EvergreenInvestments.com, for more information about our funds and other investment products available to you. Thank you for your continued support of Evergreen Investments.

Sincerely,
