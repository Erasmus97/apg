Dear Shareholder,
We are pleased to provide the annual report for the Evergreen Equity Index Fund, covering the twelve-month period ended July 31, 2006.
Domestic capital markets, both equity and fixed income, faced a variety of sometimes inconsistent influences during the past twelve months. Investors attempted to assess the positive effects of solid earnings growth and
evidence that the Federal Reserve Board (Fed) might be at the end of its cycle of credit tightening, versus fears of higher inflation and rising market interest rates.
After experiencing robust growth through 2005, the nation's economy surged by more than 5% in the first quarter of 2006. The expansion then slowed more than had been expected, with growth in Gross Domestic Product (GDP)
decelerating to 2.5% in the second quarter. Corporate earnings for the second quarter, meanwhile, continued on their growth trajectory, rising at a double-digit pace for the twelfth consecutive quarter. The Fed, at its August meeting, left the
influential fed funds rate at 5.25%, marking the first time the nation's central bank has not raised rates since it began tightening in June 2004. However, year-after-year core inflation remained above the range preferred by the Fed, and it remained
uncertain whether the monetary policy was ending its cycle of hikes in short-term rates or simply pausing. In this environment, interest rates of longer-term fixed income securities rose, leading to some erosion in their values. Shorter-duration strategies, especially money market funds, tended to produce superior relative performance.
In assessing the sometimes conflicting pieces of evidence about the state of the economy, Evergreen's Investment Strategy Committee focused on a variety of signals pointing to the resilience of the economic recovery. While
rates of growth in corporate profits, capital expenditures and personal consumption were starting to decelerate, all these economic indicators nevertheless still were rising at what we believed to be more sustainable paces.
In the equity markets, domestic stocks tended to produce moderate performance for the twelve months, but returns of the market indexes masked considerable short-term volatility. Large-cap names finally started to show a
performance edge over small- and mid-cap stocks, while value stocks, including cyclical corporations, continued to outperform growth. Stock performance was uneven from sector to sector, and results were heavily influenced by relatively narrow groups
of stocks. International stocks generated higher returns, albeit with greater volatility.
1
LETTER TO SHAREHOLDERS 
In this environment, the management teams of Evergreen's actively managed equity funds tended to emphasize higher-quality companies with solid balance sheets, healthy cash flows and more consistent profitability. For
example, the managers of the Evergreen Equity Income Fund focused on companies with the ability to maintain and increase their dividend flows to shareholders. The Evergreen Fundamental Large Cap Fund emphasized large-cap companies with histories of
stable earnings growth. The management team of Evergreen Large Cap Value Fund concentrated on higher-quality companies with stable earnings and either minimal or no debt. Portfolio managers of Evergreen Small Cap Value Fund emphasized investments in
smaller companies with superior returns on equity, strong balance sheets and consistent earnings. The Evergreen Special Values Fund favored small, high-quality companies with strong balance sheets. Managers of the Evergreen Disciplined Value Fund
employed a combination of quantitative tools and traditional fundamental equity analysis to assess prospects of large-company stocks, while the team supervising the Evergreen Disciplined Small-Mid Value Fund used a similar strategy in investing in smaller companies. The team managing the Evergreen Equity Index Fund used a highly disciplined process to
control trading and operational costs while reflecting the Standard and Poor's 500 Index.
We continue to encourage investors to maintain diversified investment strategies, including allocations to equity funds, for their long-term portfolios.
EvergreenInvestments.com
Sincerely,


