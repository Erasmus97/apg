Dear Fellow Shareholder:

Beginning in May of this year, investors became increasingly preoccupied with the course of the economy. A more pessimistic outlook pervaded the markets as leading economic indicators began to warn of slower growth. The
resulting correction undercut much of the progress that markets had achieved in the previous three months. However, in August, the Federal Reserve (the Fed) made the decision to leave interest rates unchanged, marking a milestone in its shift to a
tighter monetary policy and contributing to a more favorable market environment as your funds reporting period drew to a close.

Despite investors ongoing concerns about the impact of higher rates, we believe that todays interest-rate levels, far from being a threat to global economic fundamentals, are in fact an integral part of them.
Higher rates in Europe and Japan are shifting the landscape in the fixed-income market and may lead to stronger performance from non-U.S. asset classes in the future. Economic growth may, indeed, be slowing somewhat, but we consider this a typical
development for the middle of an economic cycle, and one that could help provide the basis for a longer and more durable business expansion and a continued healthy investment environment going forward.

Putnam Investments management team, under the leadership of Chief Executive Officer Ed Haldeman, continues to focus on investment performance, and the investment professionals managing your fund have been working to
take advantage of the opportunities presented by this environment.

We would like to take this opportunity to announce the retirement of one of your funds Trustees, John Mullin, an independent Trustee of the Putnam funds since We thank him for his service.

In the following pages, members of your funds management team discuss the funds performance and strategies for the fiscal period ended August and provide their outlook for the months ahead. As always,
we thank you for your support of the Putnam funds.

Respectfully yours,

Dear Fellow Shareholder

Beginning in May of this year, investors became increasingly preoccupied with the course of the economy. A more pessimistic outlook pervaded the markets as leading economic indicators began to warn of slower growth. The resulting
correction undercut much of the progress that markets had achieved in the previous three months. However, in August, the Federal Reserve Board (the Fed) made the decision to leave interest rates unchanged, marking a milestone in its shift to a
tighter monetary policy and contributing to a more favorable market environment as your funds reporting period drew to a close.

Despite investors ongoing concerns about the impact of higher rates, we believe that todays interest-rate levels, far from being a threat to global economic fundamentals, are in fact an integral part of them. Higher
rates in Europe and Japan are shifting the landscape in the fixed-income market and may lead to stronger performance from non-U.S. asset classes in the future. Economic growth may, indeed, be slowing somewhat, but we consider this a typical
development for the middle of an economic cycle, and one that could help provide the basis for a longer and more durable business expansion and a continued healthy investment environment going forward.

Putnam Investments management team, under the leadership of Chief Executive Officer Ed Haldeman, continues to focus on investment performance, and the investment professionals managing your fund have been working to take
advantage of the opportunities presented by this environment.











We would like to take this opportunity to announce the retirement of one of your funds Trustees, John Mullin, an independent Trustee of the Putnam funds since We thank him for his service.

In the following pages, members of your funds management team discuss the funds performance and strategies for the fiscal period ended August and provide their outlook for the months ahead. As always, we
thank you for your support of the Putnam funds.

Respectfully yours,
