Dear Shareholder:

During the months ended September the U.S. economy continued to
grow. Economic growth was strong in first quarter amid moderate core
inflation, some improving economic data and generally positive corporate
earnings reports. During the period's second half, concerns about historically
high energy prices, a cooling housing market and rising interest rates
contributed to slower growth. In light of moderating economic growth, the
Federal Reserve Board left interest rates unchanged at its August and September
meetings. After making gains for most of the period under review, U.S. equity
markets slid in May before rebounding to end the period higher. The Standard &
Poor's Index (S&P returned the Dow Jones Industrial Average
returned and the NASDAQ Composite Index had a return of for the
months under review.

Source: Standard & Poor's Micropal. The S&P consists of stocks
chosen for market size, liquidity and industry group representation. Each
stock's weight in the index is proportionate to its market value. The S&P
is one of the most widely used benchmarks of U.S. equity performance.
The Dow Jones Industrial Average is price weighted based on the average
market price of blue chip stocks of companies that are generally
industry leaders. The NASDAQ Composite Index measures all NASDAQ domestic
and international based common type stocks listed on The NASDAQ Stock
Market. The index is market value weighted and includes more than
companies.

- --------------------------------------------------------------------------------

eDELIVERY DETAILS

Log in at franklintempleton.com and click on eDelivery. Shareholders who are
registered at franklintempleton.com can receive these reports via email. Not all
accounts are eligible for eDelivery.

- -----------------------------------------------------
NOT FDIC INSURED | MAY LOSE VALUE | NO BANK GUARANTEE
- -----------------------------------------------------


Not part of the annual report |


In the enclosed annual report for Franklin Rising Dividends Fund, Don Taylor,
the lead portfolio manager, discusses market conditions and Fund performance
during the period under review. You will also find performance data and
financial information. Please remember that all securities markets fluctuate, as
do mutual fund share prices. As always, we encourage you to consult your
financial advisor who can review your portfolio to design a long-term strategy
and portfolio allocation that meet your individual needs, goals and risk
tolerance.

If you would like more frequent updates, FRANKLINTEMPLETON.COM provides daily
prices, monthly performance figures, portfolio holdings and other information.
You can also access your account, buy and sell shares, and find helpful
financial planning tools. We hope you will take advantage of these online
services.

We thank you for investing with Franklin Templeton, welcome your questions and
comments, and look forward to serving your investment needs in the years ahead.

Sincerely,
