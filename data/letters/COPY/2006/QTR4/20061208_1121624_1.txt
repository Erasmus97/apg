Dear Shareholders:
 
Over the 12 months ended September 30, 2006, the U.S. economy and financial markets have moved solidly ahead while facing shifting pressures from fluctuating energy prices and interest rates, the ongoing war in Iraq, and a
change in Federal Reserve (Fed) leadership and policies.
During the reporting period, we've also begun to see some encouraging shifts in the areas of U.S. stock market leadership as investors move from pursuing more speculative, short-term trends toward higher-quality, more
fundamentally oriented investing. Many of our portfolio managers and Funds have faced significant headwinds over the last several years as certain management styles (e.g., value and small-cap) and market sectors (e.g., energy and industrials) have dominated market
performance. We know you may have concerns about the performance of your Calvert Funds, and we want you to know we share your concerns and would like to address them.
 
A Look at Market Headwinds
Cyclical "headwinds" are a part of any investment process as economic and market trends change, and styles go in and out of favor. As long-term investors, we know that performance leadership of different styles rotates. And at
Calvert, we strive to provide you with a broad array of investment options, managed by experienced money managers, so you can diversify your portfolio to weather market ups and downs. While we remain fully confident in our sub-advisors and investment process that
combines rigorous financial analysis with analysis of a company's corporate responsibility practices, we are nonetheless challenged by certain sector and cyclical issues in the marketplace.
Specifically, many of Calvert's portfolio managers employ disciplines that screen for stocks that exhibit strong fundamentals--such as steady earnings growth, high returns on equity or low debt--and that are fairly priced. For
many of our Funds, this fundamental financial analysis, coupled with our social screening process, leads to a bias toward more  growth-oriented companies, many of which are in areas like healthcare,  information technology, and the consumer discretionary
sectors--areas that until  recently have been underperforming.
With signs of a slowing economy and the recent outperformance of higher-quality companies with solid, long-term fundamentals, we believe a readjustment in the financial markets is underway that should benefit Calvert
investors. In time, the cyclical headwinds should blow more strongly in our direction, favoring the high-quality companies with strong fundamentals and long-term growth potential that our Fund managers favor and portfolios emphasize.
 
Former Calvert Board Member Awarded 2006 Nobel Peace Prize
Recently, Dr. Muhammad Yunus and Grameen Bank were jointly awarded the 2006 Nobel Peace Prize for their pioneering work in microfinance. An original board member of Calvert World Values Fund, Dr. Yunus founded the Grameen Bank
in 1976 to provide poor people with access to small loans that empower them to start or expand their own businesses. This award is especially significant in that it makes explicit and acknowledges the direct link between financial self-sufficiency and peace. Calvert
is proud of our 10-year association with Dr. Yunus and of the role that our firm and shareholders have played in supporting Dr. Yunus's Nobel Prize-winning work in microfinance.
 
Advancing Regulatory Oversight
On the regulatory front, we continue to strengthen compliance operations with regard to codes of ethics, compliance programs, and SEC and NASD disclosure requirements. Our Chief Compliance Officer for Calvert Funds, Karen
Becker, a Calvert veteran of 20 years, has oversight of and administers all Fund policies and procedures which have been designed with the highest level of integrity.
 
30 Acts of Caring
For our 30th anniversary, Calvert staff decided to honor the founding spirit of the company with 30 separate acts of caring. From book and clothing drives to refurbishing homes with Habitat for Humanity-- to working in soup
kitchens, volunteering at elder daycare centers, and raising money to grant the wishes of ill children for the Make A Wish Foundation--Calvert employees are participating fully in this celebration of community service.
 
A Long-Term, Disciplined Outlook
Looking ahead, we believe our disciplined, research-driven investment process will lead to rewarding long-term performance for Calvert investors. We encourage you to work with a financial professional to maintain a strategic
investment plan and diversified portfolio. Your advisor can provide important insights into investment markets and personal financial planning, particularly in challenging markets.
As Calvert celebrates its 30th anniversary year, I'd like to thank you for your continued confidence in our investment products. Calvert continues to strive toward its dual goals of favorable investment results and a positive
impact on corporate responsibility, and we look forward to serving you in the year ahead.
 
Sincerely,


Dear Shareholders:
 
Over the 12 months ended September 30, 2006, the U.S. economy and financial markets have moved solidly ahead while facing shifting pressures from fluctuating energy prices and interest rates, the ongoing war in Iraq, and a
change in Federal Reserve (Fed) leadership and policies.
During the reporting period, we've also begun to see some encouraging shifts in the areas of U.S. stock market leadership as investors move from pursuing more speculative, short-term trends toward higher-quality, more
fundamentally oriented investing. While we remain fully confident in our sub-advisors and investment process that combines rigorous financial analysis with analysis of a company's corporate responsibility practices, we have been nonetheless challenged by certain
sector and cyclical issues in the marketplace.
 
A Look at Market Headwinds
Cyclical "headwinds" are a part of any investment process as economic and market trends change, and styles go in and out of favor. As long-term investors, we know that performance leadership of different styles rotates. And at
Calvert, we strive to provide you with a broad array of investment options, managed by experienced money managers, so you can diversify your portfolio to weather market ups and downs.
Many of Calvert's portfolio managers employ disciplines that screen for stocks that exhibit strong fundamentals--such as steady earnings growth, high returns on equity or low debt--and that are fairly priced. For many of our
Funds, this fundamental financial analysis, coupled with our social screening process, leads to a bias toward more growth-oriented companies, even in our value funds.  Stocks in the healthcare, information technology, and consumer discretionary sectors have been
generally favored --areas that until recently have been underperforming.
With signs of a slowing economy and the recent outperformance of higher-quality companies with solid, long-term fundamentals, we believe a readjustment in the financial markets is underway that should benefit Calvert
investors. In time, we expect the market headwinds to shift to favoring the high-quality companies with strong fundamentals and long-term growth potential that our Fund managers favor and portfolios emphasize.
 
Former Calvert Board Member Awarded 2006 Nobel Peace Prize
Recently, Dr. Muhammad Yunus and Grameen Bank were jointly awarded the 2006 Nobel Peace Prize for their pioneering work in microfinance. An original board member of Calvert World Values Fund, Dr. Yunus founded the Grameen Bank
in 1976 to provide poor people with access to small loans that empower them to start or expand their own businesses. This award is especially significant in that it makes explicit and acknowledges the direct link between financial self-sufficiency and peace. Calvert
is proud of our 10-year association with Dr. Yunus and of the role that our firm and shareholders have played in supporting Dr. Yunus's Nobel Prize-winning work in microfinance.
 
Advancing Regulatory Oversight
On the regulatory front, we continue to strengthen compliance operations with regard to codes of ethics, compliance programs, and SEC and NASD disclosure requirements. Our Chief Compliance Officer for Calvert Funds, Karen
Becker, a Calvert veteran of  20 years, has oversight of and administers all Fund policies and procedures which have been designed with the highest level of integrity.
 
30 Acts of Caring
For our 30th anniversary, Calvert staff decided to honor the founding spirit of the company with 30 separate acts of caring. From book and clothing drives to refurbishing homes with Habitat for Humanity-- to working in soup
kitchens, volunteering at elder daycare centers, and raising money to grant the wishes of ill children for the Make A Wish Foundation--Calvert employees are participating fully in this celebration of community service.
 
A Long-Term, Disciplined Outlook
Looking ahead, we believe our disciplined, research-driven investment process will lead to rewarding long-term performance for Calvert investors. We encourage you to work with a financial professional to maintain a strategic
investment plan and diversified portfolio. Your advisor can provide important insights into investment markets and personal financial planning, particularly in challenging markets.
As Calvert celebrates its 30th anniversary year, I'd like to thank you for your continued confidence in our investment products. Calvert continues to strive toward its dual goals of favorable investment results and a positive
impact on corporate responsibility, and we look forward to serving you in the year ahead.
 
 
Sincerely,


