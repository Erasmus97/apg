Dear fellow shareholder,

As we begin our year of operations, we are pleased to provide you
with our Funds' Annual Report.

In these uncertain financial times, isn't it comforting to know that you
are invested in the Hawaii Municipal Fund and the Hawaii Intermediate Fund? As
shareholders of the Funds, you are earning tax-free income* and supporting
local projects designed to enrich our community. The money raised through
municipal bonds is commonly used to build schools, hospitals, roads, airports,
harbors, and water and electrical systems that serve to create jobs and improve
the quality of life here in our islands.

On the following pages are line graphs comparing each Fund's performance
to the Lehman Muni Bond Index for the years ended September or the
inception of the Fund if less than years. Each graph assumes a hypothetical
investment in the respective Fund. The object of the graphs is to
permit a comparison of the Funds with a benchmark and to provide perspective on
market conditions and investment strategies and techniques that materially
affected the performance of each Fund.

Interest rates are the most important of many factors which can affect
bond prices. Over the course of the fiscal year, the treasury yield curve
flattened with short-term interest rates rising by and long-term interest
rates slightly increasing. This accounts for the Hawaii Municipal Fund's
fiscal year price change of The Hawaii Municipal Fund Investor Class
had a Net Asset Value ("NAV") of on October and a NAV of
on September The Hawaii Municipal Fund Institutional Class had a NAV
of on October and a NAV of on September The
rise in short term-rates resulted in the Hawaii Intermediate Fund's fiscal year
price change of The Hawaii Intermediate Fund had a NAV of of
October and a NAV of on September The primary
investment strategy of the Hawaii Municipal Fund is to purchase high quality
long-term Hawaii municipal bonds. The primary investment strategy of the
Hawaii Intermediate Fund is to purchase high quality three to ten year Hawaii
municipal bonds. The past year's performance for these Funds, which is
presented in this Annual Report, was primarily a result of the implementation
of these strategies. As of September of the Hawaii Municipal
Bond Fund's portfolio was invested in bonds rated AAA by Standard & Poor's
("S&P"). As of September of the Hawaii Intermediate Fund's
portfolio was invested in bonds rated AAA by S&P.

During the fiscal year ended September the Federal Reserve Bank
increased the Federal Funds Rate from to Despite this increase in
short term rates, the ten year treasury bond's yield rose by only In our
opinion, the market's relatively mild reaction suggests that these actions,
among other market events, will keep inflation pressures under control for the
foreseeable future. The modest increase in rates at the long end of the yield
curve can be interpreted as an indication that bond investors believe inflation
will not greatly increase over the long-term. Still, there continues to be
risks to inflation and the bond market, among which are US fiscal policy,
international conflicts/terrorism and global economic factors.


STANDARD & POOR'S
MUNICIPAL BOND RATINGS
September

[The following tables were depicted as pie charts in the printed material.]

Hawaii Municipal Fund Hawaii Intermediate Fund
AAA AAA
AA
AA- BBB+
NR
BBB+
BBB
NR


We are proud to report that as a Hawaii resident, of the dividends
earned in were both state and federal tax-free.* There are no capital
gain distributions for the Hawaii Intermediate Fund for the calendar year.
There will be a long-term capital gain distribution for the calendar year
to shareholders of the Hawaii Municipal Fund.

If you have any questions about this Annual Report or would like us to
provide information about the Funds to your family or friends, please call us
at

Thank you for your business as well as the many referrals. On behalf of
the staff and management of the Funds, I would like to extend to you and your
family best wishes for a safe and happy holiday season.

Warmest Aloha,

/s/ Terrence K.H. Lee

Terrence K.H. Lee
President and CEO First Pacific Securities, Inc./Distributor

A description of the policies and procedures that the Funds use to determine
how to vote proxies relating to portfolio securities is available without
charge, upon request, by calling

Before investing, read the prospectus carefully. Please carefully consider the
Funds' investment objectives, risks, and charges and expenses before investing.
*Some income may be subject to the federal alternative minimum tax for certain
investors. The prospectus contains this and other information about the Funds.
Call for a free prospectus.

Funds' yields, share prices and investment returns fluctuate so that you may
receive more or less than your original investment upon redemption. Past
performance is no guarantee of future results. Hawaii Municipal Fund and
Hawaii Intermediate Fund are series of First Pacific Mutual Fund, Inc.





Hawaii Municipal Fund Investor Class
Investment in Fund Compared to Lehman Muni Bond Index

[The following table was depicted as a line chart in the printed material.]


Hawaii Municipal Fund Lehman Muni
Investor Class Bond Index












Average Annual Total Return
Year
Year
Year

The graph above compares the increase in value of a investment in the
Hawaii Municipal Fund Investor Class with the performance of the Lehman Muni
Bond Index. The objective of the graph is to permit you to compare the
performance of the Fund with the current market and to give perspective to
market conditions and investment strategies and techniques pursued by the
investment manager that materially affected the performance of the Fund. The
Lehman Muni Bond Index reflects reinvestment of dividends but not the expenses
of the Fund. The return and principal value of an investment in the Fund will
fluctuate so that an investor's shares, when redeemed, may be worth more or
less than their original cost. Past performance is not indicative of future
results. The total returns are before taxes on distributions or redemptions of
Fund shares.


















Hawaii Municipal Fund Institutional Class
Investment in Fund Compared to Lehman Muni Bond Index

[The following table was depicted as a line chart in the printed material.]


Hawaii Municipal Fund Lehman Muni
Institutional Class Bond Index






Average Annual Total Return
Year
Since Inception

The graph above compares the increase in value of a investment in the
Hawaii Municipal Fund Institutional Class with the performance of the Lehman
Muni Bond Index. The objective of the graph is to permit you to compare the
performance of the Fund with the current market and to give perspective to
market conditions and investment strategies and techniques pursued by the
investment manager that materially affected the performance of the Fund. The
Lehman Muni Bond Index reflects reinvestment of dividends but not the expenses
of the Fund. The return and principal value of an investment in the Fund will
fluctuate so that an investor's shares, when redeemed, may be worth more or
less than their original cost. Past performance is not indicative of future
results. The total returns are before taxes on distributions or redemptions of
Fund shares.



























Hawaii Intermediate Fund
Investment in Fund Compared to Lehman Muni Bond Index

[The following table was depicted as a line chart in the printed material.]


Hawaii Intermediate Lehman Muni
Fund Bond Index












Average Annual Total Return
Year
Year
Year

The graph above compares the increase in value of a investment in the
Hawaii Intermediate Fund with the performance of the Lehman Muni Bond Index.
The objective of the graph is to permit you to compare the performance of the
Fund with the current market and to give perspective to market conditions and
investment strategies and techniques pursued by the investment manager that
materially affected the performance of the Fund. The Lehman Muni Bond Index
reflects reinvestment of dividends but not the expenses of the Fund. The
return and principal value of an investment in the Fund will fluctuate so that
an investor's shares, when redeemed, may be worth more or less than their
original cost. Past performance is not indicative of future results. The
total returns are before taxes on distributions or redemptions of Fund shares.




















Your Fund's Expenses
