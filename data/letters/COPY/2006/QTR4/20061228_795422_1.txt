Dear Shareholder:

Stock and bond markets around the world have seen largely positive results year to date, although weakness in the technology sector and growth stocks in general
have tempered performance. While financial markets are always unpredictable, there are a number of time-tested principles that can put the historical odds in
your favor.

One of the basic tenets is to invest for the long term. Over time, riding out the markets' inevitable ups and downs has proven much more effective than selling into
panic or chasing the hottest trend. Even missing only a few of the markets' best days can significantly diminish investor returns. Patience also affords the benefits
of compounding - of earning interest on additional income or reinvested dividends and capital gains. There are tax advantages and cost benefits to consider as
well. The more you sell, the more taxes you pay, and the more you trade, the higher the costs. While staying the course doesn't eliminate risk, it can considerably
lessen the effect of short-term declines.

You can further manage your investing risk through diversification. And today, more than ever, geographic diversification should be taken into account. Studies
indicate that asset allocation is the single most important determinant of a portfolio's long-term success. The right mix of stocks, bonds and cash - aligned to your
particular risk tolerance and investment objective - is very important. Age-appropriate rebalancing is also an essential aspect of asset allocation. For younger
investors, an emphasis on equities - which historically have been the best performing asset class over time - is encouraged. As investors near their specific goal,
such as retirement or sending a child to college, consideration may be given to replacing volatile assets (e.g. common stocks) with more-stable fixed investments
(bonds or savings plans).

A third investment principle - investing regularly - can help lower the average cost of your purchases. Investing a certain amount of money each month or
quarter helps ensure you won't pay for all your shares at market highs. This strategy - known as dollar cost averaging - also reduces unconstructive &quot;emotion&quot;
from investing, helping shareholders avoid selling weak performers just prior to an upswing, or chasing a hot performer just before a correction.

We invite you to contact us via the Internet, through our Investor Centers or over the phone. It is our privilege to provide you the information you need to make the
investments that are right for you.

Sincerely,

Dear Shareholder:

Stock and bond markets around the world have seen largely positive results year to date, although weakness in the technology sector and growth stocks in general
have tempered performance. While financial markets are always unpredictable, there are a number of time-tested principles that can put the historical odds in
your favor.

One of the basic tenets is to invest for the long term. Over time, riding out the markets' inevitable ups and downs has proven much more effective than selling into
panic or chasing the hottest trend. Even missing only a few of the markets' best days can significantly diminish investor returns. Patience also affords the benefits
of compounding - of earning interest on additional income or reinvested dividends and capital gains. There are tax advantages and cost benefits to consider as
well. The more you sell, the more taxes you pay, and the more you trade, the higher the costs. While staying the course doesn't eliminate risk, it can considerably
lessen the effect of short-term declines.

You can further manage your investing risk through diversification. And today, more than ever, geographic diversification should be taken into account. Studies
indicate that asset allocation is the single most important determinant of a portfolio's long-term success. The right mix of stocks, bonds and cash - aligned to your
particular risk tolerance and investment objective - is very important. Age-appropriate rebalancing is also an essential aspect of asset allocation. For younger
investors, an emphasis on equities - which historically have been the best performing asset class over time - is encouraged. As investors near their specific goal,
such as retirement or sending a child to college, consideration may be given to replacing volatile assets (e.g. common stocks) with more-stable fixed investments
(bonds or savings plans).

A third investment principle - investing regularly - can help lower the average cost of your purchases. Investing a certain amount of money each month or
quarter helps ensure you won't pay for all your shares at market highs. This strategy - known as dollar cost averaging - also reduces unconstructive &quot;emotion&quot;
from investing, helping shareholders avoid selling weak performers just prior to an upswing, or chasing a hot performer just before a correction.

We invite you to contact us via the Internet, through our Investor Centers or over the phone. It is our privilege to provide you the information you need to make the
investments that are right for you.

Sincerely,

Dear Shareholder:

Stock and bond markets around the world have seen largely positive results year to date, although weakness in the technology sector and growth stocks in general
have tempered performance. While financial markets are always unpredictable, there are a number of time-tested principles that can put the historical odds in
your favor.

One of the basic tenets is to invest for the long term. Over time, riding out the markets' inevitable ups and downs has proven much more effective than selling into
panic or chasing the hottest trend. Even missing only a few of the markets' best days can significantly diminish investor returns. Patience also affords the benefits
of compounding - of earning interest on additional income or reinvested dividends and capital gains. There are tax advantages and cost benefits to consider as
well. The more you sell, the more taxes you pay, and the more you trade, the higher the costs. While staying the course doesn't eliminate risk, it can considerably
lessen the effect of short-term declines.

You can further manage your investing risk through diversification. And today, more than ever, geographic diversification should be taken into account. Studies
indicate that asset allocation is the single most important determinant of a portfolio's long-term success. The right mix of stocks, bonds and cash - aligned to your
particular risk tolerance and investment objective - is very important. Age-appropriate rebalancing is also an essential aspect of asset allocation. For younger
investors, an emphasis on equities - which historically have been the best performing asset class over time - is encouraged. As investors near their specific goal,
such as retirement or sending a child to college, consideration may be given to replacing volatile assets (e.g. common stocks) with more-stable fixed investments
(bonds or savings plans).

A third investment principle - investing regularly - can help lower the average cost of your purchases. Investing a certain amount of money each month or
quarter helps ensure you won't pay for all your shares at market highs. This strategy - known as dollar cost averaging - also reduces unconstructive &quot;emotion&quot;
from investing, helping shareholders avoid selling weak performers just prior to an upswing, or chasing a hot performer just before a correction.

We invite you to contact us via the Internet, through our Investor Centers or over the phone. It is our privilege to provide you the information you need to make the
investments that are right for you.

Sincerely,

Dear Shareholder:

Stock and bond markets around the world have seen largely positive results year to date, although weakness in the technology sector and growth stocks in general
have tempered performance. While financial markets are always unpredictable, there are a number of time-tested principles that can put the historical odds in
your favor.

One of the basic tenets is to invest for the long term. Over time, riding out the markets' inevitable ups and downs has proven much more effective than selling into
panic or chasing the hottest trend. Even missing only a few of the markets' best days can significantly diminish investor returns. Patience also affords the benefits
of compounding - of earning interest on additional income or reinvested dividends and capital gains. There are tax advantages and cost benefits to consider as
well. The more you sell, the more taxes you pay, and the more you trade, the higher the costs. While staying the course doesn't eliminate risk, it can considerably
lessen the effect of short-term declines.

You can further manage your investing risk through diversification. And today, more than ever, geographic diversification should be taken into account. Studies
indicate that asset allocation is the single most important determinant of a portfolio's long-term success. The right mix of stocks, bonds and cash - aligned to your
particular risk tolerance and investment objective - is very important. Age-appropriate rebalancing is also an essential aspect of asset allocation. For younger
investors, an emphasis on equities - which historically have been the best performing asset class over time - is encouraged. As investors near their specific goal,
such as retirement or sending a child to college, consideration may be given to replacing volatile assets (e.g. common stocks) with more-stable fixed investments
(bonds or savings plans).

A third investment principle - investing regularly - can help lower the average cost of your purchases. Investing a certain amount of money each month or
quarter helps ensure you won't pay for all your shares at market highs. This strategy - known as dollar cost averaging - also reduces unconstructive &quot;emotion&quot;
from investing, helping shareholders avoid selling weak performers just prior to an upswing, or chasing a hot performer just before a correction.

We invite you to contact us via the Internet, through our Investor Centers or over the phone. It is our privilege to provide you the information you need to make the
investments that are right for you.

Sincerely,

Dear Shareholder:

Stock and bond markets around the world have seen largely positive results year to date, although weakness in the technology sector and growth stocks in general
have tempered performance. While financial markets are always unpredictable, there are a number of time-tested principles that can put the historical odds in
your favor.

One of the basic tenets is to invest for the long term. Over time, riding out the markets' inevitable ups and downs has proven much more effective than selling into
panic or chasing the hottest trend. Even missing only a few of the markets' best days can significantly diminish investor returns. Patience also affords the benefits
of compounding - of earning interest on additional income or reinvested dividends and capital gains. There are tax advantages and cost benefits to consider as
well. The more you sell, the more taxes you pay, and the more you trade, the higher the costs. While staying the course doesn't eliminate risk, it can considerably
lessen the effect of short-term declines.

You can further manage your investing risk through diversification. And today, more than ever, geographic diversification should be taken into account. Studies
indicate that asset allocation is the single most important determinant of a portfolio's long-term success. The right mix of stocks, bonds and cash - aligned to your
particular risk tolerance and investment objective - is very important. Age-appropriate rebalancing is also an essential aspect of asset allocation. For younger
investors, an emphasis on equities - which historically have been the best performing asset class over time - is encouraged. As investors near their specific goal,
such as retirement or sending a child to college, consideration may be given to replacing volatile assets (e.g. common stocks) with more-stable fixed investments
(bonds or savings plans).

A third investment principle - investing regularly - can help lower the average cost of your purchases. Investing a certain amount of money each month or
quarter helps ensure you won't pay for all your shares at market highs. This strategy - known as dollar cost averaging - also reduces unconstructive &quot;emotion&quot;
from investing, helping shareholders avoid selling weak performers just prior to an upswing, or chasing a hot performer just before a correction.

We invite you to contact us via the Internet, through our Investor Centers or over the phone. It is our privilege to provide you the information you need to make the
investments that are right for you.

Sincerely,

Dear Shareholder:

Stock and bond markets around the world have seen largely positive results year to date, although weakness in the technology sector and growth stocks in general
have tempered performance. While financial markets are always unpredictable, there are a number of time-tested principles that can put the historical odds in
your favor.

One of the basic tenets is to invest for the long term. Over time, riding out the markets' inevitable ups and downs has proven much more effective than selling into
panic or chasing the hottest trend. Even missing only a few of the markets' best days can significantly diminish investor returns. Patience also affords the benefits
of compounding - of earning interest on additional income or reinvested dividends and capital gains. There are tax advantages and cost benefits to consider as
well. The more you sell, the more taxes you pay, and the more you trade, the higher the costs. While staying the course doesn't eliminate risk, it can considerably
lessen the effect of short-term declines.

You can further manage your investing risk through diversification. And today, more than ever, geographic diversification should be taken into account. Studies
indicate that asset allocation is the single most important determinant of a portfolio's long-term success. The right mix of stocks, bonds and cash - aligned to your
particular risk tolerance and investment objective - is very important. Age-appropriate rebalancing is also an essential aspect of asset allocation. For younger
investors, an emphasis on equities - which historically have been the best performing asset class over time - is encouraged. As investors near their specific goal,
such as retirement or sending a child to college, consideration may be given to replacing volatile assets (e.g. common stocks) with more-stable fixed investments
(bonds or savings plans).

A third investment principle - investing regularly - can help lower the average cost of your purchases. Investing a certain amount of money each month or
quarter helps ensure you won't pay for all your shares at market highs. This strategy - known as dollar cost averaging - also reduces unconstructive &quot;emotion&quot;
from investing, helping shareholders avoid selling weak performers just prior to an upswing, or chasing a hot performer just before a correction.

We invite you to contact us via the Internet, through our Investor Centers or over the phone. It is our privilege to provide you the information you need to make the
investments that are right for you.

Sincerely,

Dear Shareholder:

Stock and bond markets around the world have seen largely positive results year to date, although weakness in the technology sector and growth stocks in general
have tempered performance. While financial markets are always unpredictable, there are a number of time-tested principles that can put the historical odds in
your favor.

One of the basic tenets is to invest for the long term. Over time, riding out the markets' inevitable ups and downs has proven much more effective than selling into
panic or chasing the hottest trend. Even missing only a few of the markets' best days can significantly diminish investor returns. Patience also affords the benefits
of compounding - of earning interest on additional income or reinvested dividends and capital gains. There are tax advantages and cost benefits to consider as
well. The more you sell, the more taxes you pay, and the more you trade, the higher the costs. While staying the course doesn't eliminate risk, it can considerably
lessen the effect of short-term declines.

You can further manage your investing risk through diversification. And today, more than ever, geographic diversification should be taken into account. Studies
indicate that asset allocation is the single most important determinant of a portfolio's long-term success. The right mix of stocks, bonds and cash - aligned to your
particular risk tolerance and investment objective - is very important. Age-appropriate rebalancing is also an essential aspect of asset allocation. For younger
investors, an emphasis on equities - which historically have been the best performing asset class over time - is encouraged. As investors near their specific goal,
such as retirement or sending a child to college, consideration may be given to replacing volatile assets (e.g. common stocks) with more-stable fixed investments
(bonds or savings plans).

A third investment principle - investing regularly - can help lower the average cost of your purchases. Investing a certain amount of money each month or
quarter helps ensure you won't pay for all your shares at market highs. This strategy - known as dollar cost averaging - also reduces unconstructive &quot;emotion&quot;
from investing, helping shareholders avoid selling weak performers just prior to an upswing, or chasing a hot performer just before a correction.

We invite you to contact us via the Internet, through our Investor Centers or over the phone. It is our privilege to provide you the information you need to make the
investments that are right for you.

Sincerely,

Dear Shareholder:

Stock and bond markets around the world have seen largely positive results year to date, although weakness in the technology sector and growth stocks in general
have tempered performance. While financial markets are always unpredictable, there are a number of time-tested principles that can put the historical odds in
your favor.

One of the basic tenets is to invest for the long term. Over time, riding out the markets' inevitable ups and downs has proven much more effective than selling into
panic or chasing the hottest trend. Even missing only a few of the markets' best days can significantly diminish investor returns. Patience also affords the benefits
of compounding - of earning interest on additional income or reinvested dividends and capital gains. There are tax advantages and cost benefits to consider as
well. The more you sell, the more taxes you pay, and the more you trade, the higher the costs. While staying the course doesn't eliminate risk, it can considerably
lessen the effect of short-term declines.

You can further manage your investing risk through diversification. And today, more than ever, geographic diversification should be taken into account. Studies
indicate that asset allocation is the single most important determinant of a portfolio's long-term success. The right mix of stocks, bonds and cash - aligned to your
particular risk tolerance and investment objective - is very important. Age-appropriate rebalancing is also an essential aspect of asset allocation. For younger
investors, an emphasis on equities - which historically have been the best performing asset class over time - is encouraged. As investors near their specific goal,
such as retirement or sending a child to college, consideration may be given to replacing volatile assets (e.g. common stocks) with more-stable fixed investments
(bonds or savings plans).

A third investment principle - investing regularly - can help lower the average cost of your purchases. Investing a certain amount of money each month or
quarter helps ensure you won't pay for all your shares at market highs. This strategy - known as dollar cost averaging - also reduces unconstructive &quot;emotion&quot;
from investing, helping shareholders avoid selling weak performers just prior to an upswing, or chasing a hot performer just before a correction.

We invite you to contact us via the Internet, through our Investor Centers or over the phone. It is our privilege to provide you the information you need to make the
investments that are right for you.

Sincerely,

Dear Shareholder:

Stock and bond markets around the world have seen largely positive results year to date, although weakness in the technology sector and growth stocks in general
have tempered performance. While financial markets are always unpredictable, there are a number of time-tested principles that can put the historical odds in
your favor.

One of the basic tenets is to invest for the long term. Over time, riding out the markets' inevitable ups and downs has proven much more effective than selling into
panic or chasing the hottest trend. Even missing only a few of the markets' best days can significantly diminish investor returns. Patience also affords the benefits
of compounding - of earning interest on additional income or reinvested dividends and capital gains. There are tax advantages and cost benefits to consider as
well. The more you sell, the more taxes you pay, and the more you trade, the higher the costs. While staying the course doesn't eliminate risk, it can considerably
lessen the effect of short-term declines.

You can further manage your investing risk through diversification. And today, more than ever, geographic diversification should be taken into account. Studies
indicate that asset allocation is the single most important determinant of a portfolio's long-term success. The right mix of stocks, bonds and cash - aligned to your
particular risk tolerance and investment objective - is very important. Age-appropriate rebalancing is also an essential aspect of asset allocation. For younger
investors, an emphasis on equities - which historically have been the best performing asset class over time - is encouraged. As investors near their specific goal,
such as retirement or sending a child to college, consideration may be given to replacing volatile assets (e.g. common stocks) with more-stable fixed investments
(bonds or savings plans).

A third investment principle - investing regularly - can help lower the average cost of your purchases. Investing a certain amount of money each month or
quarter helps ensure you won't pay for all your shares at market highs. This strategy - known as dollar cost averaging - also reduces unconstructive &quot;emotion&quot;
from investing, helping shareholders avoid selling weak performers just prior to an upswing, or chasing a hot performer just before a correction.

We invite you to contact us via the Internet, through our Investor Centers or over the phone. It is our privilege to provide you the information you need to make the
investments that are right for you.

Sincerely,

Dear Shareholder:

Stock and bond markets around the world have seen largely positive results year to date, although weakness in the technology sector and growth stocks in general
have tempered performance. While financial markets are always unpredictable, there are a number of time-tested principles that can put the historical odds in
your favor.

One of the basic tenets is to invest for the long term. Over time, riding out the markets' inevitable ups and downs has proven much more effective than selling into
panic or chasing the hottest trend. Even missing only a few of the markets' best days can significantly diminish investor returns. Patience also affords the benefits
of compounding - of earning interest on additional income or reinvested dividends and capital gains. There are tax advantages and cost benefits to consider as
well. The more you sell, the more taxes you pay, and the more you trade, the higher the costs. While staying the course doesn't eliminate risk, it can considerably
lessen the effect of short-term declines.

You can further manage your investing risk through diversification. And today, more than ever, geographic diversification should be taken into account. Studies
indicate that asset allocation is the single most important determinant of a portfolio's long-term success. The right mix of stocks, bonds and cash - aligned to your
particular risk tolerance and investment objective - is very important. Age-appropriate rebalancing is also an essential aspect of asset allocation. For younger
investors, an emphasis on equities - which historically have been the best performing asset class over time - is encouraged. As investors near their specific goal,
such as retirement or sending a child to college, consideration may be given to replacing volatile assets (e.g. common stocks) with more-stable fixed investments
(bonds or savings plans).

A third investment principle - investing regularly - can help lower the average cost of your purchases. Investing a certain amount of money each month or
quarter helps ensure you won't pay for all your shares at market highs. This strategy - known as dollar cost averaging - also reduces unconstructive &quot;emotion&quot;
from investing, helping shareholders avoid selling weak performers just prior to an upswing, or chasing a hot performer just before a correction.

We invite you to contact us via the Internet, through our Investor Centers or over the phone. It is our privilege to provide you the information you need to make the
investments that are right for you.

Sincerely,

Dear Shareholder:

Stock and bond markets around the world have seen largely positive results year to date, although weakness in the technology sector and growth stocks in general
have tempered performance. While financial markets are always unpredictable, there are a number of time-tested principles that can put the historical odds in
your favor.

One of the basic tenets is to invest for the long term. Over time, riding out the markets' inevitable ups and downs has proven much more effective than selling into
panic or chasing the hottest trend. Even missing only a few of the markets' best days can significantly diminish investor returns. Patience also affords the benefits
of compounding - of earning interest on additional income or reinvested dividends and capital gains. There are tax advantages and cost benefits to consider as
well. The more you sell, the more taxes you pay, and the more you trade, the higher the costs. While staying the course doesn't eliminate risk, it can considerably
lessen the effect of short-term declines.

You can further manage your investing risk through diversification. And today, more than ever, geographic diversification should be taken into account. Studies
indicate that asset allocation is the single most important determinant of a portfolio's long-term success. The right mix of stocks, bonds and cash - aligned to your
particular risk tolerance and investment objective - is very important. Age-appropriate rebalancing is also an essential aspect of asset allocation. For younger
investors, an emphasis on equities - which historically have been the best performing asset class over time - is encouraged. As investors near their specific goal,
such as retirement or sending a child to college, consideration may be given to replacing volatile assets (e.g. common stocks) with more-stable fixed investments
(bonds or savings plans).

A third investment principle - investing regularly - can help lower the average cost of your purchases. Investing a certain amount of money each month or
quarter helps ensure you won't pay for all your shares at market highs. This strategy - known as dollar cost averaging - also reduces unconstructive &quot;emotion&quot;
from investing, helping shareholders avoid selling weak performers just prior to an upswing, or chasing a hot performer just before a correction.

We invite you to contact us via the Internet, through our Investor Centers or over the phone. It is our privilege to provide you the information you need to make the
investments that are right for you.

Sincerely,
