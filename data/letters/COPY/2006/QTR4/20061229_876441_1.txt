Dear Shareholder:

During the months ended October the global economies continued to
expand at a moderate pace. However, during the latter half of the period, there
were some indications that the momentum was winding down. Equity markets
performed well despite the effects of rising interest rates and investor worries
about higher inflation. In this environment, the Morgan Stanley Capital
International (MSCI) World Index returned and the MSCI Europe,
Australasia, Far East (EAFE) Index returned

In the enclosed annual report for Templeton Foreign Smaller Companies Fund, the
portfolio managers discuss market conditions, investment management decisions
and Fund performance during the period under review. You will also find
performance data and financial information. Please remember that all securities
markets fluctuate, as do mutual fund share prices.

If you would like more frequent updates, FRANKLINTEMPLETON.COM provides daily
prices, monthly performance figures, portfolio holdings and other information.
You can also access your account, buy and sell shares, read timely articles, and
find helpful financial planning tools. We hope you will take advantage of these
online services.

Source: Standard & Poor's Micropal. The MSCI World Index is a free
float-adjusted, market capitalization-weighted index designed to measure
equity market performance in global developed markets. The MSCI EAFE Index
is a free float-adjusted, market capitalization-weighted index designed to
measure equity market performance in global developed markets excluding
the U.S. and Canada.

- --------------------------------------------------------------------------------

EDELIVERY DETAILS

Log in at franklintempleton.com and click on eDelivery. Shareholders who are
registered at franklintempleton.com can receive these reports via email. Not all
accounts are eligible for eDelivery.

-----------------------------------------------------
NOT FDIC INSURED | MAY LOSE VALUE | NO BANK GUARANTEE
-----------------------------------------------------


Not part of the annual report |


As always, we recommend investors consult their financial advisors and review
their portfolios to design a long-term strategy and portfolio allocation that
meet their individual needs, goals and risk tolerance. We firmly believe that
most people benefit from professional advice, and that advice is invaluable as
investors navigate changing market environments.

We thank you for investing with Franklin Templeton, welcome your questions and
comments, and look forward to serving your investment needs in the years ahead.

Sincerely,

Dear Shareholder:

During the months ended October the global economies continued to
expand at a moderate pace. However, during the latter half of the period, there
were some indications that the momentum was winding down. Equity markets
performed well despite the effects of rising interest rates and investor worries
about higher inflation. In this environment, the Morgan Stanley Capital
International (MSCI) World Index returned and the MSCI Europe,
Australasia, Far East (EAFE) Index returned

In the enclosed annual report, the portfolio manager discusses market
conditions, investment management decisions and Fund performance during the
period under review. You will also find performance data and financial
information. Please remember that all securities markets fluctuate, as do mutual
fund share prices.

Source: Standard & Poor's Micropal. The MSCI World Index is a free
float-adjusted, market capitalization-weighted index designed to measure
equity market performance in global developed markets. The MSCI EAFE Index
is a free float-adjusted, market capitalization-weighted index designed to
measure equity market performance in global developed markets excluding
the U.S. and Canada.

- --------------------------------------------------------------------------------

EDELIVERY DETAILS

Log in at franklintempleton.com and click on eDelivery. Shareholders who are
registered at franklintempleton.com can receive these reports via email. Not all
accounts are eligible for eDelivery.

-----------------------------------------------------
NOT FDIC INSURED | MAY LOSE VALUE | NO BANK GUARANTEE
-----------------------------------------------------


Not part of the annual report |


If you would like more frequent updates, FRANKLINTEMPLETON.COM provides daily
prices, monthly performance figures, portfolio holdings and other information.
You can also access your account, buy and sell shares, read timely articles, and
find helpful financial planning tools. We hope you will take advantage of these
online services.

As always, we recommend investors consult their financial advisors and review
their portfolios to design a long-term strategy and portfolio allocation that
meet their individual needs, goals and risk tolerance. We firmly believe that
most people benefit from professional advice, and that advice is invaluable as
investors navigate changing market environments.

We thank you for investing with Franklin Templeton, welcome your questions and
comments, and look forward to serving your investment needs in the years ahead.

Sincerely,
