Dear Shareholder:

We are pleased to present this annual report for Dreyfus Premier Enterprise Fund, covering the period from October through September

After more than two years of steady and gradual increases, the Federal Reserve Board (the "Fed") held short-term interest rates unchanged at its meetings in August and September.The Fed has indicated that the U.S.
economy has moved to a slower-growth phase of its cycle, as evidenced by softening housing markets in many regions of the United States.Yet, energy prices have moderated from record highs, calming fears that the economy may fall into a full-blown
recession.

Most sectors of the U.S. stock market rallied in the wake of the Fed's announcement, while the Dow Jones Industrial Average approached its all-time high, set more than six years ago. Recently, for the first time in
several years, large-cap stocks generally have produced higher returns than their small-cap counterparts, which some analysts believe may signify a fundamental shift in investors attitudes toward risk.As always, we encourage you to talk with
your financial advisor to ensure that your portfolio remains properly diversified across all investment types based on your current financial needs and future investment goals.

For information about how the fund performed during the reporting period, as well as market perspectives, we have provided a Discussion of Fund Performance given by the fund's portfolio manager.

Thank you for your continued confidence and support.

Sincerely,

Dear Shareholder:

We are pleased to present this annual report for Dreyfus Premier Financial Services Fund, covering the period from October through September

After more than two years of steady and gradual increases, the Federal Reserve Board (the "Fed") held short-term interest rates unchanged at its meetings in August and September.The Fed has indicated that the U.S.
economy has moved to a slower-growth phase of its cycle, as evidenced by softening housing markets in many regions of the United States.Yet, energy prices have moderated from record highs, calming fears that the economy may fall into a full-blown
recession.

Most sectors of the U.S. stock market rallied in the wake of the Fed's announcement, while the Dow Jones Industrial Average approached its all-time high, set more than six years ago. Recently, for the first time in
several years, large-cap stocks generally have produced higher returns than their small-cap counterparts, which some analysts believe may signify a fundamental shift in investors attitudes toward risk.As always, we encourage you to talk with
your financial advisor to ensure that your portfolio remains properly diversified across all investment types based on your current financial needs and future investment goals.

For information about how the fund performed during the reporting period, as well as market perspectives, we have provided a Discussion of Fund Performance given by the fund's portfolio manager.

Thank you for your continued confidence and support.

Sincerely,

Dear Shareholder:

We are pleased to present this annual report for Dreyfus Premier Natural Resources Fund, covering the period from October through September

After more than two years of steady and gradual increases, the Federal Reserve Board (the "Fed") held short-term interest rates unchanged at its meetings in August and September.The Fed has indicated that the U.S.
economy has moved to a slower-growth phase of its cycle, as evidenced by softening housing markets in many regions of the United States.Yet, energy prices have moderated from record highs, calming fears that the economy may fall into a full-blown
recession.

Most sectors of the U.S. stock market rallied in the wake of the Fed's announcement, while the Dow Jones Industrial Average approached its all-time high, set more than six years ago. Recently, for the first time in
several years, large-cap stocks generally have produced higher returns than their small-cap counterparts, which some analysts believe may signify a fundamental shift in investors attitudes toward risk.As always, we encourage you to talk with
your financial advisor to ensure that your portfolio remains properly diversified across all investment types based on your current financial needs and future investment goals.

For information about how the fund performed during the reporting period, as well as market perspectives, we have provided a Discussion of Fund Performance given by the fund's portfolio managers.

Thank you for your continued confidence and support.

Sincerely,
