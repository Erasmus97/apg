Dear Shareholder:

In last year's letter to shareholders we spoke of the devastation caused
by Hurricanes Katrina and Rita and the impact on the economy of the
billion estimated cost of rebuilding. We spoke of the ongoing war in Iraq, oil
prices at dollars a barrel, and the fact that the Federal Reserve Board was
not about to let up its restrictive monetary policy. Though we had concerns
about all of these issues as well as inflationary concerns and an over-heated
housing market, we believed that the economy had enough fundamental strength to
sustain reasonable economic growth.

During the twelve-month period ending August the economy
remained fairly healthy, albeit showing signs of fatigue toward the end of the
period. This slowing pace of economic growth combined with relatively moderate
inflationary metrics caused the Federal Reserve Board to hold off on further
increases in the Fed Funds Rate (the rate banks charge one another for
overnight borrowing). After seventeen straight rate increases, the Fed paused
its record rate increases to determine if its restrictive policy had finally
begun to work its way through the economy. Ben Bernanke, Alan Greenspan's
replacement as Chairman of the Federal Reserve Board, has been tasked with the
challenge of using monetary policy to prevent the accelerated and unsustainable
economic growth that often leads to overheated inflationary pressures and
widespread economic concerns. The question now is whether Mr. Bernanke and the
Fed can orchestrate a soft economic landing and avoid recession.

So far this appears to be the case, as inflationary metrics such as
Consumer and Producer Price indexes have registered moderate readings and, most
importantly, wage prices remain under control. Moreover, subsequent to the Fed
holding rates firm, energy prices and commodity prices have softened
considerably, which further allays inflationary concerns and supports the Fed's
most recent decision. One significant worry is the weakening housing market and
the extent to which job losses in the construction industry and lower home
prices will negatively impact the consumer and the economy.

Second quarter Real Gross Domestic Product (GDP) grew at an annual rate
of less than half of the robust growth experienced in the first
quarter of the year. The slowdown is due in part to the slowing housing market
as well as reduced business investment and government spending. Significant
business investment and government spending at the beginning of the year may be
the reason for the recent drop off, so it is difficult to determine if these
sectors are trending lower. However, there appears to be little doubt that the
housing market is softening as increases in home prices have moderated in most
markets and have begun to decrease in several areas. Moreover, inventories of
new homes have increased in each of the last four months creating a supply
overhang that may worsen the outlook for home prices.

Interestingly, the Fed's rate decisions rallied the bond market leaving
the and treasury yields at roughly and respectively.
These rates are not meaningfully higher than a year ago and have sustained a
historically favorable long-term interest rate environment, which is helpful to
the economy in general and more particularly to a slumping housing market. This
rally in longer-term Treasuries has caused a slightly inverted yield curve,
which has traditionally portended a weakening economy. However, the inversion
is very slight and many believe that absent any significant changes in the
employment or inflation picture the economy will experience a soft landing and
avoid recession.

Underlying much of the current fundamental support of the economy is the
fact that jobs growth has been sufficient to maintain an unemployment rate
below which has historically been considered "full" employment. However,
should the housing market continue to weaken, a ripple effect would be felt
throughout the economy that could quickly change the jobs market. Moreover, a
change in the jobs market combined with lower home prices would negatively
impact consumer confidence and spending and would shift expectations from a
soft landing to recession.

CSI Equity Fund





The economy faces a number of continuing challenges, including the
ability to sustain productivity improvements, which moderate wage price
increases, the single biggest contributor to inflationary pressure. In
addition, the growing federal budget and trade deficits create upward pressure
on interest rates. These deficits have largely been financed by foreign
purchasers of U.S. Treasury obligations, a situation on which we have become
overly dependent and which could prove to be problematic if dislocated.

We continue to believe that historically low interest rates combined with
a relatively healthy economy will permit a soft landing and avoid a more severe
economic downturn. As long as corporate America remains healthy enough to
continue capital investment and the consumers remain employed, the economy is
likely to maintain a growth rate in the range of Accordingly, we
remain relatively positive about the prospects for the equity market for the
year ahead.

Portfolio Discussion

For the year ending August the CSI Equity Fund was up
inclusive of all fund expenses. Before expenses, the Fund's portfolio was up
outpacing the return of the S&P and the other domestic
indices. As discussed in years past, our approach to managing the fund is
distinctly conservative, the benefits of which have resulted in meaningful
out-performance of the S&P since the inception of the Fund.

For the year ending August the portfolio performance was helped
by a number of our domestic holdings, especially our financial holdings in
companies such as American International Group, Bank of America, Wells Fargo,
and State Street Bank. In addition, positions in Federal Express, Harley
Davidson, and Cisco Systems contributed nicely to both the absolute return of
the portfolio, as well as the performance relative to the S&P However, the
biggest contributor to portfolio performance was our global diversification in
a group of stellar blue-chip international holdings, including China Mobile,
Diageo, HSBC, National Australia Bank, Nestle, and Toyota Motors.

As is the case in most years, certain of our portfolio companies had
disappointing performance for the year on both and absolute and relative basis.
We remain believers in most of these companies and will continue to hold them
over the long-term. In the case of a company like Dell Computer, whose history
and business model we greatly respect, we chose to sell our shares because of
our doubts in their ability to turn the business around over the next year or
two. Of course, we will watch the progress of Dell and would not be surprised
if we added it back to the portfolio some time in the future.

We remain committed to high-quality, industry-leading companies with
strong balance sheets and experienced management. The portfolio is well
diversified among industries and markets and has excellent prospects for
consistent and profitable growth over the next several years. While we believe
that the portfolio is well positioned to benefit from continued economic
growth, we also have confidence that its high quality nature provides
protection should the economy and the market fail to meet our short-term
expectations.

Leland Faust
Portfolio Manager

Dear Shareholder,

In last year's letter, I wrote about two concepts that I would bring to
the Fund that I felt would help to provide long term value to shareholders.
Those two concepts were diversification and fundamentals. These concepts are
still evident in the Fund today.

With respect to diversification, over the last years the number of
companies that are, or will be deriving a substantial amount of their revenues
from advances in the Genomics industry, has increased dramatically. Beginning
with more technology-like companies, the industry has expanded to include drug
discovery, testing, lab services and products, and marketing. In general, the
Fund currently consists of equal weights of each of those types of companies.
The result is a more diversified portfolio that has eliminated some volatility
while still capturing significant upside.

With respect to fundamentals, some would argue; "fundamentals and
investing in Genomics is an oxymoron". However, an interesting development
occurred over the last years in this industry; years actually occurred!
My point is that companies in the industry have had more time to develop new
targets, new treatments, new products and applications. Treatments, which were
brand new years ago, are now reaching their stride and providing real
revenues (and eventually earnings per share) to the companies participating in
them. And "pipelines" which are the assets of these companies, have had
additional years to be tested, mature and move from trials to the market place.

For the fiscal year ended August the fund returned Over the
same time period, the S & P Index and the NASDAQ Composite Indexes returned
and respectively. Additionally, the AMEX Bio Tech index (BTK)
gained Further, for the five years ending August the fund
returned, on an annualized basis, versus the S & P Index and the
NASDAQ Composite Indexes returns of and respectively and the BTK
index return of Finally, since taking over as the Fund manager (January
the fund has returned on an annualized basis.

As I mentioned in last year's letter, marked the anniversary of
the discovery of the double helix structure of DNA. Watson and Crick (using a
technology called X-ray crystallography) discovered that DNA copies hereditary
material over and over again. Since the time of their discovery, the
Bio-Technology Industry has grown fold in terms of companies to invest in.
In terms of Genomics, a real industry has also developed as well to include not
only classic science but also R & D, testing, marketing and sales services, lab
products and equipment. As the portfolio manager of GENEX, I will continue to
create a portfolio of diversified companies that are benefiting from advances
in the Genomics industry. At the same time, the companies I invest in will have
some form of fundamentals. I am confident that focusing on diversification and
fundamentals will create value and superior returns for shareholders over the
long term.

Sincerely,
