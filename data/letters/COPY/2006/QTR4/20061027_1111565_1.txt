Dear Shareholder:




We are pleased to present this annual report covering the period from September through
August

After more than two years of steady and gradual increases, on August the Federal Reserve Board (the
Fed) decided to hold short-term interest rates unchanged at . In the announcement of its decision, the
Fed indicated that its previous rate hikes and high energy prices have contributed to a mild slowdown in U.S. economic growth. Recent
reports of cooling housing markets in many regions of the United States appeared to confirm this view.

Most sectors of the taxable fixed income market rallied in the wake of the recent Fed announcement.
Fixed-income investors apparently are optimistic that high energy prices and moderating home values may wring the adverse
effects of excessive speculation from the market. In the municipal bond markets, most states and municipalities have continued to
report higher-than-expected tax receipts as a result of the recovering economy, helping to support the credit quality
of many municipal bond issuers.

As always, we encourage you to talk with your portfolio manager about these and other developments to help
ensure that your portfolio remains aligned with your current financial needs and future investment goals.

For information about how each Fund performed during the reporting period, as well as market perspectives,
we have provided a Discussion of Funds Performance.

Thank you for your continued confidence and support.











DISCUSSION OF




FUND PERFORMANCE














John F. Flahive and Mary Collette OBrien,




Portfolio Managers
