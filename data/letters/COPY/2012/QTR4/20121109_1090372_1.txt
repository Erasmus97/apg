Dear Fellow Investors,
In the fiscal year ended August the economic recovery continued in a weak and choppy manner. Under the shadow of mixed economic indicators in the U.S. and lingering sovereign debt issues in Europe,
investors are having difficulty finding confidence. Consequently, despite decent overall performance in the stock market, investors have continued to shun riskier assets. This has adversely affected our more aggressively positioned fundsJacob
Internet Fund and Jacob Small Cap Growth Fund. On the other hand, our more conservatively oriented Jacob Wisdom Fund did well in capturing most of the upside performance of the benchmark S&amp;P Index over the past year, even while staying true
to its mandate. Despite the volatile environment, we continue our steadfast focus on high quality companies that operate in secular growth
markets. Though market sentiment can gyrate wildly, we believe dominant companies in expanding markets have a greater potential to be successful over time.
Looking forward, we believe there are some reasons for optimism, even in the face of an important political election and the potential fiscal cliff facing our nation at the end of the year.
First, after much dallying, European leaders are finally bringing forth detailed plans for addressing their debt crisis. Also, the glimmers of housing market improvement that we saw late last year have strengthened and could provide a tailwind for
economic growth. Finally, an aggressive Fed determined to keep interest rates at historic lows should help accelerate improvement in the unemployment rate. We believe that a combination of these factors will engender more investor confidence in the
coming months. Jacob Internet Fund
Jacob Internet Fund returned during the fiscal year, compared to the Nasdaq Composite Index, which was up Our largest companiesincluding Apple and Googleperformed well, but our
mid-size and smaller technology companies struggled, as did our Chinese holdings. In a year when the Nasdaq was buoyed by its non-tech components, notably consumer and healthcare, the index was an unusually difficult benchmark for our Fund.
The Chinese economy slowed dramatically over the last year, and our Chinese stocks were disproportionately hit. We have trimmed our weighting
in this sector somewhat for the time being, though they remain a significant part of the portfolio. We anticipate seeing China enact more stimulative measures to reaccelerate their economy, which it certainly has the cash resources to do.
Social media IPOs made headlines this year, but we avoided such investments for mainly valuation reasons and looked elsewhere for additions to
our portfolio. We are excited about two stocks that are riding the rapid growth of mobile internet: Glu Mobile and Velti. Glu Mobile is the largest independent publisher of mobile games, with an excellent track record of producing profitable
products with minimal capital investment; it could well be an attractive acquisition candidate. Velti is a pioneer in interactive marketing and advertising solutions in the mobile space, with industry-leading technology and a growing client roster
of Fortune With real estate activity rising, we added shares of Zillow and Ellie Mae. Zillow has handily increased its
competitive lead in online real estate listings and has emerged as a clear market leader. This company has impressed us with its ability to handle the complex transition from desktop to smartphone and tablet devices, and to monetize mobile internet
usage at as high a rate as traditional internet usage, which is no mean feat. Meanwhile, Ellie Mae delivers software-as-a-service solutions to the mortgage industry, bringing






mortgage processing into the cloud. While they have already been very successful in selling to small and mid-size banks and mortgage brokers, some larger institutions such as Wells Fargo and
Citigroup are currently trialing their services. Jacob Small Cap Growth Fund
Jacob Small Cap Growth Fund was down for the fiscal year versus the Russell Growth Index, which was up The Fund is inherently more volatile as it invests in small companies in a broad
swath of sectors, including technology, energy, and healthcare. While healthcare stocks outperformed in this period, other sectors dragged down the Funds return. Energy has been a particularly frustrating area, since stock prices have not
rebounded along with oil prices as of yet, though it is difficult to see this continuing long-term. Our largest position, Amarin, is up about
since August The company continues to secure patents for its fish oil-based drug for high tryglicerides and to test its efficacy when taken in combination with statins. With the drugs recent FDA approval, we anticipate more positive
developments in coming months for this stock. As in Jacob Internet Fund, we added shares of Glu Mobile, Velti, Zillow and Ellie Mae (see
above). Another new holding is Cardtronics, the leading network of private label ATM machines for non-bank sites. This company has
successfully acquired and consolidated smaller networks to reach national scale and is benefitting from the growth of card activity in retail, banking, government benefits and other consumer transactions. They have also been successful in selling
naming rights for their ATM networks to large financial institutions. Weve also recently added Chart Industries, a leading manufacturer
of equipment for converting natural gas to liquid form. With the rising demand for natural gas-based infrastructure, Chart has had difficulty in keeping up with the demand for its products. With the large price discount between natural gas and
diesel fuel, we expect this trend to continue and perhaps even accelerate. We expect to increase the number of small cap names in the Fund
over the next several months to help bolster performance and increase our diversification. Jacob Wisdom Fund
Our conservatively-positioned Jacob Wisdom Fund performed well over the past year, returning versus for the S&amp;P Index. Given its
capital-preservation focus, this Funds aim is to hopefully capture a majority of the performance of the benchmark in a rising market, while investing in stocks that are much less risky.
Our performance was driven in large part by mortgage REIT holdings, including Anworth Mortgage Asset Corp and Annaly Capital Management. These high-yield stocks were propelled by the Federal
Reserves stimulative measures this year, which put the sector in a positive light to investors. We have added significantly to these positions over the course of the year.
The biggest change in the Funds portfolio was the addition of Apple. We have admired this stellar company for a long time. This tech winner is usually classified as a growth company, but it is also
a stock that meets a rigorous set of financial criteria given its consistently impressive return on invested capital and unmatched






cash reserves. When Apple announced a more shareholder friendly capital allocation policy, including share buybacks and a dividend, we began building a significant long-term position.
As always, we continue our focus in seeking to invest in innovative, game-changing or enduring companies that present attractive long-term opportunities.
We thank you for choosing our Funds and look forward to continuing to work with you in the coming year. Ryan Jacob
Chairman and Chief Investment Officer Frank
Alexander Portfolio Manager
Past performance is not a guarantee of future results.
Must be accompanied or preceded by a prospectus. The opinions expressed above are those
of the portfolio manager and are subject to change. Forecasts cannot be guaranteed. Mutual fund investing involves risk; loss of
principal is possible. The Funds invest in foreign securities which involve greater volatility and political, economic and currency risks and differences in accounting methods. Growth stocks typically are more volatile than value stocks; however,
value stocks have a lower expected growth rate in earnings and sales. There are specific risks inherent in investing in the Internet area, particularly with respect to smaller capitalized companies and the high volatility of Internet stocks. The
Funds can invest in small-and mid-cap securities which involve additional risks such as limited liquidity and greater volatility. Investments in debt securities typically decrease in value when interest rates rise. This risk is usually greater for
longer-term debt securities. The value of the Wisdom Funds investments in REITs may change in response to changes in the real estate market such as declines in the value of real estate, lack of available capital or financing opportunities, and
increase in property taxes or operating costs. Fund holdings are subject to change and should not be construed as a recommendation to
buy or sell any security. Current and future portfolio holdings are subject to risk. Please refer to the schedule of investments for complete fund holdings information.
The S&amp;P Index is a broad based unmanaged index of
stocks, which is widely recognized as representative of the equity market in general. The NASDAQ Composite Index is a market capitalization weighted index that is designed to represent the performance of the National Market System which includes
over stocks traded only over-the-counter and not on an exchange. The Russell Growth Index measures the performance of the small-cap growth segment of the U.S. equity universe. It includes those Russell companies with higher
price-to-book ratios and higher forecasted growth values. It is not possible to invest directly in an index. Performance data reflects
fee waivers and in the absence of these waivers performance would be reduced. The Funds are distributed by Quasar Distributors, LLC.






Jacob Internet Fund
INDUSTRY BREAKDOWN AS OF AUGUST
(as a percentage of total investments) (Unaudited)
