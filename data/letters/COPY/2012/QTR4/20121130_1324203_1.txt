Dear Fellow Shareholder, 
Equity markets continued their momentum
from the second quarter of 2012 and rose sharply during the third quarter, ending an impressive trailing one-year period where the S&amp;P 500 Index and the Russell 2000 Index each climbed over 30 percent. The strong move in equities lifted the
entire market, with all ten economic sectors producing positive results for the quarter and over the past year. These results were impressive in light of concerns about the fiscal cliff, the sovereign debt crisis, and unsettling developments in the
Middle East. With just three months left in the calendar year, the S&amp;P 500 index is up over 16 percent. The dominant story during the third quarter proved to be the role of central banks, both in the U.S. and in Europe. In early September, the
European Central Bank (ECB) announced a bond purchase plan designed to help boost the struggling European economies. This was soon followed by the Federal Reserve’s extensive plan to purchase mortgage backed securities. These announcements
provided a boost to housing-related stocks and also could favorably impact job growth. The news sent risk assets higher, and caused a sharp decline in the yields on troubled Spanish debt. Elsewhere, some notable economic reports during September
included a large revision to second quarter Gross Domestic Product (GDP). Although the GDP growth of 1.3 percent was somewhat disappointing, it does not necessarily mean that a new recession is looming. The U.S. economy, however, remains well below
its long-term trend. Meanwhile, estimates for third quarter GDP, which will be released on October 26th, average a tepid 1.8 percent. Despite this data, there were a number of other indicators demonstrating the possibility of a healthier
economy. Housing prices and sales volume, for example, are both up in double-digit percentages from a year ago. Consumer confidence is at a four-month high. On the jobs front, unemployment is still high, but the unemployment rate has declined this
year, as has the number of people filing for new weekly unemployment claims. And, importantly, the stock market has risen steadily and nearly hit a five-year high. All of these developments on the economic front combined with an extremely skeptical
equity investment landscape give us optimism that the strength in equities that we experienced over the past year may be sustained over the long-term. 
Over the past year, we have been pleased with our relative and absolute performance, and even more pleased with the improvements our Funds have made in a difficult
economic environment. Our portfolio companies have effectively navigated some of the headwinds of a slow growth environment and maintained impressive margins through effective cost cutting and other strategic initiatives. Corporate restructuring is
the cornerstone of our investment process and in slow growth environments we expect companies to consider restructuring and other 
 
1 
Table of Contents

forms of financial engineering to manufacture growth. A good example of an industry that has experienced massive restructuring is homebuilders. From a macroeconomic perspective, the long-awaited
housing recovery has finally arrived with improvement in a number of measures, such as increases in prices and permits, a substantial decline in inventory, and momentum in new and existing home sales. Each of these developments have a direct benefit
to the homebuilding industry, where the larger, publicly traded companies have gained significant market share due to the loss of smaller local players during the peak of the downturn. At a company specific level, we have been impressed with
PulteGroup (PHM), Toll Brothers (TOL) and Lennar (LEN). The swift collapse in demand forced each of these companies to undergo significant restructuring. Like many of our holdings in this low interest rate environment, many companies in this
industry have beneficially refinanced a substantial amount of debt and strengthened their capital structures. They have also improved their inventory of homes by liquidating poorly performing holdings, and strengthened the performance of many
existing communities. With a leaner structure, reduced costs, and an increase in demand, we continue to believe the industry has substantial upside. Lastly, after a substantial plunge in 2008 and 2009, new home starts are beginning to climb off
decade lows, which we believe is a significant catalyst for future gains and momentum. However, despite our long-term enthusiasm for the industry, near-term valuations are stretched at current earnings levels, and we responded by reducing exposure
to some positions during the most recent quarter. 
From a portfolio positioning perspective, a number of notable changes have occurred over the past
year. Although we will never waver from our bottom-up, fundamental approach to security selection, the market volatility that has persisted since the downturn in 2008 has caused us to place more emphasis on portfolio construction in recent years.
More specifically, we focused heavily on how such construction can impact the overall volatility in our portfolios. In an over 35 year career of managing a multitude of portfolios and 19 years directing our longest-tenured mutual fund, the Keeley
Small Cap Value Fund, I have always understood that preservation of capital is of the utmost importance. During the challenges of 2008 and 2009, we recognized that some of our industry and sector concentrations were factors in our relative
underperformance. The results that you experienced over the past year are driven from not only good stock selection, which will always be the key driver of our results, but also from good sector allocation and, most importantly, what we believe is a
more balanced portfolio. For example, the Keeley Small Cap Value Fund held over 25 percent of its positions in financials as of the end of the third quarter, which was our highest weight in the sector since 2003. Additionally, the weight in
financials is also higher than our weight in industrials, also for the first time since 2003. This is in stark contrast to the past five years, where industrials commonly represented more than 30 percent of the Fund and our financial weight was
often in the low-to-mid teens, even reaching an all-time low of less than 10 percent of the Fund in the third quarter of 2008. Historically we embraced the financial sector, especially Savings &amp; Loan and Thrift conversions, which offer
heightened transparency, a less complex business plan, and calculated exit strategies. Due to the financial crisis, S&amp;L conversion activity went dormant for a number of years, as the remaining companies in the sector grappled with insufficient
capital, poor loan portfolios, and bleak growth prospects. Although onerous regulatory changes remain a concern, we recognize that vast improvements have 
 
2 
Table of Contents

been made and we believe that many companies are poised to benefit from the restructuring that has taken place throughout the sector. We believe this says a number of things about our outlook
with respect to the sector and the way we are looking at the portfolio in general. First, we are excited to see what we consider to be more attractive opportunities in the financial sector, an area where we have delivered strong outperformance in
the past. Second, we believe that the past year is also indicative of the type of balance you will see in our portfolios going forward. Working closely with Assistant Portfolio Manager Brian Keeley and the other members of our research team, we have
paid deliberate attention to risk and our ability to deliver strong risk-adjusted returns and, most importantly, improve our downside capture ratios. Although we will continue to direct our research process to identify the best opportunities
regardless of economic sector, we believe that paying more attention to correlation and risk versus reward, and how such risks impact the portfolio as a whole, can have a meaningful long-term benefit on our returns. 
With respect to our investment process of corporate restructuring, activity appears robust. The lack of credit in the early 2009 period caused a slow-down, but
spin-off activity has been plentiful in recent years. In our March 31, 2012 Semi-Annual Report, we discussed the strength in the number of spin-offs in 2011, which provided our research team with the largest number of spin-off opportunities
since 2003. Much of that momentum has continued in 2012, with a number of companies making attempts to unlock shareholder value through corporate restructuring. While spin-offs will always be a significant part of our portfolio we often become
enthusiastic over “new” forms of restructuring that often evolve in challenging economic periods. For example, over the past year, we have witnessed a new wave of restructuring where asset-rich companies convert to Real Estate Investment
Trusts (REITs) in a strategic move that can attract income-seeking investors. Our portfolio has benefited from this trend, and it is yet another example of our process of identifying unique opportunities in restructuring. In a slow growth
environment, companies have even greater motivation to seek strategic alternatives that could improve efficiency or enhance public market valuations. A few companies that announced this form of restructuring are Gaylord Entertainment (GET), Iron
Mountain (IRM), Lamar Advertising (LAMR), and Corrections Corporation of America (CXW). Typically, the primary cause for this move is to boost investor interest, especially in an environment where income becomes paramount. In a REIT structure,
companies are required to distribute 90 percent of their taxable income to investors. The move to a REIT structure could also provide tax advantages to the companies as well. We view these types of conversions favorably. We believe the move to a
REIT structure can maintain the growth potential of the company while at the same time attract a new shareholder base that will appreciate the transparency and consistency of regular income distributions. We anticipate more companies considering
this strategic alternative, especially if market volatility and low interest rates persist. 
Despite an investment backdrop that has been surrounded with
fear and anxiety, we have remained committed to our time-tested approach and have been fully-invested in the face of this uncertainty. We have been pleased with our investment results over the past year and we were certainly happy to reward many of
our patient shareholders with strong relative outperformance over that time period. With that said, we recognize that our work is not done, and a number of uncertainties remain. 
 
3 
Table of Contents

The recession in Europe and slower growth in China and the U.S. will continue to be a drag on the long-term economic outlook. The looming “fiscal cliff” will also be a significant
hurdle regardless of who wins the election in November. Although we expect politicians to delay a significant tightening in the short-term, we also recognize that any action may have a slight negative impact on GDP growth. However, a number of
positive developments provide significant optimism from a macroeconomic perspective. We have already discussed our enthusiasm toward housing-related businesses, and the prospect of a continued recovery there should have a positive impact on U.S.
households going forward. Monetary policy continues to be accommodative, and unlike the first rounds of Quantitative Easing (QE), this last round was communicated with unlimited duration. This could continue to provide a boost to risk assets, and
also provide momentum to the recovery in housing. With interest rates expected to remain low through 2015, it is our view that equities will continue to offer an attractive alternative to low-interest rate bonds, which also carry the significant
risk of capital losses in the future when interest rates rise. Regardless of the macroeconomic picture, our focus remains on identifying the best possible investment ideas in our universe of stocks. Our philosophy is based upon the belief that
companies will unlock value through corporate restructuring or other catalysts and that this type of financial engineering will drive earnings despite any potential macroeconomic headwinds. Additionally, our companies have done an impressive job of
maintaining margins and subsequently producing strong earnings in a slow growth environment. Although we have concerns regarding slowing top line growth, we also recognize that even subtle relief to the top line would produce another expansion in
margins. These elements coupled with a still skeptical retail investor have created an environment of fear, which to a large degree has allowed our team to uncover attractive opportunities with compelling valuations. Strength in the equity markets
over the past year surprised many, proving that fear and uncertainty often provide opportunity. Our process is designed to thrive on change and uncertainty, which was a primary driver in our ability to identify mispriced opportunities that
positively impacted our results over the past year. We anticipate much of the uncertainty and hesitation that has been in the market over the last few years to endure, giving us optimism in our ability to capitalize on these mispriced opportunities
in the coming year as well. I appreciate your confidence in our expertise to deliver long-term capital appreciation on your investment, and thank you for your patience and loyalty in an admittedly challenging environment for all investors.

Thank you for your continued commitment to the KEELEY Funds. 
Sincerely, 


Dear Fellow Shareholder, 
While the Funds fiscal year is over, the
end of the calendar year is still ahead of us. The approach of the end of the year seems to bring out questions like “What do you see for next year?” or “How are you positioning the portfolio for next year?” While these are fair
questions, they should not just be relevant at year-end. Money management, unlike most other professions, is not one where we can ever look back on a finished product. It is always a work in process. This is both a source of frustration and a source
of great enjoyment for those of us fortunate to make our living in the business of managing money for others. 
The “work” means that we are
always looking to improve your portfolio. This can mean we buy one stock or sell another, but it also means that we continue to look for opportunities to refine and evolve our process. This year, for example, we put a procedure into place that helps
us identify recent dividend initiators more quickly. Stocks of companies that raise or initiate dividends have historically outperformed the market and even the dividend-paying universe. Unfortunately, it can take time for the initiation to show up
in databases that investors use to screen for ideas. By putting in a process to track these events more rigorously, we hope to gain an edge. While it is too early to tell if our refinement will lead to better performance over time, we are optimistic
that the results will be worth the effort. 
Getting back to the original two questions. Regardless of who wins the election in November, we believe
investors are pricing in an increased possibility that the U.S. will go over the “fiscal cliff.” We do not disagree with this assessment. We believe that it seems contrary to the evidence accumulated over the last two years to think that
the two political parties will be able to come to a compromise that addresses our country’s fiscal problems over the next few months. While we would not be surprised to see some kind of stop-gap measure enacted that could diminish the magnitude
of the “cliff,” the last stop-gap measure (last year’s “Super Committee”) seems to have made the cliff steeper. 
The tone of
recent third quarter earnings reports and our discussions with senior managers of portfolio companies suggests that many companies slowed spending in the third quarter due to a lack of clarity on the economic outlook. Certainly the slowdown in
Europe, which may be now spreading to Northern Europe, and China are contributing factors. Other companies, however, also cite a lack of clarity on the tax, fiscal, and regulatory fronts in the U.S. The results of the election will not immediately
resolve any of this uncertainty. We expect the slow growth trend to continue and, from an economic standpoint, we do not anticipate much improvement in 2013. 
As for our portfolio positioning, we do not really “position” the portfolio. By mandate, we are almost fully invested at all times, and by preference we are relatively sector neutral. We look to grind out
solid, consistent returns by buying companies that are better than their peers at favorable times in their life cycles at attractive valuations. 
 
6 
Table of Contents

For the NCAA football fans out there, we are more Alabama than Oregon (great defense and a decent offense vs. challenging anyone to keep up with you). Nevertheless, we do survey the landscape and
certainly have developed opinions on a variety of topics that impact your portfolio. 
More recently, the concern we hear most often about our strategy is
whether or not dividend-paying stocks are in a bubble. Admittedly, there are some worrisome signs: 
1) Dividend-paying stocks have performed well,
especially during some of the challenging periods since the downturn in 2008. 
2) Interest in dividend-paying mutual funds and ETFs has emerged over the
last five years, whereas it previously was non-existent. 
3) The investment management industry has satisfied this interest by introducing new products.
Furthermore, flows into these strategies have been very strong due to performance and investors’ thirst for income and safety. 
4) We discussed the
impact of past changes in the taxation of dividends in the shareholder letter in the March 31, 2012 Semi-Annual Report. The bottom line is that dividend-paying stocks have significantly lagged the market in the two years (1936 and 1954) when
dividend taxes were boosted and capital gains taxes were not. Both years were strong years in the market, so one would expect a lower beta portfolio to lag. In addition, the structure and ownership of the stock market has changed a great deal since
1954. Nevertheless, the historical precedents give us some pause. 
These data points, combined with the fact that some of the defensive sectors most
associated with dividends (Consumer Staples and Utilities) are viewed to be trading at above average valuations, has led some observers to conclude that dividend stocks are the next bubble waiting to be popped. 
In the words of ESPN College Football GameDay’s Lee Corso, “Not so fast!” 
1) The outperformance of dividend-paying stocks did not begin in the last few years. In fact, it has been more common for dividend-paying stocks to outperform non-dividend-paying stocks than vice-versa. In the
decades since the 1920’s, dividend-paying stocks outperformed in all but the 1940’s and 1990’s. The history shows that they outperform a little more than half the time, but outperform 70%-80% of the time in down markets (a key element
of our decision to focus on dividend-paying stocks). 
2) Some observers have suggested that the dividend-paying sectors are overvalued because they are
trading at price to earnings (P/E) multiples that are closer to the broad indices than they have in the past. The table below presents P/E ratios for the various sectors of the Russell 2000 and Russell 1000 indices as of the end of the third quarter
compared with the end of 2006. As the table shows, overall valuations have come down slightly since then despite the decline in interest rates. Furthermore, they have come down slightly less for dividend-payers than for the broader indices.
Regardless, the relative valuations have not changed very much. We believe that this hardly looks like a bubble, although we admit that December 2006 was not a very good time to buy any stocks. Furthermore, we think that the greater stability
in earnings usually associated with dividend-paying stocks justifies some premium in light of current global macroeconomic conditions. 
 
7 
Table of Contents
Consumer Disc.
Consumer Staples
Energy
Financial Svcs.
Health Care
Materials
Producer Durables
Technology
Utilities
Consumer Disc.
Consumer Staples
Energy
Financial Svcs.
Health Care
Materials
Producer Durables
Technology
Utilities
3) Companies, both large and small, are financially strong and payout ratios are relatively low. For small-cap companies, the
average payout ratio is 35% today vs. 31% at the end of 2006; still pretty low, and the 2006 payout ratio was based on earnings expectations during a strong economy. In addition, net debt for small cap companies is 23% of market cap, little changed
from six years ago despite the intervening downturn. 
When we put it all together, we conclude that dividends may not be the tailwind they have been for
the last several years, but we do not see them as a headwind either. Additionally, if market volatility persists, it is our belief that the stability and financial strength of dividend-paying stocks could outperform on a relative basis as they have
consistently in the past. 
What if we are wrong? An important part of managing money is recognizing and understanding the risks in a portfolio and taking
steps to make sure that an adverse outcome in one of those risks does not negatively impact your client’s investment. 
The most profound
“risk” in the Keeley Dividend Funds is that they are significantly less volatile and have significantly lower betas than the market as a whole. Therefore, in a strong market, they are likely to lag. We attempt to offset this risk through
stock selection and a focus on buying companies that we believe will outperform peers from a return standpoint, have better futures than recent pasts, and are available at attractive valuations. We do not add to portfolio risk by concentrating the
Fund (relative to its benchmark) in sectors that have a higher propensity to pay dividends than others. We would expect that certain sectors, such as utilities, could be more vulnerable to any investor selling of dividend-payers because of the
increase in taxes. 
Finally, if we actually are in a “bubble” in dividend-paying stocks, we think that an unwinding would be dramatically
different from that of the technology sector in the early 2000’s or the recent housing bubble. In the case of those bubbles, investors suffered severe, permanent impairment of capital. Further, those bubbles deflated in a down market.
Historically, dividend-paying stocks generally outperform in down 
 
8 
Table of Contents

markets and underperform in up markets. We believe that this historical pattern would similarly accompany any unwinding of a dividend stock bubble. Such a scenario could result in investor
returns outperforming in a down market, while being positive but underperforming in an up market. This assumes that interest rates do not rise significantly. That, however, is a topic for another time. 
Thank you for your continued commitment to the KEELEY Dividend Value portfolios. 
Sincerely, 


