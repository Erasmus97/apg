Dear Fellow Shareholders: 
I am pleased to present the annual report and investment-performance review of the Eagle Family of Funds for the fiscal year ended October 31, 2012 (the
“reporting period”). 
This has been a year of ups and downs in the broad equity markets. Most major stock indices climbed steadily higher at
the end of 2011 and into the first few months of this year before taking a breather in the spring. They rallied throughout the summer and hit peaks in September. On the fixed-income side, the U.S. Federal Reserve kept its overnight interest rate at
near zero percent, suggested it likely would not raise rates until mid-2015 (it previously said 2014) and began its third round of quantitative-easing (often called QE3). 
Over-arching news for the year included continued economic malaise in Europe (and the ongoing debt issues of its southern neighbors); a slowing growth rate in China; and the U.S. general elections. 
One of the hallmarks of Eagle, over its more than 35 years, has been the fundamental research our managers do in constructing portfolios. Our goal here is to
provide superior risk-adjusted returns for our long-term clients. Producing the desired results means avoiding getting caught up in today’s headlines and focusing on individual companies the Portfolio Managers believe have the characteristics
necessary to make money for our investors. 
I hope you will read the commentaries that follow in which our Portfolio Managers discuss their specific
funds. 
Here are just a few highlights from this year: 
 
Many of Eagle’s funds continue to perform well, including the Eagle Growth &amp; Income Fund that finished the ten-year period ended October 31,
2012 with a five-star rating
 
®
 
®

analyzing companies that are important elements of the value- and dividend-focused benchmarks. He has more than 15 years of investment industry experience as a Portfolio Manager and analyst,
including most recently serving as a Portfolio Manager of ING’s highly regarded Large Cap Value and Equity Dividend Focus funds. 
 
®
 
The Board of Trustees changed the name of the Eagle Small Cap Core Value Fund to the Eagle Smaller Company Fund in an effort to better differentiate the Fund and
its investments from other Eagle Boston Investment Management accounts and other small cap funds in the industry. 
I would like to
remind you that investing in any mutual fund carries certain risks. The principal risk factors for each fund are described at the end of this report. Carefully consider the investment objectives, charges and expenses of any fund before you invest.
Contact us at 800.421.4184 or eagleasset.com or your financial advisor for a prospectus, which contains this and other important information about the Eagle Family of Funds. 
We are grateful for your continued support of, and confidence in, the Eagle Family of Funds. 
Sincerely, 


