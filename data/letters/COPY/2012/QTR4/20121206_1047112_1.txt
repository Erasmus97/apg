DEAR SHAREHOLDER:



Enclosed is your annual report for The Marsico Investment Fund, encompassing the one-year fiscal period from October to September



The purpose of this report is to provide a review of the Marsico Funds one-year investment results by discussing what we believe were the main areas that impacted performance including the macroeconomic environment, sector and industry positioning, and individual stock selection as compared to the Funds performance benchmark indexes. For updated information regarding the market environment and the Funds overall investment postures and performance, please refer to the Funds most recent monthly fact sheets and quarterly investment updates, which are available under the name of each Fund on the Funds website at www.marsicofunds.com.



NOTES REGARDING CHANGES TO THE MARSICO INVESTMENT TEAM



Effective July A. Douglas Rao, former portfolio manager of the Marsico Flexible Capital Fund and co-manager of the Marsico Focus Fund and the Marsico Growth Fund, no longer served in those roles. Munish Malhotra, CFA, and Jordon Laycob now serve as co-managers of the Marsico Flexible Capital Fund. Thomas F. Marsico and Coralie Witter, CFA, continue to serve as co-portfolio managers of the Marsico Focus Fund and the Marsico Growth Fund.




































TABLE OF CONTENTS















KEY FUND STATISTICS








MARKET ENVIRONMENT














MARSICO FOCUS FUND





Investment Review








Fund Overview








Schedule of Investments














MARSICO GROWTH FUND





Investment Review








Fund Overview








Schedule of Investments














MARSICO CENTURY FUND





Investment Review








Fund Overview








Schedule of Investments














MARSICO INTERNATIONAL OPPORTUNITIES FUND





Investment Review








Fund Overview








Schedule of Investments














MARSICO FLEXIBLE CAPITAL FUND





Investment Review








Fund Overview








Schedule of Investments














MARSICO GLOBAL FUND





Investment Review








Fund Overview








Schedule of Investments













FINANCIAL STATEMENTS








NOTES TO FINANCIAL STATEMENTS








REPORT OF INDEPENDENT REGISTERED PUBLIC ACCOUNTING FIRM








EXPENSE EXAMPLE
