Dear Shareholder,
Over the past 12 months, stocks rebounded strongly from depressed levels. A large part of the gain occurred in the first six months as investors grew more optimistic about the economy. Returns were more modest in the second half of the year as concerns resurfaced about the pace of economic growth at home and the debt crisis in Europe.
The market’s rise was broad-based, with both growth and value stocks, including dividend-paying stocks, producing strong returns. For the fiscal year ended September 30, 2012, Vanguard Equity Income Fund returned 29.00% for Investor Shares and 29.06% for Admiral Shares. The fund’s performance was in line with the 28.89% return of its benchmark, the FTSE High Dividend Yield Index, and well ahead of the 25.02% average return of its equity income fund peers.
At the end of the period, the fund’s Investor Shares had a 30-day SEC yield of 2.91%, roughly 1 percentage point higher than the broad U.S. stock market’s yield.
If you own the fund’s shares in a taxable account, you may want to review the information about after-tax returns that appears later in this report.
2
While U.S. stocks were the standouts, European and emerging markets stocks also posted double-digit results. The developed markets of the Pacific region were the weakest performers but still recorded a modest advance.
In July, the president of the European Central Bank declared that policymakers would do whatever was needed to preserve the euro common currency. That pronouncement was encouraging to investors, but Europe’s financial troubles are by no means resolved. Vanguard economists believe the most likely scenario is that the Eurozone will “muddle through” for several years, with occasional spikes in market volatility, as fiscal tightening continues in the face of weak economic growth.
 
3
long-term bonds were particularly strong as they benefited from the Federal Reserve’s bond-buying program.
As bond prices rose, the yield of the 10-year U.S. Treasury note fell to a record low in July, closing below 1.5%. (Bond yields and prices move in opposite directions.) By the end of the period, the yield had climbed, but it still remained low by historical standards.
Bondholders have enjoyed years of strong returns. But as Tim Buckley, our incoming chief investment officer, has noted, investors shouldn’t be surprised if future results are much more modest. As yields tumble, the scope for further declines—and price increases—diminishes.
The Federal Reserve announced on September 13 that it would continue to hold its target for short-term interest rates between 0% and 0.25% at least through mid-2015. The exceptionally low rates, in place since late 2008, have kept a tight lid on returns from money market funds and savings accounts.
The fund expense ratios shown are from the prospectus dated January 27, 2012, and represent estimated costs for the current fiscal year. For the fiscal year ended September 30, 2012, the fund’s expense ratios were 0.30% for Investor Shares and 0.21% for Admiral Shares. The peer-group expense ratio is derived from data provided by Lipper Inc. and captures information through year-end 2011.
Peer group: Equity Income Funds.
4
the fund to return a bit more than its benchmark index during the fiscal year and about 4 percentage points more than its equity income fund peers.
All ten of the fund’s industry sectors produced returns above 17%, with consumer discretionary stocks leading the way. Relative to the index, the fund had an overweight exposure to this sector and specifically to stocks of home improvement retailers, which posted stellar returns as more homeowners took on repair and remodeling projects.
The materials sector was another cyclical group in which the fund outperformed the index. Here, too, stocks related to residential construction and remodeling did well. Chemical companies involved in activities relating to agriculture were another source of strength, as rising food prices and widespread droughts helped drive up demand for drought-resistant and higher-yielding crops.
Utilities, a defensive sector that traditionally generates substantial dividend income, was among the weaker performers as investors became more comfortable taking on riskier assets. While low interest rates helped
The figures shown represent past performance, which is not a guarantee of future results. (Current performance may be lower or higher than the performance data cited. For performance data current to the most recent month-end, visit our website at vanguard.com/performance.) Note, too, that both investment returns and principal value can fluctuate widely, so an investor’s shares, when sold, could be worth more or less than their original cost.
5
keep borrowing costs down in this capital-intensive sector, falling natural gas and power prices put pressure on profit margins.
Dear Shareholder,
Global stock markets began the fiscal year strongly, struggled through much of the spring, and then finished with four straight months of gains. Vanguard Growth Equity Fund returned 26.80% for the year ended September 30, 2012. The fund’s return for fiscal 2012 trailed that of its benchmark, the Russell 1000 Growth Index, by a bit more than 2 percentage points and slightly lagged the average return of large-cap growth funds.
The fund found superior performers among information technology and energy stocks compared to its benchmark, but these results were more than offset by subpar returns from its holdings in consumer discretionary and health care.
If you own shares of the fund in a taxable account, you may wish to review the section on the fund’s after-tax returns that appears later in this report.
2
While U.S. stocks were the standouts, European and emerging markets stocks also posted double-digit results. The developed markets of the Pacific region were the weakest performers but still recorded a modest advance.
In July, the president of the European Central Bank declared that policymakers would do whatever was needed to preserve the euro common currency. That pronouncement was encouraging to investors, but Europe’s financial troubles are by no means resolved.
As bond prices rose, the yield of the 10-year U.S. Treasury note fell to a record low in July, closing below 1.5%. (Bond yields and prices move in opposite directions.) By the end of the period, the yield had climbed, but it still remained low by historical standards.
 
3
Bondholders have enjoyed years of strong returns. But as Tim Buckley, our incoming chief investment officer, has noted, investors shouldn’t be surprised if future results are much more modest. As yields tumble, the scope for further declines—and price increases—diminishes.
The Federal Reserve announced on September 13 that it would continue to hold its target for short-term interest rates between 0% and 0.25% at least through mid-2015. The exceptionally low rates, in place since late 2008, kept a tight lid on returns from money market funds and savings accounts.
The advisors continued to emphasize information technology stocks, which represented by far the largest industry
The fund expense ratio shown is from the prospectus dated January 27, 2012, and represents estimated costs for the current fiscal year. For the fiscal year ended September 30, 2012, the fund’s expense ratio was 0.54%. The peer-group expense ratio is derived from data provided by Lipper Inc. and captures information through year-end 2011.
Peer group: Large-Cap Growth Funds.
4
weighting in the portfolio. The advisors’ IT selections returned 37%, outpacing the sector’s performance in the fund’s benchmark and providing the biggest margin of relative outperformance. The fund’s energy stocks also did well.
The advisors’ positioning in the materials sector gave a leg up to the fund’s results in that sector compared to the benchmark; the fund held only two companies in the sector, and one of them, Monsanto, returned 54%.
Of course, results from active management can go the other way. The advisors’ selections in the consumer discretionary arena returned 23%—but in the index, consumer discretionary returned 30%.
Some of this underperformance was caused by the advisors’ omission of any positions in the cable and satellite TV subsector, which rose nearly 50%.
The fund was also hurt by subpar performance in health care and in financials. But returns for those sectors were 25% and 28%, respectively—far above long-term historical averages. These strong returns helped take some of the sting out of the underperformance.
For more on the strategy and outlook of the fund’s two advisory firms, Baillie Gifford Overseas and Jennison Associates, please see the Advisors’ Report following this letter.
The figures shown represent past performance, which is not a guarantee of future results. (Current performance may be lower or higher than the performance data cited. For performance data current to the most recent month-end, visit our website at vanguard.com/performance.) Note, too, that both investment returns and principal value can fluctuate widely, so an investor’s shares, when sold, could be worth more or less than their original cost.
5
The fund’s two advisory firms, which joined the fund about four years ago, use different but complementary approaches to identifying companies poised to grow profitably over the long term. We’re confident in the advisors’ ability to successfully navigate the uncertainties inherent in stock investing. Fortunately, the fund’s low costs give the advisors a head start in the pursuit of long-term performance.
In the ensuing months, I was struck both by how fortunate I was to work with a great team of Vanguard “crew” and by the remarkable steadiness demonstrated by our clients. Many clients experienced significant losses, but signs of panic were few. On balance, they remained committed to their long-term investment programs and managed to benefit from the financial markets’ subsequent recovery.
As the crisis recedes further in time, it’s important not to lose sight of the lessons that it illuminated about investing and sound financial practices generally. First among those lessons is that diversification does work. Diversification didn’t immunize investors from the market’s decline, but it certainly helped to insulate them from the worst of it.
Second, saving money and living within your means are critical. Investors are acting on this lesson as they pay off debt, which is a form of saving, and increase their savings rates from the dangerously low levels that prevailed before the crisis.
Third, having the courage to stick with a sound investment plan—as so many of our clients did—is important during volatile, uncertain times. Investors who resisted the urge to bail out of stocks at the depths of the crisis have largely been rewarded in the succeeding years.
6
I am very optimistic that, if investors embrace these lessons, they can give themselves a better chance of reaching their long-term goals.
As always, thank you for entrusting your assets to Vanguard.
Sincerely,


