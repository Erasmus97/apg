Dear Shareholder,
Stocks were buoyed by cautious optimism about the economy at the beginning of 2012. However, as the year went on, renewed concerns about the European debt crisis and about growth both at home and abroad curbed investor enthusiasm, especially for riskier assets. In this environment, returns for small-capitalization growth stocks, the focus of Vanguard Explorer Fund, generally lagged those of their large-cap and value-oriented counterparts.
For the 12 months ended October 31, 2012, the fund returned 7.00% for Investor Shares and 7.16% for Admiral Shares. These results trailed the 10.08% return of the benchmark Russell 2500 Growth Index and the 8.96% average return for peer funds.
The fund outperformed its benchmark in consumer discretionary and financials, but not by enough to offset the laggards in information technology and missed opportunities among health care stocks.
If you hold shares of the fund in a taxable account, you may wish to review information about the fund’s after-tax returns later in this report.
2
U.S. stocks led the advance for global equity markets
U.S. stocks returned about 14% for the 12 months ended October 31, putting domestic equities ahead of their international counterparts. Stocks in Europe and Asia, meanwhile, posted modestly positive results.
The advances came amid moves by U.S. and European central bankers to manage risks to the U.S. economy and the finances of European governments and banks. The president of the European Central Bank declared in July that policymakers would do whatever was needed to preserve the euro (common currency).
Although investors’ worries have eased, Europe’s financial troubles are by no means resolved. Vanguard economists believe the most likely scenario is that the Eurozone will “muddle through” for several years, with occasional spikes in market volatility, as fiscal tightening persists in the face of weak economic growth.
Bonds continued their march, but leaner times may lie ahead
The broad U.S. taxable bond market returned about 5% for the 12 months. Municipal bonds delivered a robust performance, returning 9%.
 
3
As bond prices rose, the yield of the 10-year U.S. Treasury note fell to a record low in July, closing below 1.5%. (Bond yields and prices move in opposite directions.) By the end of the period, the yield had climbed, but it still remained exceptionally low by historical standards.
Bondholders have enjoyed years of healthy returns. But as Tim Buckley, our incoming chief investment officer, has noted, investors shouldn’t be surprised if future results are much more modest. As yields remain low, the opportunity for similarly strong returns diminishes.
The Federal Reserve announced on September 13 that it would continue to hold its target for short-term interest rates between 0% and 0.25% at least through mid-2015. The exceptionally low rates, in place since late 2008, kept a tight lid on returns from money market funds and savings accounts.
For investors, grass was greener outside small-cap growth stocks
Vanguard Explorer Fund gained ground during the 12 months under review, but relatively high volatility in the stock market and modest prospects for economic
4
growth led investors to favor better-known names. The fund’s slight tilt toward smaller-cap stocks compared with its benchmark, a strategy that has served it well under different market conditions, proved a modest hindrance to performance for the period. The same was true of the fund’s allocations to innovative but not-yet-profitable companies and to struggling companies poised to make a turnaround.
Information technology stocks, which accounted for almost one-quarter of the fund’s assets during the period, returned about –6% for the fund; in the benchmark,
where their weighting was less, these stocks turned in a slightly positive result. A large portion of the tech-sector declines came from its holdings among semiconductor makers, internet software firms, and communications firms, as a cooling off in business spending contributed to double-digit drops in their stock prices.
Health care, another of Explorer’s heavily weighted sectors, returned almost 15% for the fund. For the benchmark, however, which had a slightly lighter weighting, the sector returned more than 22%. Here, the relative underperformance
5
was largely attributable to the fund’s lack of exposure to several pharmaceutical and biotechnology stocks that produced stellar returns on product pipeline advances and merger-and-acquisition activity.
Among the fund’s successes in stock selection were homebuilders, which helped the fund generate a return almost double that of the benchmark in the consumer discretionary sector. Strong stock selection among banks and insurance companies enabled the fund to outpace the benchmark in financials as well.
Small-cap investors were rewarded for additional risk
For the ten years ended October 31, 2012, the Explorer Fund posted an average annual return of 8.86% for Investor Shares and 9.04% for the Admiral Shares, outperforming the 8.00% average annual return of competing small-cap growth funds but lagging the benchmark index’s annualized return of 10.40%.
Although the fund’s relative results were subpar, investors benefited from the relative strength of small caps. The fund’s annualized return bested the Russell 1000 Index (+6.79%), which tracks larger-cap U.S. stocks, and the Russell 3000 Index (+7.64%), which represents the broad U.S. stock market. All strategies go through periods of relative strength and weakness. Over time, however, we expect that Explorer’s use of multiple talented advisors with diverse strategies, delivered to investors at a low cost, will help strength to predominate.
How our core purpose informs our approach to active management
At Vanguard, we sum up our core purpose this way: To take a stand for all investors, to treat them fairly, and to give them the best chance for investment success. When it comes to our actively managed funds, such as the Explorer Fund, this commitment to investors is reflected both in our rigorous process for selecting fund advisors and in our ongoing efforts to keep the costs of our funds low. We believe our approach gives investors the opportunity to outperform market indexes over the long term.
Because we recognize the challenges inherent in active management, Vanguard has offered, and advocated for, index funds for more than 35 years. By seeking to track—rather than beat—market returns, index funds provide several benefits: low costs, diversification across a market, and limited deviation from the performance of market benchmarks.
6
Still, the case for indexing doesn’t preclude the potential for skilled, seasoned managers to deliver outperformance. And the chances for success can increase if those managers deliver their services at low cost, allowing investors to keep more of their returns.
As always, thank you for entrusting your assets to Vanguard.
Sincerely,


