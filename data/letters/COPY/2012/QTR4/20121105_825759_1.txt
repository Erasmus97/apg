Dear Credit Union Shareholders, 
Credit unions are posting strong results in 2012. Membership has increased for six consecutive quarters. Core deposits are rising and now account for two-thirds of total share balances. Lending activity is at a
record pace through mid-year. Earnings are the highest in seven years. The performance results are an outcome of credit unions’ ongoing success in building member relationships. 
While loan growth is picking up, growth in share balances continues to outpace the increase in loans outstanding. As a result, liquidity remains
high with a loan to share ratio of 67% at mid-year 2012. The $392 billion investment portfolio is being re-shaped as it grows. Agency securities have become the primary investment vehicle for credit unions, followed by certificates of deposits at
banks and S&amp;Ls. Corporate credit unions now account for less than 10% of the industry’s portfolio. 
The shift in the investment
portfolio underscores the need for credit union designed and led investment options. Trust for Credit Unions’ (“TCU”) 25-year history is a reflection of the relevance of such an alternative to the industry. With the Federal Reserve
now expecting to maintain the historically low interest rate environment into 2015, having the capability to respond to changing credit union investment needs is more important than ever. TCU provides such a platform, supported by the credit unions
in the Callahan Credit Union Financial Services LLLP partnership. 
The three Portfolios in the TCU family of mutual funds complement each
other with different objectives and duration targets. Each Portfolio provides daily pricing and same-day or next-day liquidity. By combining the Portfolios to create a duration profile that meets their balance sheet objectives, investors are able to
tailor their usage of the TCU Portfolios to their situation. Investors can see the effect of combining the three Portfolios by using the Yield Optimizer tool, available at www.TrustCU.com. 
The TCU Money Market Portfolio’s yield reflects the Federal Reserve’s interest rate policies. The standardized 7-day current and effective
yields, with fee waivers, on the Money Market Portfolio as of August 31, 2012 are 0.11%. 
The Ultra-Short Duration and Short
Duration Portfolios are utilized by credit unions looking to hold longer duration investments. The cumulative total return was 0.58% for the Ultra-Short Duration and 1.02% for the Short Duration for the 12-month period ended August 31, 2012.

Please visit our website, www.TrustCU.com, for the most current information on the portfolios, including performance and portfolio
holdings. We appreciate your investment in Trust for Credit Unions. Let us know if there are other ways in which we can help complement your credit union’s investment strategy. 
Sincerely, 


