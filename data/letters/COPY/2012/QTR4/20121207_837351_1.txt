Dear fellow shareholder,

On June the First Pacific Low Volatility Fund ("Fund") commenced
operations as a new series of First Pacific Mutual Fund, Inc. The investment
objective of the Fund is to achieve long-term capital appreciation and
preservation of capital while lowering volatility.

Despite the many headwinds faced by a global economic recovery, equity
markets finished the twelve-months ending September with strong gains.
The European sovereign debt crisis was aided by debt purchase programs from the
European Central Bank, and statement from President Draghi committing to
maintaining the Eurozone. In the U.S., the Federal Reserve announced its third
major round of bond purchases and focused efforts on the mortgage-backed sector.
The central bank also extended its commitment to maintain interest rate levels
low until the middle of The efforts have been successful in maintaining
the equity market rally. However, investors are displaying concerns over the
fiscal-cliff of U.S. automatic spending cuts and increased tax rates and its
effects on consumers' income and company profitability.

Despite a rally in higher-beta and cyclical sectors of the market, the
portfolio managers of the Fund maintained a preference for dividend-paying
companies with a historically lower market sensitivity profile. The Fund's cash
levels detracted from relative performance as the broad equity markets rose
versus a very-low yielding cash environment. At September the Fund had
a cash allocation of of its net assets.

The portfolio managers weighted a higher percentage of the portfolio
within defensive sectors throughout the most recent twelve months. As of
September the top five categories as a percentage of net assets were
Health Care Structured Notes Information Technology
Consumer Staples and Cash The sector focus also
detracted from relative performance as the higher-beta sectors and cyclical
sectors of the market outperformed during the time period.

CATEGORY ALLOCATION (% of Net Assets)
September

[The following table was depicted as a pie chart in the printed material.]

Consumer Discretionary
Consumer Staples
Energy
Financials
Health Care
Industrials
Information Technology
Telecommunication Services
Utilities
Exchange Traded Funds
Structured Notes
Money Market Fund
Options
Other Liabilities

There was no capital gain distribution to shareholders for the
calendar year. There will be a capital gain distribution for the Fund for the
calendar year.

On the following pages you will find our September Annual Report.
If you have any questions or would like us to provide information about the Fund
to your family or friends, please call us at or

Thank you for your business. On behalf of the staff and management of the
Fund, I would like to extend to you and your family best wishes for a safe and
happy holiday season.


Warmest Aloha,

/s/ Terrence K.H. Lee

Terrence K.H. Lee
President and CEO

Dear fellow shareholder,

As we begin our year of operations, we are pleased to provide you
with our Fund's Annual Report.

It has been and continues to be daunting times for investors. We have
seen unprecedented downturns in the equity markets followed by sharp rises in
major indices. The economy which was in a recession is now showing signs of a
sluggish recovery. Amidst this continued turmoil, municipal bonds have
generally experienced less volatility than many of their equity counterparts.
As shareholders of the Hawaii Municipal Fund, you are earning tax-free income*
and supporting local projects designed to enrich our community. The money
raised through municipal bonds is commonly used to build schools, hospitals,
roads, airports, harbors, and water and electrical systems that serve to create
jobs and improve the quality of life here in our islands.

Interest rates are the most important of many factors which can affect
bond prices. Over the course of the fiscal year, the treasury yield curve was
virtually unchanged with short-term rates remaining near and long-
term interest rates falling by just basis points to The long-end of
the municipal yield curve, however experienced a greater drop in rates.
Municipal rates fell due to increasing concerns of higher future State of
Hawaii and federal income taxes. This accounts for the Hawaii Municipal Fund's
fiscal year price increase of per share. The Hawaii Municipal Fund
Investor Class had a net asset value ("NAV") of on October and a
NAV of on September The primary investment strategy of the
Hawaii Municipal Fund is to purchase primarily investment grade long-term Hawaii
municipal bonds. The past year's performance of the Fund, which is presented in
this Annual Report, was primarily a result of the implementation of this
strategy.

During the fiscal year ended September the Federal Reserve Bank
kept the Federal Funds Rate between and During this period the year
treasury bond's yield fell by basis points to In our opinion, the
continuation of low interest rates in the long end of the yield curve can be
interpreted as an indication that bond investors believe inflation will not
greatly increase over the long-term. Still, there continues to be risks to
inflation and the bond market, among which are U.S. fiscal policy, international
conflicts/terrorism and global economic factors.

On the following pages are line graphs comparing the Fund's performance
to the Barclays Capital Municipal Bond Index for the years ended September
The graph assumes a hypothetical investment in the Fund. The
object of the graph is to permit a comparison of the Fund with a benchmark and
to provide perspective on market conditions and investment strategies and
techniques that materially affected the performance of the Fund. For the fiscal
year ended September the Hawaii Municipal Fund's concentration in
Hawaii municipal bonds caused the portfolio to lag the Index. For the and
year periods ended September the Hawaii Municipal Fund's portfolio had
a shorter effective maturity than the Barclays Capital Municipal Bond Index,
therefore, the Hawaii Municipal Fund lagged the Index due to the declining
interest rate environments.

MOODY'S MUNICIPAL BOND RATINGS
Hawaii Municipal Fund
September

[The following table was depicted as a pie chart in the printed material.]

Aaa









NA^

^Primarily all of the investments in the Hawaii Municipal Fund portfolio
are investment grade securities. Only of the municipal bonds purchased
for the portfolio are deemed to be below investment grade by the
Investment Manager.

We are proud to report that as a Hawaii resident, of the income
dividends earned in were both state and federal tax-free*. There was no
capital gain distribution to shareholders for the calendar year.
There will not be a capital gain distribution for the Hawaii Municipal Fund for
the calendar year.

If you have any questions about this Annual Report or would like us to
provide information about the Fund to your family or friends, please call us at


Thank you for your business as well as the many referrals. On behalf of
the staff and management of the Fund, I would like to extend to you and your
family best wishes for a safe and happy holiday season.

Warmest Aloha,

/s/ Terrence K.H. Lee

Terrence K.H. Lee
President and CEO
