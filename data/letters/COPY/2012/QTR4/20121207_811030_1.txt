Dear Fellow Shareholder:



Thank you for your investment in the TCM Small Cap Growth Fund (the Small Cap Fund) and the TCM Small-Mid Cap Growth Fund (the SMID Cap Fund).This is the annual report to shareholders of the Funds, covering the fiscal year ended September report includes a discussion of the factors that impacted the performance of the Funds for the period, as well as information on Fund expenses and holdings.The report also contains the audited financial statements of each Fund and a Report of Independent Registered Public Accounting Firm.



The markets have been very strong for the one year period ended September with a significant portion of that performance coming last October and the first quarter of letter summarizes the market environment over the past fiscal year and our current investment outlook.Following this letter are the discussions on performance and Fund information for the Small Cap Fund and the SMID Cap Fund, which begin on pages and respectively.The individual Fund discussions are similar because the investment process for each Fund is the same and there is significant overlap in the portfolios.



Market Review



Fourth Quarter A Relief Rally Starts Off the Fiscal Year.The very poor performance of the equity markets in the third quarter of (the Russell Growth was down over was driven by macro-economic factors (slowing growth worldwide and sovereign debt), as valuations and fundamentals took a back seat to fear and liquidity concerns.By the end of September the fear and loathing for stocks peaked for the year, setting the stage for a rally on any positive news, or just the absence of bad news.Many companies have taken meaningful steps since to shore up their balance sheets and achieve healthy profit margins.With record levels of cash on U.S. corporate balance sheets (itself a fear indicator) reaching all-time highs, companies were positioned to withstand short-term shocks and had the resources to maintain spending levels and respond to a challenging growth environment.The market had discounted so much bad news that the relief of uncertainty, particularly as it relates to the prospects of avoiding an EU banking crisis, led to a snap back relief rally.That relief came in the first week of October as European leaders met, yet again, to address the Greek debt crisis and affirmed that the major players were prepared to backstop the weaker members in the European Union.Even though there was very little substance to these public comments and meetings, the market rallied on any positive news throughout the month.That rally culminated on October when an accord was announced that bondholders would























TCM GROWTH FUNDS








accept a voluntary write-down on Greek bonds, which miraculously did not trigger the default provisions on credit default swaps.In any event, when it was all said and done, October turned out to be the third best month on record for the Russell Growth Index.



The rally was short-lived, however, and the rest of the quarter ended up essentially flat with high volatility and low trading volume.In November, small cap stocks were seemingly headed for a month of double digit negative returns, when a strong rally over the last three trading days of the month turned the period into more of a nonevent.While it appeared that fundamentals and valuations were making a comeback in terms of their importance to stock prices, the very positive but volatile response by markets at the end of November to global efforts by the worlds Central Banks to calm fears relating to the European debt crisis only served to remind us that macro/political factors were still driving short term market performance.December was also a relatively flat month for the equity markets as investors digested a stream of moderately improving economic data, including improving sales of durable goods and new homes, as well as expressed some relief at the extension of the payroll tax cuts.



First Quarter Climbing a Wall of Worry.A significant rally in the first quarter of completed the round trip for small cap stocks as the Russell Growth Index ended the quarter exactly where it started on March investors are only too aware, the trip was volatile and it took a rise in the benchmark over the last two quarters of the period to complete the rebound.During this period, smaller cap stocks outperformed larger caps and higher beta stocks have made the strongest contribution to returns (although not consistently).As the perceived risk of fat-tailed events and global recession lessened during this period, the correlation of returns (the percentage of stocks moving in the same direction) declined from historic highs in October to a more normal range in the first quarter.In addition, volatility (as measured by the VIX Index) and credit spreads fell significantly in the first quarter, which provided a more normal investing environment as investors began to focus on the fundamentals and valuations of individual companies again.In this higher beta environment, traditional growth sectors such as consumer discretionary, healthcare and technology performed the best, as investors focused on companies with stronger revenue and earnings growth rates.The consumer discretionary sector also benefitted from an extraordinarily warm winter in the East and a positive trend in consumer sentiment due to an improving labor market and growing confidence that the U.S. economy was on a positive track.There were plenty of reasons for fear (European debt problems, sluggish economy, withdrawal of stimulus, political paralysis, China hard landing, high oil prices/Iran, etc.), but the market climbed this wall of worry with vigor.


























TCM GROWTH FUNDS









Second Quarter Euro Worries Reduex.Uncertainty continued to reign in Europe, however, and like a broken record and for the third spring in a row, sovereign and bank debt issues weighed on the markets, resulting in higher volatility and low trading volumes in April and May.Recent cooperative efforts to support the European banking institutions created a bit of a relief rally in late June, but still reflected a kicking the can down the road mentality.Greece continued to weigh on equity and bond markets after needing two different elections to form a coalition government that would ultimately accept the terms mandated by the European Union bailout.Even that coalition had to support some form of renegotiation of the timetable for the austerity cuts forced on the Greek government.The fiscal difficulties of Spain and Italy also weighed on the market, exacerbating the fear of contagion and recession.



Emerging market economies have slowed down in response to previous government efforts (e.g., raising interest rates and bank reserve requirements, tightening lending) to contain inflation.Some of those countries (China and Brazil) have recently cut rates in an effort to reverse that slow down and stimulate growth, which will likely help offset the impacts from a slowing Euro zone.In this environment of volatile macro news and sentiment shifts, defensive sectors such as healthcare and consumer staples performed the best during the quarter and more economically sensitive sectors, particularly energy and technology, were punished as investors sought safety from news headlines.



Third Quarter Central Bankers to the Rescue.During the third quarter of there was relatively little change in the number of uncertainties facing investors the looming fiscal cliff, slower growth in emerging markets, sovereign debt and recessionary pressures in Europe, the U.S. presidential election and a lackluster domestic economy.Although July was weak based on concerns over earnings expectations, the quarter proved to be very calm compared to the third quarters of and as equity markets rallied on the back of dramatic steps taken by central bankers worldwide.First, President Draghi of the European Central Bank (ECB) pledged that the ECB was willing to do whatever it takes to keep the Euro zone unified in response to fiscal troubles in the weaker countries.This pledge was followed by the announcement of a substantial sovereign debt program for those troubled countries that were in compliance with bailout terms.These and other actions in Europe helped to calm bond markets but rates remain high because the deficit/economic problems still exist and much has to occur to fix them.In addition, it is becoming increasingly obvious that countries like Spain, Italy and France, among others, will not make their budget deficit reduction targets without raising more revenue or cutting spending.























TCM GROWTH FUNDS









The Europeans are not alone in their efforts to stimulate growth through monetary policy.As mentioned, China and Brazil have lowered rates in response to tepid growth and declining inflation, reversing their tightening efforts earlier in the year.And all eyes were on the Federal Reserve (the Fed) in September as it announced a third round of quantitative easing by initiating an open-ended round of major bond buying up to billion a month in an effort to keep rates low, stimulate mortgage financing and boost the housing sector.The Fed is pinning its hopes on the power of cheap money to create a virtuous circle of asset inflation, increased borrowing and spending by consumers and businesses, higher employment, and economic growth.Signaling that rates will stay low until at least mid the Fed is willing to risk the debasement of the dollar and higher future inflation in order to stimulate growth and lower unemployment rates.Clearly, the equity markets have responded positively to a zero interest rate environment but it is doubtful that monetary policy alone will stimulate growth.



Market Outlook.The actions of central bankers have, however, mitigated the impact of many of the macroeconomic factors that have weighed lately on the market.That is not say that they are resolved or eliminated, but lower rates can help drive investors to riskier assets and buy time, which should insulate the equity markets from the macro shocks we experienced in the past.Uncertainty is still a significant drag on equity valuations and corporate capital equipment decisions, but the weight of uncertainty should lessen as we move into the new year.By then, the presidential and congressional elections will be over and steps to deal with the fiscal cliff will be in process.The overall reduction of uncertainty will likely be a positive for business spending and the market.



Trading volumes have been quite low during the summer and have yet to meaningfully increase, which is exacerbating individual stock volatility.As a result, we continue to focus on attempting to take advantage of the volatility by keeping a longer term perspective with our companies.It is clear that revenue and profit growth is becoming scarcer, which can be a positive for valuations when we successfully identify high quality companies that have strong cash flow and stable growth prospects.In addition, assuming the U.S. avoids a recession, merger activity may pick up post the election uncertainties, as companies with cash seek to augment organic growth through acquisitions.Small and midcap companies can be prime beneficiaries of a pickup in deal activity.We also believe technology companies that provide products which enhance productivity and efficiency may command a premium in this environment of constrained world growth.



Thank you for your continued support.While we have seen an improvement in market conditions since last October, we know it has been a frustrating period for relative performance.The firm is fortunate to have clients with a long term

























TCM GROWTH FUNDS









investment perspective.We continue to be confident in the merits of our investment process and resolutely believe that this patience should be rewarded.Again, please see the following pages for the discussions on the individual Funds.



Sincerely,
