Dear Shareholder, 
If the last few years have taught us anything, it’s that being a patient,
long-term investor isn’t always easy. It takes courage and commitment to stay invested when the moods of the markets swing from euphoria to panic, with very little provocation. A comment made by a European leader, a government official in the
U.S. or simply a news media spokesperson can send the markets racing up or spiraling down. But this is the world in which we live and invest today and, consequently, it has never been more important to have a long-term financial plan, realistic
goals and timelines, and regular check-ins with your financial advisor. 
®
Whether you’re saving for retirement, already there or building a college fund or a charitable giving trust, Russell has a long, proud heritage of developing multi-asset solutions to help investors
like you reach your financial goals. This year we’ve made a number of changes to our portfolios designed to deliver greater diversification and potentially lower volatility to many of the investments we manage. Combined with the guidance of
your advisor, we believe these changes and additions to our funds and portfolios will help you achieve a more broadly diversified portfolio and a more consistent outcome. 
On the following pages you can gain additional insights by reviewing our Russell Investment Company 2012 Annual Report for the fiscal year ending October 31, 2012, including portfolio management
discussions and fund performance information. 
Thank you for the trust you have placed in our firm. All of us at Russell Investments appreciate
the opportunity to help you achieve financial security. 
Best regards, 


Dear Shareholder, 
If the last few years have taught us anything, it’s that being a patient,
long-term investor isn’t always easy. It takes courage and commitment to stay invested when the moods of the markets swing from euphoria to panic, with very little provocation. A comment made by a European leader, a government official in the
U.S. or simply a news media spokesperson can send the markets racing up or spiraling down. But this is the world in which we live and invest today and, consequently, it has never been more important to have a long-term financial plan, realistic
goals and timelines, and regular check-ins with your financial advisor. 
®
Whether you’re saving for retirement, already there or building a college fund or a charitable giving trust, Russell has a long, proud heritage of developing multi-asset solutions to help investors
like you reach your financial goals. This year we’ve made a number of changes to our portfolios designed to deliver greater diversification and potentially lower volatility to many of the investments we manage. Combined with the guidance of
your advisor, we believe these changes and additions to our funds and portfolios will help you achieve a more broadly diversified portfolio and a more consistent outcome. 
On the following pages you can gain additional insights by reviewing our Russell Investment Company 2012 Annual Report for the fiscal year ending October 31, 2012, including portfolio management
discussions and fund performance information. 
Thank you for the trust you have placed in our firm. All of us at Russell Investments appreciate
the opportunity to help you achieve financial security. 
Best regards, 


