Dear Fellow Shareholder,



Despite challenging macroeconomic conditions, the S&amp;P Index advanced over the past fiscal year. Its route was not direct, however. Equities saw tremendous volatility, cycling between fear-driven declines and relief rallies as investors focused on three key issuesthe ongoing European sovereign debt crisis, the direction of the U.S. economy, and the potential global impact of a growth slowdown in China.



In the fall of markets suffered as fears of contagion within the eurozone and beyond reached new highs, incorporating the possibility of a second global banking collapse and dissolution of the euro. In the U.S., high unemployment, low consumer confidence and weak retail sales, productivity and manufacturing statistics also affected sentiment. Further, growth appeared to be slowing in the BRIC economies (Brazil, Russia, India and China), with repercussions for global growth as well as the global commodity cycle.



In December, markets rallied as the European Central Bank (ECB) presented a viable option for relieving pressure on eurozone banks through LTRO (long-term refinancing option) programs. This dovetailed with stronger-than-expected first quarter results from U.S. corporationsdue in part to an unusually mild winter in North Americain fueling the rally through April.



Momentum reversed in May, on renewed concerns from Europe, where Greek voters rejected austerity platforms and Spain's largest bank requested a bailout on real estate losses. Slower economic growth data from the U.S. and China, which has seen GDP decline from roughly in to in added to the downward pressure.



From June through the end of August, markets recovered on anticipation of additional support from the Federal Reserve and ECB, which in fact occurred in September, just after this period closed. Federal Reserve Chairman Bernanke announced designed to keep interest rates low and buoy employment in the U.S., just after ECB President Draghi announced "OMT," a plan to buy additional sovereign bonds to help strengthen the eurozone.



When markets are driven by macroeconomic factors and central bank policy, it is typically difficult for fundamentals-focused active managers to outperform. This period was no exception. With a high level of correlation among stocks, and a great deal of fear driving investor sentiment, investors flocked to the relative safety of U.S. large- and mega-cap stocks, especially those with high yields. Less attention was given to company-specific fundamentals such as earnings, cash flows, and balance sheet strength, or competitive advantagesfactors our portfolio managers consider essential to longer-term outperformance.



As we look ahead, signals are mixed. U.S. GDP may fall below for the second half of unemployment remains elevated, and industrial production and profit margins have declined. Politically, the upcoming "fiscal cliff" and tax and regulatory uncertainty add weight to the scale. In our opinion, recession in Europe is likely, and China has slowed significantly. Additionally, geopolitical concerns, especially in the Middle East, are on the rise.



At the same time, positive trends are evident, particularly in the U.S. represents a commitment to low interest rates through is good for businesses and consumers. Consumer sentiment rose in August, with optimism largely tied to improvements in household balance sheets. While still elevated at (as of the end of August), unemployment has dropped steadily throughout the year. In housing, new starts and sales of existing homes are up, perhaps signaling the sector has bottomed. Retail had a strong back-to-school season, and North American energy production innovations could have lasting impacts across various industry sectors.



Within the stock market, valuations remain attractive and fundamentals are strong. With equities offering good opportunity, especially relative to current bond yields, inflows from cash on the sidelines could also be positive. Until some of the uncertainties subside, however, volatility could continue. Experienced investors know that over time, the markets reward businesses that outperform. Our managers continue to apply their disciplines, using research, analysis, insight, and expertise to identify what they believe are the best ideas for the longer-term.



Thank you for your confidence in Neuberger Berman. We look forward to continuing to serve your investment needs.



Sincerely,
