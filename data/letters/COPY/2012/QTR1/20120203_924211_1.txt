Dear Investor:





Thank you for reviewing this annual report for the period ended November Our report offers investment performance and portfolio information, presented with the expert perspective of our portfolio management team.




This report remains one of our most important vehicles for conveying information about fund returns, as well as market factors and strategies that affected fund performance. For additional, updated information, we encourage you to visit our website, americancentury.com. Click on the Fund Performance and Insights &amp; News headings at the top of our Individual Investors site.




Reporting Periods Divided Nature Resulted in Mixed Returns



The financial market performance that most U.S. investors experienced during the months ended November generally reflected the periods divided nature. For the first six months, confidence in global economic growth, strong corporate earnings, and increased risk-taking generally ruled the markets. The MSCI EAFE Index and the S&amp;P Index advanced approximately for the six months ended May as stocks broadly outperformed high-quality bonds for that period.




However, the tables reversed sharply for the final six months. The risk-taking tide ebbed during the summer months, constrained by high fuel prices, federal budget management concerns in the U.S., and the worsening sovereign debt crisis in Europe. High-quality bonds, led by long-maturity U.S. Treasury securities, mostly outpaced stocks. The MSCI EAFE Index and the S&amp;P Index returned and respectively, during the six months ended November despite a significant market rebound in October.




As a result of this volatility, returns were mixed for the full period. U.S. and international bonds and U.S. stocks generally outperformed international stocks. The S&amp;P Index and the MSCI EAFE Index returned and respectively, for the fiscal year.




Unfortunately, further volatility appears likely in as the markets wrestle with uncertainties regarding European debt, economic strength, government budget deficits, and the U.S. presidential election. We believe strongly in adhering to a disciplined, diversified, long-term investment approach during volatile periods, and we appreciate your continued trust in us during these unsettled times.




Sincerely,

Dear Fellow Shareholders,





The board of directors of the fund was pleased at the announcement of a new strategic partner for the investment advisor to the American Century Investments funds. Canadian Imperial Bank of Commerce (CIBC), a leading Canadian financial institution, purchased the percent economic interest in American Century Companies, the parent corporation of the advisor, previously held by JPMorgan Chase &amp; Co. Based in Toronto, CIBC provides a full range of retail and wholesale banking services to almost million clients through approximately branches and offices in Canada, the U.S. and around the world. This transaction will benefit fund shareholders by bolstering the financial strength of the advisor and providing a strategic partner to help support its growth initiative to broaden non-U.S. distribution of its products and services.




The board also has been briefed throughout the year on the impact on fund performance of the European banking crisis, the U.S. deficit reduction debates, and the pace of economic growth. While the performance of all funds has been affected, the majority of American Century Investments funds overseen by the board are exceeding their benchmarks for the one-, three-, five-, and ten-year periods ended September This is commendable performance, particularly in these challenging market conditions.




We are completing another year of board oversight on your behalf. We appreciate any comments you would like to share with the board. Send them to me at dhpratt@fundboardchair.com. Thank you for your continued investment in American Century Investments funds.




Best regards,

Dear Investor:





Thank you for reviewing this annual report for the period ended November Our report offers investment performance and portfolio information, presented with the expert perspective of our portfolio management team.




This report remains one of our most important vehicles for conveying information about fund returns, as well as market factors and strategies that affected fund performance. For additional, updated information, we encourage you to visit our website, americancentury.com. Click on the Fund Performance and Insights &amp; News headings at the top of our Individual Investors site.




Reporting Periods Divided Nature Resulted in Mixed Returns



The financial market performance that most U.S. investors experienced during the months ended November generally reflected the periods divided nature. For the first six months, confidence in global economic growth, strong corporate earnings, and increased risk-taking generally ruled the markets. The MSCI EAFE Index and the S&amp;P Index advanced approximately for the six months ended May as stocks broadly outperformed high-quality bonds for that period.




However, the tables reversed sharply for the final six months. The risk-taking tide ebbed during the summer months, constrained by high fuel prices, federal budget management concerns in the U.S., and the worsening sovereign debt crisis in Europe. High-quality bonds, led by long-maturity U.S. Treasury securities, mostly outpaced stocks. The MSCI EAFE Index and the S&amp;P Index returned and respectively, during the six months ended November despite a significant market rebound in October.




As a result of this volatility, returns were mixed for the full period. U.S. and international bonds and U.S. stocks generally outperformed international stocks. The S&amp;P Index and the MSCI EAFE Index returned and respectively, for the fiscal year.




Unfortunately, further volatility appears likely in as the markets wrestle with uncertainties regarding European debt, economic strength, government budget deficits, and the U.S. presidential election. We believe strongly in adhering to a disciplined, diversified, long-term investment approach during volatile periods, and we appreciate your continued trust in us during these unsettled times.




Sincerely,

Dear Fellow Shareholders,





The board of directors of the fund was pleased at the announcement of a new strategic partner for the investment advisor to the American Century Investments funds. Canadian Imperial Bank of Commerce (CIBC), a leading Canadian financial institution, purchased the percent economic interest in American Century Companies, the parent corporation of the advisor, previously held by JPMorgan Chase &amp; Co. Based in Toronto, CIBC provides a full range of retail and wholesale banking services to almost million clients through approximately branches and offices in Canada, the U.S. and around the world. This transaction will benefit fund shareholders by bolstering the financial strength of the advisor and providing a strategic partner to help support its growth initiative to broaden non-U.S. distribution of its products and services.




The board also has been briefed throughout the year on the impact on fund performance of the European banking crisis, the U.S. deficit reduction debates, and the pace of economic growth. While the performance of all funds has been affected, the majority of American Century Investments funds overseen by the board are exceeding their benchmarks for the one-, three-, five-, and ten-year periods ended September This is commendable performance, particularly in these challenging market conditions.




We are completing another year of board oversight on your behalf. We appreciate any comments you would like to share with the board. Send them to me at dhpratt@fundboardchair.com. Thank you for your continued investment in American Century Investments funds.




Best regards,

Dear Investor:





Thank you for reviewing this annual report for the period ended November Our report offers investment performance and portfolio information, presented with the expert perspective of our portfolio management team.




This report remains one of our most important vehicles for conveying information about fund returns, as well as market factors and strategies that affected fund performance. For additional, updated information, we encourage you to visit our website, americancentury.com. Click on the Fund Performance and Insights &amp; News headings at the top of our Individual Investors site.




Reporting Periods Divided Nature Resulted in Mixed Returns



The financial market performance that most U.S. investors experienced during the months ended November generally reflected the periods divided nature. For the first six months, confidence in global economic growth, strong corporate earnings, and increased risk-taking generally ruled the markets. The MSCI EAFE Index and the S&amp;P Index advanced approximately for the six months ended May as stocks broadly outperformed high-quality bonds for that period.




However, the tables reversed sharply for the final six months. The risk-taking tide ebbed during the summer months, constrained by high fuel prices, federal budget management concerns in the U.S., and the worsening sovereign debt crisis in Europe. High-quality bonds, led by long-maturity U.S. Treasury securities, mostly outpaced stocks. The MSCI EAFE Index and the S&amp;P Index returned and respectively, during the six months ended November despite a significant market rebound in October.




As a result of this volatility, returns were mixed for the full period. U.S. and international bonds and U.S. stocks generally outperformed international stocks. The S&amp;P Index and the MSCI EAFE Index returned and respectively, for the fiscal year.




Unfortunately, further volatility appears likely in as the markets wrestle with uncertainties regarding European debt, economic strength, government budget deficits, and the U.S. presidential election. We believe strongly in adhering to a disciplined, diversified, long-term investment approach during volatile periods, and we appreciate your continued trust in us during these unsettled times.




Sincerely,

Dear Fellow Shareholders,





The board of directors of the fund was pleased at the announcement of a new strategic partner for the investment advisor to the American Century Investments funds. Canadian Imperial Bank of Commerce (CIBC), a leading Canadian financial institution, purchased the percent economic interest in American Century Companies, the parent corporation of the advisor, previously held by JPMorgan Chase &amp; Co. Based in Toronto, CIBC provides a full range of retail and wholesale banking services to almost million clients through approximately branches and offices in Canada, the U.S. and around the world. This transaction will benefit fund shareholders by bolstering the financial strength of the advisor and providing a strategic partner to help support its growth initiative to broaden non-U.S. distribution of its products and services.




The board also has been briefed throughout the year on the impact on fund performance of the European banking crisis, the U.S. deficit reduction debates, and the pace of economic growth. While the performance of all funds has been affected, the majority of American Century Investments funds overseen by the board are exceeding their benchmarks for the one-, three-, five-, and ten-year periods ended September This is commendable performance, particularly in these challenging market conditions.




We are completing another year of board oversight on your behalf. We appreciate any comments you would like to share with the board. Send them to me at dhpratt@fundboardchair.com. Thank you for your continued investment in American Century Investments funds.




Best regards,
