Dear Shareholder: 
 
The Sound Shore Fund ended December 31,
2011 with a net asset value of $29.53 per share after a distribution of $0.196684 at year-end. The fourth quarter total return of 12.05% was ahead of the Standard &amp; Poor’s 500 Index (“S&amp;P 500”), which returned 11.82% and
lagged the Dow Jones Industrial Average (“Dow Jones”), which returned 12.78%. For the year, the Fund declined -6.18%, which trailed results for the S&amp;P 500 and the Dow Jones of 2.11% and 8.38%, respectively. Over the last decade, the
Fund’s cumulative total return has been 40.79% versus 33.35% for the S&amp;P 500. 
 
Performance data quoted represents past performance and is no guarantee of future results. Current performance may be lower or higher
than the performance data quoted. Investment return and principal value will fluctuate so that an investor’s shares, when redeemed, may be worth more or less than original cost. The Fund’s 1, 5, 10, and 15-year average annual total
returns for the period ended December 31, 2011 were -6.18%, -2.52%, 3.48%, and 5.98%, respectively. As stated in the current prospectus, the Fund’s annual operating expense ratio is 0.95%. For the most recent month-end
performance, please visit the Fund’s website at www.soundshorefund.com.
 
US equity indices posted only marginal gains in 2011, yet were among the best performing in the world. Behind the slightly positive return of the US stock market there were
significant divergences. For example, the downside volatility of the third quarter was almost matched by the strong fourth quarter rebound, up nearly 12% for the S&amp;P 500. As well, the defensive and higher yield utilities sector was up
19% for the year while the financials were at the other end of the spectrum, down -17%. Now in our 34th year, Sound Shore Management, Inc. has seen many challenging markets and, though our performance trailed in 2011, our prior experience is
that staying committed to our disciplined value strategy remains the best route to competitive long-term returns for our investors. 
 
The recent environment of solid earnings amidst global economic uncertainty has caused many companies to recalibrate
growth expectations. Shareholders have often been beneficiaries of this evolving mindset via increased dividends and share repurchases or other restructuring activities. Sound Shore’s low price earnings investment framework has always
included free cash flow and balanced capital allocation as criteria and several of our best contributors for 2011, including Marathon, Lowe’s, and Pfizer, were leaders in this shareholder-friendly trend. 
 
At Marathon, for example, we initiated our
position in early 2010 when the integrated oil company was selling at 7 times forward earnings and 4 times cash flow. At the time, we believed the company’s refining and marketing results could prove better than depressed forecasts due to
a recent expansion at the company’s largest refinery. After executing on significant improvement through 2010, Marathon 
 
 
1 
 
 
 

announced in early 2011 that it would split into separately traded upstream and downstream companies, further unlocking value. As part of the split, the company used its de-leveraged balance
sheet to increase its dividend as well. We sold our Marathon positions as they achieved target valuations during the second half of 2011. 
 
Meanwhile, home improvement retailer Lowe’s, a new holding in 2011, hit our valuation screens in late 2010 when
the stock was trading well below norm at 11 times forward earnings with a 2.5% dividend yield. We viewed management’s recently changed capital allocation plan, emphasizing lower investment and narrowing the profitability gap to Home Depot,
as a significant departure from its past prioritization of store growth. After initiating our position in the third quarter, we benefited from the company’s fourth quarter announcement that its considerable free cash flow would be used to
repurchase up to 15% of its shares outstanding per year for a multi-year period. 
 
Similarly, global drug maker Pfizer, a longer term position, started the year valued at 7 times 2011 earnings due to investor concerns about the expiration of its Lipitor
patent. Our analysis concluded that the company’s earnings and free cash flow would prove sturdier than expected due to the significant cost cuts and portfolio restructuring moves being taken by new CEO Ian Read. Pfizer shares
steadily outperformed through the year as earnings estimates gradually rose and also after the company announced a higher than expected dividend increase and 2012 share repurchase program during the fourth quarter. 
 
Several of our larger 2011 detractors,
including General Motors and Citigroup, saw their valuation multiples compress despite improving financial performance. At GM, for example, strong vehicle pricing discipline, cost control, and market share improvement allowed the company to
grow 2011 earnings by 29% versus 2010. However, the company’s forward P/E multiple declined to 5 times our 2012 estimate due to concerns about the global economy. Likewise, Citi’s 2011 tangible book value progressed 12% through
the year but fallout from Europe’s rocky credit markets cut Citi’s price to tangible book multiple from 1.1 times to 0.5 times. Both GM and Citi are compelling risk-reward opportunities given their low valuations, stable financial
positions, and completely new management teams that are successfully executing on focused strategies that are significant departures from their over-diversified histories. 
 
Extended uncertainty in the global economy
has depressed equity valuation multiples, which we view as an opportunity for our disciplined value investment framework. Indeed, Sound Shore’s portfolio is valued at 10.3 times forward earnings which compares favorably to both the S&amp;P
500 at 12.4 times and generationally low competitive yields for 10 Year US Treasury Bonds. Since 1978 Sound Shore has been fortunate in the stability of our long-term client base and the commitment of our experienced research team to our one
value investment strategy. As in the past, we believe these critical factors should allow us to take advantage of the current confluence of economic uncertainties, investor disdain for equities, and very low valuations. We strive to keep the
confidence you have placed with us. 
 
 
2 
 
 
 
 
As always, thank you for your investment alongside ours in Sound Shore. 
 
Sincerely, 


