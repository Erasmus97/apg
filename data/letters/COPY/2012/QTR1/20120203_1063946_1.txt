Dear Shareholder,
Recent months have seen headlines and financial markets
continue to focus on the ongoing euro zone debt crisis. Acknowledging that the status quo was untenable, in early December, Europe opted to move closer
together rather than be torn apart. All 17 nations that use the euro and nine other European Union members agreed to a new fiscal compact that would
invite closer scrutiny of individual country budgets. Ratings agencies were mostly unimpressed by these machinations; ultimately, Standard &amp;
Poor’s downgraded nine countries in the region, including formerly AAA rated France.
With the stability of Italy and Spain now in question, the
need for an effective policy response has become even more acute. The new governments in both countries will have but a short grace period in which to
demonstrate their resolve; both have been forced to pay yields at euro-era highs for newly issued bonds.
Across the Atlantic, the chasm between the two major
political parties in the United States is as wide as ever, most recently evidenced by the failure of the Congressional “supercommittee” on
deficit reduction. Notwithstanding the ongoing paralysis in Washington, the U.S. economy is showing new signs of momentum.
We expect the global economy to deliver positive, if
uninspiring, growth into 2012 despite the euro zone’s debt problems. We believe interest rates should remain low in major advanced countries.
Markets — and risk assets in general — are likely to remain vulnerable to uncertainty until there is confidence that Europe has adequately
addressed its issues.
In times of uncertainty investors historically have turned
to sovereign credits, especially U.S. Treasury securities. This spotlights the critical role that governments play in defining the parameters of the
financial markets and serving as foundations for generating value within economies. The current trend toward fiscal austerity downplays this important
function and may prolong the global economic doldrums.
How should you respond to uncertainty in your own
investment program? Don’t try to time the markets. Keep your portfolio well diversified, and pay careful attention to the risks you are assuming.
Talk to your financial advisor before you make any changes that might detour your portfolio from your long-term goals.
We appreciate your continued confidence in ING Funds, and
we look forward to serving your investment needs in the future.
 
