Dear Shareholder: 
 
After an extraordinary career at Prudential, Judy Rice retired at the end of 2011 as President of Prudential Investments and President and Trustee of the Prudential Jennison 20/20 Focus Fund (the Fund). While she
will remain as Chairman of Prudential Investments until the end of 2012, I was named to succeed her as President of Prudential Investments and President and Trustee of the Fund effective January 1, 2012. I previously served as Executive Vice
President of Retail Mutual Fund Distribution for Prudential Investments for the past six years. 
 
Since this is my first letter to shareholders, I would like to recognize Judy for the significant contributions she made in building the Prudential Investments fund family and her unflagging commitment to helping
investors like you meet the challenges of a rapidly changing investment environment. My goal is to build on Judy’s accomplishments, with a particular focus on delivering the solutions you need to address your financial goals. 
 
I hope you find the annual report for the Fund informative. We recognize that ongoing
market volatility may make it a difficult time to be an investor. We continue to believe a prudent response to uncertainty is to maintain a diversified portfolio, including stock and bond mutual funds consistent with your tolerance for risk, time
horizon, and financial goals. 
 
Your financial professional can help you
create a diversified investment plan that reflects your personal investor profile and risk tolerance. Keep in mind that diversification and asset allocation strategies do not assure a profit or protect against loss in declining markets. We encourage
you to call your financial professional before making any investment decision. 
 
Prudential Investments provides a wide range of mutual funds to choose from that can help you make progress toward your financial goals. Our funds offer the experience, resources, and professional discipline of
Prudential Financial’s affiliated asset managers. Thank you for choosing the Prudential Investments family of mutual funds. 
 
Sincerely, 


