Dear Shareholder:
The fiscal year for the ICM Small Company Portfolio (the Portfolio or the Fund) was a tale of two halves: the first half saw the market (as measured by the S&amp;P
Index) rise by over only to see it fall by over in the second half. The swings in the small cap sector, as might be expected, were more pronounced, with the Russell
&reg; Index rising almost in the first half and pulling back by almost in the second half. In fact, the Russell
&reg; Index declined over for the calendar third quarter which was the fourth worst quarterly
performance for the Index since its inception in While the U.S. economy decelerated over the last six months of the Portfolios fiscal year, the main forces behind the sell-off had more to do with events in Washington, D.C. and Europe. In
July, Americans witnessed a political debate over the increase in the U.S. debt ceiling that took longer and was more politically acrimonious than we thought possible. This had a chilling effect on consumers, businesses and investors, and ultimately
led to the downgrade of the United States triple-A debt rating by Standard&amp; Poors (its highest rating). This was followed up in August with renewed fears that the European debt crisis was spreading and would leave European
banks insufficiently capitalized. We were encouraged that we were able to add value over
the benchmark in both the first half and second half of the fiscal year. As a result, for the fiscal year ended the Portfolio outperformed its primary benchmark, the Russell
&reg; Value Index.
























Total
Returns















Fiscal Year















Nov.

Feb.



Aug.









Jan.

Apr.



Oct.

Oct.

ICM Small Co. Portfolio*















Russell Value
Index















Russell
Index















Russell Growth
Index















S&amp;P Index























Portfolios Average Annual
Returns







year

years

years

Since Inception -







End

End

End

Thru





















* The returns shown for the ICM Small Company Portfolio are net of all fees and expenses.
Total annual Fund operating expenses are
Total returns assume reinvestment of all dividends and capital gains.
The performance data quoted represents past performance. Past performance does not guarantee future results. The
investment return and principal value of an investment will fluctuate so that an investors shares when redeemed may be worth more or less than their original cost and current performance may be lower or higher than the performance quoted. For
performance data current to the most recent moth end, please call or visit our website at www.icomd.com. Performance through is (one year), (five year) year).






Table of Contents










THE ADVISORS INNER CIRCLE FUND

ICMSMALLCOMPANY



PORTFOLIO





The tale of two halves was most pronounced in the Energy Sector. Although Energy remained the leader of the small cap sector for the fiscal year, it was the worst performing sector in the
second half of the fiscal year, declining almost While the Portfolio gave up performance during the first half of the fiscal year in this sector, we were able to get much of it back for the full year.
The Consumer Discretionary sector was the biggest contributor to Portfolios outperformance. The Portfolios
holdings in this sector returned over versus just for the benchmark. Stocks like Arbitron, Group Automotive, Mens Wearhouse and JOS A Bank Clothiers all had a significant positive impact on the Portfolio. We were able to add value
through strong stock selection in the Producer Durables sector where the Portfolios holdings returned over for the year versus a decline for the benchmark sector. Measurement Specialties, an industrial instrument manufacturer, and
aerospace companies Triumph Group and HEICO were notable contributors to the outperformance. Our underweight
of the relatively weak Financial Services sector was also a positive contributor to outperformance. Our stock selection was also strong, particularly the Portfolios bank holdings, which returned outperforming the benchmark counterparts
by more than basis points. Although the Portfolios Utilities holdings held up well relative to
the benchmark, our large underweight position in the sector hurt performance. We tend to have a negative bias towards Utilities as we believe their profits are artificially set by government, never mind the fact that the market seems to be
overpaying right now for their perceived safety. The Portfolios largest detractor from performance was
in the Materials&amp; Processing sector, which was down versus a positive advance for the benchmark. A meaningful portion of this underperformance can be attributed to the Portfolios significant position in Ferro, which
declined Ferro is a producer of specialty materials and chemicals for a wide range of industries. In recent years, the company has developed a strong franchise in materials for the electronic and solar markets, the latter of which grew to
of its sales in The stock has unfairly been caught up in the recent sell-off in providers of solar components and systems, and now trades at just six times conservative estimates for this years earnings. We used the recent decline in the
companys share price to increase our position. Given the events of the last few months, the risk of
another recession has clearly grown. But in most cases, the companies we hear from are not yet seeing the type of business slowdown being anticipated by the stock market. Even if we were to experience an economic contraction, corporate balance
sheets have strengthened since






Table of Contents










THE ADVISORS INNER CIRCLE FUND

ICMSMALLCOMPANY



PORTFOLIO





the last recession and should be better able to withstand a downturn in business. For these reasons, companies and insiders are increasingly taking advantage of the market decline to
purchase their own shares. With the S&amp;P Index trading at just times estimates, we believe the
market is pricing in most of the downside risks. In fact, the divergent performance between the stock market and the Treasury market has created a rare situation where the dividend yield on the S&amp;P now exceeds the yield of the Year U.S.
Treasury. According to Standard&amp; Poors, this has happened only times in the past years and, on average, has been followed by a rise in the S&amp;P over the next months. Of course, in the near term, much depends on
the ability of the European Union to contain the debt crisis in that region. Absent a resolution to this problem, we can expect continued volatility in the equity market. In the meantime, we are finding very good values and maintaining our focus on
investing in quality small cap companies that have market leading positions, solid balance sheets, and the ability to generate strong cash flow even in a softer economic environment.
Respectfully,
