Dear Shareholders: 
We are indeed living through some volatile times. Economic uncertainty is everywhere, as it seems no place in the world has been unmoved by crisis. We
have seen a devastating earthquake and tsunami that have led to disruptions in the Japanese markets and supply chains. Protests have changed the face of the Middle East and left in their wake lingering tensions and resultant higher oil prices. We
have seen debt limits tested in Europe and the United States and policymakers grappling to craft often unpopular monetary and fiscal responses at a time when consumers and businesses struggle with what appears to be a slowing global economy. On top
of all of that, we have seen long-term U.S. debt lose its Standard &amp; Poor’s AAA rating and the long-term debt ratings of 15 eurozone nations put on negative watch. 
When markets become volatile, managing risk becomes a top priority for investors and their advisors. At MFS® risk management is foremost in our minds in all market climates. Our analysts and portfolio
managers keep risks firmly in mind when evaluating securities. Additionally, we have a team of quantitative analysts that measures and assesses the risk profiles of our portfolios and securities on an ongoing basis. The chief investment risk
officer, who oversees the team, reports directly to the firm’s president and chief investment officer so the risk associated with each portfolio can be assessed objectively and independently of the portfolio management team. 
As always, we continue to be mindful of the many economic challenges faced at the local, national, and international levels. It is in times such as these
that we want to remind investors of the merits of maintaining a long-term view, adhering to basic investing principles such as asset allocation and diversification, and working closely with their advisors to research and identify appropriate
investment opportunities. 
Respectfully, 


