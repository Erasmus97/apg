Dear Shareholders,
From around the globe to within U.S. borders, many events caused emotions to run high in We saw challenges that not only affected equity and credit markets as well as our investments in them
but also changed many peoples lives in profound ways. Among the headlines that had significant effects on investors were the
political upheaval in the Middle East and North Africa, Japans earthquake and related nuclear disaster, controversy over U.S. troop withdrawal from Iraq, deterioration of the European economy, U.S. Congressional gridlocks over everything from
the debt ceiling to tax breaks, the Occupy Wall Street movement and less-than-inspiring economic and jobs growth numbers domestically. Despite all these challenges, the broader stock market ended the year relatively unchanged while the government
bond market, as represented by the Barclays Capital Year U.S. TIPS Index, appreciated by Its easy to see how uncertainty in
the markets fueled by challenging events could create an emotional response among investors. However, also reminded investors that maintaining a consistent, well-balanced investment strategy can protect against short-sighted
actions fueled by emotions that can jeopardize long-term goals. The long-term-focused, conservative management applied to the
American Beacon Treasury Inflation Protected Securities (TIPS) Fund, which invests primarily in high-quality inflation-protected government securities, provided investors with a relative bright spot in an otherwise volatile (but
ultimately flat) year overall. Over the period ended the American Beacon Treasury Inflation Protected Securities Fund (Institutional Class) returned
The year to come will surely provide its own list of unexpected national and world events. American Beacon remains focused on seeking opportunities and meeting market challenges to deliver the type of
consistency in performance and service our shareholders value. Thank you for your continued investment in the American Beacon Funds. To
obtain further details about the growing number of investment opportunities now available within the American Beacon Funds family or to access your account information, please visit our website at www.americanbeaconfunds.com.









Best Regards,

Dear Shareholders,
From around the globe to within U.S. borders, many events caused emotions to run high in We saw challenges that not only affected equity and credit markets as well as our investments in them
but also changed many peoples lives in profound ways. Among the headlines that had significant effects on investors were the
political upheaval in the Middle East and North Africa, Japans earthquake and related nuclear disaster, controversy over U.S. troop withdrawal from Iraq, deterioration of the European economy, U.S. Congressional gridlocks over everything from
the debt ceiling to tax breaks, the Occupy Wall Street movement and less-than-inspiring economic and jobs growth numbers domestically.
Its easy to see how uncertainty in the markets fueled by these events could create an emotional response among investors. However,
also reminded investors that maintaining a consistent, well-balanced investment strategy can protect against short-sighted actions fueled by emotions that can jeopardize long-term goals.
Volatility both domestically and internationally caused most equity markets to end the year flat or down. Over the period ended
the American Beacon S&amp;P Index Fund (Institutional Class) returned The American Beacon Small Cap Index Fund (Institutional Class) returned over the same period. And the American Beacon International
Equity Index Fund (Institutional Class) returned These returns were generally in line with the performance of each Funds respective Index.
The year to come will surely provide its own list of unexpected national and world events. American Beacon remains focused on seeking opportunities and meeting market challenges to deliver the type of
consistency in performance and service our shareholders value. Thank you for your continued investment in the American Beacon Funds. To
obtain further details about the growing number of investment opportunities now available within the American Beacon Funds family or to access your account information, please visit our website at www.americanbeaconfunds.com.









Best regards,
