Dear Fellow Shareholders:
 
January 23, 2012
 
 
 
Welcome to your annual report for the 12-month period ending December 31, 2011. The year 2011 seemed like a long one and we are glad to put it behind us. We witnessed a tsunami that ravaged Japan, a U.S. deficit ceiling fiasco, a sovereign debt crisis in Europe and an unparalleled credit downgrade for the U.S. Government. Despite record market volatility, the S&P 500 Index was basically flat in 2011 with the income component resulting in a 2.09% return. In contrast, the decline to record low Treasury yields translated into solid gains in the fixed income market. Treasuries with 1-3 year durations gained approximately 1.35% while 7-10 year durations posted returns of about 15.00%.
Looking across industry sectors, there was a considerable dispersion of investment returns over the past year. For equity investors, bright spots mostly came from defensive sectors including the utilities, consumer staples and healthcare industries. Conversely, financials, materials and industrials were the worst performing sectors.
Many fixed income investors fell short of the aggregate bond index returns, as their expectations that rates would rise on higher inflation and economic growth levels proved premature. Instead, renewed fears of weak global economic growth, the Euro debt crisis and the ultra-low interest policy of the Federal Reserve resulted in a rally in the bond market.
U.S. Economy Grinding Along.
During 2011, the U.S. Gross Domestic Product managed to expand at a growth rate of 1.8%. Economic growth appeared to pick up slightly in the second half of the year as manufacturing and industrial production growth held up relatively well. The most encouraging development last year was a decrease in the unemployment rates from 9.4% to 8.6% and early signs of a bottom in the housing market. This helped boost consumer confidence and spending.
As we look forward to 2012, we are cautiously optimistic. We are starting the year with unemployment at its lowest level in 2½ years, housing sales at a 1½ year high, moderate inflation levels and corporate earnings generally exceeding expectations. The S&P 500 Index is currently valued at a price earnings ratio of 12 times 2012 earnings estimates, which is at the low end of valuation levels when looking back over the past twenty years. We expect investors to experience above average market volatility as they will be sensitive to geopolitical and global macroeconomic issues.
The biggest threat to improving U.S. growth may come from overseas. The European debt situation may intensify near-term as the ECU attempts to shore up the sovereign debt crisis. Other potential wildcards are the possibility of the Federal Reserve initiating another round of quantitative easing and issues relating to the upcoming Presidential election.
Following you will find detailed information that highlights each Fund’s performance, security holdings and 2012 investment strategy for your review. We thank you for being a valued shareholder and providing us with the opportunity to help you achieve your investment goals.
Respectfully,


