Dear Shareholder:

Following a year marked by unusually high volatility, began with most major asset classes advancing steadily in January. For U.S. equities, it
was the strongest start to a new year since International stocks fared even better, despite continued uncertainty related to the sovereign debt
crisis in Europe. Investors have been acutely sensitive to the latest news, for better or worse, coming out of the eurozone and its impact on financial markets. As we look ahead, the unresolved debt crisis in Europe remains at the center of a series of risk factors, summarized below, that we
believe have the greatest potential to influence the global investment landscape.

Deleveraging and the economy

In the euro-currency area, fiscal austerity among nations and debt deleveraging among financial companies loaded with sovereign debt are deflationary measures and serve to hinder economic growth in the short term. Such an economic and financial-market scenario has not been historically
supportive of strong performance among riskier assets, and emerges at a time when many nations need a resurgent economy to assist them in closing their budget deficits and in building confidence among bond buyers to help them refinance their existing debt obligations.

Slowdown in China and Europe

China's economy is the second-largest in the world, and it has been the biggest contributor to global growth since the end of the last recession.
Thus, the slower pace of domestic growth in China has led to lower demand for imports of commodities and other construction materials from the
rest of the world. In addition, economic weakness in Europe and the broad-based global economic slowdown are putting pressure on China's export
growth, which has been largely responsible for its breakneck pace of annual gross domestic product (GDP) growth during the past three decades.

Credit deterioration and contagion

The heightened macroeconomic risk and elevated credit risk swirling around certain European nations and financial institutions have caused many
market participants to avoid purchases of or reduce exposure to short-term debt offerings by these issuers. With increased credit risk, there are
growing concerns about the potential credit contraction and contagion from European issuers spreading to other financial markets.

We invite you to learn more by visiting us on the Internet or calling us by phone. It is our privilege to provide the resources you need to choose
investments that are right for you.

Sincerely,

Dear Shareholder:

Following a year marked by unusually high volatility, began with most major asset classes advancing steadily in January. For U.S. equities, it
was the strongest start to a new year since International stocks fared even better, despite continued uncertainty related to the sovereign debt
crisis in Europe. Investors have been acutely sensitive to the latest news, for better or worse, coming out of the eurozone and its impact on financial markets. As we look ahead, the unresolved debt crisis in Europe remains at the center of a series of risk factors, summarized below, that we
believe have the greatest potential to influence the global investment landscape.

Deleveraging and the economy

In the euro-currency area, fiscal austerity among nations and debt deleveraging among financial companies loaded with sovereign debt are deflationary measures and serve to hinder economic growth in the short term. Such an economic and financial-market scenario has not been historically
supportive of strong performance among riskier assets, and emerges at a time when many nations need a resurgent economy to assist them in closing their budget deficits and in building confidence among bond buyers to help them refinance their existing debt obligations.

Slowdown in China and Europe

China's economy is the second-largest in the world, and it has been the biggest contributor to global growth since the end of the last recession.
Thus, the slower pace of domestic growth in China has led to lower demand for imports of commodities and other construction materials from the
rest of the world. In addition, economic weakness in Europe and the broad-based global economic slowdown are putting pressure on China's export
growth, which has been largely responsible for its breakneck pace of annual gross domestic product (GDP) growth during the past three decades.

Credit deterioration and contagion

The heightened macroeconomic risk and elevated credit risk swirling around certain European nations and financial institutions have caused many
market participants to avoid purchases of or reduce exposure to short-term debt offerings by these issuers. With increased credit risk, there are
growing concerns about the potential credit contraction and contagion from European issuers spreading to other financial markets.

We invite you to learn more by visiting us on the Internet or calling us by phone. It is our privilege to provide the resources you need to choose
investments that are right for you.

Sincerely,
