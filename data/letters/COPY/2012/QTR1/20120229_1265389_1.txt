Dear Shareowner,

Last year was difficult for investors, although bond investors experienced a
much smoother ride than equity investors. The year was characterized by
higher-than-usual volatility in the equity markets triggered by a series of
major events, including the nuclear disaster in Japan, the European financial
crisis, and political gridlock in Washington D.C. The Standard & Poor's
Index dropped from its May high to its October low, only to finish the year
virtually flat. The Barclays Capital Aggregate Bond Index, by contrast, was up
for the year.

As we ended some positive trends were developing in the U.S. economy.
Consumption, the most important driver of economic growth, rose in the
fourth quarter, up nicely from in the second and third quarters. Year-
over-year retail sales grew over the last six months of and auto sales
reached their highest level of the year. There were improvements in broad
economic data, including employment and rising consumer confidence. Initial
jobless claims trended in the right direction, with the final weekly report of
falling to a three-and-a-half year low.

While we expect moderate economic growth in the U.S. in there are still
reasons for investors to remain cautious. The central issue remains Europe,
which faces weak or possibly declining economic growth. The greatest risk to
our outlook for is the possible contagion effects of the European
sovereign-debt and banking crises. The European Union must find a comprehensive
solution that includes ensuring funding for troubled sovereigns, achieving
workable fiscal and economic integration, and improving labor competitiveness
in southern Europe. Further setbacks in Europe could lead to further market
volatility, while tangible progress could help the equity markets make up last
year's lackluster results.

Pioneer's investment professionals focus on finding good opportunities to
invest in both equity and bond markets using the same disciplined investment
approach we have used since Our strategy is to identify undervalued
individual securities with the greatest potential for success, carefully
weighing risk against reward. Our teams of investment professionals continually
monitor and analyze the relative valuations of different sectors and securities



Pioneer AMT-Free Municipal Fund | Annual Report |


globally to help build portfolios that we believe can help you achieve your
investment goals.

At Pioneer, we have long advocated the benefits of staying diversified* and
investing for the long term. The strategy has generally performed well for many
investors. Our advice, as always, is to work closely with a trusted financial
advisor to discuss your goals and work together to develop an investment
strategy that meets your individual needs. There is no single best strategy
that works for every investor.

We invite you to learn more about Pioneer and our time-tested approach to
investing by consulting with your financial advisor or visiting us online at
us.pioneerinvestments.com. We greatly appreciate your trust in us and we thank
you for investing with Pioneer.

*Diversification does not assure a profit or protect against loss in a
declining market.

Sincerely,

Dear Shareowner,

Last year was difficult for investors, although bond investors experienced a
much smoother ride than equity investors. The year was characterized by
higher-than-usual volatility in the equity markets triggered by a series of
major events, including the nuclear disaster in Japan, the European financial
crisis, and political gridlock in Washington D.C. The Standard & Poor's
Index dropped from its May high to its October low, only to finish the year
virtually flat. The Barclays Capital Aggregate Bond Index, by contrast, was up
for the year.

As we ended some positive trends were developing in the U.S. economy.
Consumption, the most important driver of economic growth, rose in the
fourth quarter, up nicely from in the second and third quarters. Year-
over-year retail sales grew over the last six months of and auto sales
reached their highest level of the year. There were improvements in broad
economic data, including employment and rising consumer confidence. Initial
jobless claims trended in the right direction, with the final weekly report of
falling to a three-and-a-half year low.

While we expect moderate economic growth in the U.S. in there are still
reasons for investors to remain cautious. The central issue remains Europe,
which faces weak or possibly declining economic growth. The greatest risk to our
outlook for is the possible contagion effects of the European
sovereign-debt and banking crises. The European Union must find a comprehensive
solution that includes ensuring funding for troubled sovereigns, achieving
workable fiscal and economic integration, and improving labor competitiveness in
southern Europe. Further setbacks in Europe could lead to further market
volatility, while tangible progress could help the equity markets make up last
year's lackluster results.

Pioneer's investment professionals focus on finding good opportunities to invest
in both equity and bond markets using the same disciplined investment approach
we have used since Our strategy is to identify undervalued individual
securities with the greatest potential for success, carefully weighing risk
against reward. Our teams of investment professionals continually monitor and
analyze the relative valuations of different sectors and securities

Pioneer Growth Opportunities Fund | Annual Report |


globally to help build portfolios that we believe can help you achieve your
investment goals.

At Pioneer, we have long advocated the benefits of staying diversified* and
investing for the long term. The strategy has generally performed well for many
investors. Our advice, as always, is to work closely with a trusted financial
advisor to discuss your goals and work together to develop an investment
strategy that meets your individual needs. There is no single best strategy that
works for every investor.

We invite you to learn more about Pioneer and our time-tested approach to
investing by consulting with your financial advisor or visiting us online at
us.pioneerinvestments.com. We greatly appreciate your trust in us and we thank
you for investing with Pioneer.

* Diversification does not assure a profit or protect against loss in a
declining market.

Sincerely,
