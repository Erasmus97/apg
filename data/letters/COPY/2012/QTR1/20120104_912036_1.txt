Dear Fellow Shareholder:


The past year will undoubtedly be remembered as one of the most uncertain and volatile investment
environments in modern history. Headline risk was widespread and the list of worries was long. From
political unrest in the Middle East, to the earthquake, tsunami and nuclear crisis in Japan, to the
debt issues of developed nations and recent deceleration of the global economy, markets were
buffeted by a seemingly unrelenting series of unsettling developments.


Yet markets endured. U.S. stock prices ended the period ending solidly
higher, with gains across market capitalization and style segments. U.S. bondsled by
government-backed securitiesalso fared reasonably well, especially in light of Standard Poors
decision to downgrade our countrys long-term credit rating. The Standard Poors Index, a
broad gauge of the U.S. stock market, gained during the period, while U.S.
investment-grade bonds, as measured by the Barclays Capital U.S. Aggregate Bond Index, advanced
Although foreign equity markets produced significantly divergent results for the year, many
proved surprisingly resilient.


As we look out on the coming year, we believe these words from Abraham Lincoln, who led our
nation during one of its more troubled periods, serve as a useful guide to navigating what may
lie ahead. Be sure to plant your feet in the right place, then stand firm.


Lincolns wisdom echoes some of the basic tenets of successful investing. Historically, the
performance of different types of asset categories has varied widely from year to year and the
magnitude of returns can vary significantly among asset classes in any given year, even among asset
classes that are moving in the same direction. This rotation of asset classes underscores why
investors should consider planting their feet in a diversified portfolio. At the same time,
investors who stand firm in that approach may be able to avoid the problems associated with market
timing, which can result in arriving late as an asset class is rallying or overstaying their
welcome in an asset class after performance peaks.


At Aston Funds, our commitment to standing firm is best demonstrated through our abiding commitment
to adhere to highly disciplined investment processes. This approach has sustained our resolve in
the face of market volatility and macroeconomic uncertainty and, in the process, bolstered our
reputation as a leader in subadvised mutual funds managed by boutique investment firms. Our
managers follow highly structured investment processes with clear valuation metrics designed to
achieve strategy or style consistency. While there are no sure things in investing, we believe that
a disciplined investment process should lead to consistent long-term investment performance.


We are pleased to present you with the Aston Funds annual report. As always, we are grateful to our
fellow shareholders who have planted their feet here and continue to stand firm with us.


Sincerely,
