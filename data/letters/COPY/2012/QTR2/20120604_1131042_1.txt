Dear
Shareholder:
SM 
Market Review
1 
During
2011, companies were positioned relatively strongly, having both built up
significant cash balances and restructured costs to improve operating leverage.
Rather than reflecting weakness at the company level, market volatility was
driven by a number of macro events that can be traced back to the March tsunami
and earthquake in Japan, which sparked concerns regarding the supply chain and
general production. And while the US debt ceiling and downgrade of US debt also
made headlines as the year unfolded, the resurgence of the European debt crisis
was the tipping point for the downward change in market direction. The US stock
market dropped precipitously beginning in July as concern about Europe led
investors to seek the safety of US Treasuries in the late summer and early
fall. Historically high levels of volatility continued through September.
Then,
beginning October 3, the markets saw a rapid rebound with risk appetite taking
off and fundamentals starting to be rewarded once again. However, it wasn’t all
smooth sailing, as the last calendar quarter of 2011 also saw the market
hesitate at points in time and pull back a little due to over-optimism before
continuing to move upward with generally more positive economic reports. The
labor market continued to improve as employers added more jobs and the
unemployment rate steadily declined to 8.3% as of March 2012. Moreover, stocks
generally responded favorably to the Federal Reserve’s comments and stance to
keep interest rates at their current low levels through late 2014. Despite this
more positive view on the economy overall, the Federal Reserve noted that the unemployment
rate still stood at an “elevated” level, while risks remained in the global
financial markets.
International
stocks had a similar experience to that of US equities, but with a greater
downswing and a more muted upswing leading to an overall negative return of
-6.75% for the MSCI All Country World Index (ACWI) ex-US for the 12 months
ended March 31, 2012. The MSCI All Country World Index (ACWI) ex-US returned
-19.45% for the six months ended September 30, 2011, followed by a six-month
return of 15.55% through the end of March 2012. The European region was a large
driver of ongoing concerns globally about sovereign debt, Greek default and
contagion effects into other southern European countries. Additionally,
emerging markets faced higher inflation and slowing growth leading to weakness
in the key markets of China, Russia and India. Emerging market central banks
took policy action to curtail the high level of inflation, and the European
Central Bank provided liquidity to European banks, alleviating the immediate
concerns regarding the potential for a renewed financial crisis. While these
risks have receded somewhat, slowing growth in both developed and emerging
markets and austerity measures across a number of European countries suggest
that some longer-term challenges remain.
Real
estate, commodities and natural resources—similar to equities—all saw negative
returns for the first six months of the reporting period due to
flight-to-quality pressures, a potential real estate bust in China and slowing economic
growth. Real estate, as represented by the FTSE NAREIT All Equity Index, rose
by 27.35% in the subsequent six months helped by higher demand for apartment
units and investor appetite for higher yields. For the full 12-month period the
FTSE REITs Index rose 11.29%. Commodities didn’t see the same level of rebound,
with the Dow Jones-UBS Commodity Index providing a 1.23% return for the six
months ended March 2012, and
a
12-month return of -16.28%. The same held true for global natural resources, as
the S&amp;P Global Natural Resources Index delivered a return of 15.47% for the
six months ended March 2012, and a 12-month return of -14.33%. The slowing of
growth in emerging markets, and in China in particular, coincided with a lower
demand for base metals and materials at the end of 2011 and more recently. At
the same time, concerns over Iran’s weapons program drove oil prices higher, as
natural gas prices dropped to historic lows on oversupply issues.
The
fixed income markets saw a different pattern in returns, with strength being
seen in the first half of the 12-month period. While the latter half of the
reporting period saw positive returns, they were much lower than for the first
half. The Barclays US Aggregate Bond Index garnered a return of 7.71% for the
12 months ended March 31, 2012, but it was frontloaded with a return of 6.26%
recorded for the first six-month period. Investors favored the risk-off trade
as growing concerns about the financial crisis in Europe and panic set into the
market, seeking the perceived safety of US Treasuries and government-related
securities, which together comprise over 50% of the index. But following the
equity market low established on October 3, performance cooled in the bond
markets and the Barclays US Aggregate Bond Index returned 1.43% over the six
months ended March 31, 2012.
The
Barclays US Treasury US TIPS Index ended the 12-month period with an impressive
return of 12.20%. Much of this return was achieved during the first six months
when investors sought the safety of US government securities. However, concerns
about potential inflation with rising commodity prices also led to a positive
return of 3.57% for the six months ended March 2012. As was the case with
equity markets, international fixed income markets saw weaker returns. The
Barclays Multiverse Index, which is inclusive of all fixed income markets
including the US, returned 5.28% for the 12 months ended March 2012, with the
majority of the return coming in the first six-month period when the index
returned 3.72%. A key reason for the better relative returns in the US was the
strength of the US dollar against most other currencies. While experiencing
some weakness for the six months ended March 2012, the strength of the dollar
during the summer of 2011-due to the flight to safety—softened the returns from
international markets.
The
municipal markets saw similar trends to those of the US taxable bond markets,
with a stronger first six months than second. The Barclays Municipal Bond Index
returned 12.07% for the full 12 months with the first six months providing a
return of 7.49%. Despite concerns about state and local government budgets,
limited issuance in the municipal market and growing demand boosted the returns
in this market. Issuance increased in the latter part of 2011 and, with lower
demand as investor risk appetite increased, the returns for the municipal
market slowed and the Barclays Municipal Bond Index returned 3.91% for the
second six-month period.
GuideMark and
GuidePath Fund Review
In
addition to the headwinds of a risk-off environment where macro events and
political announcements drove the markets rather than company fundamentals, the
sub-advisors in the GuideMark Funds also confronted unusually high levels of
correlation between and within markets, little dispersion between individual
security returns, and extreme volatility. This exceptional environment was not
conducive to active management. Not surprisingly, Standard and Poor’s reported
in its SPIVA US Scorecard Year-End 2011 (www.standardandpoors.com) that an
unusually high 84% of actively managed domestic equity funds underperformed the
broad US market as measured by the S&amp;P Composite 1500 Index in 2011.
However, as the market established a low on October 3 and macro concerns
receded, correlations and volatility declined to more normal levels and strong
company fundamentals were again rewarded.
As
could be expected in the difficult environment for active managers during the
first half of the reporting period, many of the GuideMark Funds lagged their
benchmarks. However, for the six months ended March 31, 2012 nearly all of the
GuideMark Funds outperformed their benchmarks as effective security selection
became a strong driver of positive returns.
The
GuidePath Funds also experienced some challenges over the past year with the
strength of the US markets relative to international markets. The globally
based mandates of the GuidePath Funds put them at a disadvantage when compared
solely to a US-based Index. However, when compared to globally based indices,
the Funds’ performance was more in line with expectations.
2
Looking Ahead
As
we look ahead, we foresee a reasonable possibility that there may be a
consolidation period for stocks. Previous cyclical bull market rallies have
often been accompanied by pullbacks between six and nine months following a
market upturn, and we feel that there are some indications that this could
repeat. Given some elevated earnings targets, we anticipate that it is possible
companies could guide expectations lower for the second half of the year in the
near term, which could contribute to a correction. However, should there be a
correction, we believe that it could eventually build strength for a
longer-term bull market cycle, particularly if companies are able to deliver
earnings-per-share growth—helped by some resilience in margins and share
repurchases.
Whether
the market rallies or experiences a correction, we continue to believe that a
balanced approach to portfolio construction will allow you to not only benefit from
positive market returns but, more importantly, allow you to limit the losses in
falling markets.
Please
contact your financial advisor to discuss any questions about your investment
strategy or changes in your financial goals. We thank you for including the
Funds in your portfolio and appreciate the trust you have placed in us.
 
Sincerely,


