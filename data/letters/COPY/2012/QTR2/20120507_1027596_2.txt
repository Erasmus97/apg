Dear Fellow Shareholders,



It is our pleasure to provide you with the American Trust Allegiance Funds Annual Report for the months ended February



The Allegiance Fund just completed its year with strong performance.We believe this should give pause to people who think one cannot invest ethically and still perform well.If you had invested in the Allegiance Fund at its inception (March on February it would have been worth invested in the benchmark S&amp;P Index, it would have been worth and if invested in our Morningstar peer group (large cap growth mutual funds), it would have been worth want you to know that we did not amass this significant growth through taking excessive risk, but instead by focusing on good quality companies with strong balance sheets, generally low debt and good management.We also feel that not only did the Allegiance Funds screens not hinder performance, but that they contributed to the Funds long-term performance.



During the months ended February the general stock market, as measured by the S&amp;P Index, was up as compared to your Allegiance Fund which was up over the past months ending February the S&amp;P Index was up while your Allegiance Fund was up



Why has the Allegiance Funds performance been so strong?Over the last several years, the stock market has exhibited markedly different performance based on the sectors and geographies in which one is invested.One side of the market has suffered with housing, building, banking and related stocks.The other side has fared far better, driven by strong earnings from such sectors as information technology and telecommunication services.Our exposure to these two sectors, in particular, drove our better than market performance during the Funds most recent fiscal year.Stocks like Apple, Inc. and Brazilian telecommunications giant, Telecomunicacoes de Sao Paulo, were standout performers in our portfolio.We have generally avoided stocks in the materials, industrials and financials sectors, such as banks and home-builders, though the portfolio holds CBRE Group and Ford Motor Co., which have detracted from the Funds performance.We believe that their day will come, but recently they have served only to add volatility.



We should note that all performance data referenced above is after fees.This means that the data reflects what you, the shareholder, actually receive.



Many have asked whether the stock markets significant rally from the bottom in March of means it is too expensive now.We believe the answer is no.For instance, technology stocks have continued to grow their earnings almost as fast as their stock price thereby keeping such stocks reasonably priced.






















American Trust Allegiance Fund




The broadest and most well known measure of value in the stock market is the price/earnings multiple which stands at only on trailing months earnings according to the S&amp;P Index.That is the average over the last years, so it would be difficult to argue stocks are expensive on that basis.In fact, with interest rates still quite low, many would argue that stocks deserve a price-to-earnings ratio of closer to



Valuing stocks cannot be done in a vacuum.In particular, they should be assessed versus the alternatives.Based on our review of data over the past years, there has almost never been a moment when stocks look so much more attractive than bonds as they do now.In fact, recently the yield on the stocks in the S&amp;P Index has exceeded the yield on the U.S. Treasury bond.



This suggests that investors remain cautious overall, and are likely underweighted in stocks and overweighted in bonds.We hope that our investors recognize that following the crowd is typically a poor maxim for investment performance, and that an analytical approach to relative value is a better approach than being driven by fear and greed.For those who would like to discuss the relative merits of stocks versus bonds in more detail, we invite you to call us for a more fulsome discussion than space permits in this report.



There are, of course, plenty of challenges to be dealt with, including the European Union debt issues, the viability of the Euro, Chinese over-expansion and U.S. budgetary wrangling.Yet, all these were with us last summer when the stock market reacted as though the EU would drag the U.S. into a double-dip recession.The stock market has rallied enormously since then and today the perception is changing to the possibility that the U.S. recovery will help the EU out of its economic woes - thus the importance of focusing on what may happen in the future and not on the consternations of the moment.



Looking forward, we see the U.S. economy improving as Americans get back to work.For the last months, employment has increased more than per month.At some point, job increases should lead to a stronger housing market and the worst post-war housing recession should begin to recover.Cash on corporate balance sheets is at a record high and corporate earnings have continued to show strength along with corporate balance sheets.



This is the kind of atmosphere that is usually conducive to stock growth and we will continue to look for opportunities in this improving economy.



Most importantly, we are grateful to you for your support of the American Trust Allegiance Fund and we hope that, in return, we can help you meet your financial goals.



Sincerely yours,
