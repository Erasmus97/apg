Dear Shareholder, 
What an interesting year 2011 turned out to be. As a result of all of the uncertainty in Europe, the Middle East and the U.S., there was considerable volatility in the markets during 2011, as one would
expect, yet somehow stock market returns for the year were basically flat. The S&amp;P 500 managed to eke out a small gain of 2.1% due mostly to better economic news and the resulting stock market rally in the fourth quarter. We hope that this trend
continues throughout 2012 and believe that once again active managers, such as ourselves, will be able to add value through superior stock selection. We look forward to helping you meet the challenges on your horizon with experience, strength,
stability and integrity. 
Burkenroad Small Cap Fund Celebrates 10-Year Anniversary with Top Morningstar Rating 
On December 31, 2011, the Hancock Horizon Burkenroad Small Cap Fund celebrated its 10-year anniversary and received the top rating of 5 stars from
Morningstar for the 5-year, 10-year and overall periods. The Fund also ranked in the top one percent amongst small cap core funds for the 10-year period ending December 31, 2011, according to Lipper Analytical – the Fund was the second
best performer of 303 funds in this category. Since its inception, the Burkenroad Small Cap Fund, Class A has experienced a 10.59 percent annualized return as compared to its benchmark, the Russell 2000’s return of 5.62 percent per year
for the same time period. 
The Burkenroad Small Cap Fund is just one of several Hancock Horizon Funds to earn the distinguished

overall five-star Morningstar rating. The Hancock Horizon Diversified International Fund also received the
top Morningstar rating, and the Hancock Horizon Value Fund received a five-star rating for its 10-year period. For the period ending January 31, 2012, the Hancock Horizon Quantitative Long/Short Fund received an overall rating of four-stars.

Diversified International Fund More than Doubles in Size 
The Hancock Horizon Diversified International Fund had an increase of 129% in assets under management for 2011. The international markets, and as result international funds, had a rough year as a whole,
so the growth in assets is a welcome exception. For the 3-year time period ending on December 31, 2011, the Hancock Horizon Diversified International Fund was in the top 1% of Morningstar’s foreign large blend category. 
Hancock Horizon Funds Launched Two New Tax-Free Funds 
The Hancock Horizon Louisiana and Mississippi Tax-Free Income Funds were launched on February 1, 2011. As the names imply, both funds’ goals are to seek income exempt from both federal and state
specific personal income tax. Since their inception, both funds have outperformed their benchmarks for the time period ending January 31, 2012. The Louisiana Tax-Free Income Fund returned 17.98% for the Trust Class and the Mississippi Tax-Free
Income Fund returned 17.32% for the Trust Class compared to Barclays Capital Municipal Bond Index’s return of 14.21 percent.

 
Table of Contents
 


