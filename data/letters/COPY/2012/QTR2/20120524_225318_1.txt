Dear Valued Shareholder,


I am pleased to present the Annual Shareholder Report for your fund covering the period from April through March This report includes Management's Discussion of Fund Performance, a complete listing of your fund's holdings,
performance information and financial statements along with other important fund information.


In addition, our website, FederatedInvestors.com, offers easy access to Federated resources that include timely fund updates, economic and market insights from our investment strategists, and financial planning tools. We invite you to register to
take full advantage of its capabilities.


Thank you for investing with Federated. I hope you find this information useful and look forward to keeping you informed.


Sincerely,
