Dear Shareholders,

We are pleased to present this annual update for the SunAmerica Income Funds
for the months ended March a period wherein volatility dominated
as Europe's sovereign debt crisis and concerns over the economic outlook and
the U.S. debt ceiling drove investors into the perceived safe haven of U.S.
Treasuries.

Some spread, or non-Treasury, sectors outperformed U.S. Treasuries during the
annual period. These included the commercial mortgage-backed securities and
emerging markets debt sectors, which generally had limited exposure to European
sovereign debt. The corporate sector, however, which had more exposure to
European financial troubles, lagged U.S. Treasuries for the annual period
overall. The Barclays U.S. Aggregate Bond Index/*/, a broad measure of the U.S.
fixed income market, returned for the months ended March

In the first three months of the annual period, disappointing U.S. economic
news, a deepening sovereign debt crisis in Europe, fears of contagion from
Europe's sovereign debt turmoil, and slowing economic growth in China caused
U.S. Treasuries to rally. Uncertainties over the end of the Federal
Reserve Board's (the "Fed's") quantitative easing program, as well as ongoing
negotiations in the U.S. Congress over the U.S. debt ceiling, further weighed
on investor sentiment. Meanwhile, Standard & Poor's lowered its outlook on U.S.
sovereign debt from stable to negative, citing disagreement among politicians
on how to address rising budget deficits and debt. All fixed income sectors
posted positive returns during the second quarter of helped by the
decline in global government bond yields.

Risk assets broadly sold off during the third quarter of Despite a second
bailout for Greece and efforts to expand the capabilities of the European
Financial Stability Facility, markets grew increasingly concerned about the
impact of a disorderly Greek default on the global financial system. In an
unprecedented move, the Fed pledged to keep benchmark interest rates at record
lows through the middle of Further, in a new stimulus measure dubbed
Operation Twist, the Fed announced plans to buy billion of longer-term
debt to be funded by selling an equal amount of shorter maturities in an effort
to put downward pressure on long-term interest rates and boost the economy.
High yield corporate bonds and emerging markets debt generated negative returns
during the quarter. Commercial mortgage-backed securities also lost ground,
impacted by limited risk appetite. U.S. Treasuries was the best performing
sector for the quarter, though mortgage-backed securities, asset-backed
securities and investment grade corporate bonds also generated positive returns.

Most fixed income sectors generated positive returns during the last months of
the calendar year. In a reversal from the prior three months, the rally was led
by high yield corporate bonds and emerging markets debt. Commercial
mortgage-backed securities and investment grade corporate bonds also performed
well. U.S. Treasuries had a modestly positive return, as safe haven demand
continued to drive yields lower. However, U.S. Treasuries lagged most riskier
assets, as U.S. economic data released during the fourth quarter of
largely pointed to an improving cycle. In Europe, extreme policy measures by
the European Central Bank, European Union and International Monetary Fund,
though widely regarded as positive moves, failed to restore confidence in
sovereign debt. In particular, the absence of a credible plan and vision for
fiscal union was a key point of vulnerability. In the U.S., the Fed maintained
its accommodative stance throughout the quarter, but the political stalemate in
the U.S. continued as lawmakers failed to reach an agreement on reducing the
nation's budget deficit.

As the new year began, the Fed maintained its accommodative stance and even
extended it by stating it would keep the Federal Funds Rate exceptionally low
until at least late By March Fed policy makers had upgraded their
assessment of the U.S. economy based on improving employment data and
diminishing strains in the global financial markets. U.S. Treasury yields rose
sharply in response to the Fed's statements, as market participants scaled back
expectations of a third round of quantitative easing. After protracted
negotiations, Eurozone officials finally approved a second rescue package for
Greece, and the European Central Bank injected another round of liquidity into
markets via its Long-Term Refinancing Operation, further reducing interbank
funding








pressures. Meanwhile, the Bank of Japan initiated aggressive policy easing. In
all, U.S. Treasuries was the weakest fixed income sector for the first quarter
of generating negative returns. High yield corporate bonds, emerging
markets debt, commercial mortgage-backed securities and investment grade
corporate bonds each generated positive returns for the quarter.

Against this backdrop, each of the SunAmerica Income Funds generated positive
returns during the annual period. On the following pages, you will find
detailed financial statements and portfolio information for each of the
SunAmerica Income Funds.

We thank you for being a part of the SunAmerica Income Funds. As we continue to
actively manage your assets, we value your ongoing confidence in us and look
forward to serving your investment needs in the future. As always, if you have
any questions regarding your investments, please contact your financial adviser
or get in touch us directly at or www.safunds.com.

Sincerely,
