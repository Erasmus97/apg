Dear Shareholder:

During the past fiscal year, equity markets continued their run of unpredictable behavior. Though the beginning of the period was
mostly flat, it did not take long before the economic environment started its bumpy ride. Shortly into the first six months of the
reporting period, stock correlations rose again and investors seemed to pay less attention to the positive news being reported by
individual companies, and more attention to the macro factors of the struggling economies. In particular, investors continued to be
apprehensive due to the debt concerns in Europe and the increasing potential of a global slowdown in growth.

However, by the end of investor sentiment turned decidedly positive. Overseas, markets started to turn around when a plan was
announced that would aim to create a stronger fiscal union and support the many struggling European banks. In the U.S., economic
data regarding employment growth and housing were on the upswing. Consumer spending was also more robust than in the recent past
and, in general, corporations were continuing to produce solid growth. For the most part, these favorable trends endured through the
fiscal year, which sent stocks higher. In fact, the S&amp;P a leading indicator of the U.S. stock market in general, reported one
of its best first-quarter returns in many years.

As we head into the remainder of the year, we anticipate smooth transitions for your Old Mutual Funds into mutual funds on the
Touchstone Advisors, Inc. platform and the FundVantage platform. As always, we are grateful for your support as an investor in the
Old Mutual Funds II portfolios. It has been our pleasure to serve you.

Sincerely,
