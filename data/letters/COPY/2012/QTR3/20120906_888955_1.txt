Dear Fellow Shareholder:



While we didnt necessarily coin the phrases, Invest in what you know or If you dont understand it, dont buy it, these concepts are certainly not new to Narragansett Insured Tax-Free Income Fund and the Aquila Group of Funds. In fact, they have been at the very core of our investment philosophy since day one.



Our countrys current volatile economic environment, coupled with fewer firms providing bond insurance, and fewer issuers willing to pay for it, make these adages ring true louder than ever.



The value provided by professional investment managers, such as those at Narragansett Insured Tax-Free Income Fund, who conduct initial research and provide on-going surveillance of issuers and individual bonds as markets develop and credit conditions change, has become increasingly important.



Over the past year or so, you may have read or heard about problems being experienced by certain municipalities including growing concerns over budget shortfalls, infrastructure demands, pension funding, and high unemployment.



Even when concerns such as these dont make the mainstream newspapers, it is in your best interest that your Funds investment team makes every effort to know about each and every little hiccup. And, since each municipality has its own distinct nuances, we feel it is vital to have a local presence.



Local investment management and research enables us to monitor the local economy, issuers in the state, and policy decisions that will impact issuers, while we conduct research on issues held by the Fund. The research conducted prior to investing in a bond, and ongoing credit monitoring, make it possible to evaluate both the risk associated with an individual bond, and the adequacy of the compensation provided for that risk.



Narragansett Insured Tax-Free Income Fund specifically benefits from its collective team of local Trustees and portfolio managers who seek to be intimately aware of any potential challenges facing the citizens of Rhode Island throughout the state.



They know the ups and downs that affect you, our shareholders, because they too are affected. Your local representatives are also your friends, neighbors and co-workers.



They hear the same discussions at little league games and pot luck dinners. They read the same small and big town newspapers that you do, shop in the same supermarkets and gas up at the same pumps.



NOT A PART OF THE ANNUAL REPORT

























They, like you, are Rhode Islanders.



As you know, by prospectus, Narragansett Insured Tax-Free Income Fund may only invest in investment grade securities. These higher rated securities are intended to indicate those municipal issues which have not only sufficient, but significant, cash flow strength in order to pay interest when due and to redeem the bonds at maturity. Nonetheless, we firmly believe in the importance of looking beyond credit ratings.



We invest in an issue based on our initial research, and we conduct frequent credit monitoring in order to evaluate the financial condition of the issuer. We devote significant resources to understanding the financial condition of issuers in Rhode Island, the financing details of individual issues, and how payments of principal and interest on those issues are secured. We monitor the difficult, but necessary, steps being taken to balance budgets within the state. Based on the research we conduct, we select the bonds held in the Funds portfolio and decide whether or not to continue holding issues already in the portfolio.



The Aquila Group of Funds has been managing the assets of Rhode Island investors for over years. Our long history in the Rhode Island market, the knowledge and experience of the Funds portfolio managers, and the research conducted on bonds held in Narragansett Insured Tax-Free Income Fund continue to provide shareholders with the benefits of local, professional investment management.



Sincerely,
