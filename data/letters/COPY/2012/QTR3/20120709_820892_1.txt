Dear Shareholders, 
Investors have many reasons to remain cautious. The challenges in the Euro area are casting a shadow over global economies and financial markets. The political support for addressing fiscal issues is eroding as the
economic and social impacts become more visible. At the same time, member nations appear unwilling to provide adequate financial support or to surrender sufficient sovereignty to strengthen the banks or unify the Euro area financial system. The
gains made in reducing deficits, and the hard-won progress on winning popular acceptance of the need for economic austerity, are at risk. To their credit, European political leaders press on to find compromise solutions, but there is increasing
concern that time will begin to run out. 
In the U.S., strong corporate earnings have enabled the equity markets to withstand much of the downward
pressures coming from weakening job creation, slower economic growth and political uncertainty. The Fed remains committed to low interest rates but has refrained from predicting another program of quantitative easing unless economic growth were to
weaken significantly or the threat of recession appears on the horizon. Pre-election maneuvering has added to the already highly partisan atmosphere in the Congress. The end of the Bush-era tax cuts and implementation of the spending restrictions of
the Budget Control Act of 2011, both scheduled to take place at year-end, loom closer. 
During the last year, U.S. based investors have experienced a
sharp decline and a strong recovery in the equity markets. The experienced investment teams at Nuveen keep their eye on a longer time horizon and use their practiced investment disciplines to negotiate through market peaks and valleys to achieve
long-term goals for investors. Experienced professionals pursue investments that will weather short-term volatility and at the same time, seek opportunities that are created by markets that overreact to negative developments. Monitoring this process
is an important consideration for the Fund Board as it oversees your Nuveen funds on your behalf. 
As always, I encourage you to contact your financial
consultant if you have any questions about your investment in a Nuveen Fund. On behalf of the other members of your Fund Board, we look forward to continuing to earn your trust in the months and years ahead. 
Sincerely, 


