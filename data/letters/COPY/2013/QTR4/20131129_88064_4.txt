Dear Investor:



The first half of brought welcome evidence that the U.S. economic recovery is gaining traction. Consumer confidence reached its highest level since U.S. stock market indexes marked a series of record highs and the housing market continues its recovery.



According to Asoka Wohrmann, co-chief investment officer for Deutsche Asset &amp; Wealth Management, "The revival of the employment market, good asset performance with rising home and share prices, and an expansive monetary policy gives further growth momentum to the real economy. As a result, we believe that U.S. economic growth could accelerate in the coming months."



Nevertheless, concerns about the European and emerging-market economies persist. Closer to home, the outlook remains guarded when it comes to the eventual end of government intervention in the bond market and the full effects of reduced government spending on employment.



Where does this leave you? That depends on a variety of factors, including your overall portfolio allocation. Given the uncertainties in today's bond and stock markets, it may be time for a thoughtful evaluation of your strategy.



Talk with a trusted advisor to determine whether any adjustments may be in order, given your specific objectives and risk tolerance. We believe even the most sophisticated investor can benefit from the assistance of a trusted, objective financial professional.



Remember that Deutsche Asset &amp; Wealth Management gives you access to Deutsche Bank's global network of economists, analysts and investment professionals. Insights are always at your fingertips at dws-investments.com.



Best regards,
