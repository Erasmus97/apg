Dear fellow shareholder,

On June the First Pacific Low Volatility Fund ("Fund")
commenced operations as a new series of First Pacific Mutual Fund, Inc. The
investment objective of the Fund is to achieve long-term capital appreciation
and preservation of capital while lowering volatility.

The Fund had a net asset value ("NAV") of on October and
a NAV of on September which resulted in a return for the
one year period. During the same period, the Fund's benchmark, the Dow Jones
Moderately Aggressive Portfolio Index had a return of The Fund had net
assets of million as of the beginning of the fiscal year and with an
increase of ended the fiscal year with million in net assets.

Despite the many headwinds faced by our economic recovery, domestic
equity markets finished the twelve-months ended September with strong
gains. For the past fiscal year, the S&P (an index measuring domestic broad
market returns) had an increase of Investors enjoyed this strong market
despite government created financial issues such as the fiscal cliff and the
federal debt ceiling. We also have seen a slowdown in international economic
growth. Investors as well as investment professionals are displaying concerns
over the continued strength of the markets. We are beginning to see corporate
profit increases created by spending cuts such as labor rather than revenue
increases. However, we believe investors are displaying more concerns over the
Federal government's inability to agree on meaningful and long-term fiscal
policy.

Despite this rally in the market, the portfolio managers of the Fund
maintained a preference for dividend-paying companies with a historically lower
market sensitivity profile. A stock's beta, which is the measurement of
volatility of a stock versus the broad market, was also a key factor in the
portfolio managers' analysis. The Fund's high cash levels during the fiscal year
detracted from relative performance to the broad market. As of September
the Fund cash allocation was almost of its net assets. The portfolio
managers maintained higher levels of low-yielding cash to manage the volatility
of the market. This could be seen by the high level of the VIX (measurement to
market volatility). For example, during the weeks prior to the fiscal cliff
resolution, the VIX was at the highest level during our fiscal period.

The portfolio managers also used stop orders on selected equities to
help manage volatility and preserve capital on equity positions held in the
Fund. Stop orders are sell orders that are placed below current values of
an equity position in the portfolio in order to protect gains or limit
losses on the related equity position. Additionally, the Fund's portfolio
managers wrote equity call options within the portfolio strategy to reduce
volatility and generate revenue. In such a management technique, the
portfolio managers sell call options on an existing equity position to reduce
the effect of price fluctuations of securities owned by the Fund on the Fund's
NAV and to generate additional revenues. The Fund's portfolio management team
uses discretion in the selection of which equity securities to write (or sell)
the call option. During the period, the Fund had realized gains on written
options of

The Fund continues to utilize structured notes also in an effort to
reduce market volatility. The portfolio's structured note holdings performed
well during the reporting period as the reference equity indices increased. At
the end of the reporting period, the Fund held of net assets in structured
notes.

CATEGORY ALLOCATION (% of Net Assets)
September

[The following table was depicted as a pie chart in the printed material.]

Consumer Discretionary
Consumer Staples
Energy
Financials
Health Care
Industrials
Information Technology
Utilities
Exchange Traded Funds
Structured Notes
Money Market Fund
Other Assets

As of September the top five categories as a percentage of net
assets were: Cash Information Technology Consumer
Discretionary Financials and Healthcare

On December there was a capital gain distribution of a
share. The Fund is anticipating a capital gain distribution for the Fund for
the calendar year.

On the following pages you will find our September Annual
Report. If you have any questions or would like us to provide information about
the Fund to your family or friends, please call us at or


Thank you for your business. On behalf of the staff and management of
the Fund, I would like to extend to you and your family best wishes for a safe
and happy holiday season.


Warmest Aloha,

/s/ Terrence K.H. Lee

Terrence K.H. Lee
President and CEO

Dear fellow shareholder,

As we begin our year of operations, we are pleased to provide you
with our Fund's Annual Report.

It has been and continues to be challenging times for investors. Equity
markets have reached new highs while bond markets have fallen. The economy
continues to show signs of slow growth. Unemployment, while still high has
fallen and the housing market has begun to recover. As shareholders of the
Hawaii Municipal Fund, you are earning tax-free income* and supporting local
projects designed to enrich our community. The money raised through
municipal bonds is commonly used to build schools, hospitals, roads, airports,
harbors, and water and electrical systems that serve to create jobs and improve
the quality of life here in our islands.

Interest rates are the most important of many factors which can affect
bond prices. Over the course of the fiscal year, the treasury yield curve
steepened with short-term rates remaining near and long-term interest rates
rising basis points to This accounts for the Hawaii Municipal Fund's
fiscal year price decrease of per share. The Hawaii Municipal Fund
Investor Class had a net asset value ("NAV") of on October
and a NAV of on September The primary investment strategy
of the Hawaii Municipal Fund is to purchase primarily investment grade long-term
Hawaii municipal bonds. The past year's performance of the Fund, which is
presented in this Annual Report, was primarily a result of the implementation of
this strategy.

During the fiscal year ended September the Federal Reserve
Bank kept the Federal Funds Rate between and During this period the
year treasury bond's yield rose by basis points to In our opinion,
this rise in long term interest rates is due to uncertainty regarding the
Federal Reserve Bank's Quantitative Easing Program. Although interest rates are
low when compared to average rates over the last ten years, there continues to
be risks to the bond market, among which are U.S. fiscal policy, international
conflicts/terrorism and global economic factors.

On the following pages are line graphs comparing the Fund's performance
to the Barclays Capital Municipal Bond Index for the years ended September
The graph assumes a hypothetical investment in the Fund.
The object of the graph is to permit a comparison of the Fund with a benchmark
and to provide perspective on market conditions and investment strategies and
techniques that materially affected the performance of the Fund. For the fiscal
year ended September the Hawaii Municipal Fund's concentration in
Hawaii municipal bonds caused the portfolio to lag the Index. For the and
year periods ended September the Hawaii Municipal Fund's portfolio had
a shorter effective maturity than the Barclays Capital Municipal Bond Index,
therefore, the Hawaii Municipal Fund lagged the Index due to the declining
interest rate environments.

MOODY'S MUNICIPAL BOND RATINGS
Hawaii Municipal Fund
September

[The following table was depicted as a pie chart in the printed material.]
Aaa


Moody's is an independent ratings service which
assigns ratings from Aaa (highest) to C (lowest) to
indicate the creditworthiness of the issuer's securities
in the Fund's portfolio. Ratings are subject to change.
These ratings apply to the issuer's creditworthiness of the
WR^ securities in the Fund's portfolio and not the Fund or its
NA^ shares.


^ Primarily all of the investments in the Hawaii Municipal Fund portfolio are
investment grade securities. Only of the municipal bonds purchased for
the portfolio are deemed to be below investment grade by the Investment Manager.

We are proud to report that as a Hawaii resident, of the income
dividends earned in were both state and federal tax-free*. There was no
capital gain distribution to shareholders for the calendar year. There
will not be a capital gain distribution for the Hawaii Municipal Fund for the
calendar year.

If you have any questions about this Annual Report or would like us to
provide information about the Fund to your family or friends, please call us at


Thank you for your business as well as the many referrals. On behalf of
the staff and management of the Fund, I would like to extend to you and your
family best wishes for a safe and happy holiday season.

Warmest Aloha,

/s/ Terrence K.H. Lee

Terrence K.H. Lee
President and CEO
