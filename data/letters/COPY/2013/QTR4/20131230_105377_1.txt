Dear Shareholders:
Enclosed in this annual report, you’ll
find information about your Fund and the factors that affected its performance during the reporting period. Inside, you’ll also find a discussion from your portfolio managers about how they managed your Fund, as well as performance data for
your Fund, a complete list of your Fund’s investments as of the close of the reporting period and other important information. I hope you find this report of interest.
1
markets alike. Most developed, non-US stock markets remained positive for the first half of 2013, despite a difficult second quarter.

    Periods of market volatility and economic uncertainty can weaken the will of even the most resolute investor. That’s why Invesco
believes it’s often helpful to work with a skilled and trusted financial adviser who can emphasize the importance of adhering to an investment plan designed to achieve long-term goals rather than being sidetracked by short-term uncertainty that
can result in inaction. A financial adviser who is familiar with your individual financial situation, investment goals and risk tolerance can be an invaluable partner as you work toward your financial goals. He or she can provide insight and
perspective when markets are volatile; encouragement and reassurance when times are uncertain; and advice and guidance when your financial situation or investment goals change. 
    Our website, invesco.com/us, is another source of timely insight and information for investors. On the website, you’ll find fund-specific as
well as more general information from many of Invesco’s investment professionals. You’ll find in-depth articles, video clips and audio commentaries – and, of course, you also can access information about your Invesco account whenever
it’s convenient for you. 
What we mean by Intentional Investing 
At
Invesco, all of our people and all of our resources are dedicated to helping investors achieve their financial objectives. It’s a philosophy we call Intentional Investing, and it guides the way we: 
    At Invesco, we believe in putting investors first. That’s why investment management is all we do. Our sole focus on
managing your money allows you and your financial adviser to build a portfolio of Invesco funds appropriate for your investment needs and goals today and when your circumstances change. 
Have questions? 
For questions about your account, feel free to contact an
Invesco client services representative at 800 959 4246. For Invesco-related questions or comments, please email me directly at phil@invesco.com. 
    All of us at Invesco look forward to serving your investment management needs for many years to come. Thank you for investing with us. 
Sincerely, 


Dear Fellow Shareholders:
The Invesco Funds Board has worked on
a variety of issues over the last several months, and I’d like to take this opportunity to discuss two that affect you and our fellow fund shareholders.
    The first issue on which your Board has been working is our annual review of the funds’ advisory and sub-advisory contracts with Invesco
Advisers and its affiliates. This annual review, which is required by the Investment Company Act of 1940, focuses on the nature and quality of the services Invesco provides as adviser to the Invesco Funds and the reasonableness of the fees that it
charges for those services. Each year, we spend months reviewing detailed information that we request from Invesco that allows us to evaluate its services and fees. We also use information from many independent sources, including materials provided
by the independent Senior Officer of the Invesco Funds, who reports directly to the independent Trustees on the Board. Additionally, we meet with independent
legal counsel and review performance and fee data prepared by Lipper Inc., an independent, third-party firm widely recognized as a leader
in its field. 
    I’m pleased to report that the Board determined in June that renewing the investment advisory agreement and the
sub-advisory contracts with Invesco Advisers and its affiliates would serve the best interests of each fund and its shareholders. 
    The second
area of focus to highlight is the Board’s efforts to ensure that we provide a lineup of funds that allow financial advisers to build portfolios that meet shareholders’ changing financial needs and goals. Today, more and more investors are
reaching, or approaching, retirement. But interest rates remain low, making it difficult for many investors to generate the income they need, or will soon need, in their retirement years. 
    The members of your Board think about these things, too, and we’ve worked with Invesco Advisers to provide more income-generating options in
the Invesco Funds lineup to help shareholders potentially meet their income needs. Your Board recently approved changes to three existing equity mutual funds, increasing their focus on generating income while also seeking to provide long-term growth
of capital. 
    As a result of these changes, the funds and their respective management teams now have more flexibility to invest in the types
of securities that could meet investors’ growing need for income generation, and in some cases, also help investors diversify their sources of income. These equity funds complement an array of fixed-income, asset allocation and alternative
investment options in the Invesco Funds lineup designed to accommodate a variety of risk tolerances. 
    Be assured that your Board will
continue working on behalf of fund shareholders, keeping your needs and interests uppermost in our minds. 
    As always, please contact me at
bruce@brucecrockett.com with any questions or concerns you may have. On behalf of the Board, we look forward to continuing to represent your interests and serving your needs. 
Sincerely, 


Dear Shareholders:
Enclosed in this annual report,
you’ll find information about your Fund and the factors that affected its performance during the reporting period. Inside, you’ll also find a discussion from your portfolio managers about how they managed your Fund, as well as performance
data for your Fund, a complete list of your Fund’s investments as of the close of the reporting period and other important information. I hope you find this report of interest.
1
accommodative monetary policies affected fixed income and equity markets alike. Most developed, non-US stock markets remained positive
for the first half of 2013, despite a difficult second quarter. 
    Periods of market volatility and economic uncertainty can weaken the will of
even the most resolute investor. That’s why Invesco believes it’s often helpful to work with a skilled and trusted financial adviser who can emphasize the importance of adhering to an investment plan designed to achieve long-term goals
rather than being sidetracked by short-term uncertainty that can result in inaction. A financial adviser who is familiar with your individual financial situation, investment goals and risk tolerance can be an invaluable partner as you work toward
your financial goals. He or she can provide insight and perspective when markets are volatile; encouragement and reassurance when times are uncertain; and advice and guidance when your financial situation or investment goals change. 
    Our website, invesco.com/us, is another source of timely insight and information for investors. On the website, you’ll find fund-specific as
well as more general information from many of Invesco’s investment professionals. You’ll find in-depth articles, video clips and audio commentaries – and, of course, you also can access information about your Invesco account whenever
it’s convenient for you. 
What we mean by Intentional Investing 
At
Invesco, all of our people and all of our resources are dedicated to helping investors achieve their financial objectives. It’s a philosophy we call Intentional Investing, and it guides the way we: 
    At Invesco, we believe in putting investors first. That’s why investment management is all we do. Our sole focus on
managing your money allows you and your financial adviser to build a portfolio of Invesco funds appropriate for your investment needs and goals today and when your circumstances change. 
Have questions? 
For questions about your account, feel free to contact an
Invesco client services representative at 800 959 4246. For Invesco-related questions or comments, please email me directly at phil@invesco.com. 
    All of us at Invesco look forward to serving your investment management needs for many years to come. Thank you for investing with us. 
Sincerely, 


Dear Fellow Shareholders:
The Invesco Funds Board
has worked on a variety of issues over the last several months, and I’d like to take this opportunity to discuss two that affect you and our fellow fund shareholders.
    The first issue on which your Board has been working is our annual review of the funds’ advisory and sub-advisory contracts with Invesco
Advisers and its affiliates. This annual review, which is required by the Investment Company Act of 1940, focuses on the nature and quality of the services Invesco provides as adviser to the Invesco Funds and the reasonableness of the fees that it
charges for those services. Each year, we spend months reviewing detailed information that we request from Invesco that allows us to evaluate its services and fees. We also use information from many independent sources, including materials provided
by the independent Senior Officer of the Invesco Funds, who reports directly to the independent
Trustees on the Board. Additionally, we meet with independent legal counsel and review performance and fee data prepared by Lipper Inc.,
an independent, third-party firm widely recognized as a leader in its field. 
    I’m pleased to report that the Board determined in June
that renewing the investment advisory agreement and the sub-advisory contracts with Invesco Advisers and its affiliates would serve the best interests of each fund and its shareholders. 
    The second area of focus to highlight is the Board’s efforts to ensure that we provide a lineup of funds that allow financial advisers to
build portfolios that meet shareholders’ changing financial needs and goals. Today, more and more investors are reaching, or approaching, retirement. But interest rates remain low, making it difficult for many investors to generate the income
they need, or will soon need, in their retirement years. 
    The members of your Board think about these things, too, and we’ve worked with
Invesco Advisers to provide more income-generating options in the Invesco Funds lineup to help shareholders potentially meet their income needs. Your Board recently approved changes to three existing equity mutual funds, increasing their focus on
generating income while also seeking to provide long-term growth of capital. 
    As a result of these changes, the funds and their respective
management teams now have more flexibility to invest in the types of securities that could meet investors’ growing need for income generation, and in some cases, also help investors diversify their sources of income. These equity funds
complement an array of fixed-income, asset allocation and alternative investment options in the Invesco Funds lineup designed to accommodate a variety of risk tolerances. 
    Be assured that your Board will continue working on behalf of fund shareholders, keeping your needs and interests uppermost in our minds. 
    As always, please contact me at bruce@brucecrockett.com with any questions or concerns you may have. On behalf of the Board, we look forward to
continuing to represent your interests and serving your needs. 
Sincerely, 


Dear Shareholders:
Enclosed in this annual report, you’ll
find information about your Fund and the factors that affected its performance during the reporting period. Inside, you’ll also find a discussion from your portfolio managers about how they managed your Fund, as well as performance data for
your Fund, a complete list of your Fund’s investments as of the close of the reporting period and other important information. I hope you find this report of interest.
1
accommodative monetary policies affected fixed income and equity markets alike. Most developed, non-US stock markets remained positive
for the first half of 2013, despite a difficult second quarter. 
Periods of market volatility and economic uncertainty can weaken the will of even
the most resolute investor. That’s why Invesco believes it’s often helpful to work with a skilled and trusted financial adviser who can emphasize the importance of adhering to an investment plan designed to achieve long-term goals rather
than being sidetracked by short-term uncertainty that can result in inaction. A financial adviser who is familiar with your individual financial situation, investment goals and risk tolerance can be an invaluable partner as you work toward your
financial goals. He or she can provide insight and perspective when markets are volatile; encouragement and reassurance when times are uncertain; and advice and guidance when your financial situation or investment goals change. 
Our website, invesco.com/us, is another source of timely insight and information for investors. On the website, you’ll find fund-specific as well as
more general information from many of Invesco’s investment professionals. You’ll find in-depth articles, video clips and audio commentaries – and, of course, you also can access information about your Invesco account whenever
it’s convenient for you. 
What we mean by Intentional Investing 
At
Invesco, all of our people and all of our resources are dedicated to helping investors achieve their financial objectives. It’s a philosophy we call Intentional Investing, and it guides the way we: 
At Invesco, we believe in putting investors first. That’s why investment management is all we do. Our sole focus on managing your
money allows you and your financial adviser to build a portfolio of Invesco funds appropriate for your investment needs and goals today and when your circumstances change. 
Have questions? 
For questions about your account, feel free to contact an
Invesco client services representative at 800 959 4246. For Invesco-related questions or comments, please email me directly at phil@invesco.com. 
All
of us at Invesco look forward to serving your investment management needs for many years to come. Thank you for investing with us. 
Sincerely, 


Dear Fellow Shareholders:
The Invesco Funds Board has worked on
a variety of issues over the last several months, and I’d like to take this opportunity to discuss two that affect you and our fellow fund shareholders.
The first issue on which your Board has been working is our annual review of the funds’ advisory and sub-advisory contracts with Invesco
Advisers and its affiliates. This annual review, which is required by the Investment Company Act of 1940, focuses on the nature and quality of the services Invesco provides as adviser to the Invesco Funds and the reasonableness of the fees that it
charges for those services. Each year, we spend months reviewing detailed information that we request from Invesco that allows us to evaluate its services and fees. We also use information from many independent sources, including materials provided
by the independent Senior Officer of the Invesco Funds, who reports directly to the independent
Trustees on the Board. Additionally, we meet with independent legal counsel and review performance and fee data prepared by Lipper Inc.,
an independent, third-party firm widely recognized as a leader in its field. 
I’m pleased to report that the Board determined in June that
renewing the investment advisory agreement and the sub-advisory contracts with Invesco Advisers and its affiliates would serve the best interests of each fund and its shareholders. 
The second area of focus to highlight is the Board’s efforts to ensure that we provide a lineup of funds that allow financial advisers to build
portfolios that meet shareholders’ changing financial needs and goals. Today, more and more investors are reaching, or approaching, retirement. But interest rates remain low, making it difficult for many investors to generate the income they
need, or will soon need, in their retirement years. 
The members of your Board think about these things, too, and we’ve worked with Invesco
Advisers to provide more income-generating options in the Invesco Funds lineup to help shareholders potentially meet their income needs. Your Board recently approved changes to three existing equity mutual funds, increasing their focus on generating
income while also seeking to provide long-term growth of capital. 
As a result of these changes, the funds and their respective management teams now
have more flexibility to invest in the types of securities that could meet investors’ growing need for income generation, and in some cases, also help investors diversify their sources of income. These equity funds complement an array of
fixed-income, asset allocation and alternative investment options in the Invesco Funds lineup designed to accommodate a variety of risk tolerances. 
Be assured that your Board will continue working on behalf of fund shareholders, keeping your needs and interests uppermost in our minds. 
As always, please contact me at bruce@brucecrockett.com with any questions or concerns you may have. On behalf of the Board, we look forward to
continuing to represent your interests and serving your needs. 
Sincerely, 


Dear Shareholders:
Enclosed in this annual report, you’ll
find information about your Fund and the factors that affected its performance during the reporting period. Inside, you’ll also find a discussion from your portfolio managers about how they managed your Fund, as well as performance data for
your Fund, a complete list of your Fund’s investments as of the close of the reporting period and other important information. I hope you find this report of interest.
1
its extraordinarily accommodative monetary policies affected fixed income and equity markets alike. Most developed, non-US stock markets
remained positive for the first half of 2013, despite a difficult second quarter. 
    Periods of market volatility and economic uncertainty can
weaken the will of even the most resolute investor. That’s why Invesco believes it’s often helpful to work with a skilled and trusted financial adviser who can emphasize the importance of adhering to an investment plan designed to achieve
long-term goals rather than being sidetracked by short-term uncertainty that can result in inaction. A financial adviser who is familiar with your individual financial situation, investment goals and risk tolerance can be an invaluable partner as
you work toward your financial goals. He or she can provide insight and perspective when markets are volatile; encouragement and reassurance when times are uncertain; and advice and guidance when your financial situation or investment goals change.

    Our website, invesco.com/us, is another source of timely insight and information for investors. On the website, you’ll find
fund-specific as well as more general information from many of Invesco’s investment professionals. You’ll find in-depth articles, video clips and audio commentaries – and, of course, you also can access information about your Invesco
account whenever it’s convenient for you. 
What we mean by Intentional Investing 
At Invesco, all of our people and all of our resources are dedicated to helping investors achieve their financial objectives. It’s a philosophy we call Intentional
Investing, and it guides the way we: 
    At Invesco, we believe in putting investors first. That’s why investment management is all we do. Our sole focus on
managing your money allows you and your financial adviser to build a portfolio of Invesco funds appropriate for your investment needs and goals today and when your circumstances change. 
Have questions? 
For questions about your account, feel free to contact an
Invesco client services representative at 800 959 4246. For Invesco-related questions or comments, please email me directly at phil@invesco.com. 
    All of us at Invesco look forward to serving your investment management needs for many years to come. Thank you for investing with us. 
Sincerely, 


Dear Fellow Shareholders:
The Invesco Funds Board has worked on
a variety of issues over the last several months, and I’d like to take this opportunity to discuss two that affect you and our fellow fund shareholders.
    The first issue on which your Board has been working is our annual review of the funds’ advisory and sub-advisory contracts with Invesco
Advisers and its affiliates. This annual review, which is required by the Investment Company Act of 1940, focuses on the nature and quality of the services Invesco provides as adviser to the Invesco Funds and the reasonableness of the fees that it
charges for those services. Each year, we spend months reviewing detailed information that we request from Invesco that allows us to evaluate its services and fees. We also use information from many independent sources, including materials provided
by the independent Senior Officer of the Invesco Funds, who reports
directly to the independent Trustees on the Board. Additionally, we meet with independent legal counsel and review performance and fee
data prepared by Lipper Inc., an independent, third-party firm widely recognized as a leader in its field. 
    I’m pleased to report that
the Board determined in June that renewing the investment advisory agreement and the sub-advisory contracts with Invesco Advisers and its affiliates would serve the best interests of each fund and its shareholders. 
    The second area of focus to highlight is the Board’s efforts to ensure that we provide a lineup of funds that allow financial advisers to
build portfolios that meet shareholders’ changing financial needs and goals. Today, more and more investors are reaching, or approaching, retirement. But interest rates remain low, making it difficult for many investors to generate the income
they need, or will soon need, in their retirement years. 
    The members of your Board think about these things, too, and we’ve worked with
Invesco Advisers to provide more income-generating options in the Invesco Funds lineup to help shareholders potentially meet their income needs. Your Board recently approved changes to three existing equity mutual funds, increasing their focus on
generating income while also seeking to provide long-term growth of capital. 
    As a result of these changes, the funds and their respective
management teams now have more flexibility to invest in the types of securities that could meet investors’ growing need for income generation, and in some cases, also help investors diversify their sources of income. These equity funds
complement an array of fixed-income, asset allocation and alternative investment options in the Invesco Funds lineup designed to accommodate a variety of risk tolerances. 
    Be assured that your Board will continue working on behalf of fund shareholders, keeping your needs and interests uppermost in our minds. 
    As always, please contact me at bruce@brucecrockett.com with any questions or concerns you may have. On behalf of the Board, we look forward to
continuing to represent your interests and serving your needs. 
Sincerely, 


