Dear Fellow Shareholder,



We are pleased to provide you with the Pear Tree Funds Semi-Annual Report for the six-month period ended September and to update you on recent market conditions and the performance of the Pear Tree Funds.



For current performance information, please visit our website at www.peartreefunds.com. We thank you for your continued confidence in the Pear Tree Funds. Please feel free to e-mail us at feedback@peartreefunds.com or call us at



with any questions or for assistance on your account.



Sincerely,
