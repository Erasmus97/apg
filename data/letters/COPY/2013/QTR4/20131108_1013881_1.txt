Dear Shareholders,
I am pleased to have this opportunity to introduce myself to you as the new independent chairman of the Nuveen Fund Board, effective July I am honored to have been selected as chairman, with its primary
responsibility to serve the interests of the Nuveen Fund shareholders. My predecessor, Robert Bremner, was the first independent director to serve as chairman of the Board and I, and my fellow Board members, plan to continue his legacy of strong
independent oversight of your funds. The global economy has hit major turning points over the last several months to a year. The developed world is
gradually recovering from their financial crisis while the emerging markets appear to be struggling with the downshift of Chinas growth potential. Japan is entering a new era of growth after decades of economic stagnation and many of the
Eurozone nations appear to be exiting their recession. Despite the positive events, there are still potential risks. Middle East tensions, rising oil prices, defaults in Europe and fallout from the financial stress in emerging markets could all
reverse the recent progress in the global economy. On the domestic front, the U.S. economy is experiencing sustainable slow growth. Corporate
fundamentals are strong as earnings per share and corporate cash are at the highest level in two decades. Unemployment is trending down and the housing market has experienced a rebound, each assisting the positive economic scenario. However, there
are some issues to be watched. Interest rates are expected to increase but significant uncertainty about the timing remains. Partisan politics in Washington D.C. with their troublesome outcome add to the uncertainties that could cause problems for
the economy going forward. In the near term, governments are focused on economic recovery and the growth of their economies, which could lead to an
environment of attractive investment opportunities. Over the long term, the uncertainties mentioned earlier could hinder the potential growth. Because of this, Nuveens investment management teams work hard to balance return and risk with a
range of investment strategies. I encourage you to read the following commentary on the management of your fund. On behalf of the other members of the
Nuveen Fund Board, we look forward to continuing to earn your trust in the months and years ahead. Sincerely,

Dear Shareholders,
I am pleased to have this opportunity to introduce myself to you as the new independent chairman of the Nuveen Fund Board, effective July I am honored to have been selected as chairman, with its primary
responsibility to serve the interests of the Nuveen Fund shareholders. My predecessor, Robert Bremner, was the first independent director to serve as chairman of the Board and I, and my fellow Board members, plan to continue his legacy of strong
independent oversight of your funds. The global economy has hit major turning points over the last several months to a year. The developed world is
gradually recovering from their financial crisis while the emerging markets appear to be struggling with the downshift of Chinas growth potential. Japan is entering a new era of growth after decades of economic stagnation and many of the
Eurozone nations appear to be exiting their recession. Despite the positive events, there are still potential risks. Middle East tensions, rising oil prices, defaults in Europe and fallout from the financial stress in emerging markets could all
reverse the recent progress in the global economy. On the domestic front, the U.S. economy is experiencing sustainable slow growth. Corporate
fundamentals are strong as earnings per share and corporate cash are at the highest level in two decades. Unemployment is trending down and the housing market has experienced a rebound, each assisting the positive economic scenario. However, there
are some issues to be watched. Interest rates are expected to increase but significant uncertainty about the timing remains. Partisan politics in Washington D.C. with their troublesome outcome add to the uncertainties that could cause problems for
the economy going forward. In the near term, governments are focused on economic recovery and the growth of their economies, which could lead to an
environment of attractive investment opportunities. Over the long term, the uncertainties mentioned earlier could hinder the potential growth. Because of this, Nuveens investment management teams work hard to balance return and risk with a
range of investment strategies. I encourage you to read the following commentary on the management of your fund. On behalf of the other members of the
Nuveen Fund Board, we look forward to continuing to earn your trust in the months and years ahead. Sincerely,

Dear Shareholders,
I am pleased to have this opportunity to introduce myself to you as the new independent chairman of the Nuveen Fund Board, effective July I am honored to have been selected as chairman, with its primary
responsibility to serve the interests of the Nuveen Fund shareholders. My predecessor, Robert Bremner, was the first independent director to serve as chairman of the Board and I, and my fellow Board members, plan to continue his legacy of strong
independent oversight of your funds. The global economy has hit major turning points over the last several months to a year. The developed world is
gradually recovering from their financial crisis while the emerging markets appear to be struggling with the downshift of Chinas growth potential. Japan is entering a new era of growth after decades of economic stagnation and many of the
Eurozone nations appear to be exiting their recession. Despite the positive events, there are still potential risks. Middle East tensions, rising oil prices, defaults in Europe and fallout from the financial stress in emerging markets could all
reverse the recent progress in the global economy. On the domestic front, the U.S. economy is experiencing sustainable slow growth. Corporate
fundamentals are strong as earnings per share and corporate cash are at the highest level in two decades. Unemployment is trending down and the housing market has experienced a rebound, each assisting the positive economic scenario. However, there
are some issues to be watched. Interest rates are expected to increase but significant uncertainty about the timing remains. Another potential fiscal cliff in October along with a possible conflict in the Middle East both add to the uncertainties
that could cause problems for the economy going forward. In the near term, governments are focused on economic recovery and the growth of their
economies, which could lead to an environment of attractive investment opportunities. Over the long term, the uncertainties mentioned earlier could hinder the potential growth. Because of this, Nuveens investment management teams work hard to
balance return and risk with a range of investment strategies. I encourage you to read the following commentary on the management of your fund. On
behalf of the other members of the Nuveen Fund Board, we look forward to continuing to earn your trust in the months and years ahead. Sincerely,
