Dear Shareowner,

When we look at the U.S. economy heading into the final quarter of we
continue to see slow, but steady, growth. Employment has also been rising
steadily, but only modestly. Consumer incomes, savings, wealth, and
debt-servicing capacity have been solid buttresses for the recovering housing
and auto industries. Industrial activity is growing only modestly, but current
corporate profits are generally solid and balance sheets appear able to support
needed capital spending and dividend* payouts. The scaled-back "fiscal cliff"
tax increases and spending cuts have meaningfully cut the budget deficit without
driving the economy into recession. In addition, we feel that continuing slack
in labor markets and capacity utilization offers the potential for continuing
growth without bottlenecks and rising inflation.

After observing the strengthening economic trends, the Federal Reserve (the Fed)
hinted that it might begin scaling back its "QE" quantitative easing program
later in and could terminate its bond purchases altogether sometime in
In September, however, the Fed surprised many market participants by
deciding not to start scaling back QE yet. The Fed has also said that short-term
interest rates are likely to remain near zero for some time to come, given that
inflation remains subdued and unemployment remains too high.

As September ended, Congress had not yet passed a continuing resolution to
prevent a government shutdown, nor had it raised the debt ceiling, and a quick
resolution to the impasse appeared unlikely. The U.S. government's partial
shutdown in October rattled the markets to a degree, but did not immediately
have a significant negative impact on the economy or capital markets.

There are certainly risks and uncertainties that continue to plague the global
economy as we head into the final months of the year. The European economy
remains weak, though it is beginning to show signs of stabilization, and a
number of countries in the emerging markets have experienced difficulties.
Still, a potential ending of the European recession, continuing economic
improvement in Japan in response to the new government's easing policies, and a
"soft landing" of growth in China could very well result in an improving
global outlook over the remainder of and in

There are also geopolitical worries abroad and the aforementioned political
fights at home, and while most of the widely recognized risks we've outlined may
already be "priced into" the market, we believe investors should continue to
expect market volatility.

* Dividends are not guaranteed.

Pioneer High Yield Fund | Annual Report |


The Fed's aggressive monetary policies and fears about economic growth had
helped drive long-term Treasury yields to unsustainably low levels; the return
to more normal levels has resulted in disappointing returns for bond investors
during the first nine months of but the stock market has delivered
double-digit returns to equity investors who were willing to brave the "wall of
worry".

At Pioneer, we have long advocated the benefits of staying diversified and
investing for the long term. And while diversification does not assure a profit
or protect against loss in a declining market, we believe there are still
opportunities for prudent investors to earn attractive returns. Our advice, as
always, is to work closely with a trusted financial advisor to discuss your
goals and work together to develop an investment strategy that meets your
individual needs, keeping in mind that there is no single best strategy that
works for every investor.

Pioneer's investment teams have, since sought out attractive opportunities
in global equity and bond markets, using in-depth research in an effort to
identify undervalued individual securities, and using thoughtful risk management
to construct portfolios which seek to balance potential risks and reward in an
ever-changing world.

We encourage you to learn more about Pioneer and our time-tested approach to
investing by consulting with your financial advisor or visiting us online at
us.pioneerinvestments.com. We greatly appreciate your trust in us, and we thank
you for investing with Pioneer.

Sincerely,
