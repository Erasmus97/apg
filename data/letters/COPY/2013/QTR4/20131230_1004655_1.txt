Dear Shareholder,
When stock markets are surging, mid-and small-capitalization equities are often at the forefront as confident investors more willingly accept the risk and volatility that typically accompany a commitment to smaller, less-established companies. For the fiscal year ended October 31, 2013, Vanguard Selected Value Fund benefited from just such an investment climate, as well as from superior stock choices and allocation decisions by the fund’s two advisors.
The fund returned more than 36%, about 3 percentage points more than its benchmark, the Russell Midcap Value Index, and 2 percentage points above the average return of its mid-cap value peers. Selected Value’s largest sector weightings––financials, industrials, consumer discretionary, information technology, and health care––were also its most productive. Of the nine sectors in which the fund had meaningful investments, only its second-smallest one, materials, had a negative return.
The fund was also hurt by the advisors’ decision to keep about 10% of its assets in cash, on average, during the period. The 10% cash position is unusual for Vanguard but not for Selected Value; its advisors have the flexibility to hold cash when they cannot find attractive stocks that meet their strict valuation criteria.
2
If you hold shares in a taxable account, you may wish to review the table and discussion on after-tax returns for the fiscal year that appear later in this report.
Amid uncertainty, U.S. stocks found a path to strong returns
Although the end of the fiscal year was notable for the budget impasse that resulted in October’s 16-day partial federal government shutdown, the period as a
whole was marked by uncertainty about Federal Reserve monetary policy and concern about the economy’s patchy growth. Vanguard’s chief economist, Joe Davis, recently noted that “as was the case at the start of the year, the U.S. economy continues to expand at a modest and uneven pace.”
 
3
Outside the United States, stocks returned about 20%. The developed markets of Europe and the Pacific region delivered robust gains; emerging-market stocks failed to keep pace.
Bond returns sagged as investors kept a close eye on the Fed
With investors fretting over the Fed’s next move in its stimulative bond-buying program, bonds recorded negative results for the 12 months. The broad U.S. taxable bond market returned –1.08%. The yield of the 10-year Treasury note closed at 2.54%, down from 2.63% at September’s close but up from 1.69% at the end of the last fiscal year. (Bond yields and prices move in opposite directions.) Municipal bonds returned –1.72%.
Outside the United States, bond markets (as measured by the Barclays Global Aggregate Index ex USD) returned –1.95%.
The Fed’s target for short-term interest rates remained at 0%–0.25%, severely limiting returns of money market funds and savings accounts.
The advisors’ decisions helped the fund outpace its index
Although small-cap stocks led the market over the 12 months with returns that approached 40%, mid-caps were also lively participants in the rally, trumping their large-cap counterparts. Mid-cap growth stocks outpaced mid-cap value
4
stocks by less than 1 percentage point, and double-digit returns were evident across the mid-cap spectrum.
Investing opportunities were abundant among mid-cap companies, and the Selected Value Fund’s two advisors made a number of wise decisions in their efforts to outperform its comparative standards. In some cases, the fund’s holdings bested the benchmark’s; in others, the advisor avoided some subpar holdings that dampened the benchmark’s return.
Financial stocks, the fund’s largest sector, returned about 34% and contributed about one-quarter of the fund’s result. Selected Value’s holdings in insurance companies, consumer finance firms, commercial banks, and asset managers all returned
at least 30%; the sector was boosted by a rosier investment climate, stronger balance sheets, improved lending conditions, and more robust housing and labor markets. The fund’s outperformance, though, was largely because the advisors almost entirely sidestepped real estate investment trusts (REITs), which stalled after a few years of solid growth.
Selected Value’s industrial and consumer discretionary sectors returned about 50% each. This reflected the economy’s slow but steady improvement, and the resulting ability and eagerness of consumers and corporations to spend more freely. Machinery firms, airlines, and electrical equipment companies were among the fund’s standouts in industrials. Clothing company Hanesbrands, which returned
5
more than 100%, led the fund’s consumer discretionary sector, in which hotels and casinos, auto-parts companies, and housewares firms also performed well.
Perhaps most strikingly, the fund returned more than 80% in technology, compared with 50% for the benchmark. The advisors stayed with Micron Technology through earlier difficulties for the semiconductor equipment firm, and their commitment was rewarded this period with a return of more than 225%. Electronic equipment, software, and IT service companies also drove the fund’s success in the sector.
In health care, the advisors’ concentrated stock choices among providers, managed care firms, and distributors proved fruitful.
Selected Value’s biggest stumbling block was in materials. With gold and silver miners struggling as precious metals prices slid, the fund’s metals and mining holdings returned –50%. That decline outweighed a 48% advance for its specialty chemicals stocks. In energy, the advisors’ holdings underperformed those of the benchmark.
You can find more information on the fund’s positioning and performance during the fiscal year in the Advisors’ Report that follows this letter.
Ten-year results show the fund’s solid advisory arrangement
The Selected Value Fund’s two advisors––Barrow, Hanley, Mewhinney & Strauss, LLC, and Donald Smith & Co., Inc.––both take a value approach to investing, albeit from different perspectives. Barrow, Hanley seeks out-of-favor stocks of solid companies that regularly pay dividends; Donald Smith considers the stocks of troubled companies that may not have much further to fall, but have the potential to stage major rebounds.
Barrow, Hanley has advised the fund since its 1996 inception and Donald Smith since 2005. As measured by the results of the last decade, the arrangement has been a solid one. For the ten years ended October 31, 2013, the Selected Value Fund posted an average annual return of 11.00%. Its benchmark (which bears no expenses) returned 10.62%, and the peer group’s average annual return was 9.20%.
6
Selected Value is served by the advisors’ experience, skill, and knowledge along with low costs, which allow you to keep more of the fund’s return.
Combining diversity of thought with low costs brings benefits
Investors sometimes ask why Vanguard uses a multi-advisor approach for many of its actively managed equity funds. Just as we recommend diversification within and across asset classes for an investor’s overall portfolio, we think significant benefits can accrue from using multiple advisory firms for a single fund: diversity of investment process and style, thought, and holdings.
These elements can lead to less risk and better results. Because not all investment managers invest the same way, their returns relative to the benchmark don’t move in lockstep.
As with many investment topics, however, there are some misconceptions about the benefits of a multi-manager approach. For example, it is often suggested that the
best ideas of the advisors are diluted when combined in one portfolio. Recent Vanguard research has found otherwise.
As always, thank you for investing with Vanguard.
Sincerely,


Dear Shareholder,
The U.S. stock market turned in a strong performance for the 12 months ended October 31, 2013. Mid-capitalization stocks—like those held in Vanguard Mid-Cap Growth Fund—outperformed their larger-cap counterparts as well as the broad market.
In this investment environment, the Mid-Cap Growth Fund returned 30.32%. That robust result notwithstanding, the fund trailed its benchmark, the Russell Midcap Growth Index, by more than 3 percentage points, mainly because of subpar stock selection in a handful of sectors. The fund also lagged the average return of its peers.
If you own shares of the fund in a taxable account, you may wish to review the information about after-tax returns presented later in this report. Please note that as of October 31, 2013, the fund had realized short-term capital gains of $0.78 per share and long-term gains of $1.74, together accounting for 9.8% of fund assets. Gains are distributed in December.
Amid uncertainties, U.S. stocks found a path to strong returns
U.S. stocks faced several challenges en route to an impressive return of about 29% for the 12 months ended October 31, 2013. Investors’ growing appetite for risk drove the rise in stocks, as corporate profit growth, on the whole, wasn’t particularly tantalizing.
2
Although the end of the fiscal year was notable for the budget impasse that resulted in October’s 16-day partial shutdown of the federal government, the period as a whole was marked by uncertainty about Federal Reserve monetary policy and concern about the economy’s patchy growth. Vanguard’s chief economist, Joe Davis, recently noted that “as was the case at the start of the year, the U.S. economy continues to expand at a modest and uneven pace.”
Outside the United States, stocks returned about 20%. The developed markets of Europe and the Pacific region delivered robust gains; emerging-market stocks failed to keep pace.
Bond returns sagged as investors kept a close eye on the Fed
With investors fretting over the Fed’s next move in its stimulative bond-buying program, bonds recorded negative results for the 12 months. The broad U.S. taxable
 
3
bond market returned –1.08%. The yield of the 10-year Treasury note closed at 2.54%, down from 2.63% at September’s close but up from 1.69% at the end of the previous fiscal year. (Bond yields and prices move in opposite directions.) Municipal bonds returned –1.72%.
Outside the United States, bond markets (as measured by the Barclays Global Aggregate Index ex USD) returned –1.95%.
The Fed’s target for short-term interest rates remained at 0%–0.25%, severely limiting returns of money market funds and savings accounts.
Mid-caps performed robustly, with gains across all sectors
In keeping with its name, Vanguard Mid-Cap Growth Fund invests primarily in stocks of mid-cap U.S. companies that the fund’s advisors believe have a strong potential for growth. Mid-cap stocks, as I mentioned previously, generated impressive results for the fiscal year, outperforming the broad market.
In this environment, the fund posted gains in all nine market sectors in which it was invested (it held no utility stocks). Its shares in the industrial, consumer discretionary, and financial sectors—which together constituted just over half of
The fund expense ratio shown is from the prospectus dated February 22, 2013, and represents estimated costs for the current fiscal year. For the fiscal year ended October 31, 2013, the fund’s expense ratio was 0.51%. The peer-group expense ratio is derived from data provided by Lipper, a Thomson Reuters Company, and captures information through year-end 2012.
Peer group: Mid-Cap Growth Funds.
4
the fund’s assets, on average, during the period—produced nearly two-thirds of its return.
The industrial sector, often considered a gauge of the health of the economy, contributed most to fund performance. Standouts included trucking companies and aerospace and defense firms.
Consumer discretionary, the fund’s largest sector, was its second-biggest contributor, as signs of economic improvement further bolstered consumer spending. The sector posted strong returns across the board, led by stocks of hotels, apparel companies, and specialty retailers.
Financial stocks were also among the market’s top performers. The industry’s widespread resurgence, which followed several years of dismal results in the wake of the 2008–2009 financial crisis, continued throughout the 12 months, generating solid returns across the sector. Regional banks and asset management firms did particularly well.
The fund’s holdings in information technology and health care also boosted its overall performance.
Your fund’s tempered results compared with its benchmark can be attributed to the advisors’ stock selection in a few sectors. Although the fund’s consumer discretionary holdings added significantly to its
The figures shown represent past performance, which is not a guarantee of future results. (Current performance may be lower or higher than the performance data cited. For performance data current to the most recent month-end, visit our website at vanguard.com/performance.) Note, too, that both investment returns and principal value can fluctuate widely, so an investor’s shares, when sold, could be worth more or less than their original cost.
5
overall return, a lack of some high-flying internet stocks hurt its relative results. The advisors’ choices in health care, energy, and information technology also weighed on relative performance.
Despite challenges, the fund yielded competitive ten-year results
For the decade ended October 31, Vanguard Mid-Cap Growth Fund posted an average annual return of 9.23%, trailing its index (+9.60%) but outpacing its peer-group average (+8.10%).
This ten-year period saw periods of extreme volatility, including the trauma of the financial crisis. We thank the current team of advisors—Chartwell Investment Partners and William Blair & Company, which came aboard in 2006, for their stewardship of the fund. We believe that their ongoing efforts, supplemented by the fund’s low costs, will keep it competitive over the long term.
Combining diversity of thought with low costs brings benefits
Investors sometimes ask why Vanguard uses a multi-advisor approach for many of its actively managed equity funds. Just as we recommend diversification within and across asset classes for an investor’s overall portfolio, we think significant benefits can accrue from using multiple advisory firms for a single fund: diversity of investment process and style, thought, and holdings.
All these elements can lead to less risk and better results. Because not all investment managers invest the same way, their returns relative to the benchmark don’t move in lockstep.
As with many investment topics, however, there are some misconceptions about the benefits of a multi-manager approach. For example, it is often suggested that the best ideas of the advisors are diluted when combined in one portfolio. Recent Vanguard research has found otherwise.
As always, thank you for investing with Vanguard.
Sincerely,


Dear Shareholder,
International stocks turned in their strongest performance in five years for the fiscal year ended October 31, 2013, as optimism took hold that better days are ahead for the global economy. Many established small-capitalization markets, including those in Germany, France, Switzerland, and the Netherlands, returned more than 35% as investors perceived brighter prospects for small companies.
Vanguard International Explorer Fund posted a return of about 31%, virtually identical to that of its benchmark, the Standard & Poor’s EPAC (Europe Pacific Asia Composite) SmallCap Index, and ahead of the average return of its peers.
The fund’s European holdings performed the best by far. Developed Pacific markets stocks did well, although those in the fund lagged those in the index. The U.S. dollar’s strength against the Japanese yen detracted from results for U.S.-based investors. The fund’s emerging markets small-cap portfolio posted modest gains.
If you own the fund in a taxable account, you may wish to review the information on after-tax returns later in this report.
2
U.S. stocks faced several challenges en route to an impressive return of about 29% for the 12 months ended October 31. Investors’ growing appetite for risk drove the rise, as corporate profit growth, in general, wasn’t particularly tantalizing.
Outside the United States, stocks returned about 20%. As reflected in the results for International Explorer, the developed
 
3
markets of Europe and the Pacific region delivered robust gains, but emerging-market stocks failed to keep pace.
Bond returns sagged as investors kept a close eye on the Fed
With investors fretting over the Fed’s next move in its stimulative bond-buying program, bonds recorded negative results for the 12 months. The broad U.S. taxable bond market returned –1.08%. The yield of the 10-year Treasury note closed at 2.54%, down from 2.63% at September’s close but up from 1.69% at the end of the previous fiscal year. (Bond yields and prices move in opposite directions.) Municipal bonds returned –1.72%.
The Fed’s target for short-term interest rates remained at 0%–0.25%, severely limiting returns of money market funds and savings accounts.
Rebounding European stocks were the largest contributors
Vanguard International Explorer Fund notched gains during the fiscal year in all but a handful of countries. Its European holdings, which made up well over half its assets, were the chief source of its impressive performance. They returned
4
about 45%, versus 39% for the benchmark’s European listings. United Kingdom stocks, composing the largest portion of the fund, contributed most to its bottom line, returning 41% compared with 39% for U.K. stocks in the index.
Other notable performers in the European portfolio included stocks in Germany, the Netherlands, and Scandinavia. Recognizing that stock markets of economies struggling with unprecedented challenges can rebound sharply, the fund’s advisors overweighted Ireland, and their selections reaped a 77% return, easily outpacing the benchmark’s Irish stocks. The fund’s holdings in France, Spain, and Belgium, though, trailed their benchmark counterparts.
The fund’s 20% advance in developed Pacific markets, although generous by historical standards, slightly trailed the index return for those markets. Japanese small-cap stocks, which dominate the region, returned 26% but lagged their index counterparts by 4 percentage points. Returns for U.S.-based investors would have been even higher had the yen’s value not fallen 19% versus the dollar as currency markets reacted to the Bank of Japan’s aggressive intervention in the nation’s fixed income market.
Among the other developed Pacific markets represented in the portfolio, Australia outperformed most, but selections there rose only a somewhat
5
tame 6%. A 9% decline in the value of the Australian dollar served as a brake for U.S. investors.
International Explorer’s emerging markets portfolio recorded more modest returns than its developed markets stocks but still beat its index counterpart. The advisors’ selections in Brazil and India were the most notable detractors.
For more information on the fund’s investment strategies and positioning, please see the Advisors’ Report that follows this letter.
The fund’s ten-year record spans a bear and two bulls
For the ten years ended October 31, 2013, Vanguard International Explorer Fund returned an average of 10.45% per year, slightly ahead of both its peer group average and benchmark index. This record is notable for a volatile decade that saw markets set record highs, plunge amid the global financial crisis, then recover most or all of their losses.
The fund outpaced the S&P EPAC SmallCap Index even though the index has no investment expenses—not an easy feat for an actively managed fund. The advisors are experienced in identifying promising small-company stocks that are expected to grow faster than the overall market. Also, the fund’s performance isn’t burdened by a high expense ratio that would detract from investors’ returns.
The past decade offered a lesson in perseverance. When stocks worldwide suffered big losses midway through the period, many investors couldn’t resist the temptation to pull out of the market. Those who kept their positions were rewarded. Of course, holding course was easier for those who had balanced their portfolios with tamer investments, including bonds and cash, which provided some cushioning when the stock markets fell. That’s why we like to remind our clients of the perils of overconcentrating in any one market or even one asset class.
Combining diversity of thought with low costs brings benefits
Investors sometimes ask why Vanguard uses a multi-advisor approach for many of its actively managed equity funds. Just as we recommend diversification within and across asset classes for an investor’s overall portfolio, we think significant benefits can accrue from using multiple advisory firms for a single fund: diversity of investment process and style, thought, and holdings.
These elements can lead to less risk and better results. Because not all investment managers invest the same way, their returns relative to the benchmark don’t move in lockstep.
As with many investment topics, however, there are some misconceptions about the benefits of a multi-manager approach. For example, it is often suggested that the
6
best ideas of the advisors are diluted when combined in one portfolio. Recent Vanguard research has found otherwise.
As always, thank you for investing with Vanguard.
Sincerely,


Dear Shareholder,
Dividend-paying stocks, which outshone the broad U.S. stock market in the two fiscal years previous to the one just ended, didn’t match the market’s swift pace in the recent period. For the 12 months ended October 31, 2013, Vanguard High Dividend Yield Index Fund returned 24.35% for Investor Shares and 24.43% for ETF Shares based on net asset value.
The fund’s return was in line with that of its benchmark, the FTSE High Dividend Yield Index, and led the average return of equity income peer funds by about 1 percentage point. However, the fund trailed the broad U.S. stock market by almost 5 percentage points.
As of October 31, the 30-day SEC yield of the fund’s Investor Shares was 2.96%, compared with the 1.73% yield of the broad stock market as measured by the Investor Shares of Vanguard Total Stock Market Index Fund. All ten industry sectors registered double-digit gains, with industrials and financials leading the way.
If you own shares of the fund in a taxable account, you may wish to review the fund’s after-tax returns presented later in this report.
2
Stocks found a path to strong returns
U.S. stocks faced several challenges en route to an impressive return of about 29% for the 12 months ended October 31, 2013. Investors’ growing appetite for risk drove the rise in stocks, as corporate profit growth, in general, wasn’t particularly tantalizing.
Outside of the United States, stocks returned about 20%. The developed markets of Europe and the Pacific region delivered robust gains; emerging-market stocks failed to keep pace.
 
3
Bond returns suffered as investors kept an eye on the Fed
With investors fretting over the Fed’s next move in its stimulative bond-buying program, bonds recorded negative results for the 12 months. The broad U.S. taxable bond market returned –1.08%. The yield of the 10-year Treasury note closed at 2.54%, down from 2.63% at September’s close, but up from 1.69% at the end of the last fiscal year. (Bond yields and prices move in opposite directions.) Municipal bonds returned –1.72%.
Outside of the United States, bond markets (as measured by the Barclays Global Aggregate Index ex USD) returned –1.95%.
The Fed’s target for short-term interest rates remained at 0%–0.25%, severely limiting returns of money market funds and savings accounts.
Dividend payers cooled off as the investing climate changed
In the two fiscal years previous to the one just ended, yield-hungry, risk-averse investors sought dividend-oriented stocks as the stock market was often jittery and the economic recovery uncertain. Dividend-paying funds mostly invest in stocks of large companies, which tend to withstand market volatility better than their smaller, less established counterparts.
4
However, the investing landscape changed during the recent fiscal year, when the stock market surged far more than it stumbled. Investors grew more comfortable with risk and looked less to dividend payers and more toward growth opportunities.
Vanguard High Dividend Yield Index Fund and the broad U.S. stock market had nearly identical returns for the fiscal year’s first six months. However, the fund trailed the broad market—for the first time since fiscal 2010—by several percentage points in the period’s second half. Of course, this underperformance does nothing to change our view that long-term exposure to high dividend-paying stocks can play a part in the well-diversified portfolio of an investor seeking equity income.
Industrial stocks, a traditional dividend source and the fund’s second-largest sector, advanced more than 40%. Gains occurred across the spectrum as corporate budgets increased, businesses expanded, and manufacturers kept producing. Diversified industrial giants, aerospace and defense corporations, support services firms, and transportation companies were among the sector’s leaders.
The fund’s financial stocks posted a return of about 35%; banks, asset managers, investment services firms, and insurance companies contributed most. The fertile investment climate, solid balance sheets, improved lending conditions, and a muted catastrophe environment boosted results.
5
The consumer goods, health care, technology, consumer services, and basic materials sectors all generated returns of 20% or more. A wide variety of industries and businesses took part in the stock market’s climb and benefited from the economy’s recovery. Although they lagged the field, oil and gas, utilities, and telecommunications still recorded double-digit gains.
The fund has made strides with each passing year
Just about every mutual fund’s record improves as we move further away from the 2008–2009 financial crisis. This effect is particularly noticeable in the High Dividend Yield Index Fund, which launched in November 2006 and suffered a –32% decline in fiscal 2008—its second year of existence.
The fund, which has now notched positive results in six of its seven fiscal years and double-digit returns in five, has an average annual return of 5.60% from inception through October 31, 2013. Its benchmark (which bears no expenses) returned 5.84%, and its peer group’s average return was 4.91%.
Credit for the fund’s success in tightly tracking its benchmark goes to its advisor, Vanguard Equity Investment Group, which uses sophisticated portfolio construction and trading techniques to reach its goal. Investors in the fund were also able to keep more of their returns thanks to its low expenses.
Watching costs is especially important for investors seeking income, because expenses are deducted from the income generated by the fund’s holdings. The higher the expenses, the greater the share of income that gets diverted to pay them. Vanguard High Dividend Yield Index Fund had an expense ratio of 0.20% for Investor Shares. The average equity income mutual fund expense ratio was more than six times higher: 1.27%, according to data from Lipper as of December 31, 2012.
In challenging markets, index funds can be a solid portfolio foundation
As you know, index funds seek to capture the return of the market they track, minus only their operating expenses, which—at least at Vanguard—are typically very low. They do so by investing in an entire market or by relying on a sophisticated sampling technique.
I use the word “typically” because we believe that some actively managed funds, including Vanguard’s, can increase the odds of outperforming their benchmarks
6
Even so, investors in actively managed funds should expect and be comfortable with the extended periods of underperfor-mance that such funds can undergo. These challenges help further clarify the potential benefits of indexing. Well-run index funds can offer you virtually the market return, year in and year out.
In a portfolio that is diversified within asset classes and balanced among them, low-cost index funds can be the bedrock of a sound investment plan.
As always, thank you for entrusting your assets to Vanguard.
Sincerely,


Dear Shareholder,
From its inception on May 31, 2013, through October 31, the fund returned –1.38% for Investor Shares and about the same for Admiral and ETF Shares. This result outpaced the average return of the fund’s peer group, emerging markets hard currency debt funds. (A “hard currency” is one that is issued by an economically and politically stable country and is widely used around the world as payment for goods and services.)
Your fund closely tracked the –1.30% return of its benchmark, the Barclays USD Emerging Markets Government RIC Capped Index, which of course incurs no expenses. The fund’s close index tracking in its first few months is a testament to the skill and experience of Vanguard Fixed Income Group, the fund’s advisor.
Barclays constructs and manages your fund’s custom index with caps, or limits, on certain holdings. The intent is to comply with Internal Revenue Service diversification standards for regulated investment companies, or RICs, which
2
include mutual funds. In seeking to track the index, the advisor uses a sampling strategy, holding a range of securities that in total approximate the index’s key risk factors and characteristics, such as average duration and credit quality. Sampling is a common practice for bond index funds because it can be very difficult and potentially costly to own all the bonds in an index.
This report includes a brief look at the financial markets over the full 12 months of the fund’s fiscal year, followed by a review of the fund’s performance since May 31.
Before moving to that discussion, however, I want to mention an important transition at Vanguard. As we announced in early November, Robert F. Auwaerter, principal
and head of Vanguard Fixed Income Group, intends to retire in March 2014. At the conclusion of this letter, I’ll have more to say about the important contributions Bob made to Vanguard in his 32-year career, and I’ll introduce his successor, Gregory Davis.
Bond returns suffered as markets worldwide kept an eye on the Fed
With investors around the world fretting over the U.S. Federal Reserve’s next move in its stimulative bond-buying program, bonds recorded negative results for the 12 months ended October 31, 2013.
Outside the United States, bond markets (as measured by the Barclays Global Aggregate Index ex USD) returned
 
3
–1.95%, trailing the broad U.S. taxable bond market. International bonds generally traced a bumpy path similar to that of U.S. bonds, in part reflecting the ebb and flow of concerns about when Fed tapering might begin—and its implications for global economic growth.
The broad U.S. taxable bond market returned –1.08%. The yield of the 10-year Treasury note closed at 2.54%, up nearly a full percentage point from 1.69% at the end of October 2012. (Bond yields and prices move in opposite directions.) Municipal bonds returned –1.72%.
The Fed’s target for short-term interest rates remained at 0%–0.25%, severely limiting returns of money market funds and savings accounts.
Stocks in developed markets found a path to strong returns
Stocks outside the United States returned about 20% in aggregate. The developed markets of Europe and the Pacific region delivered robust gains. In Japan, the pro-growth, weaker-yen policies of Prime Minister Shinzo Abe have sparked an economic revival that helped propel the stock market to return almost 65% in yen, and about half that for U.S. dollar-based investors. Emerging-market stocks failed to keep pace with those of developed economies.
U.S. stocks faced several challenges en route to an impressive return of about 29%. Investors’ growing appetite for risk
4
drove the rise in stocks, as corporate profit growth, in general, wasn’t particularly tantalizing.
Although the final month of the fiscal year was notable for the budget impasse that resulted in a partial shutdown of the federal government for 16 days, the period as a whole was marked by uncertainties about the Fed’s monetary policy and concern about the economy’s patchy growth. Nonetheless, as Vanguard’s chief economist, Joe Davis, noted, the U.S. economy continued to expand, albeit at a modest and uneven pace.
Markets large and small felt the broad reach of Fed policy
That said, using calculations based on Barclays indexes, Vanguard estimates that emerging market bonds represent only a small slice—about 6%—of the global fixed income market. And bonds issued in local currencies constitute the majority of emerging market debt. Thus, the U.S. dollar-denominated bonds that your fund invests in are a small segment within a small market—but these securities represent some of the most accessible and frequently traded of all emerging market bonds.
Amid an environment of low yields at home and muted expectations from traditional fixed income investments, investors have increasingly turned to the higher yields offered by emerging-market securities. For example, on October 31, the 30-day SEC yield of your fund’s Investor Shares was 4.3%, more than double the yield of Vanguard Total Bond Market Index Fund. Higher yields, of course, typically reflect higher risks.
In May, the first hint that the Fed might begin to scale back its monthly bond purchases sent a chill through developing countries—which have benefited greatly from the Fed’s easy money policies in recent years. Aside from the lure of higher bond yields, many types of investors have
5
been attracted to emerging economies and their stocks for their seemingly attractive growth opportunities. Thus, the prospect of capital beginning to return home to the U.S. was unwelcome news. This made for an inauspicious start for the Emerging Markets Government Bond Index Fund, which—like its index—returned about –5% in June. Positive returns in some subsequent months could not offset the initial decline.
Still, the bonds of several countries notched gains. Argentina—about 1% of assets in the fund and its benchmark—was the standout, with a return of about 22% for its dollar-denominated government bonds. Notwithstanding unresolved court cases related to Argentina’s debt default in 2001, investors were attracted to double-digit yields on the country’s bonds.
At the other end of the spectrum, bonds from Turkey, Ukraine, and Honduras had some of the weakest returns, in the range of about –6% to –13%. And those of Russia and Brazil—two of the largest country holdings—had returns of approximately 0% and –2%, respectively.
Although your fund has only a five-month history, we are pleased by its initial success in closely tracking the return of its benchmark index. As always, however, we encourage you to evaluate results over a long-term horizon.
The role of international bonds in a diversified portfolio
Bonds issued in developed markets outside the United States have long been a meaningful slice of worldwide capital markets, while emerging market bonds have been a much smaller, but fast-growing, slice. Consistent with Vanguard’s principles of balance, diversification, and broad market exposure, it would have been theoretically appropriate to offer an international bond fund before now. However, several practical obstacles to buying non-U.S. bonds—including illiquidity, high trading and currency-hedging costs, and difficulties in navigating foreign credit markets—kept them absent from Vanguard’s fund lineup.
That changed with the recent growth and maturation of bond markets abroad, accompanied by lower currency-hedging costs, further globalization of businesses, greater capital flows, and better information access. Non-U.S. bonds grew from less than 20% of the world’s capital markets—stocks and bonds combined—in 2000 to approximately 33% at year-end 2012, the largest global asset class by a wide margin.
Vanguard research has shown that international bonds have the potential to reduce the volatility of portfolio returns, providing a diversification benefit similar to that expected from international stocks. To help make this important asset class
6
available to investors, in May we launched Vanguard Total International Bond Index Fund, which includes modest exposure to emerging market bonds.
For U.S.-based investors seeking greater exposure, Vanguard Emerging Markets Government Bond Index Fund can play a supporting role in a diversified portfolio. It offers a low-cost opportunity to participate in the potentially higher yields and improving economic fundamentals of developing economies by investing in some of their U.S. dollar-denominated bonds.
Bob Auwaerter’s retirement marks the end of a remarkable era
In mid-September 2008, about two weeks after I succeeded Jack Brennan as Vanguard’s chief executive officer, Lehman Brothers went bankrupt, igniting the nation’s worst financial crisis in 70 years. It was, to put it mildly, an extremely challenging time.
Through it all, I was able to depend on Bob Auwaerter’s strong command of the Fixed Income Group, which persevered under these treacherous conditions. Although that was a difficult period for Vanguard and the industry, it was far from the only time I was grateful to have Bob at the helm of our bond group.
Bob, who joined Vanguard in 1981, was an original member of the three-person Fixed Income Group, headed by Ian MacKinnon. Over the years, he held various leadership roles in the department, and he eventually succeeded Ian as its head in 2003. He earned a reputation at Vanguard and within the industry as an extremely dedicated, honest, and insightful decision-maker and leader.
The Fixed Income Group that Bob helped start had total assets of about $1.3 billion in seven funds. He tracked his positions in the two funds he managed on index cards stored in a small metal box. Thirty-two years later, the 120-person group oversees $750 billion, which represents nearly one-third of Vanguard’s assets under management.
On behalf of our clients, I thank Bob for more than three decades of exemplary service and wish him the best in his retirement.
7
We’re fortunate that Greg Davis will become the head of the Fixed Income Group. Greg currently serves as chief investment officer for the Asia-Pacific region and as a director of Vanguard Investments Australia. He joined Vanguard in 1999 and had been head of bond indexing and a senior portfolio manager in the Fixed Income Group. Greg is an eminently qualified successor and has a strong commitment to the Vanguard way of investing. I couldn’t be more confident in his ability to lead the Fixed Income Group and its deep and talented team.
As always, thank you for investing with Vanguard.
Sincerely,


