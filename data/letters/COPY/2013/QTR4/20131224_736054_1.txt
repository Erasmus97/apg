Dear Shareholder,
For the 12 months ended October 31, 2013, Vanguard STAR Fund returned 17.36%, propelled by the robust performance of its underlying equity funds. The fund’s return was ahead of its benchmark—the STAR Composite Index, a static mix of about 44% U.S. stocks, 19% international stocks, and 37% U.S. bonds—and on par with a composite of the average returns of peer groups for its underlying funds.
The STAR Fund is a “fund of funds” that consists of 11 actively managed Vanguard funds: eight stock funds and three bond funds. All the stock funds posted returns of more than 25% for the 12 months. Vanguard Explorer™ Fund had the highest result, surging 42.89%, as small-capitalization growth stocks led the U.S. stock market’s impressive advance. (Fund returns cited in this letter are for Investor Shares.)
The STAR Fund’s underlying bond funds fared considerably worse than their stock counterparts. Reflecting an especially challenging environment for long-term bonds, Vanguard Long-Term Investment-Grade Fund had the worst result, returning –6.06%.
2
Investors’ growing appetite for risk drove the rise in stocks, as corporate profit growth, on the whole, wasn’t particularly tantalizing.
Although the end of the fiscal year was notable for the budget impasse that resulted in October’s 16-day partial federal government shutdown, the period as a whole was marked by uncertainty about Federal Reserve monetary policy and concern about the economy’s patchy growth. Vanguard’s chief economist, Joe Davis, recently noted that “as was the case at the start of the year, the U.S. economy continues to expand at a modest and uneven pace.”
The disparity between the performance of the U.S. economy and that of U.S. stocks may seem surprising—but Vanguard research has shown a weak relationship over the long term between a nation’s economic growth and its stock returns. (You can read more in The Outlook for Emerging Market Stocks in a Lower-Growth World, available at vanguard.com/research.)
Outside the United States, stocks returned about 20%. The developed markets of Europe and the Pacific region delivered sizable gains; emerging-market stocks failed to keep pace.
 
3
The Fed’s target for short-term interest rates remained between 0% and 0.25%, severely limiting returns of money market funds and savings accounts.
The acquired fund fees and expenses—drawn from the prospectus dated February 28, 2013—represent an estimate of the weighted average of the expense ratios and any transaction fees charged by the underlying mutual funds (the ”acquired” funds) in which the STAR Fund invests. The STAR Fund does not charge any expenses or fees of its own. For the fiscal year ended October 31, 2013, the annualized acquired fund fees and expenses were 0.34%.


Dear Shareholder,
U.S. and international stock markets produced exceptionally strong gains for the fiscal year ended October 31, 2013. Bonds were another story, with many fixed income categories finishing in negative territory.
Despite weakness in global bond markets, the four Vanguard LifeStrategy Funds produced positive returns ranging from about 4% to about 20%. The funds’ results were generally on par with their composite indexes but trailed the average return of their composite peer groups.
As you would expect, the funds with the heaviest allocation to stocks performed best in this environment. Short-term challenges notwithstanding, we continue to believe that bonds are a good cushion to equity market volatility, and that a higher allocation to bonds can reduce the total risk in your portfolio over the long term.
Although the end of the fiscal year was notable for the budget impasse that resulted in October’s 16-day partial federal government shutdown, the period as a whole was marked by uncertainty about
2
Federal Reserve monetary policy and concern about the economy’s patchy growth. Vanguard’s chief economist, Joe Davis, recently noted that “as was the case at the start of the year, the U.S. economy continues to expand at a modest and uneven pace.”
The disparity between the performance of the U.S. economy and that of U.S. stocks may seem surprising—but Vanguard research has shown a weak relationship over the long term between a nation’s economic growth and its stock returns. (You can read more in The Outlook for Emerging Market Stocks in a Lower-Growth World, available at vanguard.com/research.)
Outside the United States, stocks returned about 20%. The developed markets of Europe and the Pacific region delivered robust gains; emerging-market stocks failed to keep pace.
 
3
Outside the United States, bond markets (as measured by the Barclays Global Aggregate Index ex USD) returned –1.95%.
The Fed’s target for short-term interest rates remained between 0% and 0.25%, severely limiting the returns of money market funds and savings accounts.
How each of the LifeStrategy Funds performed over the 12 months was largely influenced by its level of exposure to stocks. The LifeStrategy Growth Fund, with its allocation of about 80% stocks and 20% bonds, returned about 20%—its best fiscal year result in a decade. In contrast, the LifeStrategy Income Fund—the most conservative fund in the series, with about 20% stocks and 80% bonds—returned about 4%, compared with about 6% for the previous fiscal year. The results of the two other LifeStrategy Funds fell in between.
Each fund invests in four underlying funds—two stock index funds and two bond index funds. Among those, Vanguard Total Stock Market Index Fund was the
The fund expense figures shown—drawn from the prospectus dated August 12, 2013—represent an estimate of the weighted average of the expense ratios and any transaction fees charged by the underlying mutual funds (the ”acquired” funds) in which the LifeStrategy Funds invest. The LifeStrategy Funds do not charge any expenses or fees of their own. For the fiscal year ended October 31, 2013, the annualized acquired fund fees and expenses were 0.14% for the LifeStrategy Income Fund, 0.15% for the LifeStrategy Conservative Growth Fund, 0.16% for the LifeStrategy Moderate Growth Fund, and 0.17% for the LifeStrategy Growth Fund.


Dear Shareholder,
International equity markets advanced strongly in the 12 months ended October 31, 2013, primarily driven by European and Japanese stocks.
For the fiscal year, Vanguard Total International Stock Index Fund returned 20.37% for Investor Shares—more than triple the 5.32% return a year earlier—with similar gains for its other share classes. The fund’s return was in line with that of its benchmark index but slightly behind the average return of its international peers.
If you own shares of the fund in a taxable account, you may wish to review the table of after-tax returns that appears later in this report.
Also, please note that on October 16, we announced plans to streamline Vanguard’s share-class offerings by phasing out Signal Shares. Your fund’s Signal Shares will be converted to Admiral Shares by October 2014.
Although the end of the fiscal year was notable for the budget impasse that resulted in October’s 16-day partial federal
2
government shutdown, the period as a whole was marked by uncertainty about Federal Reserve monetary policy and concern about the economy’s patchy growth. Nonetheless, as Vanguard’s chief economist, Joe Davis, noted, the U.S. economy continued to expand, albeit at a modest and uneven pace.
The disparity between the performances of the U.S. economy and U.S. stocks may seem surprising, but Vanguard research has shown a weak relationship over the long term between a nation’s economic growth and its stock returns. (You can read more in The Outlook for Emerging Market Stocks in a Lower-Growth World, available at vanguard.com/research.)
Municipal bonds returned –1.72%. Outside the United States, bond markets returned –1.95%, as measured by the Barclays Global Aggregate Index ex USD.
 
3
The Fed’s target for short-term interest rates remained between 0% and 0.25%, severely limiting the returns of money market funds and savings accounts.
Japan was the single biggest contributor to the fund’s performance. Investors applauded Prime Minister Shinzo Abe’s aggressive stimulus program, which aims to end the country’s deflation and revive the economy. The program has significantly weakened the yen, leading to a strong rally in the stocks of export-oriented companies, notably auto makers and equipment manufacturers. Japanese banks also have done well as asset prices have risen and the economy has improved. The fund’s holdings in Japan returned about 34% for the period. Other developed markets in the Pacific region turned in solid but less impressive results.
The fund expense ratios shown are from the prospectus dated August 12, 2013, and represent estimated costs for the current fiscal year. For the fiscal year ended October 31, 2013, the fund’s expense ratios were 0.22% for Investor Shares, 0.14% for Admiral Shares, 0.14% for Signal Shares, 0.12% for Institutional Shares, 0.10% for Institutional Plus Shares, and 0.14% for ETF Shares. The peer-group expense ratio is derived from data provided by Lipper, a Thomson Reuters Company, and captures information through year-end 2012.
Peer group: International Funds.
4
Despite Europe’s economic challenges, investors showed confidence in the European Central Bank’s plans to stabilize financial institutions and stimulate growth in the region as it emerges from recession. European holdings in the fund turned in double-digit returns for the fiscal year, led by the United Kingdom, France, and Switzerland. Even countries like Ireland, Italy, Spain, Greece, and Portugal, which were hurt most by the sovereign-debt crisis, returned about 30% or more for the 12 months.
Emerging-market stocks failed to keep pace with those of developed markets. Investors were disappointed by slower growth in emerging economies, including China and Brazil. China, the largest of the emerging markets, produced a positive return for the period but contributed less than one percentage point to the fund’s overall result. Brazil and India, two other large emerging markets, posted declines.
Canadian stocks, which accounted for about 7% of fund assets, on average, returned about 6%. Canada’s financial and industrial sectors did well, but its materials sector declined sharply, partly because of weaker global demand for commodities.
 
The figures shown represent past performance, which is not a guarantee of future results. (Current performance may be lower or higher than the performance data cited. For performance data current to the most recent month-end, visit our website at vanguard.com/performance.) Note, too, that both investment returns and principal value can fluctuate widely, so an investor’s shares, when sold, could be worth more or less than their original cost.
5
an average annual return of 8.15% for Investor Shares, which exceeded the yearly average for its peer group.
As you can see in the table on page 5, the fund closely tracked its benchmark index’s return over those years. Credit for this success goes to the investment advisor, Vanguard Equity Investment Group, whose experience and sophisticated portfolio tracking techniques enabled the fund to capture the returns of its benchmark while keeping the costs of doing that very low.
In actively managed funds, by contrast, advisors troll the markets and reel in selected stocks or bonds that their analysis suggests will allow them to outperform their benchmarks. As we show in The Case for Index Fund Investing, a paper available at vanguard.com/research, there’s typically a big gap between hope and reality: Consistent outperformance by any one active manager has been rare.
I use the word “typically” because we believe that some actively managed funds, including Vanguard’s, can increase the odds of outperforming benchmarks over the long term. As demonstrated in a companion paper, The Case for Vanguard Active Management: Solving the Low-Cost/Top-Talent Paradox? (also at vanguard.com/research), the most reliable quantitative indicator of future manager success is low expenses—a Vanguard hallmark. And, as the paper’s title implies, finding talented advisors is a key aspect of active investing. We believe we have the process in place to identify the best.
Even so, investors in actively managed funds should expect, and be comfortable with, the extended periods of under-performance that such funds can undergo. The challenges of active investing help further clarify the potential benefits of indexing. Well-run index funds can offer you virtually the market return, year in and year out. In a portfolio that is diversified within asset classes, and balanced among them, low-cost index funds can be the bedrock of a sound investment plan.
Thank you for entrusting your assets to Vanguard.
Sincerely,


Dear Shareholder,
Developed markets produced strong returns overall for the 12 months ended October 31, 2013, largely because of brighter prospects for Japan and Europe. Markets across the globe were roiled at times by concerns about U.S. fiscal and monetary policy, flare-ups in the European debt crisis, and signs of weakening growth in China. However, measures taken by the Japanese government to kick-start its economy and the Eurozone’s emergence from recession lifted investor sentiment and pushed developed-market stocks up significantly.
Vanguard Developed Markets Index Fund returned more than 26% for the fiscal year, a return that was in line with that of the fund’s target index and roughly 4 percentage points more than the average return of peer funds.
If you own the fund in a taxable account, you may wish to review the section on after-tax returns that appears later in this report.
2
and is expected to benefit shareholders of both funds by spreading fixed expenses over the larger asset base of the combined funds. The two funds share the same benchmark, the FTSE Developed ex North America Index, and they have identical expense ratios.
The merged fund will be renamed Vanguard Developed Markets Index Fund, a more appropriate name since the fund will be suitable for investors in nontaxable and tax-deferred accounts.
Until the merger has been completed, the Developed Markets Index Fund will be closed to new accounts. However, no restrictions are being placed on additional investments by the fund’s current shareholders.
Although the end of the fiscal year was notable for the budget impasse that resulted in October’s 16-day partial shutdown of the federal government, the period as a whole was marked by uncertainties about Federal Reserve monetary policy and concern about the economy’s patchy growth. Vanguard’s chief economist, Joe Davis, recently
 
3
noted that “as was the case at the start of the year, the U.S. economy continues to expand at a modest and uneven pace.”
The disparity between the performance of the U.S. economy and that of U.S. stocks may seem surprising—but Vanguard research has shown that over the long term, a nation’s economic growth has a weak relationship with its stock returns. (You can read more in The Outlook for Emerging Market Stocks in a Lower-Growth World, available at vanguard.com/research.)
Outside of the United States, stocks returned about 20%. The developed markets of Europe and the Asia-Pacific region delivered robust gains; emerging-market stocks failed to keep pace.
Outside of the United States, bond markets (as measured by the Barclays Global Aggregate Index ex USD) returned –1.95%.
The fund expense ratios shown are from the prospectus dated August 12, 2013, and represent estimated costs for the current fiscal year. For the fiscal year ended October 31, 2013, the expense ratios were 0.20% for Investor Shares, 0.09% for Admiral Shares, 0.07% for Institutional Shares, and 0.06% for Institutional Plus Shares. The peer-group expense ratio is derived from data provided by Lipper, a Thomson Reuters Company, and captures information through year-end 2012.
Peer group: International Funds.
4
The Fed’s target for short-term interest rates remained at 0%–0.25%, severely limiting returns of money market funds and savings accounts.
Among the big players, the standout performer was Japan. Investors applauded “Abenomics”—newly elected prime minister Shinzo Abe’s aggressive stimulus program aimed at ending deflation and breathing new life into one of the world’s largest economies. This program resulted in a significant weakening of the yen, which led to a strong rally in stocks of export-oriented companies, particularly automakers and equipment manufacturers. Japanese bank stocks also shot up, as asset prices rose and the outlook for the Japanese economy improved. For the period, the fund’s holdings in this country returned about 35%.
Other developed markets in the Asia-Pacific region generally produced solid but more modest returns.
Investors seemed to take heart from some tentative signs of improvement in Europe. Many structural problems weighed on the region’s economy, including very high levels of unemployment and a drag from some countries’ austerity measures.
 
The figures shown represent past performance, which is not a guarantee of future results. (Current performance may be lower or higher than the performance data cited. For performance data current to the most recent month-end, visit our website at vanguard.com/performance.) Note, too, that both investment returns and principal value can fluctuate widely, so an investor’s shares, when sold, could be worth more or less than their original cost.
5
However, the Eurozone’s emergence from recession and an apparent stabilization of the banking crisis fueled a sharp rise in stocks. France and Germany fared well, returning more than 30% each, as did some of the economies more directly affected by the crisis (Spain and Italy, for example). The United Kingdom, which accounts for about one-fifth of the fund’s assets, returned 21%.
Within Europe, many banks and insurance companies saw a strong rebound in their stock prices. Other bright spots were automakers and mobile communication companies.
Unsurprisingly, given the soft global demand for energy and commodities, some of the weaker performances across the developed markets came from oil and gas, mining, and utility stocks.
Credit for this close tracking of the expense-free benchmark goes to the fund’s advisor, Vanguard Equity Investment Group, whose deep experience and sophisticated portfolio-tracking techniques have enabled the fund to capture the returns of its benchmark while keeping the associated costs very low.
In actively managed funds, by contrast, advisors troll the markets and reel in selected stocks or bonds that their analysis suggests will allow them to outperform their benchmarks. As we show in The Case for Index-Fund Investing, a paper available at vanguard.com/research, there’s typically a big gap between hope and reality: Consistent outperformance by any one active manager has been rare.
I use the word typically because we believe that some actively managed funds, including Vanguard’s, can increase the odds of outperforming benchmarks over the long term. As demonstrated in a companion paper, The Case for Vanguard
6
Active Management: Solving the Low-Cost/Top-Talent Paradox? (also available at vanguard.com/research), the most reliable quantitative indicator of future manager success is low expenses—a Vanguard hallmark. And, as the paper’s title implies, finding talented advisors is a key aspect of active investing. We believe we have the process in place to identify the best.
Even so, investors in actively managed funds should expect, and be comfortable with, the extended periods of underper-formance that such funds can undergo. The challenges of active investing help further clarify the potential benefits of
indexing. Well-run index funds can offer you virtually the market return, year in and year out. In a portfolio that is diversified within asset classes, and balanced among them, low-cost index funds can be the bedrock of a sound investment plan.
As always, thank you for entrusting your assets to Vanguard.
Sincerely,


