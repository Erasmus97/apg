Dear Fellow Shareholders:



We are pleased to report U.S. stocks continued their ascent in Value Funds performance was ahead of its benchmarks (S&amp;P Russell for all of the periods ending September from one-year to Since Inception (December guess if you told most investors at the start of U.S. stocks would be up about by the end of September, they would have taken that result in a heartbeat.This is not to imply the path higher has been smooth and without some harrowing twists and turns.Indeed, investors find themselves yet again on tenterhooks as the latest version of the Washington budget follies plays out.







Annualized returns ending


Value


Russell


S&amp;P



September


Total Return


Index


Index



One-year












Two-years












Three-years












Five-years












Ten-years












Since Inception (December















The Funds Gross Expense Ratio and Net Expense Ratio were and respectively, according to the Prospectus dated January February the Adviser has contractually agreed to waive its management fee and/or reimburse the Funds other expenses.Investment performance reflects waivers in effect.In the absence of such waivers, total return would be reduced.



Performance data quoted represents past performance; past performance is no guarantee of future results.The investment return and principal value of an investment will fluctuate so that an investors shares, when redeemed, may be worth more or less than their original cost.Current performance of the fund may be lower or higher than the performance quoted.Performance data current to the most recent month-end may be obtained by calling fund imposes a redemption fee on shares held less than days.Performance data quoted does not reflect the redemption fee.If reflected, total returns would be reduced.










The performance data quoted assumes the reinvestment of capital gains and income distributions. The performance does not reflect the deduction of taxes that a shareholder would pay on Fund distributions or the redemption of Fund shares.













The Russell Index is an unmanaged, capitalization-weighted index generally representative of the overall U.S. stock market. This Index cannot be invested in directly.













The S&amp;P Index is an unmanaged, capitalization-weighted index generally representative of the U.S. market for large capitalization stocks. This Index cannot be invested in directly.






Regarding the current budget crisis and accompanying government shutdown, wed make a couple observations.First, investors appear to have become somewhat immunized against the seemingly never ending series of debt ceiling-default/credit rating decline/budget/fiscal cliff crises caused by the highly polarized and massively dysfunctional state of our political situation.Its unfortunate, but weve seen this movie where our polit-



























KIRR, MARBACH PARTNERS






VALUE FUND




ical leaders take our country to the precipice several times now.Second, signs of strength in the U.S. economy continue to improve and broaden, which could make it less susceptible to outside shocks.Third, unlike our political crises during the European Debt Crisis seems to have calmed.Finally, weve stated before we think our excellent long-term record demonstrates were very good at analyzing companies and finding stocks selling at a discount to intrinsic value.Indeed, this type of micro-analysis is the basis of value investing.However, while we certainly pay attention to the big-picture, weve never spent a lot of time trying to forecast unknowable macro-factors, such as the outcome of political processes.



We are also proud Value Fund has continued to receive national recognition.Value Fund was ranked as the Midsize-Company stock fund in the September issue of Kiplingers Personal Finance, based on the funds performance for the three-years ending June was the second consecutive year Value Fund was recognized by Kiplingers.We are big believers in eating our own cooking and are invested alongside our fellow shareholders.You cant spend national recognition, but we think it validates we can compete very effectively against the biggest and best in our industry.



Given the strong move higher, we think the overall U.S. stock market is approaching fair value.While it is becoming more challenging to find stocks with attractive risk/reward characteristics, we still like what we own and remain fully invested.Additionally, we still hope to be able to find and exploit value in special situations like spin-offs and reorganizations.We understand the markets are always subject to headline risk.However, while it seems the bad stuff gets all of the media attention, we think there are positive undercurrents that are being undervalued by investors.



the Role of Confidence



We think Howard Marks, Chairman and co-founder of Oaktree Capital Management, is a bright guy and makes a lot of sense.The heading of this section is the title of Marks most recent memo to his clients.Marks said:



I have long been impressed by the role of confidence in an economy.In fact, Ive written in the pastexaggerating only slightlythat sometimes I think confidence is all that matters.I consider its impact to be significant, pervasive, self-reinforcing and self-fulfilling.



The primary impact of confidence on the economy is simple.If people think the economic future will be good, theyll spend and investthus things will be good.










Consumers optimism will translate into incremental demand for goods, adding to GDP.













Consumer buying will convince businesses to invest in expanded facilities and additional workers in order to keep demand growing.













Businesses investment in plant and workers will add to GDP.













Newly hired workers will have money to spend, and their buying will add further to the cycle.













The reports of confidence-fueled increases in GDP and other positive mentions of the economy in the media will reinforce this virtuous circle of optimism: back to step one.






So, just like the wealth effect, increased confidence makes people and businesses spend more, and this in turn cycles back into the economy.Confidence leads to spending; spending strengthens the economy; and economic strength buttresses confidence.Its a circular, self-fulfilling prophecy.


























KIRR, MARBACH PARTNERS






VALUE FUND




Marks also notes confidence tends to swing like a pendulum between optimism and pessimism.At the optimistic extreme, only good outcomes are possible and there is nothing but blue sky ahead.At the negative extreme, only bad outcomes are possible and doom and gloom are pervasive.As noted above, both optimism and pessimism can be self-fulfilling as positive and negative feedback loops push the pendulum farther in one direction or the other.



Five years ago, at the depth of the financial crisis, the pendulum had swung about as far to the pessimistic side as possible.Armageddon was at our doorstep as we stared down into the abyss.The pendulum has gradually begun to swing back towards normal.Surveys indicate private sector business leader confidence continues to increase.Similarly, consumer confidence is growing.U.S. household net worth has reached an all-time high as stock and housing prices have gained.At the same time, the U.S. household debt service burden has reached an all-time low as Americans paid down debt and took advantage of historically low borrowing rates.Finally, gasoline prices have dropped sharply.AAA reports the current national average for regular is down from a year ago.According to Business Insider, as a rule of thumb, each penny change in a gallon of gas translates into about billion, so a drop of cents represents a huge boost to consumer pocketbooks.



Update on the Feds Tapering



In late June, Fed Chairman Bernanke indicated the Fed was becoming more confident in the sustainability of the economic recovery.Further, if subsequent data was consistent with the Feds expectation of continued improvement, then the Fed would be in a position to curtail (later in and eventually eliminate (sometime in its monthly purchases of US Treasury and mortgage-backed bonds (aka quantitative easing).In other words, the Fed signaled to investors the economic conditions precedent to the tapering of its emergency, crisis-induced program of quantitative easing.



The yield on the U.S. Treasury bond rose steadily from about just prior to the announcement to in early September, as stronger economic data led to the widespread expectation the Fed would announce the start of the tapering after its meeting on September mortgage rates surged more than a full percentage point during this same period.



In a surprising move, the Fed announced it had decided not to start tapering.Its possible the Fed blew it by mis-signaling its intentions to investors.Alternatively, perhaps the Fed determined the economic data was not yet strong and/or consistent enough to warrant a change.Its also conceivable the Fed saw the budget train wreck about to occur in less than two weeks and decided not to act.



Percent Change in Top Ten Holdings from Book Cost (as of










Portfolio Recovery Associates, Inc.









Liberty Media Corporation-A









NCR Corporation









WABCO Holdings, Inc.









Alliance Data Systems Corp.









LyondellBasell Industries NV









Rosetta Resources, Inc.









Innospec, Inc.









Ascent Capital Group LLC









Cognizant Technology Solutions









Performance quoted represents past performance and is no guarantee of future results.



Fund holdings and sector allocations are subject to change and are not recommendations to buy or sell any security.




























KIRR, MARBACH PARTNERS






VALUE FUND




Summary



While were pleased with our absolute and relative performance for the fiscal year ending September our goal isnt to generate great performance for one year; its to generate outstanding performance over the next years.In other words, we view investing as a marathon, not a sprint.



As Marks says, Its easier to know what to do at the extremes than it is in the middle ground, where I believe we are today.When theres nothing clever to do, the mistake lies in trying to be clever.Today it seems the best we can do is invest prudently in the coming months, avoiding aggressiveness and remembering to apply caution.We couldnt agree more.



Regards,










Mark D. Foster, CFA


Mickey Kim, CFA



President


Vice-President, Treasurer and Secretary









Value Fund invests in foreign securities, which involves greater volatility and political, economic and currency risks and differences in accounting methods.Value Fund

may also invest in small- and medium-capitalization companies, which tend to have more limited liquidity and greater price volatility than large-capitalization companies.



Past performance is not a guarantee of future results.



Please refer to the Schedule of Investments for complete fund holdings information.



The information provided herein represents the opinion of Value Funds investment adviser and is not intended to be a forecast of future events, a guarantee of future results, nor investment advice.



The ranking by Kiplingers Personal Finance was based on the average annual return for the period ending June for the Midsize-Company stocks fund category, a customized category created for Kiplingers Personal Finance by Morningstar.



This material must be preceded or accompanied by a current Prospectus.



Quasar Distributors, LLC is the Distributor for Value Fund.



For further information about Value Fund and/or an account application, please call Matt Kirr at Value Fund at or or write to Value Fund at Washington Street, Columbus,




























KIRR, MARBACH PARTNERS






VALUE FUND




Value of Investment (Unaudited)
