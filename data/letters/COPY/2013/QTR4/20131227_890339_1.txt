Dear Target Shareholder: 
 
We hope you find the annual report for Target informative and useful. The report covers performance for the 12-month period that ended October 31, 2013. 
 
We recognize that ongoing market volatility may make it a difficult time to be an investor. We continue to believe a prudent response to
uncertainty is to maintain a diversified portfolio of funds consistent with your tolerance for risk, time horizon, and financial goals. Whether you are looking for capital growth, current income, or a combination of both, the Target portfolios
feature a wide range of strategies to suit a variety of investment needs. 
 
Target is founded upon the belief that investment management talent is dispersed across a variety of firms and can be systematically identified through research.
The managers for each portfolio are carefully chosen from among the leading institutional money managers and are monitored by our team of experienced investment management analysts. Of course, the future performance of the Target portfolios cannot
be guaranteed. 
 
Your selections among the Target portfolios can evolve as
your needs change. Your financial professional can help you stay informed of important developments and assist you in determining whether you need to modify your investments. 
 
Thank you for your continued confidence. 
 
Sincerely, 


