Dear Shareholder:






We are pleased to present the annual report of the Glenmede
family of funds for the fiscal year ended
The volatility in the markets that has marked the last few years
continued through this fiscal year. The sovereign debt crisis in
Europe, the anticipated fiscal cliff, the U.S.Election and
worry about employment and the housing markets effects on
the economy all contributed to the volatility. Despite this,
equity and fixed income markets had positive returns for the
fiscal year ended International equities,
due to the more serious problems in Europe, had lower but
positive returns for the period. Money market funds suffered
very low yields as the Federal Reserve maintained short term
rates at near zero.





At the fiscal year end on The Glenmede
Fund, Inc. and The Glenmede Portfolios (collectively the
Glenmede Funds) consisted of nineteen portfolios
with total assets of The fund family includes
five International Portfolios; International is
sub-advised
by Philadelphia International Advisors (PIA).
Philadelphia International, Philadelphia International Small
Cap, and Philadelphia International Emerging Markets are advised
by PIA. These four portfolios account for of the fund
familys assets as of The
International Secured Options Portfolio was launched on
to provide an International product
similar to the domestic Secured Options Portfolio. This
Portfolio is advised by Glenmede Investment Management LP.





All of the Glenmede Funds, other than the money market funds,
are managed to seek long-term total returns consistent with
reasonable risk to principal for their asset category. Efforts
are made to keep expenses at competitive levels. All of the
portfolios managed by Glenmede Investment Management LP use a
quantitative style of investing.





This fiscal year ended showed positive
returns across all domestic asset classes in anticipation of
slow, but positive growth in the U.S.economy. The SP



gained and the Barclays Capital U.S.Aggregate Bond


gained for the fiscal year ended
The MSCI EAFE


performed worse than domestic stocks for the fiscal year gaining
hurt by the debt problems in Europe. Small Cap stocks did
not do as well as Large Cap stocks with the Russell


returning versus a return of on the Russell


for the fiscal year ended The Glenmede
Large Cap Value Portfolio achieved a four star (****)
Morningstar Overall


among Large Value Equity Funds for the period ended
(based on risk adjusted returns). The
Glenmede Large Cap Portfolio achieved a four star (****)
Morningstar Overall


among Large Blend Equity Funds for the period ended
(based on risk adjusted returns). The
Glenmede Large Cap Growth Portfolio achieved a four star (****)
Morningstar Overall


among Large Growth Equity Funds for the period ended
(based on risk adjusted returns).























In response to the continuing slow economy, the Federal Reserve
has maintained short term rates at since
The yield curve flattened during the
current fiscal year as one year maturity treasuries rose



in yield and the treasury dropped
points. Money Market fund yields remain at all time lows.





Returns in the fixed income markets were good across all
sectors, but especially in long corporate bonds which returned
as investors sought current yield. The Glenmede bond
portfolios all have a high quality bias. The Muni Intermediate
Portfolio achieved a four star (****) Morningstar Overall


among Muni National Short Funds for the period ended
(based on risk-adjusted returns). The
Morningstar Overall
Ratingtm

for a fund is derived from a weighted average of the performance
figures associated with its three-, five- and ten-year (if
applicable) Morningstar Rating metric.





We have featured the Strategic Equity Portfolio and included
Portfolio Highlights for each individual fund in this report. We
welcome any questions about the Glenmede Funds and thank our
clients for their continued support.





Sincerely,
