Dear Fellow Shareholder, 
On behalf of Transamerica Partners Institutional Funds Group and Transamerica Institutional Asset Allocation Funds, we would like to thank you for your continued support and confidence in our products as we look
forward to continuing to serve you and your financial adviser in the future. We value the trust you have placed in us. 
This annual report is provided
to you with the intent of presenting a comprehensive review of the investments of each of your funds. The Securities and Exchange Commission requires that annual and semi-annual reports be sent to all shareholders, and we believe this report to be
an important part of the investment process. In addition to providing a comprehensive review, this report also provides a discussion of accounting policies as well as matters presented to shareholders that may have required their vote. 
We believe it is important to understand market conditions over the last year in order to provide a context for reading this report. One year ago, fears that a
sovereign default or a large bank failure in Europe would precipitate a global crisis had driven U.S. and many international markets down. In February 2012, the European Central Bank responded by making unlimited loans available to European banks,
which used this money to honor deposits and to purchase sovereign bonds. As a result, market fear subsided and global economies improved modestly, helping markets to rally strongly throughout the first quarter of 2012. 
®
In addition to your active involvement in the investment process, we firmly believe that a financial adviser is a key resource to help you build a
complete picture of your current and future financial needs. Financial advisers are familiar with the market’s history, including long-term returns and volatility of various asset classes. With your financial adviser, you can develop an
investment program that incorporates factors such as your goals, your investment timeline, and your risk tolerance. 
Please contact your financial
adviser if you have any questions about the contents of this report, and thanks again for the confidence you have placed in us. 
 
The views expressed in this report reflect those of the portfolio managers only and may not necessarily represent the views of
the Transamerica Partners Institutional Funds Group and Transamerica Institutional Asset Allocation Funds. These views are subject to change based upon market conditions. These views should not be relied upon as investment advice and are not
indicative of trading intent on behalf of the Transamerica Partners Institutional Funds Group and Transamerica Institutional Asset Allocation Funds. 
(This page intentionally left blank) 
(This page intentionally left blank) 
Management Review 
Management Review 
 
(unaudited) 


