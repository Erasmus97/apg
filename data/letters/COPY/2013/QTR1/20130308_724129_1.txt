Dear Shareholders,

We are pleased to present this annual shareholder report for the SunAmerica
Money Market Funds, Inc./*/ for the month period ended December

For the money markets, the annual period was one wherein mixed U.S. and
international economic data, political uncertainty, Federal Reserve Board (the
"Fed") policy, and supply/demand conditions within the repurchase agreement and
U.S. Treasury securities markets combined to push money market yields lower.

On the whole, the annual period was marked by a slowing worldwide economy and a
low yield environment. The U.S. economy expanded moderately, notwithstanding
some apparent slowing in global growth, as domestic indicators pointed to
improvement in overall labor market conditions and advancing household
spending. Business fixed investment appeared to be increasing less rapidly.
Inflation moderated during the annual period, and longer-term inflation
expectations remained relatively stable. Throughout, the Fed maintained its
target range for the Federal Funds Rate at to In September the
Fed stated it continued to anticipate that U.S. economic conditions, including
low rates of resource utilization, subdued inflation trends and stable
inflation expectations, are likely to warrant exceptionally low levels of the
Federal Funds Rate through at least The Fed also decided, at its
September meeting, to increase monetary policy accommodation by purchasing
additional agency mortgage-backed securities at a pace of billion per month
via a third round of quantitative easing, dubbed In December, the Fed
committed to an open-ended purchase program of billion per month, inclusive
of the billion per month in continuing mortgage-backed securities purchases
plus billion per month of U.S. Treasury purchases. For the first time, the
Fed also replaced date-specific Federal Funds Rate guidance with open-ended
guidance based on unemployment and inflation data thresholds, wherein U.S.
unemployment would need to reach or below and inflation would need to
exceed a year before the Fed would raise interest rates.

With the Fed keeping the targeted Federal Funds Rate near zero throughout the
annual period and with no near-term indication of this changing, money market
yields remained anchored near zero as well, and the taxable money market yield
curve was extremely flat, meaning the difference between yields at the
short-term end of the money market yield curve and the longer-term end was
quite narrow. Indeed, with interest rates remaining near zero and with
securities even being offered at negative rates at times, the annual period did
not provide many opportunities to add yield.

There were also significant regulatory developments relating to tightening
money market funds regulation during the annual period. In August the
Securities and Exchange Commission ("SEC") chair was unable to persuade a
majority of the SEC Commissioners to vote in favor of its reform proposal to
tighten regulation of money market funds. Despite some resistance, in November
the Financial Stability Oversight Council ("FSOC") came forth with three
proposed alternative reform scenarios for money market funds. Proposals include
mandating floating net asset values, requiring a capital buffer to absorb
losses and imposing limits on redemptions. The immediate next steps were a
comment period on the FSOC recommendations, followed by a final
recommendation to the SEC in late January At the end of the annual
period, it was unclear whether any money market reform would be enacted anytime
in the near future. (THE FSOC ANNOUNCED ON JANUARY THAT IT HAD
EXTENDED THE COMMENT PERIOD FOR PROPOSED RECOMMENDATIONS FOR MONEY MARKET
MUTUAL FUND REFORM UNTIL FEBRUARY TO ALLOW THE PUBLIC MORE TIME TO
REVIEW, CONSIDER AND COMMENT ON THE PROPOSED RECOMMENDATIONS.)

On the following pages, you will find a brief discussion of the annual
period from the portfolio manager, as well as detailed financial statements and
portfolio information for the SunAmerica Money Market Fund for the annual
period ended December






DECEMBER ANNUAL REPORT

SHAREHOLDERS' LETTER -- (UNAUDITED)
