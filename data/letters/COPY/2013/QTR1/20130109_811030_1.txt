Dear Shareholders,




Equity market results have shown a continuation of the uptrend dating back to March as measured by the Russell Value and S&amp;P indices. For its fiscal year ending October the Becker Value Equity Fund (The Fund) gained trailing the Russell Value and S&amp;P indices, which returned and respectively.




Equity markets have been rising despite slowing global economic growth, a prolonged recession in Europe, and the so-called fiscal cliff. Expectations for US corporate revenue and operating earnings growth has been ratcheted down to what appears to be reasonable levels given current economic activity. Corporate America remains cautious given legislative uncertainty, while consumer confidence is, somewhat surprisingly, on the upswing.




Stock market performance has been good despite tepid economic growth and stubbornly high unemployment. The primary accelerant of stock price has been repeated assurances by our Federal Reserve and foreign central banks that stimulative policies will continue. Aggressive monetary policy, executed through the series of quantitative easing programs, have been enormously beneficial to stock valuations. As Government sponsored monetary stimulus programs wane, equity markets weaken only to reverse when a new program is initiated. Importantly, each subsequent stimulus program appears to be providing less economic invigoration. And, with the fiscal cliff looming, the Federal Reserve has fewer rabbits to pull from its hat. A domestic recession, while not imminent, could be lying in the weeds.




A review of your Funds past year performance indicates that both sector weighting and individual stock selection decisions impacted results versus the Russell Value Index. The Funds investment in what we believe are high quality, conservative financial stocks proved a drag on relative returns as investors rotated into higher risk names in the sector. In addition, our lack of exposure to retail stocks, which we believed to be over-valued, hurt relative returns. Conversely, the technology company holdings, especially Visa and Harris Corp., augmented returns as did our underweight in the poorly performing utility sector.




We continue to focus our efforts on what we believe are high quality, attractively valued companies with strong balance sheets that may be able to provide positive returns on invested capital in a wide range of global economic environments. The Fund established positions in several new names toward the end of the fiscal year, including Teck Resources, a Canadian metallurgical coal company. Teck is one of the lowest cost producers of high quality coal used in steel production. Historical solid free cash flow, a strong balance sheet and a valuation in the lowest quartile of its year historic range due to macro concerns, particularly regarding infrastructure growth in China, give this new holding tremendous appreciation potential.































Becker Value Equity Fund





In addition, the Fund initiated a position in Corning, one of the worlds largest glass companies. Again, a very strong global franchise, low cost and high cash flow generating business selling at a single digit price-to-earnings (P/E), a discount to book value and an EV/EBITDA in the lowest quartile of its historic trading range. We took profits in Amgen, Target, WalMart and Time Warner, among others, given their strong relative performance in the second half of the fiscal year. Economic and market risks remain high as we move into following strong equity market year-to-date performance and uncertainty surrounding global economic growth and its impact on corporate profitability.




Sincerely,
