Dear Shareholder,
Vanguard California Intermediate-Term Tax-Exempt Fund returned 10.14% for Investor Shares and 10.23% for Admiral Shares for the 12 months ended November 30, 2012. Over the same period, Vanguard California Long-Term Tax-Exempt Fund returned 13.20% for Investor Shares and 13.29% for Admiral Shares. Both funds exceeded the returns of their benchmark indexes by more than 1 percentage point.
The Intermediate-Term Fund outpaced the average return of its peers by almost 2 percentage points. The Long-Term Fund lagged its peers’ average return by more than 1 percentage point, in part because the maturity and credit quality of its holdings were more conservative.
Both funds performed strongly from a historical perspective, but keep in mind that, for reasons I’ll discuss later in this letter, we anticipate a more challenging environment in coming years for investors in both municipal and taxable bonds.
The California Tax-Exempt Money Market Fund, meanwhile, returned 0.03% for the period as short-term interest rates hovered just above zero.
As demand for municipal bonds drove prices higher, yields fell. (Bond yields and prices move in opposite directions.) The 30-day SEC yield for Investor Shares of the Intermediate-Term Fund dropped to 1.46%
2
as of November 30, 2012, from 2.63% a year earlier. For Investor Shares of the Long-Term Fund, the yield fell to 2.10% from 3.42%. The 7-day SEC yield of the Money Market Fund, which invests in short-term securities, inched up to 0.03% as of November 30, 2012, from 0.01% 12 months earlier.
Note: The funds are permitted to invest in securities whose income is subject to the alternative minimum tax (AMT). As of November 30, 2012, neither the Intermediate-Term Fund nor the Long-Term Fund owned securities that would generate income distributions subject to the AMT, but the Money Market Fund did.
Bonds notched solid results, but challenges lie ahead
The broad U.S. taxable bond market returned more than 5% for the 12 months. Municipal bonds performed more robustly, with returns of about 10%.
As bond prices rose, the yield of the 10-year U.S. Treasury note slipped to a record low in July, closing below 1.5%. By the end of the period, the yield had climbed, but it still remained exceptionally low by historical standards.
After years of relatively high bond returns, investors shouldn’t be surprised if future results are much more modest. The low yields mean the opportunity for similarly strong returns has diminished.
 
3
As it has since late 2008, the Federal Reserve held its target for short-term interest rates between 0% and 0.25%, which kept a tight lid on returns from money market funds and savings accounts. Shortly after the period closed, the Fed announced it would not boost interest rates until unemployment fell to 6.5% or lower, provided the long-term inflation outlook remains about 2% or less. Based on current Fed projections, short-term interest rates are expected to remain near zero into 2015.
Stocks weathered turbulence to record a healthy advance
Global stock markets seesawed through the 12 months ended November 30, ultimately finishing with double-digit gains. U.S. stocks led the way, returning about
16%, followed by European and emerging markets stocks. Stocks in the developed markets of the Pacific region had the smallest return but still rose about 10%.
The gains masked a significant level of apprehension during the period, with investors concerned about U.S. economic growth and the finances of European governments and banks. Worries about Europe, in particular, flared up in the spring and then quieted in the summer. The president of the European Central Bank declared in July that policymakers would do whatever was needed to preserve the euro common currency.
Although investors’ worries have eased, Europe’s financial troubles aren’t resolved. Vanguard economists believe the most
4
likely scenario is that the Eurozone will “muddle through” for several years, with occasional spikes in market volatility, as fiscal tightening persists in the face of weak economic growth.
As the period drew to a close soon after the U.S. elections, attention to the United States’s considerable budgetary challenges intensified. The near-term focus on the “fiscal cliff” led to unsettling headlines and nervousness in the markets. But it also sparked serious debate about spending
and tax policy options, and such attention could prove a first step to a long-term solution that resolves the nation’s fiscal imbalance and opens the way for growth.
Investors’ search for yield helped boost muni returns
California has struggled with many of the same postrecession economic challenges facing the rest of the country. As the chart on page 6 illustrates, total state tax collection for the states rebounded from recession lows, but the pace of growth has
5
been trending down in recent quarters, and California has fared even worse because of a sharp drop in corporate income tax revenues. And California’s payrolls, though rising at a good clip lately, remain well below prerecession levels; that depresses revenues from personal income and sales taxes. The November 6 passage by California voters of two tax measures, Propositions 30 and 39, was a step toward addressing some of the state’s fiscal challenges.
On the cost side, shrinking revenues and increasing pension obligations left the state and local governments with little appetite to take on new capital projects. Many did, however, strengthen their finances by refinancing debt at today’s exceptionally low interest rates. As a result, total California tax-exempt bond issuance rose for the 12-month period, but much of it represented refinancing activity.
 
