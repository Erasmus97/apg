Dear Praxis shareholder: 
While the U.S. elections are behind us, and the “fiscal cliff” has been averted, the uncertainty continues, knowing that many of the systemic problems with
entitlements and potential tax code revisions have not been resolved. Additionally, the need to raise the national debt limit in the first quarter looms on the horizon, underscoring the challenges ahead. As we have stated numerous times, if the past
is any predictor of the future, at least in the short-term, there is little to assure us that volatility won’t be with us into the coming year. 
This past year,
2012, was significant for Praxis Mutual Funds. In April, the Praxis International Fund was closed, and in December the Praxis Core Stock Fund was merged into the Praxis Growth Index Fund in a tax-free reorganization. These changes come as a result
of our ongoing efforts to deliver value to you, our shareholders. 
Shareholder advocacy is an integral part of our stewardship investing philosophy and practice. In
2012, we saw significant progress on a number of fronts, but particularly positive was our dialog with The Hershey Corporation. Since 2009, we have engaged Hershey on the use of children who are forced to work, often through coercion and violence,
in the cocoa plantations in West Africa where 70 percent of the world’s cocoa is grown. And, thanks in part to our shareholder advocacy efforts, Hershey recently announced it will use 100 percent certified cocoa for all of its global chocolate
product lines by 2020, as well as strengthen its programs to eradicate child labor in the cocoa industry. 
As the largest chocolate manufacturer in North America,
this commitment from Hershey should significantly increase the global supply and demand for certified cocoa. We applaud Hershey for taking this big step toward eliminating labor abuses in West African cocoa plantations, and we will continue
encouraging them and others to address cocoa sourcing and labor standards around the world. 
In December, the Praxis Board of Trustees recognized the significant
work of Howard Brenneman who served as chair of the Board of Trustees of Praxis since its inception in 1994. We are deeply grateful for his vision and leadership. While serving as president of MMA, now Everence, Howard encouraged the development of
Praxis, one of the first faith-based mutual funds in the country. We will miss his leadership and insights, and wish him well in his retirement from the Board. We also welcome R. Clair Sauder as the new chair of the Praxis Board of Trustees, and
look forward to his leadership in the years to come. 
On the following pages, you will find portfolio managers’ letters and performance reviews for each of the
Praxis Mutual Funds, as well as an update on our stewardship investing activities. The work we do on your behalf in company screening, shareholder advocacy and community development are reminders that you can make a significant impact by the way you
invest your dollars. I encourage you to read these letters for further expansion and explanation of their investment strategies and perspectives. 
 
1 
Table of Contents
Message from the President (unaudited), continued 


