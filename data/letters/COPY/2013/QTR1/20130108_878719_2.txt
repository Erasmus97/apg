Dear Shareholders:
We are pleased to provide you with our annual report for the year ended on the TS&amp;W Portfolios managed by
Thompson, Siegel&amp; Walmsley LLC (TS&amp;W). On the Equity Portfolios value was and the Fixed Income Portfolios value was Participants in these Portfolios include the
TS&amp;W retirement plan, TS&amp;W investment advisory clients, and others seeking investment management direction from TS&amp;W. We encourage our clients to pursue a balanced investment approach, and where appropriate, utilize a combination of
these Portfolios to achieve their specific investment objectives. The Portfolios are managed by the TS&amp;W team of
investment professionals utilizing a value investment philosophy. Our investment teams utilize a consistent investment process in managing all client portfolios. Our equity portfolio managers use a unique four-factor quantitative screen combined
with rigorous fundamental research conducted by experienced teams of analysts who are trying to answer three questions: Why is the stock inexpensive? What are the catalysts for change? And, are the catalysts sustainable? Our fixed income team
primarily focuses on yield curve/duration analysis, sector analysis, and security selection. Relative value analysis, historical spread relationships, and fundamental credit analysis are also used in the construction of fixed income portfolios. Our
long-term goal is to provide returns that exceed our benchmark indices over a complete economic or market cycle. TS&amp;W
Equity Portfolio The TS&amp;W Equity Portfolio experienced a return of after fees and expenses while the S&amp;P
was up in the twelve months ended For the most recent three-month period, the Equity Portfolio return was after fees and expenses, while the benchmark index returned
The recent experience in the market has been encouraging as our process has been successful at identifying companies that are being
rewarded by the marketplace forimproving fundamentals coupled with attractive valuations. While the macro-economic factors that so dominated investor attentions have not been wholly resolved, they have moved off the front page and now
investors appear to be focused once again on company specific factors. As well, the tilt in the marketplace toward very large, growth-oriented names has moderated as value stocks are once again performing in line with broad averages.
We have maintained our consistent focus on companies that offer fundamental catalysts and that produce high levels of cash flow relative
to their market values.














THE ADVISORS INNER CIRCLE FUND

THETS&amp;WPORTFOLIOS









We have witnessed substantial multiple compressions even among high quality names as investor psyches have been mired in indifference. We firmly believe that a shift in investor sentiment lies
ahead of us, to be triggered by some external catalyst or simply by the powerful relative valuation opportunity in stocks versus bonds. When that trigger occurs our Portfolios should deliver excellent returns; the improvement we have seen in the
past four months serving as an initial indication of that opportunity. TS&amp;W Fixed Income Portfolio
The TS&amp;W Fixed Income Portfolio gained after fees and expenses in the twelve-month fiscal period ended
The Portfolios benchmark, the Barclays U.S. Aggregate Bond Index, returned over the same period. For the most recent three-month period, the Fixed Income Portfolio was up while the benchmark index return gained Relative
outperformance for the fiscal year end and most recent three months was driven primarily by the Portfolios overweight to corporate bonds. The Portfolios high yield bond allocation was especially beneficial during these time periods.
Interest rate volatility has persisted throughout the fiscal year with both the uncertainty in the Eurozone and quantitative
easing by the Federal Reserve pushing and pulling interest rates higher and lower. The result for the fiscal year ending has been that the long end of the yield curve significantly outperformed the short end. The year U.S.
Treasury returned versus a return of just for the year U.S. Treasury for the fiscal year. The past four months began quietly with both stock and bond markets moving sideways for the month of July. Entering August, the on again,
off again market dynamic was apparent. The Eurozone crisis was off again and investors instead turned their attention to Ben Bernankes upcoming sequel entitled scheduled for release in September. Six days
after a very weak August employment report, the Fed Chairman announced steps that were both more aggressive and open-ended than expected, indicating interest rates will remain low not only into but even well into any economic recovery. In
addition, the Fed announced their intention to purchase billion per month of agency mortgage-backed securities until further notice. Ben Bernanke has made it clear that as far as monetary policy is able to prevent it, the U.S. will not fall
victim to deflationary forces on his watch. The Portfolio is currently structured with duration of approximately years with
an average credit rating. TS&amp;W believes that one of the biggest beneficiaries of the Federal Reserves easy money has been corporate America. Companies have strengthened their balance sheets through refinancing at lower rates and by
retiring














THE ADVISORS INNER CIRCLE FUND

THETS&amp;WPORTFOLIOS









debt. As a result, TS&amp;W continues to find great relative value in corporate bonds and mortgage-backed securities versus very low yielding treasury securities. TS&amp;W believes that a
strategy of investing in solid, stable companies with higher yielding debt securities will produce strong absolute and relative returns.
Respectfully submitted,
