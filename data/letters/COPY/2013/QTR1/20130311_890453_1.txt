Dear Wilshire Mutual Fund Shareholder:



We are pleased to present this annual report to all shareholders of the Wilshire Mutual Funds. This report covers the period from January to December for all share classes of the Large Company Growth Fund, Large Company Value Fund, Small Company Growth Fund, Small Company Value Fund, Wilshire IndexSM Fund, and Wilshire Large Cap Core Plus Fund.



Market Environment



U.S. Equity Market



Despite short-term volatility in equities mainly due to uncertainty surrounding the situation in the euro-zone, domestic markets were able to push ahead in with the Wilshire IndexSM gaining for the year. This advance marked the Wilshire fourth consecutive year of positive returns, and a advance since the end of All style and size segments delivered very strong returns across the board, with growth stocks outpacing value in the large cap segment v. and small cap outperforming large cap for the year v. Markets suffered through the months of October and November as concerns over the outcome of the Presidential election, as well as the fallout from Superstorm Sandy, began to weigh on investor sentiment. In the fourth quarter, markets remained volatile as the fiscal cliff negotiations heated up, but ultimately rose when an agreement was reached. Financials was the best performing sector for the year, returning while the Energy and Utilities sectors lagged, returning and respectively.



International Equity Market



Despite continued fiscal upheaval in Europe, proved to be a good year for global stocks overall, with the MSCI EAFE Index and MSCI Emerging Markets Index gaining and respectively. In July, European Central Bank President Mario Draghi said he would do whatever it takes to preserve the euro, boosting investor confidence worldwide. The euro received additional support from the European Central Bank through several stimulus packages, averting a potential breakup of the euro-zone. European stocks rallied at the end of the year, with the MSCI Europe Index rising for its strongest annual gain since China also made headlines as investors grew nervous about a possible slowdown in the worlds second largest economy. While Chinas economy did cool down, expanding at an annualized rate of in the third quarter, by the end of the year the country was showing increasing signs of renewed economic growth.



Bond Market



Fixed income securities rallied in with all sectors of the debt market gaining and the Barclays U.S. Aggregate Bond Index returning As Federal Reserve policy kept Treasury yields depressed, investors in search of higher yield ventured farther out on the yield curve. The Treasury yield remained volatile over the year, soaring to over in March, and then falling to an all-time low of in July as concerns over the euro-zone debt crisis rapidly escalated. At the end of the year, the Treasury ended up just about where it started at High yield and corporate sectors performed well, as the Barclays U.S. Corporate High Yield Index and Barclays U.S. Credit Index rose and respectively. The Barclays EM Local Currency Government Universal Index posted strong performance, returning However, the Barclays Global Aggregate Index posted returns in line with its U.S. counterpart, gaining



Fund Performance Review



The Wilshire Mutual Funds turned in mixed performance in The Wilshire Large Company Growth Portfolio Institutional Class returned for the year, underperforming the Russell Growth Index return of The Wilshire Large Company Value Portfolio Institutional Class returned for the year, underperforming the Russell




























Wilshire Mutual Funds

Letter to Shareholders - (Continued)








Value Index return of The Wilshire Small Company Growth Portfolio Institutional Class returned for the year, underperforming the Russell Growth Index return of The Wilshire Small Company Value Portfolio Institutional Class returned for the year, underperforming the Russell Value Index return of The Wilshire IndexSM Fund Institutional Class returned for the year, underperforming the Wilshire IndexSM return of Lastly, the Wilshire Large Cap Core Plus Fund Institutional Class returned for the year, underperforming the S&amp;P Index return of



We are mindful that markets can behave erratically and current trends shift swiftly. You can expect us to continue to work diligently to manage your investments and seek to generate returns commensurate with the Funds investment objectives. As always, we sincerely appreciate your continued support and confidence in Wilshire Associates.
