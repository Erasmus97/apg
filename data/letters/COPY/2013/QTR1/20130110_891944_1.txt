Dear Hennessy Funds Shareholder:






The past year has been jam-packed with politics, continued economic uncertainty in the United States and around the world, and most recently, non-stop talk of the possible effects of the looming Fiscal Cliff.



Since the economic crisis of most Americans seem to have taken a glass half empty view and now seem to expect doom and gloom.Talking about the decline of America has become a popular pastime around the world, and even Americans are jumping into the discussion. In fact, it feels like the United States has become an underdog, while China and other emerging nations are coined as the new world economic leaders.But the reality is that the U.S. remains the worlds largest economy, and the stock market has continued to perform well.Following the crisis in the Dow Jones Industrial Average was up in up in and up in I believe, however, that many Americans, including individuals, business leaders, and political leaders, have remained resilient and continue to exhibit the stamina to work hard and the character to succeed.



I am not saying that our economy isnt without issues.But, I believe investors may be missing some key facts about our economy, and those facts transcend the rhetoric.In last years shareholder letter, I told you that the major obstacle facing the U.S. economy and the stock market was clarity from our leaders in Washington on taxes, regulation and healthcare.With the elections behind us, we have the clarity that President Obama will be our leader for another four years.We know that the Democrats have the majority in the Senate and the Republicans have the majority in the House.But where does that leave our economy, the financial markets and investors?



Post-Election Economy



The moment the last ballot was cast in November, the media began its talk of the Fiscal Cliff.The dramatic use of the word Cliff is making everyone nervous.I keep picturing the old Road Runner cartoons with the Coyote plummeting off a cliff and landing in a cloud of dust. It is my somewhat controversial opinion that we should drive right off the Fiscal Cliff and force our policy makers to get things right and not compromise for the sake of a compromise. I strongly believe that if we fall off the Cliff, in six months time our leaders would have to work together to create better policies, rather than trying to force quick fixes on these issues critical to our economy and our nation.



The election, the Fiscal Cliff and slow earnings growth may be eroding the confidence of our business leaders, who have cut costs and driven profits. However, companies here in the U.S. are still sitting on record amounts of cash.Businesses require faith in their government to execute strategies that will put that capital to work and to hire in earnest.Now that there is some clarity, it must, in my opinion, be coupled with actionable policy on taxes and regulation for corporate attitudes and behavior to shift.Corporate America, and frankly most of America, is losing its tolerance with polarized and stagnant politics.



For better or worse, business leaders need to know what regulations theyll have to comply with, what tax rates will be and what healthcare will cost.The writing is on the wall for higher taxes, and the Healthcare Reform Law appears to be here to stay.The current administration seems to feel that they have a green light to push the regulations related to Dodd-Frank and the numerous policies outlined in the Healthcare Reform Law into practice in the next four years.I know that innovative business leaders in this country will implement new strategies as they seek to remain profitable, because thats what they do. Many of Americas business leaders have shown that they have the character to succeed in any political or economic climate.



Financial Markets



The stock market will be forced to wade through this murky economy as we continue to navigate the partisan political quagmire. Many companies comprising the Dow Jones Industrial Average Index or the S&amp;P have strong balance sheets, respectable fundamentals and reasonable returns.When our fiscal year ended on October the Dow Jones was going strong at calendar year to date.Of course, after the election, the market rioted in order to force some decisions on the Fiscal Cliff.In fact, in the ten days after the election, we saw the Dow lose in the long term, I believe the strengths of the economy should filter through to the markets.We are in the midst of a slow but steady recovery that wont easily be derailed.I am, therefore, expecting another year with slow to moderate growth. There are still plenty of great stocks to buy. We are seeing improvement in many sectors, including the housing industry, and we still see strength in lower-end retailers.



Investors



Investors are still uncertain about putting their cash to work in the stock market, and they continue to flock to fixed income products.Like business leaders, I believe that investors feel the same frustration with partisanship in Washington, and they need more answers in order to believe in the economic recovery. The strength I have seen in many of the lower-end retailers tells me that investors and consumers are still looking for value for their dollar. Many industry statistics report







HENNESSY FUNDS


















that Americans have been paying down their debt and saving more, even with interest rates at all-time lows.And, I believe, the average investor could be experiencing some long-awaited comfort in the slow return of the housing market in our country.








While fiscal proved another difficult year for the economy, at Hennessy Funds we remain focused on our proven investment strategies, and we will not compromise our long-standing commitment to manage our portfolios in the best interest of our shareholders.As investors regain their confidence and return to investing based on facts and fundamentals, I believe we should return to steady, long-term market gains.



On October Hennessy became the investment manager to the ten FBR Funds.We want to welcome new shareholders to the Hennessy family of Funds. We are excited to now offer all of our shareholders an expanded line-up of products, including domestic equity, sector and specialty, as well as more conservative balanced and fixed income mutual funds.



I personally would like to take a moment to reach out to the victims of Hurricane Sandy and Sandy Hook Elementary School.No words can express the sorrow we feel for the families who lost loved ones.



Thank you for your continued confidence and investment in the Hennessy Funds.If you have any questions or want to speak with us directly, please dont hesitate to call us at



Best regards,

Dear Hennessy Funds Shareholder:






The past year has been jam-packed with politics, continued economic uncertainty in the United States and around the world, and most recently, non-stop talk of the possible effects of the looming Fiscal Cliff.



Since the economic crisis of most Americans seem to have taken a glass half empty view and now seem to expect doom and gloom.Talking about the decline of America has become a popular pastime around the world, and even Americans are jumping into the discussion. In fact, it feels like the United States has become an underdog, while China and other emerging nations are coined as the new world economic leaders.But the reality is that the U.S. remains the worlds largest economy, and the stock market has continued to perform well.Following the crisis in the Dow Jones Industrial Average was up in up in and up in I believe, however, that many Americans, including individuals, business leaders, and political leaders, have remained resilient and continue to exhibit the stamina to work hard and the character to succeed.



I am not saying that our economy isnt without issues.But, I believe investors may be missing some key facts about our economy, and those facts transcend the rhetoric.In last years shareholder letter, I told you that the major obstacle facing the U.S. economy and the stock market was clarity from our leaders in Washington on taxes, regulation and healthcare.With the elections behind us, we have the clarity that President Obama will be our leader for another four years.We know that the Democrats have the majority in the Senate and the Republicans have the majority in the House.But where does that leave our economy, the financial markets and investors?



Post-Election Economy



The moment the last ballot was cast in November, the media began its talk of the Fiscal Cliff.The dramatic use of the word Cliff is making everyone nervous.I keep picturing the old Road Runner cartoons with the Coyote plummeting off a cliff and landing in a cloud of dust. It is my somewhat controversial opinion that we should drive right off the Fiscal Cliff and force our policy makers to get things right and not compromise for the sake of a compromise. I strongly believe that if we fall off the Cliff, in six months time our leaders would have to work together to create better policies, rather than trying to force quick fixes on these issues critical to our economy and our nation.



The election, the Fiscal Cliff and slow earnings growth may be eroding the confidence of our business leaders, who have cut costs and driven profits. However, companies here in the U.S. are still sitting on record amounts of cash.Businesses require faith in their government to execute strategies that will put that capital to work, and to hire in earnest.Now that there is some clarity, it must, in my opinion, be coupled with actionable policy on taxes and regulation for corporate attitudes and behavior to shift.Corporate America, and frankly most of America, is losing its tolerance with polarized and stagnant politics.



For better or worse, business leaders need to know what regulations theyll have to comply with, what tax rates will be and what healthcare will cost.The writing is on the wall for higher taxes, and the Healthcare Reform Law appears to be here to stay.The current administration seems to feel that they have a green light to push the regulations related to Dodd-Frank and the numerous policies outlined in the Healthcare Reform Law into practice in the next four years.I know that innovative business leaders in this country will implement new strategies as they seek to remain profitable, because thats what they do. Many of Americas business leaders have shown that they have the character to succeed in any political or economic climate.



Financial Markets



The stock market will be forced to wade through this murky economy as we continue to navigate the partisan political quagmire. Many companies comprising the Dow Jones Industrial Average Index or the S&amp;P have strong balance sheets, respectable fundamentals and reasonable returns.When our fiscal year ended on October the Dow Jones was going strong at calendar year to date.Of course, after the election, the market rioted in order to force some decisions on the Fiscal Cliff.In fact, in the ten days after the election, we saw the Dow lose in the long term, I believe the strengths of the economy should filter through to the markets.We are in the midst of a slow but steady recovery that wont easily be derailed.I am, therefore, expecting another year with slow to moderate growth. There are still plenty of great stocks to buy. We are seeing improvement in many sectors, including the housing industry, and we still see strength in lower-end retailers.



Investors



Investors are still uncertain about putting their cash to work in the stock market, and they continue to flock to fixed income products.Like business leaders, I believe that investors feel the same frustration with partisanship in Washington, and they need more answers in order to believe in the economic recovery. The strength I have seen in many of the lower-end retailers tells me that investors and consumers are still looking for value for their dollar. Many industry statistics report






WWW.HENNESSYFUNDS.COM


















that Americans have been paying down their debt and saving more, even with interest rates at all-time lows.And, I believe, the average investor could be experiencing some long-awaited comfort in the slow return of the housing market in our country.



Japan Market



I remain very excited about the possibilities for growth in the Japanese market.Japanese companies have been employing the same tactics as U.S. based companies:they are saving money, paying dividends and making strategic acquisitions.With the passing of the torch to a new Prime-minister (Shinzo Abe) it looks as though the final piece to the economic puzzle in Japan may be in place.It appears that the Japanese Government is supporting the weakening of the Yen, and that companies in Japan are prepared to increase their exports.While I remain hopeful that the U.S. Administrations actions and policies will support business and encourage personal success, Japans governing body seems to truly want Japanese companies and citizens to prosper and is taking action.In a surprise move, Shinzo Abes call for an inflation target of and a Yen of around (up from approximately to the dollar could do two things. First, it could help to end the deflation that has hampered the country for years, and second, it can encourage Japanese business leaders to increased profits, which would in turn drive more revenues for the Japanese Government.All told, I truly believe that companies in Japan are poised to take off, and in turn so should the Japanese markets. I am confident that Japan is on the brink of the financial comeback that many investors have been waiting for.








While fiscal proved another difficult year for the global economy, at Hennessy Funds we remain focused on our proven investment strategies, and we will not compromise our long-standing commitment to manage our portfolios in the best interest of our shareholders.As investors regain their confidence and return to investing based on facts and fundamentals, I believe we should return to steady, long-term market gains.



In late October, Hennessy Funds added seven new funds to our family of funds. We are excited to now offer our shareholders an expanded line-up of products, including traditional domestic equity, sector and specialty, as well as more conservative balanced and fixed income mutual funds.



I personally would like to take a moment to reach out to the victims of Hurricane Sandy and Sandy Hook Elementary School.No words can express the sorrow we feel for the families who lost loved ones.



Thank you for your continued confidence and investment in the Hennessy Funds.If you have any questions or want to speak with us directly, please dont hesitate to call us at



Best regards,
