Dear Shareholder:




Stocks and bonds produced better returns in than most expected a year ago, despite a global economic environment compromised by recession in Europe and Japan, below-par growth in the United States and, for good measure, gridlock in Congress. It no doubt helped that the Federal Reserve and most of the worlds central banks continued to flood the markets with low-cost money and assurances that interest rates will remain low ad infinitum. While concern over the U.S. fiscal cliff produced some anxious moments as the year wound down, U.S. stock market averages still managed to end near five-year highs. Bonds boasted positive returns for with the riskier classes of the fixed-income markets generally earning higher returns as market volatility receded and investors clamored for yield. High-yield bonds finished with returns approximating the total return on the S&amp;P Foreign stocks outdistanced their U.S. counterparts for the first time since




As the U.S. taxpayers date with the fiscal cliff approached, the Federal Reserve unveiled a new tranche of Quantitative Easing to reduce the risk of recession in On top of the program of mortgage bond buying announced in September (at a billion per month clip), the Fed upped the ante with an additional billion a month in Treasury bond purchases in an attempt to keep downward pressure on interest rates in order to stimulate economic activity. During the summer of Mario Draghi, president of the European Central Bank, pledged that the ECB is ready to do whatever it takes to preserve the euro. These assurances come along with projections from the Federal Open Market Committee that its target range for the federal funds rate at to percent will be appropriate at least as long as the unemployment rate remains above inflation between one and two years ahead is (expected to be no more than longer-term inflation expectations continue to be well anchored. In the slow growth, low inflation climate envisioned for it is hard to get excited about interest rate risk until the central banks swing over from the side of growth to the inflation-fighting side.




After trying, with some success, to support prices for five years, the Fed may have more success in engineering higher inflation in It is virtually impossible for prices to increase markedly without rising wage rates, and since the U.S. is only two months off the lowest rate of year-on-year increase in average hourly earnings in nearly five decades inflation pressures figure to build only slowly. In other words, the U.S. is not likely to have an inflation problem until personal incomes grow fast enough to afford higher prices. Until that happens, consumers will eschew higher priced goods for lower priced ones, to the extent they are interchangeable. With the Feds massive injections of quantitative easing, one suspects that an inflationary pulse is being generated, but so long as money velocity remains depressed, that pulse is likely to remain weak.




Another factor weighing on the inflation outlook is the output gap, the degree to which the U.S. economy and other developed economies are operating below their potential. The IMF estimates that the output gap for the advanced economies of the world is in the neighborhood of down from the gap that existed at the depths of the global recession in By the Congressional Budget Offices reckoning, the U.S. economy is running about below capacity, compared to a operating gap as the great recession was ending in In commodities, where growth in China and India make the world most vulnerable to cost-push price pressures, the inflation potential appears to be limited in the near term. Still, relaxing its inflation target is a slippery slope for the Fed to be treading on, one that might lead to more than the optimal amount of inflation, which the Fed says is for the next few years.




Where the three constraints on global economic growth since have been deleveraging, deflation and demographics, we find three positive factors at the start of pent-up demand for automobiles, a more vigorous rate of household formation, and an upturn in business capital expenditures deferred in due to the impending fiscal cliff. Motor vehicle sales ended at the highest rate since early and to judge by the


































Letter to Shareholders (Unaudited)
