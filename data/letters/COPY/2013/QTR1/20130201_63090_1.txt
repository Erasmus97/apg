Dear Shareholders: 
The global market outlook for 2013 is one of cautious optimism. While we are seeing some positive economic trends in the United States, Europe, and China, the
overall 

environment remains challenging. In the United States, the recent fiscal cliff agreement was received positively by investors, even though it mostly addressed pressing taxation issues and did not
resolve additional concerns, including the need for spending cuts and a large-scale reduction of the federal debt. These issues will be front and center again in the spring. Despite the ongoing uncertainty, economic tailwinds are gathering strength
as the U.S. housing and job markets are improving and consumer confidence is rising. 
Overseas, the debt crisis continues to weigh heavily on eurozone
markets, with even Germany — long an economic 

stalwart — experiencing some contraction. These ongoing challenges could be a drag on global market performance this year. In Asia, manufacturing activity has accelerated in emerging markets
such as China and India, and we are seeing signs of stabilized loan growth in China, a leading indicator of that country’s economic health. In contrast, Japan’s economy is contracting sharply under deflationary pressures. Nevertheless,
Japanese markets have responded favorably to early actions by the new government, which appears determined to act aggressively, along with the Bank of Japan, to stimulate growth. 
As always, managing risk in the face of uncertainty remains a top priority for investors. At MFS®, our uniquely collaborative investment
process revolves around global research and our disciplined risk management approach. Our global team of investment professionals shares ideas and evaluates opportunities across continents, investment disciplines and asset classes — all with a
goal of building better insights, and ultimately better results, for our clients. 
We remain mindful of the many economic challenges investors face
today, and believe it is more important than ever to maintain a long-term view, employ time-tested principles, such as asset allocation and diversification, and work closely with investment advisors to identify and pursue the most suitable
opportunities. 
Respectfully, 


