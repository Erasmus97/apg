Dear Shareholder: 
Thank you for your
investment in The Managers Funds. Our foremost goal at Managers Investment Group (MIG) is to provide investment products and solutions that help our shareholders and clients successfully reach their investment goals and objectives. We do this by
offering a broad selection of funds managed by a collection of Affiliated Managers Group’s (AMG) Affiliate investment boutiques, along with a complementary series of open-architecture mutual funds. 
The past year has been an exciting one for us at MIG. In connection with AMG’s investment in Yacktman Asset Management (“Yacktman”), MIG
partnered with Yacktman in reorganizing the Yacktman Focused Fund and the Yacktman Fund into The Managers Funds. The addition of the Yacktman Funds to our platform brought our total assets under management to over $25 billion at the end of 2012.

Additionally, in an effort to better meet our shareholders’ needs as well as bring consistency across our funds, we restructured our
share class offerings across many of our Funds, which included discontinuing certain share classes with sales charges (commonly called sales loads). As a result, many of our Funds now offer three No Load share classes – Investor, Service, and
Institutional Share Classes. We believe this simplified structure makes it easier for our clients as well as Financial Advisors to select the appropriate share class to match their needs. 
During 2012, we also executed on other changes to certain Funds, which included reducing expense ratios on several Funds to ensure that our offerings remain competitive and affordable for our clients.

As we enter into 2013, both known and unknown risks remain to the global economy and its growth prospects. Nevertheless, we remain optimistic
that the collective fiscal and monetary efforts undertaken over the past several years will continue to have a positive impact on the global economy. In the meantime, we remain confident that our Funds are well positioned to weather an uncertain
economic environment. 
We thank you for your continued confidence and investment in The Managers Funds. You can rest assured that under all
market conditions our team is focused on delivering excellent investment management services for your benefit. 
Respectfully, 


Dear Shareholder: 
Thank you for your
investment in The Managers Funds. Our foremost goal at Managers Investment Group (MIG) is to provide investment products and solutions that help our shareholders and clients successfully reach their investment goals and objectives. We do this by
offering a broad selection of funds managed by a collection of Affiliated Managers Group’s (AMG) Affiliate investment boutiques, along with a complementary series of open-architecture mutual funds. 
The past year has been an exciting one for us at MIG. In connection with AMG’s investment in Yacktman Asset Management (“Yacktman”), MIG
partnered with Yacktman in reorganizing the Yacktman Focused Fund and the Yacktman Fund into The Managers Funds. The addition of the Yacktman Funds to our platform brought our total assets under management to over $25 billion at the end of 2012.

Additionally, in an effort to better meet our shareholders’ needs as well as bring consistency across our funds, we restructured our
share class offerings across many of our Funds, which included discontinuing certain share classes with sales charges (commonly called sales loads). As a result, many of our Funds now offer three No Load share classes – Investor, Service, and
Institutional Share Classes. We believe this simplified structure makes it easier for our clients as well as Financial Advisors to select the appropriate share class to match their needs. 
During 2012, we also executed on other changes to certain Funds, which included reducing expense ratios on several Funds to ensure that our offerings remain competitive and affordable for our clients.

As we enter into 2013, both known and unknown risks remain to the global economy and its growth prospects. Nevertheless, we remain optimistic
that the collective fiscal and monetary efforts undertaken over the past several years will continue to have a positive impact on the global economy. In the meantime, we remain confident that our Funds are well positioned to weather an uncertain
economic environment. 
We thank you for your continued confidence and investment in The Managers Funds. You can rest assured that under all
market conditions our team is focused on delivering excellent investment management services for your benefit. 
Respectfully, 


Dear Shareholder: 
Thank you for your
investment in The Managers Funds. Our foremost goal at Managers Investment Group (MIG) is to provide investment products and solutions that help our shareholders and clients successfully reach their investment goals and objectives. We do this by
offering a broad selection of funds managed by a collection of Affiliated Managers Group’s (AMG) Affiliate investment boutiques, along with a complementary series of open-architecture mutual funds. 
The past year has been an exciting one for us at MIG. In connection with AMG’s investment in Yacktman Asset Management (“Yacktman”), MIG
partnered with Yacktman in reorganizing the Yacktman Focused Fund and the Yacktman Fund into The Managers Funds. The addition of the Yacktman Funds to our platform brought our total assets under management to over $25 billion at the end of 2012.

Additionally, in an effort to better meet our shareholders’ needs as well as bring consistency across our funds, we restructured our
share class offerings across many of our Funds, which included discontinuing certain share classes with sales charges (commonly called sales loads). As a result, many of our Funds now offer three No Load share classes – Investor, Service, and
Institutional Share Classes. We believe this simplified structure makes it easier for our clients as well as Financial Advisors to select the appropriate share class to match their needs. 
During 2012, we also executed on other changes to certain Funds, which included reducing expense ratios on several Funds to ensure that our offerings remain competitive and affordable for our clients.

As we enter into 2013, both known and unknown risks remain to the global economy and its growth prospects. Nevertheless, we remain optimistic
that the collective fiscal and monetary efforts undertaken over the past several years will continue to have a positive impact on the global economy. In the meantime, we remain confident that our Funds are well positioned to weather an uncertain
economic environment. 
We thank you for your continued confidence and investment in The Managers Funds. You can rest assured that under all
market conditions our team is focused on delivering excellent investment management services for your benefit. 
Respectfully, 


