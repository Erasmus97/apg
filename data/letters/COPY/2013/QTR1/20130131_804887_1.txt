Dear Shareholder:
We are pleased to present this annual report for Dreyfus Short-Intermediate Government Fund, covering the 12-month period from December 1, 2011, through November 30, 2012. For information about how the fund performed during the reporting period, as well as general market perspectives, we provide a Discussion of Fund Performance on the pages that follow.
Despite a robust rally among longer-term U.S. government securities during the spring of 2012, those with maturities in the short- to intermediate-term range posted only mildly total returns over the reporting period as their yields remained anchored by an unchanged federal funds rate between 0% and 0.25%. In contrast, long-term U.S. debt obligations benefited to a greater degree as investors responded to aggressively accommodative monetary policies from the Federal Reserve Board (the “Fed”) and other major central banks.
In light of the easy monetary policies adopted by many countries, we expect global growth to be slightly more robust in 2013 than in 2012.The U.S. economic recovery is likely to persist at subpar levels over the first half of the new year, as growth may remain constrained by uncertainties surrounding fiscal policy and tax reforms. In addition, the Fed has signaled its intention to keep short-term interest rates near historical lows through mid-2015. As always, we encourage you to stay in touch with your financial advisor as new developments unfold.
Thank you for your continued confidence and support.
Sincerely,


