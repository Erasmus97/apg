DEAR SHAREHOLDER





ECONOMIC AND MARKET OVERVIEW





A BRIEF NOTE ON THE COMPOUNDING OF RETURNS





ABOUT SHAREHOLDERS&rsquo; FUND EXPENSES





S&amp;P STRATEGY FUND





INVERSE S&amp;P STRATEGY FUND





STRATEGY FUND





INVERSE STRATEGY FUND





DOW STRATEGY FUND





INVERSE DOW STRATEGY FUND





RUSSELL STRATEGY FUND





INVERSE RUSSELL STRATEGY FUND





NOTES TO FINANCIAL STATEMENTS





REPORT OF INDEPENDENT REGISTERED PUBLIC ACCOUNTING FIRM





OTHER INFORMATION





INFORMATION ON BOARD OF TRUSTEES AND OFFICERS





GUGGENHEIM INVESTMENTS PRIVACY POLICIES







THE RYDEX FUNDS ANNUAL














December



Dear Shareholder:



Security Investors, LLC (the &ldquo;Investment Adviser&rdquo;)
is pleased to present the annual shareholder report for eight of our leveraged and inverse mutual funds.



The Investment Adviser is a part of Guggenheim Investments,
which represents the investment management businesses of Guggenheim Partners, LLC, a global, diversified financial services firm.



This report covers performance of the following Rydex Funds
for the annual period ended December with the H-Class Share ticker in parentheses:



&ndash; Rydex S&amp;P Strategy Fund (RYTNX)

&ndash; Rydex Inverse S&amp;P Strategy Fund (RYTPX)

&ndash; Rydex Strategy
Fund (RYVYX)

&ndash; Rydex Inverse
Strategy Fund (RYVNX)

&ndash; Rydex Dow Strategy Fund (RYCVX)

&ndash; Rydex Inverse Dow Strategy Fund (RYCWX)

&ndash; Rydex Russell Strategy
Fund (RYRSX)

&ndash; Rydex Inverse Russell
Strategy Fund (RYIRX)



Guggenheim Distributors, LLC is the distributor of the Funds.
Guggenheim Distributors, LLC is affiliated with Guggenheim Partners, LLC and Security Investors, LLC.



We encourage you to read the Economic and Market Overview section
of the report, which follows this letter, and then the Performance Report and Fund Profile for each Fund.



We are committed to providing innovative investment solutions
and appreciate the trust you place in us.



Sincerely,
