Dear Shareowner,

Pioneer has been cautiously optimistic about the U.S. economy from the start of
the year, and the data continues to be encouraging. Employment continues to
rise, albeit slowly, and we believe it should continue to do so in barring
a negative shock to the system. The housing and auto sectors continue to
recover, benefiting from record-low interest rates. Banks' willingness to lend
to consumers and businesses also continues to rise, broad measures of inflation
remain subdued, and, if the weather improves in that should help to bring
food prices back down. While corporate profit growth has slowed, many U.S.
companies still have strong balance sheets and continue to display the ability
to both pay and increase dividends*.

While the so-called "fiscal cliff " scheduled to take effect at year-end
dominated the media in December--and while no deal was struck before markets
closed for the year--investors who owned financial assets like equities and
high-yield corporate bonds generally enjoyed good returns in The Standard
& Poor's Index returned in and the Bank of America Merrill Lynch
High Yield Master II Index returned Meanwhile, the higher-quality
Barclays Capital Aggregate Bond Index gained for the year, the safer-still
Barclays Capital Intermediate Treasuries Index returned and
Treasury bills, generally regarded as essentially "risk free" by the markets,
returned just in

Despite generally improving economic conditions and positive market returns in
investors still face daunting challenges in the year ahead, although we
remain optimistic that the underlying economic trends are moving in the right
direction. The year-end "fiscal cliff " deal did not eliminate the risk of
further tax increases or spending cuts, nor did it eliminate the risk that the
U.S. could face further downgrades to its credit rating from one or more of the
major ratings agencies.The Federal Reserve Board continues to provide
extraordinary support to the U.S. economy and the bond market, but will not do
so indefinitely. Europe has made progress towards dampening its sovereign-debt
crisis, but has not resolved the problem as yet; the region also was mired in a
recession as drew to a close. In Asia, Japan continues to struggle with low
economic growth,

* Dividends are not guaranteed.

Pioneer Fundamental Value Fund | Semiannual Report |


deflation, high levels of debt, and an aging population. In the emerging
markets, China and other developing economies, while generally in better shape
than most "developed" markets, also face a range of ongoing challenges.

While most of the risks outlined above are widely recognized and may already be
"priced in" to the market, we believe investors should continue to expect market
volatility tied to these factors.

At Pioneer, we have long advocated the benefits of staying diversified and
investing for the long term. And while diversification alone does not assure a
profit or protect against loss in a declining market, we believe there are still
opportunities for prudent investors to earn attractive returns. Our advice, as
always, is to work closely with a trusted financial advisor to discuss your
goals and work together to develop an investment strategy that meets your
individual needs, keeping in mind that there is no single best strategy that
works for every investor.

In Pioneer proudly celebrates its anniversary. Since our
investment teams have sought out attractive opportunities in global equity and
bond markets, using in-depth research to identify undervalued individual
securities, and using thoughtful risk management to construct portfolios which
balance potential risks and reward in an ever-changing world.

We encourage you to learn more about Pioneer and our time-tested approach to
investing by consulting with your financial advisor or visiting us online at
us.pioneerinvestments.com. We greatly appreciate your trust in us, and we thank
you for investing with Pioneer.

Sincerely,
