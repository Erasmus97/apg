Dear Shareholders:






The economy
continues to recover. Fueled by unprecedented injections of
central bank liquidity and an improving economy, the SP
Index posted a gain through the second quarter of
as markets around the globe hit record highs. However,
after a May peak of the SP Index, volatility returned
to the market when the Federal Reserve (the Fed)
announced plans to ease off its quantitative easing policy. The
Feds mere utterance of plans to taper its bond-buying
program sparked a steep,
across-the-board
global sell-off. Also, in another troublesome development,
interest rates spiked higher, pushing bond returns into
uncharted negative territory.






The markets
immediate reaction to the Feds statement served both as a
reality check for investors who had prematurely heralded a
return to normalcy and as a harbinger of future challenges. The
Feds decision to end cash infusions could negatively
impact European economies that have just begun to move past the
recession as well as more fragile emerging markets.






With market
uncertainty underscoring the need to implement investment
decisions with greater precision, investors continued to embrace
a broad array of ETFs for their ability to provide transparent,
low cost and liquid access to all corners of the global market.






As part of our
continued commitment to provide the products you need to help
achieve your investment goals, State Street Global Advisors
launched a number of new SPDR ETFs since July including
the following:







New Equity
SPDRS





The SPDR SP
Value Tilt ETF (Ticker Symbol: VLU) provides exposure to
U.S. equity securities exhibiting value
characteristics. The SPDR SP Momentum Tilt ETF
(Ticker Symbol: MMTM) provides exposure to U.S. equity
securities exhibiting price momentum. The SPDR Russell Low
Volatility ETF (Ticker Symbol: SMLV) is designed to track a
small cap, low volatility index, while the SPDR Russell Low
Volatility ETF (Ticker Symbol: LGLV) is designed to track a
large cap, low volatility index.







New Fixed Income
SPDRS





The SPDR Barclays
Year TIPS ETF (Ticker Symbol:TIPX) provides
exposure to the inflation protected sector of the
U.S. Treasury market.






We take great pride
in these new additions to our SPDR family. You will find
additional information on the SPDR
SeriesTrustExchange Traded Funds, including
Managements Discussion of FundPerformance, in the
enclosed Annual Report.






On behalf of the
SPDR SeriesTrust, I thank you for your continued support.






Sincerely,
