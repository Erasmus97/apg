Dear Shareowner,

Pioneer continues to see only modest economic growth in the U.S. Employment
continues to rise, albeit slowly, and we believe it will continue to do so in
barring a negative shock to the system. The housing and auto sectors
continue to recover, benefiting from record-low interest rates. Banks'
willingness to lend to consumers and businesses also continues to rise, broad
measures of inflation remain subdued, and, if the weather cooperates in
food prices should come back down. And, while corporate profit growth has
slowed, profits remain high and many U.S. companies continue to both pay and
increase dividends*. Offsetting some of these positives are the continued
contraction of fiscal policy in Washington and a recessionary Europe.

The Federal Reserve's aggressive monetary policy has driven Treasury yields to
generational lows and supported investments in all financial assets, including
equities and high-yield corporate bonds. For example, the Standard & Poor's
Index (the S&P a broad measure of the U.S. stock market, returned
for the full calendar year ended December and the Bank of America
Merrill Lynch High Yield Master II Index (the High Yield Index), which measures
the performance of high-yield corporate bonds, returned for the same
period. On the other hand, the Barclays Aggregate Bond Index (the
Aggregate Index), which tracks the performance of a higher-quality bond
universe, gained for the months ended December the
safer-still Barclays Government Credit Index (the Government/Credit Index)
returned and Treasury bills, generally regarded as essentially
"risk free" by the markets, returned just in "Risky" assets
outperformed again in the first quarter of as the S&P returned
and the High Yield Index returned In contrast, the Aggregate Index
returned in the first quarter, the Government Credit Index returned
and Treasury bills returned

Despite generally improving economic conditions and a rising stock market,
global economies and investors still face daunting challenges as moves
forward, although we remain cautiously optimistic. U.S. fiscal policy remains
unsettled, and we feel the U.S. government could be at risk of credit rating
downgrades from one or more of the major ratings agencies if the uncertainties
persist. The Federal Reserve continues to provide extraordinary support to the
U.S. economy and the bond market, but will not do so indefinitely. Europe has
made progress, but has not yet resolved its sovereign-debt/banking problem, nor
has the region been able to exit recession. Japan recently has unveiled

* Dividends are not guaranteed.

Pioneer Emerging Markets Fund | Semiannual Report |


aggressive and unconventional monetary and fiscal policies, but the country
continues to face issues such as high levels of debt as well as an aging
population. China and other emerging economies, while generally in better
shape than most "developed" markets, also face a range of challenges.

While most of the risks outlined here are widely recognized and may already be
"priced in" to the market, we believe investors should continue to expect market
volatility.

At Pioneer, we have long advocated the benefits of staying diversified and
investing for the long term. And while diversification does not assure a profit
or protect against loss in a declining market, we believe there are still
opportunities for prudent investors to earn attractive returns. Our advice, as
always, is to work closely with a trusted financial advisor to discuss your
goals and work together to develop an investment strategy that meets your
individual needs, keeping in mind that there is no single best strategy that
works for every investor.

Pioneer's investment teams have, since sought out attractive opportunities
in global equity and bond markets, using in-depth research to identify
undervalued individual securities, and using thoughtful risk management to
construct portfolios which balance potential risks and reward in an
ever-changing world.

We encourage you to learn more about Pioneer and our time-tested approach to
investing by consulting with your financial advisor or visiting us online at
us.pioneerinvestments.com. We greatly appreciate your trust in us, and we thank
you for investing with Pioneer.

Sincerely,
