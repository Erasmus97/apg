Dear Shareholders, 
After nine years of serving as lead director and independent chairman of the Nuveen Fund Board, my term of office is coming to an end. It has been a privilege to use this space to communicate with you on some of
the broad economic trends in the U.S. and abroad and how they are impacting the investment environment in which your funds operate. In addition, I have enjoyed offering some perspective on how your Board views the various Nuveen investment teams as
they apply their investment disciplines in that investment environment. 
My term has coincided with a particularly challenging period for both mutual
fund sponsors and investors. Since 2000 there have been three periods of unusually strong stock market growth and two major market declines. Recent years have been characterized by a search for yield in fixed income securities to compensate for an
extended period of very low interest rates. Funds are investing more in foreign and emerging markets that require extensive research capabilities to overcome the more limited transparency and higher volatility in those markets. New fund concepts
often incorporate derivative financial instruments that offer efficient ways to hedge investment risk or gain exposure to selected markets. Fund trading teams operate in many new domestic and international venues with quite different
characteristics. Electronic trading and global communication networks mean that fund managers must be able to thrive in financial markets that react instantaneously to newsworthy events and are more interconnected than ever. 
Nuveen has committed additional resources to respond to these changes in the fund industry environment. It has added IT and research resources to assemble and
evaluate the increased flow of detailed information on economies, markets and individual companies. Based on its experience during the financial crisis of 2008-09, Nuveen has expanded its resources dedicated to valuing and trading portfolio
securities with a particular focus on stressed financial market conditions. It has added systems and experienced risk management professionals to work with investment teams to better help evaluate whether their funds’ risk exposures are
appropriate in view of the return targets. The investment teams have also reflected on recent experience to reaffirm or modify their investment disciplines. Finally, experienced professionals and IT resources have been added to address new
regulatory requirements designed to better inform and protect investors. The Board has enthusiastically encouraged these initiatives. 
The Nuveen Fund
Board has always viewed itself as your representatives to assure that Nuveen brings together experienced people, proven technologies and effective processes designed to produce results that meet investor expectations. It is important to note that
our activities are highlighted by the annual contract renewal process. Despite its somewhat formal language, I strongly encourage you to read the summary because it offers an insight into our oversight process. The report is included in the back of
this or a subsequent shareholder report. The renewal process is very comprehensive and includes a number of evaluations and discussions between the Board and Nuveen during the year. The summary also describes what has been achieved across the Nuveen
fund complex and at individual funds such as yours. 
As I leave the chairmanship and resume my role as a member of the Board, please be assured that I
and my fellow Board members will continue to hold your interests uppermost in our minds as we oversee the management of your funds and that we greatly appreciate your confidence in your Nuveen fund. 
Very sincerely, 


