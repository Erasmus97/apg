Dear Shareholder:
 
We are pleased to provide you with the
Touchstone Tax-Free Trust Annual Report. Inside you will find key financial information for the 12 months ended June 30, 2013.
 
Uneven economic conditions and volatility
reigned throughout most of the fiscal year. Despite a U.S Presidential election that left many apprehensive about the potential
direction of spending and taxation, and a Congressional “fiscal cliff” that brought the return of the payroll tax,
the U.S. economy, although uneven, continued to grow at a moderate pace. Stock market gains and rising home prices helped to bolster
consumer confidence, which sparked consumer spending. Employment was also a bright spot as new jobs were added to the economy and
the unemployment rate declined. Federal Reserve (the Fed) policy remained accommodative throughout the period as the Fed kept short-term
rates in the 0.00% - 0.25% range where they have been since 2008. Late in the period the Fed indicated it might start scaling back
its massive bond-buying program, negatively affecting both equity and bond markets.
 
The municipal market performed well during
the first half of the period, due to the continued low interest rate environment and strong investor demand. When interest rates
began to rise as a result of the Fed’s actions, the impact on the overall market was dramatic and the municipal market experienced
a significant increase in yields in the final two months of the period. As a result, municipals ended the fiscal year as one of
the poorer performing bond market sectors.
 
Although the municipal sector faces many
challenges, it is believed that the Ohio economy has been one of the leaders nationwide, especially regarding employment growth
and enacting legislation to improve the funding levels of its pension plans.
 
The Touchstone Ohio Tax-Free Bond Fund
seeks the highest level of interest income exempt from federal income and Ohio personal income taxes consistent with the protection
of capital. It invests primarily in high-quality long-term Ohio municipal bonds and seeks the appropriate alignment of risk and
return in an attempt to provide the most favorable opportunity to maximize risk-adjusted performance.
 
Touchstone’s tax-free money market
funds seek current tax-free income combined with liquidity and stability. The Funds seek the highest level of interest income exempt
from Federal (and in some cases, state) income tax, consistent with protection of capital by primarily investing in high-quality,
short-term municipal obligations.
 
We believe it remains more important than
ever to focus on the long-term composition of your investment portfolio as diversification is essential to balancing risk and return.
We recommend that you continue to work with your financial professional on a sound asset allocation strategy to help keep your
financial goals on course.
 
We greatly appreciate your continued support.
Thank you for including Touchstone as part of your investment plan.
 
Sincerely,


