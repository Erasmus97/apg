Dear Valued Shareholder: 
Wells Fargo Advantage Equity Gateway Funds
Central banks continued to provide stimulus. 
Major central banks, including the U.S. Federal Reserve Board (Fed) and the European Central Bank (ECB), continued to inject liquidity into the banks and the markets
through various quantitative easing policies. Throughout the reporting period, the Federal Open Market Committee (FOMC) kept its key interest rates effectively at zero in order to support the economy and the financial system. After its September
2012 meeting, the FOMC announced its intention to keep interest rates low until at least mid-2015 and to make open-ended purchases of $40 billion per month in mortgage-backed securities to support the housing market. However, toward the end of the
reporting period, Fed Chairman Ben Bernanke gave testimony to Congress in which he hinted that the FOMC might taper its bond purchases over its next two meetings. 
In July 2012, the ECB cut its key rate to a historic low of 0.75%. In addition, the ECB announced in September 2012 that it would purchase an unlimited amount of one-
to three-year sovereign debt from countries that had formally applied for a bailout. The ECB’s aggressive actions helped ease investor worries about a eurozone sovereign debt default. Moreover, in mid-March 2013, the governor of the Bank of
Japan resigned to allow Shinzo Abe, the recently reelected prime minister, to appoint a new bank governor who announced aggressive monetary policies to attack Japan’s persistent deflation. 
The debt crisis in the eurozone remained on center stage but with less impact than before. 
Ongoing weakness in the Greek economy made it difficult for the country to meet its austerity targets, even as investors worried about a stagnant economy and political
gridlock in Italy, as well as the continued fallout from a housing market bust in Spain. Because many eurozone banks owned Greek, Spanish, and Italian debt and many U.S. banks had financial ties to eurozone banks, investors continued to worry that a
debt default in southern Europe might result in another financial crisis. However, as central banks continued to provide liquidity, investor worries about the effect of a European sovereign debt default on the global economy eased. When the tiny
eurozone nation of Cyprus was forced to implement capital controls and impose losses on uninsured bank depositors in March 2013, global stock markets remained resilient despite short-term volatility. 
U.S. and developed foreign market stocks gained on relatively good news. 
For most of the period, U.S. economic data remained moderately positive. Reported gross domestic product (GDP) growth came in at a 1.3% annualized rate in the second
quarter of 2012, slower than the pace in the first quarter of 2012 but in solidly positive territory. Reported GDP growth reaccelerated to a 3.1% annualized rate in the third quarter of 2012, only to fall back to a 0.4% annualized rate in the fourth
quarter of 2012. Many analysts attributed the fourth-quarter 
Table of Contents
 

weakness to the temporary aftereffects from Hurricane Sandy, which devastated the Eastern Seaboard in October 2012—a view that was given credence by the rebound in GDP growth to a 2.4%
annualized rate in the first quarter of 2013. Even the stubbornly high unemployment rate showed signs of improvement, declining from 8.2% in June 2012 to 7.6% in May 2013. The federal budget sequestration that took effect on March 1, 2013, had the
potential to reduce economic growth, but investors paid more attention to economic fundamentals, such as a recovery in the housing market. 
1
We employ a diverse array of investment strategies, even as many variables are at work in the market. 
Wells Fargo Advantage Funds
Wells Fargo Advantage Funds
Sincerely, 


Dear Valued Shareholder: 
Wells Fargo Advantage Income Funds
Despite risk aversion at the beginning of the period, U.S. fixed-income
markets generally rallied over the first six months. 
In May 2012, just prior to the reporting period, global credit markets were agitated once again by a
crescendo of sovereign debt problems in Europe that appeared to threaten the very viability of the euro. Consequently, global investors returned to a protective posture and rallied to the U.S. Treasury market, driving long-term yields to some of
their lowest levels on record. U.S. Treasuries and the highest-rated credit tiers were the best performers during the first two weeks of June 2012, while the lower-rated credit tiers generally performed positively but offered less price return. 
1
In November and December 2012, most investment-grade sectors began to throttle down. U.S. Treasuries and TIPS
rallied in November 2012 and outperformed the credit sectors due to a brief flare-up in risk aversion from the deepening recessionary conditions in the eurozone. However, in December 2012, U.S. Treasury yields reversed course and began to shift
higher on growing optimism for economic expansion and appreciating equity values. Most investment-grade credit tiers also saw yields move higher in tandem, resulting in a broad but modest decline in core bond prices. 
Strengthening economic optimism in 2013 led to declines in bond prices as yields rose higher. 
In the opening months of 2013, U.S. Treasury yields began to once again rise higher on optimistic expectations for a strengthening U.S. economy. Consequently,
fixed-income security yields shifted higher and prices declined across much of the U.S. investment-grade bond markets. U.S. Treasury and TIPS

 
 
Table of Contents
 

prices significantly declined in January 2013, most notably in the longer-maturity ranges, as investors began to reprice bond yields for potential interest-rate increases in upcoming years. 
In February and March 2013, investment-grade fixed-income markets rebounded when equity market exuberance was reined in by some sobering signs of caution from mixed
economic indicators. Investors again returned to the U.S. Treasury market, driving yields lower and bond prices higher. These trends only deepened during a volatile month of April that was rattled by geopolitical events, domestic terrorism in
Boston, and uncertain global economic conditions. U.S. Treasury and TIPS prices increased sharply, temporarily erasing all the losses they endured in January 2013. 
Unfortunately for the core bond investment-grade markets, May 2013 unleashed a massive rally in U.S. equity markets. U.S. Treasury prices declined, investment-grade
corporate bond prices declined, and structured product prices declined. On the whole, the positive gains during 2013 were unwound in May, resulting in modestly negative returns for 2013 thus far in each sector except for CMBS. 
2
Don’t let short-term uncertainty derail long-term investment goals.

Wells Fargo Advantage Funds
Wells Fargo Advantage Funds
Sincerely, 


Dear Valued Shareholder: 
Much of the period was marked by continued concerns about the possible effects that the ongoing European sovereign debt crisis would have on the global economy.
However, relatively solid economic data in the U.S., efforts by European authorities to address the sovereign debt issue and improving U.S. corporate fundamentals helped alleviate investor fears and supported global financial markets during much of
the reporting period. 
Global accommodative monetary policy provided economic support, but political uncertainties were present.

The U.S. economy grew modestly over the past year, but the recovery remained weak compared with previous business cycles. Three forces—discretionary
fiscal policy, housing market activity, and household income expectations—have not been firing on all cylinders; therefore, the recovery has been modest. However, the unemployment rate continued to decline, reaching 7.6% in May 2013, and the
housing market advanced from its recessionary levels. 
Global economic growth varied across countries. Eurozone economies remained in recession. However, the
European Central Bank and several key members of the eurozone, including France and Germany, announced their commitment to maintaining the integrity of the single currency. Subsequently, investor fears about a sovereign debt crisis in Europe abated.
In China, economic growth decelerated from double-digit advances but remained at a stable, positive pace. 
Citing its dual mandate of fostering maximum employment
and price stability, the U.S. Federal Reserve (Fed) reaffirmed its commitment to highly accommodative monetary policy. In addition, the Fed formally announced that exceptionally low interest rates would be appropriate at least as long as the
unemployment rate remains above 6.5% and inflation remains no more than half a percentage point above its 2.0% inflation target. It also continued its quantitative easing program. However, after the reporting period ended, the Fed indicated that it
may begin to reverse some of its quantitative easing activities by the end of 2013, increasing investor concerns over the potential of higher interest rates. As a result, U.S. bond yields rose and U.S. equity prices stumbled. 
For most of the year, uncertainty about political outcomes was a constant. U.S. presidential elections in November 2012, the debate over the fiscal cliff and tax rates
at year-end, and speculation about the effects of sequestration at the end of February 2013 weighed on business leaders and investors. Elections in eurozone countries such as Greece, France, and Italy sparked speculation about the future of the
eurozone. New leadership in China also figured into the political landscape. In the end, investors managed around these concerns and capital markets generally advanced during this time period. 
Ongoing debt problems in the eurozone were persistent. 
Early in the period, concerns about the eurozone sovereign debt situation held center stage as investors again focused on economic weakness in southern European
economies. Because many eurozone banks owned southern European debt and many U.S. banks had financial ties to eurozone banks, investors worried about the effects of a possible southern European debt default on the global financial system and
economy. Ongoing weakness in the Greek economy made it difficult for the country to meet previously agreed-upon austerity targets.

Table of Contents
 
Wells Fargo Advantage WealthBuilder Portfolios
Eurozone uneasiness returned to the markets during the final few months of the reporting period due to a bank debt crisis
in Cyprus. Although the island represented less than 0.5% of European Union gross domestic product, the European troika decided to break precedent and impose losses not only on debt and equity holders but also on uninsured depositors in the
country’s two failing banks. As a result, volatility sharply increased in several financial markets as investors grew concerned about the long-term impact of the Cyprus deal. 
Financial markets advanced. 
®
3
 
 
Despite ongoing uncertainties, investor confidence grew during the period as political and economic concerns lessened. As a result, equity
markets and high-yield bond markets sharply advanced. 
 
 
 
 
 
 
 
 
 
 
Table of Contents
 
Our goal is to meet the financial needs of our shareholders. 
We are committed to providing our shareholders with long-term investment strategies and focusing on appropriate risk while seeking to deliver consistent returns. We know
that your ability to meet your long-term investment goals depends on the investment decisions you make today. Despite economic uncertainties and investment challenges, staying invested and adapting to emerging opportunities and threats will help you
manage investment risk. 
Wells Fargo Advantage Funds
Sincerely, 


Dear Valued Shareholder: 
Much of the period was marked by continued concerns about the possible effects that the ongoing European sovereign debt crisis would have on the global economy.
However, relatively solid economic data in the U.S., efforts by European authorities to address the sovereign debt issue and improving U.S. corporate fundamentals helped alleviate investor fears and supported global financial markets during much of
the reporting period. 
Global accommodative monetary policy provided economic support, but political uncertainties were present.

The U.S. economy grew modestly over the past year, but the recovery remained weak compared with previous business cycles. Three forces—discretionary
fiscal policy, housing market activity, and household income expectations—have not been firing on all cylinders; therefore, the recovery has been modest. However, the unemployment rate continued to decline, reaching 7.6% in May 2013, and the
housing market advanced from its recessionary levels. 
Global economic growth varied across countries. Eurozone economies remained in recession. However, the
European Central Bank and several key members of the eurozone, including France and Germany, announced their commitment to maintaining the integrity of the single currency. Subsequently, investor fears about a sovereign debt crisis in Europe abated.
In China, economic growth decelerated from double-digit advances but remained at a stable, positive pace. 
Citing its dual mandate of fostering maximum employment
and price stability, the U.S. Federal Reserve (Fed) reaffirmed its commitment to highly accommodative monetary policy. In addition, the Fed formally announced that exceptionally low interest rates would be appropriate at least as long as the
unemployment rate remains above 6.5% and inflation remains no more than half a percentage point above its 2.0% inflation target. It also continued its quantitative easing program. However, after the reporting period ended, the Fed indicated that it
may begin to reverse some of its quantitative easing activities by the end of 2013, increasing investor concerns over the potential of higher interest rates. As a result, U.S. bond yields rose and U.S. equity prices stumbled. 
For most of the year, uncertainty about political outcomes was a constant. U.S. presidential elections in November 2012, the debate over the fiscal cliff and tax rates
at year-end, and speculation about the effects of sequestration at the end of February 2013 weighed on business leaders and investors. Elections in eurozone countries such as Greece, France, and Italy sparked speculation about the future of the
eurozone. New leadership in China also figured into the political landscape. In the end, investors managed around these concerns and capital markets generally advanced during this time period. 
Ongoing debt problems in the eurozone were persistent. 
Early in the period, concerns about the eurozone sovereign debt situation held center stage as investors again focused on economic weakness in southern European
economies. Because many eurozone banks owned southern European debt and many U.S. banks had financial ties to eurozone banks, investors worried about the effects of a possible southern European debt default on the global financial system and
economy. Ongoing weakness in the Greek economy made it difficult for the country to meet previously agreed-upon austerity targets.

Table of Contents
 
Eurozone uneasiness returned to the markets during the final few months of the reporting period due to a bank debt crisis
in Cyprus. Although the island represented less than 0.5% of European Union gross domestic product, the European troika decided to break precedent and impose losses not only on debt and equity holders but also on uninsured depositors in the
country’s two failing banks. As a result, volatility sharply increased in several financial markets as investors grew concerned about the long-term impact of the Cyprus deal. 
Financial markets advanced. 
®
3
 
 
 
Despite ongoing uncertainties, investor
confidence grew during the period as political and economic concerns lessened. As a result, equity markets and high-yield bond markets sharply advanced. 
 
 
 
 
 
 
 
 
 
Table of Contents
 
Our goal is to meet the financial needs of our shareholders. 
We are committed to providing our shareholders with long-term investment strategies and focusing on appropriate risk while seeking to deliver consistent returns. We know
that your ability to meet your long-term investment goals depends on the investment decisions you make today. Despite economic uncertainties and investment challenges, staying invested and adapting to emerging opportunities and threats will help you
manage investment risk. 
Wells Fargo Advantage Funds
Sincerely, 


