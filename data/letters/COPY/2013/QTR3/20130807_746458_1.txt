Dear Investor:
 
Thank you for reviewing this annual report for the 12 months ended May 31, 2013. It provides investment performance, market analysis, and portfolio information, presented with the expert perspective of our portfolio management team.
 
Annual reports remain important vehicles for conveying information about fund returns, including key factors that affected fund performance. For additional, updated investment and market insights, we encourage you to visit our website, americancentury.com. 
 
Positive Fiscal-Year Returns for Stocks and Municipal Bonds
 
The 12-month reporting period ended May 31, 2013 started with softening global economic conditions and growing uncertainty in the summer of 2012 as the U.S. elections and dreaded U.S. fiscal deadlines loomed ahead. These factors, and the recession fears they represented, helped provoke aggressive monetary intervention by central banks, which encouraged investors to take more risk.
 
In this “risk-on” investing environment, stocks generally outperformed bonds, broad non-U.S. stock measures outperformed their broad U.S. stock counterparts, and U.S. municipal bonds generally outperformed U.S. Treasury bonds.
 
Non-U.S., U.S. mid-cap, small-cap, and value stock indices achieved performance leadership during the period, outpacing the S&P 500 Index’s 27.28% return. The MSCI EAFE Index, for example, returned 31.62%. U.S. bond index returns ranged from roughly 15% gains for corporate high-yield indices all the way down to negative returns for longer-maturity U.S. Treasury benchmarks. For example, the 10-year U.S. Treasury note returned –2.17%, according to Barclays, as its yield rose over half a percentage point, from 1.56% to 2.13%.
 
Municipal bonds, which enjoyed strong demand for much of the period, generally fit more in the middle of the U.S. bond return spectrum. The Barclays Municipal Bond Index advanced 3.05%.
 
The U.S. economy is showing signs of improvement in 2013, particularly the long-depressed housing market. However, U.S. economic growth remains subpar compared with past recession recoveries, and is still vulnerable to fiscal, financial, and overseas threats that could trigger further slowdowns and market volatility.
 
Under these conditions, we continue to believe in a disciplined, diversified, long-term investment approach, using professionally managed stock and bond portfolios—as appropriate—for meeting financial goals. We appreciate your continued trust in us in this challenging environment. 
 
Sincerely,


Dear Investor:
 
Thank you for reviewing this annual report for the 12 months ended May 31, 2013. It provides investment performance, market analysis, and portfolio information, presented with the expert perspective of our portfolio management team.
 
Annual reports remain important vehicles for conveying information about fund returns, including key factors that affected fund performance. For additional, updated investment and market insights, we encourage you to visit our website, americancentury.com. 
 
Positive Fiscal-Year Returns for Stocks and Municipal Bonds
 
The 12-month reporting period ended May 31, 2013 started with softening global economic conditions and growing uncertainty in the summer of 2012 as the U.S. elections and dreaded U.S. fiscal deadlines loomed ahead. These factors, and the recession fears they represented, helped provoke aggressive monetary intervention by central banks, which encouraged investors to take more risk.
 
In this “risk-on” investing environment, stocks generally outperformed bonds, broad non-U.S. stock measures outperformed their broad U.S. stock counterparts, and U.S. municipal bonds generally outperformed U.S. Treasury bonds.
 
Non-U.S., U.S. mid-cap, small-cap, and value stock indices achieved performance leadership during the period, outpacing the S&P 500 Index’s 27.28% return. The MSCI EAFE Index, for example, returned 31.62%. U.S. bond index returns ranged from roughly 15% gains for corporate high-yield indices all the way down to negative returns for longer-maturity U.S. Treasury benchmarks. For example, the 10-year U.S. Treasury note returned –2.17%, according to Barclays, as its yield rose over half a percentage point, from 1.56% to 2.13%.
 
Municipal bonds, which enjoyed strong demand for much of the period, generally fit more in the middle of the U.S. bond return spectrum. The Barclays Municipal Bond Index advanced 3.05%.
 
The U.S. economy is showing signs of improvement in 2013, particularly the long-depressed housing market. However, U.S. economic growth remains subpar compared with past recession recoveries, and is still vulnerable to fiscal, financial, and overseas threats that could trigger further slowdowns and market volatility.
 
Under these conditions, we continue to believe in a disciplined, diversified, long-term investment approach, using professionally managed stock and bond portfolios—as appropriate—for meeting financial goals. We appreciate your continued trust in us in this challenging environment. 
 
Sincerely,


Dear Investor:
 
Thank you for reviewing this annual report for the 12 months ended May 31, 2013. It provides investment performance, market analysis, and portfolio information, presented with the expert perspective of our portfolio management team.
 
Annual reports remain important vehicles for conveying information about fund returns, including key factors that affected fund performance. For additional, updated investment and market insights, we encourage you to visit our website, americancentury.com. 
 
Positive Fiscal-Year Returns for Stocks and Municipal Bonds
 
The 12-month reporting period ended May 31, 2013 started with softening global economic conditions and growing uncertainty in the summer of 2012 as the U.S. elections and dreaded U.S. fiscal deadlines loomed ahead. These factors, and the recession fears they represented, helped provoke aggressive monetary intervention by central banks, which encouraged investors to take more risk.
 
In this “risk-on” investing environment, stocks generally outperformed bonds, broad non-U.S. stock measures outperformed their broad U.S. stock counterparts, and U.S. municipal bonds generally outperformed U.S. Treasury bonds.
 
Non-U.S., U.S. mid-cap, small-cap, and value stock indices achieved performance leadership during the period, outpacing the S&P 500 Index’s 27.28% return. The MSCI EAFE Index, for example, returned 31.62%. U.S. bond index returns ranged from roughly 15% gains for corporate high-yield indices all the way down to negative returns for longer-maturity U.S. Treasury benchmarks. For example, the 10-year U.S. Treasury note returned –2.17%, according to Barclays, as its yield rose over half a percentage point, from 1.56% to 2.13%.
 
Municipal bonds, which enjoyed strong demand for much of the period, generally fit more in the middle of the U.S. bond return spectrum. The Barclays Municipal Bond Index advanced 3.05%.
 
The U.S. economy is showing signs of improvement in 2013, particularly the long-depressed housing market. However, U.S. economic growth remains subpar compared with past recession recoveries, and is still vulnerable to fiscal, financial, and overseas threats that could trigger further slowdowns and market volatility.
 
Under these conditions, we continue to believe in a disciplined, diversified, long-term investment approach, using professionally managed stock and bond portfolios—as appropriate—for meeting financial goals. We appreciate your continued trust in us in this challenging environment. 
 
Sincerely,


Dear Investor:
 
Thank you for reviewing this annual report for the 12 months ended May 31, 2013. It provides investment performance, market analysis, and portfolio information, presented with the expert perspective of our portfolio management team.
 
Annual reports remain important vehicles for conveying information about fund returns, including key factors that affected fund performance. For additional, updated investment and market insights, we encourage you to visit our website, americancentury.com. 
 
Positive Fiscal-Year Returns for Stocks and Municipal Bonds
 
The 12-month reporting period ended May 31, 2013 started with softening global economic conditions and growing uncertainty in the summer of 2012 as the U.S. elections and dreaded U.S. fiscal deadlines loomed ahead. These factors, and the recession fears they represented, helped provoke aggressive monetary intervention by central banks, which encouraged investors to take more risk.
 
In this “risk-on” investing environment, stocks generally outperformed bonds, broad non-U.S. stock measures outperformed their broad U.S. stock counterparts, and U.S. municipal bonds generally outperformed U.S. Treasury bonds.
 
Non-U.S., U.S. mid-cap, small-cap, and value stock indices achieved performance leadership during the period, outpacing the S&P 500 Index’s 27.28% return. The MSCI EAFE Index, for example, returned 31.62%. U.S. bond index returns ranged from roughly 15% gains for corporate high-yield indices all the way down to negative returns for longer-maturity U.S. Treasury benchmarks. For example, the 10-year U.S. Treasury note returned –2.17%, according to Barclays, as its yield rose over half a percentage point, from 1.56% to 2.13%.
 
Municipal bonds, which enjoyed strong demand for much of the period, generally fit more in the middle of the U.S. bond return spectrum. The Barclays Municipal Bond Index advanced 3.05%.
 
The U.S. economy is showing signs of improvement in 2013, particularly the long-depressed housing market. However, U.S. economic growth remains subpar compared with past recession recoveries, and is still vulnerable to fiscal, financial, and overseas threats that could trigger further slowdowns and market volatility.
 
Under these conditions, we continue to believe in a disciplined, diversified, long-term investment approach, using professionally managed stock and bond portfolios—as appropriate—for meeting financial goals. We appreciate your continued trust in us in this challenging environment. 
 
Sincerely,


