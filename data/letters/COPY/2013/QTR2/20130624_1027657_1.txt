Dear NYSA Fund Shareholder,
The reporting period was marked by considerable volatility.  The fiscal year ended March 31, 2013, with both positive and negative global events. At the beginning of March 2012, many investors were still skittish about taking on risk.  At the conclusion of 2012, many market indices posted positive returns, and many investors were pleasantly surprised with the performance of their equity positions.  As the Fund’s fiscal year concluded, many market indices continued to experience positive returns in the first quarter of 2013.
Market Overview
As noted above, many benchmark indices appreciated throughout 2012, with some experiencing double-digit returns.  Overall, global economic growth remained lethargic, but positive.  Many global economies were still experiencing high unemployment, uneasy political elections, potential year-end tax increases and government spending cuts.  These events caused many investors to remain pessimistic.
Investor hesitation continued in the United States, driven, in part, by the volatility of the global markets, and, in part, by the lingering effects of the 2008 financial crisis.  The Federal Reserve continued its stance of continued quantitative easing in an effort to ensure ample liquidity in the marketplace.  These efforts contributed to lower bond yields on fixed-income instruments, which lured many investors back into riskier assets. The domestic housing market and unemployment continued to recover, while corporations continued to strengthen their balance sheets.  Abroad, we saw the European debt crisis finally begin to subside, with the Chinese, European, and Japanese Central Banks creating their own measures to keep liquidity flowing in their marketplaces.
Fund Performance
Average Annual Total Returns
(For the period ending March 31, 2013)
FUND/INDEX
1-YEAR
5-YEAR
10-YEAR
NYSA Fund (1)
-9.57%
-4.41%
0.85%
S&P 500 Index (2)
13.96%
5.81%
8.53%
