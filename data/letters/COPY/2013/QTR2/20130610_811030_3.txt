Dear Fellow Investor,



CANGX

NorthCoast Asset Management, Advisor

Annual Report, March



So, first of all, let me assert my firm belief that the only thing we have to fear is...fear itself



Franklin D. Roosevelt



Our President boldly delivered these words during his first inauguration speech, and after a year of market action, we struggle to find a quotation that better captures todays mindset. Many investors are scared.The data (put/call ratios, bull/bear sentiment indicators, and dollars redeeming from equity funds) not only supports this view but also the intelligence we garner in simply speaking with investors daily in the course of our business.



As our investors know, however, fear or any emotion for that matter, does not guide our investment portfolios.Data does.And though it felt scary along the way, a strong market helped propel the CAN SLIM Select Growth Fund for the fiscal year.



During the fiscal year April through March CAN SLIM Select Growth Fund (CANGX) gained compared to in the S&amp;P to increasingly weak economic data in Europe, stock market exposure was reduced to a low of in May of In the following months, positions were steadily added in response to strong market internals. The Fund closed the fiscal year March at invested.Overweight positions in Consumer Discretionary, Consumer Staples and Utilities attributed to the outperformance.Stocks like The Madison Square Garden Co. (MSG) and Western Refining, Inc. (WNR) were large winners for us during the year.



As we all have seen, there are frightening news stories out there Italian elections, Cyprus, sequestration, and fiscal cliff negotiations to name a few.But headline news doesnt drive the market and no one makes much money watching CNBC.The market moves over time

























CAN SLIM Select Growth Fund




















because of data. We measure that data everyday and adjust accordingly.We rely on a rigorously tested combination of technical, valuation, sentiment, and macroeconomic indicators.These together signaled an opportunistic market in the first quarter ending March and based on that, we invested and our clients gained.



Our approach to managing market exposure is similar in design to how one would drive a car.We adjust investments to market conditions much like a driver would to driving conditions. Consider the speed at which one drives most of the time, on the highway at least, mph is a pretty comfortable ride. We all know, however, that there are factors in which changes in speed (faster or slower) are necessary. The quickest and most efficient drive is the one that actively adapts to the driving environment. Clear roads and fewer drivers allow us to drive faster, while heavy traffic and snow storms will inevitably slow us down.Driving mph through a snow-storm makes for a dangerous, anxiety-filled ride...just as driving mph on an open highway isnt getting you where you need to go in the time you need to get there. You adapt your driving to the conditions around you. No single condition determines our speed on the road just as no single factor can navigate the market. Its the aggregate that matters.



At the end of the first quarter of our market outlook hasnt varied.It still calls for a bullish posture, and as the second quarter unfolds, we will continue to monitor these indicators daily and act accordingly.That doesnt mean a trap door couldnt open up and wreak havoc for weeks.In todays volatile environment, we wouldnt be surprised to witness as many as five, corrections a year now as markets move more quickly.That could happen.But what the outlook does mean is that the current market condition is strong and that a bear market decline is unlikely.

























CAN SLIM Select Growth Fund













Consider the data of years ago versus today:







Market Indicator









Valuation Earnings Yield





of S&amp;P Index minus





Year Treasury Note









Valuation Inflation





(as measured by the





Consumer Price Index)









Macroeconomic Current





Economic


Declining


Steady/Increasing



Sentiment Net Equity





Mutual Fund


Strongly Negative


Steady



Technical VIX





(as measured by the





Volatility Index)















Thank you for your business.It is a privilege to serve shareholders as advisor to the fund and look forward to a bright future together.










Patrick Jamin










Current Economic Activity A proprietary indicator measured by Goldman Sachs, named US Current Activity Index (CAI)











Net Equity Mutual Fund Flow Mutual fund data measured by Investment Company Institute (ICI)
































CAN SLIM Select Growth Fund













Opinions expressed are subject to change, are not guaranteed and should not be considered a recommendation to buy or sell any security.



Past performance is no guarantee of future results.



Mutual fund investing involves risk. Principal loss is possible.The Fund may invest in foreign securities which will involve greater volatility and political, economic and currency risks and differences in accounting methods. The Fund invests in micro-, small-and medium-capitalization companies, which involve additional risks such as limited liquidity and great price volatility than large-capitalization companies. The Fund will experience portfolio turnover, which may result in adverse tax consequences to the Funds shareholders.Because the Fund can invest in other investment companies your cost of investing in the Fund will be higher than your cost of investing directly in the shares of the mutual funds in which it invests. By investing in the Fund, you will indirectly bear your share of any fees and expenses charged by the underlying funds, in addition to indirectly bearing the principal risks of those funds. Because the fund invests in ETFs, it is subject to additional risks that do not apply to conventional mutual funds, including the risks that the market price of an ETFs shares may trade at a discount to its net asset value (NAV), an active secondary trading market may not develop or be maintained, or trading may be halted by the exchange in which they trade, which may impact a funds ability to sell its shares.



Investment performance reflects fee waivers. In the absence of fee waivers, total returns would be reduced.



The S&amp;P Index is a broad based unmanaged index of stocks, which is widely recognized as representative of the equity market in general.The VIX Index is an index measuring the expected level of volatility in the U.S. stock market over the next days.You cannot invest directly in an index.



Fund holdings and sector allocations are subject to change and should not be considered a recommendation to buy or sell any security. For a complete list of holdings, please refer to the Schedule of Investments in this report.Current and future holdings are subject to risk.



Must be preceded or accompanied by a current prospectus.



The CAN SLIM Select Growth Fund is distributed by Quasar Distributors, LLC.


































CAN SLIM Select Growth Fund













SECTOR ALLOCATION



AT MARCH (UNAUDITED)
