Dear Fellow Shareholder:



Credit crunch financial crisis fiscal cliff these are just some of the terms that have been in the media over the past year. Its enough to make your head spin and your stomach turn.



While we certainly take note of the opinions in the press, Management of your Trust doesnt unduly stress over them. Why? Because we stick to basics emphasis on high quality securities, intermediate maturity and geographic diversification among projects and communities throughout the islands. Add to this, local orientation, detailed research and top quality service providers, and we believe we have a formula that has served you, and our other shareholders, well over the years in your search for preservation of capital and tax-free income. And, we believe it will continue to serve you well in whatever economic environment prevails during



Investment Quality. No matter what the quality rating for a particular security may be, it will still be subject to market fluctuations even in the calmest of markets. However, in general, the higher the quality rating of a municipal security, the greater and more reliable the cash flow there is for the municipality to cover interest and principal payments when due on the security. Exaggerated price changes that may occur in emotionally charged securities markets normally do not represent the ability of a municipal issuer to pay interest and principal in a timely manner on any particular security. It is the cash flow and solidness of the municipal issuer that count - and this is reflected in the quality level of the credit rating.



Thus, in accordance with your Trusts prospectus, Hawaiian Tax-Free Trust may only purchase investment grade securities those rated within the top four credit ratings by a nationally-recognized statistical rating organization - or, if unrated, determined by your investment team to be of comparable quality. We have specifically designed your Trust this way since we believe there is no substitute for quality.



Intermediate Maturity. Through utilizing a blend of maturities both shorter-term and longer-term Hawaiian Tax-Free Trust attempts to provide a satisfactory level of return without subjecting the share price to excessive swings as interest rates increase and decrease. We feel that this approach takes the best that each investment has to offer gaining stability from the shorter-term maturities and higher yields from the longer-term maturities.



Diversification of the Portfolio. To the maximum extent possible, Hawaiian Tax-Free Trust strives to invest in as many projects, and types of projects, as possible throughout the state. This is done not only to limit exposure in any particular situation, but also to enhance the quality of life throughout Hawaii by financing worthy municipal projects.



NOT A PART OF THE ANNUAL REPORT























Local Orientation. To strengthen the fingers that Hawaiian Tax-Free Trust keeps on the pulse of the communities it serves, we have intentionally structured your Trusts Management to include several residents of Hawaii. With this structure, we believe your Trust is more sensitive to the subtle nuances within Hawaii.



Detailed Research. The research conducted prior to investing in a bond, and ongoing credit monitoring, make it possible to evaluate potential risks associated with an individual bond and the adequacy of the compensation provided for that risk. Simply put, we seek to evaluate whether, as a bond investor, your Trust is adequately compensated for the risk associated with lending to a particular issuer.



Top Quality Service Providers. We seek to ensure that highly qualified and knowledgeable organizations look after your investment on a day-to-day basis. The Administrator, Adviser, shareholder servicing and transfer agent, custodian, fund accounting agent, security pricing services, distributor, legal counsel, and auditors were all very carefully chosen and, in our opinion, possess a high level of integrity and expertise.



So, when it appears that the world just might be coming apart at the seams, rest assured that Management of your Trust intends to continue to stick to some tried and true basics.



Sincerely,
