Dear Shareholder,
Still, Treasury bond yields began moving up from the historically low levels they had hit over the summer. Demand for perceived safe-harbor investments such as Treasury securities slackened as investors grew more optimistic that Europe’s financial troubles were stabilizing. Declining demand lowered bond prices and raised yields, an effect that was most pronounced in longer-maturity bonds.
You can see this in the returns of Vanguard Long-Term Treasury Fund: As yields declined in the first half of the fiscal year, the fund returned a robust 8%, but those gains were erased as yields subsequently rose. For fiscal 2013 as a whole, the fund returned 0% for Investor Shares and 0.10% for Admiral Shares. The effect was not as dramatic for the other U.S. Government Bond Funds because of their shorter maturities.
In the Treasury and Federal Funds, the impact of rising rates was mitigated somewhat by modest allocations to mortgage-backed securities, which performed relatively well during the period.
4
Bonds have been in a long-running bull market as prices climbed and yields tumbled for many years. (Prices and yields move in opposite directions.) Indeed, the yield of the 10-year U.S. Treasury note slipped to a record low in July, closing below 1.5%. That trend later reversed, and the 10-year yield broke 2% in late January.
As for money market funds and savings accounts, their returns barely budged as the Federal Reserve held short-term interest rates between 0% and 0.25%, a policy in place since late 2008.
 
5
European stocks gained about 20% even as many countries in the region continued to struggle with economic woes. This disparity isn’t as unusual as it may appear. Vanguard research has found that the relationship between a country’s economic growth and its stock market returns has typically been weak over time.
In the United States, attention to federal budget challenges intensified as 2012 drew to a close. The focus on the “fiscal cliff” led to investor anxiety before policymakers reached a limited tax rate agreement on the cusp of the new year. Although a credible long-term deficit-reduction strategy has yet to be crafted, investors propelled U.S. stocks to a 12-month gain of nearly 17%.
The fund expense ratios shown are from the prospectus dated May 30, 2012, and represent estimated costs for the current fiscal year. For the fiscal year ended January 31, 2013, the funds’ expense ratios were: for the Short-Term Treasury Fund, 0.20% for Investor Shares and 0.10% for Admiral Shares; for the Short-Term Federal Fund, 0.20% for Investor Shares and 0.10% for Admiral Shares; for the Intermediate-Term Treasury Fund, 0.20% for Investor Shares and 0.10% for Admiral Shares; for the GNMA Fund, 0.21% for Investor Shares and 0.11% for Admiral Shares; and for the Long-Term Treasury Fund, 0.20% for Investor Shares and 0.10% for Admiral Shares. Peer-group expense ratios are derived from data provided by Lipper Inc. and capture information through year-end 2012.
Peer groups: For the Short-Term Treasury Fund, Short-Term U.S. Treasury Funds; for the Short-Term Federal Fund, Short-Intermediate U.S. Government Funds; for the Intermediate-Term Treasury Fund, General U.S. Treasury Funds; for the GNMA Fund, GNMA Funds; and for the Long-Term Treasury Fund, General U.S. Treasury Funds.
6
or surpassed the bond price declines associated with rising market yields. As a result, except for Investor Shares of the Long-Term Treasury Fund, all of the share classes of all of the funds produced positive results.
I mentioned earlier that holdings of mortgage-backed securities played a role in boosting the returns of the Treasury and Federal Funds. The Treasury Funds took advantage of their ability to invest
 
The figures shown represent past performance, which is not a guarantee of future results. (Current performance may be lower or higher than the performance data cited. For performance data current to the most recent month-end, visit our website at vanguard.com/performance.) Note, too, that both investment returns and principal value can fluctuate widely, so an investor’s shares, when sold, could be worth more or less than their original cost.
7
a modest portion of assets in mortgage-backed securities; the Federal Fund, which invests a portion of its assets in mortgage securities, overweighted its typical position. The GNMA Fund, of course, concentrates on mortgage-backed securities, focusing on those whose principal and interest payments are guaranteed by the Government National Mortgage Association and, ultimately, the full faith and credit of the federal government.
The appeal of mortgage securities has stemmed from the response by certain homeowners to the unusually low level of mortgage rates, a consequence of the general interest rate environment. These homeowners have been slow to refinance
their mortgages because of lenders’ tightened loan standards in the wake of the financial crisis. This has allowed the funds to focus with greater confidence on mortgage securities that offer more attractive coupon interest rates, as there is less chance that mortgage prepayments will accelerate and negatively affect their returns.
In the spring of 2012, the Treasury Funds began selling their mortgage securities at a profit, a process that was completed as the year drew to a close. The Federal Fund also lightened its position. The sales removed some higher-yielding securities from the funds’ portfolios, which produced a decline in their SEC yields when compared with
 
8
the levels a year earlier. (You can read how SEC yields are calculated in the Glossary of this report.) For the GNMA Fund, by contrast, the lower SEC yield was more a reflection of the strong demand for mortgage securities during the year.
When it announced its latest bond-buying program in September, the Federal Reserve said it would purchase every month—surprisingly, no end date was specified—$45 billion of mortgage securities (in addition to $40 billion of Treasury securities). All of the funds’ MBS holdings benefited from investors’ anticipation of these purchases, although the extent of the Fed’s plans was larger than the market expected.
Among the Treasury and Federal Funds, which are advised by Vanguard’s Fixed Income Group, returns for all but the Intermediate-Term Treasury Fund have been in line with index benchmarks when you take into account operating costs. They also have surpassed the average returns of their peer groups. The GNMA Fund, advised by Wellington Management Company, performed similarly.
In producing these results, the funds have been aided not only by the skill of the advisors but also by Vanguard’s low expenses. Lower costs allow more of a fund’s earnings to be passed along to shareholders.
The first point to keep in mind is that investing has always involved uncertainties. When asked what he thought the stock market would do, financier J. P. Morgan had a standard response: “It will fluctuate.”
In our view, the most sensible approach is to focus on what’s within your control. In short, we believe investors can give themselves a greater chance for success by acting on four key points:
9
Successful investing doesn’t have to be complicated, but that’s not to say it’s easy. Investing can provoke strong emotions, and it’s hard to stay level-headed during times of volatility. If you’re getting pulled off course, try to redirect your attention to the principles I’ve outlined. These principles—which reflect the core philosophy Vanguard has held for decades—can be the answer to uncertainty.
As always, thank you for investing with Vanguard.
Sincerely,


Dear Shareholder,
Despite periods of heightened anxiety about the health of Spanish banks, “fiscal cliff” negotiations, and other troubling news, bond investors favored yield over safety in the 12 months ended January 31, 2013. This preference has become worryingly pronounced as the yields available from the safest securities hover near record lows.
High-yield bonds produced the highest returns, as investors bid up the prices of the riskiest assets. Vanguard High-Yield Corporate Fund returned 11.91% for the 12-month period. (Unless otherwise noted, returns mentioned in this letter are for Investor Shares.) The fund’s strong performance nevertheless fell short of its comparative standards in part because of its limited exposure to bonds at the lowest end of the credit-quality spectrum.
The Short-Term Investment-Grade Fund returned 3.48%, the Intermediate-Term Investment-Grade Fund 6.20%, and the Long-Term Investment-Grade Fund 7.39%. All three investment-grade funds outpaced their peers on the strength of good security selection. While the short-term and intermediate-term funds underperformed their benchmark indexes, the long-term fund outperformed its benchmark index.
2
Bond prices rose, accounting for more than one-third of the total return for each of the four funds. Given that bond prices and yields move in opposite directions, the funds’ 30-day SEC yields were lower at the end of January 2013 than they were a year earlier.
European stocks gained about 20% even as many countries in the region continued to struggle with economic woes. This disparity isn’t as unusual as it may appear. Vanguard research has found that the relationship between a country’s economic growth and its stock market returns has typically been weak over time.
In the United States, attention to federal budget challenges intensified as 2012 drew to a close. The focus on the “fiscal cliff” led to investor anxiety before policymakers reached a limited tax rate agreement on the cusp of the new year. Although a credible long-term deficit-
 
3
reduction strategy had yet to be crafted, investors propelled U.S. stocks to a gain of nearly 17% for the fiscal year.
As for short-term interest rates, there was virtually no room for them to move lower; the federal funds target rate for overnight borrowing has been held at 0% to 0.25% since December 2008.
The scarcity of income from short-term and high-quality bonds, along with tame inflation and the healthy finances of many companies, encouraged investors to venture into riskier corporate bonds offering more yield. Over the fiscal year, corporate profits reached record highs, balance sheets improved, and default rates remained relatively low.
The Short-Term and Intermediate-Term Investment-Grade Funds outperformed the average returns of their peer groups
The fund expense ratios shown are from the prospectus dated May 30, 2012, and represent estimated costs for the current fiscal year. For the fiscal year ended January 31, 2013, the funds’ expense ratios were: for the Short-Term Investment-Grade Fund, 0.20% for Investor Shares, 0.10% for Admiral Shares, and 0.07% for Institutional Shares; for the Intermediate-Term Investment-Grade Fund, 0.20% for Investor Shares and 0.10% for Admiral Shares; for the Long-Term Investment-Grade Fund, 0.22% for Investor Shares and 0.12% for Admiral Shares; for the High-Yield Corporate Fund, 0.23% for Investor Shares and 0.13% for Admiral Shares. Peer-group expense ratios are derived from data provided by Lipper Inc. and capture information through year-end 2012.
Peer groups: For the Short-Term Investment-Grade Fund, 1–5 Year Investment-Grade Debt Funds; for the Intermediate-Term Investment-Grade Fund, Intermediate Investment-Grade Debt Funds; for the Long-Term Investment-Grade Fund, Corporate A-Rated Debt Funds; and for the High-Yield Corporate Fund, High Yield Funds.
4
Selection among financial securities was a strength for all three funds. Banks, brokerage houses, and insurance companies have rallied as policymakers in Europe and the United States have
 
The figures shown represent past performance, which is not a guarantee of future results. (Current performance may be lower or higher than the performance data cited. For performance data current to the most recent month-end, visit our website at vanguard.com/performance.) Note, too, that both investment returns and principal value can fluctuate widely, so an investor’s shares, when sold, could be worth more or less than their original cost.
5
helped rehabilitate the sector. Overall, returns of investment-grade financial bonds were about double those of industrial and utility bonds.
 
6
about 1 to 3 percentage points. This is a significant advantage over a relatively long compounding period. Compared with their index benchmarks, which incur no costs, the returns of the short-term and intermediate-term funds came up a little short, while the long-term fund outperformed.
The High-Yield Corporate Fund’s tilt toward better-quality bonds weighed on its relative ten-year performance. The fund fell just shy of the average annual return of its peer group and trailed that of its benchmark.
In our view, the most sensible approach is to focus on what’s within your control. In short, we believe investors can give themselves a greater chance for success by acting on four key points: 
7
Successful investing doesn’t have to be complicated, but that’s not to say it’s easy. Investing can provoke strong emotions, and it’s hard to stay levelheaded during times of volatility. If you’re getting pulled off course, try to redirect your attention to the principles I’ve outlined. These principles—which reflect the core philosophy Vanguard has held for decades—can be the answer to uncertainty.
As always, thank you for investing with Vanguard.
Sincerely,


