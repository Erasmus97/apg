Dear Shareholders,
U.S. stocks flat, foreign markets strong in finale
After a strong third quarter, U.S. stock market averages treaded water as came to a close. However, they ended the year up strongly, as first and third quarter gains more than offset second and fourth quarter weakness. Typically a strong quarter for domestic small- and mid-cap issues, the fourth quarter of indeed proved to be another year-end positive for small-cap stocks. For the full calendar year the S&amp;P Index rose
Stock markets outside the United States generated some of the best returns for the fourth quarter, as optimism rebounded, thanks to the September actions of the European Central Bank in support of the euro and an improving outlook from China. Both developed and emerging foreign markets topped U.S. stocks by a solid margin.
Corporate and emerging markets led fixed income
Fixed-income investors took their cue from the equity markets and continued to favor the highest risk sectors through the end of Global fixed-income returns posted mixed results in the final quarter of the year. Gains were the highest for corporate high-yield and emerging market bonds. Although investors remained cautious ahead of the year-end budget negotiations, better economic data and a further improvement in the European sovereign debt crisis supported riskier assets and depressed government bond prices. In December, the Federal Reserve announced its intention to continue to purchase both Treasury and mortgage-backed securities and said that it would seek to keep short-term interest rates unchanged until the unemployment rate reaches or inflation turned noticeably higher.
Stay on track with Columbia Management
Backed by more than years of experience, Columbia Management is one of the nation's largest asset managers. At the heart of our success and, most importantly, that of our investors are highly talented industry professionals, brought together by a unique way of working. We are dedicated to helping you take advantage of today's opportunities and anticipate tomorrow's. We stay abreast of the latest investment trends and ideas, using our collective insight to evaluate events and transform them into solutions you can use.
Visit columbiamanagement.com for:
&gt;The Columbia Management Perspectives blog, featuring timely posts by our investment teams
&gt;Detailed up-to-date fund performance and portfolio information
&gt;Economic analysis and market commentary
&gt;Quarterly fund commentaries
&gt;Columbia Management Investor, our award-winning quarterly newsletter for shareholders
Thank you for your continued support of the Columbia Funds. We look forward to serving your investment needs for many years to come.
Best Regards,

Dear Shareholders,
U.S. stocks flat, foreign markets strong in finale
After a strong third quarter, U.S. stock market averages treaded water as came to a close. However, they ended the year up strongly, as first and third quarter gains more than offset second and fourth quarter weakness. Typically a strong quarter for domestic small- and mid-cap issues, the fourth quarter of indeed proved to be another year-end positive for small-cap stocks. For the full calendar year the S&amp;P Index rose
Stock markets outside the United States generated some of the best returns for the fourth quarter, as optimism rebounded, thanks to the September actions of the European Central Bank in support of the euro and an improving outlook from China. Both developed and emerging foreign markets topped U.S. stocks by a solid margin.
Corporate and emerging markets led fixed income
Fixed-income investors took their cue from the equity markets and continued to favor the highest risk sectors through the end of Global fixed-income returns posted mixed results in the final quarter of the year. Gains were the highest for corporate high-yield and emerging market bonds. Although investors remained cautious ahead of the year-end budget negotiations, better economic data and a further improvement in the European sovereign debt crisis supported riskier assets and depressed government bond prices. In December, the Federal Reserve announced its intention to continue to purchase both Treasury and mortgage-backed securities and said that it would seek to keep short-term interest rates unchanged until the unemployment rate reaches or inflation turned noticeably higher.
Stay on track with Columbia Management
Backed by more than years of experience, Columbia Management is one of the nation's largest asset managers. At the heart of our success and, most importantly, that of our investors are highly talented industry professionals, brought together by a unique way of working. We are dedicated to helping you take advantage of today's opportunities and anticipate tomorrow's. We stay abreast of the latest investment trends and ideas, using our collective insight to evaluate events and transform them into solutions you can use.
Visit columbiamanagement.com for:
&gt;The Columbia Management Perspectives blog, featuring timely posts by our investment teams
&gt;Detailed up-to-date fund performance and portfolio information
&gt;Economic analysis and market commentary
&gt;Quarterly fund commentaries
&gt;Columbia Management Investor, our award-winning quarterly newsletter for shareholders
Thank you for your continued support of the Columbia Funds. We look forward to serving your investment needs for many years to come.
Best Regards,

Dear Shareholders,
U.S. stocks flat, foreign markets strong in finale
After a strong third quarter, U.S. stock market averages treaded water as the came to a close. However, they ended the year up strongly, as first and third quarter gains more than offset second and fourth quarter weakness. Typically a strong quarter for domestic small- and mid-cap issues, the fourth quarter of indeed proved to be another year-end positive for small-cap stocks. For the full calendar year the S&amp;P Index rose
Stock markets outside the United States generated some of the best returns for the fourth quarter, as optimism rebounded, thanks to the September actions of the European Central Bank in support of the euro and an improving outlook from China. Both developed and emerging foreign markets topped U.S. stocks by a solid margin.
Corporate and emerging markets led fixed income
Fixed-income investors took their cue from the equity markets and continued to favor the highest risk sectors through the end of the Global fixed-income returns posted mixed results in the final quarter of the year. Gains were the highest for corporate high-yield and emerging market bonds. Although investors remained cautious ahead of the year-end budget negotiations, better economic data and a further improvement in the European sovereign debt crisis supported riskier assets and depressed government bond prices. In December, the Federal Reserve announced its intention to continue to purchase both Treasury and mortgage-backed securities and said that it would seek to keep short-term interest rates unchanged until the unemployment rate reaches or inflation turned noticeably higher.
Stay on track with Columbia Management
Backed by more than years of experience, Columbia Management is one of the nation's largest asset managers. At the heart of our success and, most importantly, that of our investors are highly talented industry professionals, brought together by a unique way of working. We are dedicated to helping you take advantage of today's opportunities and anticipate tomorrow's. We stay abreast of the latest investment trends and ideas, using our collective insight to evaluate events and transform them into solutions you can use.
Visit columbiamanagement.com for:
&gt;The Columbia Management Perspectives blog, featuring timely posts by our investment teams
&gt;Detailed up-to-date fund performance and portfolio information
&gt;Economic analysis and market commentary
&gt;Quarterly fund commentaries
&gt;Columbia Management Investor, our award-winning quarterly newsletter for shareholders
Thank you for your continued support of the Columbia Funds. We look forward to serving your investment needs for many years to come.
Best Regards,

Dear Shareholders,
U.S. stocks flat, foreign markets strong in finale
After a strong third quarter, U.S. stock market averages treaded water as came to a close. However, they ended the year up strongly, as first and third quarter gains more than offset second and fourth quarter weakness. Typically a strong quarter for domestic small- and mid-cap issues, the fourth quarter of indeed proved to be another year-end positive for small-cap stocks. For the full calendar year the S&amp;P Index rose
Stock markets outside the United States generated some of the best returns for the fourth quarter, as optimism rebounded, thanks to the September actions of the European Central Bank in support of the euro and an improving outlook from China. Both developed and emerging foreign markets topped U.S. stocks by a solid margin.
Corporate and emerging markets led fixed income
Fixed-income investors took their cue from the equity markets and continued to favor the highest risk sectors through the end of Global fixed-income returns posted mixed results in the final quarter of the year. Gains were the highest for corporate high-yield and emerging market bonds. Although investors remained cautious ahead of the year-end budget negotiations, better economic data and a further improvement in the European sovereign debt crisis supported riskier assets and depressed government bond prices. In December, the Federal Reserve announced its intention to continue to purchase both Treasury and mortgage-backed securities and said that it would seek to keep short-term interest rates unchanged until the unemployment rate reaches or inflation turned noticeably higher.
Stay on track with Columbia Management
Backed by more than years of experience, Columbia Management is one of the nation's largest asset managers. At the heart of our success and, most importantly, that of our investors are highly talented industry professionals, brought together by a unique way of working. We are dedicated to helping you take advantage of today's opportunities and anticipate tomorrow's. We stay abreast of the latest investment trends and ideas, using our collective insight to evaluate events and transform them into solutions you can use.
Visit columbiamanagement.com for:
&gt;The Columbia Management Perspectives blog, featuring timely posts by our investment teams
&gt;Detailed up-to-date fund performance and portfolio information
&gt;Economic analysis and market commentary
&gt;Quarterly fund commentaries
&gt;Columbia Management Investor, our award-winning quarterly newsletter for shareholders
Thank you for your continued support of the Columbia Funds. We look forward to serving your investment needs for many years to come.
Best Regards,
