DEAR SHAREHOLDER





ECONOMIC AND MARKET OVERVIEW





A BRIEF NOTE ON THE COMPOUNDING OF RETURNS





ABOUT SHAREHOLDERS&rsquo; FUND EXPENSES





S&amp;P
STRATEGY FUND





INVERSE S&amp;P
STRATEGY FUND






STRATEGY FUND





INVERSE
STRATEGY FUND





DOW
STRATEGY FUND





INVERSE DOW
STRATEGY FUND





RUSSELL
STRATEGY FUND





INVERSE RUSSELL
STRATEGY FUND





NOTES TO FINANCIAL STATEMENTS





REPORT OF INDEPENDENT REGISTERED PUBLIC ACCOUNTING FIRM





OTHER INFORMATION





INFORMATION ON BOARD OF TRUSTEES AND OFFICERS





GUGGENHEIM INVESTMENTS PRIVACY POLICIES






THE RYDEX FUNDS ANNUAL REPORT |













March





Dear Shareholder:



Security Investors, LLC (the &ldquo;Investment Adviser&rdquo;)
is pleased to present the annual shareholder report for eight of our Rydex Funds.



The Investment Adviser is part of Guggenheim Investments,
which represents the investment management businesses of Guggenheim Partners, LLC (&ldquo;Guggenheim&rdquo;), a global,
diversified financial services firm.



Guggenheim Distributors, LLC, is the distributor of the Funds.
Guggenheim Distributors, LLC is affiliated with Guggenheim Partners and Security Investors, LLC.



This report covers performance of the following Funds for the
three-month period ended March



DOMESTIC EQUITY FUNDS



&ndash; S&amp;P
Strategy Fund



&ndash; Inverse S&amp;P
Strategy Fund



&ndash;
Strategy Fund



&ndash; Inverse
Strategy Fund



&ndash; Dow Strategy Fund



&ndash; Inverse Dow Strategy
Fund



&ndash; Russell
Strategy Fund



&ndash; Inverse Russell
Strategy Fund



We encourage you to read the Economic and Market Overview section
of the report, which follows this letter, and then the Performance Report for each Fund.



We are committed to providing innovative investment solutions
and appreciate the trust you place in us.



Sincerely,
