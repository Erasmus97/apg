Dear Shareholders, 
U.S. equities had a strong first quarter 
Similar to 2012, equities
once again were the best performing asset class in the first quarter of 2013. The S&amp;P 500 Index reached an all-time closing high on the last trading day of the quarter and pushed through its October 2007 peak. Although global equities have
performed well year-to-date, there is significant performance divergence among regions. In local currency terms, Japanese equities were the best performing developed market globally, and U.S. stocks outperformed most other global equity markets.
European equities rose in the first quarter but trailed U.S. stocks and had a turbulent March, as investors were reminded of instability in the eurozone with news of a banking crisis in Cyprus. 
Although all 10 sectors of the S&amp;P 500 Index delivered positive returns, this was a rally led by defensive stocks such as those in
health care, consumer staples and utilities. Materials and technology stocks were the weakest sectors. 
High yield
leads fixed income; most sectors flat to down 
The fixed-income markets lagged equities with modest, single-digit returns
coming from municipals and high-yield sectors in the United States. Most government and investment-grade credit sectors were 
roughly flat to
down for the first quarter of 2013. Emerging market bonds were the biggest disappointment with single-digit losses. 
Columbia
Management to begin delivering summary prospectuses 
Each Columbia fund is required to update its prospectus on an annual basis. Beginning
with June 2013 prospectus updates, shareholders of Columbia retail mutual funds will start to receive a summary prospectus, rather than the full length (statutory) mutual fund prospectus they have received in the past. 
Each fund’s summary prospectus will include the following key information: 
 
Investment objective 
Fee and expense table 
Portfolio turnover rate information 
Principal investment strategies, principal risks and performance information 
Management information 
Purchase and sale information 
Tax information 
Financial intermediary compensation information 
Each fund’s statutory prospectus will contain additional information about the fund and its risks. Both the statutory and summary prospectus will be updated each year, and will be available at
columbiamanagement.com. Shareholders may request a printed version of a statutory prospectus at no cost by calling 800.345.6611 or sending an email to serviceinquiries@columbiamanagement.com. 
Stay on track with Columbia Management 
Backed by more than 100 years of experience,
Columbia Management is one of the nation’s largest asset managers. At the heart of our success — and, most importantly, that of our investors — are highly talented industry professionals, brought together by a unique way of working.
We are dedicated to helping you take advantage of today’s opportunities and anticipate tomorrow’s. We stay abreast of the latest investment trends and ideas, using our collective insight to evaluate events and transform them into solutions
you can use. 
Visit columbiamanagement.com for: 
 
The Columbia Management Perspectives blog, featuring timely posts by our investment teams 
Detailed up-to-date fund performance and portfolio information 
Economic analysis and market commentary 
Quarterly fund commentaries 
Thank you for your continued support of the Columbia Funds. We look forward to serving your investment needs for many years to come. 
Best Regards, 


