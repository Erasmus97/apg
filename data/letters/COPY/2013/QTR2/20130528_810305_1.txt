Dear Shareholder:
This annual report for Dreyfus Short-Intermediate Municipal Bond Fund covers the 12-month period from April 1, 2012, through March 31, 2013. For information about how the fund performed during the reporting period, as well as general market perspectives, we provide a Discussion of Fund Performance on the pages that follow.
The search for higher current yields amid historically low interest rates continued to be a major force in the solid performance of the municipal bond market over the reporting period. In addition, municipal bonds benefited from favorable supply-and-demand dynamics. Robust investor demand was met with a relatively meager supply of newly issued securities stemming from political pressure to reduce government spending and borrowing. The market also was buoyed by improvements in the fiscal condition of most states and many municipalities as tax revenues increased in a gradually recovering U.S. economy.
However, the pace of economic growth has remained sluggish compared to historical norms, helping to prevent new imbalances from developing even as monetary policymakers throughout the world maintain aggressively accommodative postures. Therefore, in our analysis, the economic expansion is likely to continue over the foreseeable future. As always, we encourage you to discuss our observations with your financial adviser, who can help you respond to the challenges and opportunities the financial markets provide.
Thank you for your continued confidence and support.
Sincerely,


