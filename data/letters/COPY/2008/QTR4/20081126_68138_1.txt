Dear Shareholder,
 
Vanguard Morgan Growth Fund returned –23.7% for Investor Shares and –23.6% for Admiral Shares in the 12 months ended September 30, 2008. This disappointing result reflects a tumultuous time in the U.S. and foreign financial markets that drove most stock funds to register double-digit losses. 
 
The Morgan Growth Fund declined a bit more than its index benchmark, but slightly less than the average return of peer funds.
 
If you own the fund in a taxable account, you may wish to review our report on the fund’s after-tax returns on page 30.
 
Credit-market turbulence weighed heavily on stock prices
Troubles simmering in the credit markets for much of the past year came to a boil at the end of the fiscal period, producing several high-profile bankruptcies and putting severe pressure on stock prices around the world. The broad U.S. stock market returned –21.2% for the 12 months ended September 30. In September alone, stock prices fell more than 9%. International stock markets were similarly disappointing, returning –30.0% for the full 12 months.
 
Policymakers and elected officials, both in the United States and abroad, responded to the upheavals with dramatic new programs designed to help stabilize the
 
2


credit markets. As participants struggled to make sense of the markets’ fast-changing dynamics, stock prices were exceptionally volatile, with daily ups and downs of 2 percentage points or more becoming commonplace.
 
U.S. Treasuries rallied in a nervous market Nervousness in the stock market was echoed, and even amplified, in the bond market. For the 12 months, the broad U.S. bond market returned 3.7%, largely on the strength of Treasuries—investors’ security of choice in times of duress. Corporate bonds generally produced negative returns for the period, coming under heavy selling pressure during investors’ flight to safety. Even the municipal market, made up of generally high-quality securities issued by states and municipalities, recorded a negative 12-month return.
 
The U.S. Federal Reserve Board responded to the turmoil with a dramatic easing of monetary policy. Over the full 12 months, the Fed reduced its target for the federal funds rate from 4.75% to 2.00%. On October 8, shortly after the close of the fiscal period, the Fed cut rates again, to 1.50%. The move was made in coordination with rate cuts by several other central banks.
 
 
Market Barometer
 
 
 
Average Annual Total Returns
Periods Ended September 30, 2008
 
One Year
Three Years
Five Years
Stocks
 
 
 
Russell 1000 Index (Large-caps)
–22.1%
0.1%
5.5%
Russell 2000 Index (Small-caps)
–14.5
1.8
8.1
Dow Jones Wilshire 5000 Index (Entire market)
–21.2
0.6
6.0
MSCI All Country World Index ex USA (International)
–30.0
3.1
11.8
 
 
 
 
Bonds
 
 
 
Lehman U.S. Aggregate Bond Index (Broad taxable market)
3.7%
4.2%
3.8%
Lehman Municipal Bond Index
–1.9
1.9
2.8
Citigroup 3-Month Treasury Bill Index
2.6
4.0
3.1
 
 
 
 
CPI
 
 
 
Consumer Price Index
4.9%
3.2%
3.4%
 
 
3
 


The fiscal year was marked by rising uncertainty
Over the Morgan Growth Fund’s fiscal year, the market environment grew increasingly difficult for stocks as investors worried about the economic outlook and the growing distress in the financial markets. 
These challenges were reflected in Morgan Growth’s –23.7% return (Investor Shares)—its first decline in six years—which was moderately below the –20.6% return of growth stocks in general, as measured by the Russell 3000 Growth Index. (By comparison, the Russell 3000 Value Index posted a –22.7% return.) 
 
The fund did perform a bit better than the average result for its peer group (–24.1%). In relative terms, however, the advisors’ stock selection in the industrials and health care sector fell short. Among the poorest performers were Boeing (–44%), which faced aircraft-construction delays; Fluor (–22%), an energy-industry construction and engineering firm that was affected by the recent drop in oil prices as well as by the credit crisis; and Elan (–49%), an Irish biotech company, which discovered that two drugs it was developing appeared to have serious side effects.
 
 
 
 
 
Your Fund Compared With Its Peer Group
 
 
 
 
 
 
Average
 
Investor
Admiral
Multi-Cap
 
Shares
Shares
Growth Fund
Morgan Growth Fund
0.37%
0.21%
1.43%
 
 
 
1
The fund expense ratios shown are from the prospectus dated January 25, 2008. For the fiscal year ended September 30, 2008, the fund’s expense ratios were 0.38% for Investor Shares and 0.21% for Admiral Shares. The peer-group expense ratio is derived from data provided by Lipper Inc. and captures information through year-end 2007.
 
 
 
4


Stock selection was also disappointing in the information technology sector. Many firms in this sector—the fund’s largest industry group, approaching one-third of assets—faced tighter customer budgets as corporations reacted to the economic slowdown.
Compared with the benchmark index, the fund had a relatively modest exposure to consumer staples, and one of the market’s better-performing sectors. During economic downturns, investors typically consider consumer staples a “defensive” sector because people generally are reluctant to curtail spending on basic foods and household items.
 
The Morgan Growth Fund benefited from generally good stock selection in the materials sector, especially from its holdings of fertilizer suppliers enjoying strong worldwide demand from farmers. The fund’s relatively small holdings in the troubled financial sector had little impact on overall returns.
Don’t lose sight of the long term during short-term ups and downs
As Vanguard has long advised investors, a sensible approach to evaluating an investment is to assess its longer-term performance. That can help you put short-term swings (especially downward swings) in perspective—something particularly valuable in trying times like these. 
 
 
Total Returns
 
Ten Years Ended September 30, 2008
 
 
Average
 
Annual Return
Morgan Growth Fund Investor Shares
4.2%
Russell 3000 Growth Index
0.9
3.5
The figures shown represent past performance, which is not a guarantee of future results. (Current performance may be lower or higher than the performance data cited. For performance data current to the most recent month-end, visit our website at www.vanguard.com/performance.) Note, too, that both investment returns and principal value can fluctuate widely, so an investor’s shares, when sold, could be worth more or less than their original cost.
 
1 Derived from data provided by Lipper Inc.
 
 
5


However, an investment’s long-term record can be skewed, for better or worse, by the most recent short-term results. The Morgan Growth Fund provides a good example. When we reported to you 12 months ago, the fund had provided an average annual return of 6.7% for its Investor Shares over the ten years ended September 30, 2007. Now, following the difficult 2008 fiscal year, the ten-year picture looks very different: For the decade that ended this past September 30, the fund’s average annual return was 4.2%. 
A look at the performance of the best available benchmarks during the same period can provide additional context and perspective. As you can see in the table on page 5, over those ten years the Morgan Growth Fund outpaced the performance of its benchmark index as well as the average return of its competitors.
Some things about investing never go out of style
Over the past year—and more acutely in recent weeks—global financial markets have experienced an unnerving confluence of events. History teaches us that selling in a panic, or letting your emotions drive your investment decisions, is often a recipe for disappointment.
 
Instead, it’s important to focus on the time-tested principles of balance and diversification, both within and across asset classes. Vanguard Morgan Growth Fund can play a useful supporting role within the stock portion of a diversified portfolio built with those ideas in mind. 
Of course, even balanced portfolios of stocks and bonds have faced tough times during the past 12 months. But everything that history has taught us about the markets suggests that these principles can put you in the best position to achieve long-term investment success.
Thank you for your confidence in Vanguard.
Sincerely,


