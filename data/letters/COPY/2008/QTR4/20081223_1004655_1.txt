Dear Shareholder,
For the fiscal year ended October 31, 2008, Vanguard High Dividend Yield Index Fund returned –32.17% for Investor Shares and –32.07% for ETF Shares based on net asset value. The fund closely tracked the performance of the benchmark FTSE High Dividend Yield Index, and was a few paces ahead of the average return of the peer group.
The fund’s above-average dividend yield provided some cushion for returns, but this was small comfort as stocks were buffeted by winds from all directions. The stock market contended with bank failures, frozen credit markets, and several months of record-high oil and commodity prices that pinched household budgets.
On October 31, the fund’s 30-day SEC yield for Investor Shares stood at 4.04%—more than a full percentage point above the 2.81% average dividend yield of the large-cap Standard & Poor’s 500 Index. Reflecting the steep decline in stock prices, the fund’s yield was up almost 1.40 percentage points from a year earlier.
If you own shares of the fund in a taxable account, see page 25 for a report on after-tax returns for the 12 months, based on the highest tax bracket.
Stock prices fell sharply in a global upheaval
Global stock markets started the 12-month period near all-time highs, but then declined sharply, laid low by the financial crisis that originated in the fixed income markets. The descent traced a series of jagged ups and 
 
2


downs. During the week ended October 10, for example, the U.S. stock market returned about –18%. When Wall Street opened the following Monday, stocks surged, returning more than 10% over the next six and a half hours.
For the full 12 months, the broad U.S. stock market returned –36.43%; international stocks returned –48.27%. The pain was especially acute in emerging markets—among the strongest performers in recent years—as investors became increasingly risk-averse.
Bond market averages masked disparate returns
The broad U.S. taxable bond market registered an unremarkable return of 0.30% for the 12 months, but by its own typically sedate standards, the dislocations were extreme. The strong performance of U.S. Treasury and government securities was offset by double-digit declines in the corporate bond market. These dynamics led to unusually large differences between the yields of Treasuries and their corresponding private sector securities—both a reflection and a cause of the credit market’s distress. Despite their generally high creditworthiness, municipal bonds also fell in price, with the broad tax-exempt market registering a 12-month return of –3.30%.
The U.S. Federal Reserve Board responded to the turmoil with new lending programs and a dramatic easing of monetary policy. Over the full 12 months, the Fed reduced its target for the federal funds rate from 4.50% to 1.00%.
 
 
Market Barometer
 
 
Average Annual Total Returns
 
Periods Ended October 31, 2008
 
One Year
Three Years
Five Years
Stocks
 
 
 
Russell 1000 Index (Large-caps)
–36.80%
–5.51%
0.37%
Russell 2000 Index (Small-caps)
–34.16
–4.79
1.57
Dow Jones Wilshire 5000 Index (Entire market)
–36.43
–5.10
0.81
MSCI All Country World Index ex USA (International)
–48.27
–3.93
5.05
 
 
 
 
Bonds
 
 
 
Lehman U.S. Aggregate Bond Index (Broad taxable market)
0.30%
3.60%
3.48%
Lehman Municipal Bond Index
–3.30
1.71
2.73
Citigroup 3-Month Treasury Bill Index
2.31
3.93
3.10
 
 
 
 
CPI
 
 
 
Consumer Price Index
3.66%
2.83%
3.20%
 
 
3


All market sectors succumbed to investors’ anxiety
Your fund’s portfolio seeks to closely replicate the FTSE High Dividend Yield Index. The sector weightings within the portfolio typically differ from those of the broad market because of the fund’s focus on investing in stocks that pay high dividends and may have the potential for long-term capital appreciation. Although no sector escaped harsh downdrafts over the past year, your fund had significant exposure to some of those hardest-hit, notably financial and industrial stocks.
Financial stocks—which boasted some of the highest dividend yields—represented the largest slice of the fund, almost one-quarter of the portfolio during the fiscal year. This sector returned –44%, accounting for about one-third of the fund’s decline, as beleaguered commercial and investment banks, brokerages, and other financial firms grappled with unprecedented credit market turmoil that spanned the globe.
The industrial sector, which posted a –42% return, was also one of the fund’s largest positions. These companies—such as conglomerates, construction and farm machinery manufacturers, and aerospace and defense contractors—suffered from investors’ concern about slower global economic growth. Industrial holdings shaved 6 percentage points from the fund’s return.
Consumer staples companies, another large position, held up best as the economy slumped. This somewhat
 
 
Total Returns
 
 
 
Average
 
Annual Return
High Dividend Yield Index Fund Investor Shares
–13.84%
FTSE High Dividend Yield Index
–13.59
–17.03
 
The figures shown represent past performance, which is not a guarantee of future results. (Current performance may be lower or higher than the performance data cited. For performance data current to the most recent month-end, visit our website at www.vanguard.com/performance.) Note, too, that both investment returns and principal value can fluctuate widely, so an investor’s shares, when sold, could be worth more or less than their original cost. 
 
1 Share-class inception.
2 Derived from data provided by Lipper Inc..
 
4


recession-resistant sector returned –11%, with some holdings—notably packaged food companies, a beer brewer, and a tobacco company—posting gains
 
Whether markets are rising or falling, the hallmark of a successful index fund is to deliver market-tracking—not market-beating—returns. Your fund has achieved this goal since its inception almost two years ago. This is a tribute to the risk-control and portfolio management skills, as well as the efficient trading systems, of Vanguard Quantitative Equity Group, the fund’s advisor. Also important is the fund’s low expense ratio, which allows it to more closely track the cost-free index.
 
When risks seem to outrun rewards, don’t lose sight of the long term
Seasoned investors know that the opportunity associated with stocks—which historically have outperformed bonds over the long term—comes with a string attached: higher risk, or volatility. Over the past year, we have all been starkly reminded of that risk.
When the markets are churning, the temptation to make emotional decisions can be strong—and counterproductive. Our experience indicates that diversification within and across asset classes—including stocks, bonds, and cash—can help investors to resist that temptation.
 
 
 
 
 
Your Fund Compared With Its Peer Group
 
 
 
 
 
 
Average
 
Investor
ETF
Large-Cap
 
Shares
Shares
Value Fund
High Dividend Yield Index Fund
0.40%
0.25%
1.28%
 
 
 
1 The fund expense ratios shown are from the prospectuses dated February 22, 2008. For the fiscal year ended October 31, 2008, the fund’s expense ratios were 0.35% for Investor Shares and 0.20% for ETF Shares. The peer-group expense ratio is derived from data provided by Lipper Inc. and captures information through year-end 2007.
 
 
5


Of course, even balanced portfolios have faced tough times during the past 12 months. Still, history suggests that following the time-tested principles of diversification and balance can put you in a good position to benefit from the market’s long-term opportunities. By providing the potential to track segments of the market with above-average dividend yields, Vanguard High Dividend Yield Index Fund can play a useful role in a portfolio built on these principles.
 
Thank you for entrusting your assets to Vanguard.
 
Sincerely,


Dear Shareholder,
During the fiscal year ended October 31, 2008, the crisis that began in the U.S. financial markets pummeled international stocks, reversing gains they have enjoyed in recent years. For the year, Vanguard International Explorer Fund returned –53.80%, slightly trailing its benchmark, the Standard & Poor’s Europe and Pacific SmallCap Index. The fund lost a bit less than rival small-capitalization international funds did. 
In a challenging market in which every region suffered, the fund advisor’s investment decisions in Asian and Pacific emerging markets and in developed countries in Europe hurt the fund’s performance relative to its benchmark. Across borders, the fund’s industrial and consumer staples stocks weighed heavily on its results. The fund’s utilities and health care holdings were among its better performers. On October 31, Vanguard International Explorer Fund, which had been closed in August 2004, reopened to new investors.
Stock prices fell sharply in global upheaval
Global stock markets started the 12-month period near all-time highs, but then declined sharply, laid low by the financial crisis that originated in the fixed income markets. The descent traced a series of jagged ups and downs. During the week ended October 10, for example, the U.S. stock market returned about –18%. When
 
2


Wall Street opened the following Monday, stocks surged, returning more than 10% over the next six and a half hours.
For the full 12 months, the broad U.S. stock market returned –36.43%; international stocks returned –48.27%. The pain was especially acute in emerging markets—among the strongest performers in recent years—as investors became increasingly risk-averse.
Bond market averages masked disparate returns
The broad U.S. taxable bond market registered an unremarkable return of 0.30% for the 12 months, but by its own typically sedate standards, the dislocations were extreme. The strong performance of U.S. Treasury and government securities was offset by double-digit declines in the corporate bond market. These dynamics led to unusually large differences between the yields of Treasuries and their corresponding private sector securities—both a reflection and a cause of the credit market’s distress. Despite their generally high creditworthiness, municipal bonds also fell in price, with the broad tax-exempt market registering a 12-month return of –3.30%. 
The U.S. Federal Reserve Board responded to the turmoil with new lending programs and a dramatic easing of monetary policy. Over the full 12 months, the Fed reduced its target for the federal funds rate from 4.50% to 1.00%.
 
 
Market Barometer
 
 
Average Annual Total Returns
 
Periods Ended October 31, 2008
 
One Year
Three Years
Five Years
Stocks
 
 
 
MSCI All Country World Index ex USA (International)
–48.27%
–3.93%
5.05%
Russell 1000 Index (Large-caps)
–36.80
–5.51
0.37
Russell 2000 Index (Small-caps)
–34.16
–4.79
1.57
Dow Jones Wilshire 5000 Index (Entire market)
–36.43
–5.10
0.81
 
 
 
 
Bonds
 
 
 
Lehman U.S. Aggregate Bond Index (Broad taxable market)
0.30%
3.60%
3.48%
Lehman Municipal Bond Index
–3.30
1.71
2.73
Citigroup 3-Month Treasury Bill Index
2.31
3.93
3.10
 
 
 
 
CPI
 
 
 
Consumer Price Index
3.66%
2.83%
3.20%
 
 
3


Small-cap funds struggled as credit crunch spread
Vanguard International Explorer Fund’s advisor, Schroder Investment Management North America Inc., seeks the stocks of foreign small-cap companies with attractive valuations and the potential for rapid earnings growth. 
During the past fiscal year, the impact of the U.S. credit crisis on global markets has been especially challenging for smaller companies, which tend to be in less defensive sectors, are more sensitive to economic downturns, and may be perceived by investors as more risky. 
The fund’s investment in emerging markets in the Asia/Pacific region dragged down the fund’s performance relative to the S&P EPAC SmallCap Index, as weak returns in Chinese and South Korean companies weighed heavily on performance. (The benchmark was previously called the Standard & Poor’s/ Citigroup Extended Market Europe & Pacific Index.)
Although emerging market stocks continue to have strong long-term growth prospects, their performance during the fiscal year was disappointing. This weakness reflects fundamental problems such as the decline in commodity prices, which has hurt many commodity-rich markets, and investors’ diminished appetite for risk. 
 
 
 
 
Your Fund Compared With Its Peer Group
 
 
 
 
Average
 
 
International
 
Fund
Small-Cap Fund
International Explorer Fund
0.35%
1.65%
 
 
 
1 The fund expense ratio shown is from the prospectus dated February 22, 2008. For the fiscal year ended October 31, 2008, the fund’s expense ratio was 0.36%. The peer-group expense ratio is derived from data provided by Lipper Inc. and captures information through year-end 2007. 
 
4


Developed markets in Europe—which, with an average weighting of 64%, represented the fund’s largest holding by region—also were hit hard. The United Kingdom accounted for much of the decline as the fund’s investments in the industrial, financial, and consumer-oriented sectors performed poorly.  
On a worldwide basis, the fund’s heavy commitment to the industrial sector—on average, its allocation to industrials was about 12 percentage points higher than that of its benchmark—was an important factor in its decline. Consumer staples, a much smaller sector by comparison, also caused the fund to fall short of its index.
The sectors in which the fund managed to stay a few steps ahead of its benchmark included utilities, a traditionally defensive sector that the advisor overweighted during the period, and health care. The advisor managed to identify some of the better-performing companies in pharmaceuticals and health-care equipment.
The fund has a strong record of outperforming its competitors 
For the past ten years, the International Explorer Fund has produced an average annual return more than 4 percentage points better than that of its primary benchmark, and more than 2 percentage points better than that of its peers.  
 
 
Total Returns
 
Ten Years Ended October 31, 2008
 
 
Average
 
Annual Return
International Explorer Fund
8.90%
S&P EPAC SmallCap Index
4.34
6.09
 
The figures shown represent past performance, which is not a guarantee of future results. (Curre nt performance may be lower or higher than the performance data cited. For performance data current to the most recent month-end, visit our website at www.vanguard.com/performance.) Note, too, that both investment returns and principal value can fluctuate widely, so an investor's shares, when sold, could be worth more or less than their original cost. 
 
 
1 Derived from data provided by Lipper Inc.
 
5


The fund’s strong long-term performance record can be illustrated in dollar terms. A hypothetical investment of $25,000 in the fund ten years ago would have grown to $58,660. The same investment compounded at the average return of international small-cap funds would have produced $13,492 less.


Dear Shareholder,
The fiscal year ended October 31, 2008, was a period in which “success” was clearly a relative concept. Vanguard Mid-Cap Growth Fund returned –40.02% for the 12 months. Amid gale-force headwinds, the fund held up a bit better than its benchmarks. 
It was the Mid-Cap Growth Fund’s first fiscal-year decline since 2002, occurring as the nation faced an unusually challenging series of events in the financial markets. 
If you own shares of the fund in a taxable account, you may wish to review our report on the fund’s after-tax returns on page 23.
Stock prices fell sharply in a global upheaval
Global stock markets started the 12-month period near all-time highs, but then declined sharply, laid low by the financial crisis that originated in the fixed income markets. The descent traced a series of jagged ups and downs. During the week ended October 10, for example, the U.S. stock market returned about –18%. When Wall Street opened the following Monday, stocks surged, returning more than 10% over the next six and a half hours.
 
2


For the full 12 months, the broad U.S. stock market returned –36.43%; international stocks returned –48.27%. The pain was especially acute in emerging markets—among the strongest performers in recent years—as investors became increasingly risk-averse.
Bond market averages masked disparate returns
The broad U.S. taxable bond market registered an unremarkable return of 0.30% for the 12 months, but by its own typically sedate standards, the dislocations were extreme. The strong performance of U.S. Treasury and government securities was offset by double-digit declines in the corporate bond market. These dynamics led to unusually large differences between the yields of Treasuries and their corresponding private sector securities—both a reflection and a cause of the credit market’s distress. Despite their generally high creditworthiness, municipal bonds also fell in price, with the broad tax-exempt market registering a 12-month return of –3.30%. 
The U.S. Federal Reserve Board responded to the turmoil with new lending programs and a dramatic easing of monetary policy. Over the full 12 months, the Fed reduced its target for the federal funds rate from 4.50% to 1.00%.
Technology and industrial stocks bore the brunt of the decline
Declines in all market sectors contributed to the Mid-Cap Growth Fund’s –40.02% return over the past 12 months. As
 
 
Market Barometer
 
 
Average Annual Total Returns
 
Periods Ended October 31, 2008
 
One Year
Three Years
Five Years
Stocks
 
 
 
Russell 1000 Index (Large-caps)
–36.80%
–5.51%
0.37%
Russell 2000 Index (Small-caps)
–34.16
–4.79
1.57
Dow Jones Wilshire 5000 Index (Entire market)
–36.43
–5.10
0.81
MSCI All Country World Index ex USA (International)
–48.27
–3.93
5.05
 
 
 
 
Bonds
 
 
 
Lehman U.S. Aggregate Bond Index (Broad taxable market)
0.30%
3.60%
3.48%
Lehman Municipal Bond Index
–3.30
1.71
2.73
Citigroup 3-Month Treasury Bill Index
2.31
3.93
3.10
 
 
 
 
CPI
 
 
 
Consumer Price Index
3.66%
2.83%
3.20%
 
 
3


disappointing as this performance was, the returns of the fund’s benchmark index and peer group suggest that it could have been worse. The fund lost a bit less than mid-cap growth stocks in general.
Holdings in the information technology and industrial sectors, which accounted for about half the fund’s assets, were responsible for about half its negative return. A handful of stocks, including shares of companies in the software and semiconductor industries, did relatively well and helped blunt the decline a bit.
The fund also benefited from strongly performing stocks in other sectors, such as energy, financials, and consumer discretionary, despite the overall declines of these groups. In the consumer discretionary category, for example, education services companies contributed positively to return. The fund missed out on some opportunities in the health care sector, however.
Don’t lose sight of the long term during short-term ups and downs
Even when world markets aren’t churning, the Mid-Cap Growth Fund’s aggressive strategy can make for sharp swings in year-to-year performance, as you can see in the Performance Summary section of this report. When we reported to you 12 months ago, the fund had just gained 26.39% in its 2007 fiscal year. That seems light years away by now.
 
 
 
 
Your Fund Compared With Its Peer Group
 
 
 
 
Average
 
 
Mid-Cap
 
Fund
Growth Fund
Mid-Cap Growth Fund
0.56%
1.50%
 
                                          
     
Total Returns
 
Ten Years Ended October 31, 2008
 
 
Average
 
Annual Return
Mid-Cap Growth Fund
6.65%
Russell Midcap Growth Index
2.19
3.39
 
The figures shown represent past performance, which is not a guarantee of future results. (Current performance may be lower or higher than the performance data cited. For performance data current to the most recent month-end, visit our website at www.vanguard.com/performance.) Note, too, that both investment returns and principal value can fluctuate widely, so an investor’s shares, when sold, could be worth more or less than their original cost.
 
1 The fund expense ratio shown is from the prospectus dated February 22, 2008. For the fiscal year ended October 31, 2008, the fund’s expense ratio was 0.55%. The peer-group expense ratio is derived from data provided by Lipper Inc. and captures information through year-end 2007. 
2 Derived from data provided by Lipper Inc.
 
4


As Vanguard has long advised, when evaluating any investment it’s sensible to look at longer-term performance. That can help you put short-term swings (especially downward swings) in perspective—something particularly valuable in trying times like these. 
However, an investment’s long-term record can be skewed, for better or worse, by the most recent results. A year ago, for example, the Mid-Cap Growth Fund reported an average annual return of 13.05% since its inception on December 31, 1997. At the end of fiscal year 2008, however, the fund’s ten-year average annual return stood at 6.65%, a reflection of the past year’s struggle.
Of course, your fund’s recent reversal is a clear disappointment. But it’s important to recognize that the fund searches for opportunities in a volatile section of the stock market, and this can produce some dramatic ups and downs. 
Some things about investing never go out of style
Over the past year, global financial markets have experienced an unnerving confluence of events. As experienced investors know, selling in a panic, or letting your emotions drive your investment decisions, is often a recipe for disappointment.
 
Instead, it’s important to focus on the time-tested principles of balance and diversification, both within and across asset classes. That is why we always encourage you to determine the mix of stock, bond, and money market funds that is consistent with your goals, time horizon, and tolerance for the markets’ inevitable ups and downs—and then try to stick with it. The Mid-Cap Growth Fund can be an important element of such a diversified portfolio.
Of course, even balanced portfolios of stocks and bonds have faced tough times during the past 12 months. But history suggests that these principles can put you in a good position to achieve long-term investment success.
Thank you for your confidence in Vanguard.
Sincerely,


Dear Shareholder,
For the 12 months ended October 31, 2008—a period in which “success” was clearly a relative concept—Vanguard Selected Value Fund returned –37.79%. Though this is obviously a disappointing result, the fund’s comparative standards posted even weaker 12-month performances. 
Selected Value’s first fiscal-year loss since 2002 came about as the nation faced an unusually challenging series of events in the financial markets. In these gale-force headwinds, the fund held up better than its benchmarks. 
If you own shares of the Selected Value Fund in a taxable account, you may wish to review our report of the fund’s after-tax returns on page 23.
Stock prices fell sharply in global upheaval
Global stock markets started the 12-month period near all-time highs, but then declined sharply, laid low by the financial crisis that originated in the fixed income markets. The descent traced a series of jagged ups and downs. During the week ended October 10, for example, the U.S. stock market returned about –18%. When Wall Street opened the following Monday, stocks surged, returning more than 10% over the next six and a half hours.
For the full 12 months, the broad U.S. stock market returned –36.43%; international stocks returned –48.27%. The 
 
2


pain was especially acute in emerging markets—among the strongest performers in recent years—as investors became increasingly risk-averse.
Bond market averages masked disparate returns
The broad U.S. taxable bond market registered an unremarkable return of 0.30% for the 12 months, but by its own typically sedate standards, the dislocations were extreme. The strong performance of U.S. Treasury and government securities was offset by double-digit declines in the corporate bond market. These dynamics led to unusually large differences between the yields of Treasuries and their corresponding private sector securities—both a reflection and a cause of the credit market’s distress. Despite their generally high creditworthiness, municipal bonds also fell in price, with the broad tax-exempt market registering a 12-month return of –3.30%. 
The U.S. Federal Reserve Board responded to the turmoil with new lending programs and a dramatic easing of monetary policy. Over the full 12 months, the Fed reduced its target for the federal funds rate from 4.50% to 1.00%.
Financials held up a bit better despite the overall market slide
The Selected Value Fund returned –37.79% for the 12 months in the face of an unusually tough market, and all sectors suffered declines. As disappointing as this performance was, the return of the benchmark index and the average return 
 
 
Market Barometer
 
 
Average Annual Total Returns
 
Periods Ended October 31, 2008
 
One Year
Three Years
Five Years
Stocks
 
 
 
Russell 1000 Index (Large-caps)
–36.80%
–5.51%
0.37%
Russell 2000 Index (Small-caps)
–34.16
–4.79
1.57
Dow Jones Wilshire 5000 Index (Entire market)
–36.43
–5.10
0.81
MSCI All Country World Index ex USA (International)
–48.27
–3.93
5.05
 
 
 
 
Bonds
 
 
 
Lehman U.S. Aggregate Bond Index (Broad taxable market)
0.30%
3.60%
3.48%
Lehman Municipal Bond Index
–3.30
1.71
2.73
Citigroup 3-Month Treasury Bill Index
2.31
3.93
3.10
 
 
 
 
CPI
 
 
 
Consumer Price Index
3.66%
2.83%
3.20%
 
 
3


of the fund’s peer group suggest that it could have been worse. The fund’s holdings lost a bit less than mid-cap value stocks in general. 
The financial and consumer discretionary sectors accounted for somewhat more than a third of Selected Value’s assets and, coincidentally, accounted for somewhat more than a third of the fund’s negative return. Despite the drop in the financial sector’s return, the fund’s holdings declined about 3 percentage points less than that same sector in the benchmark index. The fund’s stock choices in the consumer discretionary sector, which is typically a bellwether of consumer spending cutbacks in economic downturns, included some of the sector’s better performers. 
 
The fund’s tobacco company stocks, its only holdings in the consumer staples sector, helped relative returns; they declined 11%, compared with the 31% drop in the index’s broader group of stocks in this sector. The fund’s stock choices in materials and information technology, however, did not fare as well by comparison.
Don’t lose sight of the long term during short-term ups and downs
The Selected Value Fund’s aggressive strategy can make for sharp swings in year-to-year performance. When we reported to you 12 months ago, the fund had returned 10.15% for the 2007 fiscal year, which seems light-years away from the –37.79% reported for the 2008 fiscal year.
 
 
 
 
Your Fund Compared With Its Peer Group
 
 
 
 
Average
 
 
Mid-Cap
 
Fund
Value Fund
Selected Value Fund
0.42%
1.41%
 
 
1 The fund expense ratio shown is from the prospectus dated February 22, 2008. For the fiscal year ended October 31, 2008, the fund’s expense ratio was 0.38%. The peer-group expense ratio is derived from data provided by Lipper Inc. and captures information through year-end 2007.
 
4


As Vanguard has long advised investors, a sensible way to evaluate an investment is to assess its longer-term performance. That can help you put short-term swings (especially downward swings) in perspective—which may be particularly valuable in trying times like these. However, an investment’s long-term record can be skewed, for better or worse, by the most recent short-term results. A year ago, for example, the Selected Value Fund had reported an average annual return over the previous decade of 8.79%; as of October 2008, the ten-year figure stood at 5.80%, a reflection of the past year’s struggle.
Of course, Selected Value’s recent reversal is a clear disappointment. It’s important to remember that the fund focuses on a relatively small number of mid-cap stocks that the market has deeply discounted but that the advisor believes have high potential for growth. Such an investment approach can be expected to produce ups and downs that are more dramatic than those of the broad U.S. stock market, especially in tumultuous periods such as the past 12 months.
Some principles of investing never go out of style
Over the past year, global financial markets have experienced an unnerving confluence of events. As experienced investors know, selling in a panic, or letting your emotions drive your investment decisions, is often a recipe for disappointment.
 
 
Total Returns
 
Ten Years Ended October 31, 2008
 
 
Average
 
Annual Return
Selected Value Fund
5.80%
Russell Midcap Value Index
5.74
6.36
 
The figures shown represent past performance, which is not a guarantee of future results. (Current performance may be lower or higher than the performance data cited. For performance data current to the most recent month-end, visit our website at www.vanguard.com/performance.) Note, too, that both investment returns and principal value can fluctuate widely, so an investor’s shares, when sold, could be worth more or less than their original cost.
 
 
1 Derived from data provided by Lipper Inc.
 
5


Instead, it’s important to focus on the time-tested principles of balance and diversification, both within and across asset classes. That is why we always encourage you to choose a mix of stock, bond, and money market funds that is consistent with your goals, time horizon, and tolerance for the markets’ inevitable ups and downs—and then try to stick with it. The Selected Value Fund can be an important element of such a diversified portfolio.
Of course, even balanced portfolios of stocks and bonds have faced tough times during the past 12 months. But history suggests that these principles can put you in a good position to achieve long-term investment success.
Thank you for your confidence in Vanguard.
Sincerely,


