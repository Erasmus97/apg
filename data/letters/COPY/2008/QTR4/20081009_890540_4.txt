Dear Shareholders,

As this is our inaugural letter to the shareholders of this Fund, we first want
to take this opportunity to express our enthusiasm and gratitude for the
opportunity and privilege to manage a portion of your assets.

The Champlain Mid Cap Fund is being managed with the same process by which we
have managed small caps equities since although we have some slightly
different portfolio construction guidelines that allow for a somewhat greater
concentration of individual holdings and sectors. We believe that our investment
process translates well into the mid cap arena, and we encourage our investors
to examine the performance track record of our mid cap composite which, we have
managed since as highlighted in the prospectus for this Fund. From our
perspective, we have typically found the risk/reward ratio for mid caps to be
compelling as most of the companies we review are still small enough to enjoy
above average organic growth while also being more established and having more
experienced management teams than the average small cap company.

Our investment process strives to buy superior mid cap companies at a discount
to our idea of their Fair or Intrinsic Value. So what, in our estimation,
constitutes a superior company? As with small caps, our definition of a superior
company incorporates passing specific factor tests, which we believe mitigate
business model risk within each of the major sectors of the stock market -
Consumer, Healthcare, Industrials, Technology and Financials. These factors
serve another key function in that they work to keep us within our realm of
competence. We then look for certain attributes of a high-performance business,
including a positive spread between the cash return on invested capital and the
cost of capital - without excessive debt. We also like to see real potential for
faster than average growth within a given sector, as well as sincere and capable
management. Our focus on cash earnings is entirely consistent with the axiom:
cash is fact while accounting profits are only opinion. Finally, we use a
variety of valuation models and analysis to help us develop a realistic and
grounded idea of what Fair or Intrinsic Value might be for a company. In the
final analysis, we consider Fair Value to be what another well-informed company
or financial buyer would pay to own the whole company, less a typical control
premium.

Our process has a distinct and unwavering bias for quality companies -
consistent growth and stable returns - while also being sensitive to valuation.
We






THE ADVISORS' INNER CIRCLE FUND II CHAMPLAIN
MID CAP FUND

believe successful execution of our investment process will dampen the
volatility of this Fund's returns and lower the risk of large losses. We
understand how important it is to avoid large losses if the goal is to compound
returns and grow capital. However, our process is also expected to
under-perform, to some degree, during speculative periods or when investors are
hungry for low quality and/or highly cyclical companies. Conversely, we expect
to outperform the mid cap benchmarks when quality is in favor or when investors
flee risk and place a premium on safety. We certainly seek out companies with
strong prospects for sustainable growth, but we are very careful about how much
we will pay for them.

Looking ahead, we still find the valuations of quality companies across all
market caps to be attractive on both an absolute and relative sense. While the
housing, consumer and capital market problems associated with an era of too easy
credit are likely to require more time to resolve, and may lead to a sustained
period of slow to little domestic economic growth for the United States; an
environment that is likely to help the quality companies that consistently grow
their profits regain investor favor and the premium valuations they have often
enjoyed in past periods. Quality companies that operate consistent and stable
business models also will typically have the means to prosper and grow even
during a period of slow economic growth. Indeed, because this Fund owns a
diversified collection of many of America's best mid cap companies that are
reasonably valued, we are enthusiastic about the Fund's potential to produce
attractive absolute and relative long-term returns for its shareholders.

Finally, should you want to learn more about Champlain Investment Partners, LLC
and the team that manages this product, we encourage you to read the prospectus
for this Fund and visit our web site at www.cipvt.com.

Sincerely,

Dear Shareholders,

During the fiscal year ended July the Champlain Small Company Fund
("Fund") returned compared to a return for the Russell Index.
Since its inception on November your Fund has returned compared
to a return for the Russell Index.

The strong relative return for your Fund during a period of intense financial
turmoil in the credit markets and increased risk aversion by equity investors
reinforces our confidence in Champlain's investment process and its strong bias
toward quality companies with stable business models that trade at a discount to
our idea of the Intrinsic or Fair Value for the shares. While we cannot manage
away all of the inherent risks associated with small cap companies, it has been
our long-held belief that avoiding the riskiest business models, as well as
overvalued stocks, would help us protect our shareholders' capital when the
inevitable storms hit the equity markets. For more insight into our process and
how we expect this Fund to perform over time, we encourage shareholders to
examine our prior letters to shareholders of this Fund.

Over the past year, our bias for quality companies has enabled strong returns
for your Fund in almost every sector relative to the Russell While our
reluctance to own consumer companies selling highly discretionary and large
ticket items has helped make the consumer sector our best performing sector on a
relative basis; the energy sector was clearly the best performing sector on an
absolute basis. Favorable stock selection and weighting within the energy sector
also allowed us to outperform on a relative basis. Financials and industrials
were the other sectors that generated meaningfully better relative performance
this past year. Although our health care holdings lagged behind the sector
slightly, they outperformed the overall small cap market with a positive
absolute return and rewarded our decision to overweight the sector.

Our constant efforts to rebalance your Fund's capital away from companies that
are overvalued or trade at only modest discounts and into the companies trading
at better discounts to Fair Value remains an integral part of our investment
process. While market volatility may increase this rebalancing activity and
contribute to turnover, we also expect it to contribute positively to our
absolute and relative returns during difficult market environments.
Additionally, we






THE ADVISORS' INNER CIRCLE FUND II CHAMPLAIN SMALL
COMPANY FUND

continuously challenge ourselves to high-grade the Fund's holdings so that we
minimize the opportunity costs that can accompany being too patient with
fundamentally challenged companies.

Since our last letter in January, your Fund continued to benefit from
acquisitions and strategic investors. In June, the management-led buyout of
Bright Horizons Family Solutions was completed, despite the difficult credit
environment. Also in June, Smith International agreed to acquire WH Energy
Services. Further, the recent announcement that Tokio Marine would acquire
Philadelphia Consolidated at a significant premium to both book value and the
last sale has reinforced our thinking that niche insurance companies remain
attractive business models and are materially undervalued. Even more exciting
than a takeover was the recent announcement that American Express would take a
minority stake in Concur Technologies and that both companies would form an
exclusive joint marketing partnership. This relationship should only strengthen
Concur's already formidable competitive position and improve the odds that
Concur will maintain its rapid revenue and earnings growth for several more
years.

Economic data concerning the health of the U.S. mortgage and housing industries,
as well as the consumer, remains quite negative, and the financial system has
yet to fully recognize all of its bad loans and money-losing positions. Still,
we see reasonable valuations for quality companies with a history of consistent
growth and this leaves us optimistic about the future returns our strategy may
produce over the next years. This is particularly true for the health care
sector, which looks poised for stronger returns once we get past the elections
and the early days of a new Presidential Administration. In addition to
reasonable valuations, we also are encouraged by the overall market
opportunities for the companies we own. To be sure, none of this means that most
stocks cannot trade lower, they probably will before this bear market is over.
However, we have a strong sense that we own a diversified collection of many of
America's best small cap companies - at least those that are publicly traded and
also priced at a reasonable discount to our idea of their Fair Value.

We are delighted to inform you that we have recently launched the Champlain Mid
Cap Fund (CIPMX) through The Advisors' Inner Circle Fund II. We have always
believed that our investment process would translate well into the mid






THE ADVISORS' INNER CIRCLE FUND II CHAMPLAIN SMALL
COMPANY FUND

cap arena. Since before the inception of our firm in the fall of we have
managed a mid cap strategy with the same investment process that we use for
small caps, but with some slightly different portfolio construction guidelines.
Please know that we are equally excited about the absolute return potential of
our mid cap fund as we are for our small cap fund.

On behalf of all of the partners and associates at Champlain Investment
Partners, let me express once again our sincerest appreciation for the
opportunity you gave us to manage some of your investable assets this past year.

Sincerely,
