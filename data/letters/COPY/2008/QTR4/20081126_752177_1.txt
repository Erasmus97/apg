Dear Shareholder,
For the 12 months ended September 30, Vanguard PRIMECAP Fund declined –14.0% for Investor Shares and –13.9% for Admiral Shares, holding up significantly better than the Standard & Poor’s 500 Index and the average multi-cap growth fund in a difficult environment.
Although the fiscal year began auspiciously, with many global stock markets reaching record highs in October 2007, the depth and breadth of credit market turmoil drove stocks far below their peaks by the end of the period. Nearly all sectors in the fund—and the stock market in general—lost ground. However, the advisor’s favorable stock selection among materials and beleaguered financial companies significantly boosted the fund’s relative performance. 
For those who invest in the fund through taxable accounts, page 25 shows after-tax returns based on the highest tax bracket. Please note that our preliminary estimates suggest that the fund will distribute capital gains of roughly $3.60 per share for Investor Shares and $3.70 per share for Admiral Shares in December 2008.
Credit-market turbulence weighed heavily on stock prices
Troubles simmering in the credit markets for much of the past year came to a boil at the end of the fiscal period, producing several high-profile bankruptcies and putting severe pressure on stock prices 
 
2


around the world. The broad U.S. stock market returned –21.2% for the 12 months ended September 30. In September alone, stock prices fell more than –9%. International stock markets were similarly disappointing, returning –30.0% for the full 12 months.
Policymakers and elected officials, both in the United States and abroad, responded to the upheavals with dramatic new programs designed to help stabilize the credit markets. As participants struggled to make sense of the markets’ fast-changing dynamics, stock prices were exceptionally volatile, with daily ups and downs of 2 percentage points or more becoming common.
 
U.S. Treasuries rallied in a nervous market
Nervousness in the stock market was echoed, and even amplified, in the bond market. For the 12 months, the broad U.S. bond market returned 3.7%, largely on the strength of Treasuries—investors’ security of choice in times of duress. Corporate bonds generally produced negative returns for the period, coming under heavy selling pressure during investors’ flight to safety. Even the municipal market, made up of generally high-quality securities issued by states and municipalities, recorded a negative 12-month return.
 
 
Market Barometer
 
 
Average Annual Total Returns
 
Periods Ended September 30, 2008
 
One Year
Three Years
Five Years
Stocks
 
 
 
Russell 1000 Index (Large-caps)
–22.1%
0.1%
5.5%
Russell 2000 Index (Small-caps)
–14.5
1.8
8.1
Dow Jones Wilshire 5000 Index (Entire market)
–21.2
0.6
6.0
MSCI All Country World Index ex USA (International)
–30.0
3.1
11.8
 
 
 
 
Bonds
 
 
 
Lehman U.S. Aggregate Bond Index (Broad taxable market)
3.7%
4.2%
3.8%
Lehman Municipal Bond Index
–1.9
1.9
2.8
Citigroup 3-Month Treasury Bill Index
2.6
4.0
3.1
 
 
 
 
CPI
 
 
 
Consumer Price Index
4.9%
3.2%
3.4%
 
 
3


The U.S. Federal Reserve Board responded to the turmoil with a dramatic easing of monetary policy. Over the full 12 months, the Fed reduced its target for the federal funds rate from 4.75% to 2.00%. On October 8, shortly after the close of the fiscal period, the Fed cut rates again, to 1.50%. The move was made in coordination with rate cuts by several other central banks.
Stock selection added value in several sectors 
The primary drivers of PRIMECAP Fund’s performance were information technology and health care holdings, which together represented about half of the fund’s average assets. The advisor has built significant positions in these two sectors in recent years, anticipating expanded use of computer chips and other electronics, particularly in the developing world, as well as strong growth in pharmaceuticals consumption. The challenging environment, however, led companies in both sectors to struggle.
As credit grew scarce, consumers and businesses (including major financial customers) cut spending on technology, a sector that accounted for about half of the fund’s retreat. Among the notable detractors were Google and the semiconductor manufacturers Texas Instruments (a top-ten holding during the year) and Micron Technology. 
In health care, Genzyme and Amgen reported higher revenues and promising clinical results, and were among the fund’s
 
 
 
 
 
Your Fund Compared With Its Peer Group
 
 
 
 
 
 
Average
 
Investor
Admiral
Multi-Cap
 
Shares
Shares
Growth Fund
PRIMECAP Fund
0.43%
0.31%
1.43%
 
 
Total Returns
 
Ten Years Ended September 30, 2008
 
 
 
 
Average
 
Annual Return
PRIMECAP Fund Investor Shares
9.1%
S&P 500 Index
3.1
3.5
 
The figures shown represent past performance, which is not a guarantee of future results. (Current performance may be lower or higher than the performance data cited. For performance data current to the most recent month-end, visit our website at www.vanguard.com/performance.) Note, too, that both investment returns and principal value can fluctuate widely, so an investor’s shares, when sold, could be worth more or less than their original cost.
 
 
1 The fund expense ratios shown are from the prospectus dated January 25, 2008. For the fiscal year ended September 30, 2008, the fund’s expense ratios were 0.43% for Investor Shares and 0.31% for Admiral Shares. The peer-group expense ratio is derived from data provided by Lipper Inc. and captures information through year-end 2007.
2 Derived from data provided by Lipper Inc. 
 
4


strongest contributors. Overall, however, health care stocks trimmed almost 2 percentage points from the fund’s return for the fiscal year. 
PRIMECAP Management Company, the fund’s advisor, seeks to identify companies or industries that appear to have superior long-term growth potential but are underappreciated by the market. This somewhat contrarian strategy can cause the fund’s results to diverge significantly from the performance of its benchmark index and peers, as was evident during the volatile 12 months. Two of the smaller sectors in the portfolio—materials and financials—illustrate this point. 
After climbing steeply during much of the past year, prices of raw materials and industrial commodities reversed course in midsummer, leading to weak 12-month returns in the market’s materials sector. In the PRIMECAP portfolio, by contrast, the advisor managed to capitalize on rising grain and food prices through sizable investments in fertilizer producer Potash Corp. of Saskatchewan (one of the fund’s largest holdings) and Monsanto. The fund’s materials holdings rose almost 9%, compared with a decline of more than –21% for this sector in the benchmark. 
And although gains were scarce in the embattled financial sector, the fund’s relatively small exposure to banks, brokerages, insurers, and other financial companies meant that shareholders escaped some of the industry’s trauma. The advisor also held some of the sector’s  better performers, including insurance broker Marsh & McLennan, which notched a double-digit gain for the year—helping to moderate some of the downdraft from small positions in fallen giants AIG, Fannie Mae, and Freddie Mac. 
For more on the advisor’s strategy and outlook, please see the Advisor’s Report on page 7.
Focus and discipline have enhanced the fund’s long-term results 
PRIMECAP Management Company has adhered to a disciplined, focused, and patient investment philosophy throughout the fund’s almost 25-year history. The advisor’s approach to identifying out-of-favor growth companies typically results in low turnover and fairly concentrated holdings, and it has handsomely rewarded shareholders over the years.
For the ten years ended September 30, the Investor Shares of PRIMECAP Fund earned an average annual return of 9.1%, almost 6 percentage points above the result for both the fund’s benchmark index and the average return of competing multi-cap growth funds. A hypothetical initial $25,000 investment in the fund’s Investor Shares made ten years ago would have compounded to almost $60,000 by September 30, compared with about $34,000 for a similar investment in the S&P 500. And the fund’s modest expense ratio has helped investors to keep more of the portfolio’s returns, an advantage that compounds over time.
 
 
5


During turbulent times, focus on the long run
Over the past year—and more acutely in recent weeks—global financial markets have experienced an unprecedented confluence of events. Understandably, many investors have been unnerved by the headlines about shrinking investment portfolios, declining home values, tight credit, and last-minute bank rescues. We firmly believe that maintaining a long-term perspective can help investors get through such rocky periods. Selling in a panic—or letting your emotions drive your investment decisions—is often a recipe for disappointment.
 
Instead, it’s important to focus on the time-tested principles of balance and diversification, both within and across asset classes. That’s why we encourage you to determine a mix of stock, bond, and money market funds that is consistent with your goals, time horizon, and tolerance for the markets’ inevitable ups and downs—and then try to stick with it. Of course, even balanced portfolios have faced tough times during the past 12 months. Everything that history has taught us about the markets, however, suggests that these principles can put you in the best position to achieve long-term investment success. 
Thank you for your confidence in Vanguard.
Sincerely,


Dear Shareholder,
For the fiscal year ended September 30, 2008, both domestic and international stocks suffered their worst losses since the bear market that ended in 2002. U.S. bond returns were modestly positive, providing a bit of counterbalance for the Target Retirement Funds, particularly those with larger weightings in bonds. 
The six Target Retirement Funds included in this report all posted declines consistent with their respective asset allocations. The returns ranged from –4.2% for the Target Retirement Income Fund, which has the heaviest allocation to bonds, to –17.6% for the Target Retirement 2025 Fund, with its heavier weighting in stocks, particularly international equities.
Credit market turbulence weighed heavily on stock prices
Troubles simmering in the credit markets for much of the past year came to a boil at the end of the fiscal period, producing several high-profile bankruptcies and putting severe pressure on stock prices around the world. The broad U.S. stock market returned –21.2% for the 12 months ended September 30. In September alone, stock prices fell more than 9%. International stock markets were similarly disappointing, returning –30.0% for the full 12 months.
 
2


Policy-makers and elected officials, both in the United States and abroad, responded to the upheavals with dramatic new programs designed to help stabilize the credit markets. As participants struggled to make sense of the markets’ fast-changing dynamics, stock prices were exceptionally volatile, with daily ups and downs of 2 percentage points or more becoming commonplace.
U.S. Treasuries rallied in a nervous market
Nervousness in the stock market was echoed, and even amplified, in the bond market. For the 12 months, the broad U.S. bond market returned 3.7%, largely on the strength of Treasuries—investors’ security of choice in times of duress.
Corporate bonds generally produced negative returns for the period, coming under heavy selling pressure during investors’ flight to safety. Even the municipal market, made up of generally high-quality securities issued by states and municipalities, recorded a negative 12-month return.
The U.S. Federal Reserve Board responded to the turmoil with a dramatic easing of monetary policy. Over the full 12 months, the Fed reduced its target for the federal funds rate from 4.75% to 2.00%. On October 8, shortly after the close of the fiscal period, the Fed cut rates again, to 1.50%. The move was made in coordination with rate cuts by several other central banks.
 
 
Market Barometer
 
 
Average Annual Total Returns
 
Periods Ended September 30, 2008
 
One Year
Three Years
Five Years
Stocks
 
 
 
Russell 1000 Index (Large-caps)
–22.1%
0.1%
5.5%
Russell 2000 Index (Small-caps)
–14.5
1.8
8.1
Dow Jones Wilshire 5000 Index (Entire market)
–21.2
0.6
6.0
MSCI All Country World Index ex USA (International)
–30.0
3.1
11.8
 
 
 
 
Bonds
 
 
 
Lehman U.S. Aggregate Bond Index (Broad taxable market)
3.7%
4.2%
3.8%
Lehman Municipal Bond Index
–1.9
1.9
2.8
Citigroup 3-Month Treasury Bill Index
2.6
4.0
3.1
 
 
 
 
CPI
 
 
 
Consumer Price Index
4.9%
3.2%
3.4%
 
 
 
3


Funds with higher bond allocations suffered less than the others
During the fiscal year, performance for the Vanguard Target Retirement Funds depended on where each fund stood on the retirement-year spectrum. The best return—although still negative—came from the Target Retirement Income Fund, which is designed for investors in or near retirement. It has the highest exposure to bonds and invests about 5% of its assets in a money market portfolio. 
At the other end of the spectrum was the Target Retirement 2025 Fund, which has the most distant retirement date in this group. Consequently, it is the most aggressive of the six funds, with 15%  of its assets in international stocks, 63% in U.S. stocks, and 22% in bonds as of September 30. Therefore, its return was the lowest, given the poor performance of stocks.
Each Target Retirement Fund includes a mix of Vanguard stock and bond funds (and in two cases a money market fund), with that mix calibrated according to the fund’s target maturity date. As the retirement date approaches, these mixes gradually shift to become more conservative, and more income-oriented.
The funds designed for investors in or closest to retirement—the Target Retirement Income Fund and the Target
 
 
Asset Allocations on September 30, 2008
 
 
 
 
 
 
 
 
 
 
Short-Term
 
Bonds
Investments
30%
65%
5%
2005
42
56
2
2010
54
46
0
2015
63
37
0
2020
70
30
0
2025
78
22
0
 
 
1 As of September 30, 2008, international stock weightings for the Income, 2005, 2010, 2015, 2020, and 2025 Funds were 6%, 8%, 11%, 12%, 14%, and 15% of assets, respectively.
2 Allocations do not change. 
 
 
4


Retirement 2005 Fund—both hold some portion of their assets in Vanguard Prime Money Market Fund and Vanguard Inflation-Protected Securities Fund. The inclusion of these two funds is intended to provide shareholders with a combination of inflation protection and stability. The Inflation-Protected Securities Fund seeks to protect investors against the long-term effects of inflation by investing in bonds with a builtin inflation safeguard.
 
Over the 12 months, the worst performers among the underlying Vanguard funds represented in the Target Retirement portfolios were the international funds: Vanguard Emerging Markets Stock Index Fund (–32.7%), European Stock Index Fund (–30.0%), and Pacific Stock Index Fund (–27.3%). The Total Stock Market Index Fund, reflecting U.S. equities, returned –21.2%. On the other hand, modest gains were posted by Vanguard
 
 
Total Returns
 
 
 
Average Annual
 
Total Return
Vanguard Target Retirement Income Fund
4.2%
Target Income Composite Index
4.2
3.1
Vanguard Target Retirement 2005 Fund
4.4%
Target 2005 Composite Index
4.4
3.3
Vanguard Target Retirement 2010 Fund
2.2%
Target 2010 Composite Index
2.2
0.1
Vanguard Target Retirement 2015 Fund
4.6%
Target 2015 Composite Index
4.6
3.5
Vanguard Target Retirement 2020 Fund
1.2%
Target 2020 Composite Index
1.1
–0.7
Vanguard Target Retirement 2025 Fund
4.7%
Target 2025 Composite Index
4.6
3.7
 
The figures shown represent past performance, which is not a guarantee of future results. (Current performance may be lower or higher than the performance data cited. For performance data current to the most recent month-end, visit our website at www.vanguard.com/performance.) Note, too, that both investment returns and principal value can fluctuate widely, so an investor’s shares, when sold, could be worth more or less than their original cost.
 
 
1 For the Income, 2005, 2015, and 2025 Funds, inception was October 27, 2003; for the 2010 and 2020 Funds, inception was June 7, 2006.
2 Derived from data provided by Lipper Inc.
 
5
 
Total Bond Market Index Fund (+3.8%) and the Inflation-Protected Securities Fund (+6.2%). The Prime Money Market Fund provided a return of 3.4%.
Low expenses provide a cost-efficient investment 
To accomplish their mission of providing broadly diversified investments across asset classes in a mix that becomes more conservative over time, the Target Retirement Funds invest primarily in cost-efficient Vanguard index funds that seek to capture the returns of the broad stock and bond markets.
The funds have provided competitive performance since their inception, as you can see in the table on page 5. The return of each of the Target Retirement Funds falls within 0.1 percentage point of the performance of its composite benchmark index, which reflects market performance without operating costs subtracted. And each fund’s return has outperformed an appropriate composite average based on mutual-fund peer groups.
Over time, the low costs of the Vanguard Target Retirement Funds have provided a distinct advantage. For a look at how the funds’ costs compare with the composite average expenses of peers, please see the table below.
Still a smart way to help keep your retirement plan on track 
There’s no doubt the past year has been unsettling for investors. While we cannot predict what will happen in the markets 
 
 
Expense Ratios
 
 
Your Fund Compared With Its Peer Group
 
 
 
Acquired Fund
Peer-Group
 
Fees and
Expense
 
Income
0.19%
1.11%
2005
0.19
1.16
2010
0.20
1.22
2015
0.19
1.27
2020
0.20
1.29
2025
0.19
1.32
 
 
 
1 This figure—drawn from the prospectus dated January 25, 2008—represents a weighted average of the annualized expense ratios and any transaction fees charged by the underlying mutual funds (the “acquired” funds) in which the Target Retirement Funds invest. The Target Retirement Funds do not charge any expenses or fees of their own. For the fiscal year ended September 30, 2008, the acquired fund fees and expenses were 0.19% for the Target Retirement Income Fund, 0.18% for the 2005 Fund, 0.19% for the 2010 Fund, 0.18% for the 2015 Fund, 0.19% for the 2020 Fund, and 0.18% for the 2025 Fund. 


Dear Shareholder,
 
For the fiscal year ended September 30, 2008, both domestic and international stocks suffered their worst losses since the bear market that ended in 2002. U.S. bond returns were modestly positive, providing a bit of counterbalance for the Target Retirement Funds. 
The five Target Retirement Funds included in this report all posted declines consistent with their respective asset allocations. The four Target Retirement Funds that have allocated 90% of their assets to stocks returned –20.4%, while the 2030 fund, with its slightly lower stock weighting, lost –19.4%.
Credit market turbulence weighed heavily on stock prices
Troubles simmering in the credit markets for much of the past year came to a boil at the end of the fiscal period, producing several high-profile bankruptcies and putting severe pressure on stock prices around the world. The broad U.S. stock market returned –21.2% for the 12 months ended September 30. In September alone, the market fell more than 9%. International stock markets were similarly disappointing, returning –30.0% for the full 12 months.
Policymakers and elected officials, both in the United States and abroad, responded to the upheavals with dramatic new programs designed to help stabilize the credit markets. As participants struggled to make sense of the markets’ fast-changing 
 
2


dynamics, stock prices were exceptionally volatile, with daily ups and downs of 2 percentage points or more becoming commonplace.
U.S. Treasuries rallied in a nervous market
Nervousness in the stock market was echoed, and even amplified, in the bond market. For the 12 months, the broad U.S. bond market returned 3.7%, largely on the strength of Treasuries—investors’ security of choice in times of duress. Corporate bonds generally produced negative returns for the period, coming under heavy selling pressure during investors’ flight to safety. Even the municipal market, made up of generally high-quality securities issued by states and municipalities, recorded a negative 12-month return.
 
The U.S. Federal Reserve Board responded to the turmoil with a dramatic easing of monetary policy. Over the full 12 months, the Fed reduced its target for the federal funds rate from 4.75% to 2.00%. On October 8, shortly after the close of the fiscal period, the Fed cut rates again, to 1.50%. The move was made in coordination with rate cuts by several other central banks.
The funds’ stock-heavy allocations led to significant losses
Each Target Retirement Fund includes a mix of Vanguard stock and bond funds calibrated according to the fund’s target maturity date. As the targeted retirement year approaches, each fund’s stock-to-bond ratio gradually shifts to a more conservative, income-oriented mix.
 
 
Market Barometer
 
 
Average Annual Total Returns
 
Periods Ended September 30, 2008
 
One Year
Three Years
Five Years
Stocks
 
 
 
Russell 1000 Index (Large-caps)
–22.1%
0.1%
5.5%
Russell 2000 Index (Small-caps)
–14.5
1.8
8.1
Dow Jones Wilshire 5000 Index (Entire market)
–21.2
0.6
6.0
MSCI All Country World Index ex USA (International)
–30.0
3.1
11.8
 
 
 
 
Bonds
 
 
 
Lehman U.S. Aggregate Bond Index (Broad taxable market)
3.7%
4.2%
3.8%
Lehman Municipal Bond Index
–1.9
1.9
2.8
Citigroup 3-Month Treasury Bill Index
2.6
4.0
3.1
 
 
 
 
CPI
 
 
 
Consumer Price Index
4.9%
3.2%
3.4%
 
 
3


Because the Target Retirement Funds in this report are designed for longer time horizons (with targeted dates at least 20 years away), all five funds invest primarily in U.S. and international stocks in seeking to provide long-term growth of capital. However, a stock-heavy mix presents greater short-term risks at times, and this was clearly the case during the just-concluded fiscal year.
As stock markets outside the United States retreated, the worst performers among the underlying Vanguard funds represented in the Target Retirement portfolios were the international funds: Vanguard Emerging Markets Stock Index Fund (–32.7%), European Stock Index Fund
(–30.0%), and Pacific Stock Index Fund (–27.3%). These funds constitute between 17% and 18% of the Target Retirement Funds’ assets. The rest of the Target Retirement Funds’ stock allocation is invested in Vanguard Total Stock Market Index Fund, which tracks the broad U.S. market and returned –21.2%. 
On the other hand, modest gains were posted by Vanguard Total Bond Market Index Fund (+3.8%). Although the Target Retirement Funds in this report allocate only a small portion of their assets to bonds, their positive return during the retreat by stocks reflected their classic role as a useful counterweight to the sometimes-volatile stock market.
 
 
Asset Allocations on September 30, 2008
 
 
 
Short-Term
 
Bonds
Investments
2030
86%
14%
0%
2035
90
10
0
2040
90
10
0
2045
90
10
0
2050
90
10
0
 
 
1 As of September 30, 2008, international stock weightings for the 2030, 2035, 2040, 2045, and 2050 Funds were 17%, 18%, 18%, 18%, and 18% of assets, respectively.
 
 
4


The funds’ low expenses provide a cost-efficient investment
To accomplish their mission of providing broadly diversified investments across asset classes in a mix that becomes more conservative over time, the Target Retirement Funds invest primarily in cost-efficient Vanguard index funds that seek to capture the returns of the broad stock and bond markets.
The funds have provided competitive performance since their inceptions, as you can see in the table below. The return of each of the Target Retirement Funds falls within 0.1 percentage point of the performance of its composite benchmark index, which reflects market performance without operating costs subtracted. And each fund’s return is higher than an appropriate composite average based on mutual-fund peer groups.
Over time, the low costs of the Vanguard Target Retirement Funds have provided a distinct advantage. For a look at how the funds’ costs compare with the composite average expenses of peers, please see page 6.
 
 
Total Returns
 
 
 
Average
 
Annual Return
Vanguard Target Retirement 2030 Fund
0.2%
Target 2030 Composite Index
0.1
–1.4
Vanguard Target Retirement 2035 Fund
5.2
Target 2035 Composite Index
5.2
4.2
Vanguard Target Retirement 2040 Fund
–0.4
Target 2040 Composite Index
–0.5
–1.9
Vanguard Target Retirement 2045 Fund
5.8
Target 2045 Composite Index
5.8
4.8
Vanguard Target Retirement 2050 Fund
–0.2
Target 2050 Composite Index
–0.3
–1.7
 
The figures shown represent past performance, which is not a guarantee of future results. (Current performance may be lower or higher than the performance data cited. For performance data current to the most recent month-end, visit our website at www.vanguard.com/performance.) Note, too, that both investment returns and principal value can fluctuate widely, so an investor’s shares, when sold, could be worth more or less than their original cost.
 
1 For the 2035 and 2045 Funds, October 27, 2003; for the 2030, 2040, and 2050 Funds, June 7, 2006.
2 Derived from data provided by Lipper Inc.
 
 
5


Still a smart way to help keep your retirement plan on track 
There’s no doubt the past year has been unsettling for investors. While we cannot predict what will happen in the markets in the future, experienced investors know we have navigated through stormy times before. Through good times and bad, investors who have been in the market over several decades have experienced the benefits of sticking with a long-term investment program, particularly one that relies on a diversified, carefully constructed mix of stock, bond, and money market investments suited to one’s personal goals, time horizon, and tolerance for risk.
As we’ve seen in the past year, even a highly diversified strategy that combines stocks and bonds doesn’t prevent sharp losses during times of widespread market turmoil. Over time, however, the principles of broad and steady diversification, careful balance, and low costs have served as powerful allies for long-term investors. By investing in a Target Retirement Fund, you have chosen an investment that is designed for your time horizon and is managed to help you meet your needs for a lifetime.
 
Thank you for your confidence in Vanguard.
 
Sincerely,


