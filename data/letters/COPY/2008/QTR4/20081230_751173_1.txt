Dear
Shareholders: 
®
For global investors, the past year has been challenging to say the least. From October 2007 highs, U.S. equity markets
were down more than 35% through October 31, 2008. Economic news is not much better, with slowing global growth, and continued fallout from the credit crisis hanging over the economy. While this has resulted in negative returns over the course
of the year, it has also resulted in historically low valuations for many U.S. stocks. 
Late in 2007, we selectively trimmed positions in
the Information Technology and Consumer Staples sectors, where investment themes were playing out and valuations reflected higher expectations going forward. Many of our holdings with themes linked to global growth began to hit fair value, which
allowed us to take gains. As a result, we also lightened our allocation to Energy stocks late in the year, and have remained underweight compared to the benchmark since then. 
We took advantage of the increased volatility and short-term weakness in the first quarter of 2008 to reinitiate positions in several Information Technology stocks that we had reduced in 2007, as
well as to add to areas in the Health Care sector. Given reasonable valuations in many areas of the market prior to this year, we used the market volatility in 2008 both to opportunistically buy more of existing holdings that were trading at more
attractive levels and to upgrade into new holdings with stronger competitive positioning that now met our pricing disciplines. 
Currently,
our largest sector weightings remain Information Technology and Health Care, specifically focused on areas that are less sensitive to slowing U.S. consumer spending. We continue to have comparatively small positions in both Energy and Financials
relative to the benchmark. Our small position in Financials has helped relative performance over the course of the year in light of the credit crisis. While the small position in Energy detracted from returns in the first half of 2008, it has added
value relative to the benchmark so far in the second half of the year. 
Through the first half of 2008, we increased the Series’
holdings in industry leaders in the Industrials and transportation sectors that were gaining market share in the face of higher energy costs, primarily at the expense of their weaker competitors. The declines in energy and commodity prices benefited
these holdings to a greater extent than the overall market, and drove significant relative outperformance during the third quarter of 2008. 
As always, we appreciate your business. 
Sincerely, 


Dear
Shareholders: 
The Overseas Series produced negative absolute returns for the 12-month period ended October 31, 2008, it
substantially outperformed its benchmark, the Morgan Stanley Capital International (MSCI) All Country World Index ex U.S. for that time period. Over the current foreign stock market cycle (which includes both rising and falling markets and began on
January 1, 2000), the Overseas Series has also meaningfully outperformed the benchmark. 
Our team of analysts uses time-tested
strategies to choose stocks for the portfolio. These strategies include the Strategic Profile Strategy, Hurdle Rate Strategy, and the Bankable Deal Strategy. Our investment process also follows strict pricing disciplines in order to avoid buying
stocks that are not attractively valued. 
The Series currently has relatively large positions compared to the benchmark in the Health Care
and Consumer Staples sectors. Within the Health Care sector, the majority of the stocks we bought fall under our Strategic Profile Strategy. In this strategy, our analysts seek to identify companies that we believe can grow their earnings faster
than overall global economic growth. Typically this is accomplished by some sustainable competitive advantage that keeps competitors from entering their markets. Our holdings in the Health Care sector are spread among pharmaceutical companies,
diagnostic testing companies, and firms that supply products to the drug discovery market. Within the Consumer Staples sector, the Series’ holdings are concentrated in large, well-diversified companies with strong brand names that derive a
meaningful portion of their earnings and revenue from both developed and emerging economies. Given the defensive nature of the Health Care and Consumer Staples sectors, the Series’ overweight positions to these areas helped performance relative
to the benchmark in light of the credit crisis and weakening economic environment. 
The Series has also maintained a larger position than
the benchmark in the Information Technology sector. Many of the companies in the Information Technology sector are owned under either the Strategic Profile Strategy or the Hurdle Rate Strategy. Under the Hurdle Rate Strategy, we seek to identify
industries in a downturn where profits are temporarily depressed and future expectations are low. When capacity has been taken out of the industry and the supply/demand situation is more in balance, we want to own those companies that are in the
strongest competitive position to increase profits and market share. We currently own companies that develop software titles for the video game console market under the Hurdle Rate Strategy. Under the Profile Strategy, we own an enterprise
application software company and a company that provides billing and customer relationship management software to the telecommunications industry. Stocks selection in this sector benefited performance relative to the benchmark for the year.

The Series currently has a relatively small position in the Financials sector as compared to the benchmark, and this benefited performance
as the sector was one of the worst performers over the past 12 months. We have largely shied away from companies with direct exposure to the subprime mortgage market and positioned the portfolio in companies that earn most of their revenues from
fee-based business. These include large diversified banks, insurance brokerage, and ratings agency businesses in developed European countries. We hold a few stocks in the Bankable Deal Strategy within the Financials sector. Under this strategy, we
look for companies that are trading for a fraction of the underlying asset values or cash flows they can generate. 
The Series maintained a
relatively small position in emerging markets, which benefited relative performance. Stock markets in these regions are highly sensitive to global growth and retreated markedly after the speculative boom in energy and commodity prices ended during
the third quarter of 2008. The Series maintained its exposure to economies in Europe, specifically France, Switzerland, and Germany. Continued structural reform and compelling valuations have offered unique opportunities in these markets. Relative
to the benchmark, the Series has a small position in Japanese stocks. As always, we adhere to our stock selection strategies, unique investment process, and our strict pricing disciplines in positioning the portfolio. 
As always, we appreciate your business. 
Sincerely, 


Dear
Shareholders: 
®
Uncertainty in both equity and fixed income markets dominated this year. Volatility, which was slightly bolstered at the end of 2007, soared to unimagined heights through the 3
®
The Series’ relatively large positions in stocks relative to their benchmarks
had the greatest negative impact on their performance. The comparatively small allocation to Energy stocks relative to the benchmark also held back performance for the first eight months of this period, most notably in the Target Series 2040 and
2050 Series portfolios. On a bottom-up basis, our investment process is designed to identify good businesses at attractive valuations and to avoid speculatively priced segments. This discipline kept holdings in the Energy sector limited throughout
the twelve month period through October 31, 2008, which helped performance over the last four months of the period. Broadly, relatively large allocations to Health Care and relatively small positions in the Financials sector both contributed
positively to performance compared to the benchmarks. Also, our stock selection strategies identified specific positive performers, particularly in the Industrials sector. 
As mentioned briefly above, given the protracted credit crisis, spawned by over-extension of consumer and mortgage credit, the Target Series benefited from relatively small positions in the
Financials sector as compared to their benchmarks. The crisis resulted in a succession of failed financial institutions and ultimately governmental intervention. Several major institutions ceased to exist while others expanded by acquisition of
their former competitors. Fortunately, our direct exposure to distressed financials was limited. Our investments in the financial services sector focused predominantly on companies with diversified business lines which produce recurring revenue in
various market environments. These selections reflected our preference for banks with low mortgage exposure and high fee-based income relative to industry peers. 
Fixed income market returns reflected sharp differences between returns of various sectors and credit quality segments. Our fixed income returns performed better than the fixed income portions of the benchmarks,
particularly for the Target Income Series, which has close to 60% of the holdings allocated to fixed income. Our positive fixed income performance was aided specifically by our decision to avoid most financial corporate bonds in favor of Treasury
bonds, as well as high quality industrial bonds and government-sponsored FNMA (Fannie Mae) and FHLMC (Freddie Mac) securities. In addition, we took advantage of a sharp interest rate decline in September, paring back long-term bond holdings which
had been established during the second quarter when rates had spiked. 
 
(unaudited)


Dear
Shareholders: 
®
For global investors, the past year has been
challenging to say the least. From October 2007 highs, U.S. equity markets were down more than 35% through October 31, 2008. Economic news is not much better, with slowing global growth, and continued fallout from the credit crisis hanging over
the economy. While this has resulted in negative returns over the course of the year, it has also resulted in historically low valuations for many U.S. stocks. 
Late in 2007, we selectively trimmed positions in the Information Technology and Consumer Staples sectors, where investment themes were playing out and valuations reflected higher expectations going forward. Many of
our holdings with themes linked to global growth began to reach fair value, which allowed us to take gains. As a result, we also lightened our allocation to Energy stocks late in the year, and have remained underweight compared to the benchmark
since then. 
We took advantage of the increased volatility and short-term weakness in the first quarter of 2008 to reinitiate positions in
several Information Technology stocks that we had reduced in 2007, as well as to add to areas in the Health Care sector. Given reasonable valuations in many areas of the market prior to this year, we used the market volatility in 2008 both to
opportunistically buy more of existing holdings that were trading at more attractive levels and to upgrade into new holdings with stronger competitive positioning that now met our pricing disciplines. 
Currently, our largest sector weightings remain Information Technology and Health Care, specifically focused on areas that are less sensitive to slowing
U.S. consumer spending. We continue to have comparatively small positions in both Energy and Financials relative to the benchmark. Our small position in Financials has helped relative performance over the course of the year in light of the credit
crisis. While the small position in Energy detracted from returns in the first half of 2008, it has added value relative to the benchmark so far in the second half of the year. 
Through the first half of 2008, we increased the Series’ holdings in industry leaders in the Industrials and transportation sectors that were gaining market share in the face of higher
energy costs, primarily at the expense of their weaker competitors. The declines in energy and commodity prices benefited these holdings to a greater extent than the overall market, and drove significant relative outperformance during the third
quarter of 2008. 
As always, we appreciate your business. 
Sincerely, 


Dear
Shareholders: 
Uncertainty in both equity and fixed income markets dominated
this year. Volatility, which was slightly bolstered at the end of 2007, soared to unimagined heights through the 3
®
®
®
®
®
 
(unaudited)


