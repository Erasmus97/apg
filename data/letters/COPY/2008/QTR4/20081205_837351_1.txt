Dear fellow shareholder,

As we begin our year of operations, we are pleased to provide you
with our Fund's Annual Report.

It has been and continues to be daunting times for investors. We have
seen unprecedented downturns in the domestic and international equity
markets. The economy seems to be in a negative spin with few signs of
immediate recovery. Amidst this continued turmoil, municipal bonds have
generally experienced less volatility than many of their equity counterparts.
As shareholders of the Hawaii Municipal Fund, you are earning tax-free
income* and supporting local projects designed to enrich our community. The
money raised through municipal bonds is commonly used to build schools,
hospitals, roads, airports, harbors, and water and electrical systems that
serve to create jobs and improve the quality of life here in our islands.

On the following pages are line graphs comparing the Fund's performance
to the Lehman Muni Bond Index for the years ended September The
graph assumes a hypothetical investment in the Fund. The object of
the graph is to permit a comparison of the Fund with a benchmark and to
provide perspective on market conditions and investment strategies and
techniques that materially affected the performance of the Fund.

Interest rates are the most important of many factors which can affect
bond prices. Over the course of the fiscal year, the treasury yield curve
steepened with short-term interest rates falling by and long-term
interest rates dropping by just under one-half percent. Toward the end of
the fiscal year, municipal bonds were adversely affected by the global
financial crisis. The municipal market was hurt by selling pressure from
insurance companies, hedge funds, and mutual funds, which were experiencing
significant redemptions. Institutional buyers were scarce, with retail
investors being the main source of demand. This accounts for the Hawaii
Municipal Fund's fiscal year price decrease of The Hawaii Municipal
Fund Investor Class had a Net Asset Value ("NAV") of on October
and a NAV of on September The primary investment
strategy of the Hawaii Municipal Fund is to purchase high quality long-term
Hawaii municipal bonds. The past year's performance for the Fund, which is
presented in this Annual Report, was primarily a result of the implementation
of this strategy. As of September of the Hawaii Municipal
Bond Fund's portfolio was invested in bonds rated AAA by Standard & Poor's.

During the fiscal year ended September the Federal Reserve
Bank lowered the Federal Funds Rate six times from to Despite
this basis point decrease in short term rates, the year treasury
bond's yield fell by basis points. In our opinion, this modest decrease
in rates at the long end of the yield curve can be interpreted as an
indication that bond investors believe inflation will not greatly increase
over the long-term. Still, there continues to be risks to inflation and the
bond market, among which are US fiscal policy, international
conflicts/terrorism and global economic factors.






STANDARD & POOR'S
MUNICIPAL BOND RATINGS
Hawaii Municipal Fund
September

[The following table was depicted as a pie chart in the printed material.]

Hawaii Municipal Fund
AAA
AA
AA-
BBB+
BBB
NR


We are proud to report that as a Hawaii resident, of the income
dividends earned in were both state and federal tax-free.* There were
no capital gain distributions for the Hawaii Municipal Fund for the
calendar year. There will be a capital gain distribution for the Hawaii
Municipal Fund for the calendar year.

If you have any questions about this Annual Report or would like us to
provide information about the Fund to your family or friends, please call us
at

Thank you for your business as well as the many referrals. On behalf
of the staff and management of the Fund, I would like to extend to you and
your family best wishes for a safe and happy holiday season.

Warmest Aloha,

/s/ Terrence K.H. Lee

Terrence K.H. Lee
President and CEO Lee Financial Securities, Inc./Distributor

A description of the policies and procedures that the Fund uses to determine
how to vote proxies relating to portfolio securities is available without
charge, upon request, by calling

Before investing, read the prospectus carefully. Please carefully consider
the Fund's investment objective, risks, and charges and expenses before
investing.* Some income may be subject to the federal alternative minimum
tax for certain investors. The prospectus contains this and other
information about the Fund. This Annual Report must be accompanied or
preceded by a prospectus.

*Fund's yield, share price and investment return fluctuate so that you may
receive more or less than your original investment upon redemption. Past
performance is no guarantee of future results. Hawaii Municipal Fund is a
series of First Pacific Mutual Fund, Inc.








Hawaii Municipal Fund Investor Class
Investment in Fund Compared to Lehman Muni Bond Index

[The following table was depicted as a line chart in the printed material.]


Hawaii Municipal Fund Lehman Muni
Investor Class Bond Index

$ $










Average Annual Total Return
Year
Year
Year

The graph above compares the increase in value of a investment in the
Hawaii Municipal Fund Investor Class with the performance of the Lehman Muni
Bond Index. The objective of the graph is to permit you to compare the
performance of the Fund with the current market and to give perspective to
market conditions and investment strategies and techniques pursued by the
investment manager that materially affected the performance of the Fund. The
Lehman Muni Bond Index reflects reinvestment of dividends but not the
expenses of the Fund. The return and principal value of an investment in the
Fund will fluctuate so that an investor's shares, when redeemed, may be worth
more or less than their original cost. Past performance is not indicative of
future results. The total returns are before taxes on distributions or
redemptions of Fund shares. The Fund's annual operating expense ratio, as
stated in the current prospectus, is This rate can fluctuate and may
differ from the expenses incurred by the Fund for the period covered by this
report.













Your Fund's Expenses
