Dear Investor:

Thank you for taking time to review the following discussions, from our
experienced portfolio management team, of the fund reporting period ended
October It was a time of enormous upheaval and change. We understand
and appreciate the challenges you have faced during this historic period, and
share your concerns about the economy, the markets, and fund holdings. To help
address these issues, I'd like to provide my perspective on how we have
managed--and continue to manage--your investments in these uncertain times.

As a company, American Century Investments&reg; is well positioned to deal with
market turmoil. We are financially strong and privately held, which allows us
to align our resources with your long-term investment interests. In addition,
our actively managed, team-based approach allows our portfolio teams to
identify attractive investment opportunities regardless of market conditions.

Our seasoned investment professionals have substantial experience and have
successfully navigated previous market crises. These portfolio managers and
analysts continue to use a team approach and follow disciplined investment
processes designed to produce the best possible long-term results for you. For
example, our equity investment teams are working closely with our fixed income
group to monitor and assess credit crisis developments. The fixed income team
anticipated dislocation in the credit markets and--through its disciplined
processes and teamwork--helped reduce our exposure to investments that
suffered substantial losses.

How soon a sustainable recovery will occur is uncertain. But I am certain of
this: Since we've demonstrated a consistent ability to execute solid,
long-term investment strategies and the discipline to remain focused during
times of volatility or shifts in the markets. We've stayed true to our
principles, especially our belief that your success is the ultimate measure of
our success.

Thank you for your continued confidence in us.

Sincerely,

Dear Investor:

Thank you for taking time to review the following discussions, from our
experienced portfolio management team, of the fund reporting period ended
October It was a time of enormous upheaval and change. We understand
and appreciate the challenges you have faced during this historic period, and
share your concerns about the economy, the markets, and fund holdings. To help
address these issues, I'd like to provide my perspective on how we have
managed--and continue to manage--your investments in these uncertain times.

As a company, American Century Investments&reg; is well positioned to deal with
market turmoil. We are financially strong and privately held, which allows us
to align our resources with your long-term investment interests. In addition,
our actively managed, team-based approach allows our portfolio teams to
identify attractive investment opportunities regardless of market conditions.

Our seasoned investment professionals have substantial experience and have
successfully navigated previous market crises. These portfolio managers and
analysts continue to use a team approach and follow disciplined investment
processes designed to produce the best possible long-term results for you. For
example, our equity investment teams are working closely with our fixed income
group to monitor and assess credit crisis developments. The fixed income team
anticipated dislocation in the credit markets and--through its disciplined
processes and teamwork--helped reduce our exposure to investments that
suffered substantial losses.

How soon a sustainable recovery will occur is uncertain. But I am certain of
this: Since we've demonstrated a consistent ability to execute solid,
long-term investment strategies and the discipline to remain focused during
times of volatility or shifts in the markets. We've stayed true to our
principles, especially our belief that your success is the ultimate measure of
our success.

Thank you for your continued confidence in us.

Sincerely,

Dear Investor:

Thank you for taking time to review the following discussions, from our
experienced portfolio management team, of the fund reporting period ended
October It was a time of enormous upheaval and change. We understand
and appreciate the challenges you have faced during this historic period, and
share your concerns about the economy, the markets, and fund holdings. To help
address these issues, I'd like to provide my perspective on how we have
managed--and continue to manage--your investments in these uncertain times.

As a company, American Century Investments&reg; is well positioned to deal with
market turmoil. We are financially strong and privately held, which allows us
to align our resources with your long-term investment interests. In addition,
our actively managed, team-based approach allows our portfolio teams to
identify attractive investment opportunities regardless of market conditions.

Our seasoned investment professionals have substantial experience and have
successfully navigated previous market crises. These portfolio managers and
analysts continue to use a team approach and follow disciplined investment
processes designed to produce the best possible long-term results for you. For
example, our equity investment teams are working closely with our fixed income
group to monitor and assess credit crisis developments. The fixed income team
anticipated dislocation in the credit markets and--through its disciplined
processes and teamwork--helped reduce our exposure to investments that
suffered substantial losses.

How soon a sustainable recovery will occur is uncertain. But I am certain of
this: Since we've demonstrated a consistent ability to execute solid,
long-term investment strategies and the discipline to remain focused during
times of volatility or shifts in the markets. We've stayed true to our
principles, especially our belief that your success is the ultimate measure of
our success.

Thank you for your continued confidence in us.

Sincerely,

Dear Investor:

Thank you for taking time to review the following discussions, from our
experienced portfolio management team, of the fund reporting period ended
October It was a time of enormous upheaval and change. We understand
and appreciate the challenges you have faced during this historic period, and
share your concerns about the economy, the markets, and fund holdings. To help
address these issues, I'd like to provide my perspective on how we have
managed--and continue to manage--your investments in these uncertain times.

As a company, American Century Investments&reg; is well positioned to deal with
market turmoil. We are financially strong and privately held, which allows us
to align our resources with your long-term investment interests. In addition,
our actively managed, team-based approach allows our portfolio teams to
identify attractive investment opportunities regardless of market conditions.

Our seasoned investment professionals have substantial experience and have
successfully navigated previous market crises. These portfolio managers and
analysts continue to use a team approach and follow disciplined investment
processes designed to produce the best possible long-term results for you. For
example, our equity investment teams are working closely with our fixed income
group to monitor and assess credit crisis developments. The fixed income team
anticipated dislocation in the credit markets and--through its disciplined
processes and teamwork--helped reduce our exposure to investments that
suffered substantial losses.

How soon a sustainable recovery will occur is uncertain. But I am certain of
this: Since we've demonstrated a consistent ability to execute solid,
long-term investment strategies and the discipline to remain focused during
times of volatility or shifts in the markets. We've stayed true to our
principles, especially our belief that your success is the ultimate measure of
our success.

Thank you for your continued confidence in us.

Sincerely,

Dear Investor:

Thank you for taking time to review the following discussions, from our
experienced portfolio management team, of the fund reporting period ended
October It was a time of enormous upheaval and change. We understand
and appreciate the challenges you have faced during this historic period, and
share your concerns about the economy, the markets, and fund holdings. To help
address these issues, I'd like to provide my perspective on how we have
managed--and continue to manage--your investments in these uncertain times.

As a company, American Century Investments&reg; is well positioned to deal with
market turmoil. We are financially strong and privately held, which allows us
to align our resources with your long-term investment interests. In addition,
our actively managed, team-based approach allows our portfolio teams to
identify attractive investment opportunities regardless of market conditions.

Our seasoned investment professionals have substantial experience and have
successfully navigated previous market crises. These portfolio managers and
analysts continue to use a team approach and follow disciplined investment
processes designed to produce the best possible long-term results for you. For
example, our equity investment teams are working closely with our fixed income
group to monitor and assess credit crisis developments. The fixed income team
anticipated dislocation in the credit markets and--through its disciplined
processes and teamwork--helped reduce our exposure to investments that
suffered substantial losses.

How soon a sustainable recovery will occur is uncertain. But I am certain of
this: Since we've demonstrated a consistent ability to execute solid,
long-term investment strategies and the discipline to remain focused during
times of volatility or shifts in the markets. We've stayed true to our
principles, especially our belief that your success is the ultimate measure of
our success.

Thank you for your continued confidence in us.

Sincerely,

Dear Investor:

Thank you for taking time to review the following discussions, from our
experienced portfolio management team, of the fund reporting period ended
October It was a time of enormous upheaval and change. We understand
and appreciate the challenges you have faced during this historic period, and
share your concerns about the economy, the markets, and fund holdings. To help
address these issues, I'd like to provide my perspective on how we have
managed--and continue to manage--your investments in these uncertain times.

As a company, American Century Investments&reg; is well positioned to deal with
market turmoil. We are financially strong and privately held, which allows us
to align our resources with your long-term investment interests. In addition,
our actively managed, team-based approach allows our portfolio teams to
identify attractive investment opportunities regardless of market conditions.

Our seasoned investment professionals have substantial experience and have
successfully navigated previous market crises. These portfolio managers and
analysts continue to use a team approach and follow disciplined investment
processes designed to produce the best possible long-term results for you. For
example, our equity investment teams are working closely with our fixed income
group to monitor and assess credit crisis developments. The fixed income team
anticipated dislocation in the credit markets and--through its disciplined
processes and teamwork--helped reduce our exposure to investments that
suffered substantial losses.

How soon a sustainable recovery will occur is uncertain. But I am certain of
this: Since we've demonstrated a consistent ability to execute solid,
long-term investment strategies and the discipline to remain focused during
times of volatility or shifts in the markets. We've stayed true to our
principles, especially our belief that your success is the ultimate measure of
our success.

Thank you for your continued confidence in us.

Sincerely,

Dear Investor:

Thank you for taking time to review the following discussions, from our
experienced portfolio management team, of the fund reporting period ended
October It was a time of enormous upheaval and change. We understand
and appreciate the challenges you have faced during this historic period, and
share your concerns about the economy, the markets, and fund holdings. To help
address these issues, I'd like to provide my perspective on how we have
managed--and continue to manage--your investments in these uncertain times.

As a company, American Century Investments&reg; is well positioned to deal with
market turmoil. We are financially strong and privately held, which allows us
to align our resources with your long-term investment interests. In addition,
our actively managed, team-based approach allows our portfolio teams to
identify attractive investment opportunities regardless of market conditions.

Our seasoned investment professionals have substantial experience and have
successfully navigated previous market crises. These portfolio managers and
analysts continue to use a team approach and follow disciplined investment
processes designed to produce the best possible long-term results for you. For
example, our equity investment teams are working closely with our fixed income
group to monitor and assess credit crisis developments. The fixed income team
anticipated dislocation in the credit markets and--through its disciplined
processes and teamwork--helped reduce our exposure to investments that
suffered substantial losses.

How soon a sustainable recovery will occur is uncertain. But I am certain of
this: Since we've demonstrated a consistent ability to execute solid,
long-term investment strategies and the discipline to remain focused during
times of volatility or shifts in the markets. We've stayed true to our
principles, especially our belief that your success is the ultimate measure of
our success.

Thank you for your continued confidence in us.

Sincerely,

Dear Investor:

Thank you for taking time to review the following discussions, from our
experienced portfolio management team, of the fund reporting period ended
October It was a time of enormous upheaval and change. We understand
and appreciate the challenges you have faced during this historic period, and
share your concerns about the economy, the markets, and fund holdings. To help
address these issues, I'd like to provide my perspective on how we have
managed--and continue to manage--your investments in these uncertain times.

As a company, American Century Investments&reg; is well positioned to deal with
market turmoil. We are financially strong and privately held, which allows us
to align our resources with your long-term investment interests. In addition,
our actively managed, team-based approach allows our portfolio teams to
identify attractive investment opportunities regardless of market conditions.

Our seasoned investment professionals have substantial experience and have
successfully navigated previous market crises. These portfolio managers and
analysts continue to use a team approach and follow disciplined investment
processes designed to produce the best possible long-term results for you. For
example, our equity investment teams are working closely with our fixed income
group to monitor and assess credit crisis developments. The fixed income team
anticipated dislocation in the credit markets and--through its disciplined
processes and teamwork--helped reduce our exposure to investments that
suffered substantial losses.

How soon a sustainable recovery will occur is uncertain. But I am certain of
this: Since we've demonstrated a consistent ability to execute solid,
long-term investment strategies and the discipline to remain focused during
times of volatility or shifts in the markets. We've stayed true to our
principles, especially our belief that your success is the ultimate measure of
our success.

Thank you for your continued confidence in us.

Sincerely,

Dear Investor:

Thank you for taking time to review the following discussions, from our
experienced portfolio management team, of the fund reporting period ended
October It was a time of enormous upheaval and change. We understand
and appreciate the challenges you have faced during this historic period, and
share your concerns about the economy, the markets, and fund holdings. To help
address these issues, I'd like to provide my perspective on how we have
managed--and continue to manage--your investments in these uncertain times.

As a company, American Century Investments&reg; is well positioned to deal with
market turmoil. We are financially strong and privately held, which allows us
to align our resources with your long-term investment interests. In addition,
our actively managed, team-based approach allows our portfolio teams to
identify attractive investment opportunities regardless of market conditions.

Our seasoned investment professionals have substantial experience and have
successfully navigated previous market crises. These portfolio managers and
analysts continue to use a team approach and follow disciplined investment
processes designed to produce the best possible long-term results for you. For
example, our equity investment teams are working closely with our fixed income
group to monitor and assess credit crisis developments. The fixed income team
anticipated dislocation in the credit markets and--through its disciplined
processes and teamwork--helped reduce our exposure to investments that
suffered substantial losses.

How soon a sustainable recovery will occur is uncertain. But I am certain of
this: Since we've demonstrated a consistent ability to execute solid,
long-term investment strategies and the discipline to remain focused during
times of volatility or shifts in the markets. We've stayed true to our
principles, especially our belief that your success is the ultimate measure of
our success.

Thank you for your continued confidence in us.

Sincerely,

Dear Investor:

Thank you for taking time to review the following discussions, from our
experienced portfolio management team, of the fund reporting period ended
October It was a time of enormous upheaval and change. We understand
and appreciate the challenges you have faced during this historic period, and
share your concerns about the economy, the markets, and fund holdings. To help
address these issues, I'd like to provide my perspective on how we have
managed--and continue to manage--your investments in these uncertain times.

As a company, American Century Investments&reg; is well positioned to deal with
market turmoil. We are financially strong and privately held, which allows us
to align our resources with your long-term investment interests. In addition,
our actively managed, team-based approach allows our portfolio teams to
identify attractive investment opportunities regardless of market conditions.

Our seasoned investment professionals have substantial experience and have
successfully navigated previous market crises. These portfolio managers and
analysts continue to use a team approach and follow disciplined investment
processes designed to produce the best possible long-term results for you. For
example, our equity investment teams are working closely with our fixed income
group to monitor and assess credit crisis developments. The fixed income team
anticipated dislocation in the credit markets and--through its disciplined
processes and teamwork--helped reduce our exposure to investments that
suffered substantial losses.

How soon a sustainable recovery will occur is uncertain. But I am certain of
this: Since we've demonstrated a consistent ability to execute solid,
long-term investment strategies and the discipline to remain focused during
times of volatility or shifts in the markets. We've stayed true to our
principles, especially our belief that your success is the ultimate measure of
our success.

Thank you for your continued confidence in us.

Sincerely,

Dear Investor:

Thank you for taking time to review the following discussions, from our
experienced portfolio management team, of the fund reporting period ended
October It was a time of enormous upheaval and change. We understand
and appreciate the challenges you have faced during this historic period, and
share your concerns about the economy, the markets, and fund holdings. To help
address these issues, I'd like to provide my perspective on how we have
managed--and continue to manage--your investments in these uncertain times.

As a company, American Century Investments&reg; is well positioned to deal with
market turmoil. We are financially strong and privately held, which allows us
to align our resources with your long-term investment interests. In addition,
our actively managed, team-based approach allows our portfolio teams to
identify attractive investment opportunities regardless of market conditions.

Our seasoned investment professionals have substantial experience and have
successfully navigated previous market crises. These portfolio managers and
analysts continue to use a team approach and follow disciplined investment
processes designed to produce the best possible long-term results for you. For
example, our equity investment teams are working closely with our fixed income
group to monitor and assess credit crisis developments. The fixed income team
anticipated dislocation in the credit markets and--through its disciplined
processes and teamwork--helped reduce our exposure to investments that
suffered substantial losses.

How soon a sustainable recovery will occur is uncertain. But I am certain of
this: Since we've demonstrated a consistent ability to execute solid,
long-term investment strategies and the discipline to remain focused during
times of volatility or shifts in the markets. We've stayed true to our
principles, especially our belief that your success is the ultimate measure of
our success.

Thank you for your continued confidence in us.

Sincerely,
