Dear Shareholders,

Each RiverSource State Tax-Exempt Fund underperformed its respective Lehman
benchmark index, but outperformed or closely tracked its respective Lipper peer
group for the months ended Aug. All Fund returns reported are for
Class A shares, excluding sales charge, for the months ended Aug.
All Lipper categories represent the respective fund's peer group.

RIVERSOURCE CALIFORNIA TAX-EXEMPT FUND rose The Fund underperformed
the Lehman Brothers California Plus Year Municipal Bond Index, which
advanced The Fund outperformed the Lipper California Municipal Debt
Funds Index, which gained for the same period.

RIVERSOURCE MINNESOTA TAX-EXEMPT FUND gained The Fund underperformed
the Lehman Brothers Minnesota Plus Year Enhanced Municipal Bond Index,
which rose but outperformed the Lipper Minnesota Municipal Debt Funds
Index, which was up for the same period.

RIVERSOURCE NEW YORK TAX-EXEMPT FUND advanced The Fund underperformed
the Lehman Brothers New York Plus Year Municipal Bond Index, which rose
However, the Fund closely tracked the Lipper New York Municipal Debt
Funds Index, which gained for the same period.

A broad barometer applicable to each of the Funds, the Lehman Brothers Municipal
Bond Index, was up for the same period.


- --------------------------------------------------------------------------------
RIVERSOURCE STATE TAX-EXEMPT FUNDS -- ANNUAL REPORT



MANAGER COMMENTARY (continued) -------------------------------------------------


RiverSource California Tax-Exempt Fund

QUALITY BREAKDOWN (at Aug. % of bond portfolio assets)
- ---------------------------------------------------------------------



Bond ratings apply to the underlying holdings of the Fund and not the Fund
itself. Whenever possible, the Standard and Poor's rating is used to determine
the credit quality of a security. Standard and Poor's rates the creditworthiness
of corporate bonds, with categories, ranging from AAA (highest) to D
(lowest). Ratings from AA to CCC may be modified by the addition of a plus (+)
or minus (-) sign to show relative standing within the major rating categories.
If Standard and Poor's doesn't rate a security, then Moody's rating is used.
RiverSource Investments, LLC, the Fund's investment manager, rates a security
using an internal rating system when Moody's doesn't provide a rating. Ratings
for of the bond portfolio assets were determined through internal analysis.

RiverSource Minnesota Tax-Exempt Fund

QUALITY BREAKDOWN (at Aug. % of bond portfolio assets)
- ---------------------------------------------------------------------



Bond ratings apply to the underlying holdings of the Fund and not the Fund
itself. Whenever possible, the Standard and Poor's rating is used to determine
the credit quality of a security. Standard and Poor's rates the creditworthiness
of corporate bonds, with categories, ranging from AAA (highest) to D
(lowest). Ratings from AA to CCC may be modified by the addition of a plus (+)
or minus (-) sign to show relative standing within the major rating categories.
If Standard and Poor's doesn't rate a security, then Moody's rating is used.
RiverSource Investments, LLC, the Fund's investment manager, rates a security
using an internal rating system when Moody's doesn't provide a rating. Ratings
for of the bond portfolio assets were determined through internal analysis.

RiverSource New York Tax-Exempt Fund

QUALITY BREAKDOWN (at Aug. % of bond portfolio assets)
- ---------------------------------------------------------------------



Bond ratings apply to the underlying holdings of the Fund and not the Fund
itself. Whenever possible, the Standard and Poor's rating is used to determine
the credit quality of a security. Standard and Poor's rates the creditworthiness
of corporate bonds, with categories, ranging from AAA (highest) to D
(lowest). Ratings from AA to CCC may be modified by the addition of a plus (+)
or minus (-) sign to show relative standing within the major rating categories.
If Standard and Poor's doesn't rate a security, then Moody's rating is used.
RiverSource Investments, LLC, the Fund's investment manager, rates a security
using an internal rating system when Moody's doesn't provide a rating. Ratings
for of the bond portfolio assets were determined through internal analysis.


- --------------------------------------------------------------------------------
RIVERSOURCE STATE TAX-EXEMPT FUNDS -- ANNUAL REPORT



- --------------------------------------------------------------------------------




SIGNIFICANT PERFORMANCE FACTORS
The tax-exempt fixed income market produced positive returns but underperformed
the taxable fixed income market for the annual period, as credit market turmoil,
subprime mortgage market concerns and fears regarding liquidity spilled over
into the municipal market. Through February a flight to quality into
Treasuries ensued as investors grew increasingly risk averse. Investors also
grew concerned that the main bond insurance companies might be challenged to
maintain their AAA ratings. Further, liquidity evaporated, as hedge funds and
other leveraged municipal participants were forced to sell assets in an already
distressed market to obtain cash for margin calls. Plus, balance sheet pressure
for several Wall Street firms caused a lack of support and failure of the
auction rate securities market during the period. February was the worst
month in the history of municipal bond market performance, with the ratio of
municipal bond yields to Treasury yields across the yield curve, or spectrum of
maturities, reaching record high levels.

Then, as new, nontraditional buyers came into the market seeking to take
advantage of weakness, demand increased, which calmed unwarranted fears of
excess new issue supply. As a result, the ratio of municipal bond yields to
Treasury yields was driven back down toward historical averages. The tax-exempt
fixed income market rallied significantly from March through May, notably
outperforming Treasuries during these months. Indeed, March through May
generated the best relative performance for the tax-exempt fixed income market
since

In June, the ratio of municipal bond yields to Treasury yields rose amidst
several bond insurance company downgrades, investment bank balance sheet
deterioration and heightened inflation pressures. The tax-exempt bond market
rallied until mid July, when FSA, a bond insurance company, was put on "negative
watch" by Moody's. As the market weakened, more nontraditional buyers returned,
serving as the catalyst for positive performance in August. By the end of
August, the ratio of municipal bond yields to Treasury yields had been
driven back down to approximately having reached triple digits just months
earlier. Interestingly, only of new municipal bonds issued in August
were insured, compared with in August indicating the dramatically
declining use of insurance within the tax-exempt bond market.


- --------------------------------------------------------------------------------
RIVERSOURCE STATE TAX-EXEMPT FUNDS -- ANNUAL REPORT



MANAGER COMMENTARY (continued) -------------------------------------------------


For the annual period overall, short-term tax-exempt yields declined, as the
Federal Reserve (the Fed) sought to strengthen liquidity and calm market fears
by cutting the targeted federal funds rate seven times during the period by a
total of Demand from money market funds increased as well. Longer-dated
municipal bond yields rose. All told, the slope of the tax-exempt bond yield
curve grew steeper over the annual period.

As long-term municipal bond yields rose and short-term municipal bond yields
fell during the annual period, each of the RiverSource State Tax-Exempt Funds
benefited from our bias toward a steeper yield curve, which we implemented in
December Another contributing factor to each of the three Funds'
performance during the period was duration positioning. The Funds maintained a
longer duration than their respective Lehman benchmark indexes for most of the
annual period, which helped as short-term rates declined. Duration is a measure
of a fund's sensitivity to changes in interest rates. Further boosting the three
Funds' results relative to their respective peers was having only modest
exposure to high yield and non-investment grade municipal bonds, which
underperformed during the annual period.

A sizable allocation to pre-refunded* bonds, which are higher quality
securities, helped RiverSource California Tax-Exempt Fund. RiverSource Minnesota
Tax-Exempt Fund and RiverSource New York Tax-Exempt Fund, however, had only
modest exposure to pre-refunded bonds, which detracted from returns.

Significant exposure to health care bonds, especially BBB-rated hospital
credits, detracted from each of the three RiverSource State Tax-Exempt Funds, as
these securities were under pressure during most of the annual period. Among
these pressures were potential budget cuts to Medicaid and the challenges of a
slowing economy. RiverSource California Tax-Exempt Fund and RiverSource New York
Tax-Exempt Fund were hurt by significant exposure to municipal bonds subject to
the Alternative Minimum Tax (AMT) and to non-enhanced municipal tobacco bonds
because both categories lagged.


- --------------------------------------------------------------------------------
RIVERSOURCE STATE TAX-EXEMPT FUNDS -- ANNUAL REPORT



- --------------------------------------------------------------------------------


Investments in non-rated special district land development
bonds, commonly known as "dirt deals," further detracted from
RiverSource California Tax-Exempt Fund's results, as these bonds
were impacted by the especially severe housing market slowdown
in that state. A sizable allocation to AA-rated prepaid natural
gas municipal bonds also hurt this Fund's performance. These
bonds, which carry the backing of major investment firms, were
impacted by concerns regarding these institutions and their
exposure to subprime mortgages. RiverSource New York Tax-Exempt
Fund and RiverSource Minnesota Tax-Exempt Fund were both hurt
during the annual period by their holdings in single-family
housing bonds.

A significant allocation to bonds rated A and BBB across a
variety of sectors detracted from all three RiverSource State
Tax-Exempt Funds' performance, as spreads, or the difference in
yields between these securities and Treasuries, widened over the
annual period.

CHANGES TO THE FUNDS' PORTFOLIOS
Overall, we sought to upgrade quality in the Funds' portfolios
during the annual period. In implementing this strategy, we
increased all three Funds' positions in investment grade
essential service revenue bonds, such as public power, water and
sewer issues. In RiverSource California Tax-Exempt Fund and
RiverSource New York Tax-Exempt Fund, we also increased exposure
to higher quality school district general obligation bonds. We
feel that both the utilities and education sectors may enable
the Funds to potentially pick up incremental yield without
taking on significant risk, as they tend to be less economically
sensitive.



We intend to use any near-term weakness in the tax-exempt bond market as a
buying opportunity, focusing on high quality issues like education and
essential service revenue sector bonds.






- --------------------------------------------------------------------------------
RIVERSOURCE STATE TAX-EXEMPT FUNDS -- ANNUAL REPORT



MANAGER COMMENTARY (continued) -------------------------------------------------


In RiverSource California Tax-Exempt Fund and RiverSource New York Tax-Exempt
Fund, we correspondingly reduced exposure to the following: non-enhanced
municipal tobacco bonds, transportation municipal bonds, Commonwealth of Puerto
Rico municipal bonds and tender option bond programs.** We also reduced
RiverSource California Tax-Exempt Fund's allocations to prepaid natural gas
municipal bonds and to state general obligation bonds given our view that
spreads on these bonds would widen further over the near term. In RiverSource
New York Tax-Exempt Fund, we reduced exposure to New York City general
obligation bonds due primarily to the ongoing concerns Wall Street faces.

We reduced each of the Funds' duration slightly during the annual period but
kept duration longer than each Funds' respective Lehman benchmark index.

OUR FUTURE STRATEGY
At the end of August we viewed the longer-term prospects for the tax-
exempt bond market as attractive. However, for the near term, we believe the
tax-exempt bond market may be under pressure, as still-elevated levels of new
issue supply, the unwinding of tender option bond programs, the dysfunction of
the auction rate securities markets and headlines about bond insurance companies
remain ongoing concerns. We intend to use any near-term weakness in the tax-
exempt bond market as a buying opportunity, focusing on high quality issues like
education and essential service revenue sector bonds.

We expect to maintain a duration that is modestly longer than its respective
Lehman benchmark index. We also believe that the tax-exempt yield curve may grow
a bit steeper but will basically remain within its current range.

Longer term, we remain quite constructive on the tax-exempt bond market for
several reasons. First, we believe federal income taxes will go up regardless of
the outcome of the November presidential election. As a result, we expect
the appeal of municipal bond funds to increase. Second, with less reliance on
bond insurance companies, we anticipate an enhanced focus on the fundamentals of
individual municipal bonds. It remains our ongoing policy to focus on the
underlying credit quality and fundamentals of all municipal securities held in
the RiverSource State Tax-Exempt Funds, including those covered by bond
insurance, as we make investment decisions.


- --------------------------------------------------------------------------------
RIVERSOURCE STATE TAX-EXEMPT FUNDS -- ANNUAL REPORT



- --------------------------------------------------------------------------------

Each Fund's emphasis continues to be on generating a high level of income
generally exempt from federal income tax as well as from respective state and
local taxes.

Catherine Stienstra
Portfolio Manager
