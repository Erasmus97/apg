Dear Fellow Shareholder,
 
As stewards of our customers’ savings, the management team and Directors of the Davis New York Venture Fund recognize the importance of candid, thorough, and regular communication with our shareholders. In our Annual and Semi-Annual Reports, we include all of the required quantitative information, such as audited financial statements, detailed footnotes, performance reports, fund holdings, and performance attribution. Also included is a list of positions opened and closed.
 
In addition, we produce a Quarterly Review. In this Review, we give a more qualitative perspective on fund performance, discuss our thoughts on individual holdings, and share our investment outlook. You may obtain a copy of the current Quarterly Review either on our website, www.davisfunds.com, or by calling 1-800-279-0279.
 
Sincerely,


