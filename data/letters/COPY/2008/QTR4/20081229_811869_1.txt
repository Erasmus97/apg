Dear Shareholder:
Economic and Market Review Stocks and most types of bonds generally posted negative returns during the months ended
Credit market troubles that started in sub-prime mortgages spread far and wide during the period, culminating in a global liquidity crisis that affected many of the worlds investment markets. This led to major policy adjustments by
central bankers around the globe as they attempted to facilitate the availability of credit and liquidity to the markets. Over the period, the U.S. Federal Reserve lowered targeted short-term rates by as heightened risks in both the real
economy and the financial markets became apparent. U.S. Economy
The nations gross domestic product shrank during part of the reporting period and grew only modestly the
remainder of the time, as tight credit, declining housing and investment values, rising unemployment and somber economic news weighed on consumers. GDP growth increased from a annual rate in the fourth quarter of to in the first
quarter, rose to in the second quarter, and then declined to in the third quarter (the latter figure according to advanced
The U.S. housing market malaise continued to curb economic activity. Weak sales, rising
inventories, falling prices and much tighter lending standards caused the imbalances in the housing market to worsen over the period. Sales of existing homes nationwide rose just to units annually from September to
September For the same period, the average sale price of an existing home fell to Two years earlier, national sales totaled units annually with an average price of
As the economy cooled, job
losses began to mount during the period. Employers cut jobs in October following declines of in September and in August. For the first months of employment fell by jobs, bringing the
unemployment rate to in October. That was the highest unemployment rate since March During the reporting period, the number of unemployed persons increased by and the unemployment rate rose by
Inflation&amp; Monetary Policy
Inflation indicators generally eased during the period. The Consumer Price Index (CPI) rose at a annual rate for the months ended compared
with a rate for all of The index for energy, which rose in all of advanced at an annual rate during the period. Excluding the volatile prices of food and energy, the core CPI advanced at a annual rate
during the period, compared to a increase for all of Notably, inflation appeared to abate sharply late in the period as credit market troubles
intensified and economic activity slowed. The CPI decreased in October largest decline on recordfollowing very little change in September and August. The energy index fell in October after declining in September
and in August. After returning to a rate-easing mode in September of the Federal Reserve Open Market Committee (FOMC) cut interest rates
repeatedly during the period, with the federal funds target rate dropping from on to on At their meeting, policymakers said they expected inflation to moderate in coming
quarters but that downside risks to growth remained. Equity Performance
Many stock indexes set record highs in early October just prior to this reporting period. For much of the following year, however, prices pulled back sharplyfor some indexes to five-year lows in October
to worries in the credit, housing, job and consumer markets and the growing potential for a severe recession.
Small-company stocks outperformed large-company issues during the period. The Russell Index of small-company stocks posted a total return,
while the S&amp;P Index of large-company stocks recorded a return. Value stocks modestly outperformed growth stocks during the period. The Russell Value Index returned
- while the Russell Growth Index posted a return of Major market foreign
stocks underperformed most domestic issues in dollar terms. The Morgan Stanley Capital International Europe, Australasia, Far East (EAFE) Index posted a total return in dollar terms.
Fixed Income Performance The worsening credit crisis hurt all areas
of the bond market except U.S. Treasury securities during the period. An investor flight to safety pushed U.S. Treasury bond prices up, driving Treasury yields down. Mortgage- and asset-backed bonds, municipals, high-yield bonds, bank loans and
corporate bonds all suffered price declines as liquidity drained out of the market. Late in the period, the situation intensified with the failure and subsequent federal takeover or forced sale of a number of large financial institutions. Fannie
Mae, Freddie Mac, Lehman Brothers, Washington Mutual, Wachovia, Merrill Lynch and AIG all were casualties of the crisis that overtook the financial industry.
The yield curve steepened during the period as Treasury yields fell especially sharply in shorter maturity lengths. The six-month Treasury yield fell from to the five-year yield declined from to the yield
fell from to and the Treasury yield declined from to





Table of Contents

The Barclays Capital (formerly Lehman Brothers) Aggregate Bond Index of the broad U.S. bond market posted a total
return for the period, while the Barclays Capital Government/Corporate Year Bond Index registered a total return. The Barclays Capital Municipal Bond Index posted a total return. Below-investment-grade corporate bonds were
among the weakest performers, with the Barclays Capital U.S. Corporate High Yield Bond Index registering a total return. Outlook
Economic activity will likely remain sluggish at least until Eventually, the unprecedented amount of government stimulus in the
systemin the form of bailouts and other liquidity programsshould begin to spur faster growth. Until then, the Federal Reserve will likely keep short-term interest rates low.
The mortgage crisis that started in has spread from the consumer through the banking system and is now poised to impact the broader economy. Rising unemployment, very weak consumer spending, and business spending
cutbacks are now the primary risks to the economy. The support from exports that U.S. corporations have enjoyed is also now in jeopardy as global growth slows and the U.S. dollar strengthens. Helping to offset these risks somewhat, the U.S.
government and governments around the world have initiated financial system rescue plans that may begin to show signs of returning some liquidity to the credit markets.
On a Personal Note My colleague, Pam Moret, has recently taken on a new role at Thrivent Financial and will no
longer serve as the President of Thrivent Mutual Funds. I, along with all of us at Thrivent Mutual Funds, want to thank Pam for her years of dedicated service to our shareholders. Her tenure saw tremendous improvement in investment performance,
innovative product design and lower expenses for our shareholders. Her leadership and integrity set a standard for the position. Its a standard I will work to build upon as I assume the position of President of Thrivent Mutual Funds, in
addition to maintaining my current role as Chief Investment Officer of Thrivent Asset Management. I look forward to using this forum in future
publications to share thoughts and insight on the markets and investment strategies, along with updates on your Thrivent Mutual Funds. Until then, thank you for continuing to turn to us for your financial options.






Sincerely,
