Dear Shareholder,
Economic growth in the U.S. was mixed during the reporting period ended Looking
back, third quarter U.S. gross domestic product (GDP)i growth was a very strong However, continued weakness in the housing market, an ongoing credit crunch and soaring oil and food prices then took their toll on the
economy as fourth quarter GDP declined The economy then expanded during the first quarter of and the preliminary estimate for second quarter GDP growth was In recent months, the economy was supported by strong exports and
consumer spending, the latter of which was aided by the governments tax rebate checks. While the economy may not fall into a recession, it is a
moot point for many Americans, as the job market continues to weaken and energy and food prices remain elevated. In terms of the employment picture, the U.S. Department of Labor reported that payroll employment declined in each of the first seven
months of and the unemployment rate rose to in July, its highest level since March After oil reached a record a barrel on it fell to as of which represented an approximate
increase from the price as of Ongoing issues related to the housing and
subprime mortgage markets and seizing credit markets prompted the Federal Reserve Board (Fed)ii to take aggressive and, in some cases, unprecedented actions. Beginning in September the Fed reduced the federal funds
rateiii from to This marked the first such reduction since June The Fed then reduced the federal funds rate on six additional occasions through April bringing the federal funds rate to The Fed then shifted
gears in the face of mounting inflationary prices and a weakening U.S. dollar. At its latest meetings that took place in June and August (after the reporting period ended), the Fed held rates steady. In conjunction with its August meeting, the Fed
stated: Economic activity expanded in the second quarter, partly reflecting growth in consumer spending and exports. However, labor markets have softened further and financial markets remain under considerable stress. Inflation has been
high, spurred by the earlier increases in the prices of energy and some other commodities, and some









Legg Mason Partners Municipal High Income Fund

I









Letter from the chairman continued

indicators of inflation expectations have been elevated. The Committee expects inflation to moderate later this year and next year, but the inflation
outlook remains highly uncertain. In addition to the interest rate cuts, the Fed took several actions to improve liquidity in the credit
markets. In March the Fed established a new lending program allowing certain brokerage firms, known as primary dealers, to also borrow from its discount window. The Fed also increased the maximum term for discount window loans from to
days. Then, in mid-March, the Fed played a major role in facilitating the purchase of Bear Stearns by JPMorgan Chase. During the reporting
period ended July both short- and long-term Treasury yields experienced periods of extreme volatility. Investors were initially focused on the subprime segment of the mortgage-backed market. These concerns broadened, however, to include a
wide range of financial institutions and markets. As a result, other fixed-income instruments also experienced increased price volatility. This turmoil triggered several flights to quality, causing Treasury yields to move lower (and
their prices higher), while riskier segments of the market saw their yields move higher (and their prices lower). Treasury yields then rose in April, May and early June as oil prices hit record levels. However, an additional credit crunch in
mid-June resulted in another flight to quality, with Treasury yields again moving lower. Overall, during the months ended July two-year Treasury yields fell from to Over the same time frame, Treasury yields moved
from to The municipal bond market underperformed its taxable bond counterpart
over the months ended July Over that period, the Lehman Brothers Municipal Bond Indexiv and the Lehman Brothers U.S. Aggregate Indexv returned and respectively. During the reporting period, the
municipal market was adversely affected by increased investor risk aversion and fears that an economic recession would negatively impact municipalities, as they would generate less tax revenues. In addition, several large bond insurers experienced
rating downgrades due to concerns that they no longer had enough capital to guarantee billions of dollars in debt due to the fallout from the subprime mortgage crisis.
Please read on for a more detailed look at prevailing economic and market conditions during the Funds reporting period and to learn how those conditions have affected Fund performance.









II

Legg Mason Partners Municipal High Income Fund









Information about your fund
As you may be aware, several issues in the mutual fund industry have come under the scrutiny of federal and state regulators. Affiliates of the
Funds manager have, in recent years, received requests for information from various government regulators regarding market timing, late trading, fees, and other mutual fund issues in connection with various investigations. The regulators
appear to be examining, among other things, the Funds response to market timing and shareholder exchange activity, including compliance with prospectus disclosure related to these subjects. The Fund is not in a position to predict the outcome
of these requests andinvestigations. Important information with regard to recent regulatory developments that may affect the Fund is contained
in the Notes to Financial Statements included in this report. As always, thank you for your confidence in our stewardship of your assets. We look
forward to helping you meet your financial goals. Sincerely,
