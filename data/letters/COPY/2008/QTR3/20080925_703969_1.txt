Dear Fellow Shareholder:

The past months have presented the economy with a growing set of challenges, and financial markets have responded with losses across a wide range of sectors globally. It is always unsettling to see the markets and
ones investment returns declining. Times like these are a reminder of why it is important to keep a long-term perspective, to ensure that your portfolio is well diversified, and to seek the counsel of your financial representative.

At Putnam, we continually strive to offer the best investment returns, innovative products, and award-winning service to our shareholders. In keeping with this tradition, we are pleased to announce that Robert L. Reynolds,
a well-known leader and visionary in the mutual fund industry, has joined the Putnam leadership team as President and Chief Executive Officer of Putnam Investments, effective July Charles E. Haldeman, Jr., former President and CEO, has
taken on the role of Chairman of Putnam Investment Management, LLC, the firms fund management company.

Mr. Reynolds brings to Putnam substantial industry experience and an outstanding record of success, including serving as Vice Chairman and Chief Operating Officer at Fidelity Investments from to We look forward
to working with Bob as we continue our goal to position Putnam to exceed our shareholders expectations.

We would also like to take this opportunity to welcome new shareholders to the fund and to thank all of our investors for your continued confidence in Putnam.

Respectfully yours,
