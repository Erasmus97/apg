Dear Fellow Shareholder:

If you've read any of our previous shareholder communications, you know
that we try to skip the financial jargon and speak in terms with which we
believe the average person can relate and understand.

The recent movements in the stock market got us to thinking about how risk
tolerance mirrors choosing rides at an amusement park.

In our younger days, we always enjoyed the thrill of roller coasters. If
you're like us, you can vividly remember holding your breath with anticipation
as you climbed and climbed to the top, never knowing just when you would finally
get there. Then, just as you began to relax, the roller coaster would begin its
decline. The ride to the bottom seemed like it would never end as you laughed,
screamed, and implored the heavens to let you make it through alive.

Much of what has transpired in the stock market over the last year or so
has reminded us of our younger roller coaster riding days. Of course, the ride
to the "top" of the market is always exhilarating. But, as we've matured, we've
come to realize that roller coasters have lost some of their allure.

We no longer find adrenaline-pumping adventures so attractive. While you
can be brought to dizzying heights, some dramatic lows are also inevitable. For
those of us who don't have the stomach for this type of volatility, especially
with our investment money, a more stable alternative may be the ticket.

We like to think of investing in a tax-free municipal bond fund, such as
Tax-Free Trust of Arizona, as more like riding the swings at a county fair.
There is definitely some up and down movement, but you usually don't go quite
that far. In general, you know what to expect next and the ride is fairly
pleasant.


NOT A PART OF THE ANNUAL REPORT



Of course, we believe that there is a place in everyone's life for roller
coasters. But, as one matures, it might be prudent to have the more stable
predictable swings begin to play a larger role. After all, what good are highs
if they are often followed by lows?

Sincerely,
