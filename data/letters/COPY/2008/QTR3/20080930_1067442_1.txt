Dear Shareholder: 
 
On the following pages, you’ll find your annual report for the Target Conservative Allocation
Fund. 
 
Target Asset Allocation Funds are managed by institutional-quality asset managers
selected, matched, and monitored by a research team from Prudential Investments LLC. Portions of the Funds’ assets are assigned to carefully chosen asset managers, with the allocations actively managed on the basis of our projections for the
financial markets and the managers’ individual strengths. 
 
We believe our Target
Conservative Allocation Fund will help you to achieve broad, actively managed diversification at a targeted risk/return balance with a single investment purchase. We appreciate your continued confidence in us. Keep in mind that diversification and
asset allocation do not assure a profit or protect against a loss in a declining market. 
 
Sincerely, 


Dear Shareholder: 
 
On the following pages, you’ll find your annual report for the Target Moderate Allocation Fund.

 
Target Asset Allocation Funds are managed by institutional-quality asset managers
selected, matched, and monitored by a research team from Prudential Investments LLC. Portions of the Funds’ assets are assigned to carefully chosen asset managers, with the allocations actively managed on the basis of our projections for the
financial markets and the managers’ individual strengths. 
 
We believe our Target
Moderate Allocation Fund will help you to achieve broad, actively managed diversification at a targeted risk/return balance with a single investment purchase. We appreciate your continued confidence in us. Keep in mind that diversification and asset
allocation do not assure a profit or protect against a loss in a declining market. 
 
Sincerely, 


Dear Shareholder: 
 
On the following pages, you’ll find your annual report for the Target Growth Allocation Fund.

 
Target Asset Allocation Funds are managed by institutional-quality asset managers
selected, matched, and monitored by a research team from Prudential Investments LLC. Portions of the Funds’ assets are assigned to carefully chosen asset managers, with the allocations actively managed on the basis of our projections for the
financial markets and the managers’ individual strengths. 
 
We believe our Target
Growth Allocation Fund will help you to achieve broad, actively managed diversification at a targeted risk/return balance with a single investment purchase. We appreciate your continued confidence in us. Keep in mind that diversification and asset
allocation do not assure a profit or protect against a loss in a declining market. 
 
Sincerely, 


