Dear Shareholders,
RiverSource Short Duration U.S. Government Fund (the Fund) Class A shares rose
(excluding sales charge) for the months ended May The Fund
underperformed its benchmark, the Lehman Brothers Year Government Index
(Lehman Index), which gained for the same period. The Fund also
underperformed the Lipper Short U.S. Government Funds Index, representing the
Fund's peer group, which advanced during the same time frame.

SIGNIFICANT PERFORMANCE FACTORS
The annual period ended May was marked by chaos in the financial
markets and volatile interest rates as a result of the subprime mortgage
crisis and its aftermath. A classic flight to quality seized the U.S. fixed
income market as investor aversion to risk remained elevated. During the last
few months of the annual period, inflation became an additional concern for the
fixed income market, with oil, gasoline and food prices reaching record highs.
The Federal Reserve Board (the Fed) responded to the fallout from the chaos and
concern by taking a variety of strong and decisive actions. The Fed embarked on
an aggressive interest rate cutting campaign beginning in September to
address the tighter credit markets and the ongoing downturn in the housing
market. In all, the Fed cut the targeted federal funds rate a total of basis
points The Fed also introduced several new tools to provide liquidity
to the financial system at large and the mortgage market in particular.

For the annual period overall, interest rates fell significantly, as investors
fled riskier assets for the relative safety of U.S. Treasuries, pushing yields
sharply lower. While yields moved lower across the yield curve, they did so more
significantly in the shorter segments than at the long-term end,

SECTOR DIVERSIFICATION (at May % of portfolio assets)
- -----------------------------------------------------------------


Of the is due to forward commitment mortgage-backed securities
activity. Short-term securities are held as collateral for these
commitments.
Cash & Cash
Of the is due to security lending activity and is the
Fund's cash equivalent position.

- --------------------------------------------------------------------------------

RIVERSOURCE SHORT DURATION U.S. GOVERNMENT FUND -- ANNUAL REPORT


MANAGER COMMENTARY (continued) -------------------------------------------------

causing the U.S. Treasury yield curve to steepen over the annual period. Away
from Treasuries, other fixed income sectors posted positive but comparatively
weaker results.

The primary contributor to the Fund's performance was its exposure to Treasury
Inflation Protected Securities, or TIPS. TIPS outperformed nominal Treasury
securities, or non-inflation protected Treasury securities, as inflationary
pressures grew and actual inflation came in higher than expected during the
period.

Also, issue selection within mortgage-backed securities boosted returns,
particularly the Fund's emphasis on premium coupon securities. Higher coupon
mortgage-backed securities outperformed lower coupon mortgage-backed securities
for the period.

Still, these positives were more than outweighed by the impact of yield curve
positioning, which detracted from performance over the period. As mentioned,
while U.S. Treasury yields moved lower across the yield curve, they did so more
significantly in the shorter segments than at the long-term end, causing the
U.S. Treasury yield curve to steepen over the annual period. The Fund, however,
had a more modest exposure to the year portion of the yield curve, which
outperformed, and a more sizable allocation to the segment of the yield
curve, which lagged.

The Fund's sizable exposure to mortgage-backed securities and agency securities
also detracted from results, as these spread, or non-Treasury sectors of the
fixed income market, posted positive returns but underperformed Treasuries
during the annual period. An exposure to non-

QUALITY BREAKDOWN
(at May % of portfolio assets excluding cash equivalents and equities)
- -----------------------------------------------------------------


Bond ratings apply to the underlying holdings of the Fund and not the Fund
itself. Whenever possible, the Standard and Poor's rating is used to determine
the credit quality of a security. Standard and Poor's rates the creditworthiness
of corporate bonds, with categories, ranging from AAA (highest) to D
(lowest). Ratings from AA to CCC may be modified by the addition of a plus (+)
or minus (-) sign to show relative standing within the major rating categories.
If Standard and Poor's doesn't rate a security, then Moody's rating is used.
RiverSource Investments, LLC, the Fund's investment manager, rates a security
using an internal rating system when Moody's doesn't provide a rating.

- --------------------------------------------------------------------------------

RIVERSOURCE SHORT DURATION U.S. GOVERNMENT FUND -- ANNUAL REPORT


- --------------------------------------------------------------------------------

As always, our strategy is to provide added portfolio value with a moderate
amount of risk.

agency mortgage-backed securities also detracted, as
this sector of the fixed income market was
especially hurt due to concerns regarding credit
broadly, and residential credit in particular, grew.

CHANGES TO THE FUND'S PORTFOLIO
During the first half of the annual period, we
shifted to a somewhat more aggressive strategy
within the Fund's allocation to both mortgage-
backed securities and agency securities, as spreads,
or the difference in yields between these securities
and U.S. Treasuries, widened and valuations reached
compelling levels. In the last months of the fiscal
year, we increased the Fund's position in TIPS, as
actual and anticipated inflation pressures rose. We
strategically adjusted the Fund's duration relative
to the Lehman Index throughout the annual period, as
market conditions shifted. The Fund ended the period
with a duration slightly longer than the Lehman
Index. The Fund's portfolio turnover rate for the
period was

OUR FUTURE STRATEGY
At the end of May, the market for high quality
spread, or non-Treasury, securities finally appeared
to be improving, and even lower quality bonds had
rebounded from their lows. While economic growth
over the coming months may not be as strong as
earlier in the cycle, we expect the U.S. economy to
narrowly avoid recession this year, as the economic
stimulus package passed by Congress should help keep
economic growth positive. However, we do believe
that low interest rates and a weakened currency have
combined to

* A significant portion of the turnover was the
result of rolling-maturity mortgage securities,
processing of prepayments and opportunistic
changes we made at the margin in response to
valuations or market developments.

- --------------------------------------------------------------------------------

RIVERSOURCE SHORT DURATION U.S. GOVERNMENT FUND -- ANNUAL REPORT


MANAGER COMMENTARY (continued) -------------------------------------------------

form an inflation problem that may weigh heavily on consumers and financial
markets in the second half of this year and into To accommodate for this
pressure, we believe that the Fed will eventually have to increase interest
rates, and Treasury yields should continue to drift higher.

In this environment, we intend to maintain the Fund's exposure to TIPS. We also
believe the most compelling opportunities at the end of the annual period were
in high-quality spread products, as they were priced at historically wide
levels. We therefore view the Fund as well-positioned for a recovery in high
quality spread, or non-Treasury, sectors of the fixed income market. Having
begun in the last months of the annual period, we expect this recovery to
continue over the months ahead, as fears ultimately subside and liquidity
returns to the markets. Among its mortgage-backed holdings, we intend to
maintain the Fund's premium coupon bias, as prepayments are expected to remain
slow. We also view select pockets of non-agency mortgage-backed securities as
attractively priced, though our focus is on those securities within this sector
with superior credit enhancements. We intend to maintain the Fund's current
duration positioning for the near term. As always, our strategy is to provide
added portfolio value with a moderate amount of risk. Quality issues and
security selection remain a priority as we continue to seek attractive buying
opportunities.


Of the is due to forward commitment mortgage-backed securities
activity. Short-term securities are held as collateral for these
commitments.
Cash & Cash Equivalents.

- --------------------------------------------------------------------------------

RIVERSOURCE U.S. GOVERNMENT MORTGAGE FUND -- ANNUAL REPORT


MANAGER COMMENTARY (continued) -------------------------------------------------

became evident. What had started in the mortgage sector then spread to all
corners of the bond market, resulting in an en masse flight to quality.
Heightened fears, in turn, dramatically reduced liquidity. In response, the
Federal Reserve Board (the Fed) lowered interest rates an aggressive basis
points over the period and made other changes designed to support
liquidity in the banking and financial systems. Securities with credit
risk - especially those with mortgage-related credit risk - faced significant
selling pressure during the July and August "credit panic." Although the market
stabilized temporarily in the fall, November brought a fresh round of panic, as
the market was spooked by increasing talk of an impending recession. The
mortgage securities sector faced a period of deleveraging and forced selling of
assets. At the same time, there were not enough buyers, as residential credit
concerns not seen before came to the surface, and investors tried to figure out
where fair value was. Following months of extreme dislocation and high levels
of volatility, the mortgage market finally rebounded beginning in mid-March,
triggered by the Fed's non-traditional measures and willingness to provide
liquidity to the markets. Mortgage securities continued to perform well through
May.
For the annual period overall, interest rates fell significantly, as investors
fled riskier assets for the relative safety of U.S. Treasuries, pushing yields
sharply lower. While yields moved lower across the yield curve, they did so more
significantly in the shorter segments than at the long-term end, causing the
U.S. Treasury yield curve to steepen over the annual period.

QUALITY BREAKDOWN
(at May % of portfolio assets excluding cash equivalents and equities)
- -----------------------------------------------------------------


Bond ratings apply to the underlying holdings of the Fund and not the Fund
itself. Whenever possible, the Standard and Poor's rating is used to determine
the credit quality of a security. Standard and Poor's rates the creditworthiness
of corporate bonds, with categories, ranging from AAA (highest) to D
(lowest). Ratings from AA to CCC may be modified by the addition of a plus (+)
or minus (-) sign to show relative standing within the major rating categories.
If Standard and Poor's doesn't rate a security, then Moody's rating is used.
RiverSource Investments, LLC, the Fund's investment manager, rates a security
using an internal rating system when Moody's doesn't provide a rating.

- --------------------------------------------------------------------------------

RIVERSOURCE U.S. GOVERNMENT MORTGAGE FUND -- ANNUAL REPORT


- --------------------------------------------------------------------------------

On the positive side, issue selection helped the Fund's results, particularly an
emphasis on premium coupon securities.

The Fund produced solid positive absolute returns
for the annual period, but underperformed the Lehman
Index. On the positive side, issue selection helped
the Fund's results, particularly an emphasis on
premium coupon securities. Higher coupon
mortgage-backed securities outperformed lower coupon
mortgage-backed securities for the period. Also,
many of the Fund's long-held specified pools of
mortgages helped its results during the period, as
interest rates declined. These specified pools added
some stability to the Fund's portfolio, given their
attractive prepayment profile. The Fund's yield
curve steepening bias, maintained throughout the
annual period, further contributed to the Fund's
performance, as short-term rates declined more than
longer-term rates.
Conversely, the Fund's duration positioning
detracted from its relative results. We maintained
the Fund's duration shorter than that of the Lehman
Index throughout the annual period. This stance
helped the Fund during the last two months of the
fiscal year when interest rates rose. However, it
hurt when interest rates declined with the
unexpectedly strong flight-to-quality during the
first months of the period. Duration is a measure
of the Fund's sensitivity to changes in interest
rates.

Another reason the Fund lagged the Lehman Index was
its significant allocation to AAA-rated super-senior
commercial mortgage-backed securities (CMBS). While
these securities performed well in April and May,
they lagged the Lehman Index for the period as a
whole. An exposure to non-agency mortgage-backed
securities also detracted; this sector of the fixed
income market was especially hurt as concerns
regarding credit broadened, and residential credit
in particular, grew.

- --------------------------------------------------------------------------------

RIVERSOURCE U.S. GOVERNMENT MORTGAGE FUND -- ANNUAL REPORT


MANAGER COMMENTARY (continued) -------------------------------------------------

CHANGES TO THE FUND'S PORTFOLIO
During the first half of the annual period, we sought to take advantage of
weakness in the mortgage market by shifting to a somewhat more aggressive
strategy within the mortgage sector, as spreads, or the difference in yields
between these securities and U.S. Treasuries, widened and valuations reached
compelling levels. We reduced the Fund's position in agency pass-through
mortgages and increased its exposure to non-agency pass-through mortgages and
CMBS. In the last months of the fiscal year, we rotated out of some CMBS and
into more non-agency pass-through mortgages, as spreads on these latter
securities were comparatively more compelling. We also lengthened the Fund's
duration a bit in May, though still maintaining a duration shorter than the
Lehman Index.

Even with these changes, we continued to emphasize higher quality issues, namely
those securities issued by government mortgage agencies, including Ginnie Mae,
Fannie Mae and Freddie Mac, and those rated AAA throughout the annual period. We
maintained our focus on higher coupon mortgage securities and emphasized
investment in more seasoned pools of mortgages. The Fund's portfolio turnover
rate for the period was

* A significant portion of the turnover was the result of rolling-maturity
mortgage securities, processing of prepayments and opportunistic changes we
made at the margin in response to valuations or market developments.

OUR FUTURE STRATEGY
At the end of May, the market for high quality spread or non-Treasury,
securities, finally appeared to be improving, and even lower quality bonds had
rebounded from their lows. While economic growth over the coming months may not
be as strong as earlier in the cycle, we expect the U.S. economy to narrowly
avoid recession this year, as the economic stimulus package passed by Congress
should help keep economic growth positive. However, we do believe that low
interest rates and a weakened currency have combined to form an inflation
problem that may weigh heavily on consumers and financial markets in the second
half of this year and into To accommodate for this pressure, we believe
that the Fed will eventually have to increase interest rates, and Treasury
yields should continue to drift higher.

- --------------------------------------------------------------------------------

RIVERSOURCE U.S. GOVERNMENT MORTGAGE FUND -- ANNUAL REPORT

- --------------------------------------------------------------------------------

In this environment, we intend to position the Fund to take advantage of the
compelling valuations we saw in the last months of the period and to participate
in the recovery of high quality spread sectors, or non-Treasury sectors. We
expect this recovery to continue over the months ahead, as fears ultimately
subside and liquidity returns to the markets. We intend to maintain the Fund's
premium coupon bias among its mortgage-backed holdings, as prepayments are
expected to remain slow. We also view select pockets of non-agency
mortgage-backed securities as attractively priced, though our focus is on those
securities within this sector with superior credit enhancements. As we expect
rates may well move higher over the coming months, we intend to maintain the
Fund's shorter-than-Lehman Index duration. As always, our strategy is to provide
added portfolio value with a moderate amount of risk. Quality issues and
security selection remain a priority as we continue to seek attractive buying
opportunities.

