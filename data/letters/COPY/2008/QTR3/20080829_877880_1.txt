Dear Fellow Shareholders:
          Equity
prices declined over the past twelve months, reflecting a weak housing market,
credit losses in the finance sector and a sharp rise in oil prices. While it
may take time for these issues to be resolved, we believe that market
volatility has created opportunities for long-term investors.
2
 
 
 
 
 
 
 
 
