Dear Fellow Shareholders, 
Fighting the trend of the
broader market averages, seven of our eleven Funds ended the June 2008 quarter with positive returns. Nine Funds beat their primary market benchmarks as detailed on the facing page (market beating returns are in green). For the full fiscal year
ending in June, the same nine Funds beat their primary market benchmark, although only Aggressive Investors 1 Fund and Aggressive Investors 2 Fund had a positive one-year return as the market tumbled in the environment of a credit crisis, inflation,
and economic pull-back. While negative returns don’t feel good, we aim to be fully invested for the long haul, so we are generally pleased when and where we are able to provide some “cushion” in a downturn. 
and
As always, we appreciate your feedback. We take your comments very seriously and regularly discuss them internally to help in managing our Funds and this company. Please
keep your ideas coming—both favorable and critical. They provide us with a vital tool helping us serve you better. 
Sincerely,


Dear Fellow Aggressive Investors 1 Fund
Shareholder, 
down 
Likewise, our Fund performed well for the full fiscal year on both an absolute and relative return basis, outperforming each of its benchmarks. For the 12-months ending June 30, 2008, the Fund rose by 3.51% while each of the indexes
closed in negative territory: S&amp;P 500 (-13.12%), Lipper Capital Appreciation Funds Index (-0.16%), and Russell 2000 Index (-16.19%). While we welcome such strong results during these shorter-term quarterly and 12-month time horizons, we strive
to achieve excellent consistent returns over the long term. We are pleased to report that Aggressive Investors 1 Fund has rewarded our shareholders with double-digit returns and outperformed each of its benchmarks over the past five and ten years as
well as since inception in August 1994 (where we have ranked first among our peers according to Lipper research as detailed below). Our strategies stay constant and consistent in both good times and bad, in both strong and weak markets, during both
“the sky is the limit” and “the sky is falling” investor environments. Such long-term results always will remain the key objective of our firm. The information below bears out our historical success in these areas. 
The table below presents our June quarter, one-year, five-year, ten-year and life-to-date financial results according to the formula required by the SEC. See the next page for a
graph of performance from inception to June 30, 2008. 
 
June Qtr.
1 Year
5 Year
10 Year
Life-to-Date
8/5/94
to 6/30/08
Aggressive Investors 1 Fund
S&P 500 Index (large companies)
Lipper Capital Appreciation Funds Index
Russell 2000 Index (small companies)
Performance figures quoted in the table above and graph below represent past performance and are no guarantee of future
results. The table above and the graph below do not reflect the deduction of taxes that a shareholder would pay on Fund distributions or the redemption of Fund shares. 
The S&amp;P 500 Index is a broad-based, unmanaged measurement of changes in stock market conditions based on the average of 500 widely held common stocks with dividends reinvested, while the Russell 2000 Index is an unmanaged, market
value weighted index, that measures performance of the 2,000 companies that are between the 1,000th and 3,000th largest in the market with dividends reinvested. The Lipper Capital Appreciation Funds Index reflects the record of the 30 largest funds
in this category, comprised of more aggressive domestic growth mutual funds, as reported by Lipper, Inc. It is not possible to invest directly in an index. Periods longer than one year are annualized. 
According to data from Lipper, Inc. as of June 30, 2008, Aggressive Investors 1 Fund ranked 33rd of 296 capital
appreciation funds for the twelve months ending June 30, 2008, 28th of 218 over the last five years, 3
 
 
 
Dear Fellow Aggressive Investors 2 Fund
Shareholder, 
down 
Likewise, our Fund outperformed all of its benchmarks for the full fiscal year on both an absolute and relative return basis. For the twelve months ending June 30, 2008, the
Fund appreciated 5.88%, while each of the indexes closed in negative territory: S&amp;P 500 (-13.12%), Lipper Capital Appreciation Funds Index (-0.16%), and Russell 2000 Index (-16.19%). While we welcome such strong results during these shorter-term
quarterly and twelve month time horizons, we strive to achieve excellent consistent returns over the long term. We are pleased to report that Aggressive Investors 2 Fund has rewarded our shareholders with double-digit returns and outperformed each
of its benchmarks over the past five years as well as since inception in October, 2001. Our strategies stay constant and consistent in both good times and bad, in both strong and weak markets, during both “the sky is the limit” and
“the sky is falling” investor environments. Such long-term results always will remain the key objective of our firm. The information below bears out our longer term success in these areas. 
The table below presents our June quarter, one-year, five-year and life-to-date financial results, according to the formula required by the SEC. See the next page for a graph of
performance from inception to June 30, 2008. 
 
10/31/01
to 6/30/08
Aggressive Investors 2 Fund
S&P 500 Index (large companies)
Lipper Capital Appreciation Funds Index
Russell 2000 Index (small companies)
Performance figures quoted in the table above and graph below represent past performance and are no guarantee of future
results. The table above and the graph below do not reflect the deduction of taxes that a shareholder would pay on Fund distributions or the redemption of Fund shares. 
The S&amp;P 500 Index is a broad-based, unmanaged measurement of changes in stock market conditions based on the average of 500 widely held common stocks with dividends reinvested, while the Russell 2000 Index is an unmanaged, market
value weighted index, that measures performance of the 2,000 companies that are between the 1,000th and 3,000th largest in the market with dividends reinvested. The Lipper Capital Appreciation Funds Index reflects the record of the 30 largest funds
in this category, comprised of more aggressive domestic growth mutual funds, as reported by Lipper, Inc. It is not possible to invest directly in an index. Periods longer than one year are annualized. 
According to data from Lipper, Inc. as of June 30, 2008, Aggressive Investors 2 Fund ranked 23rd of 296 capital appreciation funds for the twelve months ending June 30,
2008, 15th of 218 over the last five years, and 12th of 198 since inception in October, 2001. Lipper, Inc. is an independent mutual fund rating service that ranks funds in various fund categories by making comparative calculations using total
returns. 
 
 
 
Dear Fellow Ultra-Small Company Fund
Shareholder, 
increased
very 
The table below presents our June quarter, one-year, five-year, ten-year and life-to-date financial results according to the formula required by the SEC. See the next page for a graph of performance from inception. 
 
Ultra-Small Company Fund
CRSP Cap-Based Port. 10 Index
Russell 2000 Index (small companies) 
Performance figures quoted in the table above and graph below represent past performance and are no guarantee of future
results. The table above and the graph below do not reflect the deduction of taxes that a shareholder would pay on Fund distributions or the redemption of Fund shares. 
The Lipper Small-Cap Stock Funds Index is an index of small-company funds compiled by Lipper, Inc. The Russell 2000 Index is an unmanaged, market value weighted index, which measures performance of the 2,000 companies that are between
the 1,000th and 3,000th largest in the market with dividends reinvested. The CRSP Cap-Based Portfolio 10 Index is an unmanaged index of 1,780 of the smallest publicly traded U.S. stocks (with dividends reinvested), as reported by the Center for
Research on Security Prices. It is not possible to invest directly in an index. Periods longer than one year are annualized. 
According to data from Lipper, Inc.
as of June 30, 2008, Ultra-Small Company Fund ranked 66th of 97 micro-cap funds for the twelve months ending June 30, 2008, 15th of 66 over the last five years, 3rd of 38 over the last ten years, and 1st of 9 since inception in August,
1994. These long-term numbers and the graph below give two snapshots of our long-term success. Lipper, Inc. is an independent mutual fund rating service that ranks funds in various fund categories by making comparative calculations using total
returns. 
 
 
 
Dear Fellow Ultra-Small Company Market Fund Shareholder, 
In the environment of the ongoing credit crisis and market downturn, our passively-managed
Ultra-Small Company Market Fund declined 1.67% for the quarter ended June 30, 2008, providing a “cushion” of 3.86% against the decline of our primary market benchmark, the CRSP Cap Based Portfolio 10 Index, which declined 5.53%. The
smallest companies were the hardest hit during this period, and this fact is reflected in our unfavorable performance versus our other benchmarks that invest in “larger” small companies. The Russell 2000 Index of small companies increased
0.58%, and the Lipper Small-Cap Stock Funds Index increased 1.78%. Given our primary investment strategy seeking to approximate the return of the CRSP 10 Index of ultra-small companies, it was a good quarter. Of course, on an absolute return basis,
it was not. 
very 
Ultra-Small
Company Market Fund is (generally) passively managed to track the CRSP 10 Index, though it holds many fewer stocks and considers turnover, costs, and tax efficiency in trade decisions. Therefore, the Fund may lag or exceed the return of its
benchmark over any given period. The table below presents our June quarter, one-year, five-year, ten-year and life-to-date financial results according to the formula required by the SEC. See the next page for a graph of performance from inception.

 
to 6/30/08
Ultra-Small Company Market Fund
CRSP Cap-Based Port. 10 Index
Russell 2000 Index (small companies) 
Performance figures quoted in the table above and graph below represent past performance and are no guarantee of future
results. The table above and the graph below do not reflect the deduction of taxes that a shareholder would pay on Fund distributions or the redemption of Fund shares. 
The Lipper Small-Cap Stock Funds Index is an index of small-company funds compiled by Lipper, Inc. The Russell 2000 Index is an unmanaged, market value weighted index, which measures performance of the 2,000 companies that are between
the 1,000th and 3,000th largest in the market with dividends reinvested. The CRSP Cap-Based Portfolio 10 Index is an unmanaged index of 1,780 of the smallest publicly traded U.S. stocks (with dividends reinvested), as reported by the Center for
Research on Security Prices. It is not possible to invest directly in an index. Periods longer than one year are annualized. 
According to data from Lipper, Inc.
as of June 30, 2008, Ultra-Small Company Market Fund ranked 44th of 97 micro-cap funds for the twelve months ended June 30, 2008, 31st of 66 over the last five years, 11th of 38 over the last ten years, and 7th of 30 since inception in
July 1997. These long-term numbers and the graph below give two snapshots of our long-term success. Lipper, Inc. is an independent mutual fund rating service that ranks funds in various fund categories by making comparative calculations using total
returns. 
 
 
 
Dear Fellow Micro-Cap Limited Fund
Shareholder, 
For the quarter ending June 30, 2008, our Fund increased by 2.32% and outperformed our primary benchmark, the CRSP Cap-Based Portfolio 9 Index,
which actually lost 2.79% for the quarter. We beat our other two performance benchmarks as well: the Russell 2000 Index of small companies climbed 0.58% and the Lipper Small-Cap Stock Funds Index rose 1.78%. Our Fund withstood the onslaught of
negative news surrounding the economy, the financial services sector, and threats of inflation from the continued increases in food and energy prices. We have now outperformed our primary market benchmark three quarters out of the last four, and we
are quite pleased. 
For the full fiscal year ending June 30, 2008, Micro-Cap Limited Fund declined a significant 17.58%, as the economic news was especially
hard on some of the markets’ micro-cap stocks. Nevertheless, we provided a cushion of more than seven percentage points relative to our primary market benchmark of micro-cap companies, the CRSP 9 Index—exactly what we hope to do in a bear
market decline. Smaller and mid-size companies as a whole were not hit as hard; consequently, our performance lagged the Russell 2000 Index of small companies (down 16.19%) as well as the Lipper Small-Cap Stock Funds (down 12.67%). 
Stepping back from the short-term performance picture, we still lag the five-year record of our primary market benchmark by 0.7% per year, but lead it since inception by more
than five percent per year. Market and peer-beating returns over the long haul remain one of our firms’ top goals. The table below presents our June quarter, one-year, five-year, ten-year and life-to-date financial results according to the
formula required by the SEC. See the next page for a graph of performance since inception. 
 
June Qtr.
4/1/08
to 6/30/08
1 Year
7/1/07
to 6/30/08
5 Year
7/1/03
to 6/30/08
10 Year
7/1/98
to 6/30/08
Life-to-Date
Micro-Cap Limited Fund
CRSP Cap-Based Portfolio 9 Index
Lipper Small-Cap Stock Funds Index
Russell 2000 Index (small stocks) 
Performance figures quoted in the table above and graph below represent past performance and are no guarantee of future
results. The table above and the graph below do not reflect the deduction of taxes that a shareholder would pay on Fund distributions or the redemption of Fund shares. 
The Lipper Small-Cap Stock Funds Index is an index of small-company funds compiled by Lipper, Inc. The Russell 2000 Index is an unmanaged, market value weighted index, which measures performance of the 2,000 companies that are between
the 1,000th and 3,000th largest in the market with dividends reinvested. The CRSP Cap-Based Portfolio 9 Index is an unmanaged index of 644 publicly traded U.S. micro-cap stocks (with dividends reinvested), as reported by the Center for Research on
Security Prices. It is not possible to invest directly in an index. Periods longer than one year are annualized. 
According to data from Lipper, Inc., for the
period ended June 30, 2008, the Micro-Cap Limited Fund ranked 21st of 97 micro-cap funds for the last twelve months ended June 30, 2008, 53rd of 66 such funds for the last five years, 5th of 38 funds for the last 10 years and 5th of 40
funds since inception in June, 1998. Lipper, Inc. is an independent mutual fund rating service that ranks funds in various fund categories by making comparative calculations using total returns. 
 
 
 
Dear Fellow Small-Cap Growth Fund Shareholder,

For the quarter ending June 30, 2008, our Fund increased by 3.26%, outperforming our peer benchmark, the Lipper Small-Cap Growth Funds Index (up 2.50%), but
underperforming our primary market benchmark, the Russell 2000 Growth Index (up 4.47%). Small-cap growth stocks held up better than most corners of the market in the midst of an onslaught of negative economic news. It was a decent quarter on an
absolute basis, but a “mixed” (mediocre) quarter overall. 
For the full fiscal year ending June 30, 2008, Small-Cap Growth Fund declined a significant
12.87% as the economic news took its toll on small-cap stocks. Similar to the quarter, we outperformed the Lipper Small-Cap Growth Funds Index (down 13.60%), but underperformed the Russell 2000 Growth Index (down 10.83%). 
As we approach the five-year anniversary of the Fund, we continue to apply our quantitative strategies with discipline and diligence. Focusing on the long term will remain a key
objective of our firm. We look forward to reporting our first five-year returns later this year. 
The table presents our June quarter, one-year, and life-to-date
financial results according to the formula required by the SEC. See the next page for a graph of performance since inception. 
 
June Qtr.
4/1/08
to 6/30/08
1 Year
7/1/07
to 6/30/08
Life-to-Date
10/31/03
to 6/30/08
Small-Cap Growth Fund
Russell 2000 Growth Index
Lipper Small-Cap Growth Funds Index
Performance figures quoted in the table above and graph below represent past performance and are no guarantee of future
results. The table above and the graph below do not reflect the deduction of taxes that a shareholder would pay on Fund distributions or the redemption of Fund shares. 
The Russell 2000 Growth Index is an unmanaged index which consists of stocks in the Russell 2000 Index with higher price-to-book ratios and higher forecasted growth values with dividends reinvested. The Lipper Small-Cap Growth Funds
Index is an index of small-company, growth-oriented funds compiled by Lipper, Inc. It is not possible to invest directly in an index. Periods longer than one year are annualized. 
According to data from Lipper, Inc. as of June 30, 2008, Small-Cap Growth Fund ranked 270th of 604 small cap growth funds for the twelve-month period ended June 30, 2008 and 114th of 412 such funds since inception in
October, 2003. Lipper, Inc. is an independent mutual fund rating service that ranks funds in various fund categories by making comparative calculations using total returns. 
 
 
 
Dear Fellow Small-Cap Value Fund Shareholder,

For the quarter ending June 30, 2008, our Fund increased by 2.65% and outperformed our performance benchmarks by significant margins. The Russell 2000 Value
Index declined 3.55%, and the Lipper Small-Cap Stock Funds Index declined 1.89%. Posting a positive return in a down market is quite pleasing to us. 
For the full
fiscal year ending June 30, 2008, Small-Cap Value Fund declined a significant 15.37%, as the economic news took its toll on small-cap stocks. Nevertheless, we provided a cushion of more than 6% relative to our primary market benchmark and more than
4% relative to our peer benchmark, the Lipper Small-Cap Value Funds Index. As we approach the five-year anniversary of the Fund, we continue to apply our quantitative strategies with discipline and diligence. Focusing on the long term will remain a
key objective of our firm. We look forward to reporting our first five-year returns later this year. 
The table below presents our June quarter, one-year, and
life-to-date financial results according to the formula required by the SEC. See the next page for a graph of performance since inception. 
 
June Qtr.
4/1/08
to 6/30/08
1 Year
7/1/07
to 6/30/08
Life-to-Date
10/31/03
to 6/30/08
Small-Cap Value Fund
Russell 2000 Value Index
Lipper Small-Cap Value Funds Index
Performance figures quoted in the table above and graph below represent past performance and are no guarantee of future
results. The table above and the graph below do not reflect the deduction of taxes that a shareholder would pay on Fund distributions or the redemption of Fund shares. 
The Russell 2000 Value Index is an unmanaged index which consists of stocks in the Russell 2000 Index with higher price-to-book ratios and higher forecasted growth values with dividends reinvested. The Lipper Small-Cap Value Funds Index
is an index of small-company, value-oriented funds compiled by Lipper, Inc. It is not possible to invest directly in an index. Periods longer than one year are annualized. 
According to data from Lipper, Inc. as of June 30, 2008, Small-Cap Value Fund ranked 72nd of 314 small-cap value funds for the twelve-month period ended June 30, 2008 and 33rd of 211 such funds since inception in
October, 2003. Lipper, Inc. is an independent mutual fund rating service that ranks funds in various fund categories by making comparative calculations using total returns. 
 
 
 
Dear Fellow Large-Cap Growth Fund Shareholder,

For the quarter ending June 30, 2008, our Fund increased 3.54%, exceeding the returns of both our performance benchmarks. The Russell 1000 Growth Index
appreciated 1.25% and the Lipper Large-Cap Growth Funds Index appreciated 1.04%. It was a very good quarter overall. 
Likewise, our Fund outperformed its benchmarks
for the full fiscal year, though it ended the twelve-month period in negative territory. Our Fund declined by 2.50%, while the Russell 1000 Growth Index fell by 5.96% and the Lipper Large-Cap Growth Funds Index dropped by 4.21%. Providing a nice
“cushion” in a market downturn is a favorable result as we remain roughly fully invested—ready for a future market upturn. 
We apply our quantitative
strategies in a disciplined manner in both good times and bad, both strong and weak markets. Focusing on the long term will remain a key objective of our firm. The information below bears out our historical success as we approach our five-year
anniversary. 
The table below presents our June quarter, one-year, and life-to-date financial results according to the formula required by the SEC. See the next page
for a graph of performance since inception. 
 
June Qtr.
4/1/08
to 6/30/08
1 Year
7/1/07
to 6/30/08
to 6/30/08
Large-Cap Growth Fund
Russell 1000 Growth Index
Lipper Large-Cap Growth Funds Index
Performance figures quoted in the table above and graph below represent past performance and are no guarantee of future
results. The table above and the graph below do not reflect the deduction of taxes that a shareholder would pay on Fund distributions or the redemption of Fund shares. 
The Russell 1000 Growth Index is an unmanaged index which consists of stocks in the Russell 1000 Index with higher price-to-book ratios and higher forecasted growth values with dividends reinvested. The Lipper Large-Cap Growth Funds
Index is an index of large-company, growth-oriented funds compiled by Lipper, Inc. It is not possible to invest directly in an index. Periods longer than one year are annualized. 
According to data from Lipper, Inc. as of June 30, 2008, Large-Cap Growth Fund ranked 125th of 493 multi-cap growth funds for the twelve-month period ended June 30, 2008 and 153rd of 331 such funds since inception in
October, 2003. Lipper, Inc. is an independent mutual fund rating service that ranks funds in various fund categories by making comparative calculations using total returns. 
 
 
 
Dear Fellow Large-Cap Value Fund Shareholder,

Along with the large-cap value segment of the stock market, our Fund continued to suffer the ill-effects of the ongoing credit crisis and economic downturn. For the
quarter ending June 30, 2008, the Fund declined 4.22%, beating its primary benchmark, the Russell 1000 Value Index, which fell 5.31%. Our peer benchmark, however, dropped by a smaller 3.40%. While certain growth stocks experienced a bit of a
rebound during the period, investors avoided value-oriented companies. It was a mixed (mediocre to poor) quarter on a relative return basis. 
For the full fiscal
year ended June 30, 2008, Large-Cap Value Fund declined a significant 16.46%, as the economic news took its toll on value stocks. Nevertheless, we did provide some “cushion” against the downturn, outperforming both our benchmarks. The
Russell 1000 Value Index declined 18.78% and the Lipper Large-Cap Value Funds Index dropped 16.53% during the period. 
As presented in the table below, we are
pleased to report that Large-Cap Value Fund has also outperformed each of its benchmarks since inception in October 2003. We apply our quantitative strategies in a disciplined manner in both good times and bad, both strong and weak markets. Focusing
on the long term will remain a key objective of our firm. The information below bears out our historical success as we approach our five year anniversary. 
The table
below presents our June quarter, one-year, and life-to-date financial results according to the formula required by the SEC. See the next page for a graph of performance since inception. 
 
Large-Cap Value Fund
Russell 1000 Value Index
Lipper Large-Cap Value Funds Index
Performance figures quoted in the table above and graph below represent past performance and are no guarantee of future
results. The table above and the graph below do not reflect the deduction of taxes that a shareholder would pay on Fund distributions or the redemption of Fund shares. 
The Russell 1000 Value Index is an unmanaged index which consists of stocks in the Russell 1000 Index with higher price-to-book ratios and higher forecasted growth values with dividends reinvested. The Lipper Large-Cap Value Funds Index
is an index of large-company, value-oriented funds compiled by Lipper, Inc. It is not possible to invest directly in an index. Periods longer than one year are annualized. 
According to data from Lipper, Inc. as of June 30, 2008, Large-Cap Value Fund ranked 173rd of 431 multi-cap value funds for the twelve-month period ended June 30, 2008 and 58th of 262 such funds since inception in
October, 2003. Lipper, Inc. is an independent mutual fund rating service that ranks funds in various fund categories by making comparative calculations using total returns. 
 
 
 
Dear Fellow Blue Chip 35 Index Fund
Shareholder, 
For the six months ending June 30, 2008, our Fund declined sharply, 14.57%, a steeper decline than either our primary market or peer benchmark. By
comparison, the S&amp;P 500 Index was down 11.91%, and the Lipper Large-Cap Core Funds Index was down 10.80%. Our own proprietary index, the Bridgeway Ultra-Large 35 Index, declined 14.89% during the past six months, slightly more than our Fund
(even after considering our expense ratio). Accounting for the benchmark-lagging performance were the facts that a) mid-size companies strongly outperformed our ultra-large ones during the period, and b) we had slightly stronger representation among
financial stocks, some of which were among those particularly hard hit in the credit crisis. Except for the favorable operational execution against our own index, this was a poor six-month period. 
Based on the negative performance drag of the recent six-month period, our Fund declined a similar 14.28% for the fiscal year ending June 30, 2008, underperforming the
S&amp;P 500 Index by 1.16% and the Lipper Large-Cap Core Funds Index by 2.90%. 
The table below presents our six month, one-year, five-year, ten-year and
life-to-date financial results according to the formula required by the SEC. See the next page for a graph of performance from inception. We are now “dead even” with the S&amp;P 500 Index over the last ten years and lead by a bit since
inception. 
 
to
6/30/08
Blue Chip 35 Index Fund
S&P 500 Index
Bridgeway Ultra-Large 35 Index
Lipper Large-Cap Core Funds Index
Performance figures quoted in the table above and graph below represent past performance and are no guarantee of future
results. The table above and the graph below do not reflect the deduction of taxes that a shareholder would pay on Fund distributions or the redemption of Fund shares. 
The S&amp;P 500 Index is a broad-based, unmanaged measurement of changes in stock market conditions based on the average of 500 widely held common stocks with dividends reinvested. The Bridgeway Ultra-Large 35 Index is an index comprised
of very large, “blue chip” U.S. stocks, excluding tobacco; it is compiled by the adviser of the Fund. The Lipper Large-Cap Core Funds Index reflects the aggregate record of domestic large-cap core mutual funds as reported by Lipper, Inc.
It is not possible to invest directly in an index. Periods longer than one year are annualized. 
According to data from Lipper, Inc. as of June 30, 2008,
Blue-Chip 35 Index Fund ranked 565th of 817 large-cap core funds for the twelve months ending June 30, 2008, 497th of 574 over the last five years, 137th of 323 over the last ten years, and 83rd of 270 since inception in July 1997. Lipper, Inc.
is an independent mutual fund rating service that ranks funds in various fund categories by making comparative calculations using total returns. 
 
 
 
Dear Fellow Balanced Fund Shareholder:

For the quarter ending June 30, 2008, Balanced Fund bounced back from a rough first quarter ended March 31, 2008, to increase by 1.62% and outperform each of
its benchmarks, which declined over the three-month period: Balanced Benchmark (-1.69%), S&amp;P 500 (-2.73%), Bloomberg/EFFAS US Government 1-3 year Total Return Bond (-0.99%), and Lipper Balanced Funds Index (-1.07%). Our Fund ended in positive
territory, despite the onslaught of negative news surrounding the economy, the financial services sector, and threats of inflation. We are generally pleased with the turnaround quarter and our absolute and relative performance. As always, our
investment objective is to manage a conservative, low risk balanced portfolio with less than or equal to 40% of the volatility of the S&amp;P index (i.e., 60% less risk). 
Unfortunately the results were not quite as favorable for the full fiscal year ending June 30, 2008. The Fund dropped in value by 1.57% and slightly trailed the Balanced Benchmark, which declined by 0.87%. The Fund also underperformed
the Bloomberg/EFFAS US Government 1-3 Year Total Return Bond Index, which rose by 7.29% during the 12-month period. Bond-only funds and indexes benefited during the period as economic and stock market concerns led to a flight-to-quality from certain
equities into fixed income. On the other hand, Balanced Fund handily beat the equity-only S&amp;P 500 Index (-13.12%) and the Lipper Balanced Funds Index (-5.59%). From a longer-term perspective, we are pleased to report that the Fund has rewarded
our shareholders by outperforming each of the benchmarks since inception in July 2001; such long-term results will remain a key objective of our firm. 
Our hybrid
total-return Fund invests in both equity and fixed income securities, while incorporating an options strategy designed to produce a conservative, lower volatility balanced portfolio. During very favorable equity market conditions, the Fund often
under-performs many of the more aggressive benchmarks. On the other hand, when stocks struggle and investors seek the safe haven of more conservative bonds, Balanced Fund tends to perform better than the equity-only indexes. 
The table below presents our June quarter, one-year, five-year and life-to-date financial results according to the formula required by the SEC. A graph of quarterly performance
since inception appears on the following page. 
 
June Qtr.
1 Year
Life-to-Date
Balanced Fund
Bloomberg/ EFFAS U.S. Government
1-3 Year Total Return Bond Index
Balanced Benchmark
S&P 500 Index
Lipper Balanced Funds Index
Performance figures quoted in the table above and graph on the next page represent past performance and are no guarantee
of future results. The table above and graph below do not reflect the deduction of taxes that a shareholder would pay on Fund distributions or the redemption of Fund shares. 
The Lipper Balanced Funds Index is an index of balanced funds compiled by Lipper, Inc. Balanced Benchmark is a combined index of which 40% reflects the S&amp;P 500 Index (an unmanaged index of large companies with dividends
reinvested) and 60% reflects the Bloomberg/ EFFAS U.S. Government 1-3 year Total Return Bond Index (transparent benchmark for the total return of the 1-3 year U.S. Government bond market). 
 
According to data from Morningstar as of June 30, 2008, the Balanced Fund ranked 204th of 637 Conservative Allocation funds for the calendar year
ended June 30, 2008 and 39th out of 278 funds for five years. Morningstar ranks funds in various fund categories by making comparative calculations using total returns. 
 
 
 
