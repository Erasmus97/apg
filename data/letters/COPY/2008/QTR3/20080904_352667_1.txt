Dear Shareholder,

We are pleased to provide you with the Touchstone Tax-Free Trust Annual Report.
Inside you will find key financial information for the twelve months ended June


During the past twelve months, investors were confronted with a weakening
housing market, subprime-mortgage problems, higher oil prices, the weakening
dollar, and slower economic growth. These difficulties continued to impact the
economy, credit markets and financial institutions.

In reviewing the challenges faced in the municipal market and the downgrade of
several bond insurers, adjustments were made to the investment parameters of the
Ohio Insured Tax-Free Fund. The requirement that the Fund have of its assets
invested in insured bonds was removed during the first quarter of Under
the new parameters, the Fund can now buy Ohio municipal bonds with investment
grade (Baa/BBB or higher) ratings, with at least invested in bonds rated "A"
or better. As a result, the Fund's name was changed to the Touchstone Ohio
Tax-Free Bond Fund.

Touchstone's tax-free money market funds continue to be appealing to those
investors seeking current tax-free income combined with liquidity and stability.
The Funds seek the highest level of interest income exempt from Federal (and in
some cases state) income tax, consistent with protection of capital by primarily
investing in high-quality, short-term municipal obligations.

Fort Washington Investment Advisors, the sub-advisor of the Funds within the
Touchstone Tax-Free Trust, has extensive experience through interest rate cycles
and market events. Fort Washington's rigorous credit standards and consistent
focus on risk management have enabled us to deliver on each Fund's respective
objective.

We continue to believe that diversification is key to balancing risk and return.
As always, we recommend that you work with your financial professional to
utilize a sound asset allocation strategy that invests in a combination of
stock, bond and money market mutual funds to help keep your financial strategy
on course. We also suggest maintaining a long-term approach to gain the full
potential benefits of investing.

We greatly appreciate your continued support. Thank you for including Touchstone
as part of your investment plan.

Sincerely,
