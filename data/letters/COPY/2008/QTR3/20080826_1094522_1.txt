Dear Shareowner,

Staying diversified and keeping your portfolio invested in the markets are two
general investment principles that have served investors well over time. They
are particularly useful guides to keep in mind today, at a time when markets
around the globe are being buffeted by problems in the financial and real estate
industries and by concerns about a slowing economy.

After an extended period of steady economic growth with sustained low
unemployment and low inflation, the U.S. economy ran into difficulty as
drew to a close. Investors in subprime mortgages were forced to mark down the
value of their assets, imperiling leveraged balance sheets. The ensuing credit
crunch forced central banks in the United States and Europe to assume the role
of "lender of last resort" to keep credit markets functioning. Conditions
worsened in the first quarter of as falling prices, margin calls and
deleveraging continued and while the auction-rate preferred market seized up. By
then, recession talk was widespread as concern grew that falling home prices,
rising unemployment, sharply rising food and energy prices, and disruptions in
financial markets posed a significant threat to economic growth. In the next few
months, though, there were no further banking crises, and recession fears began
to fade in light of positive economic news. However, a seemingly unstoppable
rise in the price of oil became a new source of recession fears.

Markets reacted poorly to the developments leading up to the near failure of
Bear Stearns, with fixed-income credit spreads (the difference in rates between
corporate and U.S. government bonds) widening dramatically and stock markets
declining, wiping out the positive returns markets had delivered in the
preceding calendar year. Treasury bond prices rose as the market underwent a
classic "flight to quality." Those trends reversed in the months after the fall
of Bear Stearns, as stock markets rallied, recouping much of their first-quarter
losses, while Treasury bond prices declined. The stock market then reversed
direction yet again, falling sharply to end June near earlier lows while
Treasury bond prices ended June near end-of-year levels.

In the six-month period ending June the Dow Jones Industrial Average
fell the Standard & Poor's Index fell and the NASDAQ Composite
Index fell The MSCI EAFE Developed Market Index of international stock
markets fell and the MSCI Emerging Markets Index fell

Pioneer Research Fund | Semiannual Report |


In each case, the majority of the decline was concentrated in the month of
June. The U.S. investment-grade bond market, as measured by the Lehman Brothers
Aggregate Bond Index, rose over the six-month period, while the U.S.
high-yield bond market, as measured by the Merrill Lynch High Yield Bond Master
II Index, fell

Looking forward, the risk of a recession seems diminished, but higher
commodity prices, lower real estate prices, and a weakened banking system still
pose substantial risks to the real economy. On the other hand, a weak U.S.
dollar and substantial fiscal and monetary stimulus are potent support for the
economy. Markets remain volatile, and falling risk tolerances and deleveraging
may depress asset prices in the short term, but equity and corporate bond
valuations look attractive over a longer time horizon unless the U.S. economy
falls into a severe and protracted recession.

Sudden swings in the markets are always to be expected, but they are difficult
to time. Maintaining a long-term time horizon, being diversified, and paying
attention to asset allocation are important investment principles. As always, we
encourage you to work closely with your financial advisor to find the mix of
stocks, bonds and money market assets that is best aligned to your particular
risk tolerance and investment objective, and to adhere to a strategic plan
rather than letting emotions drive investment decisions.

Respectfully,
