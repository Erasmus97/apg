Dear Shareholder:

We are pleased to present this semiannual report for Dreyfus BASIC Municipal Money Market Fund, covering the six-month period from September through February

The past six months proved to be one of the more challenging periods for investors in recent memory.The U.S. economy sputtered under the weight of plunging housing values, and credit concerns that originated in the bond
markets sub-prime mortgage sector spread to other areas of the financial markets.These developments dampened investor sentiment and produced heightened volatility in the stock market and the higher-yielding segments of the bond market. Even
some areas of the taxable money markets were affected when difficult liquidity conditions arose in the inter-bank lending and commercial paper markets.

Recently, the Fed and the U.S. government have adopted accommodative monetary and fiscal policies in an effort to stimulate the U.S. economy, boost market liquidity and forestall a potential recession.While its too
early to know if their actions will be effective, we believe that the best defense against any economic volatility is to maintain a long-term perspective. To benefit from this focus, talk to your financial adviser today about your specific portfolio
to ensure that your investments are best suited to capture the potential opportunities and manage the risks that may continue to surface during this current economic cycle.

For information about how the fund performed during the reporting period, as well as market perspectives, we have provided a Discussion of Fund Performance given by the funds Portfolio Manager.

I wish you all continued success and I thank you all for your continued confidence and support.











DISCUSSION OF FUND PERFORMANCE

For the period of September through February as provided by Colleen Meehan, Senior Portfolio Manager

Fund and Market Performance Overview

For the six-month period ended February Dreyfus BASIC Municipal Money Market Fund produced an annualized yield of .Taking into account the effects of compounding, the fund also produced an annualized
effective yield of

Tax-exempt money market instruments were primarily influenced by lower short-term interest rates during the reporting period, as the Federal Reserve Board (the Fed) took aggressive action to forestall a
potential recession and promote bond market liquidity in the midst of a credit crisis.

The Funds Investment Approach

The fund seeks as high a level of current income exempt from federal income tax as is consistent with the preservation of capital and the maintenance of liquidity.To pursue this goal, the fund normally invests substantially
all of its net assets in short-term, high-quality, municipal obligations that provide income exempt from federal income tax.The fund may also invest in high-quality, short-term structured notes, which are derivative instruments whose value is tied
to underlying municipal obligations.

In pursuing this approach, we employ two primary strategies. First, we attempt to add value by constructing a diverse portfolio of high-quality, federally tax-exempt money market instruments. Second, we actively manage the
funds average maturity in anticipation of what we believe are interest-rate trends, supply-and-demand changes in the short-term municipal marketplace and anticipated liquidity needs.

For example, if we expect an increase in short-term supply, we may decrease the average weighted maturity of the fund, in an effort to position the fund to purchase new securities with higher yields, if higher yields
materialize as a result of the increase in supply.Yields tend

The Fund







DISCUSSION OF FUND PERFORMANCE (continued)

to rise when there is an increase in new-issue supply competing for investor interest. New securities are generally issued with maturities in the one-year range, which if purchased, would tend to lengthen the funds
average weighted maturity. We also may decrease the average weighted maturity in a rising interest-rate environment. If we anticipate limited new-issue supply and lower interest rates, we may extend the funds average maturity to maintain
current yields for as long as we deem practical.At other times, we typically try to maintain an average weighted maturity that reflects our view of short-term interest-rate trends and future supply-and-demand considerations while anticipating
liquidity needs.

Sub-Prime Mortgage Woes Dampened Economic Growth

By the start of the reporting period, economic conditions and investor sentiment had deteriorated as a result of turmoil originating in the sub-prime mortgage sector of the U.S. bond market. As investors reassessed their
attitudes toward risk, they flocked toward the relative safety of U.S.Treasury securities and money market funds.

At the same time, consumers curtailed their spending, stoking recession concerns.The Fed intervened in September by reducing the federal funds rate for the first time in more than four years. However, news of massive
sub-prime related losses by investment and commercial banks led to renewed market turbulence.The Fed responded with several more cuts in the federal funds rate, which stood at by year-end. Heightened market volatility persisted through
February as additional evidence of economic weakness accumulated, including the first monthly net loss of jobs in more than four years. The Fed took aggressive action in late January, reducing the federal funds rate by another basis points
to in two moves.

Assets Flowed into Tax-Exempt Money Market Funds

As expected, tax-exempt money market yields declined along with the federal funds rate. In addition, a record level of assets flowed into municipal money market funds from risk-averse investors. For the most part, rising
demand was met with ample supply, as investment banks









continued to create a substantial volume of short-term variable-rate demand notes and tender option bonds. Consequently, yields of floating-rate instruments were higher at times than those of longer-dated municipal notes.
By the reporting periods end, however, unrelenting investor demand began to overwhelm supply, putting downward pressure on short-term yields.

The fiscal conditions of most municipal issuers remained sound during the reporting period, but some states began to expect renewed budget pressures. In some areas, weak housing markets may reduce future tax revenues at a
time when states are struggling with high levels of long-term debt.

Maintaining a Conservative Investment Posture

We generally maintained a cautious investment approach, focusing whenever possible on instruments issued directly by cities, states, school districts and other taxing authorities.We set the funds weighted average
maturity in a range that was longer than industry averages to capture higher yields for as long as we deemed practical.As always, our research staff maintained rigorous credit standards, which, in our judgment, have become even more important in the
current credit crunch.

As of the reporting periods end, we expect the Fed to implement further reductions in the federal funds rate. We currently intend to maintain the funds longer weighted average maturity, which we believe should
protect its yield as interest rates decline.

March








An investment in the fund is not insured or guaranteed by the FDIC or any other government









agency. Although the fund seeks to preserve the value of your investment at per share, it is









possible to lose money by investing in the fund.









Annualized effective yield is based upon dividends declared daily and reinvested monthly. Past









performance is no guarantee of future results.Yields fluctuate. Income may be subject to state and









local taxes, and some income may be subject to the federal alternative minimum tax (AMT) for









certain investors.Yields provided reflect the absorption of certain fund expenses by The Dreyfus









Corporation, pursuant to an agreement in effect until such time as shareholders are given at least









days notice. Had these expenses not been absorbed, the funds annualized yield would have









been and the funds annualized effective yield would have been




The Fund







UNDERSTANDING YOUR FUNDS EXPENSES (Unaudited)

Dear Shareholder:

We are pleased to present this semiannual report for Dreyfus BASIC New Jersey Municipal Money Market Fund, covering the six-month period from September through February

The past six months proved to be one of the more challenging periods for investors in recent memory.The U.S. economy sputtered under the weight of plunging housing values, and credit concerns that originated in the bond
markets sub-prime mortgage sector spread to other areas of the financial markets.These developments dampened investor sentiment and produced heightened volatility in the stock market and the higher-yielding segments of the bond market. Even
some areas of the taxable money markets were affected when difficult liquidity conditions arose in the inter-bank lending and commercial paper markets.

Recently, the Fed and the U.S. government have adopted accommodative monetary and fiscal policies in an effort to stimulate the U.S. economy, boost market liquidity and forestall a potential recession.While its too
early to know if their actions will be effective, we believe that the best defense against any economic volatility is to maintain a long-term perspective. To benefit from this focus, talk to your financial adviser today about your specific portfolio
to ensure that your investments are best suited to capture the potential opportunities and manage the risks that may continue to surface during this current economic cycle.

For information about how the fund performed during the reporting period, as well as market perspectives, we have provided a Discussion of Fund Performance given by the funds Portfolio Manager.

I wish you all continued success and I thank you all for your continued confidence and support.









DISCUSSION OF FUND PERFORMANCE

For the period of September through February as provided by Joseph Irace, Senior Portfolio Manager

Fund and Market Performance Overview

For the six-month period ended February Dreyfus BASIC New Jersey Municipal Money Market Fund produced an annualized yield of . Taking into account the effects of compounding, the fund also produced an
annualized effective yield of

Tax-exempt money market instruments were primarily influenced by lower short-term interest rates during the reporting period, as the Federal Reserve Board (the Fed) took aggressive action to forestall a
potential recession and promote liquidity in the midst of a credit crisis.

The Funds Investment Approach

The fund seeks as high a level of current income exempt from federal and New Jersey state income taxes as is consistent with the preservation of capital and the maintenance of liquidity.To pursue this goal, the fund
normally invests substantially all of its net assets in short-term, high-quality municipal obligations that provide income exempt from federal and New Jersey state personal income taxes.The fund may also invest in high-quality, short-term structured
notes, which are derivative instruments whose value is tied to underlying municipal obligations.

In pursuing this investment approach, we employ two primary strategies. First, we attempt to add value by constructing a diverse portfolio of high-quality, tax-exempt money market instruments from New Jersey exempt issuers.
Second, we actively manage the funds average maturity in anticipation of what we believe are interest-rate trends and supply-and-demand changes in New Jerseys short-term municipal marketplace.

For example, if we expect an increase in short-term supply, we may decrease the average weighted maturity of the fund, which should position the fund to purchase new securities with higher yields, if higher yields
materialize as a result of the increase in supply.Yields tend to rise

The Fund







DISCUSSION OF FUND PERFORMANCE (continued)

when there is an increase in new-issue supply competing for investor interest. New securities are generally issued with maturities in the one-year range, which if purchased, would tend to lengthen the funds average
weighted maturity. If we anticipate limited new-issue supply, we may extend the funds average maturity to maintain then-current yields for as long as we deem practical.At other times,we typically try to maintain an average weighted maturity
that reflects our view of short-term interest-rate trends and future supply-and-demand considerations.

Sub-Prime Mortgage Woes Dampened Economic Growth

By the start of the reporting period, economic conditions and investor sentiment had deteriorated as a result of turmoil originating in the sub-prime mortgage sector of the U.S. bond market. As investors reassessed their
attitudes toward risk, they flocked toward the relative safety of U.S.Treasury securities and money market funds.

At the same time, consumers curtailed their spending, stoking recession concerns. The Fed intervened in September by reducing the federal funds rate for the first time in more than four years. However, news of massive
sub-prime related losses by investment and commercial banks led to renewed market turbulence. The Fed responded with several more cuts in the federal funds rate, which stood at by year-end. Heightened market volatility persisted through
February as additional evidence of economic weakness accumulated, including the first monthly net loss of jobs in more than four years.The Fed took aggressive action in late January, reducing the federal funds rate by another basis points
to in two moves.

Assets Flowed into Tax-Exempt Money Market Funds

As expected, tax-exempt money market yields declined along with the federal funds rate. In addition, a record level of assets flowed into municipal money market funds from risk-averse investors. For the most part, rising
demand was met with ample supply, as investment banks continued to create a substantial volume of short-term variable-rate demand notes and tender option bonds. Consequently, yields of floating-rate instruments were higher at times than those
of









longer-dated municipal notes. By the reporting periods end, however, unrelenting investor demand began to overwhelm supply, putting downward pressure on short-term yields.

New Jerseys fiscal condition remained sound during the reporting period, as the wealth of state residents helped offset the negative influences of high pension obligations and an onerous debt burden.While the state
can expect to encounter renewed budget pressures as the economy slows, the governor recently offered a proposal designed to reduce debt and enhance revenues.

Maintaining a Conservative Investment Posture

We generally maintained a cautious investment approach, focusing whenever possible on instruments issued directly by New Jersey, its school districts and municipal authorities with their own revenue streams. We set the
funds weighted average maturity in a range that was longer than industry averages to capture higher yields for as long as we deemed practical.As always, our research staff maintained rigorous credit standards, which, in our judgment, have
become even more important in the current credit crunch.

As of the reporting periods end, we expect the Fed to implement further reductions in the federal funds rate. We currently intend to maintain the funds longer weighted average maturity, which we believe should
protect its yield as interest rates decline.

March








An investment in the fund is not insured or guaranteed by the FDIC or any other government









agency. Although the fund seeks to preserve the value of your investment at per share, it is









possible to lose money by investing in the fund.









Annualized effective yield is based upon dividends declared daily and reinvested monthly. Past









performance is no guarantee of future results.Yields fluctuate. Income may be subject to state and









local taxes for non-New Jersey residents, and some income may be subject to the federal alternative









minimum tax (AMT) for certain investors.Yields provided reflect the absorption of certain fund









expenses by The Dreyfus Corporation pursuant to an agreement in effect until such time as









shareholders are given at least days notice to the contrary. Had these expenses not been









absorbed, the funds annualized yield would have been and the funds annualized effective









yield would have been




The Fund







UNDERSTANDING YOUR FUNDS EXPENSES (Unaudited)

Dear Shareholder:

We are pleased to present this semiannual report for Dreyfus Premier High Yield Municipal Bond Fund, covering the six-month period from September through February

The past six months proved to be one of the more challenging periods for fixed-income investors in recent memory. The U.S. economy sputtered under the weight of plunging housing values, and credit concerns that originated
in the U.S. sub-prime mortgage sector spread to other areas of the global financial markets. These developments dampened investor sentiment and produced heightened volatility in many segments of the municipal bond market. Particularly hard-hit were
lower-rated municipal bonds and those carrying third-party insurance from independent bond insurers.

Recently, the Fed and the U.S. government have adopted accommodative monetary and fiscal policies in an effort to stimulate the U.S. economy, boost market liquidity and forestall a potential recession.While its too
early to know if their actions will be effective, we believe that the best defense against any economic volatility is to maintain a long-term perspective.To benefit from this focus, talk to your financial adviser today about your specific portfolio
to ensure that your investments are best suited to capture the potential opportunities and manage the risks that may continue to surface during this current economic cycle.

For information about how the fund performed during the reporting period, as well as market perspectives, we have provided a Discussion of Fund Performance given by the funds Portfolio Managers.

I wish you all continued success and I thank you all for your continued confidence and support.











DISCUSSION OF FUND PERFORMANCE

For the period of September through February as provided by W. Michael Petty and James Welch, Portfolio Managers

Fund and Market Performance Overview

For the six-month period ended February Dreyfus Premier High Yield Municipal Bond Funds Class A shares produced a total return, Class C shares produced a total return and Class Z
shares produced a total The funds benchmark, the Lehman Brothers Municipal Bond Index, which does not include securities rated below investment
grade, produced a total return, and the funds Lipper category average provided an average return of for the same

The reporting period proved to be a challenging time for high yield municipal bonds. An intensifying credit crisis and economic slowdown triggered a flight to quality in which investors turned away from
higher-yielding investments, often without regard to their underlying credit fundamentals.As might be expected, the fund underperformed its benchmark, which we mainly attribute to the funds holdings of lower-rated securities relative to its
benchmark that were more vulnerable to the sub-prime fallout. However, a relatively cautious investment posture enabled the fund to post higher returns than its Lipper category average.

The Funds Investment Approach

The fund primarily seeks high current income exempt from federal income tax. Secondarily, the fund may seek capital appreciation to the extent consistent with its primary goal.To pursue its goals, the fund normally invests
at least of its assets in municipal bonds that provide income exempt from federal income tax. The fund normally invests at least of its assets in municipal bonds rated BBB/Baa or lower by independent rating agencies or the unrated equivalent
as determined by Dreyfus. Municipal bonds rated below investment grade (BB/Ba or lower) are commonly known as high yield or junk bonds.The fund

The Fund







DISCUSSION OF FUND PERFORMANCE (continued)

may invest up to of its assets in higher-quality municipal bonds rated AAA/Aaa to A, or the unrated equivalent as determined by Dreyfus.

We may buy and sell bonds based on credit quality, market outlook and yield potential. When selecting municipal bonds, we may assess the current interest rate environment and the municipal bonds credit profile and
potential volatility in different rate environments.We focus on bonds with the potential to offer attractive current income, including those that can provide consistently attractive current yields or that are trading at competitive market prices. A
portion of the funds assets may be allocated to discount bonds, which sell at a price below their face value, or to premium bonds, which sell at a price above their face value. The funds allocation to either
discount or premium bonds will change with our view of the current interest rate and market environments.We also may look to select bonds that are most likely to obtain attractive prices when sold.

Municipal Bonds Suffered in the Credit Crunch

The reporting period began amid a credit crisis that began when an unexpectedly high number of homeowners defaulted on their sub-prime mortgages. This development sent shockwaves throughout the global financial markets as
investors reassessed their attitudes toward risk and began to shun lower-rated bonds.

The sub-prime meltdown produced massive losses among bond insurers. Because many of these companies had written insurance on both mortgage-backed securities and municipal bonds, investors responded negatively when insurers
came under financial pressure. Even municipal bonds with strong underlying credit characteristics lost value as liquidity conditions become more difficult.

The effects of the credit crisis were exacerbated by slower economic growth as declining housing prices, soaring energy costs and a softer job market put pressure on consumer spending. Aggressive reductions of short-term
interest rates by the Federal Reserve Board and a fiscal stimulus package from Congress have not yet forestalled further eco-









nomic deterioration. Not surprisingly, the economic slowdown led to concerns that states and municipalities may face greater fiscal pressures if tax revenues fail to meet budgeted projections.

A Cautious Investment Posture Helped Support Fund Returns

The fund weathered the credit storm relatively well. We focused primarily on municipal bonds with credit ratings at the upper end of the high yield range as yield spreads widened along the credit spectrum. Toward the end of
the reporting period, we invested in the short-term auction securities market, where difficult liquidity conditions produced unusually high tax-exempt yields. Most important, our staff of credit analysts continued to conduct extensive research into
the funds current and prospective holdings in an effort to ensure that they will continue to pay interest and principal in a timely manner.

Positioned for Continued Volatility

As of the reporting periods end, the financial markets have remained unsettled and economic conditions have continued to falter.Therefore, we currently intend to maintain a defensive investment posture, focusing
primarily on bonds that our research suggests have strong credit and liquidity profiles. While we have begun to find value-oriented opportunities among high yield municipal bonds that appear to have been punished too severely, we believe it is
prudent to wait until we see signs of improvement before seeking to capitalize on them.








Total return includes reinvestment of dividends and any capital gains paid. Past performance is no

Dear Shareholder:

We are pleased to present this semiannual report for Dreyfus Premier AMT-Free Municipal Bond Fund, covering the six-month period from September through February

The past six months proved to be one of the more challenging periods for fixed-income investors in recent memory. The U.S. economy sputtered under the weight of plunging housing values, and credit concerns that originated
in the U.S. sub-prime mortgage sector spread to other areas of the global financial markets. These developments dampened investor sentiment and produced heightened volatility in many segments of the municipal bond market. Particularly hard-hit were
lower-rated municipal bonds and those carrying third-party insurance from independent bond insurers.

Recently, the Fed and the U.S. government have adopted accommodative monetary and fiscal policies in an effort to stimulate the U.S. economy, boost market liquidity and forestall a potential recession.While its too
early to know if their actions will be effective, we believe that the best defense against any economic volatility is to maintain a long-term perspective.To benefit from this focus, talk to your financial adviser today about your specific portfolio
to ensure that your investments are best suited to capture the potential opportunities and manage the risks that may continue to surface during this current economic cycle.

For information about how the fund performed during the reporting period, as well as market perspectives, we have provided a Discussion of Fund Performance given by the funds Portfolio Manager.

I wish you continued success and I thank you for your continued confidence and support.









DISCUSSION OF FUND PERFORMANCE

For the period of September through February as provided by Douglas Gaylor, Portfolio Manager

Fund and Market Performance Overview

For the six-month period ended February Class A, B, C and Z shares of Dreyfus Premier AMT-Free Municipal Bond Fund produced total returns of and
In comparison, the Lehman Brothers Municipal Bond Index (the Index), the funds benchmark, produced a total return of for the
same

An intensifying credit crisis and economic slowdown triggered a flight to quality in which investors turned away from many asset classes, including municipal bonds, often without regard to the underlying credit
fundamentals of individual securities.The fund underperformed its benchmark in this unsettled environment, due in part to price dislocations resulting from liquidity concerns, as well as the portfolios relatively cautious investment
posture.

The Funds Investment Approach

The fund seeks as high a level of current income exempt from federal income tax as is consistent with the preservation of capital.To pursue this goal, the fund normally invests substantially all of its net assets in
municipal bonds that provide income exempt from federal income tax. The fund also seeks to provide income exempt from the federal alternative minimum tax.

The fund invests at least of its assets in municipal bonds with an A or higher credit rating, or the unrated equivalent as determined by The remaining of the funds assets may be invested in municipal
bonds with a credit quality lower than A, including high yield (or junk) bonds. The dollar-weighted average maturity of the funds portfolio normally exceeds years, but there are no specific requirements with respect to average portfolio
maturity.

We may buy and sell bonds based on credit quality, market outlook and yield potential. In selecting municipal bonds for investment, we may

The Fund







DISCUSSION OF FUND PERFORMANCE (continued)

assess the current interest-rate environment and the municipal bonds potential volatility in different rate environments. We focus on bonds with the potential to offer attractive current income, typically looking for
bonds that can provide consistently attractive current yields or that are trading at competitive market prices. A portion of the funds assets may be allocated to discount bonds, which are bonds that sell at a price below their face
value, or to premium bonds, which are bonds that sell at a price above their face value.The funds allocation to either discount bonds or premium bonds will change along with our changing views of the current interest-rate and
market environment.We also may look to select bonds that are most likely to obtain attractive prices when sold.

Municipal Bonds Suffered in the Credit Crunch

The reporting period began amid a credit crisis originating in the sub-prime mortgage market, where an unexpectedly high number of homeowners defaulted on their loans. This development sent shock-waves throughout the
financial markets as investors reassessed their attitudes toward risk.

The sub-prime meltdown produced massive losses among bond insurers. Because many of these companies had written insurance on both mortgage-backed securities and municipal bonds, municipal bond investors responded negatively
when insurers came under financial pressure. Even municipal bonds with strong underlying credit characteristics lost value as liquidity conditions become more difficult.

The credit crisis was exacerbated by slower economic growth, as declining housing prices, soaring energy costs and a softer job market put pressure on consumer spending. Aggressive reductions of short-term interest rates by
the Federal Reserve Board and a fiscal stimulus package from Congress have not yet forestalled further economic deterioration. Not surprisingly, the economic slowdown led to concerns that states and municipalities may soon face greater fiscal
pressures.

A Cautious Investment Posture Supported Fund Returns

While the funds performance was influenced by the various market and economic factors that depressed municipal bond prices, several strategies helped boost its performance. For example, our longstanding







focus on higher-quality bonds from well-known issuers helped protect the fund from the brunt of weakness among lower-rated and less liquid bonds.The fund also benefited from the sale of certain bonds with long-term
maturities, which we considered vulnerable to interest-rate risks.We sold many of those securities to individual investors at what we considered to be attractive prices. We used a portion of the sales proceeds to maintain relatively robust cash
levels, putting us in a position to take advantage of unusually high yields on short-term auction rate securities toward the end of the reporting period.

Finding Opportunities in a Distressed Market

As of the reporting periods end, the financial markets have remained unsettled.Therefore, we currently intend to maintain a defensive investment posture, focusing primarily on bonds that, after rigorous research, meet
our credit and liquidity criteria. In addition, we have identified fundamentally sound municipal bonds that appear to have been punished too severely during the downturn. We have begun to capitalize on some of these opportunities, particularly among
bonds from states where local laws enhance their ability to withstand market weakness.

March








Total return includes reinvestment of dividends and any capital gains paid and does not take into
