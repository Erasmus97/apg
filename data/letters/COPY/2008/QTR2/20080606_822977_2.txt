Dear Shareholder:




The Goldman Sachs Enhanced
Income Fund (the Fund) has changed its fiscal year
end from October to March As a result, this report
provides an overview on the performance of the Fund during the
five-month reporting period that ended




Performance Review















Over the five-month period that
ended the Funds ClassA, B,
Institutional, and Administration Shares generated cumulative
total returns of and respectively.
These returns compare to the and cumulative total
returns of the Funds benchmarks, the Six-Month U.S.
Treasury Bill Index and the One-Year U.S. Treasury
NoteIndex, respectively, over the same time period.










Volatility along with generally
wider sector spreads hampered relative performance over the
five-month period, particularly in February and March. Overall,
the portfolios term structure positioning (its positioning
relative to our interest rate expectations) had a negative
impact. We believed that the U.S. Federal Reserve (the
Fed) would continue with monetary policy easing
while medium- to long-term inflation expectations would remain
to the upside.










At the cross-sector level, the
portfolios long exposure (we held securities not
represented in the benchmark index) to the corporate sector
detracted from relative performance. Given the widening of
credit spreads and increase in volatility, we were targeting a
small long credit risk position. We were seeking to find the
valuation anomalies that existed as a result of the
indiscriminate spread widening and were looking to take
advantage of the attractively priced new issues. However, due to
the global flight to quality, Treasuries outperformed corporate
bonds and this detracted from relative results. Our long
exposure to the asset-backed sector also hurt performance as
sector spreads widened. At the security level, our selection of
auto asset-backed securities added to returns; however, this
positive contribution was offset by selection of government and
corporate securities.






Market Review















The spread sectors
all non-Treasury investment grade sectors including federal
agency securities, corporate bonds, asset backed securities,
mortgage backed securities, and commercial mortgage backed
securities experienced unprecedented volatility over
the five-month reporting period. U.S. Treasury yields fell
dramatically as contagion from the subprime mortgage crisis
spread, sparking a global flight to quality and the massive
unwinding of leveraged positions. Credit and mortgage-related
sector spreads widened to historic levels as supply flooded the
market. Even short duration securities were affected.










To restore liquidity, the Fed
took extraordinary steps to stave off a financial crisis. It
created: Term Auction Lending Facility (TALF) of
Term Securities Lending Facility
(TSLF) of Primary Dealer Credit
Facility) (PDCF) with no specified limit; and lending
facility specifically for Bear Stearns of The
Fed also cut the discount rate and the Fed Funds rate, citing
risks to economic growth. Meanwhile, the federal government
enacted policy changes that allow Fannie Mae and Freddie Mac to
purchase additional mortgage securities.
















PORTFOLIO RESULTS















These actions helped to create
liquidity and ease supply and demand pressures on the market. As
result, mortgage-backed and asset-backed security prices
stabilized in the second half of March and began a tentative
rebound in April. The Fed has stated they are considering
additional steps to further improve liquidity.










Economic data released over the
five-month period suggested that a recession was growing more
likely. The housing market struggled as home prices continued to
depreciate and sales of existing and new homes declined. Along
with surging oil and gas prices, a weaker dollar, and a softer
labor market, this served to heighten inflation concerns.


















INVESTMENT
OBJECTIVE

















The Fund seeks to generate return
in excess of traditional money market products while maintaining
an emphasis on preservation of capital and liquidity.

It is important to note that the Fund is not a money market
fund and its net asset value will fluctuate.








Portfolio Composition















Within our sector exposures, we
continue to emphasize shorter-term, higher quality securities
that we believe offer an attractive yield advantage. The Fund
continues to hold a significant position in short-term
quasi-government securities, specifically agency debentures;
however, this exposure decreased over the period as some of
these shorter-term securities matured, and our allocation to
short-term investments, or cash, increased accordingly. Within
the corporate sector, we slightly increased our overweight to
financials because of the high quality of many of these issuers.
We continue to look for attractive security selection
opportunities to emerge in both the agency and corporate sectors.










We thank you for your investment
and look forward to your continued confidence.















Goldman Sachs U.S. Fixed
Income Management Team

Dear Shareholder:




The Goldman Sachs Ultra-Short
Duration Government Fund (the Fund) has changed its
fiscal year end from October to March As a result, this
report provides an overview on the performance of the Fund
during the five-month reporting period that ended




Performance Review















Over the five-month period that
ended the Funds ClassA,
Institutional, and Service Shares generated cumulative total
returns, without sales charges, of and
respectively. These returns compare to the and
cumulative total returns of the Funds benchmarks, the
Six-Month U.S. Treasury Bill Index and the One-Year U.S.
Treasury NoteIndex, respectively, over the same time
period. In the period since their inception through
the Funds ClassIR Shares
generated cumulative total returns of versus and
for the Six-Month U.S. Treasury Bill Index and the
One-Year U.S. Treasury NoteIndex, respectively, over the
same period.





















Increased volatility in sector
and swap spreads over the past five months and a global flight
to quality which led to the outperformance of
Treasuries relative to all spread sectors was the
primary driver of the relative underperformance of our short
duration strategies. (A swap is a simultaneous exchange of
assets for other assets of comparable value. Swap spreads are
the difference between the negotiated and fixed rate of a swap
and tend to be determined by market supply and creditworthiness.)





















As mortgage sector spreads
widened, the Funds overweight position to the sector
hampered its relative performance. The most meaningful detractor
from performance, however, was the portfolios position in
high-quality, non-agency adjustable-rate mortgages (ARMs). In
our view, the underperformance was almost entirely caused by
liquidity problems in the credit markets. Although the
delinquency rate on underlying mortgages rose more quickly than
investors anticipated, we do not think it increased enough to
fully explain the volatility. Instead, we believe prices were
pushed down not because the fundamental value of
these securities changed but because of supply and
demand pressures. To meet their debt obligations, a large number
of investors were forced to sell their high-quality liquid
assets. We continue to believe that the ARMs we hold are
structured to withstand ongoing pressure from the extremely weak
housing market.





















Overall, the portfolios
term structure positioning had a modestly positive impact.
Because we believed the U.S. Federal Reserve (the
Fed) would continue lowering the Federal Funds rate
(causing shorter maturity Treasury yields to fall), we were
well-positioned when short-term rates declined more than
long-term rates.






Market Review















The spread sectors
all non-Treasury investment grade sectors including federal
agency securities, corporate bonds, asset backed securities,
mortgage backed securities, and commercial mortgage backed
securities experienced unprecedented volatility over
the five-month reporting period. U.S. Treasury yields fell
dramatically as contagion from the subprime mortgage crisis
spread, sparking a global flight to quality and the massive
















PORTFOLIO RESULTS















unwinding of leveraged positions.
Credit and mortgage-related sector spreads widened to historic
levels as supply flooded the market. Even short duration
securities were affected.










To restore liquidity, the Fed
took extraordinary steps to stave off a financial crisis. To
this end, the Fed created: Term Auction Lending
Facility (TALF) of Term Securities
Lending Facility (TSLF) of
Primary Dealer Credit Facility (PDCF) with no specified limit;
and lending facility specifically for Bear Stearns of
The Fed also cut the discount rate and the Fed
Funds rate, citing risks to economic growth. Meanwhile, the
federal government enacted policy changes that allow Fannie Mae
and Freddie Mac to purchase additional mortgage securities.










These actions helped to create
liquidity and ease supply and demand pressures on the market. As
result, mortgage-backed and asset-backed security prices
stabilized in the second half of March and began a tentative
rebound in April. The Fed has stated they are considering
additional steps to further improve liquidity.










Economic data released over the
five-month period suggested that a recession was growing more
likely. The housing market struggled as home prices continued to
depreciate and sales of existing and new homes declined. Along
with surging oil and gas prices, a weaker dollar, and a softer
labor market, this served to heighten inflation concerns.


















INVESTMENT
OBJECTIVE

















The Fund seeks a high level of
current income, consistent with low volatility of principal.








Portfolio Composition















At the end of the period, the
Fund held a short duration position that was shorter than the
benchmark. We believe that interest rates appear quite low, as
the market has been too aggressive in pricing in a potential
recession. Over the period, we continued to focus on security
selection and subsector strategies across the collateralized and
government sectors. Within mortgages, our primary exposures were
within high quality non-agency ARMs and passthrough mortgages.
We tactically adjusted the Funds collateralized and
government exposures to take advantage of changing relative
opportunities. In our view, the current market volatility offers
attractive security selection opportunities and we look to add
to our sector exposures whenever we find value from a
risk-return perspective. Nevertheless, we remain prudent as
volatility remains high and market events unfold.










We thank you for your investment
and look forward to your continued confidence.














Goldman Sachs U.S. Fixed
Income Investment Management Team

Dear Shareholder:




The Goldman Sachs Short Duration
Government Fund (the Fund) has changed its fiscal
year end from October to March As a result, this report
provides an overview on the performance of the Fund during the
five-month reporting period that ended




Performance Review















Over the five-month period that
ended the Funds ClassA, B, C,
Institutional and Service Shares generated cumulative total
returns, without sales charges, of
and respectively. These returns compare to the
cumulative total return of the Funds benchmark, the
Two-Year U.S. Treasury NoteIndex, over the same time
period. In the period since their inception through
the Funds ClassIR Shares
generated cumulative total returns of versus for the
Two-Year U.S. Treasury Note Index, over the same period.










Increased volatility in sector
and swap spreads over the past five months and a global flight
to quality which led to the outperformance of
Treasuries relative to all spread sectors was the
primary driver of underperformance relative to the benchmark of
our short duration strategies. (A swap is a simultaneous
exchange of assets for other assets of comparable value. Swap
spreads are the difference between the negotiated and fixed rate
of a swap and tend to be determined by market supply and
creditworthiness.)










Overall, the portfolios
term structure positioning (its positioning relative to our
interest rate expectations) had a modestly positive impact on
relative performance. Because we believed the U.S. Federal
Reserve (the Fed) would continue lowering the
Federal Funds rate (causing shorter maturity Treasury yields to
fall), we were well-positioned when short-term rates declined
more than long-term rates.










As mortgage sector spreads
widened in reaction to the subprime mortgage crisis, the
Funds overweight position to the sector hampered its
relative performance. Security selection, however, boosted
results. Particularly beneficial were the portfolios
holdings of adjustable rate mortgages (ARMs), collateralized
mortgage obligations, and fixed rate passthroughs. (A
passthrough is a pool of fixed income securities backed by a
package of assets where the holder receives both the principal
and interest payments.) Security selection within government
securities also added value.






Market Review















The spread sectors
all non-Treasury investment grade sectors including federal,
agency securities, corporate bonds, asset backed securities,
mortgage backed securities, and commercial mortgage backed
securities experienced unprecedented volatility over
the five-month reporting period. U.S. Treasury yields fell
dramatically as contagion from the subprime mortgage crisis
spread, sparking a global flight to quality and the massive
unwinding of leveraged positions. Credit and mortgage-related
sector spreads widened to historic levels as supply flooded the
market. Even short duration securities were affected.










To restore liquidity, the Fed
took extraordinary steps to stave off a financial crisis. To
this end, the Fed created: Term Auction Lending
Facility (TALF) of Term Securities
Lending Facility (TSLF) of
Primary Dealer Credit Facility (PDCF) with no specified limit;
and lending facility specifically for Bear






















PORTFOLIO RESULTS















Stearns of The
Fed also cut the discount rate and the Fed Funds rate, citing
risks to economic growth. Meanwhile, the federal government
enacted policy changes that allow Fannie Mae and Freddie Mac to
purchase additional mortgage securities.










These actions helped to create
liquidity and ease supply and demand pressures on the market. As
result, mortgage-backed and asset-backed security prices
stabilized in the second half of March and began a tentative
rebound in April. The Fed is considering additional steps to
further improve liquidity.










Economic data released over the
five-month period suggested that a recession was growing more
likely. The housing market struggled as home prices continued to
depreciate and sales of existing and new homes declined. Along
with surging oil and gas prices, a weaker dollar, and a softer
labor market, this served to heighten inflation concerns.


















INVESTMENT
OBJECTIVE

















The Fund seeks a high level of
current income and may also consider the potential for capital
appreciation.








Portfolio Composition















At the end of the period, we held
a modest short duration bias relative to the benchmark. While we
believe more Fed cuts are possible, we also think that the
current level of interest rates reflects an overly aggressive
easing policy. Over the period, we continued to focus on
security selection and sub-sector strategies. Within mortgages,
we emphasized securities offering attractive spreads relative to
Treasuries; in particular, we hold a bias toward fixed rate
passthroughs and ARMs. (A passthrough is a pool of fixed income
securities backed by a package of assets where the holder
receives both the principal and interest payments.) As
valuations changed over the period, we tactically adjusted the
Funds exposures to take advantage of changing relative
opportunities within the market. We modestly decreased the
portfolios allocation to agencies, ARMs and passthrough
mortgages, and the allocation to short-term investments, or cash
equivalents, increased accordingly.










We thank you for your investment
and look forward to your continued confidence.














Goldman Sachs U.S. Fixed
Income Investment Management Team

Dear Shareholder:




The Goldman Sachs Government
Income Fund (the Fund) has changed its fiscal year
end from October to March As a result, this report
provides an overview on the performance of the Fund during the
five-month reporting period that ended




Performance Review















Over the five-month period that
ended the Funds ClassA, B, C,
Institutional and Service Shares generated cumulative total
returns, without sales charges, of
and respectively. These returns compare to the
cumulative total return of the Funds benchmark, the Lehman
Brothers Government/Mortgage Index, over the same time period.
In the period since their inception through
the Funds ClassR and IR Shares generated cumulative
total returns of and respectively, versus for
the Lehman Brothers Government/Mortgage Index, over the same
time period.










The primary detractor from
relative results, particularly during the second half of the
five-month period, was the Funds position in high-quality,
non-agency adjustable-rate mortgages (ARMs). In our view, the
underperformance was almost entirely caused by liquidity
problems in the credit markets. Although the delinquency rate on
underlying mortgages rose more quickly than investors
anticipated, we do not think it increased enough to fully
explain the volatility. Instead, we believe prices were pushed
down not because the fundamental value of these
securities changed but because of supply and demand
pressures. To meet their debt obligations, a large number of
investors were forced to sell their high-quality liquid assets.
We continue to believe that the ARMs we hold are structured to
withstand ongoing pressure from the extremely weak housing
market.










Another detractor was our
overweight position in commercial mortgage-backed securities
(CMBS), a sector that underperformed Treasuries. While
fundamentals in the CMBS sector were weakening, spreads appeared
attractive given recent spread widening. During the latter part
of the period, the portfolios modest overweight to fixed
rate passthroughs detracted slightly as mortgage sector spreads
widened. (A passthrough is a pool of fixed income mortgages
whereby the holder receives both principal and interest
payments.)










Overall, the portfolios
term structure positioning (its positioning relative to our
interest rate expectations) had a modestly positive impact on
performance. We believed that the U.S. Federal Reserve (the
Fed) would continue with monetary policy easing
while medium- to long-term inflation expectations would remain
to the upside. In particular, we were well-positioned when
short-term rates declined more than long-term rates; we believed
the Fed would continue lowering the Federal Funds rate (causing
shorter maturity Treasury yields to fall).






Market Review















The spread sectors
all non-Treasury investment grade sectors including federal
agency securities, corporate bonds, asset backed securities,
mortgage backed securities, and commercial mortgage backed
securities experienced unprecedented volatility over
the five-month reporting period. U.S. Treasury yields fell
dramatically as contagion from the subprime mortgage crisis
spread, sparking a global flight to quality and the massive
unwinding of leveraged positions. Credit and mortgage-related
sector spreads widened to historic levels as supply flooded the
market. Even short duration securities were affected.






















PORTFOLIO RESULTS















To restore liquidity, the Fed
took extraordinary steps to stave off a financial crisis. To
this end, the Fed created: Term Auction Lending
Facility (TALF) of Term Securities
Lending Facility (TSLF) of
Primary Dealer Credit Facility (PDCF) with no specified limit;
and lending facility specifically for Bear Stearns of
The Fed also cut the discount rate and the Fed
Funds rate, citing risks to economic growth. Meanwhile, the
federal government enacted policy changes that allow Fannie Mae
and Freddie Mac to purchase additional mortgage securities.










These actions helped to create
liquidity and ease supply and demand pressures on the market. As
result, mortgage-backed and asset-backed security prices
stabilized in the second half of March and began a tentative
rebound in April. The Fed is considering additional steps to
further improve liquidity.










Economic data released over the
five-month period suggested that a recession was growing more
likely. The housing market struggled as home prices continued to
depreciate and sales of existing and new homes declined. Along
with surging oil and gas prices, a weaker dollar, and a softer
labor market, this served to heighten inflation concerns.


















INVESTMENT
OBJECTIVE

















The Fund seeks a high level of
current income, consistent with safety of principal.








Portfolio Composition















At the end of the period, we held
a modest short duration bias relative to the benchmark. While we
believe more Fed cuts are possible, we also think that the
current level of interest rates reflects an overly aggressive
easing policy. We continue to focus on sectors and securities
that have the potential to generate a competitive total rate of
return relative to the benchmark. To take advantage of changing
relative valuations, we tactically adjusted the Funds
exposures, and added value through security-specific trades.
Following the recent spread widening, we increased our
passthrough exposure. We continued to find opportunities in
municipal securities and modestly increased our allocation to
them. In our view, the current market volatility offers
attractive security selection opportunities and we look to add
to our sector exposures whenever we find value from a
risk-return perspective. Nevertheless, we remain prudent as
market events unfold and volatility persists.














We thank you for your investment
and look forward to your continued confidence.














Goldman Sachs U.S. Fixed
Income Investment Management Team

Dear Shareholder:




This report provides an overview
on the performance of The Goldman Sachs Inflation Protected
Securities Fund (the Fund) during the seven-month
reporting period from the Funds inception on
through



Performance Review















Over the period since its
inception on through ended
the Funds ClassA, C, and Institutional Shares
generated cumulative total returns, without sales charges, of
and respectively. These returns compare
to the cumulative total return of the Funds
benchmark, the Lehman Brothers U.S. TIPS Index, over the same
time period. In the period since their inception on
through the
Funds ClassR and IR Shares generated cumulative
total returns of and respectively, versus for
the Lehman Brothers U.S. TIPS Index, over the same time period.










The Funds outperformance
versus the benchmark was largely the result of its term
structure positioning (its positioning relative to our interest
rate expectations), and particularly our yield curve steepening
trade. We were well-positioned when short-term rates declined
more than long-term rates; we believed the U.S. Federal Reserve
(the Fed) would continue lowering the Federal Funds
rate (causing shorter maturity Treasury yields to fall). At the
security level, the Funds outperformance versus the
benchmark was the result of our Treasury Inflation Protected
Securities (TIPS)selection, in which we focused on the
relative attractiveness of each security based on its risk,
liquidity and/or potential return. In the current low-yield
environment, TIPS have generally offered attractive yields and
tend to be a hedge against interest rate risk.






Market Review















Over the five-month reporting
period, breakeven inflation rates (the spread between real
yields and the nominal yields of similar-maturity issues) in the
U.S. rose concurrently with the increase in headline inflation
concerns. The increase in headline inflation was largely driven
by continuing media reports about soaring energy prices.
Meanwhile, economic data released over the period suggested that
a recession was growing more likely. The housing market
struggled as home prices continued to depreciate and sales of
existing and new homes declined. Along with surging oil and gas
prices, a weaker dollar, and a softer labor market, this served
to heighten inflation concerns.










The spread sectors
all non-Treasury investment grade sectors including federal
agency securities, corporate bonds, asset backed securities,
mortgage backed securities, and commercial mortgage backed
securities experienced unprecedented volatility over
the five-month reporting period. U.S. Treasury yields fell
dramatically as contagion from the subprime mortgage crisis
spread, sparking a global flight to quality and the massive
unwinding of leveraged positions. Credit and mortgage-related
sector spreads widened to historic levels as supply flooded the
market. Even short duration securities were affected.
















PORTFOLIO RESULTS















To restore liquidity, the Fed
took extraordinary steps to stave off a financial crisis. To
this end, the Fed created: Term Auction Lending
Facility (TALF) of Term Securities
Lending Facility (TSLF) of
Primary Dealer Credit Facility (PDCF) with no specified limit;
and lending facility specifically for Bear Stearns of
The Fed also cut the discount rate and the Fed
Funds rate, citing risks to economic growth. Meanwhile, the
federal government enacted policy changes that allow Fannie Mae
and Freddie Mac to purchase additional mortgage securities.










These actions helped to create
liquidity and ease supply and demand pressures on the market. As
result, mortgage-backed and asset-backed security prices
stabilized in the second half of March and began a tentative
rebound in April. The Fed is considering additional steps to
further improve liquidity.


















INVESTMENT
OBJECTIVE

















The Fund seeks real return
consistent with preservation of capital. Real return is the
return on an investment adjusted for inflation.








Portfolio Composition















The Fund invests primarily in
inflation protected securities of varying maturities issued by
the U.S. Treasury and other U.S. and non-U.S. government
agencies and corporations. The Fund can also diversify its
portfolio by investing in other fixed income securities such as
U.S. government, asset-backed, mortgage-backed, corporate
securities and high yield, as well as securities issued by
foreign corporate and government issuers.










Inflation protected securities
are income-generating instruments whose interest and principal
payments are adjusted for inflation. The inflation adjustment,
which is typically applied daily to the principal of the bond,
follows a designated inflation index, such as the Consumer Price
Index for Urban Consumers (CPIU). The Fund can provide investors
with a hedge against inflation, as it helps preserve the
purchasing power of an investment. Because of this inflation
adjustment feature, inflation protected bonds typically have
lower yields than conventional fixed-rate bonds.










We thank you for your investment
and look forward to your continued confidence.














Goldman Sachs U.S. Fixed
Income Investment Management Team
