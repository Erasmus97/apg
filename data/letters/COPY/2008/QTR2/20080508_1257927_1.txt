Dear Fellow Shareholder:
          The
Nebraska Tax-Free Income Fund’s total return for the first quarter of 2008 was
+0.4%, which consisted of approximately +0.9% from net interest income (after
deducting fees and expenses) and -0.5% from (realized and unrealized)
depreciation of our bonds. Our Fund’s primary benchmark, the Lehman Brothers
5-Year Municipal Bond Index, returned +1.9%. 
          The
table below shows the results of the Nebraska Tax-Free Income Fund over various
time periods through March 31, 2008, along with the Lehman Brothers 5-Year
Municipal Bond Index, our primary benchmark. The key measures of the Fund’s
portfolio (average maturity and duration) have most closely resembled this
Lehman Brothers index. As a reminder, we don’t manage the Fund to mimic any
particular index but thought it would be informative to provide an example of a
broadly constructed unmanaged index whose credit quality and maturity
composition were similar to the Fund.
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
Total Return*
 
Average Annual
  Total Returns*
 
 
 
 
 
 
 
1-Year
 
3-Year
 
5-Year
 
10-Year
 
15-Year
 
20-Year
 
 
 
 
 
 
 
 
 
Nebraska Tax-Free
  Income Fund**
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
#
 
 
6.2
 
 
 
 
4.2
 
 
 
 
3.4
 
 
 
 
4.7
 
 
 
 
4.9
 
 
 
 
5.8
 
 
 
These performance numbers reflect the
deduction of the Fund’s annual operating expenses which as stated in its most
recent Prospectus are 1.03% of the Fund’s net assets. This information
represents past performance and past performance does not guarantee future
results. The investment return and the principal value of an investment in the
Fund will fluctuate so that an investor’s shares, when redeemed, may be worth
more or less than the original cost. Current performance may be higher or lower
than the performance data quoted above. Performance data current to the most
recent month end may be obtained at www.weitzfunds.com/performance/monthly.asp.
 
 
*
All
  performance numbers assume reinvestment of dividends and/or income.
 
 
**
As of
  December 29, 2006, the Fund succeeded to substantially all of the assets of
  Weitz Income Partners Limited Partnership (the “Partnership”). The investment
  objectives, policies and restrictions of the Fund are materially equivalent
  to those of the Partnership and the Partnership was managed at all times with
  full investment authority by Wallace R. Weitz &amp; Company. The performance
  information includes performance for the period before the Fund became an
  investment company registered with the Securities and Exchange Commission.
  During these periods, the Partnership was not registered under the Investment
  Company Act of 1940 and therefore was not subject to certain investment
  restrictions imposed by the 1940 Act. If the Partnership had been registered
  under the 1940 Act during these periods, the Partnership’s performance might
  have been adversely affected. 
 
 
#
Index
  performance is hypothetical and is for illustrative purposes only. 
The following table shows a profile of our portfolio as of March
31:
 
 
Average Maturity
7.6 years
Average Duration
3.5 years
30-Day SEC Yield at 3-31-08
3.3%
Average Rating
AA
and Nebraska income taxes
Over 80%
Income subject to alternative
  minimum tax
Less than 5%
42
 
 
Fiscal Year in Review
less 
 
Ten-Year Munis as a Pct of Ten-Year Treasuries, Jan 96-Apr 08
Dear Fellow Shareholder:
Short-Intermediate
Income Fund Overview
          The
Short-Intermediate Income Fund’s total return for the first quarter of 2008 was
+2.3%, which consisted of approximately +0.9% from net interest and dividend
income (after deducting fees and expenses) and +1.4% from (net realized and
unrealized) appreciation of our bonds and other investments. Our first quarter
return was less than the 3.0% return of the Lehman Brothers Intermediate U.S.
Government/Credit Index, our Fund’s primary benchmark. For the fiscal year
ended March 31, 2008, our total return was +7.0%.
          The
table below shows the results of the Short-Intermediate Income Fund over various
time periods through March 31, 2008, along with the Lehman Brothers
Intermediate U.S. Government/Credit Index and two additional Lehman Brothers
Indexes with a shorter average maturity (1-3 and 1-5 year) which more closely
resemble the historical average life of our Fund.
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 Return*
 

 Total Returns*
 
 
 
 
 
1-Year
 
3-Year
 
5-Year
 
10-Year
 
 
 
 
 
Short-Intermediate Income
Fund
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
#
 
 
 
8.9
 
 
 
 
5.7
 
 
 
 
4.4
 
 
 
 
5.9
 
#
 
 
 
8.7
 
 
 
 
5.5
 
 
 
 
4.0
 
 
 
 
5.5
 
#
 
 
 
8.2
 
 
 
 
5.3
 
 
 
 
3.7
 
 
 
 
5.1
 
These performance numbers reflect the
deduction of the Fund’s annual operating expenses which as stated in its most
recent Prospectus are 0.67% of the Fund’s net assets. This information
represents past performance and past performance does not guarantee future
results. The investment return and the principal value of an investment in the
Fund will fluctuate so that an investor’s shares, when redeemed, may be worth
more or less than the original cost. Current performance may be higher or lower
than the performance data quoted above. Performance data current to the most
recent month end may be obtained at www.weitzfunds.com/performance/monthly.asp
.
 
*
All
 performance numbers assume reinvestment of dividends.
 
 
#
Index
 performance is hypothetical and is for illustrative purposes only. 
The following table and chart show a profile
of our portfolio
and asset allocation as of March 31:
 
 
Average
 Maturity
3.0 years
Average
 Duration
1.7 years
Average
 Coupon
4.2%
30-Day SEC
 Yield at 3-31-08
3.4%
Average
 Rating
AA+

