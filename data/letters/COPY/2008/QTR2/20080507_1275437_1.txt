Dear Fellow Shareholders,

The major indices continued where they left off late last year by posting
declines as we began Larger caps stocks held up better and outperformed
smaller cap stocks. Since our last letter, the Russell Value Index, the
Fund's benchmark index (the "Benchmark"), has declined for the months
ended February The S&P Index was off and the Dow Jones
Industrial Average fell for the same period. The Veracity Small Cap Value
Fund (Class R) slightly underperformed its Benchmark which lost For the
fiscal year ended February the Fund was down this compared
favorably to its Benchmark which was down

During the first half of the Fund's year, the domestic economy slowed, but
avoided recession. With economies abroad remaining robust and earnings staying
fairly healthy, a number of our stocks performed well, especially those tied to
global growth. Shaw Group (SGR) and Perini (PCR) were top performers as they
benefited from large scale construction projects. BE Aerospace (BEAV) and
Curtiss-Wright (CW) were solid performers that raised estimates on the back of a
solid aerospace cycle. GEO Group (GEO), a prison operator, continued to see
increased demand and higher per diems. The Fund was the beneficiary of a
takeover for Andrews (ANDW) during the summer which appreciated over since
initial purchase. The strong earnings trumped rising economic concerns as the
markets in general were up slightly. The healthy earnings were broad based among
many sectors and this helped our diversified approach to outperform quite
nicely.

In recent months, economic headwinds continued to build as the economy slowed
and a credit crunch developed. The source of the problem originated in the
mortgage space with numerous bankruptcies and near bankruptcies of mortgage
companies as the credit crunch took hold, choking off the lifeblood of the
industry. There was even serious talk of Countrywide (CFC), the largest home
lender in the country, going bankrupt. Credit spreads widened, repricing risk
across the board. These events crescendoed when the asset-backed commercial
paper market essentially shut down, leaving many businesses wondering where they
would get financing. In the middle of September, the Federal Reserve responded
by cutting the rate at the Discount Window in between meetings and by lowering
the Fed Funds rate at its September meeting. This bit of good news rallied
stocks in the months of September and October.

Housing concerns and the unfolding credit crunch continued to take their toll
with anxiety increasing that what started in the mortgage sector would spillover
into the rest of the economy. Spreads continued to widen and the market for
asset-backed commercial paper continued to contract. Many financials took large
write-downs and announced intentions to raise capital as a result of the housing
downturn and credit market turmoil. In response, the Federal Reserve continued
to cut rates and initiated a new Term Auction Facility to increase liquidity.
This effort was duplicated abroad with other Central Banks in Europe. The
Treasury department joined in on the act with a plan designed to freeze teaser
rates helping some homeowners faced with





higher mortgage rate resets. With lower rates domestically and overseas
economies remaining robust, inflationary concerns intensified as energy and
commodity prices surged into the beginning of this year. With this backdrop,
equity markets were in the red for the past months.

Energy continued to be the place to invest. It has been the only sector with
positive appreciation as the average energy name in the Benchmark was up more
than over the past months. On the other end of the spectrum, as recession
was increasingly being discounted, Technology, Consumer Discretionary and
Financials were down more than

The sector positioning of the Veracity Small Cap Value Fund, compared to the
Benchmark, was slightly underweight Energy and Basic Materials, which hurt
performance. Being underweight to Consumer Discretionary and Financial stocks
offset much of this. We were overweight Technology and Producer Durables and
this was a small negative to performance. Stock selection was a minor detractor
as a number of our Financials suffered.

A few names that we would like to highlight include Walter Industries (WLT), a
producer of metallurgical coal, was up as their end-markets remain red-hot
and they have been able to increase production to take advantage. Perrigo
(PRGO), a branded drug maker, appreciated as it has benefited from recent
drug approvals. Carrizo Oil & Gas (CRZO), an exploration and production company
focused on natural gas, increased as it benefited from higher natural gas
prices. CF Industries Holdings (CF) was up as it has beat expectations and
fertilizer demand remained strong. Alpharma (ALO), a drug maker, was up as
new product launches continued to gain steam.

The housing crisis and associated credit crunch impacted a number of our names.
IndyMac Bancorp (IMB) and BankUnited Financial (BKUNA) were off and
respectively as the expected impact on asset values, credit quality and capital
levels weighed heavily on both of these names. Corinthian College (COCO), a
provider of post-secondary education, became a victim of the credit crunch
suffering a decline. Sallie Mae reduced new funding, thus making it more
challenging for students to secure ongoing financial support. Arris Group (ARRS)
declined on disappointing first half earnings guidance. We expect their
earnings to accelerate over the next few quarters due to a new product cycle.

We have been focusing on larger, more liquid and higher quality companies within
the Benchmark for a number of quarters. Our overweight to larger and higher
quality companies were positive attributes during this tough time. The top
quintile of market cap companies outperformed by basis points relative to
the lowest quintile. The performance differential in quality was even greater as
the highest quintile outperformed by almost basis points against the
lowest quintile. Liquidity was not much of a differentiating factor over the
past months.

The markets have reminded us of the scene from the movie "Marathon Man" where
Dustin Hoffman's character is asked "Is it safe?" His response is, "Yes, it(C)s
safe, it(C)s very safe, it(C)s so safe you wouldn(C)t believe it." He is asked
once again "Is it safe?" His response is, "No. It(C)s not safe, it(C)s... very
dangerous, be careful." Just as the character had no idea what Lawrence Olivier
was referring to and went back and forth in his response, the market had no clue
about the depth of the financial problems. On the one hand the financial crisis
seemed to have no bottom as





reflected in bankruptcies, significant losses, capital raises and the emergency
sale of Bear Stearns to JP Morgan. On the other hand we had the Fed aggressively
lowering interest rates, TAF auctions and talk of reduced capital requirements
for Fannie Mae and Freddie Mac designed to improve liquidity and prevent an
economic slowdown from becoming a recession. Is it safe? The market certainly
thinks it's not safe; that it is dangerous. Our view is that we are beginning to
see the signs that the market is safe. We see the seeds being sown for better
times.

Our outlook for the months ahead is this: Stocks have been increasingly
discounting recession. This has been most apparent in the Financial, Technology
and Consumer sectors. We think that valuations have made it reasonable to start
increasing positions in these sectors.

We are changing our stance from the last year and a half where we believed that
with a slowing economy it made sense to position the Fund in stocks that were
larger cap, higher quality, more liquid and later cycle. The source of the
current market and economic crisis is financial. The crisis manifested itself in
the form of severely reduced liquidity in the debt markets, widening credit
spreads and a contraction in available credit. If we are right that the root of
the current turmoil is financial, then it seems to us that when it resolves,
credit spreads will narrow and debt availability will increase. In that
environment, we would expect to see Financials stocks significantly outperform.
We also think we will see smaller, less liquid, higher leveraged and lower
quality stocks outperform.

The question for us is when, not if. However, "when?" is a significant question
in the context of the type of stocks we believe will work. It is not a group
that we want to be in too early, but we also believe today's investing
environment is presenting us with opportunities not seen in twenty years. Our
plan is to incrementally move the Fund toward the above characteristics while
adding to Financials, Consumer Cyclicals and Technology. In addition, in an
environment where valuations are low, but company-specific risk is high, we
believe it is prudent to increase the number of holdings in the Fund as we
implement this incremental shift.

We appreciate your continued investment in the Fund and look forward to
participating with you in the better economic environment to come.

Sincerely,
