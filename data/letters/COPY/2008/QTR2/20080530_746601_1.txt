Dear fellow shareholders:
2
 
 
 
 
 
 
 
 
Dear fellow shareholders:
          Interest
rates declined and the yield curve steepened as a result of multiple reductions
in the targeted federal funds rate by the Federal Open Market Committee (FOMC),
which were unprecedented in both their frequency and magnitude. The 3-month
T-Bill yield declined the most during the past 12 months, dropping 3.71% to
yield 1.32% at March 31, 2008, while the 30-year maturity U.S. Treasury yield
fell by 0.54% to 4.31%. The Federal Reserve also aggressively continued to
employ multiple methods of providing liquidity to banks and primary dealers to
address the effects of the sub-prime mortgage crisis.
th
          With
respect to the U.S. economy, GDP growth slowed significantly in the first
quarter of 2008 and we expect that second quarter real GDP growth will be
slightly negative at -0.5%. To date, the key support factor for the U.S.
economy has been the consumer sector which accounts for 70% of U.S. GDP.
Changes in monthly real personal consumption expenditures (PCE) have been
slowly declining since August 2007 and is on pace to fall below 2%. This trend
has tracked the trend in unemployment and largely underpins our forecast for
weak real U.S. GDP growth through the first half of 2008. 
          The
current difficulties in the residential real estate market are likely to
persist for some time. Though housing starts appear to be near a bottom, new
houses for sale, new house prices, and existing houses for sale all appear to
have further to go in the correction phase. Despite this weakness, most homes
are likely worth more than they were five years ago. The weak housing market
has not prevented the annual headline CPI data from showing a sharp rise as
rising fuel and food prices have continued throughout the year. However, core
CPI (excludes food and energy) remains relatively mild at +2.4%.
          The
U.S. dollar exchange rate declined to 70.3 in March, as measured by the Federal
Reserve’s trade-weighted major currencies index. The decline has coincided with
the worsening sub-prime mortgage crisis. As we move toward resolution of the
U.S. credit crunch, the dollar is likely to rebound. 
          February’s
budget deficit of $176 billion was slightly worse than the expectation of $170
billion, and significantly worse than the $120 billion deficit recorded in
February 2007. For the first five months of fiscal 2008 through February,
receipts are up just +1.3 % while outlays have increased +10.2 %. 
For the fiscal 2008 budget,
the Office of Management and Budget (OMB) currently projects a deficit of $410
billion which includes the recently passed economic stimulus bill.
Strategy Summary
          We
expect the deceleration in U.S. economic growth to most likely bottom in the
first half of 2008 due to the large amount of fiscal and monetary stimulus in
place. However, we are forecasting the recovery to potential GDP growth to be
rather drawn out, as all of the economic fallout from the real estate
correction works its way through the system. As a result, we estimate that real
GDP growth will be +1.2% for calendar year 2008. Our forecast for CPI inflation
in 2008 is in a range between +3.0 % and +3.5 %.
          The
U.S. economy and financial markets experienced unprecedented difficulties over
the past nine months. Liquidity, or credit, crises are driven by fear resulting
from an inability to quantify the downside risk to various investments.
Likewise, liquidity crises all eventually end once enough clarity returns for
investors to be able to make rational risk/reward assessments. 
 increase
          The
first signs of the effects of “reset shock” appeared in the fourth quarter of
2006. This followed the first batch of subprime adjustable-rate mortgages that
had to contend with the Federal Reserve’s determined efforts to raise interest
rates. The sudden and unprecedented spike in defaults halted funding for
subprime borrowers by February of 2007. As a result, just as the largest waves
of resets were about to occur, subprime borrowers were denied the opportunity
to refinance into a fixed-rate mortgage. 
          Thankfully,
the underlying cause of the mortgage problems is now being addressed.
Specifically, the “reset shock” for all borrowers with an adjustable-rate
mortgage has been effectively eliminated for mortgages resetting after April 1,
2008. Also, nearly every mortgage that reset higher in 2007 
 
 
 
 
 
