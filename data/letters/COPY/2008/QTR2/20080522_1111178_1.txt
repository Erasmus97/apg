Dear Shareholder:

We are pleased to present this semiannual report for Dreyfus Premier Enterprise Fund, covering the six-month period from October through March

The reporting period proved to be one of the more challenging times for equity investors during this recent bull market run. The U.S. economy continues to sputter under the weight of a weakening housing market, and a credit
crisis that originated in the U.S. sub-prime mortgage sector continues to disrupt other areas of the financial markets. These developments dampened investor sentiment and produced heightened volatility in many segments of the stock market. Financial
stocks were hit particularly hard due to sub-prime related write-downs, as were other market sectors that historically have been considered sensitive to economic downturns.

The Federal Reserve Board and the U.S. government have adopted stimulative monetary and fiscal policies in an effort to boost market liquidity and the economy.While it is too early to tell how effective their actions will
be, the time is right to position your portfolio for the investment challenges and opportunities that may arise.As always, we encourage you to stay in close contact with your financial advisor, who can help you maintain a disciplined approach and a
long-term perspective, which historically have been key to investment success over the long run.

For information about how the fund performed during the reporting period, as well as market perspectives, we have provided a Discussion of Fund Performance given by the funds Portfolio Manager.

Thank you for your continued confidence and support.









DISCUSSION OF FUND PERFORMANCE

For the period of October through March as provided by James D. Padgett, CFA, Portfolio Manager

Fund and Market Performance Overview

For the six-month period ended March Dreyfus Premier Enterprise Funds Class A shares produced a total return of Class B shares produced Class C shares produced and
Class T shares produced In comparison, the funds benchmark, the Russell Growth Index (the Index), produced a total return of
All capitalization ranges within the U.S. stock market lost value amid an intensifying economic slowdown and a flight to quality among
investors, but smaller stocks were hit harder than larger ones. Because the fund focuses on micro-cap companies at the lower end of the small-cap range, it did face a performance headwind relative to its benchmark which consists of
companies with a larger weighted average market capitalization. Compared to the benchmark, the bulk of the funds relative performance shortfall occurred during the fourth quarter of Encouragingly, the fund achieved improved relative
performance (albeit still lagging the benchmark) when market conditions began to stabilize over the first quarter of

The Funds Investment Approach

The fund seeks capital appreciation.To pursue this goal,the fund normally invests at least of its assets in the stocks of micro-cap companies, which typically are small (under million market cap) and relatively
unknown.The fund may also invest in larger companies if we believe they represent better prospects for capital appreciation.Although the fund normally will invest in common stocks of U.S.-based companies,it may invest up to of its total assets
in foreign securities.The funds investments may include common stocks, preferred stocks and convertible securities, including those purchased in initial public offerings. The fund may also invest in securities issued by exchange-traded
investment companies, which are designed to track a specific equity index.

We look for companies with fundamental strengths that indicate the potential for strong profit growth.We focus on individual stock selection, searching one by one for companies with one or more of the following

The Fund







DISCUSSION OF FUND PERFORMANCE (continued)

characteristics: strong management teams; competitive industry positions; focused business plans; compelling product or market opportunities; strong business prospects; and the ability to benefit from changes in technology,
regulations or industry sector trends.

Economic and Credit Woes Undermined Small-Cap Stocks

U.S. stocks posted negative absolute returns over the reporting period due to a sharp downturn in the U.S. economy and an intensifying credit crunch that began in the sub-prime mortgage market and spread throughout the
financial markets. Concerns regarding declining U.S. housing values, surging energy and food prices and their impact on consumer spending weighed heavily on investor psychology. The Federal Reserve Board attempted to promote market liquidity and
forestall further economic deterioration by injecting capital into the banking system and reducing short-term interest rates from at the start of the reporting period to at the end.

In this turbulent market environment, investors shifted their focus away from domestic small-cap and micro-cap companies toward larger companies with histories of more consistent earnings growth in a variety of economic
climates. In our judgment, many fundamentally sound micro-cap companies were unfairly punished by indiscriminate selling among newly risk-averse investors.

Health Care Holdings Weighed on Performance

Damage from deteriorating investor sentiment was particularly severe among the funds health care holdings.InfuSystems Holdings,which rents infusion pumps for the oncology market, lost value as the companys
near-term profit targets began to look too optimistic. InfuSystems evolved from a SPAC (special purpose acquisition company) structure as it acquired its first operating business from I-Flow Corporation (another portfolio holding) during the
quarter. Unfortunately, low trading liquidity and relative obscurity have accentuated volatility in the price of InfuSystems shares. Among other health care positions that negatively impacted fund performance, Penwest Pharmaceuticals was hurt by
fears of generic competition for its pain management drug, and biotechnology firm Metabolix suffered after announcing a delay in production of a new generation of environmentally-friendly plastics.

The fund also realized disappointing performance from certain of our security selections in the consumer discretionary and financials sectors







due to unsettled conditions in the consumer and capital markets, respectively.The funds technology holdings generally performed in line with the benchmarks technology component overall. Beneath this, however,
Israeli networking equipment maker Radware and telephone headset company Plantronics ranked among the funds most significant laggards as they were impacted by fears that a slowing economy could hurt corporate IT spending.

The fund achieved better relative performance from individual stocks in a variety of market sectors. Health care consulting firm First Consulting Group and specialty medical products manufacturer Lifecore Biomedical were
both acquired for significant premiums during the period. Diversified specialty chemicals company Balchem Corporation executed its business plan well and hit its financial targets. Specialty drug developer Auxilium Pharmaceuticals benefited from
growing investor excitement in advance of the release of clinical trial data for an important new product. Transportation logistics company Hub Group rallied as investors anticipated the early stages of an economic recovery, and the company aided
this sentiment by reporting strong earnings.

Finding Opportunities in a Distressed Market

Despite ongoing market turmoil, we are optimistic regarding the longer-term prospects of micro-cap stocks. The current downturn has created more attractive valuations among a number of micro-cap companies with attractive
business fundamentals and bright growth prospects.While it is impossible to predict accurately when economic and market recoveries will begin, we believe that investors with patience, discipline and a long-term perspective are in a strong position
to benefit when they occur.

April








Total return includes reinvestment of dividends and any capital gains paid, and does not take into

Dear Shareholder:

We are pleased to present this semiannual report for Dreyfus Premier Natural Resources Fund, covering the six-month period from October

through March

The reporting period proved to be one of the more challenging times for equity investors during this recent bull market run. The U.S. economy continues to sputter under the weight of a weakening housing market, and a credit
crisis that originated in the U.S. sub-prime mortgage sector continues to disrupt other areas of the financial markets.These developments dampened investor sentiment and produced heightened volatility in many segments of the stock market. Financial
stocks were hit particularly hard due to sub-prime related write-downs,as were other market sectors that historically have been considered sensitive to economic downturns.

The Federal Reserve Board and the U.S. government have adopted stimulative monetary and fiscal policies in an effort to boost market liquidity and the economy.While it is too early to tell how effective their actions will
be, the time is right to position your portfolio for the investment challenges and opportunities that may arise.As always, we encourage you to stay in close contact with your financial advisor, who can help you maintain a disciplined approach and a
long-term perspective, which historically have been key to investment success over the long run.

For information about how the fund performed during the reporting period, as well as market perspectives, we have provided a Discussion of Fund Performance given by the funds Portfolio Managers.

Thank you for your continued confidence and support.











DISCUSSION OF FUND PERFORMANCE

For the reporting period of October through March as provided by Alexander S. Marshall and William E. Costello, Portfolio Managers

Fund and Market Performance Overview

For the six-month period ended March Dreyfus Premier Natural Resources Fund produced total returns of for Class A shares, for Class B shares, for Class C shares, for Class I shares and
for Class T This compares with the funds benchmark, the Standard &amp; Poors Composite Stock Price Index (S&amp;P Index),
which produced a total return of for the same The S&amp;P North American Natural Resources Sector Index, which more closely reflects the funds
composition, returned for the reporting

The stock market generally declined under pressure from slowing U.S. growth. However, natural resources-related stocks outperformed most other sectors of the market on the strength of high commodity prices, particularly in
the areas of energy and metals-and-mining, which were bolstered by robust global demand and the declining value of the U.S. dollar. As a result, the S&amp;P North American Natural Resources Sector Index produced higher returns than the funds
more broadly based benchmark. The fund outperformed both indices on the strength of good individual stock selections, particularly among independent energy exploration and production (E&amp;P) companies with significant exposure to
natural gas.

The Funds Investment Approach

The fund seeks long-term capital appreciation, normally investing at least of its assets in stocks of companies in natural resources and natural-resource-related sectors. While the fund typically invests in U.S.-based
companies, it may invest up to of its total assets in foreign securities, including emerging market securities. The fund invests in growth and value stocks, typically maintaining exposure to the major natural resources sectors. Using fundamental
research and direct management contact, we seek stocks of companies with strong positions in their

The Fund







DISCUSSION OF FUND PERFORMANCE (continued)

sector, sustained achievement records and strong financial conditions. We also look for special situations, such as corporate restructurings, turnarounds or management changes that could increase the stock price.

Benefiting from Strength in Natural Gas

The broad U.S. stock market was undermined during the reporting period by slowing U.S. economic growth and an intensifying credit crisis that originated in the sub-prime mortgage market and spread to other areas of the
financial markets. Despite these adverse domestic influences, the global economy generally continued its robust expansion. Rising demand for energy and raw materials from fast-growing economies, such as China and India, drove many commodity prices
sharply higher. Crude oil climbed by more than from approximately a barrel at the beginning of the reporting period to well over a barrel by March Natural gas rose by an even greater percentage, increasing from roughly
to over by the end of the reporting period.

With roughly of its holdings in oil-and-gas related stocks, the fund was well positioned to capitalize on these trends. Performance benefited from our emphasis on independent E&amp;P companies, which tend to be more
sensitive to rising commodity prices than major integrated energy companies. Top E&amp;P performers included Concho Resources and Continental Resources, which maintain mixed oil and gas operations, as well as Southwestern Energy and SandRidge
Energy, both of which focus primarily on natural gas. Another E&amp;P holding, Denbury Resources, gained ground due to its enhanced oil recovery capabilities, which frees additional oil from previously tapped deposits.

Good Stock Selections Boosted Returns

In other areas of the energy-related resources market, the fund benefited from good individual stock selections in the face of uneven sub-sector performance. Oil-services stocks generally lagged in early after a strong
run in However, the fund avoided or underweighted some of the areas weaker companies, and we emphasized several relatively good performers, such as Dawson Geophysical and Hornbeck Offshore Services. Similarly, among major integrated oil
and gas companies,









the fund steered clear of the areas poorer performers in favor of holdings such as Hess and BG Group, which delivered comparatively strong returns.

Non-energy related holdings produced more mixed results. Returns were bolstered by the funds move to avoid paper and forest-products stocks, and by investments in agricultural chemical companies, such as Monsanto and
Agrium. However underweighted exposure to gold stocks and disappointing timing in the purchase of steel producer Allegheny Technologies undermined the funds relative performance.

An Attractive Environment for Select Natural Resources

As of the end of the reporting period, we have continued to emphasize independent E&amp;P energy companies, particularly those with significant exposure to natural gas.At the same time, we have de-emphasized major
integrated oil and gas producers. This position reflects our view that petroleum prices are likely to remain at attractive levels, while natural gas remains undervalued. Among the funds non-energy holdings, we have maintained an overweighted
position among agricultural chemical stocks, which have benefited from intensifying demand for biofuels.We also have continued to hold relatively few gold stocks, which appear likely to depend on currency fluctuations for future gains.

April








Total return includes reinvestment of dividends and any capital gains paid, and does not take into
