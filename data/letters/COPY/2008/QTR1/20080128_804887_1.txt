Dear Shareholder:

We are pleased to present this annual report for Dreyfus Short-Intermediate Government Fund, covering the period from December through November

Volatility has returned to the U.S. stock and bond markets.The past few months have been filled with greater swings in security valuations compared to the past several years,as the economic cycle matured and a credit crisis
stemming from the sub-prime mortgage sector of the bond market has drastically affected other areas of the financial markets.A high degree of leverage within parts of the financial system has made these price fluctuations more intense than they
otherwise might have been. In the ensuing flight to quality among investors, U.S. Treasury bonds fared relatively well, while riskier fixed-income securities generally languished, offsetting some of the gains achieved earlier in the
reporting period.

In our view, these developments signaled a shift to a new phase of the credit cycle, including more prudent mortgage requirements and a normalization of credit terms for other loans after a sustained period of low
compensation for investment risk. Although we expect slower financial conditions in lower short-term interest rates from the Federal Reserve Board may help forestall a technical recession. In addition, turning points such as this may be a good
time to review your portfolio with your financial advisor, who can help you consider the allocation of your fixed-income investments in this changing market environment.

For information about how the fund performed during the reporting period, as well as market perspectives, we have provided a Discussion of Fund Performance given by the funds Portfolio Manager.

Thank you for your continued confidence and support.




Thomas F. Eggers




Chief Executive Officer




The Dreyfus Corporation




December













DISCUSSION OF FUND PERFORMANCE

For the period of December through November as provided by Catherine Powers, Portfolio Manager

Fund and Market Performance Overview

While the U.S. bond market was relatively stable over the first half of the reporting period, the second half saw heightened market volatility as a credit crisis originating in the sub-prime mortgage sector sparked a
flight to quality among investors. U.S. Treasury securities gained value in this environment, but other types of fixed-income securities languished.The fund underperformed its benchmark due to weakness among its holdings of agency
debentures and mortgage-backed securities, which are securities not represented by the funds benchmark.

For the period ended November Dreyfus Short-Intermediate Government Fund achieved a total return of In comparison, the
funds benchmark, the Merrill Lynch Governments, U.S.Treasury, Short-Term Years) Index (the Index), achieved a total return of

The Funds Investment Approach

The fund seeks to maximize total return, consisting of capital appreciation and current income.To pursue its goal, the fund normally invests at least of its assets in securities issued or guaranteed by the U.S.
government or its agencies or instrumentalities, and in repurchase agreements collateralized by such securities.The fund may invest up to of its assets in mortgage-related securities issued by U.S. government agencies, such as mortgage
pass-through securities issued by the Government National Mortgage Association, the Federal National Mortgage Association and the Federal Home Loan Mortgage Corporation, and collateralized mortgage obligations (CMOs). These instruments
include those backed by the full faith and credit of the U.S. government and those that are neither insured nor guaranteed by the U.S. government. The fund generally maintains an average effective duration of approximately three years or
less.

The Fund







DISCUSSION OF FUND PERFORMANCE (continued)

U.S. Government Securities Produced Mixed Results During the Credit Crisis

Most sectors of the U.S. bond market produced positive absolute returns for the reporting period, but those results were achieved amid a challenging market environment over the summer and fall. Beginning in late February
a higher-than-expected number of delinquencies and defaults among homeowners with sub-prime mortgages resulted in heightened market volatility and concerns that consumer spending might slow. By July, weakness in the sub-prime lending sector
had spread to other areas of the bond market, causing a flight to quality among fixed-income investors. Illiquid market conditions worsened as leveraged institutional investors were forced to sell their more liquid high quality bonds in order to
raise cash for redemptions. As risk aversion has picked up, we have seen greater downward pricing pressure on non-Treasury assets.

In an effort to promote greater liquidity and forestall a possible recession, the Federal Reserve Board (the Fed) reduced key short-term interest rates in August, September and October, which helped produce a
very brief market rebound during those months. However, mounting losses among major U.S. and global banks and intensifying concerns regarding the possibility of a U.S. recession derailed the rally in November.

Non-Treasury Holdings Constrained the Funds Relative Performance

Strategies that had supported the funds performance relative to its benchmark in previous reporting periods proved to be less successful during the credit crunch.We had allocated a substantial portion of the
funds assets to U.S. government agency securities, including agency debentures and agency-backed mortgage-related securities, in an attempt to capture higher yields than were available from U.S.Treasury securities.While these high quality
positions achieved positive absolute returns and fared better than lower-rated securities, they caused the funds returns to lag its Treasury benchmark.









Other strategies were more successful, helping to offset some sector-related weakness during the credit crisis. We maintained the funds average duration in a range that was slightly longer than the Index, which helped
the fund participate in relative strength toward the short end of the markets maturity range.We particularly focused on securities with two-year and five-year maturities, while de-emphasizing those with maturities.This yield curve
strategy benefited the funds return as the yield curve steepened.

Maintaining Caution in a Changing Market

Despite the Feds attempts to calm the markets through lower short-term interest rates and injections of liquidity, uncertainty has persisted with regard to the future impact of elevated energy prices, the housing
recession, ongoing sub-prime turmoil and mounting bank losses. On a brighter note, recent price dislocations have created opportunities to purchase U.S. government agency securities and other high-quality, short-duration assets at more attractive
valuations. However, we have not yet done so, as we currently prefer to maintain a cautious investment posture, including a larger-than-usual cash position, until the economic and market outlooks become clearer. In our view, these are prudent
strategies in todays uncertain market environment.

December








Total return includes reinvestment of dividends and any capital gains paid. Past performance is no
