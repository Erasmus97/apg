Dear Shareholder:

We are pleased to present this semiannual report for Dreyfus New York Tax Exempt Bond Fund, Inc., covering the six-month period from June through November

Volatility has returned to the municipal bond market.The past few months have been filled with greater swings in security valuations than weve seen in several years, as the economic cycle matured and a credit crisis
spread from the sub-prime mortgage sector of the taxable bond market to other areas of the financial markets, including municipal bonds. A high degree of leverage within parts of the financial system made these price fluctuations more intense than
they otherwise might have been.While we saw few changes in the underlying credit fundamentals of municipal bonds,the tax-exempt market nonetheless suffered bouts of difficult liquidity.

In our view, these developments signaled a shift to a new phase of the credit cycle in which the price of risk has increased. Although the housing downturn and sub-prime turmoil may persist, fiscal conditions so far have
remained sound for most municipal bond issuers, and lower short-term interest rates from the Federal Reserve Board may help forestall a technical recession.Turning points such as this one may be a good time to review your portfolio with your
financial advisor, who can help you consider whether to reposition your investments in this changing market environment.

For information about how the fund performed during the reporting period, as well as market perspectives, we have provided a Discussion of Fund Performance given by the funds Portfolio Manager.

Thank you for your continued confidence and support.




Thomas F. Eggers




Chief Executive Officer




The Dreyfus Corporation




December













DISCUSSION OF FUND PERFORMANCE

For the period of June through November as provided by Joseph Darcy, Portfolio Manager

Fund and Market Performance Overview

Declining U.S. housing values and a credit crisis over the summer of led to heightened volatility in fixed-income markets, including among municipal bonds, as investors reassessed their attitudes toward risk.The
funds return lagged that of its benchmark, which is not limited geographically to New York, and which does not reflect fees and expenses like the fund. However, the fund produced a higher return than its Lipper category average, primarily due
to the funds longstanding emphasis on quality and liquidity.

For the six-month period ended November Dreyfus New York Tax Exempt Bond Fund achieved a total return of The Lehman Brothers
Municipal Bond Index, the funds benchmark index, achieved a total return of for the same The average total return for all funds reported in the Lipper
New York Municipal Debt Funds category was

The Funds Investment Approach

The fund seeks as high a level of current income exempt from federal, New York state and New York city income taxes as is consistent with the preservation of capital. To pursue this goal, the fund normally invests
substantially all of its assets in municipal bonds that provide income exempt from federal, New York state and New York city personal income taxes.The dollar-weighted average maturity of the funds portfolio normally exceeds years, but the
fund may invest without regard to maturity. The fund will invest at least of its assets in investment-grade municipal bonds or the unrated equivalent as determined by Dreyfus. The fund may invest up to of its assets in municipal bonds rated
below investment grade ( junk bonds) or the unrated equivalent as determined by Dreyfus.

The Fund







DISCUSSION OF FUND PERFORMANCE (continued)

We may buy and sell bonds based on credit quality, market outlook and yield potential. In selecting municipal bonds for investment, we may assess the current interest-rate environment and the municipal bonds potential
volatility in different rate environments. We focus on bonds with the potential to offer attractive current income, typically looking for bonds that can provide consistently attractive current yields or that are trading at competitive market prices.
A portion of the funds assets may be allocated to discount bonds, which are bonds that sell at a price below their face value, or to premium bonds, which are bonds that sell at a price above their face value.The
funds allocation either to discount bonds or to premium bonds will change along with our changing views of the current interest-rate and market environment.We also may look to select bonds that are most likely to obtain attractive prices when
sold.

Interest Rates Fall Amid Economic and Credit Concerns

After a sustained period of relative stability, market conditions changed dramatically over the summer of when turmoil in the sub-prime mortgage sector of the taxable bond market spread to other fixed-income
sectors.Although we saw little evidence of credit deterioration among municipal bond issuers, the tax-exempt market was affected by selling pressure from highly leveraged hedge funds and other institutional investors, which were forced to sell
creditworthy securities to meet redemption requests and margin calls.

Difficult liquidity conditions prompted the Federal Reserve Board to cut the discount rate in August and the federal funds rate in September and October, the first reductions in short-term rates in more than four years.The
market generally responded favorably to these moves,enabling municipal bond prices to erase some, but not all, of their earlier declines.

New York was adversely affected by the U.S. housing downturn and the credit crisis, but the states fiscal condition generally has been supported by a diverse economy and recent efforts to adopt budgetary and spending
controls. However, challenges remain, as recent losses among major Wall Street firms may constrain tax revenues at a time when the state is contending with future budget deficits and a rising debt load.









Quality and Liquidity Helped Support Fund Performance

The fund continued to benefit from its seasoned holdings of income-oriented bonds, many of which were purchased with higher coupon rates than are available today. Moreover, our consistent emphasis on higher-quality bonds
with strong liquidity characteristics enabled the funds holdings to fare relatively well during the downturn. For example, we tended to favor general obligation bonds and securities backed by revenues from essential-services facilities.We
maintained the funds weighted average maturity in a range that was roughly in line with industry averages, helping it capture the benefits of falling interest rates without assuming unnecessary duration risk.

Because fixed-income markets have remained volatile, we have retained a relatively conservative investment posture, including a focus on higher quality securities with maturities of years or less. Should we see a
sustainable improvement in market conditions, we may increase the funds weighted average maturity in seeking to capture higher current yields. In the meantime, we believe that maintaining high levels of credit quality and liquidity represent
prudent strategies in todays uncertain investment environment.

December








Total return includes reinvestment of dividends and any capital gains paid. Past performance is no
