Dear Shareholders,

We are pleased to present you with the financial report for the High Watermark
Funds for the annual period ending October

The financial markets faced many challenges during the annual period. Rising
energy prices, deterioration in the housing market, concerns in the sub-prime
mortgage market which has lead to a credit crisis and a Federal Reserve easing
of interest rates in September are among the major events of the period. Much
uncertainty still hangs over both the equity and fixed income markets as a
debate over whether the US is heading into a recession is waged.

The High Watermark Funds provide investors with risk-controlled exposure to the
S&P This benefit is obtained through the utilization of proprietary
methodology developed by the Funds' subadviser, Trajectory Asset Management
LLC. In addition, subject to certain conditions, the Funds offer another unique
benefit to preserve both principal and investment gains over the life of each
Fund.

These funds do not purchase individual securities but rather invest in S&P
Index Futures and related options and high-quality, fixed-income instruments.
Therefore, it is important to understand the broad movements of the S&P
the U.S. Government bond market, and each Fund's asset allocation in order to
evaluate their performance. The commentary from the Trajectory portfolio team
enclosed in this report provides useful information.

We believe these Funds are ideal vehicles for retirement savings, college
savings or any savings goal with a set time horizon. Furthermore, we believe
that the High Watermark Funds can play an important role within diversified
portfolios. Be sure to sit down with your financial adviser to review your
portfolio.

We thank you for including AIG SunAmerica Mutual Funds in your investment plan.
We value your ongoing confidence in us and look forward to serving your
investment needs in the future.

Sincerely,
