Dear Shareholder:

We are pleased to present the annual shareholder report for the SunAmerica
Senior Floating Rate Fund. Included is a discussion about the market conditions
that have shaped the investment environment during the annual period ended
December

The year was certainly an interesting and challenging one in the financial
markets. Concerns over the state of the housing market and the potential impact
on the consumer, along with price increases in food and energy transitioned
during the summer to worries about subprime mortgages. The fear of a credit
crunch weighed on investors who became risk-averse as the stock market sold off
in both August and November. In September, the Federal Reserve Open Market
Committee lowered the Federal Funds target rate for the first time in
signaling concern that the economy's prospects were then at least equal with
inflationary concerns. Unease about the housing market and the potential
dampening effect of constrained lenders continued through year-end with the
Federal Reserve continuing to ease. The Federal Funds rate ended the year at
basis points lower than where it began the year.

The markets have posed challenges to managers in all asset classes,
including bank loans. Senior, secured and floating rate loans gained increased
prominence in the press for their use in financing private equity buyouts in
however, we believe that this asset class continues to offer strong
risk/return characteristics in building a diversified portfolio.

We remain diligent in the management of your assets and thank you for your
continued investment in our Fund.

Sincerely,
