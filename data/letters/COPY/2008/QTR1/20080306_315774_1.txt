Dear Shareholder:

was a very challenging year for equity investors. While the year started
off on a strong note, fears of a pullback in the housing market caused a
correction in the early spring. The markets then moved higher, only to face
more serious fears in the late summer over credit derivatives and the housing
market. Although the market moved higher again after the mid-August correction,
it peaked for the year in mid-October. For the year, most U.S. market indices
showed single digit gains, but the year was more volatile than one might have
initially predicted.

For the year, the Adams Harkness Small Cap Growth Fund increased versus
a return for its benchmark, the Russell Growth Index. We attribute
the Fund's performance to several factors. First, as you know, we added to our
research staff at the end of we believe that this additional staff had a
positive impact on performance. Second, the market environment was suited to
our strengths. Ours is a stock-picking process: we attempt to identify those
few companies that we think can deliver above average earnings growth in a wide
variety of economic scenarios, because their growth is being driven by secular,
not cyclical, trends. As many investors became more concerned about earnings
growth, as the economy slowed throughout some became willing to pay more
for the kinds of companies in which the Fund invests.

FOR A LONGER PERSPECTIVE, THE FUND'S AND SINCE INCEPTION
AVERAGE ANNUAL TOTAL RETURNS FOR THE PERIOD ENDED DECEMBER
WERE AND RESPECTIVELY. PERFORMANCE DATA QUOTED REPRESENTS PAST
PERFORMANCE AND IS NO GUARANTEE OF FUTURE RESULTS. CURRENT PERFORMANCE MAY BE
LOWER OR HIGHER THAN THE PERFORMANCE DATA QUOTED. INVESTMENT RETURN AND
PRINCIPAL VALUE WILL FLUCTUATE SO THAT AN INVESTOR'S SHARES, WHEN REDEEMED, MAY
BE WORTH MORE OR LESS THAN ORIGINAL COST. THE FUND'S RETURN ASSUMES THE
REINVESTMENT OF DIVIDEND AND CAPITAL GAIN DISTRIBUTIONS. FOR THE PERIOD
REPORTED, SOME OF THE FUND'S FEES WERE WAIVED OR EXPENSES REIMBURSED;
OTHERWISE, TOTAL RETURN WOULD HAVE BEEN LOWER. AS STATED IN THE CURRENT
PROSPECTUS, THE FUND'S ANNUAL OPERATING EXPENSE RATIO (GROSS) IS
HOWEVER, THE FUND'S ADVISER HAS AGREED TO CONTRACTUALLY WAIVE A PORTION OF ITS
FEES AND/OR REIMBURSE EXPENSES SUCH THAT TOTAL OPERATING EXPENSES DO NOT EXCEED
WHICH IS IN EFFECT UNTIL APRIL SHARES REDEEMED OR EXCHANGED
WITHIN DAYS OF PURCHASE WILL BE CHARGED A REDEMPTION FEE. RETURNS
GREATER THAN ONE YEAR ARE ANNUALIZED. FOR THE MOST RECENT MONTH END
PERFORMANCE, PLEASE CALL OR VISIT THE FUND'S WEBSITE AT
WWW.AHSMALLCAP.COM.

Several events occurred in on the macroeconomic front which we believe
will be significant to the investment climate for many years to come: the
Federal Reserve reversed its interest rate policy from one of rate increases to
one of rate cuts, and economic growth slowed significantly. Both of these
events have the potential to enhance the value of growth stocks. The Federal
Reserve's previous policy of interest rate increases put a cap on the
price/earnings multiples that many investors were willing to pay for earnings
growth; as rates rose, that cap went down, putting downward pressure on
multiples. At the same time, when economic growth was more robust, earnings
growth was taken for granted by investors -- earnings growth of or better
was assumed by the investors for most companies. As the economy slowed
throughout however, earnings growth also slowed, from high single digits
for the S&P Index in the first quarter of to negative single digits
forecasted for the fourth quarter of This slowdown in earnings growth
made some investors willing to pay higher prices for those companies that
continued to deliver above average earnings growth. Both






- --------------------------------------------------------------------------------
ADAMS HARKNESS SMALL CAP GROWTH FUND
SHAREHOLDER LETTER
DECEMBER
- --------------------------------------------------------------------------------

of these changes we view as "secular" as opposed to "cyclical". We see both
slower growth and lower rates being part of the economic backdrop for some time
to come, and we believe that this will be a positive backdrop for our
investment strategy.

From a sector viewpoint, we were well positioned in being underweight in
financial stock, no exposure to Real Estate Investment Trusts (REITs) and
overweight in healthcare, consumer and technology stocks for most of the year.
The attribution work indicates, however, that it was individual stock selection
that was the major contributor to the Fund's performance. In that respect, we
had something very unusual happen in that we believe is worth mentioning:
we had quite a few takeovers in the Fund. We do not normally see a lot of
takeovers in our strategy, as growth companies are not usually targets of
takeovers because such acquisitions tend to be dilutive to other corporations,
given the price/earnings multiples they carry, and they are not cheap enough in
the public market to be attractive as LBO's (leveraged buyouts). However, in
large corporations were buyers of smaller corporations that could provide
them with new avenues of business. We had over ten corporations that either
were bought by larger corporations in or had agreements to be acquired
announced; among the more notable were Ventana Medical Systems, and Pharmion
Corporation. We believe that this is a trend that will continue as larger
corporations look for new avenues of growth; we caution, however, that we do
not invest in companies for their takeover potential, nor do we consider that
possibility in any of our analysis or valuation work.

As you know, our investment discipline pushes us to take losses quickly, as a
risk mitigation tool. While there were holdings that declined during the year,
the majority of them were smaller holdings and thus did not have a significant
negative impact on the performance of the overall portfolio. Among them were
Optium Corporation, Compellent Technologies, and Opnext Inc.

As we write this, the consensus on Wall Street has become that a recession is
inevitable, and the issue is the length and the severity of it. The current
economic climate reminds us in many ways of the situation that existed after
the Savings and Loan collapse in the timeframe. In that case, a mild
six-month recession did occur, and while the economy did come out of the
recession reasonably quickly, it grew at a very slow pace for a number
of years after the recession was over. We think the current environment will
unfold in a very similar manner. The system will heal itself, but it will take
time. As a result of all of the issues facing housing and the banking system,
we believe that Gross Domestic Product (GDP) will remain in the range for
some number of years to come. As the rest of the world slows slightly, which we
believe it will, inflation issues will ease. What we are describing is, in
general, a very positive climate for domestic equities. What makes it
challenging is that in this kind of slower economic backdrop, not every
corporation will be able to grow its earnings. Thus, it will continue to be a
stock-picker's market.

We believe that our continued efforts to identify those companies that have the
potential to grow their earnings at an above-average rate, even given the
slowing economy, could provide us with the biggest opportunities as well as our
greatest challenges. The resources of the four professionals who form the small
cap growth investment team at AH Lisanti Capital Growth, LLC, the investment
adviser to the Adams Harkness Small Cap Growth Fund, are focused on meeting
those challenges.






- --------------------------------------------------------------------------------
ADAMS HARKNESS SMALL CAP GROWTH FUND
SHAREHOLDER LETTER
DECEMBER
- --------------------------------------------------------------------------------


We thank you for investing in the Fund. We wish you and those close to you a
happy, healthy and prosperous

Sincerely,

Dear Fellow Shareholder:

was a great year for the Jordan Opportunity Fund, as growth stocks
continued to outperform throughout the year. For the S&P Index was up
and the Fund was up We expect this emergence of outperformance
by growth stocks to last several more years, as economic momentum becomes more
subdued and growth stocks gain premium valuations. Thus, we believe the stock
picking environment that occurred in should persist throughout Due
to our expectation for a sharp but short Gross Domestic Product (GDP)
deceleration, we remain focused on companies that will continue to demonstrate
strong growth and whose valuations have already priced in a cyclical slowdown.

FOR A LONGER TERM PERSPECTIVE, THE FUND'S AVERAGE ANNUAL TOTAL RETURNS FOR THE
FIVE AND TEN YEAR PERIOD HAS BEEN AND RESPECTIVELY. THE
PERFORMANCE QUOTED REPRESENTS PAST PERFORMANCE AND DOES NOT GUARANTEE FUTURE
RESULTS. INVESTMENT RETURN AND PRINCIPAL VALUE OF AN INVESTMENT IN THE FUND
WILL FLUCTUATE SO THAT AN INVESTOR'S SHARES, WHEN REDEEMED, MAY BE WORTH MORE
OR LESS THAN THEIR ORIGINAL COST. AS STATED IN THE CURRENT PROSPECTUS, THE
FUND'S PROJECTED ANNUAL OPERATING EXPENSE RATIO (GROSS) IS SHARES
REDEEMED OR EXCHANGED WITHIN DAYS OF PURCHASE WILL BE CHARGED A
REDEMPTION FEE, SUBJECT TO LIMITED EXCEPTIONS, WHICH ARE MORE FULLY DESCRIBED
IN THE FUND'S PROSPECTUS. DURING THE PERIOD, CERTAIN FEES WHERE WAIVED AND/OR
EXPENSES REIMBURSED; OTHERWISE, RETURNS WOULD HAVE BEEN LOWER. THE CURRENT
PERFORMANCE MAY DIFFER FROM THAT QUOTED. FOR MOST RECENT MONTH END PERFORMANCE,
AND/OR A PROSPECTUS, PLEASE CALL

PLEASE SEE PAGE FOR ADDITIONAL PERFORMANCE INFORMATION CONCERNING FUND
PERFORMANCE.

Energy was a very positive sector for the Fund in and we retain
overweight positions in energy industries, including oil service, exploration &
production, and alternatives such as solar power. While we understand that
these have historically been cyclical industries and have often suffered in a
decelerating economic environment, we believe that the energy theme is a
secular growth story and the stocks remain cheap relative to the market and
their historical valuations. Companies such as Schlumberger Ltd. (SLB) and
Transocean Inc. (RIG) have been strong performers for the Fund throughout





JORDAN OPPORTUNITY FUND
A MESSAGE TO OUR SHAREHOLDERS
DECEMBER

and should a pullback in the price of oil negatively affect this sector,
we believe it would provide an opportunity for it to strongly outperform the
market for the rest of the

Health care continues to be our other favorite sector, as it should be rewarded
with a premium valuation as earnings in other sectors decline. The upcoming
presidential election may cause some volatility in health care segments this
year, but we would view sell-offs as buying opportunities. Health care may
continue to benefit from strong demographics and necessary increases in medical
spending for the foreseeable future. Our holdings include large capitalization
pharmaceutical and biotechnology companies with strong drug pipelines, high
tech medical device manufacturers, life science tools providers, and managed
care organizations.

The Fund also has a small position in metals and materials as well, including
gold/silver producers who may benefit from increasing economic uncertainty. We
also recognize that market volatility could create attractive entry points in
companies that benefit from the ongoing infrastructure build in developing
nations. In addition to our holdings in copper miners, we are monitoring
opportunities in other materials providers.

Additionally, late-cycle multi-national industrial companies, along with the
technology sector, was a highlight for the Fund in Companies with
exposure to next-generation wireless handsets, internet advertising and
communications infrastructure, such as Apple (AAPL) and Google (GOOG) helped
performance in these areas throughout the year. It is important to note that
going into the Fund has significantly reduced its exposure to these
stocks due to high valuations in a weakening retail economy.

On the negative side, the Fund had a medium weight in financial stocks until
the quarter of mainly in the form of brokerage stocks such as
Goldman Sachs (GS), Merrill Lynch (MER), and Morgan Stanley (MS). This was one
of the best performing groups of the quarter, and one of the worst
performing groups of the and quarters. This brokerage weighting
caused some drag on overall performance.

Looking back on the year as a whole, there was a short burst of stronger
economic activity in the third quarter, when the US economy grew at a





JORDAN OPPORTUNITY FUND
A MESSAGE TO OUR SHAREHOLDERS
DECEMBER

annual rate. But due to inventory builds and net export growth, we believe this
burst was an anomaly against the prevailing trend of a slowing economy. The
domestic economy is clearly afflicted by the spreading credit crunch and the
steep decline in the US housing market. In fact, the acceleration in the third
quarter will likely make any subsequent return to weak conditions feel even
more abrupt. The momentum of the developing economies has been the largest
positive economic influence, as export growth has remained very strong. The
continued strength of the developing countries, when viewed against the
weakness of developed countries, is unprecedented and will likely cause an
overall global economic slowdown in the months ahead.

We agree with the conventional wisdom that the economy will "brush with
recession", with a recovery later in In addition to the credit market
trauma and the crisis in housing, the highly respected Economic Cycle Research
Institute recently indicated an economic contraction has started, based on its
weekly leading indicator index. This dovetails with recent weak readings of
manufacturing and retail activity. At this time, however, we do not believe
that the contraction will be either long or severe because: unemployment in
the US is still very low; export growth will likely remain strong; the
depressed dollar has made the pricing of US products and services highly
competitive; and the duration of the current slowdown in interest rate
sensitive areas, such as housing, would indicate a potential bottoming out in
the latter half of the year.

The Federal Reserve has cut interest rates and we are optimistic they may
continue to do so during the first half of the year due to the weak economic
conditions described above. In fact, it appears to be "behind the curve", and
may be forced to accelerate rate cuts. During the last two Federal Open Market
Committee (FOMC) meetings, the Federal Reserve indicated renewed concern in
fighting inflation, and less concern regarding the weakening economy and rising
credit spreads. While recent inflation readings have been high, as the global
economy slows, inflation pressure will inevitably recede. As the economic news
gets uglier in the months ahead, we expect the Federal Reserve to get more
aggressive in its accommodative policy stance.

In conclusion, was a great year for growth stocks, which, as a sector,
started to outperform in late We believe this recent emergence of growth
outperformance to last several more years, as economic momentum may become more
subdued and growth stocks may be poised to gain premium





JORDAN OPPORTUNITY FUND
A MESSAGE TO OUR SHAREHOLDERS
DECEMBER

valuations. Thus, the stock picking environment that occurred in should
persist throughout We fully expect to be an opportunistic year and
that it will require active investment strategies. Many industries will
struggle, but strong growth themes should continue to prosper.

Please feel free to contact us, or visit our website,
www.jordanopportunity.com, if you have any comments or questions.

Sincerely,

Dear Fellow Shareholder,

For the year ended December the Polaris Global Value Fund ("the
Fund") had a total return of while the MSCI World Index, net, the
Fund's benchmark, advanced

is only the fifth annual period in the Fund's history that
performance has not exceeded the Fund's benchmark. Since one of the last three
years is below the benchmark, the trailing three-year performance lags the
benchmark. However, the and inception-to-date returns
continue to exceed the benchmark. The Fund's returns have been accompanied by
lower market risk, as measured by the beta statistic of since the Fund's
inception (volatility measurement relative to the MSCI World Index).

The following table summarizes total returns through December Please
refer to page for the Fund's annual return.


PERFORMANCE DATA QUOTED REPRESENTS PAST PERFORMANCE AND IS NO GUARANTEE OF
FUTURE RESULTS. CURRENT PERFORMANCE MAY BE LOWER OR HIGHER THAN THE PERFORMANCE
DATA QUOTED. INVESTMENT RETURN AND PRINCIPAL VALUE WILL FLUCTUATE SO THAT AN
INVESTOR'S SHARES, WHEN REDEEMED, MAY BE WORTH MORE OR LESS THAN ORIGINAL COST.
FOR THE MOST RECENT MONTH END PERFORMANCE, PLEASE VISIT THE FUND'S WEBSITE AT
WWW.POLARISFUNDS.COM. AS STATED IN THE CURRENT PROSPECTUS, THE FUND'S ANNUAL
OPERATING EXPENSE RATIO (GROSS) IS SHARES REDEEMED OR EXCHANGED WITHIN
DAYS OF PURCHASE WILL BE CHARGED A FEE. FUND PERFORMANCE SHOWN FOR
PERIODS OF DAYS OR LESS DOES NOT REFLECT THIS FEE; OTHERWISE, IF REFLECTED,
THESE RETURNS WOULD HAVE BEEN LOWER. RETURNS GREATER THAN YEAR ARE
ANNUALIZED. SEE PAGE FOR ADDITIONAL DISCLOSURE.

PERFORMANCE ANALYSIS:

After a first quarter of outperformance relative to the benchmark, the Fund
gave back the gains in subsequent quarters. Holdings declined across several
countries and industries - a reaction to investors' concerns yet inconsistent
with reports of individual company business results.

During our research process clearly identified values in non-U.S. banks:
we increased our financial holdings in Germany and Ireland. The Fund's U.S.
bank investments generally have strong balance sheets, good customer funding
and clean loan portfolios - yet these holdings had lackluster performance. As
these banks report earnings, many investors may ultimately pinpoint and reward
solidly managed banks.

International bank holdings of the Fund, such as Anglo Irish and Bank of
Ireland, were down early in the fourth quarter but recovered and
respectively from quarter lows, backed by better-than-expected preliminary





- --------------------------------------------------------------------------------
POLARIS GLOBAL VALUE FUND
A MESSAGE TO OUR SHAREHOLDERS
DECEMBER
- --------------------------------------------------------------------------------

reports and reassuring outlooks. Both banks have little exposure to structured
investment vehicles, collateralized debt obligations or other tenuous
investment vehicles that have hindered some U.S. and non-U.S. financial
institutions. Both banks have real estate exposure that is performing quite
well and is well supported by the cash flow of borrowers.

We believe companies with ample free cash flow provide management with multiple
tools to weather economic or market declines. The Fund holds many companies
that use stock buybacks to enhance shareholder value. Two portfolio companies,
a U.S. electrical equipment distributor and a U.K. homebuilder that experienced
stock price declines of - announced stock repurchases of approximately
of shares outstanding. Since the companies generate substantial cash flow,
and company management believes their shares are undervalued, the firms are
able to undertake multiple strategies for shareholders.

The following table shows the Fund's asset allocation at December


Table may not foot due to rounding

Fund management continues to find better values overseas; consequently, the
Fund remains underweight in North America relative to the World Index. The
Fund's Scandinavian and Japanese holdings are weighted greater than the
benchmark, while investments in continental Europe are less heavily weighted.
Emerging markets in the portfolio, but not the World Index, include South
Africa, Korea, Thailand and Mexico.

Among sectors, the Fund's more economically-sensitive investments (materials,
industrials and consumer discretionary) were overweight. While these sector
stocks generally suffered setbacks, there were some strong performances from
growth drivers including Yara International (materials) and Continental AG
(industrial), resulting in these companies appearing in the top holdings at
year end. The overweight position in financials reflects management's view that
this sector offers some of the better values in the world.

Based on the extreme declines in many company valuations worldwide, management
is actively making portfolio changes and evaluating a host of new investments.





- --------------------------------------------------------------------------------
POLARIS GLOBAL VALUE FUND
A MESSAGE TO OUR SHAREHOLDERS
DECEMBER
- --------------------------------------------------------------------------------


INVESTMENT ENVIRONMENT AND STRATEGY:

Behavioral finance research suggests that many investors project their most
recent experiences far into the future. In today's panic-stricken markets,
investors fear that credit tightening may lead to a recession. Under such
uncertainty, investor behavior has historically led to uncontrolled selling
resulting in irrational pricing of assets and stocks.

The recession logic hinges on rapid contraction of credit and extreme economic
fragility with no possibility of corrective behavior by individuals, companies
or government. Such a scenario does not consider the dynamic flexibility of the
U.S. economy. Rather investors seem to assume the once-powerful and diversified
U.S. economy has become dependent for its growth on financial engineering
whereby the largest and most aggressive financial firms create greater and
greater leverage in the economy.

Furthermore, it is U.S.-centric to believe that the trillion global
financial system will be unable to function because, as investors fear, the
billion in U.S. sub-prime loans will all become worthless. This implies
that values of homes, secured by sub-prime mortgages, will go to zero, a highly
improbable event. The world economy is much broader and stronger than news
headlines would suggest, especially in economies with billions of emerging
consumers. The sheer weight of this emerging economic power is greater than the
financial cost of mortgage loan losses in a fraction of the U.S. market. It is
our opinion that global financial markets are waiting to take advantage of low
priced assets, and that well-managed companies can take away business from
their less competent competitors through this cycle. Even in tough markets,
good management can create value for shareholders.

In the near term, Fund management remains constructive about global market
valuations, buying companies at compelling valuations. In fact, we haven't seen
such opportunity since Behavioral finance indicates that irrational
thinking often leads to market inefficiencies. Our investment strategy requires
patience, waiting for investors to panic and creating an environment conducive
to buying companies priced to provide returns in excess of the benchmark's
return. This strategy has contributed to the Fund success over the past
years; we believe this strategy will prove fruitful in future markets.

CAPITAL GAINS DISTRIBUTION UPDATE

On December the Polaris Global Value Fund distributed a short-term
capital gain of per share and a long-term capital gain of per
share to shareholders of record as of December The gains were
primarily realized in the first six months of the year, when several portfolio
companies were sold following company acquisitions.

The Fund paid an income dividend of per share for shareholders of record
on December with an ex-dividend date of December This
dividend was paid from interest and dividends received on investments during


On the day that a fund distributes capital gains to shareholders, the fund's
net asset value per share drops by the amount of the distribution per share, to
reflect that the distribution has been paid out. The drop in the net asset
value does not reflect a loss in the shareholder's overall investment value,
but instead indicates that a portion of





- --------------------------------------------------------------------------------
POLARIS GLOBAL VALUE FUND
A MESSAGE TO OUR SHAREHOLDERS
DECEMBER
- --------------------------------------------------------------------------------

that value has been given to the shareholder as a capital gain. Keep in mind
that there may also be appreciation or depreciation in the fund value from the
market activity on that day that will also be reflected in the NAV.

We welcome your questions and comments.

Sincerely,
