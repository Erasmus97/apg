Dear Shareowner,
- --------------------------------------------------------------------------------
Staying diversified and keeping your portfolio invested in the markets are two
general investment principles that have served investors well over time. They
were particularly useful guides in the second half of when an otherwise
healthy long-term bull market was buffeted by problems in the financial
services industry and the emergence of worries about a slowing economy.

After an extended period of steady growth with sustained low unemployment and
low inflation, the U.S. economy ran into difficulty as drew to a close.
Problems in the financial system tied to poor practices in the mortgage
financing industry and the end of home price appreciation forced investors and
bankers to mark down the value of assets on their balance sheets by over one
hundred billion dollars. A late-summer credit crunch forced central banks in
the United States and Europe to act in the role of "lender of last resort" to
keep credit markets functioning. As the repercussions of the credit crunch and
falling home prices were felt in the real economy, unemployment rose and
consumer confidence fell. Inflation concerns moved to the back burner for the
Federal Reserve, which lowered interest rates, first gradually, then more
rapidly, as concern grew that falling home prices and disruptions in financial
markets posed a significant threat to economic growth.

Even against this "wall of worry" backdrop, the performance of major asset
classes in was generally positive. Despite several interim setbacks and
poor performance near year-end, the Standard & Poor's Index increased in
the Dow Jones Industrial Average increased and the NASDAQ Composite
Index increased International developed and emerging markets equities
performed even better, reflecting both a weakening U.S. dollar, which boosts
returns for U.S. dollar-based investors, and solid local currency returns. The
MSCI EAFE Developed Market Index rose and the MSCI Emerging Markets Index
rose over the same period. The U.S. bond market, as measured by the Lehman
Aggregate Bond Index, rose while the U.S. high-yield bond market, as
measured by the Merrill Lynch High Yield Bond Master II Index, rose as
higher-coupon yields could not compensate for falling bond prices as credit
spreads (differences between yields of higher- and lower-quality bonds) widened
during the second half of




Letter

Looking forward, a growing number of economists are concerned about a
recession. As always, though, emotions can get ahead of reality. Higher
mortgage defaults, a spreading of weakness to other consumer sectors or to
employment, and the possibility of a liquidity/
credit crunch represent risks to the economy. Conversely, economic growth in
the rest of the world remains relatively positive, and a weak U.S. dollar has
significantly benefited U.S. companies competing in the global marketplace.
While falling risk tolerances may continue to depress asset prices in the short
term, equity and corporate bond valuations look reasonable unless the U.S.
economy falls into a severe recession.

Sudden swings in the markets are always to be expected. The history of the
stock market demonstrates that sharp market downturns are frequently followed
by strong recoveries, but they are also difficult to time. Just as staying
diversified and invested are important investment principles, it is also
important to pay attention to asset allocation. As always, we encourage you to
work closely with your financial advisor to find the mix of stocks, bonds and
money market assets that is best aligned to your particular risk tolerance and
investment objective.

Respectfully,
