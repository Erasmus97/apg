Dear Shareholder,




This report provides an overview
on the performance of the Goldman Sachs Enhanced Income Fund
(the Fund) during the one-year reporting period that
ended



Performance Review















Over the one-year period that
ended the Funds ClassA,
Institutional and Administration Shares generated cumulative
total returns, without sales charges, of and
respectively. These returns compare to the and
cumulative total returns of the Funds benchmarks, the
Six-Month U.S. Treasury Bill Index and the One-Year U.S.
Treasury NoteIndex, respectively, over the same time
period. Over the period from its inception on
to the Funds ClassB Shares
generated a cumulative total return of This return
compares to the and cumulative total returns of the
Six-Month U.S. Treasury Bill Index and the One-Year U.S.
Treasury NoteIndex, respectively, over the same time
period.










A combination of top-down and
bottom-up investment strategies impacted the Funds
performance relative to its benchmarks. Overall, our tactical
duration management made a positive contribution. At the
beginning of the reporting period, we maintained a defensive
posture, taking a shorter duration position than the indexes
based on our belief that interest rates would trend higher. This
strategy contributed to relative returns as yields edged higher
through the first eight months of the period. Also beneficial
was our shift to a long duration position as volatility picked
up in the third quarter of and yields rallied on the back
of a flight-to-quality.










Within our cross-sector
strategies, the Funds primary sector exposures were to
agencies and corporates. Increased spread volatility and wider
swap spreads were a significant drag on returns, particularly in
the last part of the period. As investor risk aversion increased
in response to concerns about the subprime market, spread
sectors particularly credit and asset-backed
securities (ABS) came under significant
pressure. In addition, the Funds bias toward high-quality
financials hampered results as the turmoil in the subprime
market and the summer liquidity crunch disproportionately
affected issuers in the bank and broker-dealer sectors.






Market Review















The reporting period was
volatile for interest rates. Short-term rates fell more than
basis points after the Federal Reserve Board (the
Fed) cut the federal funds rate a total of to
In fact, yields were lower across the entire yield curve.
The Treasury fell basis points, ending the period at
The Treasury rally was primarily the result of a summer
flight-to-quality, when demand for U.S. Treasuries soared to the
detriment of all other asset classes. As investors reacted to
the subprime mortgage meltdown, there was a wave of global
selling, illiquidity, and heightened risk aversion across
several asset classes.










At the beginning of the period,
particularly during the second quarter, yields trended higher as
U.S. economic growth appeared to be rebounding. Investors were
heartened by stronger industrial data and a healthy job market
as well as by widespread economic strength
















PORTFOLIO RESULTS















outside of the U.S. However, a
growing number of delinquencies in the subprime sector of the
mortgage market continued to exacerbate concerns about the
housing market and whether its weakness would spill over into
the broader economy. The market tone changed dramatically going
into the summer months as volatility picked up significantly and
what had started of as relatively isolated concerns in the
subprime market morphed into a larger credit and liquidity
crisis.










Financial markets reacted poorly
to the increased volatility, and yields fell dramatically in a
flight-to-quality as risk aversion peaked. The global selling
that ensued led to a severe repricing of assets across the risk
spectrum with many high quality sectors trading at discounts not
supported by fundamentals. Spreads widened across all markets
with the collateralized sectors hit the hardest, particularly
high quality mortgages and ABS. Within corporates, financial
issuers underperformed the most as they were disproportionately
impacted by developments in the subprime market and by mounting
liquidity concerns.










In an effort to prevent the
market turmoil from threatening U.S. economic growth, the Fed
took several steps to inject liquidity into the financial
markets, culminating in a cut to the fed funds
rate at the Federal Open Market Committee
meeting and an additional cut at the
meeting. In the statement following the
October cut, the Fed announced that downside risks to growth and
upside risks to inflation appear to be balanced.


















INVESTMENT
OBJECTIVE

















The Fund seeks to generate return
in excess of traditional money market products while maintaining
an emphasis on preservation of capital and liquidity.

It is important to note that the Fund is not a money market
fund and its net asset value will fluctuate.








Portfolio Composition















At the end of the period, we held
a modest short duration bias relative to the benchmark. While we
believe more Fed cuts are possible, we also think that the
current level of interest rates reflects an overly aggressive
easing policy. Within our sector exposures, we continue to
emphasize shorter-term, higher quality securities that we
believe can offer an attractive yield advantage. The Fund
continues to hold a significant position in short-term
quasi-government securities, specifically agency debentures;
however, this exposure decreased over the period as some of
these shorter securities matured, and the allocation to
short-term investments, or cash, increased accordingly. Within
the corporate sector, we hold an overweight to financials given
the high quality of many of these issuers. We continue to look
for attractive security selection opportunities to emerge in
both the agency and corporate sectors.














We thank you for your investment
and look forward to your continued confidence.














Goldman Sachs U.S. Fixed
Income Investment Management Team

Dear Shareholder:




This report provides an overview
on the performance of the Goldman Sachs Ultra-Short Duration
Government Fund (theFund) during the one-year
reporting period that ended



Performance Review















Over the one-year period that
ended the Funds ClassA,
Institutional, and Service Shares generated cumulative total
returns, without sales charges, of and
respectively. These returns compare to the and
cumulative total returns of the Funds benchmarks, the
Six-Month U.S. Treasury Bill Index and the One-Year U.S.
Treasury Note Index, respectively, over the same time period.










A combination of top-down and
bottom-up investment strategies impacted the Funds
performance relative to its benchmarks. Overall, our tactical
duration management made a positive contribution. At the
beginning of the reporting period, we maintained a defensive
posture, taking a shorter duration position than the indexes
based on our belief that interest rates would trend higher. This
strategy contributed to relative returns as yields edged higher
through the first eight months of the period. Also beneficial
was our shift to a relative long duration position as volatility
picked up in the third quarter of and yields rallied on the
back of a flight-to-quality.










Within our cross-sector
strategies, the Funds primary exposures were to the
government and collateralized sectors, including mortgages and
asset-backed securities (ABS). Increased spread
volatility and wider swap spreads were a significant drag on
returns, particularly in the latter part of the period. As
market risk increased and liquidity concerns mounted, spread
sectors underperformed across the board. Mortgages in particular
endured a difficult third quarter due to the fallout in the
subprime market, a global flight-to-quality, continued credit
concerns, and a lack of investor demand.










Within mortgages, we favored
securities that were less sensitive to volatility
specifically, pass-throughs, adjustable-rate mortgages
(ARMs), and shorter duration collateralized mortgage
obligations (CMOs). Although this low-volatility
bias was beneficial, select securities in the Fund
underperformed primarily due to a mounting liquidity crisis in
the market. High quality mortgages, particularly ARMs and ABS
underperformed the most as a result of increased selling by
market participants.






Market Review















The reporting period was
volatile for interest rates. Short-term rates fell more than
basis points after the Federal Reserve Board (the
Fed) cut the federal funds rate a total of to
In fact, yields were lower across the entire yield curve.
The Treasury fell basis points, ending the period at
The Treasury rally was primarily the result of a summer
flight-to-quality, when demand for U.S. Treasuries soared to the
detriment of all other asset classes. As investors reacted to
the subprime mortgage meltdown, there was a wave of global
selling, illiquidity, and heightened risk aversion across
several asset classes.
















PORTFOLIO RESULTS















At the beginning of the period,
particularly during the second quarter, yields trended higher as
U.S. economic growth appeared to be rebounding. Investors were
heartened by stronger industrial data and a healthy job market
as well as by widespread economic strength outside of the U.S.
However, a growing number of delinquencies in the subprime
sector of the mortgage market continued to exacerbate concerns
about the housing market and whether its weakness would spill
over into the broader economy. The market tone changed
dramatically going into the summer months as volatility picked
up significantly and what had started of as relatively isolated
concerns in the subprime market morphed into a larger credit and
liquidity crisis.










Financial markets reacted poorly
to the increased volatility and yields fell dramatically in a
flight-to-quality as risk aversion peaked. The global selling
that ensued led to a severe repricing of assets across the risk
spectrum with many high quality sectors trading at discounts not
supported by fundamentals. Spreads widened across all markets
with the collateralized sectors hit the hardest, particularly
high quality mortgages and ABS. Within corporates, financial
issuers underperformed the most as they were disproportionately
impacted by developments in the subprime market and by mounting
liquidity concerns.










In an effort to prevent the
market turmoil from threatening U.S. economic growth, the Fed
took several steps to inject liquidity into the financial
markets, culminating in a cut to the fed funds
rate at the Federal Open Market Committee
meeting and an additional cut at the
meeting. In the statement following the
October cut, the Fed announced that downside risks to growth and
upside risks to inflation appear to be balanced, suggesting that
another ease could be unlikely.


















INVESTMENT
OBJECTIVE

















The Fund seeks a high level of
current income, consistent with low volatility of principal.









Portfolio Composition















At the end of the period, we held
a modest short duration bias relative to the benchmark. While we
believe more Fed cuts are possible, we also think that the
current level of interest rates reflects an overly aggressive
easing policy. Over the period, we continued to focus on
security selection and subsector strategies across the
collateralized and government sectors. Within mortgages, our
primary exposures were to ARMs and pass-through mortgages. We
tactically adjusted the Funds exposures to take advantage
of changing relative opportunities. We believe that the current
market volatility offers attractive security selection
opportunities and we will look to opportunistically add to our
sector exposures where we find value from a risk-return
perspective. Nevertheless, we remain prudent as market events
unfold and volatility persists.










We thank you for your investment
and look forward to your continued confidence.














Goldman Sachs U.S. Fixed
Income Investment Management Team

Dear Shareholder:




This report provides an overview
on the performance of the Goldman Sachs Short Duration
Government Fund (theFund) during the one-year
reporting period that ended



Performance Review















Over the one-year period that
ended the Funds ClassA, B, C,
Institutional and Service Shares generated cumulative total
returns, without sales charges, of
and respectively. These returns compare to the
cumulative total return of the Funds benchmark, the
Two-Year U.S.Treasury NoteIndex, over the same time
period.










A combination of top-down and
bottom-up investment strategies impacted the Funds
performance relative to its benchmark. Overall, our tactical
duration management made a positive contribution. At the
beginning of the reporting period, we maintained a defensive
posture, taking a shorter duration position than the index based
on our belief that interest rates would trend higher. This
strategy contributed to relative returns as yields edged higher
through the first six months of the period. Also beneficial was
our shift to a relative long duration position as volatility
picked up in the third quarter of and yields rallied on the
back of a flight-to-quality.










Within our cross-sector
strategies, the Funds primary exposures were to the
government and collateralized sectors. Increased spread
volatility and wider swap spreads were a significant drag on
returns, particularly in the latter part of the period. As
market risk increased and liquidity concerns mounted, spread
sectors underperformed across the board. Mortgages in particular
endured a difficult summer, due to the fallout in the subprime
market, a global flight-to-quality, continued credit concerns,
and a lack of investor demand. Within mortgages, we favored
securities that were less sensitive to volatility
specifically,
pass-throughs, adjustable-rate mortgages (ARMs), and
shorter duration collateralized mortgage obligations
(CMOs). Although this low-volatility bias was
beneficial, select securities in the Fund underperformed
primarily due to a mounting liquidity crisis in the market. High
quality mortgages, particularly ARMs and ABS underperformed the
most as a result of increased selling by market
participants.





Market Review















The
reporting
period was volatile for interest rates. Short-term rates fell
more than basis points after the Federal Reserve Board (the
Fed) cut the federal funds rate a total of to
In fact, yields were lower across the entire yield curve.
The Treasury
fell points, ending the period at The
Treasury rally was primarily the result of a summer
flight-to-quality, when demand for U.S.Treasuries soared
to the detriment of all other asset classes. As investors
reacted to the subprime mortgage meltdown, there was a wave of
global selling, illiquidity, and heightened risk aversion across
several asset classes.









At the beginning of the period,
particularly during the second quarter, yields trended higher as
U.S.economic growth appeared to be rebounding. Investors
were heartened by stronger industrial data and a healthy job
market as well as by widespread economic strength






















PORTFOLIO RESULTS















outside of
the U.S. However, a growing number of delinquencies in the
subprime sector of the mortgage market continued to exacerbate
concerns about the housing market and whether its weakness would
spill over into the broader economy. The market tone changed
dramatically going into the summer months as volatility picked
up significantly and what had started of as relatively isolated
concerns in the subprime market morphed into a larger credit and
liquidity crisis.










Financial markets reacted poorly
to the increased volatility and yields fell dramatically in a
flight-to-quality as risk aversion peaked. The global selling
that ensued led to a severe repricing of assets across the risk
spectrum with many high quality sectors trading at discounts not
supported by fundamentals. Spreads widened across all markets
with the collateralized sectors hit the hardest, particularly
high quality mortgages and ABS. Within corporates, financial
issuers underperformed the most as they were disproportionately
impacted by developments in the subprime market and by mounting
liquidity concerns.










In an effort to prevent the
market turmoil from threatening U.S.economic growth, the
Fed took several steps to inject liquidity into the financial
markets, culminating in a
cut to
the fed funds rate at the Federal Open
Market Committee meeting and an additional
cut at
the meeting. In the statement following the
October cut, the Fed announced that downside risks to growth and
upside risks to inflation appear to be balanced, suggesting that
another ease could be unlikely.

















INVESTMENT
OBJECTIVE

















The Fund seeks a high level of
current income and secondarily, in seeking currentincome,
may also consider the potential for capital appreciation.









Portfolio Composition















At the end of the period, we held
a modest short duration bias relative to the benchmark. While we
believe more Fed cuts are possible, we also think that the
current level of interest rates reflects an overly aggressive
easing policy. Over the period, we continued to focus on
security selection and sub-sector strategies. Within mortgages,
we emphasized securities that offered attractive spreads
relative to Treasuries; in particular, we hold a bias toward
ARMs. As valuations changed over the period, we tactically
adjusted the Funds exposures to take advantage of changing
relative opportunities within the market. We increased the
allocation to pass-through mortgages as we found attractive
opportunities. In addition, we continued to find value within
short-dated agency paper and maintained our exposure to the
quasi-government sector.






We thank you for your investment and look forward
to your continued confidence.



Goldman Sachs U.S. Fixed Income Investment
Management Team
