DEAR SHAREHOLDER: 
It was quite a ride for stocks in 2007. They rose at a healthy clip in the first half of the year,
reflecting a relatively resilient economy, but the market deteriorated in the second half as investors became concerned about the mortgage crisis and the condition of the economy overall. Despite the cut in interest rates by the Federal Reserve, we
saw the unemployment rate rise to 5% by year-end. The Parnassus Equity Income Fund had a great year, returning 14.1% versus 5.5% for the S&amp;P 500. In this report, you’ll find out more about how each of our funds performed, as well as our
outlook and strategy. We have combined the information on all of our funds into one report to make them easier for shareholders to read and also to save paper. 
Shareholder Communication Awards 
We believe an essential part of our job in serving you, the shareholder, is to communicate how we’ve
performed for you and how we invest in general. We try to use plain language and explain things in a straightforward way. We try to talk about our mistakes as well as our successes. We take pride in these reports, but until now, you’ve had to
take our word for it that these reports are among the best in the mutual fund world. Now we’re proud to say that we have some confirmation. In October, the Mutual Fund Education Alliance (MFEA) awarded Parnassus with their STAR award for the
best Annual Report to Shareholders for a small fund group. The MFEA is a mutual fund industry trade group that gives annual awards to fund groups for various categories in shareholder communication. We also won the award for the best retail website,
also for the small fund category. We believe our website is a great tool to provide our shareholders with current and historical information on their investment with Parnassus. We won’t let these awards go to our head, though. We’ll keep
working hard to write informative, clearly-written reports; we’ll also keep improving our website. 
Thank you for being an investor with Parnassus.

 
