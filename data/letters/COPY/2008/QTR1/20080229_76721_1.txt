Dear fellow shareholders,


I am pleased to provide you with the
Annual Report of Pax World Funds.


As I write, financial markets continue to experience unusual turmoil due
to the subprime mortgage crisis, resulting contagion and a loss of
confidence in the ability of major financial institutions to properly
manage risk. The housing downturn, rising energy prices and a weakened
dollar are fueling concern about the direction of the U.S. economy. At
the same time, there is a growing interest in sustainability and all
things green, and a growing sense of urgency about tackling some of
the worlds major problems from global poverty to climate change.
Sustainable investing is at the heart of this burgeoning movement. This
presents a unique set of challenges and opportunities for Pax World and
its shareholders, and I believe there is much cause for optimism as we
take stock of and look ahead to


Although was somewhat of an emotional roller coaster, it turned out
to be a generally positive year for markets as well as for Pax World
investors. You will read about our own portfolio performance below, but
overall, the Dow Jones Industrial Average, SP and NASDAQ Composite
Index returned and respectively. These returns
were respectable considering the challenges facing markets rising
energy prices, soft corporate profits, a weakening dollar, and toward
the end of the subprime mortage debacle and credit crunch. As
began, talk of recession dominated the airwaves and markets were
whipsawed by volatility linked to the housing downturn and the
subprime/liquidity crisis. The Federal Reserve felt it necessary to
intervene with aggressive rate cuts while the President and Congress
hurriedly put together a stimulus package to boost the economy. There
has been some debate about the wisdom of both moves.


Regarding interest rates, the Fed has been criticized in some quarters
for acting precipitously, panicked by volatility in the capital markets.
These critics argue that restoring faith in financial markets will be
more difficult if policy makers simply kick the can down the road and
postpone the day of reckoning. Low interest rates and easy credit helped
fuel this crisis, which is fundamentally a debt crisis. Lending
standards deteriorated to the point where risk was not adequately being
priced into the market. Risky mortage loans based on inflated home
prices were pooled together







Table of Contents



in so-called structured investment vehicles (SIVs) which were then sold
to institutional investors. Banks, hedge funds and private equity firms
increasingly financed their expansion via such increasingly complex and
opaque leverage. Total household debt soared by over the past five
years as consumers ran to the mall and bought homes beyond their reach
hoping that rising home prices and low interest rates would continue
to bail them out. Our national debt has soared to trillion and our
current trade deficit has soared to approximately as we
continue to consume more than we produce, borrowing more and more to
finance the difference. Bubbles like this tend to burst, and when the
financial sector is involved, it impacts the entire economy. Whether
continued low interest rates are the anwer or instead compound the
problem is at this point anybodys guess.


The stimulus package passed by Congress and signed by the President may
be less than meets the eye as well. This legislation operates with a lag
and its effects will not likely be felt until the second half of the
year, if at all. Moreover, a significant part of the stimulus will
apparently go as investment tax credits to businesses, many of whom
would have made the investments anyway, while the remainder will come in
the form of rebates to taxpayers who might better spend the money paying
off debt rather than heading back to the mall. Boosting short-term
consumption, in other words, may not do the trick, particularly if
housing prices continue to fall. Policy makers may have also missed a
golden opportunity to do something far more creative and stimulative:
investing in efforts to stabilize housing prices; in badly needed
infrastrucure improvements (bridges, ports, mass transit, the
electricity grid); and in programs to lower emissions and promote
renewable energy. Some of these options could have helped create jobs
and stimulate the green sector, on which so much of our future
economic growth will depend. So, it may turn out that an election-year
stimulus package focused on short-term palliatives will not have
appreciable impact on what are really long-term problems.


Amidst all this uncertainty, we at Pax World believe there is reason for
optimism even if markets remain volatile in the near term. Our portfolio
managers continue to believe, for example, that there are manifold
opportunities for investors who take a global perspective. Much of the
rest of the world, both emerging as well as developed markets, has been
growing at a faster rate than the United States. This creates an
opportunity that our portfolio managers have seized upon by increasing
exposure to global markets, which provides the dual positive effect of
solid diversification with potentially higher returns. Other
opportunities include the growth of alternative energy and the rise in
commodity prices. The price of a barrel of oil went from the low to
the high range in less than a year. These rising oil prices, coupled







Table of Contents



with the need to address global climate change, have resulted in a
growing push for alternative energy. Solar stocks, for example, have
been strong contributors in the Pax World Growth Fund. The healthy
demand in commodities, fueled by rapidly growing economies such as
those in China, Russia, Brazil, and India, is also a trend our fund
managers have taken advantage of by investing in companies that provide
agricultural equipment and infrastructure services. So, despite market
jitters, our portfolio managers believe that staying defensive with
U.S. companies and looking for companies that can take advantage of
continued global growth will help position our portfolios to weather
the storms of a volatile market.


We are confident enough in the future and in particular the future of
sustainable investing that we have been busy launching new funds so
that we can offer additional investment options to Pax World
shareholders. In we launched the Pax World Value Fund
and in acquired what is now known as the Pax World Womens
Equity Fund. We also have filed registration statements with the
Securities and Exchange Commission to launch three additional funds a
Small-Cap Fund, an International Fund and a global clean tech fund that
we are calling the Global Green Fund in the early months of
Of the five new funds we will have launched during
and three are in asset classes that fill out Pax Worlds fund
line-up by providing additional diversification and asset allocation
opportunities to our shareholders: value stocks, small-cap stocks and
international stocks. The other two funds touch on themes we believe are
of particular interest to Pax World investors: gender equality and
womens empowerment in the case of the Pax World Womens Equity Fund;
environmental innovation and clean technology in the case of the Global
Green Fund.


Sadly, the visionary founder of the Womens Equity Fund, Linda Pei,
passed away less than two months after the merger of this Fund into Pax
World. We are determined to carry out Lindas vision and honor her
legacy as we bring the Fund to a wider audience of investors who seek to
promote gender equality and womens advancement by investing in
companies that invest in women. You will find a brief tribute to Lindas
life and legacy on the Pax World website (www.paxworld.com).

Pax World had a busy on other fronts as well. We began providing
quarterly account statements to shareholders. We overhauled our web
site. We even moved into

















The information in this report regarding the Pax World
Small Cap Fund, Pax World International Fund and Pax World Global Green
Fund is not complete and may be changed. No shares of these funds may
be sold until the registration statement relating to these funds filed
with the Securities and Exchange Commission is effective. This report
is not an offer to sell shares of these funds and is not soliciting an
offer to buy shares of these funds in any state where the offer or sale
is not permitted.










Table of Contents









new headquarters. Our investment adviser, Pax World Management Corp.
(PWMC), has been increasingly active in community affairs. During
PWMC endowed a scholarship fund at the School of Community Economic
Development (SCED)at Southern New Hampshire University (www.snhu.edu).
This scholarship will support African students studying in the U.S. who
intend to return to Africa to work in the areas of poverty reduction and
sustainable development. PWMC also made a three-year commitment to
support the work of Odyssey House New Hampshire (www.odysseynh.org) in
building a Recovery School for students grades through committed to
working on sobriety within a school-based alcohol and drug treatment and
recovery program. These efforts are in addition to PWMCs longstanding
support for the global relief and reconciliation work of Mercy Corps
(www.mercycorps.org) and other community and charitable activities.


As we look forward to and beyond, I am convinced there will be more
and greater opportunities for Pax World and its shareholders to have a
positive impact both in our own communities and on a global scale. The
world is shrinking. Our challenges and our opportunities are becoming
global in nature. At Pax, we are convinced that investors can make a
difference, and that investing in companies that make a difference is
the best strategy for long-term investors. We call it Sustainable
Investing, and we believe it is the wave of the future as we transition
from an industrial to a post-industrial, sustainable economy. At Pax
World, we want our investors to participate in and prosper from this
transformation in ways that benefit them and their families for
generations to come.


So, our perspective is long term because we believe thats what best
serves you, our shareholders. In our opinion it is the best way to
manage investments, to manage businesses, and for that matter, to manage
the economy. There will always be temporary downturns or volatility.
What we have to do is take a long-term view, and when one does that,
there is always reason for optimism and there is always reason for
hope.


Heres to hope and to you and your families for


Peace,

Joseph F. Keefe
President and CEO
