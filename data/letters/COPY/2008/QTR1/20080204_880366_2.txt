Dear Shareholder, 
i
iii
 
 
 
ing the December meeting, the Fed stated: “Incoming information suggests that economic growth is slowing, reflecting the intensification of the housing
correction and some softening in business and consumer spending. Moreover, strains in financial markets have increased in recent weeks. Today’s action, combined with the policy actions taken earlier, should help promote moderate growth
over time.” 
iv
v
Please read on for a more detailed look at prevailing economic and market conditions during the Fund’s
fiscal year and to learn how those conditions have affected Fund performance. 
 
II         
 
 
Information About Your Fund 
As you may be aware, several issues in the mutual fund industry have come under the scrutiny of federal and state regulators. Affiliates of the Fund’s manager have, in recent years, received requests for
information from various government regulators regarding market timing, late trading, fees, and other mutual fund issues in connection with various investigations. The regulators appear to be examining, among other things, the Fund’s response
to market timing and shareholder exchange activity, including compliance with prospectus disclosure related to these subjects. The Fund is not in a position to predict the outcome of these requests and investigations. 
Important information with regard to recent regulatory developments that may affect the Fund is contained in the Notes to Financial Statements included in
this report. 
As always, thank you for your confidence in our stewardship of your assets. We look forward to helping you meet your
financial goals. 
Sincerely, 


