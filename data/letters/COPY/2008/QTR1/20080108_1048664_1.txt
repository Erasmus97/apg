Dear Shareholder:
 
Market Review: U.S. economy and sub-prime crisis have global effects
For the 12-month period ended October 31, 2007, returns from international equities were positive. While the Japanese equity market lagged other markets, emerging market equities generated particularly strong returns.
The year saw a surge in oil prices, with crude oil moving from just over $50 per barrel to close to $100 per barrel by the end of October. Additionally, other commodities also showed strong increases in price during the year.
In the first half of the year, private equity activity drove the prices of small and mid-cap stocks to high levels. Activity slowed, however, after the sub-prime mortgage crisis became apparent.
As expected, financials were weak as a result of the sub-prime mortgage crisis. And market participants became increasingly concerned about the effect that a housing slump in the United States would have on the U.S. economy.
Strategic Review and Outlook: Expecting continued positive returns
Positive contributors to performance included our overweight position in energy. Conversely, our underweight position in capital goods detracted from relative performance, as we were incorrect in our view that the slowdown in the United States economy would have a large impact on the capital goods stocks.
Currently, the portfolio remains overweight in energy. However, within this, we have an overweight position in oil services and are underweight in 
integrated energy companies. We are overweight specific plays in emerging markets — specifically, some emerging market telecommunications services issuers. Additionally, earlier in the year, we were overweight in Korean shipbuilders.
While we believe that financials look cheap, we are not prepared to overweight them fully, as we expect there could be further bad news to come.
In our opinion, one of the main themes that will drive equity markets is whether Asian economies have "decoupled" from the United States economy. Growth in Asia remains strong; however, there is a concern that a slowing U.S. economy will have a detrimental impact on growth in Asia.
Further, we believe that equity market valuations are less attractive than they were one year ago. And while emerging markets have outperformed developed markets, they now trade at premium valuations. We expect to see positive returns from equity markets; however, we do not believe that they will be as high as they have been in previous years.
The Credit Suisse International Equity Team
International investing entails special risk considerations, including currency fluctuations, lower liquidity, economic and political risks, and differences in accounting methods. The Fund's fifteen largest holdings may account for 40% or more of the Fund's assets. As a result of this strategy, the Fund may be subject to greater volatility than a portfolio that invests in a larger number of issuers.
In addition to historical information, this report contains forward-looking statements that may concern, among other things, domestic and foreign market, industry and economic trends and developments and government regulation and their potential impact on the Fund's investments. These statements are subject to risks and uncertainties and actual trends, developments and regulations in the future, and their impact on the Fund could be materially different from those projected, anticipated or implied. The Fund has no obligation to update or revise forward-looking statements.


