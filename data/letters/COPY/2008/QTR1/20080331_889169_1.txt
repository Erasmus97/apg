Dear Shareholder:

We are pleased to present this semiannual report for Dreyfus Inflation Adjusted Securities Fund, covering the six-month period from August through January

The past six months were a time of significant change for U.S. fixed-income markets.Turmoil in the sub-prime mortgage market, a declining U.S.housing market,and soaring energy prices sparked a flight to quality
among investors, in which prices of U.S.Treasury securities surged higher while other domestic fixed-income sectors tumbled, including highly rated corporate bonds. Throughout the reporting period, the Fed took action to promote market liquidity and
forestall a potential recession, lowering short-term interest rates readily which contributed to wider yield differences along the bond markets maturity spectrum.As a result, despite the significant price swings seen along the entire maturity
spectrum, longer-term bonds generally realized better overall performance during the reporting period, as well as for as a whole.

Recent market turbulence and credit concerns have reinforced one of the central principles of successful investing: diversification.As seen last year, investors with broad exposure to both the stock and bond markets had
better protection from the full impact of weakness in areas that, prior to the credit crunch, were among the markets leaders. Of course, past performance is not an indicator of future results, and diversification does not guarantee positive
returns. However, we believe for a long-term investment objective that a diversification plan created with the help of your financial advisor can overcome any short-term market risks and also capture the potential opportunities down the road that
may arise as a result of current developments.

For information about how the fund performed during the reporting period, as well as market perspectives, we have provided a Discussion of Fund Performance given by the funds Portfolio Manager.

Thank you for your continued confidence and support.









DISCUSSION OF FUND PERFORMANCE

For the period of August through January as provided by Robert Bayston, Portfolio Manager

Fund and Market Performance Overview

For the six-month period ended January Dreyfus Inflation Adjusted Securities Fund achieved total returns of for Institutional shares and for Investor
In comparison, the funds benchmark, the Lehman Brothers U.S. Treasury Inflation Protected Securities Index (the Index), which is not subject to fees and expenses like a mutual fund, achieved a
total return of for the same In addition, the average total return of all funds reported in the Lipper Treasury Inflation Protected Securities category was
over the reporting

U.S.Treasury securities, including Treasury Inflation Protected Securities (TIPS), benefited from a flight to quality among investors, as a market-wide credit crisis intensified and a slowing economy
fueled recession concerns. In contrast, most other sectors of the U.S. bond market produced meager returns under the same conditions.The fund participated to a great extent in the market rally, but fund fees and expenses caused its return to lag the

Dear Shareholder:

We are pleased to present this semiannual report for Dreyfus Intermediate Term Income Fund,covering the six-month period from August through January

The past six months were a time of significant change for U.S. fixed-income markets.Turmoil in the sub-prime mortgage market, a declining U.S. housing market, and soaring energy prices sparked a flight to
quality among investors, in which prices of U.S.Treasury securities surged higher while other domestic fixed-income sectors tumbled, including highly rated corporate bonds. Throughout the reporting period, the Fed took action to promote market
liquidity and forestall a potential recession, lowering short-term interest rates readily which contributed to wider yield differences along the bond markets maturity spectrum.As a result, despite the significant price swings seen along the
entire maturity spectrum, longer-term bonds generally realized better overall performance during the reporting period, as well as for as a whole.

Recent market turbulence and credit concerns have reinforced one of the central principles of successful investing: diversification. As seen last year, investors with broad exposure to both the stock and bond markets had
better protection from the full impact of weakness in areas that, prior to the credit crunch, were among the markets leaders. Of course, past performance is not an indicator of future results, and diversification does not guarantee positive
returns. However, we believe for a long-term investment objective that a diversification plan created with the help of your financial advisor can overcome any short-term market risks and also capture the potential opportunities down the road that
may arise as a result of current developments.

For information about how the fund performed during the reporting period, as well as market perspectives, we have provided a Discussion of Fund Performance given by the funds Portfolio Manager.

Thank you for your continued confidence and support.









DISCUSSION OF FUND PERFORMANCE

For the period of August through January as provided by Kent Wosepka, Portfolio Manager

Fund and Market Performance Overview

For the six-month period ended January Dreyfus Intermediate Term Income Funds Institutional shares achieved a total return of and the funds Investor shares achieved a total return of
In comparison, the funds benchmark, the Lehman Brothers U.S. Aggregate Index (the Index), achieved a total return of for the same


An intensifying credit crunch and slower U.S. economic growth sparked a flight to quality during the reporting period, in which U.S.Treasury securities rallied strongly while most other bond market sectors
declined. Although the fund participated in the strength of the price performance of U.S.Treasuries to a degree, an underweight in Treasuries relative to the benchmark, and holdings in other areas, caused its returns to lag the benchmark.

The fund seeks to maximize total return, consisting of capital appreciation and current income. To pursue this goal, the fund normally invests at least of its assets in fixed-income securities of U.S. and foreign
issuers rated at least investment grade or the unrated equivalent as determined by Dreyfus. These securities include: U.S. government bonds and notes, corporate bonds, municipal bonds, convertible securities, preferred stocks, inflation-indexed
securities, asset-backed securities, mortgage-related securities, and foreign bonds. Typically, the fund can expect to have an average effective maturity ranging from five to ten years, and an average effective duration ranging between three and
eight years. For additional yield, the fund may invest up to of its assets in fixed-income securities rated below investment grade.

Sub-Prime Mortgage Woes Derailed Some Market Sectors

The reporting period began in the midst of a credit crisis in U.S. and global fixed-income markets. Heightened volatility stemming from

The Fund







greater-than-expected defaults and delinquencies among sub-prime mortgages had spread to other fixed-income market sectors in the weeks just prior to the start of the reporting period.These credit concerns arose at a time
in which the U.S. economy appeared to be slowing, mainly as a result of faltering housing prices and soaring energy costs.These factors led investors to turn away from the higher-yielding segments of the bond market that had performed well over the
past several years. Selling pressure was particularly heavy among highly leveraged institutional investors, who were forced to sell their more creditworthy and liquid holdings to meet margin calls and redemption requests.

The Federal Reserve Board (the Fed) intervened in August by reducing the discount rate, the rate it charges member banks for overnight loans. Although fixed-income investors initially responded positively to the
Feds action, the resulting rally was quashed by additional evidence of an economic slowdown and reports of heavy sub-prime related losses among major commercial and investment banks.The Fed attempted to promote market liquidity and forestall
further economic deterioration by reducing the federal funds rate the rate banks charge one another for overnight loans from at the start of the reporting period to by the end of However, investor sentiment continued
to deteriorate in January, with some analysts forecasting the first U.S. recession in seven years.The Fed demonstrated its resolve to fight economic weakness when it reduced the federal funds rate by another basis points to in
two separate moves in the latter part of January.

Non-Treasury Holdings Constrained Relative Performance

As the credit crisis unfolded, the funds relatively light holdings of U.S. Treasuries and overweighted positions in shorter-duration corporate bonds and asset-backed securities proved to be a drag on relative
performance. We had adopted a defensive investment posture with regard to investment-grade corporate bonds, including an emphasis on shorter-duration securities from companies, including those in the financials sector, that we regarded to be less
vulnerable to risks associated with leveraged buyouts. However, this focus detracted from relative performance during the credit crisis.A modest position in high yield bonds also lagged the averages.







On a more positive note, the funds bulleted yield curve strategy benefited from wider yield differences along the markets maturity range, and a slightly longer-than-average duration helped the fund
participate more fully in gains among U.S.Treasuries.The funds modest holdings of non-dollar securities, primarily from the emerging markets, also contributed positively to relative performance, as did the purchase of credit default swaps on
certain corporate bonds.

Positioning the Fund for a Changing Market

Elevated energy prices, the housing recession, tighter lending standards and mounting bank losses have continued to weigh on the U.S.economy. Therefore, we expect the Fed to reduce short-term interest rates further, and we
have maintained the funds interest-rate strategies. In addition, we have found more opportunities among short-maturity international bonds in markets where we expect short-term interest rates to fall, and we have increased the funds
positions in credit default swaps on bonds of companies in economically-sensitive industries. In our view, these are prudent strategies in todays unsettled market environment.








The fund may use derivative instruments, such as options, futures and options on futures, forward









contracts, swaps (including credit default swaps on corporate bonds and asset-backed securities),









options on swaps, and other credit derivatives.A small investment in derivatives could have a









potentially large impact on the funds performance.The use of derivatives involves risks different









from, or possibly greater than, the risks associated with investing directly in the underlying assets.









Credit default swaps and similar instruments involve greater risks than if the fund had invested in









the reference obligation directly, since, in addition to general market risks, they are subject to









illiquidity risk, counterparty risk and credit risks.









Total return includes reinvestment of dividends and any capital gains paid. Past performance is no

Dear Shareholder:

We are pleased to present this semiannual report for Dreyfus Premier Short Term Income Fund, covering the six-month period from August

through January

The past six months were a time of significant change for U.S. fixed-income markets.Turmoil in the sub-prime mortgage market, a declining U.S. housing market, and soaring energy prices sparked a flight to
quality among investors, in which prices of U.S.Treasury securities surged higher while other domestic fixed-income sectors tumbled, including highly rated corporate bonds. Throughout the reporting period, the Fed took action to promote market
liquidity and forestall a potential recession, lowering short-term interest rates readily which contributed to wider yield differences along the bond markets maturity spectrum.As a result, despite the significant price swings seen along the
entire maturity spectrum, longer-term bonds generally realized better overall performance during the reporting period, as well as for as a whole. Recent market turbulence and credit concerns have reinforced one of the central principles of
successful investing: diversification. As seen last year, investors with broad exposure to both the stock and bond markets had better protection from the full impact of weakness in areas that, prior to the credit crunch, were among the markets
leaders. Of course, past performance is not an indicator of future results, and diversification does not guarantee positive returns. However, we believe for a long-term investment objective that a diversification plan created with the help of your
financial advisor can overcome any short-term market risks and also capture the potential opportunities down the road that may arise as a result of current developments.

For information about how the fund performed during the reporting period, as well as market perspectives, we have provided a Discussion of Fund Performance given by the funds Portfolio Manager.

Thank you for your continued confidence and support.


Thomas F. Eggers Chief Executive Officer The Dreyfus Corporation February










DISCUSSION OF FUND PERFORMANCE

For the period of August through January as provided by Catherine Powers, Portfolio Manager

Fund and Market Performance Overview

For the six-month period ended January Dreyfus Premier Short Term Income Fund achieved total returns of for Class B shares, for Class D shares and for Class P shares.
In comparison, the funds benchmark index, the Merrill Lynch Year Corporate/Government Index (the Index), achieved a total return of for the same


The U.S. bond market encountered difficult conditions during the reporting period as economic weakness and a credit crisis originating in the sub-prime mortgage sector sparked a flight to quality among
investors. U.S.Treasury securities gained value as investors reassessed their attitudes toward risk, but other types of bonds performed poorly. The funds returns substantially lagged its benchmark, which we attribute to the funds
underweight position in Treasuries, as well as significant price dislocations, which resulted from the sub-prime fallout.

The fund seeks to maximize total returns consisting of capital appreciation and current income. To pursue this goal, the fund invests at least of its assets in fixed-income securities of U.S. or foreign issuers rated
investment grade or the unrated equivalent as determined by Dreyfus. This may include: U.S. government bonds and notes, corporate bonds, municipal bonds, convertible securities, preferred stocks, inflation-indexed securities, asset-backed
securities, mortgage-related securities (including CMOs), and foreign bonds. For additional yield, the fund may invest up to of its assets in fixed-income securities rated below investment grade (high yield or junk
bonds). Typically, the funds portfolio can be expected to have an average effective maturity and an average effective duration of three years or less.

The Fund







Sub-Prime Woes Derailed Most Market Sectors

Heightened volatility stemming from an unexpected surge in defaults among sub-prime mortgages spread to other bond market sectors just prior to the start of the reporting period. At the same time, the U.S. economy faltered,
mainly due to plunging housing prices and soaring energy costs. These factors led investors to turn away from the higher yielding segments of the bond market, causing yield differences between U.S.Treasury securities and other types of bonds to
widen dramatically.

In mid-August, the Federal Reserve Board (the Fed) intervened in the developing credit crunch by reducing the rate it charges member banks for overnight loans. The resulting Treasury rally was overwhelmed in
subsequent months by disappointing economic data and reports of heavy sub-prime related losses among commercial and investment banks.The Fed attempted to forestall further weakness by reducing the federal funds rate the rate banks charge one
another for overnight loans from at the start of the reporting period to by the end of However, economic indicators and investor sentiment continued to deteriorate in January, and the Fed implemented its most aggressive
policy easing in years when it lowered the federal funds rate by another basis points to in two separate moves in the latter part of January.

Non-Treasury Holdings Constrained Relative Performance

As the credit crisis unfolded, the funds relatively light holdings of U.S. Treasuries, an overweighted position in shorter duration corporate bonds and out-of-Index positions in asset-backed, residential
mortgage-backed and commercial mortgage-backed securities weighed on the funds relative performance.We had adopted a defensive investment posture with regard to investment-grade corporate bonds, including an emphasis on financial services
companies that tend to be less vulnerable to risks associated with leveraged buyouts. However, this focus detracted from relative performance when financial companies posted sub-prime losses.

While the funds holdings of asset-backed and mortgage-backed securities were composed primarily of shorter maturity, AAA-rated bonds,







they declined sharply when it became clear that sub-prime losses among bond insurers could lead to credit-rating downgrades, potentially affecting trillions of dollars of insured securities. We reduced the funds
exposure to recently issued structured securities that we regarded as vulnerable to credit-rating downgrades, but it made little sense to us to sell other holdings at distressed prices as long as we remained confident that they were likely to pay
interest and principal in a timely manner.

On a more positive note, the fund participated in the rally among U.S. Treasuries, and our bulleted yield curve strategy benefited from widening yield differences along the markets maturity range. However,
the favorable effects of these strategies were negligible compared to the stronger negative influences.

Maintaining Caution in a Changing Market

Despite the Feds attempts to calm the markets, uncertainty has persisted with regard to elevated energy prices, the housing recession, credit rating methodologies and mounting sub-prime related losses among banks and
bond insurers. Recent price dislocations have caused many high-quality corporate securities and other short-duration assets to decline to distressed valuations that, in our view, do not reflect their underlying fundamental strengths. However, we
have not yet taken advantage of these opportunities, as we prefer to maintain a cautious investment posture until we see evidence that the worst of the markets turbulence is behind us.








Total return includes reinvestment of dividends and any capital gains paid. Past performance is no

Dear Shareholder:

We present this semiannual report for Dreyfus Premier Yield Advantage Fund, covering the six-month period from August through January

The past six months were a time of significant change for U.S. fixed-income markets.Turmoil in the sub-prime mortgage market, a declining U.S. housing market, and soaring energy prices sparked a flight to
quality among investors, in which prices of U.S.Treasury securities surged higher while other domestic fixed-income sectors tumbled, including highly rated corporate bonds. Throughout the reporting period, the Fed took action to promote market
liquidity and forestall a potential recession, lowering short-term interest rates readily which contributed to wider yield differences along the bond markets maturity spectrum. As a result, despite the significant price swings seen along the
entire maturity spectrum, longer-term bonds generally realized better overall performance during the reporting period, as well as for as a whole. Recent market turbulence and credit concerns have reinforced one of the central principles of
successful investing: diversification. As seen last year, investors with broad exposure to both the stock and bond markets had better protection from the full impact of weakness in areas that, prior to the credit crunch, were among the markets
leaders. Of course, past performance is not an indicator of future results, and diversification does not guarantee positive returns. However, we believe for a long-term investment objective that a diversification plan created with the help of your
financial advisor can overcome any short-term market risks and also capture the potential opportunities down the road that may arise as a result of current developments.

For information about how the fund performed during the reporting period, as well as market perspectives, we have provided a Discussion of Fund Performance given by the funds Portfolio Manager.

Thank you for your continued confidence and support.









DISCUSSION OF FUND PERFORMANCE

For the period of August through January as provided by Laurie Carroll, Portfolio Manager

Fund and Market Performance Overview

For the six-month period ended January Dreyfus Premier Yield Advantage Fund achieved total returns of for Class B shares and for Class D
In comparison, the Citigroup Treasury Benchmark Index, the funds benchmark, achieved a total return of for the same

Income-oriented sectors of the U.S.bond market encountered challenging credit and liquidity conditions as investors reacted negatively to defaults in the sub-prime mortgage sector, an intensifying economic slowdown, and
reports of heavy losses among banks and bond insurers. In contrast, U.S.Treasury securities generally gained value in a flight to quality that boosted investor demand. The funds returns substantially lagged its benchmark, which we
attribute to the funds underweight position in Treasuries as well as significant price dislocations which resulted from the sub-prime fallout.

The fund seeks as high a level of current income as is consistent with the preservation of capital, with minimal changes in share price.To pursue its goal, the fund invests only in investment-grade fixed-income securities
of U.S. and foreign issuers or the unrated equivalent (at the time of as determined by Dreyfus. This may include: U.S. government bonds and notes, corporate
bonds, municipal bonds, convertible securities, preferred stocks, inflation-indexed securities, asset-backed securities, mortgage-related securities (including CMOs), and foreign bonds.

To help reduce share price fluctuations, the fund seeks to keep the average effective duration of its overall portfolio at one year or less, and the fund may invest in securities with effective final maturities of any
length.

The fund may also utilize risk management techniques, including futures contracts, swap agreements and other derivatives, in seeking to reduce share price volatility, increase income and otherwise manage

The Fund







the funds exposure to investment risks.The fund will focus primarily on U.S. securities, but may invest up to of its total assets in fixed-income securities of foreign issuers.

The Credit Crunch Weighed Heavily on Yield-Oriented Bonds

The reporting period began in the midst of a credit crisis. Heightened volatility stemming from an unexpectedly large number of defaults among sub-prime mortgages had spread to other fixed-income market sectors just prior
to the start of the reporting period.These credit concerns arose at a time when the U.S. economy appeared to be slowing due to faltering housing prices and soaring energy costs. Consequently, newly risk-averse investors turned away from
yield-oriented segments of the bond market that had performed well in previous reporting periods, including the higher-quality, shorter-maturity corporate- and asset-backed securities in which the fund invests.

The Federal Reserve Board (the Fed) intervened in August by reducing the rate it charges member banks for overnight loans. However, the resulting rally was derailed by disappointing economic news and reports of
heavy sub-prime losses among commercial and investment banks. The Fed attempted to forestall further economic impairment by reducing the federal funds rate the rate banks charge one another for overnight loans from at the start of
the reporting period to by the end of Still, investor sentiment continued to deteriorate in January.The Fed responded aggressively by reducing the federal funds rate another basis points to in two separate moves in
the latter part of January.

Non-Treasury Holdings Constrained Relative Performance

As the credit crisis unfolded, the funds positions in shorter-duration corporate bonds and asset-backed securities fared poorly. Highly-rated, shorter-maturity securities historically have held up relatively well
during downturns, but this time proved to be different. Many of the funds corporate-backed holdings were issued by financial companies that incurred sub-prime related losses. Higher-quality asset-backed securities backed by home equity loans,
credit card receivables and other consumer debt also suffered in the credit crisis.







Whenever it was practical to do so, we attempted to trim the funds exposure to the more troubled segments of the market, especially longer-dated holdings with exposure to sub-prime mortgages. We sought to replace
those securities with shorter-maturity instruments in areas where we regarded valuations as relatively attractive. However, our ability to make these changes was limited by market conditions and the funds asset flows. Alternatively, in some
cases, it made little sense to us to sell depressed holdings that, in our analysis, were likely to be redeemed at full face value if held to maturity.

Exercising Patience and Discipline in a Changing Market

Elevated energy prices, the housing recession, tighter lending standards, and mounting bank losses have continued to hamper the U.S. economy, and we expect the Fed to reduce short-term interest rates further. In our view,
some areas of the short-term bond market have been punished too severely by skittish investors who have disregarded sound underlying credit fundamentals. Indeed, we believe that investors who have the patience to ride out current market turbulence
and the discipline to find attractively valued income opportunities are likely to reap investment rewards when the economy and fixed-income markets begin to recover in earnest.








Total return includes reinvestment of dividends and any capital gains paid, and does not take into
