DEAR FELLOW SHAREHOLDER: 
THE STOCK MARKET IN 2007 
It was a very complex and difficult market in 2007 with a great deal of volatility throughout the year. By the end
of the year, three themes were apparent for equity holders. The first was that international equities had outperformed domestic securities. The second was that growth stocks had outperformed value stocks. The third was that large-cap companies had
outperformed small-cap companies for the first time in four years. 
It was clear that the value sector was hurt dramatically by its large exposure to the
financial area. More than 30% of the value index is in financial stocks. The median of banks and other capital market stocks was down 25% in 2007. By contrast, energy was the leading sector of the stock market in 2007 and the median stock in that
group rose 29%. The average value fund could not get away from the plunging effect of their bank holdings. 
TOXIC SECURITIES FROM WALL STREET

We have heard all about toxic toys from China but the U.S. distributed internationally toxic securities to investors around the World, and these
investors were hurt badly. This has ruined the credibility of Wall Street and is now showing ripple effects in other markets that were previously untouched by the sub-prime mortgage crisis. From municipalities in Norway to retirement funds in
Australia, many investors were hurt by the securities that they purchased, believing in the rating system and seeking higher yields that were not normally available. Wall Street played on international investor greed and it came back to punish the
investors. 
THE ECONOMICS AND MARKET OUTLOOK FOR 2008 
Employment trends are slowing significantly. New jobs created will probably be well under fifty thousand in the coming months and unemployment rates are creeping up. Admittedly, the economy is still at full employment but trends are
starting to move into negative territory. This is having a negative effect on the consumer psyche. As a result, retailing is weak compared to expectations and credit card defaults are rising. Home prices are now showing signs of significant
declines. The data reported on the Case-Schiller Index through October was a 6.5% decline year-over-year in the national average housing price. This will have a chilling effect on consumer spending especially for big ticket items. 
One bright ray of hope in the picture is that the weak U.S. dollar has expanded sharply the contribution of exports to our economy. A weak dollar makes our goods
competitive in the world markets and we are exporting a large amount of product to the emerging markets where the economies remain strong. In addition, the weak dollar attracts a much larger volume of foreign tourists to the U.S. and this will be
positive for our balance of payments. 
We expect a difficult fourth quarter of 2007 with earnings for the S&amp;P 500 forecasted to be down 20%, earlier
estimates for the fourth quarter had them rising 10%. The big swing is due to the massive write-offs in the financial sector. However, beyond that, a number of other sectors are showing negative adjustments in earnings as the economy softens.
Retailing is particularly vulnerable. The enthusiasm that was present only six months ago has ebbed considerably. Overall, we expect a lackluster economy through the first half of 2008 with relatively low real growth rates but without a plunge into
recession. It will be a very difficult economy for many companies to increase their profits. Sector selection will be very important going forward. 
 
