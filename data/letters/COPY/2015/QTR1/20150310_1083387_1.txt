Dear Fellow Shareholders:



We are pleased to present the Kinetics Mutual Funds (Funds) Annual Report for the twelve-month period ended December Equity markets continued to advance in marking the sixth consecutive annual gain for the S&amp;P Index (S&amp;P with gains in excess of in five of those six years. Incremental improvement in company fundamentals and the U.S. economy surely contributed to the positive return; however, that is not to say that the fundamentals of the average S&amp;P company actually improved by this magnitude. To the contrary, we believe that valuation multiples expanded less as a function of business improvement, and more as the result of increased investor desire for perceived stability or safety. Our belief is supported by the fact that large cap stocks (as measured by the S&amp;P outperformed small cap stocks (as measured by the Russell Index) by nearly basis points for the year. Horizon Kinetics LLC, parent company to Kinetics Asset Management LLC (Kinetics), tends to source investment ideas in higher growth companies; these tend not to be amongst the largest corporations in the economy. Thus, the markets apparent desire for safety in was to the detriment of our annual returns. The Funds generated returns as follows during the year (No Load Class): The Paradigm Fund The Multi-Disciplinary Income Fund The Small Cap Opportunities Fund The Market Opportunities Fund The Internet Fund The Medical Fund The Alternative Income Fund and The Global Fund This compares to returns of for the S&amp;P for the Russell Index, for the Barclays year U.S. Credit Bond Index, for the Barclays U.S. Aggregate Bond Index, and for the MSCI All Country World Index.



We attempt to achieve long-term investment returns that are attractive on an absolute basis, while also outperforming our respective peer investors and reference benchmarks. In order to accomplish these objectives, we utilize a repeatable investment process that we believe will unearth excellent companies that are trading at inexpensive valuations. However, unlike many of our peer investors, we do not begin our investment search by evaluating the benchmark index constituents. In fact, we believe that there is considerable value in looking beyond such companies. Our process has been very effective over the long term, but no single investment strategy can reasonably be expected to outperform during every environment. The current environment has proven to be challenging for our strategies, but we view the factors driving this dislocation as temporary in nature. Accordingly, we have






















no intention of altering our process, and we continue to believe in the investments of our Funds.



The trend toward passive investment continues unabated: in investors withdrew billion from conventional mutual funds while investing billion into exchange-traded funds (ETFs). In the short term, these fund flows result in inflated valuations for the companies that comprise the respective indexes. To the extent that the redeemed active managers own companies that are not included in the indexes, the process also has the result of compressing the valuations of those companies. Of course, there is a natural temptation to follow these trends and ride the index companies higher. However; the duration, magnitude and timing of this trend remain to be seen. In our opinion, the largest weights in many of the major indexes appear to be trading at extended valuations which, though supported by flows into ETFs in the near term, do not seem to be sustainable in the face of slowing top-line growth, among other obstacles. Timing the markets is not a viable strategy, in our view. Furthermore, we are comfortable investing where others are not; in fact, that is our preference. We are happy to allocate assets to companies that we believe will compound shareholder value over an extended horizon, though patience may be required for the stock price of such companies to reflect their intrinsic value.



Shareholders can continue to access additional information from our website, www.kineticsfunds.com. This website provides a broad array of information, including recent portfolio holdings, quarterly investment commentaries, conference call transcripts, newsflashes, recent performance data, and online access to account information.



Kinetics offers the following funds to investors:



The Paradigm Fund focuses on companies that we believe are valued attractively and currently have, or are expected to soon have, sustainable high business returns. The Fund has produced attractive risk adjusted returns since its inception, while maintaining amongst the lowest turnover rates in the industry. The Fund is Kinetics flagship fund.



The Multi-Disciplinary Income Fund seeks to utilize stock options and fixed-income investments in order to provide investors with equity-like returns, but






















with more muted volatility. At times, the options strategies of the Fund may cause the manager to purchase equity securities.



The Small Cap Opportunities Fund focuses on undervalued and special situation small capitalization equities that we believe have the potential for rewarding long-term investment results. The same investment fundamentals employed by The Paradigm Fund are used to identify such opportunities.



The Medical Fund is a sector fund, offering an investment in scientific discovery within the promising field of medical research, particularly in the development of cancer treatments and therapies. As a sector fund, the Fund is likely to have heightened volatility.



The Internet Fund is a sector fund that focuses on companies engaged in the evolution of internet-related developments. As such, this Fund has been, and is likely to continue to be, quite volatile. The Fund is not designed to be a major component of ones equity exposure. More recently, this Fund has been focusing on content companies, which we believe will be the winners in the next generation of Internet development.



The Global Fund is focused on selecting long life cycle international companies that we believe can generate long-term wealth through their business operations. This Fund is presently identifying exciting opportunities in the more developed markets.



The Market Opportunities Fund focuses on those companies that benefit from increasing transactional volume or throughput, such as publicly-traded exchanges and credit card processors, or companies that act as facilitators, such as gaming companies, airports and publicly-traded toll roads.



The Alternative Income Fund seeks to provide current income and gains, with a secondary objective of obtaining long-term growth of capital. The Fund utilizes stock options and fixed-income investments and seeks to generate a total return that exceeds most short-term U.S. fixed income indexes, with limited market value variability.



The Kinetics Investment Team



























KINETICS MUTUAL FUNDS, INC.



Investment Commentary






Dear Fellow Shareholders,



The aggregate amount of resources that investors allocate to equity investing is ostensibly limitless. Consider that Blackrock, Inc., the largest asset management company in the world, oversees more than trillion of capital, has a market capitalization of over billion and has over employees. Over the twelve-month period ended September Blackrock recorded nearly billion of expenses related to its asset management business. If investment returns were determined simply by the amount of resources dedicated to the process, there would be no reason to look beyond Blackrock. However, time has shown that size and resources are not very predictive of superior investment returns. In fact, many of the largest managers, with the greatest amount of resources available, tend to generate returns that are not materially different from their respective benchmark indexes. This is logical, as most managers begin their investment process by evaluating the reference benchmark and selecting from companies within it for investment. This is particularly true for managers that cater to large institutions. There is very little potential for these managers to add value by attempting to emulate more traditional investment processes. The same applies within the context of lower cost exchange traded fund (ETF) alternatives, which provide very similar exposure to their benchmarks, as well as to the indexes which they seek to track. An investor who tries to outperform a benchmark by selecting amongst its components is all but guaranteed to have very high correlation to said benchmark and is unlikely to deviate in any material way in terms of performance. Perhaps this is a sound business strategy for asset retention, but surely not for superior investment results.



Why would we wish to emulate an index that can be recreated at nearly zero cost, or attempt to compete with managers the size of Blackrock? A far more attractive alternative, in our opinion, is to find securities that are not included in the indexes, and hence, are largely ignored by most investors. As a function of not belonging to an index, many of these businesses are priced at material discounts to comparable companies included in the indexes. To the extent that fundamentalsnot fund flows or sentimentdrive long-term security prices, we believe that the discounts at which these companies trade are not permanent. It seems all but certain that other investors recognize this valuation divide, but most ignore the opportunity that we are embracing due to the lack of a perceived catalyst. We believe that the uncertainty regarding the timing of an investments payout is critical to the undervaluation of its shares; we refer to this as the equity yield curve. Historically, our portfolios have been populated by companies that are at various stages of the equity yield curve; hence, performance is smoother than one might expect. However, there are periods when these dynamics do not materialize smoothly; hence, we can experience considerable deviations from index returns (both positive and negative).






















Horizon Kinetics LLC, parent company to Kinetics Asset Management LLC, has followed, through its subsidiary investment advisers, a very similar investment process dating back to its subsidiaries inception over years ago. This is important, as the process has not changed as a result of indexation, but it has always led us to investments in companies that are overlooked by most investors. For example, spin-off securities have historically traded at a discount because institutional investors did not care to understand the spun-off business and simply sold without any analysis. One might hypothesize that this opportunity is no longer available as investors have become more sophisticated and information more widely disseminated, but we believe the inverse is true; spin-off pricing remains inefficient due to index rules that tend to underweight or exclude spin-offs. Owner-operators are a similar example, as these companies are undervalued due to a limited share float, making the companies unattractive to indexes and large institutional investors. Prior to the recent market share gains by ETFs and index investing, owner operators were frequently valued at a premium to the overall market, and deservedly so. Why not pay a premium for a superior capital allocator? Though such companies have always been represented in the portfolios, we would have owned more of them historically had they been available at the low valuation multiples that exist now.



Ultimately, the investment team at Horizon Kinetics attempts to purchase good or great businesses at a fair or low valuation multiple. However, good and great businesses are seldom available at attractive prices; thus, we look for areas that are prone to structural, inefficient pricing. The previously mentioned owner-operators and spin-offs are but two examples of many predictive attributes for such mispricing and future performance. As a result of investing so differently from common practice, we recognize that there will be periods of both considerable outperformance and underperformance, but we believe that over the long term, our cumulative performance will be superior to any reference benchmark. In our belief, it is very hard to foresee a scenario where the large and fully valued S&amp;P Index constituent companies outperform our portfolios over the long term, and we view the current performance as an anomaly and a buying opportunity.



We believe that our investment process, complete with conservative assessments and high discount rates, embeds an attractive risk/reward profile in the investments. For investors who may be even more risk averse (compared to our high hurdle discount rates), we urge these individuals to consider our option income strategies, which seek to provide equity-like returns, with lowered realized volatility and greater downside protection. In our opinion, these strategies offer benefits compared to conventional yield-oriented strategies such as traditional bonds, leveraged loans, real estate investment trusts, and master limited partnerships.






















Disclosure



This material is intended to be reviewed in conjunction with a current prospectus, which includes all fees and expenses that apply to a continued investment, as well as information regarding the risk factors, policies and objectives of the Funds. Read it carefully before investing.



Mutual Fund investing involves risk. Principal loss is possible. Because The Internet Fund, The Medical Fund and The Market Opportunities Fund invest in a single industry or geographic region, their shares are subject to a higher degree of risk than funds with a higher level of diversification. Internet, biotechnology and certain capital markets or gaming stocks are subject to a rate of change in technology, obsolescence and competition that is generally higher than that of other industries, hence they may experience extreme price and volume fluctuations.



International investing [for all Funds] presents special risks including currency exchange fluctuation, government regulations, and the potential for political and economic instability. Accordingly, the share prices for these Funds are expected to be more volatile than that of U.S.-only funds. Past performance is no guarantee of future performance.



Because smaller companies [for The Small Cap Opportunities Fund] often have narrower markets and limited financial resources, they present more risk than larger, more well established, companies.



Non-investment grade debt securities [for all Funds], i.e., junk bonds, are subject to greater credit risk, price volatility and risk of loss than investment grade securities.



Further, options contain special risks including the imperfect correlation between the value of the option and the value of the underlying asset. Investments [for The Multi-Disciplinary Income Fund and The Alternative Income Fund] in futures, swaps and other derivative instruments may result in loss as derivative instruments may be illiquid, difficult to price and leveraged so that small changes may produce disproportionate losses to the Funds. To the extent the Funds segregate assets to cover derivative positions, they may impair their ability to meet current obligations, to honor requests for redemption and to manage the investments in a manner consistent with their respective investment objectives. Purchasing and writing put and call






















options and, in particular, writing uncovered options are highly specialized activities that entail greater than ordinary investment risk.



As non-diversified Funds [except The Global Fund and The Multi-Disciplinary Income Fund] the value of Fund shares may fluctuate more than shares invested in a broader range of industries and companies. Unlike other investment companies that directly acquire and manage their own portfolios of securities, The Kinetics Mutual Funds pursue their investment objectives by investing all of their investable assets in a corresponding portfolio series of the Kinetics Portfolios Trust.



The information concerning the Funds included in the shareholder report contains certain forward-looking statements about the factors that may affect the performance of the Funds in the future. These statements are based on Fund managements predictions and expectations concerning certain future events and their expected impact on the Funds, such as performance of the economy as a whole and of specific industry sectors, changes in the levels of interest rates, the impact of developing world events, and other factors that may influence the future performance of the Funds. Management believes these forward-looking statements to be reasonable, although they are inherently uncertain and difficult to predict. Actual events may cause adjustments in portfolio management strategies from those currently expected to be employed.



The Nasdaq Composite (NASDAQ) and the Standard &amp; Poors Index (S&amp;P each represent an unmanaged, broad-basket of stocks. They are typically used as a proxy for overall market performance.



Distributor: Kinetics Funds Distributor LLC is not an affiliate of Kinetics Mutual Funds, Inc. Kinetics Funds Distributor LLC is an affiliate of Kinetics Asset Management LLC, Investment Adviser to Kinetics Mutual Funds, Inc.



For more information, log onto www.kineticsfunds.com. January Kinetics Asset Management LLC

























How a Investment Has Grown:







The charts show the growth of a investment in the Feeder Funds as compared to the performance of two or more representative market indices. The tables below the charts show the average annual total returns on an investment over various periods. Returns for periods greater than one year are average annual total returns. The annual returns assume the reinvestment of all dividends and distributions, however, the graph and table do not reflect the deduction of taxes that a shareholder would pay on fund distributions or the redemption of fund shares. Past performance is not predictive of future performance. Current performance may be lower or higher than the returns quoted below. The performance data reflects voluntary fee waivers and expense reimbursements made by the Adviser and the returns would have been lower if these waivers and expense reimbursements were not in effect. Investment return and principal value will fluctuate, so that your shares, when redeemed, may be worth more or less than their original costs.






The S&amp;P Index is a capital-weighted index, representing the aggregate market value of the common equity of stocks primarily traded on the New York Stock Exchange. The S&amp;P is unmanaged and includes the reinvestment of dividends and does not reflect the payments of transaction costs and advisory fees associated with an investment in the Funds. The securities that comprise the S&amp;P may differ substantially from the securities in the Funds portfolios. It is not possible to directly invest in an index.






The NASDAQ Composite Index is a broad-based capitalization-weighted index of all NASDAQ stocks. The NASDAQ is unmanaged and does not include the reinvestment of dividends and does not reflect the payment of transaction costs or advisory fees associated with an investment in the Funds. The securities that comprise the NASDAQ Composite may differ substantially from the securities in the Funds portfolios. It is not possible to directly invest in an index.






The MSCI ACWI (All Country World Index) Index is a free float- adjusted market capitalization weighted index that is designed to measure the equity market performance of developed and emerging markets. As of June the MSCI ACWI consists of country indices comprising developed and emerging market country indices. The developed market country indices included are: Australia, Austria, Belgium, Canada, Denmark, Finland, France, Germany, Hong Kong, Ireland, Israel, Italy, Japan, Netherlands, New Zealand, Norway, Portugal, Singapore, Spain, Sweden, Switzerland, the United Kingdom and the United States. The





























emerging market country indices included are: Brazil, Chile, China, Colombia, Czech Republic, Egypt, Greece, Hungary, India, Indonesia, Korea, Malaysia, Mexico, Peru, Philippines, Poland, Qatar, Russia, South Africa, Taiwan, Thailand, Turkey, and United Arab Emirates. The securities that compromise the MSCI ACWI may differ substantially from the securities in the Funds portfolios. It is not possible to directly invest in an index.






The Russell Index is a subset of the Russell Index representing approximately of the total market capitalization of that index. It includes approximately of the smallest securities based on a combination of their market cap and current index membership. The Russell Index is constructed to provide a comprehensive and unbiased small-cap barometer and is completely reconstituted annually to ensure larger stocks do not distort the performance and characteristics of the true small-cap opportunity set. The securities that compromise the Russell may differ substantially from the securities in the Funds portfolio. It is not possible to directly invest in an index.






The MSCI EAFE Index (Europe, Australasia, Far East) is a free float-adjusted market capitalization index that is designed to measure the equity market performance of developed markets, excluding the United States and Canada. As of June the MSCI EAFE Index consisted of the following developed market country indices: Australia, Austria, Belgium, Denmark, Finland, France, Germany, Hong Kong, Ireland, Israel, Italy, Japan, the Netherlands, New Zealand, Norway, Portugal, Singapore, Spain, Sweden, Switzerland, and the United Kingdom. The securities that compromise the MSCI EAFE may differ substantially from the securities in the Funds portfolio. It is not possible to directly invest in an index.






The Barclays U.S. Year Credit Index measures the performance of investment grade corporate debt and sovereign, supranational, local authority and non-U.S. agency bonds that are U.S. dollar denominated and have a remaining maturity of greater than or equal to one year and less than three years. The securities that compromise the Barclays U.S. Year Credit Index may differ substantially from the securities in the Funds portfolio. It is not possible to directly invest in an index.






The Barclays U.S. Aggregate Bond Index covers the USD-denominated, investment-grade, fixed-rate, taxable bond market of SEC-registered securities. The Index includes multiple types of government and corporate-issued bonds, some of which are asset-backed. The securities that compromise the Barclays U.S. Aggregate Bond Index may differ substantially from the securities in the Funds portfolio. It is not possible to directly invest in an index.





























CBOE S&amp;P BuyWrite Index (BXM) is a benchmark index designed to track the performance of a hypothetical buy-write strategy on the S&amp;P The securities that comprise the CBOE S&amp;P BuyWrite Index may differ substantially from the securities in the Funds portfolio. It is not possible to directly invest in an index.






CBOE S&amp;P PutWrite Index (PUT) is a benchmark index designed to track the performance of a passive program that sells near-term, at-the-money S&amp;P Index puts. The securities that comprise the CBOE S&amp;P PutWrite Index may differ substantially from the securities in the Funds portfolio. It is not possible to directly invest in an index.


























The Internet Fund

December December













Ended









Advisor



Advisor














No Load



Class A



Class A



Advisor






NASDAQ





Class



(No Load)



(Load



Class C



S&amp;P



Composite



One Year




%



%



%



%



%



%


Five Years




%



%



%



%



%



%


Ten Years




%



%



%


N/A




%



%


Since Inception



























No Load Class
































%


N/A



N/A



N/A




%



%


Since Inception



























Advisor



























Class A































N/A




%



%


N/A




%



%


Since Inception



























Advisor



























Class C































N/A



N/A



N/A




%



%



%





Reflects front-end sales charge of



Returns for periods greater than one year are average annual total returns.






















The Global Fund

December December













Ended









Advisor



Advisor














No Load



Class A



Class A



Advisor











Class



(No Load)



(Load



Class C



S&amp;P



MSCI ACWI



One Year




%



%



%



%



%



%


Five Years




%



%



%



%



%



%


Ten Years




%


N/A



N/A



N/A




%



%


Since Inception



























No Load Class
































%


N/A



N/A



N/A




%



%


Since Inception



























Advisor



























Class A































N/A




%



%


N/A




%



%


Since Inception



























Advisor



























Class C































N/A



N/A



N/A




%



%



%





Reflects front-end sales charge of



Returns for periods greater than one year are average annual total returns.






















The Paradigm Fund

December December













Ended









Advisor



Advisor

















No Load



Class A



Class A



Advisor



Institutional











Class



(No Load)



(Load



Class C



Class



S&amp;P



MSCI ACWI



One Year




%



%



%



%



%



%



%


Five Years




%



%



%



%



%



%



%


Ten Years




%



%



%



%


N/A




%



%


Since Inception































No Load Class




































%


N/A



N/A



N/A



N/A




%



%


Since Inception































Advisor Class A



































N/A




%



%


N/A



N/A




%



%


Since Inception































Advisor Class C



































N/A



N/A



N/A




%


N/A




%



%


Since Inception































Institutional Class



































N/A



N/A



N/A



N/A




%



%



%





Reflects front-end sales charge of



Returns for periods greater than one year are average annual total returns.























The Medical Fund

December December













Ended









Advisor



Advisor














No Load



Class A



Class A



Advisor






NASDAQ





Class



(No Load)



(Load



Class C



S&amp;P



Composite



One Year




%



%



%



%



%



%


Five Years




%



%



%



%



%



%


Ten Years




%



%



%


N/A




%



%


Since Inception



























No Load Class
































%


N/A



N/A



N/A




%



%


Since Inception



























Advisor Class A































N/A




%



%


N/A




%



%


Since Inception



























Advisor Class C































N/A



N/A



N/A




%



%



%





Reflects front-end sales charge of



Returns for periods greater than one year are average annual total returns.






















The Small Cap Opportunities Fund

December December













Ended









Advisor



Advisor

















No Load



Class A



Class A



Advisor



Institutional











Class



(No Load)



(Load



Class C



Class



Russell



S&amp;P



One Year




%



%



%



%



%



%



%


Five Years




%



%



%



%



%



%



%


Ten Years




%



%



%


N/A



N/A




%



%


Since Inception































No Load Class




































%


N/A



N/A



N/A



N/A




%



%


Since Inception































Advisor Class A



































N/A




%



%


N/A



N/A




%



%


Since Inception































Advisor Class C



































N/A



N/A



N/A




%


N/A




%



%


Since Inception































Institutional Class



































N/A



N/A



N/A



N/A




%



%



%





Reflects front-end sales charge of



Returns for periods greater than one year are average annual total returns.






















The Market Opportunities Fund

January December













Ended









Advisor



Advisor
















No Load



Class A



Class A



Advisor



Institutional











Class



(No Load)



(Load



Class C



Class



S&amp;P



MSCI EAFE



One Year




%



%



%



%



%



%



%


Five Years




%



%



%



%



%



%



%


Since Inception































No Load Class




































%


N/A



N/A



N/A



N/A




%



%


Since Inception































Advisor Class A



































N/A




%



%


N/A



N/A




%



%


Since Inception































Advisor Class C



































N/A



N/A



N/A




%


N/A




%



%


Since Inception































Institutional Class



































N/A



N/A



N/A



N/A




%



%



%





Reflects front-end sales charge of



Returns for periods greater than one year are average total returns.























The Alternative Income Fund

June December













Ended





















Barclays























U.S.



Barclays








Advisor



Advisor









Year



U.S.





No Load



Class A



Class A



Advisor



Institutional



Credit



Aggregate





Class



(No Load)



(Load



Class C



Class



Index



Bond Index



One Year




%



%



%



%



%



%



%


Five Years




%



%



%



%



%



%



%


Since Inception































No Load Class




































%


N/A



N/A



N/A



N/A




%



%


Since Inception































Advisor Class A



































N/A




%



%


N/A



N/A




%



%


Since Inception































Advisor Class C



































N/A



N/A



N/A




%


N/A




%



%


Since Inception































Institutional Class



































N/A



N/A



N/A



N/A




%



%



%





Reflects front-end sales charge of



Returns for periods greater than one year are average total returns.






















The Multi-Disciplinary Income Fund

February December













Ended












Advisor























Advisor



Class A




















No Load



Class A



(Load



Advisor



Institutional



S&amp;P



CBOE



CBOE





Class



(No Load)







Class C



Class






Buy



PUT



One Year




%



%



%



%



%



%



%



%


Five Years




%



%



%



%



%



%



%



%


Since Inception



































No Load Class








































%


N/A



N/A



N/A



N/A




%



%



%


Since Inception



































Advisor Class A







































N/A




%



%


N/A



N/A




%



%



%


Since Inception



































Advisor Class C







































N/A



N/A



N/A




%


N/A




%



%



%


Since Inception



































Institutional Class







































N/A



N/A



N/A



N/A




%



%



%



%





Reflects front-end sales charge of



Returns for periods greater than one year are average total returns.


























KINETICS MUTUAL FUNDS, INC. THE FEEDER FUNDS



Expense Example
