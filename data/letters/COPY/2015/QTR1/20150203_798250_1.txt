Dear Shareholders: 
The U.S. economy stands on firmer ground than the rest of the world, expanding at an annualized pace of more than 3%. The labor market has regained momentum,


consumer confidence has improved and gasoline prices have fallen sharply. Accordingly, expectations are strong for continued economic recovery into 2015. 
In contrast, all other major economic regions are struggling. The eurozone economy is barely expanding, and deflation is a growing concern. The European Central
Bank (ECB) has attempted to stimulate the regions economy, and many market participants believe the ECB will introduce large-scale asset purchases. 
Despite Japans efforts to strengthen its economy, its sales tax increase last spring tipped the country into a technical recession, 

leading to additional monetary stimulus from the Bank of Japan. The Chinese economy is slowing down, and its growth rate will likely continue to fall as it transitions to a more sustainable
basis. 
As always, active risk management is integral to how we at MFS® manage your investments. We use a collaborative process,
sharing insights across asset classes, regions and economic sectors. Our global investment team uses a diversified, multidisciplined, long-term approach. 
Applying proven principles, such as asset allocation and diversification, can best serve investors over the long term. We are confident that this approach can help
you as you work with your financial advisors to reach your goals in the years ahead. 
Respectfully, 


