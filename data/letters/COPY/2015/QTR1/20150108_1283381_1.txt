Dear Shareholder,




Enclosed is the Annual Report to shareholders of the TFS Market Neutral Fund (ticker: TFSMX), the TFS Small Cap Fund (ticker: TFSSX) and the TFS Hedged Futures Fund (ticker: TFSHX) for the year ended October On behalf of the investment manager, TFS Capital LLC (TFS Capital), we would like to thank you for your continued investment.




TFS Market Neutral Fund (ticker: TFSMX)









return



return



return



return


Since Inception





TFS Market Neutral Fund




















S&amp;P Index























Average annual total returns for periods ended October Past performance is not a guarantee of future results.




TFSMX marked its anniversary in September. TFS Capital is proud to have launched TFSMX when there were very few liquid alternative mutual funds available to retail investors. For the period ended October TFSMX had a category percentile rank of out of funds in Morningstars market neutral




TFSMX did not produce positive alpha (i.e., the return that cannot be explained by market exposure) overall during the fiscal year. While the Fund, with a beta in the range, should not be expected to outperform the equity markets when they rise significantly, its slightly negative return for the fiscal year is well below expectations.




The shortfall occurred primarily during the first four months of the fiscal year; the Fund was down each of these months, while the S&amp;P Index rose during three of those four months. The Fund produced positive alpha during the later eight months of the year, but fell short of overcoming the negative performance during the earlier portion of the year.




Many of the Funds short positions rose significantly more than the market during those first months, leading to substantial underperformance. TFS Capital believes this period of underperformance in TFSMX at the beginning of the fiscal year may have been driven by a short-squeeze affecting the portfolio (i.e., a price dislocation that occurs when short positions in thinly traded securities are exited quickly resulting in a rise in share price). These stocks began underperforming the market later in the year, helping the Fund recover a portion of the lost alpha. We continue to believe that our selection process for short positions is advantageous to the Fund under typical market conditions.




TFSMXs performance over longer periods of time compares more favorably to the overall U.S. equity market as measured by the S&amp;P Index. From inception through October TFSMX generated a average annual return and had an annualized standard deviation (a measure of volatility) of whereas the S&amp;P Index generated an average annual return and had an annualized standard deviation of Therefore, during this time period, TFSMX produced alpha while subjecting investors to less volatility than the S&amp;P Index. For the periods ended October TFSMX was ranked among among and among funds in the Lipper Equity Market Neutral category, during the ten, five and three-year periods,










The strategies used in managing TFSMX have resulted in its returns having a low correlation to other asset classes. According to Modern Portfolio Theory, portfolios may benefit by combining non-correlated investments. For your reference, below are the TFS Market Neutral Funds correlations to several other asset classes since inception. It is notable that TFSMXs correlation to the equity indices has increased overall in recent years.








Index


Correlation Since Inception





S&amp;P Index








Russell Index








MSCI EAFE Index








Barclays U.S. Aggregate Bond Index








Dow Jones REIT ETF Index








Dow Jones Commodity Futures










The TFS Market Neutral Fund had been operating under a policy that limited additional investments. However, effective March we opened the TFS Market Neutral Fund to all investors.




TFS Small Cap Fund (ticker: TFSSX)









return



return



return


Since Inception





TFS Small Cap Fund

















Russell Index




















Average annual total returns for periods ended October Past performance is not a guarantee of future results.




During the fiscal year, TFSSX outperformed its benchmark, the Russell Index. Given its long-only mandate, TFSSX is expected to have a high correlation to U.S. small-cap equity market movements with most of the performance deviation attributable to TFS Capitals equity selection. All strategies contributed to the outperformance, and we are pleased that the Fund added such significant alpha.




TFSSX also achieved its objective of outperforming the Russell Index over longer periods of time. In fact, it now has over an track record and has achieved an average annual return since inception that is higher than that of the Russell Index. Based on its trailing return through October TFSSX was ranked among funds in the Lipper Small Cap Core




TFS Hedged Futures Fund (ticker: TFSHX)









return


Since Inception





TFS Hedged Futures Fund











S&amp;P Diversified Trends Indicator











S&amp;P Index














Average annual total returns for periods ended October Past performance is not a guarantee of future results.




TFSHX, a hedged futures fund, was down during the fiscal year. Negative performance of the currency sector offset small gains in the commodities and financial sectors during the period.










Like the TFS Market Neutral Fund, the TFS Hedged Futures Fund has demonstrated a low correlation to many market indices. This low correlation suggests the addition of the TFS Hedged Futures Fund may improve the risk-adjusted performance of many investment portfolios. The TFS Hedged Futures Fund has had a slightly negative correlation to the S&amp;P Diversified Trends Indicator, which suggests that it may also have a low correlation to other managed-futures mutual funds that track this index. The TFS Hedged Futures Fund also has a low correlation to the TFS Market Neutral Fund which suggests that these two investments may complement each other in an investment portfolio. For your reference, below are the TFS Hedged Futures Funds correlations to several other asset classes including the TFS Market Neutral Fund.








Index


Correlation Since Inception





TFS Market Neutral Fund (ticker: TFSMX)








S&amp;P Diversified Trends Indicator








S&amp;P Index








Russell Index








S&amp;P GSCI Index








Barclays U.S. Aggregate Bond Index











As always, we do not recommend allocating a high percentage of ones assets to any one investment fund managed by TFS Capital given the risks inherent in each product. Rather, we believe that they are best used as a component of a diversified portfolio of investments. If you have any questions regarding the information presented here, please contact us.



Best regards,
