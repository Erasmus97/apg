Dear Shareholder:



Fourth Quarter and Review



Bridges Investment Fund had a total return of for the one year period ending December comparison, the S&amp;P Index had a total return of while the Russell Growth Index finished up for the year.The Fund had total returns of and for the and year periods ending December compared to total returns of and for the S&amp;P Index, and and for the Russell Growth Index over the same periods of time.Three, five, and ten year periods are annualized.The Funds gross expense ratio is per the most recent prospectus.



Stocks showed solid gains in the fourth quarter of as the S&amp;P had a total return of Fund had a total return of during the fourth quarter of S&amp;P ended the year at down slightly from its all-time closing high of which was set on December



Performance data quoted represents past performance. Past performance does not guarantee future results. The investment return and principal value of an investment will fluctuate so that an investors shares, when redeemed, may be worth more or less than their original cost. Current performance of the Fund may be lower or higher than the performance stated above. Performance data current to the most recent month end may be obtained by calling



Outlook for



We believe that the risks for equity investors at the outset of are higher than they have been at any time since year-end stock prices, as measured by the S&amp;P Index, have risen since year-end with very few corrections along the way; valuations for stocks have expanded coincident with the strong rise in stock prices; continued strength in the U.S. dollar relative to other major currencies could negatively impact the earnings of U.S. companies that do substantial business outside of the U.S.; global economic conditions remain soft; and the substantial decline in many commodity prices raises uncertainty and may serve to curtail investor appetite for risk.



Notwithstanding the foregoing risks, we remain constructive on the long-term outlook for stocks.



Our constructive long-term outlook for U.S. equites is predicated on two main tenets.First, we believe that corporate earnings can continue to grow over the long run at or near rates experienced in recent years, and we believe valuation levels for U.S. equities remain reasonable given our outlook for earnings.



We believe fair value for the S&amp;P for is in a range of based on a P/E of roughly estimated earnings of per share. Our fair value estimate for is based on estimated earnings per share of





























Shareholder Letter


January









We continue to favor equities that have strong business franchises and an ability to grow revenues, cash flows, earnings, dividends, and underlying business value at solid rates despite a continued sluggish and highly competitive global economic environment. We believe that companies that are able to achieve solid growth in business value should be able to generate total returns for their shareholders that are commensurate with business value growth.



Our Portfolio



The Funds portfolio continues to be comprised primarily of companies with strong balance sheets, historically high levels of profitability, and a demonstrated ability to grow business value over the long run despite periodically challenging economic conditions.



The following table summarizes the changes we made in the Fund in



BRIDGES INVESTMENT FUND CHANGES FOR







NEW BUYS:


ADDS:


TRIMS:


ELIMINATED:



American Express


Altria


Apple


Accenture



Continental Resources


Amazon.com


Capital One


Allergan



Fedex


Biogen


Financial


Apache



Gilead Sciences


Blackrock


Roper


Cognizant



Johnson &amp; Johnson


Celgene


Union Pacific


Tech Solutions



Las Vegas Sands


Chicago Bridge &amp; Iron



Continental



Perrigo


Comcast Corp



Resources



Valeant


Davita



EMC



Pharmaceuticals


Disney



Las Vegas Sands




eBay



Valeant




Ecolab



Pharmaceuticals




Express Scripts



Valmont




JP Morgan Chase



Waters




McDonalds






Philip Morris Intl






Priceline






Schlumberger








Fund holdings and sector allocations are subject to change and are not recommendations to buy or sell any security. For a complete list of fund holdings, please refer to the Schedule of Investments in this report.



The companies that added the most value to the Funds return in included Actavis, Apple, Berkshire-Hathaway, Celgene, DirecTV, Disney, Express Scripts, Union Pacific, and Wells Fargo.



The companies that were the largest drag on performance in included Amazon, Chicago Bridge &amp; Iron, Continental Resources, Eaton, Google, Las Vegas Sands, and Priceline.



We believe the Funds holdings are attractively valued looking out over the next several years.At present, the Funds portfolio trades at estimated earnings and estimated earnings, and have projected long term annual earnings growth of compares favorably with the P/E, P/E, and long term annual earnings growth projected for the S&amp;P



We believe that the Funds companies should be well-positioned to grow shareholder value at attractive rates in the future.Our companies have strong balance





























Shareholder Letter


January









sheets and business models that we believe should allow them to grow revenues, earnings, and free cash flow at attractive rates over the long run.



The Funds investment process continues to be characterized by a few key tenets:



focus on high quality companies with good prospects for growing their business value over time



strong valuation discipline



long term approach to equity investing



The core of our investment management approach is based on the idea that over the long run, good businesses can produce good investment returns for their shareholders.We seek to identify and own undervalued businesses that have been growing their business value at attractive rates. Over time, we seek to benefit from our investment approach in two as our companies move from undervalued toward our estimate of fair value, and due to the growth in our companies underlying business value over time.



The Fund will hold its annual meeting on March management will provide its outlook for the capital markets and the Fund for and beyond.We appreciate your continued investment in the Fund, and encourage all shareholders to attend this years annual meeting.








Sincerely,
