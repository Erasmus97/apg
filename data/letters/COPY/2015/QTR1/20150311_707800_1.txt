Dear Fellow Shareholder:



Volatility remained elevated throughout the fourth quarter in both the high yield and equity markets. High yield bond spreads and the equity market volatility index spiked to levels not experienced in several years due to fears of an Ebola epidemic in October, and concerns about energy and energy-related companies caused by significant declines in oil later in the quarter. While these issues have seemingly been absorbed by the capital markets, there remain global growth concerns due to continued economic weakness in Europe, Japan, and China, as well as the potential for fiscal problems in significant oil-producing countries, such as Russia, as a result of the decline in oil prices. Commentary surrounding monetary policy actions from several Central Banks also heavily influenced how the capital markets reacted throughout the quarter. The Federal Reserve ended its long-standing asset purchase program in October due to the improvement in the U.S. economy, while the European Central Bank, along with other Central Banks around the world, signaled a more accommodative stance due to a deterioration of economic activity. The Federal Reserve (Fed) and other Central Bank actions will remain an influence that market participants will be considering throughout as the degree of monetary policy accommodation set by the Fed or other Central Banks could impact market returns going forward.



Despite elevated geopolitical tensions and increased concerns regarding global economic growth throughout the U.S. economy remained relatively resilient. A highlight of the strength of the U.S. economy in was the positive trajectory of many employment statistics. The U.S. unemployment rate declined to the lowest level since while nearly three million Americans found work in the most in years. Market participants are hopeful that the strength in employment is a sign of optimism on the part of companies that economic growth in the U.S. will persist, even as overseas economies struggle. Tighter labor market conditions may lead to more meaningful growth in wage inflation, which was relatively lackluster throughout The prospects of lower energy costs and increased wage growth heading into likely bode well for continued gains in U.S. consumer confidence and spending. With household spending accounting for approximately of U.S. economic activity, the strength of the consumer and the willingness and ability to purchase goods and services are a key driver of corporate profits and economic growth.



As we look back at what took place in and also evaluate the growing list of uncertainties that could impact the capital markets throughout and beyond, we are reminded that our relatively conservative approach (which focuses on the deleveraging of corporate balance sheets) within the high yield asset class and disciplined research process have, in our view, benefited both our high yield and equity strategies.



NOT A PART OF THE ANNUAL REPORT


























Jobs, Inflation, and the Fed Key indicators of economic strength, such as employment, capacity utilization, and retail sales continued to show positive signs throughout much of the fourth quarter. In addition, data regarding housing and autos, while not accelerating as quickly as it had been, was relatively stable. Assuming the Non-Farm Payroll report for December shows greater than jobs were created during the month, this would mark the tenth month of that registered over new jobs. The unemployment rate declined to at the end of November from at the beginning of the year. It seems likely that if job growth continues around the recent pace throughout the unemployment rate could drop below While there are signs that future wage pressure may be on the horizon, which could cause inflation measures to accelerate more rapidly, this occurrence of relatively meager wage growth has been puzzling to many market participants and has caused a fair amount of uncertainty as to how much employment slack there really is in the economy. The Bureau of Labor Statistics continues to report that the labor participation rate has yet to return to levels last seen in Commentary out of the Federal Reserve, in addition to heavily scrutinized economic data, will likely shape views with regard to how market participants allocate capital throughout



Consumer Confidence The U.S. consumer should, in our view, enter on the most solid footing since the financial crisis. Foreclosure and delinquency data has declined significantly in recent years, while recent job creation strength and a better employment picture should help bolster consumer creditworthiness. In addition, an improved housing market and rising home prices have lifted many homeowners from an underwater position. The strengthening dollar and lower oil prices should help curb inflation pressures and make purchased goods cheaper. Lower oil prices are resulting in lower fuel and heating costs, which should be a fairly significant benefit to discretionary spending, at least as we begin Lower interest rates may also help bolster consumer spending on big ticket retail items and other large consumer purchases, such as a vehicle or a house. We intend to closely monitor these potential benefits to consumer confidence as well as many data points with regard to consumer spending as the year progresses. Should consumer spending show signs of accelerating, it may have positive ramifications for economic growth and corporate earnings; however, this may also result in accelerating core inflation measures and influence the Federal Reserves decision to raise interest rates. Conversely, should it look like consumer spending is remaining stagnant, expectations for economic growth and corporate earnings may need to be revised lower.



We remain committed to our time-tested and disciplined research process that not only includes detailed analysis of companies owned in our high yield and equity strategies, but also uncovers new opportunities within the high yield and equity markets. We continue to look for fiscally responsible management teams that we believe are committed to growing operations prudently and who recognize they can improve their credit profile and equity valuations by focusing on credit-specific measures. Our efforts remain focused on stability and predictability in the investment selection process which seeks to provide a less volatile high yield strategy designed to generate a reasonably consistent total return, while also attempting to find attractive equity investments that could experience further capital appreciation.



NOT A PART OF THE ANNUAL REPORT


























Thank you for your continued support and investment.



Sincerely,
