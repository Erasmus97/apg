Dear Shareholder,
After a rough start, the broad U.S. municipal bond market posted 11 consecutive months of gains to finish the fiscal year ended November 30, 2014, with a return of more than 8%.
Demand was strong for lower-rated and longer-dated munis, a trend that helped Vanguard Ohio Long-Term Tax-Exempt Fund return 10.19% for the 12 months. Price appreciation accounted for well over half of the fund’s results, unlike in the previous fiscal year, when price depreciation detracted significantly from returns.
The Long-Term Fund performed much better than its benchmark, which returned 8.60%, and its peer group, whose average return was 8.41%.
The rise in municipal bond prices drove the Long-Term Fund’s 30-day SEC yield down to 2.33% at the end of the period, from 3.50% a year earlier. (Bond prices and yields move in opposite directions.)
With Federal Reserve policy pinning short-term interest rates near zero, Vanguard Ohio Tax-Exempt Money Market Fund, which invests in securities maturing in less than one year, returned 0.01%, compared with the average of 0.00% for peer funds. Its 7-day SEC yield began and finished the fiscal year at 0.01%.
2
Please note: Although the funds are permitted to invest in securities whose income is subject to the alternative minimum tax (AMT), throughout the year the Long-Term Fund owned no securities that would generate income distributions subject to the AMT. The Money Market Fund did hold such securities during the year and on November 30.
In case you hadn’t heard about them, I wanted to mention that the Securities and Exchange Commission (SEC) adopted a number of regulatory changes governing money market funds earlier this year. With these changes, and the significant safeguards it adopted in 2010, the SEC has issued a strong response to those who believe institutional money market funds pose a risk to the financial system.
The vast majority of investors in Vanguard money market funds will not be affected by the new rules. A brief overview of the new rules can be found in the box on page 6.
 
3
International bonds (as measured by the Barclays Global Aggregate Index ex USD) returned –2.53% for U.S. investors, as many foreign currencies weakened against the U.S. dollar during the fiscal year. (International bonds produced a positive return for U.S. investors after currency hedging, which helps mitigate the effects of movements in foreign exchange rates.)
Here, too, weakness in foreign currencies weighed on international stocks, which returned about 1% in dollar terms. Emerging markets fared better than the developed markets of Europe and the Pacific region.
4
Middle East, and muni yields that were attractive on their own and relative to those of Treasuries and corporate bonds.
Those factors, along with the positive credit trend for state-issued debt, contributed to a resurgence in demand for Ohio munis in 2014. Among those showing interest were the usual clients for these securities—residents of the Buckeye State in high income tax brackets looking for tax-exempt income—as well as nontraditional buyers, including banks, insurance companies, and hedge funds.
The Long-Term Fund was well positioned for this environment thanks to the skill of its advisor, Vanguard Fixed Income Group. A strategic overweight to pockets of lower-quality investment-grade bonds and longer maturities relative to the fund’s benchmark proved an advantage.
Another positive was the fund’s holdings in longer-term premium callable bonds, which tend to have higher yields relative to their duration risk.
Security selection also contributed to the fund’s relative performance, as did avoiding Puerto Rico’s beleaguered
5
muni bonds. Despite their high coupons and the allure of their exemption from federal, state, and local taxes, these securities lagged because of the economic and fiscal challenges facing the commonwealth.
Although this past fiscal year was a very strong one for munis, keep in mind that, given how low yields have fallen, it’s unlikely that munis—especially those with longer-maturities—will produce similar results next year.
For more information about the advisor’s approach and the funds’ positioning during the year, please see the Advisor’s Report that follows this letter.
 
6
process—has helped us sidestep potential problems and identify opportunities among muni bonds. That process is ongoing, even for the bonds already held in our portfolios.
The skill of our credit analysts, along with the competitive advantage of our low costs, has been instrumental in enabling the Money Market Fund and the Long-Term Fund to outpace the average annual return of their peer groups over the last decade by 23 and 103 basis points, respectively. (A basis point is one-hundredth of a percentage point.) The Long-Term Fund outperformed its benchmark index, which, unlike the fund or its peers, has no expenses.
If you expect too much from the markets, you might not save sufficiently. You might also take on excessive risk in your portfolio in the pursuit of unrealistically high returns
We firmly believe that a better course is to follow Vanguard’s principles for investing success:
The beauty of these principles is that, unlike market returns, each one is within your control, and focusing on them can put you on the right path.
As always, thank you for investing with Vanguard.
Sincerely,


