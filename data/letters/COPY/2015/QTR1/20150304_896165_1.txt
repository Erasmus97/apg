DEAR FELLOW SHAREHOLDER:
 
®
 
Since its formation in 1960, the Organization of Petroleum Exporting
Countries (OPEC) has operated with the primary goal of providing fair and stable prices for petroleum producers. Following the approximately 50% drop in the price of oil from June 2014 through December 2014, it is safe to say that the
cartel has not been successful in achieving that objective. The combination of OPEC maintaining production levels (led by the Saudis), increased oil supply (primarily from North American shale and Libya), subpar demand growth, and a stronger U.S.
dollar have all contributed to the precipitous price decline.
 
As gas prices plummet,
the clear beneficiaries of the energy sectors pain are consumers and businesses globally. In the U.S., it is entirely possible that we could see prices at the pump below $2 this year, representing the largest gas price decline ever during an
expansion; the other examples of this are the mid/late 1980s and mid/late 1990s. Nowhere is cheap energy more welcomed than in middle and lower income households. To this group, any price decline represents an immediate and commensurate increase in
discretionary cash availability and, given it is likely to be spent, provides strong support for overall consumer spending. We believe that over the next several months, as lower fuel costs are accepted to be more permanent in nature,
and are incorporated into household budgets, consumer-oriented economic data should continue to strengthen. Furthermore, with the U.S. jobs picture continuing to improve, particularly among small businesses, Main Street is broadly
positioned for a solid 2015.
 
For the year ahead we see the continuation of several
global, macro dynamics that have emerged over the past six months, namely, continued strength of the U.S. economy and the U.S. dollar coupled with ongoing international sluggishness. The global energy sector, currently in a state of flux, will
likely spend much of 2015 in rebalancing mode following the recent breathtaking plunge in the price of oil. While the speed and severity of the decline has unsettled financial markets, we believe that cheap energy will ultimately provide a major
shot of adrenaline for growth during the upcoming year, benefitting both consumers and businesses. Overall, this is a positive development for the U.S., which is still a net importer of oil despite its recently expanded energy production profile.
While the domestic expansion continues to become self-sustaining, monetary policy will increasingly be in focus as the Federal Reserve looks to raise interest rates, possibly within the next six months. Years after cutting short term rates to zero
in late 2008, the financial impact of this policy change is a significant wild card for 2015.
 
Sincerely yours,


