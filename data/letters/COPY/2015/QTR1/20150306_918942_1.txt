Dear Fellow Shareholders: 
We report the following performance information for the LKCM Funds: 
 
Funds
LKCM Equity Fund -
Institutional Class
®
LKCM Small Cap Equity Fund -
Institutional Class
®
LKCM Small Cap Equity Fund -
Adviser Class
®
LKCM Small-Mid Cap Equity Fund -
Institutional Class
®
LKCM Balanced Fund
®
Barclays U.S. Intermediate
LKCM Fixed Income Fund
Barclays U.S. Intermediate
Performance data quoted represents past performance and does not guarantee future results. The investment return and
principal value of an investment will fluctuate so that an investors shares, when redeemed, may be worth more or less than the original cost. Current performance of the Fund may be lower or higher than the performance quoted. Performance data
current to the most recent month end may be obtained by calling 1-800-688-LKCM. The Funds impose a 1.00% redemption fee on shares held less than 30 days. If reflected, the fee would reduce performance shown. 
 
Note: These
indices defined above are not available for direct investment and the index performance therefore does not include expenses. 
2014 Review 
Investors were faced with several data series in 2014 that shaped the trajectory of the financial markets, including declining bond yields, declining crude oil
prices, and global economic growth rates. The year started with weakness domestically due to the unusually cold weather across the United States. Real Gross Domestic Product (GDP) in the United States contracted approximately 2.1% in the first
quarter of 2014, the first quarterly contraction in three years. As weather and business activity improved in the second quarter of 2014, economic growth rebounded with real GDP expanding approximately 4.6%, followed by approximately 5.0% and 2.6%
growth in the third and fourth quarters of 2014, respectively. 
Bond yields fell both in the United States and abroad throughout 2014, finishing the year
near annual lows in most cases. The operative phrase from the Federal Open Market Committee (FOMC) has shifted from interest rates staying low for a considerable time to the path of interest rates being data dependent, which
appears to provide the Federal Reserve with necessary room to maneuver while 
 
2 

reassuring the marketplace that it will not act prematurely. We believe the phase-out of quantitative easing by the Federal Reserve in October 2014 went much more smoothly than many anticipated,
as suggested by the activity in interest rates during the year. 

reassuring the marketplace that it will not act prematurely. We believe the phase-out of quantitative easing by the Federal Reserve in October 2014 went much more smoothly than many anticipated,
as suggested by the activity in interest rates during the year. 
We believe easily the biggest story in the financial markets during the year was the steep
decline in crude oil prices. The approximate 46% decline in the price of a barrel of West Texas Intermediate crude oil from $98.42 a barrel at the beginning of the year to $53.27 a barrel at year-end negatively affected the equity values of energy
service and production companies. The volatility in crude oil prices is reminiscent of 2008 when oil began the year trading for $96.00 a barrel, briefly reaching $146.30 a barrel, before finishing the year at $44.60 a barrel. The abrupt decline in
oil prices in 2008 proved transitory as oil prices recovered to $80.00 a barrel the following year. That collapse appeared to be driven in large part by a demand shock as the market grappled with the ensuing global economic slowdown in 2009. In
contrast, we believe the current decline in oil prices is mainly a result of excess global supply, predominantly driven by the success of domestic shale oil production, although slowing economic growth in China and Europe appear to be contributing
factors as well. However, we believe the decision by the Organization of Petroleum Exporting Countries (OPEC) on Thanksgiving Day not to curtail production sent oil tumbling $7.54 a barrel the following day with a clear signal that OPEC will not
promote a short term solution for propping up crude oil prices. 
2015 Outlook 
We believe much of the current economic environment is akin to the second half of the 1990s. At that time, as today, the U.S. economy was growing faster than
its global counterparts, resulting in divergent central bank monetary policy in which the United States was tightening monetary policy while other Group of Five economies, such as Germany and Japan, were either sustaining or tightening monetary
policy. As a result, the dollar appreciated in the late 1990s and contributed in part to the decline in crude oil prices in 1997-1998. In addition, Japan entered a deflationary spiral in the late 1990s from which it is still attempting to escape, as
deflationary forces in Japan resulted in excess Japanese savings spilling into the global financial system, dragging down bond yields in other developed economies, including the United States where yields on the 10-Year Treasury Bond fell from 7.85%
in early 1995 to a low of 4.28% in 1998. At that time, the Federal Reserve had to reverse course and become more dovish in response to financial contagion in the rest of the world, which began with the Asian currency crisis in July 1997, despite
strong United States economic output. Today we believe we have a similar situation with the collapse in commodity prices exerting pressure on emerging market economies, which in our view are attempting to unwind excesses created from a cyclical boom
in commodity prices. In addition to emerging market economies risk, we believe there are deflationary fears in Europe, the result of which could well be a continued flight to quality in U.S. bonds and further flattening of the yield curve similar to
the late 1990s. 
We believe low European bond yields in conjunction with the high likelihood of European Central Bank (ECB) quantitative easing have
exerted downward pressure on U.S. Treasury bond yields by creating demand for U.S. debt securities due to their relative attractiveness. The strength of the U.S. dollar relative to the Euro appears to only exacerbate this pressure. If oil stays near
current levels, we believe it will keep a lid on headline inflation around the world and further frustrate the ECB and Bank of Japan which are battling deflationary fears. If the Federal Reserve were to tighten monetary policy by raising the federal
funds rate, we believe the U.S. dollar would likely rise, reinforcing downward pressure on oil and U.S. Treasury bond yields. 
There have been two major
rallies in the U.S. dollar since the mid-1970s, both lasting roughly seven years. The dollar appreciated approximately 52% in the early 1980s and approximately 34% in the late 1990s. The U.S. dollar has risen approximately 13% from its lows during
2014, and we believe the divergence in global growth and prospective monetary policy are likely to further boost the value of the U.S. dollar. We believe this will present a risk that the U.S. dollar overshoots and undercuts the profit expectations
for U.S.-based multinational companies. We also believe the Eurozone is flirting with deflation and structural challenges in fiscal policy within the European Union also appear to remain unaddressed. These fiscal and monetary challenges further
support our view that the U.S. dollar appears to be in a period of cyclical strength relative to other major currencies. 
If indeed the recent drop in
crude oil prices is more supply-driven than demand-drive, the logical conclusion would seemingly be that the path of future crude oil prices may more closely resemble current levels than a $100 a barrel level. Saudi Arabia has long played the role
of the swing producer within OPEC to crimp supply and buoy the price of crude oil. Thus, when Saudi Arabia has historically been faced with the choice of defending the price of crude oil or defending its market share, it has historically
chosen to defend the price of crude oil and risk forfeiting its market share. Although OPEC retains the right to shift its strategy at any point, we do believe that market clearing forces will eventually work and lower crude oil prices will beget a
reduction in unprofitable production and discourage capital investment in energy companies and projects. The dramatic decline in oil prices effectively behaves economically like a tax cut for the consumer, and we estimate that each penny
decline in gas prices translates into approximately $1 billion of annual incremental spending power for consumers. 
®
 
3 
LKCM Equity Fund 
LKCM Equity Fund 
®
LKCM Small Cap Equity Fund 
®
LKCM
Small-Mid Cap Equity Fund 
®
LKCM Fixed Income Fund 
The LKCM Fixed Income Fund
advanced 1.72% for the year ended December 31, 2014 as compared to the 3.13% return for the Funds benchmark, the Barclays Intermediate Government/Credit Bond Index. The Funds defensive duration posture during the year, which
benefited the Funds relative performance during the prior two years, was the primary reason for the Funds relative underperformance, as interest rates rose in the short-to-intermediate part of the yield curve and declined significantly
on the long-end of the yield curve. After steepening dramatically during 2013 as a result of the Federal Reserves communiques regarding the winding down of quantitative easing, the yield curve flattened throughout 2014 as quantitative easing
was withdrawn and global growth slowed. The Funds overweight position in investment grade corporate bonds, and more specifically the BBB-rated sector, benefited the Funds relative performance, as BBB-rated corporate bonds generally
outperformed the higher-rated sectors, including U.S. bonds. During a year when interest rates declined sharply on longer-dated fixed income securities, the Funds 2.9 year duration, versus the 3.8 year duration for the benchmark, while
reducing the Funds volatility, ultimately detracted from the Funds relative performance as longer duration fixed income securities outperformed their shorter duration counterparts. 
LKCM Balanced Fund 
®
 
4 

environment. We believe the Fund is well-positioned to meet the challenges and opportunities we expect the financial markets will present during the upcoming year. 

environment. We believe the Fund is well-positioned to meet the challenges and opportunities we expect the financial markets will present during the upcoming year. 
 


Dear Fellow Shareholders: 
We report the following performance information for the LKCM Aquinas Funds: 
 
Funds
LKCM Aquinas Value Fund
LKCM Aquinas Growth Fund
LKCM Aquinas Small Cap Fund
Performance data quoted represents past performance and does not guarantee future results. The
investment return and principal value of an investment will fluctuate so that an investors shares, when redeemed, may be worth more or less than the original cost. Current performance of the Fund may be lower or higher than the performance
quoted. Performance data current to the most recent month end may be obtained by calling 1-800-423-6369. The Funds impose a 1.00% redemption fee on shares held less than 30 days. If reflected, the fee would reduce performance shown. 
 
Note: These indices defined above are not
available for direct investment and the index performance therefore does not include expenses. 
2014 Review

Investors were faced with several data series in 2014 that shaped the trajectory of the financial markets, including
declining bond yields, declining crude oil prices, and global economic growth rates. The year started with weakness domestically due to the unusually cold weather across the United States. Real Gross Domestic Product (GDP) in the United States
contracted approximately 2.1% in the first quarter of 2014, the first quarterly contraction in three years. As weather and business activity improved in the second quarter of 2014, economic growth rebounded with real GDP expanding approximately
4.6%, followed by approximately 5.0% and 2.6% growth in the third and fourth quarters of 2014, respectively. 
Bond yields fell
both in the United States and abroad throughout 2014, finishing the year near annual lows in most cases. The operative phrase from the Federal Open Market Committee (FOMC) has shifted from interest rates staying low for a considerable
time to the path of interest rates being data dependent, which appears to provide the Federal Reserve with necessary room to maneuver while reassuring the marketplace that it will not act prematurely. We believe the phase-out of
quantitative easing by the Federal Reserve in October 2014 went much more smoothly than many anticipated, as suggested by the activity in interest rates during the year. 
We believe easily the biggest story in the financial markets during the year was the steep decline in crude oil prices. The approximate
46% decline in the price of a barrel of West Texas Intermediate crude oil from $98.42 a barrel at the beginning of the year to $53.27 a barrel at year-end negatively affected the equity values of energy service and production companies. The
volatility in crude oil prices is reminiscent of 2008 when oil began the year trading for $96.00 a barrel, briefly reaching $146.30 a barrel, before finishing the year at $44.60 a barrel. The abrupt decline in oil prices in 2008 proved transitory as
oil prices recovered to $80.00 a barrel the following year. That collapse appeared to be driven in large part by a demand shock as the market grappled with the ensuing global economic slowdown in 2009. In contrast, we believe the current decline in
oil prices is mainly a result of excess global supply, predominantly driven by the success of domestic shale oil production, although slowing economic growth in China and Europe appear to be contributing factors as well. However, we believe the
decision by the Organization of Petroleum Exporting Countries (OPEC) on Thanksgiving Day not to curtail production sent oil tumbling $7.54 a barrel the following day with a clear signal that OPEC will not promote a short term solution for propping
up crude oil prices. 
 
2

2015 Outlook 
2015 Outlook 
We believe much of the current economic environment is akin to the second half of the 1990s. At that time, as today, the U.S. economy was
growing faster than its global counterparts, resulting in divergent central bank monetary policy in which the United States was tightening monetary policy while other Group of Five economies, such as Germany and Japan, were either sustaining or
tightening monetary policy. As a result, the dollar appreciated in the late 1990s and contributed in part to the decline in crude oil prices in 1997-1998. In addition, Japan entered a deflationary spiral in the late 1990s from which it is still
attempting to escape, as deflationary forces in Japan resulted in excess Japanese savings spilling into the global financial system, dragging down bond yields in other developed economies, including the United States where yields on the 10-Year
Treasury Bond fell from 7.85% in early 1995 to a low of 4.28% in 1998. At that time, the Federal Reserve had to reverse course and become more dovish in response to financial contagion in the rest of the world, which began with the Asian currency
crisis in July 1997, despite strong United States economic output. Today we believe we have a similar situation with the collapse in commodity prices exerting pressure on emerging market economies, which in our view are attempting to unwind excesses
created from a cyclical boom in commodity prices. In addition to emerging market economies risk, we believe there are deflationary fears in Europe, the result of which could well be a continued flight to quality in U.S. bonds and further flattening
of the yield curve similar to the late 1990s. 
We believe low European bond yields in conjunction with the high likelihood of
European Central Bank (ECB) quantitative easing have exerted downward pressure on U.S. Treasury bond yields by creating demand for U.S. debt securities due to their relative attractiveness. The strength of the U.S. dollar relative to the Euro
appears to only exacerbate this pressure. If oil stays near current levels, we believe it will keep a lid on headline inflation around the world and further frustrate the ECB and Bank of Japan which are battling deflationary fears. If the Federal
Reserve were to tighten monetary policy by raising the federal funds rate, we believe the U.S. dollar would likely rise, reinforcing downward pressure on oil and U.S. Treasury bond yields. 
There have been two major rallies in the U.S. dollar since the mid-1970s, both lasting roughly seven years. The dollar appreciated
approximately 52% in the early 1980s and approximately 34% in the late 1990s. The U.S. dollar has risen approximately 13% from its lows during 2014, and we believe the divergence in global growth and prospective monetary policy are likely to further
boost the value of the U.S. dollar. We believe this will present a risk that the U.S. dollar overshoots and undercuts the profit expectations for U.S.-based multinational companies. We also believe the Eurozone is flirting with deflation and
structural challenges in fiscal policy within the European Union also appear to remain unaddressed. These fiscal and monetary challenges further support our view that the U.S. dollar appears to be in a period of cyclical strength relative to other
major currencies. 
If indeed the recent drop in crude oil prices is more supply-driven than demand-drive, the logical
conclusion would seemingly be that the path of future crude oil prices may more closely resemble current levels than a $100 a barrel level. Saudi Arabia has long played the role of the swing producer within OPEC to crimp supply and buoy
the price of crude oil. Thus, when Saudi Arabia has historically been faced with the choice of defending the price of crude oil or defending its market share, it has historically chosen to defend the price of crude oil and risk forfeiting its market
share. Although OPEC retains the right to shift its strategy at any point, we do believe that market clearing forces will eventually work and lower crude oil prices will beget a reduction in unprofitable production and discourage capital investment
in energy companies and projects. The dramatic decline in oil prices effectively behaves economically like a tax cut for the consumer, and we estimate that each penny decline in gas prices translates into approximately $1 billion of
annual incremental spending power for consumers. 
®
LKCM Aquinas Value Fund 
®
 
3

LKCM Aquinas Small Cap Fund 
LKCM Aquinas Small Cap Fund 
®
LKCM Aquinas Growth Fund 
®


