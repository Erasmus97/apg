Dear Shareholder:



From an economic standpoint, the view seems brighter than it has been for several years. Multiple signs suggest sustainable growth, at least for the near term. Our economists at Deutsche Asset &amp; Wealth Management confirm that they expect the global economy to accelerate in led by the United States and China.



That is heartening news. Yet one cannot ignore the complexities of an increasingly interconnected global economy. Low oil prices, a stronger employment picture and consumer spending bode well for the domestic economy, at least in the short term. Yet sluggish growth abroad, falling commodity prices and the strong U.S dollar may be headwinds to global growth and American exports. And, as we have seen time and again, any number of factors can unexpectedly shift the markets and the overall outlook.



The take-away message amidst these mixed signals: Be prepared to stick to your long-term plan, with a portfolio that can help weather short-term fluctuations. When in doubt, or if your individual situation or objectives change, talk with a trusted financial professional before taking action.



For timely information about economic developments and your Deutsche fund investment, we hope you will visit us at deutschefunds.com. There you will find the views of our Chief Investment Officer and economists. It is a resource we are proud to offer to help keep you up-to-date and make informed decisions.



As always, we thank you for your continued investment and the opportunity to put our capabilities to work for you.



Best regards,
