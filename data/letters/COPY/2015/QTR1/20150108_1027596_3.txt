Dear Fellow Shareholders,



As of October the Net Asset Value (NAV) of the Fort Pitt Capital Total Return Fund (the Fund) was per share.Total return (including a per share dividend) for the fiscal year ended October was compares with a total return of for the unmanaged Wilshire Total Market Index and for the S&amp;P Index for the same period.



Fund performance for fiscal again lagged the major indexes. In a solidly bullish year in which higher-multiple health care and technology shares led the charge, and energy, telecommunications and materials lagged, our willingness to hold generally larger businesses with a value bent held us back. The few growth-oriented names we own performed well, including the two best performing stocks in our portfolio. Allergan Inc., a maker of drugs and medical devices for both cosmetic and therapeutic uses, saw its shares more than double after the company received a buyout offer from Valeant Pharmaceuticals International, Inc. SanDisk Corporation, a leading manufacturer and marketer of flash memory, rode both the smart-phone and cloud computing waves to a nearly gain for the year. On the downside, our under-weighting in the commodity, energy and materials sectors wasnt enough to keep the bear market in these segments at bay. Our holdings in BP P.L.C., Joy Global, Inc. and Loews Corporation all saw negative total returns for the year. Finally, longstanding (and large) positions in Verizon Communications, Inc. and AT&amp;T, Inc. basically earned their dividends during the year and no more, also hindering overall returns relative to the indexes.



Annualized total return for the three years ended October was compared to for the Wilshire Total Market Index and for the S&amp;P Index.Over the five year period ended October the Funds annualized total return was while the Wilshire Total Market Indexs annualized return was and the S&amp;P Indexs annualized return was the ten year period ended October the Funds annualized total return was while the Wilshire Total Market Indexs annualized return was and the S&amp;P Indexs annualized return was inception on December the Fund has produced a total return of annualized cumulative), compared to annualized cumulative) for the Wilshire Total Market Index and annualized cumulative) for the S&amp;P Index.The annual gross operating expense ratio for the Fund is



Performance data quoted represents past performance and does not guarantee future results.The investment return and principal value of an investment will fluctuate so that an investors shares, when redeemed, may be worth more or less than their original cost.Current performance of the Fund may be lower or higher than the performance quoted.Performance data current to the most recent month end may be obtained by calling Fund imposes a redemption fee on shares held for days or less.Performance data quoted does not reflect the redemption fee.If reflected, total returns would be reduced.Performance figures reflect fee waivers in effect.In the absence of waivers, total returns would be lower.



With the bull market now well past its birthday, and the S&amp;P Index and the Wilshire Total Market Index sporting total returns approaching annualized, the tone of U.S. equity markets has begun to resemble the new era period just prior to the turn of the century. At that time, popular enthusiasm for technology and internet stocks drove valuations to all-time records. In the five years between and the S&amp;P Index returned a cumulative annualized). Equity accounts managed by Fort Pitt Capital Group, Inc. during this period trailed the S&amp;P Index by a cumulative percentage points annualized), a seemingly insurmountable disparity. Yet less than years after the early market peak, our managed accounts had recovered this difference due to our value discipline and willingness to hold cash and Treasury bonds when markets had driven prices to uneconomic levels.



This time around, the catalyst has been less new era and more financial engineering think Federal Reserve money printing, aggressive corporate share buybacks, cost cutting and debt refinancing but the end result for investors has been only somewhat less bountiful. In the five years ended October the S&amp;P Index returned a cumulative annualized). Over this period our Fund doubled in























Fort Pitt Capital Total Return Fund







value (assuming reinvestment of dividends), but still trailed the index by a cumulative annualized). Certainly a less daunting disparity than years ago, but still meaningful in an era of near-zero interest rates and generally lower stock market returns.



We cant predict the same sort of relative performance recovery over the next few years, particularly if outsized market returns continue, but our approach to evaluating individual businesses and purchasing them at reasonable prices has not changed. Neither has our willingness to hold cash when we are having difficulty finding well run companies at attractive prices. Both attributes have served us well in the past, allowing us to outperform the major indexes from the inception of the Fund, and to do so with levels of share price volatility far lower. As mentioned above, our portfolio holds a mix of both growth and value-oriented investments, with a bias towards value as measured by both absolute and relative ratios of price to book, price to cash flow, price to sales and price to earnings. These characteristics are judged less relevant when the bulls are running. They should be relevant again; we just dont know when.



After a brief hiccup in the first calendar quarter of the U.S. economy remains the Big Engine That Could. U.S. economic growth accelerated in the third quarter, to an annualized rate of With the dollar appreciating and oil prices falling, U.S. consumers have finally reaped the benefits of six years of household deleveraging. Only three times in the past years has the dollar risen and oil prices dropped as much as this year: in and when the world was entering recession, and in during the Asian financial crisis. The latter episode preceded a boom period for U.S. consumers. Contrary to what we hear from most Federal Reserve economists, low and falling inflation is exactly what the consumer ordered. Weve been arguing since that escape velocity (defined as sustained growth above to was within reach for the U.S. economy, if only business capital spending would revive. Corporate executives countered that they needed to see consumer spending accelerate before they could commit to new projects. Consumption is inversely related to headline inflation, and lower inflation will likely boost Americans purchasing power by about annualized in coming quarters. If consumers open their wallets, corporations might finally follow suit.



Thank you for your continued support of our Fund.








Charlie Smith

Portfolio Manager




Mutual fund investing involves risk; principal loss is possible. Investments in debt securities typically decrease in value when interest rates rise. This risk is usually greater for longer-term debt securities. Small and medium capitalization companies tend to have limited liquidity and greater price volatility than large capitalization companies. The Fund invests in foreign securities which involve greater volatility and political, economic and currency risks and differences in accounting methods.



The opinions expressed are those of Charles A. Smith through the end of the period for this report, are subject to change, and are not intended to be a forecast of future events, a guarantee of future results, nor investment advice.



The S&amp;P Index is a broad based unmanaged index of stocks, which is widely recognized as representative of the equity market in general. The Wilshire Total Market Index is a capitalization weighted index of all U.S. headquartered companies which provides the broadest measure of U.S. stock market performance. It is not possible to invest directly in an index.



This information is intended for the shareholders of the Fund and is not for distribution to prospective investors unless preceded or accompanied by a current prospectus.



Fund holdings and sector allocations are subject to change and should not be considered a recommendation to buy or sell any security.



For a complete list of Fund holdings, please refer to the Schedule of Investment Section of this report.



Price to book = current share price/most recent quarterly book value per share.



Price to cash flow = current share price/trailing cash flow per share.



Price to sales = current share price/trailing sales per share.



Price to earnings = current share price/trailing earnings per share.



The Fort Pitt Capital Total Return Fund is distributed by Quasar Distributors, LLC.























Fort Pitt Capital Total Return Fund







Growth of a Hypothetical Investment at October

vs.

Wilshire Total Market Index &amp; S&amp;P Index














Average Annual Total






Since







Inception




One Year


Five Year


Ten Year






Fort Pitt Capital Total Return Fund















Wilshire Total Market Index















S&amp;P Index


















Performance data quoted represents past performance and does not guarantee future results. The investment return and principal value of an investment will fluctuate, so that an investor's shares, when redeemed, may be worth more or less than their original cost.Current performance of the Fund may be lower or higher than the performance quoted. Performance data current to the most recent month end may be obtained by calling The Fund imposes a redemption fee on shares held for days or less.



Returns reflect reinvestment of dividends and capital gains distributions.Fee waivers are in effect.In the absence of fee waivers, returns would be reduced.The performance data and graph do not reflect the deduction of taxes that a shareholder may pay on dividends, capital gains distributions, or redemption of Fund shares. Indices do not incur expenses and are not available for investment.If it did, total returns would be reduced.



Average Annual Total Return represents the average change in account value over the periods indicated.



The Wilshire Total Market Index is an unmanaged index commonly used to measure performance of over U.S. stocks.



The S&amp;P Index is an unmanaged, capitalization-weighted index representing the aggregate market value of the common equity of stocks primarily traded on the NYSE.























Fort Pitt Capital Total Return Fund










ALLOCATION OF PORTFOLIO INVESTMENTS



at October (Unaudited)
