Dear Shareholder:
was a great year for the economy and the stock market. GDP grew at an annual rate of in the third quarter and almost three million new jobs were created last year, which works out to an average
of about a month. This drove the unemployment rate down to from at the beginning of the year. The stock
market went up as measured by the S&amp;P Index. Although it was a good year for stocks, it was not a good year for stock-pickers. According to Bloomberg, only of the actively-managed equity mutual funds available in the United
States were able to beat the S&amp;P According to
Lipper, the average diversified U.S. stock mutual fund gained only in By comparison, the Parnassus Funds had a
great year with four of our six equity funds beating their benchmarks. In other words, of our funds beat their respective indices, while only of the other funds accomplished that feat. Although the Parnassus Mid Cap Fund did not beat its
index, it did beat its Lipper peer group, so of our funds were ahead of mutual funds with similar strategies. Our
best-performing fund was the Parnassus Endeavor Fund (formerly the Parnassus Workplace Fund); it was up for the year. This made it the fourth-best performer of the multi-cap core funds followed by Lipper, and it was the third-best
performer of multi-cap core funds since the Fund was established on (For the three- and five-year periods respectively, the Fund was of funds and of funds.) Second was the Parnassus Fund, which gained
and a close third was the Parnassus Core Equity Fund-Investor Shares, which gained If you own shares in any of these three funds, you should be very happy.
The fourth fund of note was the Parnassus Asia Fund, which returned for the year. This may not sound like a lot, when you compare it to the earned by the S&amp;P but when you compare it
to the return of the MSCI AC Asia Pacific Index, it sounds very good. While the composite Asian markets were basically flat for the year in dollar terms, the Parnassus Asia Fund was up Enclosed you will find the annual reports for all
our funds. We think youll find they will make very interesting reading. Company News
The dynamic duo of Todd Ahlsten and Ben Allen, who manage our Parnassus Core Equity Fund, have just been named as one of the five
finalists for Morningstars Domestic-Stock Fund Manager of the Year. We congratulate them for this well-deserved recognition.
Our senior research analyst Robert Klaber has just been named to the Forbes magazine list of under rising stars in finance. We offer our congratulations to old Robby.
Parnassus Survey
In the last three reports, I mentioned that I knew all of our shareholders by name years ago, when I started the Parnassus Fund. In
those days, we had only a few million dollars in assets, so communication was easy. Although theres no way I can know each of you by name now, I would like to know more about you and why you chose the Parnassus Funds. You can help me to gain a
better understanding of our shareholders by completing a survey that should take you no more than five


*

According to Bloomberg, as of December of actively managed U.S. equity mutual funds had
one-year annual returns that exceeded the one-year annual return of the S&amp;P Index.





Table of Contents















Annual



PARNASSUSFUNDS

minutes. Your answers will be completely anonymous and your privacy will be respected. To participate in the survey, please go to www.parnassus.com/survey.
I thank all of you for investing with us.
Yours truly,
