DEAR SHAREHOLDER: 
The fiscal year ended October 31, 2014 was another period of strong equity returns. The S&amp;P 500 Index, a widely followed barometer of the U.S. equity
market, rose more than 17% during the past 12 months. International stocks, by comparison, had flat performance, as measured by the MSCI ACWI ex USA Index (in U.S. Dollar terms). The first quarter of 2014 marked the five-year anniversary of the
equity bull market. Despite a few bouts of volatility and persistent doubts about the strength of the economic recovery since the beginning of the bull market, the S&amp;P 500 Index has a cumulative gain of 236% (including reinvestment of dividends)
since the market bottom on March 9, 2009, through October 31, 2014. 
Meanwhile, the Barclays U.S. Aggregate Bond Index, a broad U.S. bond market
benchmark, returned 4% for the 12 months ended October 31. Bond markets have performed strongly during this period, to the surprise of many, despite the unwinding of the U.S. Federal Reserves bond-buying program known as QE3. 
Earlier this year, Managers Investment Group rebranded as AMG Funds. Our new name helps align our fund family more closely with our parent company, Affiliated
Managers Group (AMG). While the names of funds branded under AMG changed slightly, the ticker symbols remain the same. There was no change to the legal or ownership structure of the funds and the name change will have no impact on their
management. 
Our foremost goal at AMG Funds is to provide investment products and solutions that help our shareholders and clients successfully reach
their long-term investment goals. We do this by partnering with many of AMGs Affiliate investment boutiques to offer a distinctive array of actively managed, return-oriented funds. In addition, we oversee and distribute a number of
complementary mutual funds sub-advised by unaffiliated investment managers. We thank you for your continued confidence and investment in AMG Funds. You can rest assured that, under all market conditions, our team is focused on delivering excellent
investment management services for your benefit. 
Respectfully, 


DEAR SHAREHOLDER: 
The fiscal year ended October 31, 2014 was another period of strong equity returns. The S&amp;P 500 Index, a widely followed barometer of the U.S. equity
market, rose more than 17% during the past 12 months. International stocks, by comparison, had flat performance, as measured by the MSCI ACWI ex USA Index (in U.S. Dollar terms). The first quarter of 2014 marked the five-year anniversary of the
equity bull market. Despite a few bouts of volatility and persistent doubts about the strength of the economic recovery since the beginning of the bull market, the S&amp;P 500 Index has a cumulative gain of 236% (including reinvestment of dividends)
since the market bottom on March 9, 2009, through October 31, 2014. 
Meanwhile, the Barclays U.S. Aggregate Bond Index, a broad U.S. bond market
benchmark, returned 4% for the 12 months ended October 31. Bond markets have performed strongly during this period, to the surprise of many, despite the unwinding of the U.S. Federal Reserves bond-buying program known as QE3. 
Earlier this year, Managers Investment Group rebranded as AMG Funds. Our new name helps align our fund family more closely with our parent company, Affiliated
Managers Group (AMG). While the names of funds branded under AMG changed slightly, the ticker symbols remain the same. There was no change to the legal or ownership structure of the funds and the name change will have no impact on their
management. 
Our foremost goal at AMG Funds is to provide investment products and solutions that help our shareholders and clients successfully reach
their long-term investment goals. We do this by partnering with many of AMGs Affiliate investment boutiques to offer a distinctive array of actively managed, return-oriented funds. In addition, we oversee and distribute a number of
complementary mutual funds sub-advised by unaffiliated investment managers. We thank you for your continued confidence and investment in AMG Funds. You can rest assured that, under all market conditions, our team is focused on delivering excellent
investment management services for your benefit. 
Respectfully, 


DEAR SHAREHOLDER: 
The fiscal year ended October 31, 2014 was another period of strong equity returns. The S&amp;P 500 Index, a widely followed barometer of the U.S. equity
market, rose more than 17% during the past 12 months. International stocks, by comparison, had flat performance, as measured by the MSCI ACWI ex USA Index (in U.S. Dollar terms). The first quarter of 2014 marked the five-year anniversary of the
equity bull market. Despite a few bouts of volatility and persistent doubts about the strength of the economic recovery since the beginning of the bull market, the S&amp;P 500 Index has a cumulative gain of 236% (including reinvestment of dividends)
since the market bottom on March 9, 2009, through October 31, 2014. 
Meanwhile, the Barclays U.S. Aggregate Bond Index, a broad U.S. bond market
benchmark, returned 4% for the 12 months ended October 31. Bond markets have performed strongly during this period, to the surprise of many, despite the unwinding of the U.S. Federal Reserves bond-buying program known as QE3. 
Earlier this year, Managers Investment Group rebranded as AMG Funds. Our new name helps align our fund family more closely with our parent company, Affiliated
Managers Group (AMG). While the names of funds branded under AMG changed slightly, the ticker symbols remain the same. There was no change to the legal or ownership structure of the funds and the name change will have no impact on their
management. 
Our foremost goal at AMG Funds is to provide investment products and solutions that help our shareholders and clients successfully reach
their long-term investment goals. We do this by partnering with many of AMGs Affiliate investment boutiques to offer a distinctive array of actively managed, return-oriented funds. In addition, we oversee and distribute a number of
complementary mutual funds sub-advised by unaffiliated investment managers. We thank you for your continued confidence and investment in AMG Funds. You can rest assured that, under all market conditions, our team is focused on delivering excellent
investment management services for your benefit. 
Respectfully, 


