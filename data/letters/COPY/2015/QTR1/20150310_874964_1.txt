Dear Shareholder,
 
In 2014, global equity markets were supported by low interest
rates and declining bond yields but their overall gains were tempered by growth concerns and currency volatility. US stocks gained
steadily over the year despite the discontinuation of US quantitative easing in October. International equity markets rose slightly
in local currency terms but, for US dollar-based investors, returns were negative due to the dollar’s broad strength. Political
and economic instability in Ukraine, Russia, and Greece, along with the effects of weaker commodity prices, held back returns in
emerging markets. On the other hand, progress on structural reforms could unlock pent-up potential in countries such as India,
Indonesia, and China.
 
Globally, fixed income markets rallied as interest rates in
many countries trended lower for most of 2014. In the emerging markets, US dollar-denominated debt outperformed local currency
debt for a fourth consecutive year. Meanwhile, the decline in the price of oil led to significant underperformance in the debt
of countries that are heavily dependent on oil exports. In general, lower oil prices should be economically favorable for many
countries as it can boost consumer spending and strengthen national finances.
 
At Lazard Asset Management LLC, we remain focused on active
management and are committed to leveraging our strengths in pursuing the Portfolios’ investment objectives so that you, a
valued shareholder in Lazard Funds, achieve your financial goals. Over the years, we understand that your investment preferences
may change as markets evolve and as asset classes mature. With this in mind, we launched seven new mutual funds in 2014 across
the alternatives, equity, and currency asset classes. We believe these funds may offer our clients’ new avenues of return.
As always, we appreciate your continued confidence in our investment management capabilities, and feel privileged that you have
turned to Lazard for your investment needs.
 
Sincerely,


Dear Shareholder,
 
In 2014, global equity markets were supported by low interest
rates and declining bond yields but their overall gains were tempered by growth concerns and currency volatility. US stocks gained
steadily over the year despite the discontinuation of US quantitative easing in October. International equity markets rose slightly
in local currency terms but, for US dollar-based investors, returns were negative due to the dollar’s broad strength. Political
and economic instability in Ukraine, Russia, and Greece, along with the effects of weaker commodity prices, held back returns
in emerging markets. On the other hand, progress on structural reforms could unlock pent-up potential in countries such as India,
Indonesia, and China.
 
Globally, fixed income markets rallied as interest rates in
many countries trended lower for most of 2014. In the emerging markets, US dollar-denominated debt outperformed local currency
debt for a fourth consecutive year. Meanwhile, the decline in the price of oil led to significant underperformance in the debt
of countries that are heavily dependent on oil exports. In general, lower oil prices should be economically favorable for many
countries as it can boost consumer spending and strengthen national finances.
 
At Lazard Asset Management LLC, we remain focused on active
management and are committed to leveraging our strengths in pursuing the Portfolios’ investment objectives so that you,
a valued shareholder in Lazard Funds, achieve your financial goals. Over the years, we understand that your investment preferences
may change as markets evolve and as asset classes mature. With this in mind, we launched seven new mutual funds in 2014 across
the alternatives, equity, and currency asset classes. We believe these funds may offer our clients’ new avenues of return.
As always, we appreciate your continued confidence in our investment management capabilities, and feel privileged that you have
turned to Lazard for your investment needs.
 
Sincerely,


Dear Shareholder,
 
In 2014, global equity markets were supported by low interest
rates and declining bond yields but their overall gains were tempered by growth concerns and currency volatility. US stocks gained
steadily over the year despite the discontinuation of US quantitative easing in October. International equity markets rose slightly
in local currency terms but, for US dollar-based investors, returns were negative due to the dollar’s broad strength. Political
and economic instability in Ukraine, Russia, and Greece, along with the effects of weaker commodity prices, held back returns in
emerging markets. On the other hand, progress on structural reforms could unlock pent-up potential in countries such as India,
Indonesia, and China.
 
Globally, fixed income markets rallied as interest rates in many
countries trended lower for most of 2014. In the emerging markets, US dollar-denominated debt outperformed local currency debt
for a fourth consecutive year. Meanwhile, the decline in the price of oil led to significant underperformance in the debt of countries
that are heavily dependent on oil exports. In general, lower oil prices should be economically favorable for many countries as
it can boost consumer spending and strengthen national finances.
 
At Lazard Asset Management LLC, we remain focused on active management
and are committed to leveraging our strengths in pursuing the Portfolios’ investment objectives so that you, a valued shareholder
in Lazard Funds, achieve your financial goals. Over the years, we understand that your investment preferences may change as markets
evolve and as asset classes mature. With this in mind, we launched seven new mutual funds in 2014 across the alternatives, equity,
and currency asset classes. We believe these funds may offer our clients’ new avenues of return. As always, we appreciate
your continued confidence in our investment management capabilities, and feel privileged that you have turned to Lazard for your
investment needs.
 
Sincerely,


Dear Shareholder,
 
In 2014, global equity markets were supported by low interest
rates and declining bond yields but their overall gains were tempered by growth concerns and currency volatility. US stocks gained
steadily over the year despite the discontinuation of US quantitative easing in October. International equity markets rose slightly
in local currency terms but, for US dollar-based investors, returns were negative due to the dollar’s broad strength. Political
and economic instability in Ukraine, Russia, and Greece, along with the effects of weaker commodity prices, held back returns in
emerging markets. On the other hand, progress on structural reforms could unlock pent-up potential in countries such as India,
Indonesia, and China.
 
Globally, fixed income markets rallied as interest rates in
many countries trended lower for most of 2014. In the emerging markets, US dollar-denominated debt outperformed local currency
debt for a fourth consecutive year. Meanwhile, the decline in the price of oil led to significant underperformance in the debt
of countries that are heavily dependent on oil exports. In general, lower oil prices should be economically favorable for many
countries as it can boost consumer spending and strengthen national finances.
 
At Lazard Asset Management LLC, we remain focused on active
management and are committed to leveraging our strengths in pursuing the Portfolio’s investment objectives so that you, a
valued shareholder in Lazard Funds, achieve your financial goals. Over the years, we understand that your investment preferences
may change as markets evolve and as asset classes mature. With this in mind, we launched seven new mutual funds in 2014 across
the alternatives, equity, and currency asset classes. We believe these funds may offer our clients’ new avenues of return.
As always, we appreciate your continued confidence in our investment management capabilities, and feel privileged that you have
turned to Lazard for your investment needs.
 
Sincerely,


Dear Shareholder,
 
In 2014, global equity markets were supported by low interest rates and declining bond yields but their overall gains were tempered
by growth concerns and currency volatility. US stocks gained steadily over the year despite the discontinuation of US
quantitative easing in October. International equity markets rose slightly in local currency terms but, for US dollar-based investors,
returns were negative due to the dollar’s broad strength. Political and economic instability in Ukraine, Russia, and
Greece, along with the effects of weaker commodity prices, held back returns in emerging markets. On the other hand, progress
on structural reforms could unlock pent-up potential in countries such as India, Indonesia, and China.
 
Globally, fixed income markets rallied as interest rates in many countries trended lower for most of 2014. In the emerging markets,
US dollar-denominated debt outperformed local currency debt for a fourth consecutive year. Meanwhile, the decline in the price
of oil led to significant underperformance in the debt of countries that are heavily dependent on oil exports. In general, lower oil
prices should be economically favorable for many countries as it can boost consumer spending and strengthen national finances.
 
At Lazard Asset Management LLC, we remain focused on active management and are committed to leveraging our strengths in
pursuing the Portfolios’ investment objectives so that you, a valued shareholder in Lazard Funds, achieve your financial goals.
Over the years, we understand that your investment preferences may change as markets evolve and as asset classes mature. With
this in mind, we launched seven new mutual funds in 2014 across the alternatives, equity, and currency asset classes. We believe
these funds may offer our clients’ new avenues of return. As always, we appreciate your continued confidence in our investment
management capabilities, and feel privileged that you have turned to Lazard for your investment needs.
 
Sincerely,


Dear Shareholder,
 
In 2014, global equity markets were supported by low interest
rates and declining bond yields but their overall gains were tempered by growth concerns and currency volatility. US stocks gained
steadily over the year despite the discontinuation of US quantitative easing in October. International equity markets rose slightly
in local currency terms but, for US dollar-based investors, returns were negative due to the dollar’s broad strength. Political
and economic instability in Ukraine, Russia, and Greece, along with the effects of weaker commodity prices, held back returns
in emerging markets. On the other hand, progress on structural reforms could unlock pent-up potential in countries such as India,
Indonesia, and China.
 
Globally, fixed income markets rallied as interest rates in
many countries trended lower for most of 2014. In the emerging markets, US dollar-denominated debt outperformed local currency
debt for a fourth consecutive year. Meanwhile, the decline in the price of oil led to significant underperformance in the debt
of countries that are heavily dependent on oil exports. In general, lower oil prices should be economically favorable for many
countries as it can boost consumer spending and strengthen national finances.
 
At Lazard Asset Management LLC, we remain focused on active
management and are committed to leveraging our strengths in pursuing the Portfolios’ investment objectives so that you,
a valued shareholder in Lazard Funds, achieve your financial goals. Over the years, we understand that your investment preferences
may change as markets evolve and as asset classes mature. With this in mind, we launched seven new mutual funds in 2014 across
the alternatives, equity, and currency asset classes. We believe these funds may offer our clients’ new avenues of return.
As always, we appreciate your continued confidence in our investment management capabilities, and feel privileged that you have
turned to Lazard for your investment needs.
 
Sincerely,


