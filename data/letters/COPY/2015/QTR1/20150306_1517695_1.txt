Dear Shareholders,



The Wall Street Fund returned for the one year period ending December and has had average annual total returns of for the past five years and for the past ten years. For the respective time periods, the S&amp;P Index was up and and The Russell Index was up and As we enter the S&amp;P is trading slightly above forward earnings, a level that represents the high end of fair value from our perspective.



For the year Western Digital, NXP Semiconductor and Allergan were the largest positive contributors to performance.Each of the positions increased over during the year with Allergan up over Detractors from performance included Las Vegas Sands, Marathon Oil and Amazon.Amazon was sold early in the year.



After a very strong year for equities in our expectations for were more muted.We expected a positive year but, one with little price-earnings margin expansion and growth in line with earnings growth.It turned out quite a bit better than that as earnings multiples expanded again last year propelling the market higher.With multiples now at the very high end of our expected fair value range, we think it is possible that over the next year or two multiples could compress.This will weigh on the market and make significant appreciation from current levels over the short-term unlikely.That said, multiples are not so high that we are predicting a market decline and we are still finding interesting opportunities.



The Wall Street Fund ended with forty-three equity holdings.The top five positions represented of fund assets. Total assets for the fund at year-end were million.



Sincerely,
