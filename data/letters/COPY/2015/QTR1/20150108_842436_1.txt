Dear Shareholder:
 
“Interest Rates: How Low, For How Long?”
 
Alpine believes that over the next year, investors will be rewarded
if they focus on long-term trends. This strategy has worked well for the extended period since the Great Financial Collapse of
2008. Despite news of distressing events, traumas, coups and even wars which have had short lived impacts, the positive long-term
trend of capital markets around the world has been sustained by low interest rates and modest economic growth. Another factor has
been the U.S. dollar, which reversed a nine and one-half year decline in mid-2011, with notable appreciation from August of 2014.
The principle factor underpinning these trends has been Central Bank monetary policy, led by the U.S. Federal Reserve. In October
of 2014, the Fed concluded its quantitative easing program while other countries have adopted similar supportive policies. On top
of these measures, the price of oil and most raw material commodities have seen notable declines, lowering many production costs
and enhancing potential profitability.
 
One can argue that Central Bank policy has been the primary force
for economic recovery and growth in the absence of fiscal stimulus from most governments. Indeed, many believe that fiscal austerity
around the world, most notably in Europe, has limited opportunities for job creation and reduced economic growth. Increasingly,
the world’s central banks are adopting tools to increase money supply, but instead of boosting bank lending, much of the
liquidity has flowed into capital markets. The Federal Reserve has completed its tapering of quantitative easing by phasing out
the purchase of financial securities, making the U.S. the leader in normalizing this cycle. It should be noted that the Fed has
accumulated a $4 trillion balance sheet which will not dramatically decline over the near term, so stimulus will not yet be reduced.
Add to this expanded Japanese quantitative easing and the prospect of some form of European quantitative easing next year should
more than make up for our Fed’s reduced level of monetary support. After the close of the fiscal year in October, the financial
markets received an early Thanksgiving gift from China
via a reduction in China’s mortgage loan rates and a likely
follow up of decreased reserve requirements for some of its banks, which should push increased liquidity into the capital markets.
However, since China is not as open an economy (unlike other major nations), the flow of funds into the global liquidity pool will
be more gradual. Nonetheless, the trend is clear, “do not fear, your friendly central banker will be here”.
 
Europe faces a prospect of continued GDP growth of less than 1%
with inflation at similar low levels. This suggests that the possibility of a long-term (i.e., secular) period of minimal growth
or possibly even stagnation could occur. Thus, the current Eurozone unemployment rate of 11.5% (Spain and Greece are more than
two times that number) may not improve materially for some time. The International Monetary Fund (IMF) has suggested that countries
should adopt more stimulative fiscal policies, such as significant infrastructure spending, but we think that is not enough. Indeed,
outgoing European Council President Von Rompuy stated that “without jobs and growth, the European idea itself is in danger”.
Clearly the political consequences of stagnant or even deflationary economies can create social upheavals, as Europe itself has
witnessed over the past several hundred years. Just two years ago, the so called “Jasmine Revolution” spread political
upheaval throughout the Arab world. This was largely the product of high unemployment rates and disenfranchised populations.
 
Japan is currently the only major country utilizing both monetary
and fiscal stimulus. Indeed, their so called ‘three arrows’ approach of aggressive monetary policy, expansionary fiscal
stimulus combined with structural reforms to the economy are key products of the political situation in Japan, brought on by over
20 years of substandard economic performance. In addition to Japan, there are a few emerging market economies, most notably India,
which are in a position to reduce interest rates and expand domestic demand as well as utilize appropriate fiscal spending and
structural reforms to spur growth. However, most countries are taking the politically more expedient path of devaluing their currency
in order to make exports cheaper and, hence, more competitive. Such currency devaluation can create 
1
 
 
inflation over time and cause long term problems if growth in
output and wages does not increase materially.
 
The one country that is not in this position, of course, is the
U.S.A. Indeed, the U.S. currency has been ascending over the past year, the U.S. banking sector is in a better position than those
of most other countries, and larger companies are generally well capitalized by both the equity and debt markets. However, small
companies in this country are not fully enjoying the benefits of the modest economic recovery we have enjoyed over the past few
years. This is holding back the U.S. recovery, sustaining only moderate job growth over time. Thus, median real wages are in fact
lower today than they were in 2007. Nonetheless, the prospects for small U.S. businesses are improving. However, Alpine believes
productivity enhancements created by investments in new technology, production capabilities, and communication and physical infrastructure
are still required for economic growth to accelerate.
 
In addition to the push for greater global liquidity, the other
major driver of economic prospects and, hence, the markets, has been the continued expansion of U.S. oil and gas reserves. This
has helped to bring about lower oil and gas prices at a time when global economic activity and demand for energy is slowing. Fundamentally,
cheaper energy means the cost of economic activity has not only declined, but the transfer of economic wealth from goods producers
and transporters to energy producers has also shifted. Countries whose economies are dependent on high oil prices, notably Russia
and Iran, and to a lesser degree, Brazil, Canada, Mexico and Norway, may be hurt by an extended decline. On the other hand, Japan
and much of Europe could be big beneficiaries. The resultant increase in many industries’ profitability due to lower fuel
prices provides the possibility that some of the enhanced margins might be distributed to workers and some of the savings may also
be distributed to consumers in the form of price stability. In other words, a major inflationary input has been limited, and this
may stimulate economic activity. Alpine believes there will be minimal inflationary impulses
globally over the next year or two until global aggregate demand
starts to rise.
 
For 2015, Alpine believes that the prospects for modest economic
growth supported by abundant cheap global liquidity, combined with lower energy costs, will continue to favor capital markets and
global equities more broadly. We still expect significant regional differences in terms of growth, and individual companies may
see their prospects and share prices rise or decline based on management’s ability to utilize the capital markets during
this period. This suggests continued expansion of mergers and acquisitions activities on a global basis. It could also lead to
an increased number of IPOs in different industries, seeking both to capitalize on high historic valuations as well as position
themselves to utilize capital markets for future growth.
 
Just as the U.S. stock market outperformed much of the world during
2014 as a result of the combination of cheap money and improving economic fundamentals, we believe that 2015 will see a global
broadening of market performance to include small cap stocks in the U.S. as well as increased international opportunities. Finally,
we should note that the extended period of low volatility, including 2013 and much of 2014, may not be fully over, but political
and economic risks remain, as few of this year’s conflicts have resolved and more may surface. The market appears to be increasingly
open to more risk if returns are commensurate. Fundamentally, the Fed is still our friend, even though many market participants
have been waiting over a year for the proverbial ‘punch bowl’ to be removed and the party to end. Markets may well
continue to climb a “wall of worry” as we enter 2015.
 
We appreciate your interest and support as we enter what appears
to be a seventh year of economic and equity market recoveries.
 
Sincerely,


Dear Shareholder:
 
®

 
As we noted in the President’s letter,
broadly speaking, US equities outperformed international or non-US equities this year. We believe this reflects America’s
faster return to normalized economic levels than much of the rest of the world. Related to this, the US Dollar has been strengthening
over the past year, particularly in the final three months of the fiscal year. It’s been a period characterized by significant
currency volatility, with the Japanese Yen, Swedish Krona, Brazilian Real, the Euro and the Australian Dollar all weakening relative
to the US Dollar by 7% to 12%. The Indian Rupee gained a bit over 1% and was one of the few currencies in the portfolio to strengthen
in the period.
 
The Fund hedged a portion of its Yen exposure
for most of its fiscal year. The currency hedging mitigated the overall negative impact of currency in the portfolio. We have also
used leverage both in the execution of the strategy of the Fund and to manage inflows and outflows during the fiscal year.
 
The portfolio continues to be focused on
both growth situations and undervalued opportunities. This is reflected in both the geographic distribution of the portfolio, as
well as individual stock holdings. Increasingly, this reflects the widening divergence among countries experiencing improving economic
conditions, countries that appear to be bottoming and should
benefit from aggressive monetary and/or
fiscal stimulus, and those countries that are still suffering from severe structural and/or fiscal deficits leading to weak demand
and/or high unemployment. In the first category, the U.S. and U.K. stand out. In the second category, China and India are promising,
as is Japan. Europe is a bit more questionable and varied, although Ireland looks very promising and Spain has potential. In the
third category, several emerging market countries, notably across commodity driven economies in Latin America and Asia, may be
in for a rocky ride, although their valuations may support potential upside appreciation.
 
Portfolio Composition
 
Reflecting both economic and equity market
conditions, as well as real estate fundamentals, we have made some notable shifts in the portfolio over the past year. The largest
change occurred in the increased weighting of Asian holdings, from 43.4% to 50.7% of the portfolio. Europe increased from 33.2%
to 38.9%, and North and South America fell from 23.9% to 15% of the portfolio. While the Fund’s largest country exposure
is to the United Kingdom, which rose from 22.1% at the end of last year to a relatively constant 25.9% for much of the year, India
has seen an increase from 12.4% to 18.5%. In the opposite direction, Brazil has dropped from 21.3% to 11% of holdings. Japan’s
position was reduced from 11.5% at previous fiscal year end to just over 8% at mid-year and closed out this fiscal year at 9.8%.
Other smaller, but notable changes, are a significant increase in our exposure to China from 1.7% to 6.5% at fiscal year-end. This
contrasts with Thailand where we reduced positions from 6.4% to 2.8%. Finally, it’s worth noting that last year the Fund
had no exposure to the United Arab Emirates, while at mid-year it had 1% and concluded the year at 1.8%. The Fund also took advantage
of new opportunities to invest in Ireland, where, we see a promising dynamic for property recovery so we initiated positions by
mid-year at 0.7%, leading up to 1.9% at the end of the year.
9
Dear Shareholders:
 
®

 
As we noted in the President’s letter,
broadly speaking, US equities generally outperformed international or non-US equities this year. We believe this reflects America’s
faster return to normalized economic levels than much of the rest of the world. Related to this, the US Dollar has been strengthening
over the past year, particularly in the final three months of the fiscal year. It’s been a period characterized by significant
currency volatility, with the Japanese Yen, Swedish Krona, Brazilian Real, the Euro and the Australian Dollar all weakening relative
to the US Dollar by 7% to 12%. The Indian Rupee gained a bit over 1% and was one of the few currencies in the portfolio to strengthen
in the period. The Fund hedged a portion of its Yen exposure for most of its fiscal year. The currency hedging mitigated the overall
negative impact of currency in the portfolio. We have also used leverage both in the execution of the strategy of the Fund and
to manage inflows and outflows during the fiscal year.
 
In addition to currencies, it is noteworthy
that the dramatic disparity in performance of real estate equity returns favored U.S. REITs. For the twelve months ended October
31, 2014, the MSCI U.S. REIT Total Return Index gained 19.19%. This outperformed all global and country real estate indices with
the exception of the United Arab Emirates and Sweden. As we will discuss in the following section on portfolio composition, the
Fund was underweight U.S. equities. The portfolio continues to be focused on both growth situations and undervalued opportunities.
This is reflected in both the geographic distribution of the portfolio, as well as individual stock holdings. Increasingly, this
reflects the widening divergence among countries experiencing improving economic conditions, countries that appear to be bottoming
and should benefit from aggressive monetary and/or fiscal stimulus, and those countries that are still suffering from severe structural
and/or fiscal deficits leading to weak demand and/or high unemployment. In the first category, the U.S. and U.K. stand out. In
the second category, China and India are promising, as is Japan. Europe is a bit more questionable and varied, although Ireland
looks very
promising and Spain has potential. In the
third category, several emerging market countries, notably across commodity driven economies in Latin America and Asia, may be
in for a rocky ride, although their valuations may support potential upside appreciation.
 
Portfolio
Positioning
 
In comparing fiscal year end 2013 with fiscal
year end 2014, the major change in composition of the Fund is the decline in North and South American holdings from 50.1% to 40%
of the portfolio. Asian exposure went from 33.8% to 37.9% and European exposure was increased somewhat from 19.6% to just about
26% of the portfolio. The main impact on performance, however, was a decline in U.S. exposure from 35.4% to 28.6% of the portfolio.
Brazil was also reduced from 11.7% to 7.6%, while Mexico remained stable for the balance. Other significant holdings are the U.K.,
which rose from 8.8% to 13.6%, and Japan which declined from 15.6% to 12.9% for the fiscal year end. Notable changes also occurred
in China/Hong Kong, which had a combined weight of 1.9% last year and we increased to 8% by the end of this fiscal year. In similar
fashion, India grew from 1.7% to 4.8% of the portfolio. Notably, other countries were added, specifically, the United Arab Emirates
which rose from no representation to 2.2% this year, Spain which grew from 0% to 1.4% and Ireland where we grew from 0% to 2% of
the portfolio.
 
Top
Ten Holdings
 
LXB Retail Property Trust
22
Dear Investor
 
TM
 
Performance Drivers
 
® 
 
® 
 
The Fund outperformed in the transportation
sector due to its exposure to the railway sector both in North America and in China. The North American railroads have outperformed
due to industry trends such as the shale revolution, improving domestic output, and a shortage of truck drivers. Certain companies
within this sector have made operational improvements. For example, Hunter Harrison, the CEO of Canadian Pacific, improved the
efficiency of the company’s rail operations.
 
The Fund has also participated in a number
of Initial Public Offerings (“IPO”) and Secondary Offerings, both inside and outside of the infrastructure sector,
that have contributed to the Fund’s total return. We cannot predict
how long, if at all, these opportunities
will continue to exist, but to the extent we consider IPO’s to be attractively priced and available, the Fund may continue
to participate in them.
 
A significant portion of the Fund’s
holdings were held outside the U.S. and denominated in foreign currencies. As a result of a strengthening dollar, these currencies
had a negative impact on the Fund’s performance.
 
Emerging Market Exposure
 
® 
 
Portfolio Analysis
 
The top five stocks contributing to the Fund’s
performance for the 12-month period ended October 31, 2014, based on contribution to total return were Adani Ports &amp; Special
Economic Zone (“ADSEZ”), Abengoa (“ABG”), The Williams Companies (“WMB”), Union Pacific Corporation
(“UNP”), and Canadian Pacific Railway (“CP”).
 
Adani Ports &amp; Special Economic Zone is
the largest private port operator in India. The election of new Prime Minister, Narendra Modi, improved business sentiment in India
and raised expectations for policy reform. ADSEZ’s port business experienced strong volume growth, with its largest port
growing 15% from calendar year 2013 to calendar year 2014. The acquisition of Dhamra Port was also positive given its strategic
value.
 
Abengoa is a global integrated engineering &amp; construction
and concessions company based in Spain. ABG benefited from the successful initial public offering of Abengoa Yield (“ABY”)
in the United States. ABY is a dividend-oriented company with a portfolio of long-term
35
