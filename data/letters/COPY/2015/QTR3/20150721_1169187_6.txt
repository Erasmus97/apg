Dear Investor
The Institutional High Yield Fund
outperformed its benchmark, but results for the 12-month period were restrained
by plunging oil prices and its negative impact on the energy industrythe
largest segment of the high yield market. The U.S. economy is strengthening,
which has supported the domestic high yield market, and European economic growth
appears to be improving thanks in part to the European Central Banks aggressive
quantitative easing program, which benefited European high yield bonds.

Portfolio Performance 
The Institutional High Yield Fund posted
modest gains in our annual reporting period ended May 31, 2015. Your fund
outperformed its benchmark, the J.P. Morgan Global High Yield Index, and the
Lipper peer group average of similarly managed funds. The funds performance
versus the J.P. Morgan index benefited from security selection and holdings in
European high yield bonds. Additionally, our allocation to bank debt aided
performance over the past 12 months, a particularly volatile period. However,
our underweight to bonds rated BB and higher hurt our comparison with the
benchmark, as higher-quality bonds outperformed when interest rates declined.

