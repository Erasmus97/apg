Dear Shareholder:
 
We are pleased to provide you with the
Touchstone Strategic Trust Annual Report. Inside you will find key financial information, as well as manager commentaries, for
the 12 months ended June 30, 2015.
 
During the fiscal
year, the global markets struggled to maintain an upward trend as investors were unsettled by the decline in oil prices, economic
slowing in China and the uncertainty surrounding Greece’s status in the European Union. Oil prices declined sharply during
the second half of 2014 with both supply and demand factors playing a part in the decline. Given that China is the second largest
economy in the world and has been a significant global growth driver, signs of slower economic growth had investors worried about
the broader implications of this slowdown on other major economies. Lastly, the first half of 2015 was punctuated by the debt negotiations
between Greece and its creditors which created additional uncertainty for the markets.
 
U.S. equity markets
moved higher during the 12-month period. The U.S. market environment generally favored larger capitalization stocks and companies
with stronger growth characteristics. Economic weakness abroad and a rapidly appreciating U.S. dollar led to negative returns for
developed international and emerging market equities.
 
Shorter term interest
rates rose as the market began to assume a greater likelihood that the U.S. Federal Reserve Board would make its first foray to
raising interest rates during the second half of 2015. Bond returns, as measured by the Barclays U.S. Aggregate Index, were flat
as coupon income offset a slight price decline from higher interest rates. High yield bonds outperformed other sectors due to their
relatively higher coupon income.
 
We believe that focusing
on the long-term composition of your investment portfolio is essential to balancing risk and return. We recommend that you work
with your financial professional on a regular basis to assess and adjust your asset allocation and diversification strategy as
needed to help keep your financial goals on track.
 
We greatly appreciate your continued support.
Thank you for including Touchstone as part of your investment plan.
 
Sincerely,


