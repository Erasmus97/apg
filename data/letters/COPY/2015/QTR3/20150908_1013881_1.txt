Dear Shareholders, 
For better
or for worse, the financial markets have spent the past year waiting for the U.S. Federal Reserve (Fed) to end its ultra-loose monetary policy. The policy has propped up stock and bond markets since the Great Recession, but the question remains: how
will markets behave without its influence? This uncertainty has been a considerable source of volatility for stock and bond prices lately, despite the Fed carefully conveying its intention to raise rates slowly and only when the economy shows
evidence of readiness. 
A large consensus expects at least one rate hike before the end of 2015. After all, the U.S. has reached full employment by the
Feds standards and growth has resumed  albeit unevenly. But the picture is somewhat muddled. Inflation has remained stubbornly low, most recently weighed down by an unexpectedly sharp decline in commodity prices since mid-2014. With the
Fed poised to tighten and foreign central banks easing, the U.S. dollar has surged against other currencies, which has weighed on corporate earnings and further contributed to commodity price weakness. U.S. consumers have benefited from an improved
labor market and lower prices at the gas pump, but the overall pace of economic expansion has been lackluster. 
Nevertheless, the global recovery continues to be led
by the U.S. Policy makers around the world are deploying their available tools to try to bolster Europe and Japans fragile growth, and manage Chinas slowdown. Contagion fears ebb and flow with the headlines about Greece and China. Greece
reluctantly agreed to a third bailout package from the European Union in July and Chinas central bank and government intervened aggressively to try to stem the sell-off in stock prices. But persistent structural problems in these economies
will continue to garner market attention. 
Wall Street is fond of saying markets dont like uncertainty, and asset prices are likely to continue to
churn in the current macro environment. In times like these, you can look to a professional investment manager with the experience and discipline to maintain the proper perspective on short-term events. And if the daily headlines do concern you, I
encourage you to reach out to your financial advisor. Your financial advisor can help you evaluate your investment strategies in light of current events, your time horizon and risk tolerance. On behalf of the other members of the Nuveen Fund Board,
we look forward to continuing to earn your trust in the months and years ahead. 
Sincerely, 


Dear Shareholders, 
For better
or for worse, the financial markets have spent the past year waiting for the U.S. Federal Reserve (Fed) to end its ultra-loose monetary policy. The policy has propped up stock and bond markets since the Great Recession, but the question remains: how
will markets behave without its influence? This uncertainty has been a considerable source of volatility for stock and bond prices lately, despite the Fed carefully conveying its intention to raise rates slowly and only when the economy shows
evidence of readiness. 
A large consensus expects at least one rate hike before the end of 2015. After all, the U.S. has reached full employment by the
Feds standards and growth has resumed  albeit unevenly. But the picture is somewhat muddled. Inflation has remained stubbornly low, most recently weighed down by an unexpectedly sharp decline in commodity prices since mid-2014. With the
Fed poised to tighten and foreign central banks easing, the U.S. dollar has surged against other currencies, which has weighed on corporate earnings and further contributed to commodity price weakness. U.S. consumers have benefited from an improved
labor market and lower prices at the gas pump, but the overall pace of economic expansion has been lackluster. 
Nevertheless, the global recovery continues to be led
by the U.S. Policy makers around the world are deploying their available tools to try to bolster Europe and Japans fragile growth, and manage Chinas slowdown. Contagion fears ebb and flow with the headlines about Greece and China. Greece
reluctantly agreed to a third bailout package from the European Union in July and Chinas central bank and government intervened aggressively to try to stem the sell-off in stock prices. But persistent structural problems in these economies
will continue to garner market attention. 
Wall Street is fond of saying markets dont like uncertainty, and asset prices are likely to continue to
churn in the current macro environment. In times like these, you can look to a professional investment manager with the experience and discipline to maintain the proper perspective on short-term events. And if the daily headlines do concern you, I
encourage you to reach out to your financial advisor. Your financial advisor can help you evaluate your investment strategies in light of current events, your time horizon and risk tolerance. On behalf of the other members of the Nuveen Fund Board,
we look forward to continuing to earn your trust in the months and years ahead. 
Sincerely, 


