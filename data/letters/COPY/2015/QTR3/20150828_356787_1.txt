Dear Fellow Shareholders:

    Global equity markets overcame some significant challenges and generally
climbed higher over the past twelve months. While growth in the U.S. and other major developed markets remains on track, emerging markets have become a more significant source of risk to global growth. 
    Despite another weak first quarter in the U.S., with only modest growth in Gross Domestic Product (GDP), most current
indicators point to a rebound in the second quarter of 2015. Specifically, consumer spending (which accounts for over two-thirds of GDP) appears to have moved higher from a soft first quarter, and nonfarm payrolls continue to grow at a healthy
200,000+ monthly clip. Job growth has been relatively resilient in light of the weak trends in energy, mining and manufacturing, which have been negatively impacted by weak commodity prices and the stronger U.S. dollar. Auto sales continue to be
quite strong and, encouragingly, housing-related data have been particularly robust following a relatively tepid 2014. In summary, we continue to believe that the U.S. economy is growing at a moderate two to three percent pace. 
    U.S. inflation, as measured by the headline Consumer Price Index (CPI), remains well-controlled and in a tight range.
The key factors suppressing inflation are still in place, and we do not anticipate a change in trend. Commodity prices continue to be quite low; the stronger dollar is dampening import prices; and there is little evidence that wage inflation is
accelerating in a meaningful way. To be sure, all of these factors must be closely monitored, given the potential impact that an uptick in inflation will have on Federal Reserve policy. We believe that the Federal Reserve is likely to raise interest
rates later this year or early next year, but a traditional tightening cycle (i.e., with multiple successive rate increases) is not likely, given moderate economic growth and a lack of inflationary pressure. 
    Outside the U.S., many European economies have improved in recent months due to the combined impact of a weaker
currency, lower energy prices, and an accommodative monetary policy. Even beleaguered economies, such as Spain, have rebounded, despite very high unemployment. For the euro area overall, we have raised our economic projections based on improving
employment trends, modest wage growth, increases in consumer spending, and some signs that bank lending is picking up. 
    While we remain cautious on the long-term prospects for Japan given structural headwinds (i.e. elevated government
debt, aging population), the near-term picture looks relatively stable, as both consumption and investment spending are rising. Exports have helped, as well, due to the weak currency. However, exports to China represent a key risk to the country and
for the global economy overall. Growth has been steadily decelerating and the recent significant stock market correction is an incremental cause for concern. Policymakers, how-

ever, have taken dramatic measures to stabilize the market and we expect continued easing of
monetary policies, which should help stabilize growth. This bears watching, as growth is imperative given Chinas elevated debt levels and the major impact an interruption in growth could have on the rest of the world, particularly other
emerging markets. 
Equity Strategy 
    While we believe equity prices can grind higher in the months ahead, numerous cross-currents are likely to result in
increasing volatility as we proceed through the remainder of 2015. 
®
    All of
this points to a challenging investment environment, but we clearly see opportunities. We continue to position our equity portfolios in diversified, high quality companies that are able to generate growth in a sluggish economic environment. In
addition, we favor companies with strong balance sheets and cash flow generation and management teams that consistently return capital back to shareholders in the form of share repurchases and dividends. In terms of sectors, we have become more
cautious on the energy and producer manufacturing sectors, given their dependence on emerging markets and vulnerability to a stronger U.S. dollar. We have incrementally increased exposure to the consumer discretionary sector, as solid labor markets,
somewhat higher wages and falling energy costs provide a sound backdrop for U.S. consumers. Across all portfolios, healthcare remains a key area of focus for a number of reasons. First, the combination of steady employment gains and the Affordable
Care Act has increased the insured population, leading to higher medical utilization in many areas. Second, we believe the pace of innovation and pipeline productivity has increased in recent years, given broad-based product introductions across
therapeutic and device manufacturers. Third, the sector is being driven by the aging population all over the world. 
    In international and global portfolios, we remain overweight Europe, with an emphasis on exporting companies benefiting
from the weak euro, and have added some exposure to 
 
 
2

 
 
 
companies benefiting from improvements in consumer spending within the region. While the recent Greek debt negotiations have weighed on
near-term performance, we believe that sentiment will shift more positively as the European Central Bank remains very accommodative, mainly through its quantitative easing program, for quite some time. While Japanese stocks have performed well in
recent months, we are underweight the region given our longer term concerns over structural challenges facing the country. We continue to have significant exposure to China based on our long-term view that growth there is still quite strong relative
to the rest of the world. Regardless of the economic region, we remain focused on well-managed, high quality growth companies. 
With best wishes, 
 


