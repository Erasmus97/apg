Dear fellow shareholder,
Despite improving economic conditions in many developed countries and
continued central bank stimulus, global market volatility recently
crept up. European markets were shaken by the ongoing debt crisis in
Greece, including that country's default on debt payments and
subsequent vote to reject the terms of a bailout package from
European creditors.
In the bond market, yields rose from recent lows as the U.S. labor
market showed some signs of strength. The low levels of returns
currently offered by most bond markets mean there is little cushion
against falling prices, and, as a result, many fixed-income investors
have experienced negative total returns for the first time since
2013. A stronger domestic economy has increased the chances that the
U.S. Federal Reserve will raise short-term interest rates.
While the economic picture in many developed countries is
improvingand many central banks continue to apply stimuluswe may be
in for a period of rising market volatility as investors adjust to
the idea of a more normalized monetary policy in the United States.
Unpleasant as they are, these pullbacks can ultimately be beneficial
to the long-term health of the markets, resetting valuations and
investor expectations on a more realistic trajectory.
The near-term challenge, particularly for fixed-income investors,
will be maintaining the discipline to stick to a well-constructed
long-term financial plan in the face of short-term market dynamics.
As always, we recommend that your first course of action be a
conversation with your financial advisor. We also believe investors
can be well served by owning broadly diversified asset allocation
funds or by adding alternative strategies such as absolute return
funds to a diversified portfolio.
On behalf of everyone at John Hancock Investments, I'd like to take
this opportunity to welcome new shareholders and thank existing
shareholders for the continued trust you've placed in us.
Sincerely,


