Dear Fellow Shareholders,




For event-driven investors, was a challenging year but not for want of opportunities. A slow-growth global economy, attractive financing terms, cash-rich balance sheets, and rich acquisition currency reignited efforts to unlock shareholder value, whether through spin-offs, restructurings, refinancings, acquisitions or other corporate actions. Amidst the positive trends, macroeconomic uncertainties from falling energy prices to geopolitical uncertainties in Europe and Asia elongated many corporate event timelines and made it difficult for event-driven managers to recognize positive returns for investors in the near-term.




Yet much of the opportunity set we saw ahead of us a year ago remains intact today. Both shareholder activism and mergers and acquisitions (M&amp;A) activity are reaching record levels in Corporate management teams, increasingly turning to M&amp;A as an avenue for growth, have driven global M&amp;A volume in the first half of calendar year to trillion. This is the second-highest volume for a six-month period in history, behind only the first half of according to Dealogic. Much of this deal flow is in the large cap space, with transactions greater than billion reaching an all-time high.




More importantly, amidst the increasing deal flow, were witnessing a broadening of activity throughout sectors and market caps. While certain sectors such as health care, telecom and technology continue to lead the charge, deal flow is increasing across the board. In addition, whereas last year we saw acquisition activity concentrated amongst larger, well-established players, we are now beginning to see more acquisitions sourced from the lower end of the market cap spectrum, as well as a pickup in activity in Europe. This, finally, is the healthy environment for M&amp;A weve been waiting for. On top of it all, we are still seeing elevated deal spreads relative to what we witnessed in the period leading up to the Shire/AbbVie deal break in October That terminated transaction reverberated throughout the merger arbitrage space, causing a re-pricing of risk that remains today. With a healthy amount of deal flow in front of us and a more optimal spread environment than weve seen in years, we feel confident our merger arbitrage team will be able to focus on constructing a portfolio that avoids deals at the riskiest end of the spectrum yet still offers favorable rates of return.




On the equity special situations side, M&amp;A is delivering both pre-arbitrage situations as well as post-deal re-rating situations. Combined with a flood of shareholder activism and spin-off activity, we see ample opportunity in the equity markets. While a number of our equity special situations trades have experienced elevated levels of volatility relative to our other strategies which is to be expected we have high conviction in these names, we believe our theses are sound, and we feel patience is warranted as we wait for these situations to play out.




For our credit opportunities team, the fiscal year began with wide-open credit markets. Fears of imminent increases in interest rates failed to materialize and likely wont until September at the earliest. The severe decline in oil prices in the second half of however, caused a more significant disruption that rippled throughout the high yield markets particularly in companies associated with the energy sector. While energy failed to stage a substantial recovery, credit markets nonetheless stabilized and moved higher in a fairly orderly fashion.













Annual Report | May



















The Arbitrage Fund


Shareholder Letter (continued)






May








Following the dislocation in oil prices, we foresee a large universe of opportunities looming in the energy and commodities space. While these situations are highly dependent on the direction of commodity prices such as oil, iron ore, and coal, capital structures for many companies in these sectors have been severely weakened. At the other end of the spectrum, we anticipate the increase in M&amp;A activity will translate into more merger-related debt opportunities for us. We also expect that activist pressure on corporate managements and boards will translate into more spin-offs and asset sales, which, in turn, can lead to more refinancing and deleveraging activity from which to choose. We are particularly fond of these types of investments as their firmer catalysts and shorter durations can help produce lower correlated and less volatile returns.




While the timeline for rising interest rates continues to be extended, it is not a pipe dream eventually, rates will rise. Investors are keenly aware of what could occur to the credit markets once the Federal Reserve finally begins to ratchet rates upward, and we continue to take steps to reduce potential liquidity risks in our portfolios, particularly by focusing on the aforementioned firmer, shorter-dated catalysts; increasing the number of short credit ideas we implement; tempering the pace at which we build positions in bonds with high exchange-traded fund (ETF) ownership; maintaining higher cash levels; and increasing our opportunistic hedging programs.




Amidst these factors, our investment teams remain anchored to our discipline of capitalizing on corporate catalysts by understanding the rationale and underlying complexities of each event, sharing insights between our teams, using hedging to mitigate market influence, and building well-diversified portfolios. As always, we maintain a focus on risk management, with a keen eye toward downside protection. We believe through this approach, the Arbitrage Funds family can generate attractive long-term results with lower volatility and lower correlation to the broader equity and credit markets.




We are grateful for your continued trust and support.




Sincerely,
