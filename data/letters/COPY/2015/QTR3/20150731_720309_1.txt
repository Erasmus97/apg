DEAR SHAREHOLDER: 
Overall, U.S. equity investors were rewarded with another strong period of returns for the fiscal year ended May 31, 2015. Despite the collapse of oil prices and its
negative effects on energy stocks, the S&amp;P 500 Index, a widely followed barometer of the U.S. equity market, rose nearly 12% during the past twelve months. Nine out of ten sectors of the S&amp;P 500 were positive over the prior twelve months,
led by health care. International stocks, by comparison, fell 1%, as measured by the MSCI ACWI ex USA Index (in U.S. Dollar terms). International investment returns in U.S. Dollar terms were impacted by the strengthening U.S. Dollar. The
first quarter of 2015 also marked the six-year anniversary of the equity bull market. The first quarter of 2015 also marked 9 consecutive quarters of positive performance for the S&amp;P 500. Continuing the trend from 2014, equity funds,
particularly international funds, saw strong inflows in 2015. 
The Barclays U.S. Aggregate Bond Index, a broad U.S. bond market benchmark, returned 3.0% for the
fiscal year ended May 31, 2015. Interest rates fell throughout much of 2014, lending positive support to bond prices. However, interest rates have increased more recently, putting pressure on bond prices, driven by a combination of the
strengthening U.S. economy along with market expectation of an eventual end, in the coming months, to the Federal Reserves zero interest rate policy. 
Our
foremost goal at AMG Funds is to provide investment solutions that help our shareholders successfully reach their long-term investment goals. By partnering with AMGs affiliated investment boutiques, AMG Funds provide access to a distinctive
array of actively managed, return oriented investment strategies. Additionally, we oversee and distribute a number of complementary open-architecture mutual funds subadvised by unaffiliated investment managers. We thank you for your continued
confidence and investment in AMG Funds. You can rest assured that under all market conditions our team is focused on delivering excellent investment management services for your benefit. 
Respectfully, 


