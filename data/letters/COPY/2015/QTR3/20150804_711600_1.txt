Dear Shareholder:



Despite slow growth during the first half of the U.S. economys underlying fundamentals remain sound. Employment growth has been solid, the housing market continues to improve and households have strengthened their finances. Real income is firming thanks to the improving labor market and lower energy prices. And, while consumers remain cautious, theyre likely to loosen their purse strings over time.



In short, our economists see an environment that should support modestly above-trend domestic growth. The strong U.S. dollar continues to act as a headwind to exports and (for those whose positions are not hedged) a detractor from foreign equity returns.



The U.S. Federal Reserve Board is likely to start raising short-term interest rates in the U.S. later this year. However, the specific timing depends on whether the recent slowdown in activity reverses, the labor markets continue to heal and inflation truly bottoms. In any case, analysts expect the process to be gradual.



Meanwhile, the global picture, which had appeared to be brightening, is again in flux due to uncertainties regarding the Greek debt crisis and its potential ramifications. Overall, our strategic view remains generally positive. While we do not see Greece posing a major risk of contagion across the Eurozone, heightened volatility can be expected.



As always, we encourage you to visit us at deutschefunds.com for timely information about economic developments and your Deutsche fund investment. With frequent updates from our Chief Investment Officer and economists, we want to ensure that you have the resources you need to make informed decisions.



Thank you for your continued investment. We appreciate the opportunity to serve your investment needs.



Best regards,
