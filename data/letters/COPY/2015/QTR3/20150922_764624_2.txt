Dear Shareholder,

We are pleased to provide the annual report of Western Asset Short Duration High Income Fund for the twelve-month reporting period ended
July 31, 2015. Please read on for a detailed look at prevailing economic and market conditions during the Funds reporting period and to learn how those conditions have affected Fund performance. 
I am pleased to introduce myself as the new President and Chief Executive Officer of the Fund, succeeding Kenneth D. Fuller. I am honored to have been appointed to
my new role. During my 27 year career with Legg Mason, I have seen the investment management industry evolve and expand. Throughout these changes, maintaining an unwavering focus on our shareholders and their needs has remained paramount.

As always, we remain committed to providing you with excellent service and a full spectrum of investment choices. We also remain committed to
supplementing the support you receive from your financial advisor. One way we accomplish this is through our website, www.leggmason.com/individualinvestors. Here you can gain immediate access to market and investment information, including:

 
Fund prices and performance, 
 
Market insights and commentaries from our portfolio managers, and 
 
A host of educational resources. 
We look
forward to helping you meet your financial goals. 
Sincerely, 


