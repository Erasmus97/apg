Dear Valued Shareholder: 
Wells Fargo Advantage 
Equities continued to trend higher as economic data strengthened and central banks acted. 
1
2
In the Pacific region, the Bank of Japan maintained accommodative monetary policies
as it sought to stimulate growth in an economy that has been beset by deflationary pressures during the past two decades. Japans economy rebounded from recession in the final quarter of 2014, but growth was weaker than expected. In China,
economic growth remains positive but at lower levels than in recent years. In response, the Peoples Bank of China reduced interest rates in November 2014, February 2015, and once again after the close of the period in an effort to spark
activity that will sustain higher growth. 
 
 
 
 
Table of Contents
 
Domestic bonds outperformed foreign bonds thanks to U.S. bond yield premiums. 
4
Dont let short-term
uncertainty derail long-term investment goals. 
Wells Fargo Advantage Funds 
Wells Fargo Advantage Funds
Sincerely, 


Dear Valued Shareholder: 
Wells Fargo Advantage Asset Allocation Fund
Equities continued to trend higher as economic data strengthened and central banks acted. 
1
2
In the Pacific region, the Bank of Japan maintained accommodative monetary policies as it sought to stimulate growth in an
economy that has been beset by deflationary pressures during the past two decades. Japans economy rebounded from recession in the final quarter of 2014, but growth was weaker than expected. In China, economic growth remains positive but at
lower levels than in recent years. In response, the Peoples Bank of China reduced interest rates in November 2014, February 2015, and once again after the close of the period in an effort to spark activity that will sustain higher
growth. 
 
 
 
 
Table of Contents
 
Domestic bonds outperformed foreign bonds thanks to U.S. bond yield premiums. 
4
Dont let short-term
uncertainty derail long-term investment goals. 
Wells Fargo Advantage Funds 
Wells Fargo Advantage Funds
Sincerely, 


