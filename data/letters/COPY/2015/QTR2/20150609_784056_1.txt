Dear Fellow Shareholder:



As you may know, Aquila Management Corporation celebrated its anniversary this past year.



Back in when we were investigating whether or not it made sense to launch the first in a series of single-state tax-free municipal bond funds, we didnt have much of a road map to follow. We knew that if we went ahead, we would be creating the first fund of its kind in Hawaii (where we launched the initial Aquila single-state municipal bond fund) and one of only a few dozen such funds in the entire country. And, we knew that in order to be successful in launching such a fund, it would have to be top notch a product in which both we and the people of the state could be proud.



So, we spent a lot of time listening to potential investors and their financial advisers learning what fund features and concerns were utmost in their minds. That first fund and each of the tax-free bond funds that we launched subsequently, such as Aquila Churchill Tax-Free Fund of Kentucky, was truly designed with you, our shareholders, in mind.



As the funds have matured, so has our game plan over time, we have made a concerted effort to sharpen, clearly define and communicate just who Aquila is and what we stand for. Our long-standing philosophy has been formalized into a set of four guiding principles. These are:



Manage Conservatively most people are more sensitive to potential investment losses than they are eager for outsized gains. We seek to manage our bond funds accordingly.



Since we strive to preserve the value of shareholder assets, investment quality is critical to our investment strategy. We focus on the source and reliability of revenue and income streams, issuer management teams and the financial decisions they make, as well as the discipline those issuers apply to executing their strategic and budgeting plans. Through our security selection processes, we seek to pursue the funds objectives while attempting to limit risk.



Our investment strategy with our municipal bond funds also focuses upon intermediate maturities in order to limit volatility with any change in interest rates.



NOT A PART OF THE ANNUAL REPORT























Focus On What We Know Best our core investment skill is finding securities that we believe have a sound basis for investment. With our municipal bond funds, we do this through research conducted by locally-based municipal bond teams.



We have never tried to be all things to all people. We know that our municipal bond fund shareholder base is comprised of individuals seeking capital preservation, tax-free income and stability.



Put Customers First the money we manage belongs to those who entrust it to us. We view every interaction with investors and advisors as an opportunity to strengthen a relationship. Since day one, we have said that it is your money, invested in projects in your communities and state.



Know Whats Important we measure success by how well we meet expectations. By working to satisfy our shareholders, the success of our business will follow.



We are very proud of the fact that, in continuing to focus upon these four guiding principles, Aquila Churchill Tax-Free Fund of Kentucky, and the other single state tax-free municipal bond funds sponsored by Aquila, have, in our view, served thousands of residents and local projects well over the years.






Sincerely,

Dear Fellow Shareholder:



As you may know, Aquila Management Corporation celebrated its anniversary this past year.



Back in when we were investigating whether or not it made sense to launch the first in a series of single-state tax-free municipal bond funds, we didnt have much of a road map to follow. We knew that if we went ahead, we would be creating the first fund of its kind in Hawaii (where we launched the initial Aquila single-state municipal bond fund) and one of only a few dozen such funds in the entire country. And, we knew that in order to be successful in launching such a fund, it would have to be top notch a product in which both we and the people of the state could be proud.



So, we spent a lot of time listening to potential investors and their financial advisers learning what fund features and concerns were utmost in their minds. That first fund and each of the tax-free bond funds that we launched subsequently, such as Aquila Narragansett Tax-Free Income Fund, was truly designed with you, our shareholders, in mind.



As the funds have matured, so has our game plan over time, we have made a concerted effort to sharpen, clearly define and communicate just who Aquila is and what we stand for. Our long-standing philosophy has been formalized into a set of four guiding principles. These are:



Manage Conservatively most people are more sensitive to potential investment losses than they are eager for outsized gains. We seek to manage our bond funds accordingly.



Since we strive to preserve the value of shareholder assets, investment quality is critical to our investment strategy. We focus on the source and reliability of revenue and income streams, issuer management teams and the financial decisions they make, as well as the discipline those issuers apply to executing their strategic and budgeting plans. Through our security selection processes, we seek to pursue the funds objectives while attempting to limit risk.



Our investment strategy with our municipal bond funds also focuses upon intermediate maturities in order to limit volatility with any change in interest rates.



NOT A PART OF THE ANNUAL REPORT























Focus On What We Know Best our core investment skill is finding securities that we believe have a sound basis for investment. With our municipal bond funds, we do this through research conducted by locally-based municipal bond teams.



We have never tried to be all things to all people. We know that our municipal bond fund shareholder base is comprised of individuals seeking capital preservation, tax-free income and stability.



Put Customers First the money we manage belongs to those who entrust it to us. We view every interaction with investors and advisors as an opportunity to strengthen a relationship. Since day one, we have said that it is your money, invested in projects in your communities and state.



Know Whats Important we measure success by how well we meet expectations. By working to satisfy our shareholders, the success of our business will follow.



We are very proud of the fact that, in continuing to focus upon these four guiding principles, Aquila Narragansett Tax-Free Income Fund, and the other single state tax-free municipal bond funds sponsored by Aquila, have, in our view, served thousands of residents and local projects well over the years.



Sincerely,

Dear Fellow Shareholder:



As you may know, Aquila Management Corporation celebrated its anniversary this past year.



Back in when we were investigating whether or not it made sense to launch the first in a series of single-state tax-free municipal bond funds, we didnt have much of a road map to follow. We knew that if we went ahead, we would be creating the first fund of its kind in Hawaii (where we launched the initial Aquila single-state municipal bond fund) and one of only a few dozen such funds in the entire country. And, we knew that in order to be successful in launching such a fund, it would have to be top notch a product in which both we and the people of the state could be proud.



So, we spent a lot of time listening to potential investors and their financial advisers learning what fund features and concerns were utmost in their minds. That first fund and each of the tax-free bond funds that we launched subsequently, such as Aquila Tax-Free Fund of Colorado, was truly designed with you, our shareholders, in mind.



As the funds have matured, so has our game plan over time, we have made a concerted effort to sharpen, clearly define and communicate just who Aquila is and what we stand for. Our long-standing philosophy has been formalized into a set of four guiding principles. These are:



Manage Conservatively most people are more sensitive to potential investment losses than they are eager for outsized gains. We seek to manage our bond funds accordingly.



Since we strive to preserve the value of shareholder assets, investment quality is critical to our investment strategy. We focus on the source and reliability of revenue and income streams, issuer management teams and the financial decisions they make, as well as the discipline those issuers apply to executing their strategic and budgeting plans. Through our security selection processes, we seek to pursue the funds objectives while attempting to limit risk.



Our investment strategy with our municipal bond funds also focuses upon intermediate maturities in order to limit volatility with any change in interest rates.



Focus On What We Know Best our core investment skill is finding securities that we believe have a sound basis for investment. With our municipal bond funds, we do this through research conducted by locally-based municipal bond teams.



NOT A PART OF THE ANNUAL REPORT























We have never tried to be all things to all people. We know that our municipal bond fund shareholder base is comprised of individuals seeking capital preservation, tax-free income and stability.



Put Customers First the money we manage belongs to those who entrust it to us. We view every interaction with investors and advisors as an opportunity to strengthen a relationship. Since day one, we have said that it is your money, invested in projects in your communities and state.



Know Whats Important we measure success by how well we meet expectations. By working to satisfy our shareholders, the success of our business will follow.



We are very proud of the fact that, in continuing to focus upon these four guiding principles, Aquila Tax-Free Fund of Colorado, and the other single state tax-free municipal bond funds sponsored by Aquila, have, in our view, served thousands of residents and local projects well over the years.



Sincerely,

Dear Fellow Shareholder:



As you may know, Aquila Management Corporation celebrated its anniversary this past year.



Back in when we were investigating whether or not it made sense to launch the first in a series of single-state tax-free municipal bond funds, we didnt have much of a road map to follow. We knew that if we went ahead, we would be creating the first fund of its kind in Hawaii (where we launched the initial Aquila single-state municipal bond fund) and one of only a few dozen such funds in the entire country. And, we knew that in order to be successful in launching such a fund, it would have to be top notch a product in which both we and the people of the state could be proud.



So, we spent a lot of time listening to potential investors and their financial advisers learning what fund features and concerns were utmost in their minds. That first fund and each of the tax-free bond funds that we launched subsequently, such as Aquila Tax-Free Fund For Utah, was truly designed with you, our shareholders, in mind.



As the funds have matured, so has our game plan over time, we have made a concerted effort to sharpen, clearly define and communicate just who Aquila is and what we stand for. Our long-standing philosophy has been formalized into a set of four guiding principles. These are:



Manage Conservatively most people are more sensitive to potential investment losses than they are eager for outsized gains. We seek to manage our bond funds accordingly.



Since we strive to preserve the value of shareholder assets, investment quality is critical to our investment strategy. We focus on the source and reliability of revenue and income streams, issuer management teams and the financial decisions they make, as well as the discipline those issuers apply to executing their strategic and budgeting plans. Through our security selection processes, we seek to pursue the funds objectives while attempting to limit risk.



Our investment strategy with our municipal bond funds also focuses upon intermediate maturities in order to limit volatility with any change in interest rates.





NOT A PART OF THE ANNUAL REPORT
























Focus On What We Know Best our core investment skill is finding securities that we believe have a sound basis for investment. With our municipal bond funds, we do this through research conducted by locally-based municipal bond teams.




We have never tried to be all things to all people. We know that our municipal bond fund shareholder base is comprised of individuals seeking capital preservation, tax-free income and stability.



Put Customers First the money we manage belongs to those who entrust it to us. We view every interaction with investors and advisors as an opportunity to strengthen a relationship. Since day one, we have said that it is your money, invested in projects in your communities and state.



Know Whats Important we measure success by how well we meet expectations. By working to satisfy our shareholders, the success of our business will follow.



We are very proud of the fact that, in continuing to focus upon these four guiding principles, Aquila Tax-Free Fund For Utah, and the other single state tax-free municipal bond funds sponsored by Aquila, have, in our view, served thousands of residents and local projects well over the years.



Sincerely,

Dear Fellow Shareholder:



As you may know, Aquila Management Corporation celebrated its anniversary this past year.



Back in when we were investigating whether or not it made sense to launch the first in a series of single-state tax-free municipal bond funds, we didnt have much of a road map to follow. We knew that if we went ahead, we would be creating the first fund of its kind in Hawaii (where we launched the initial Aquila single-state municipal bond fund) and one of only a few dozen such funds in the entire country. And, we knew that in order to be successful in launching such a fund, it would have to be top notch a product in which both we and the people of the state could be proud.



So, we spent a lot of time listening to potential investors and their financial advisers learning what fund features and concerns were utmost in their minds. That first fund and each of the tax-free bond funds that we launched subsequently, such as Aquila Tax-Free Trust of Arizona, was truly designed with you, our shareholders, in mind.



As the funds have matured, so has our game plan over time, we have made a concerted effort to sharpen, clearly define and communicate just who Aquila is and what we stand for. Our long-standing philosophy has been formalized into a set of four guiding principles. These are:



Manage Conservatively most people are more sensitive to potential investment losses than they are eager for outsized gains. Weseektomanageourbondfundsaccordingly.



Since we strive to preserve the value of shareholder assets, investment quality is critical to our investment strategy. We focus on the source and reliability of revenue and income streams, issuer management teams and the financial decisions they make, as well as the discipline those issuers apply to executing their strategic and budgeting plans. Through our security selection processes, we seek to pursue the funds objectives while attempting to limit risk.



Our investment strategy with our municipal bond funds also focuses upon intermediate maturities in order to limit volatility with any change in interest rates.



Focus On What We Know Best our core investment skill is finding securities that we believe have a sound basis for investment. With our municipal bond funds, we do this through research conducted by locally-based municipal bond teams.



NOT A PART OF THE ANNUAL REPORT























We have never tried to be all things to all people. We know that our municipal bond fund shareholder base is comprised of individuals seeking capital preservation, tax-free income and stability.



Put Customers First the money we manage belongs to those who entrust it to us. We view every interaction with investors and advisors as an opportunity to strengthen a relationship. Since day one, we have said that it is your money, invested in projects in your communities and state.



Know Whats Important we measure success by how well we meet expectations. By working to satisfy our shareholders, the success of our business will follow.



We are very proud of the fact that, in continuing to focus upon these four guiding principles, Aquila Tax-Free Trust of Arizona, and the other single state tax-free municipal bond funds sponsored by Aquila, have, in our view, served thousands of residents and local projects well over the years.



Sincerely,
