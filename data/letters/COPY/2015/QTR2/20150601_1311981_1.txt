Dear Fellow Shareholders,
During the fiscal year ending March 31, 2015, the Adirondack Small Cap Fund (ADKSX) returned 2.87%.  Over the same period, its benchmark, the Russell 2000® Value Index (RUJ), increased 4.43%.  The top ten holdings represented 24.1% of fund assets and the annual portfolio turnover stood at 37%.    
 
The past twelve months were a challenge for value investors, particularly active small cap managers. Momentum stocks are presently garnering most of the attention.  For instance, the large cap Russell 1000® Growth Index advanced 16% while the small cap Russell 2000® Growth Index posted a 12% return. Both indexes significantly outperformed their respective core and value categories.  Much of the outperformance is centered in a narrow group of high flying Healthcare and Technology stocks.   This scene is vaguely reminiscent of early 2000 and thus one should heed caution when taking hot stock tips or investing in highly publicized initial public offerings.     
 
As for us, we endured the frustrating experience of underperforming our benchmark which posted relatively uninspiring returns.  When we actually dig deeper into the contributors to the RUJs outperformance, we feel a little better.  Had the Fund been similarly invested in yield plays like REITs and Utilities, it would have likely kept pace with the index.  Real Estate and Utilities, though, are industries where valuations are homogeneous and therefore its difficult, in our opinion, for active managers to add value.  We remain steady in our belief that these industries are overvalued and, as such, our continued underweight should be a source of more favorable comparisons when interest rates start to rise.  Our stock selection decisions did not pay off in certain sectors as well, especially in Energy where we were overweight in the exploration and production space that took us up and then down.  Our investments in companies benefiting from lower energy prices cushioned much of the blow but, with the benefit of hindsight, there were signs of a bubble that deserved more of our attention.  That said, we think the Energy and Materials sectors hold a significant amount of future promise for investors.         
 

1
THE ADIRONDACK SMALL CAP FUND
MANAGERS COMMENTARY (CONTINUED)
MARCH 31, 2015 (UNAUDITED)


