Dear Shareholder:



Financial Conditions During Fiscal Year Period

During the fiscal year ended March the Federal Reserve ended its Quantitative Easing (QE) asset purchase program. As of March the balance sheet of the Federal Reserve had increased to trillion (from billion The total debt issued by the U.S. Treasury reached trillion (an increase of billion over the fiscal year). The Feds balance sheet continued to support the U.S. equity markets and the U.S. Governments annual deficits are beginning to grow again.



Equity markets worldwide did not perform as well as the S&amp;P Index during the fiscal year ended March but as a group, they performed very well during the last quarter of the fiscal year. The European Central Bank joined the QE club in since the European Unions economy was not growing to central banker expectations. Over the fiscal year, there was significant price deflation in energy, agricultural commodities and precious metals. Oil was down during the fiscal year, and gold was down The U.S. Economy slowed drastically in the last days of the fiscal year, yielding a growth rate. The performance was written off to bad weather by economists.



Historical Market Performance of the Funds

During the fiscal year ended March the S&amp;P Index returned During the same period the Barrier Fund Investor Class was up the Barrier Fund Class A (without sales charge) was up and the Barrier Fund Class C (without sales charge) was down the period from April (commencement of operations) through March the Barrier Fund Institutional Class was down gaming sector significantly underperformed the market during the period, with the average performance of the top gaming holdings being down Macau gaming revenue represents of the worldwide gaming revenue and was severely impacted by Chinese Government restraints on exuberance. During the fiscal year, Galaxy Entertainment Group Ltd., the Macau based casino down was the worst performing holding. The Chinese Governments crackdown on exuberance and corruption had an effect on sales of premium spirits in Asia.During the course of the fiscal year, equity options were used successfully to generate additional income for the Barrier Fund.



The Generation Wave Growth Fund lost over the fiscal year while the S&amp;P Index increased by A comparable index that represents multi alternative strategies is the IQ Hedge Multi-Strategy Index, which was up for the same period.Short equity options were utilized in the Generation Wave Growth Fund as part of the multi alternative strategy. The exposure to fixed assets and commodities caused the differential with the comparable index. The IQ Hedge Multi-Strategy Index has a greater exposure to fixed income, an asset class that performed well, with interest rates hitting generational lows.



Outlook QE Whitewashes Profligate Government Spending

The Central Bankers of the world are pursuing the same strategy: purchase debt to stimulate lending and boost their economies. The funds used to purchase the debt are created as a book entry on their balance sheets. What has happened in the U.S. since has been an increase of in the U.S. debt outstanding while the balance sheet of the Fed quadrupled. In a normal period of time, the bond market would have reacted negatively to the excess























deficit spending and influenced Congress to address the deficit. With synthetically lower rates, it actually became cheaper to finance the deficits. A Keynesian economist would claim that the deficits are stimulating, but in normal periods of time, a rebound from recession would generate to growth, not to Our fragile economy could not sustain the unwinding of QE and does not seem to be able to reduce the annual deficit through growth. It would appear as though the Federal Reserve has painted itself into a corner. If they cannot reduce their balance sheet to impact the money supply, was QE truly successful, or did they achieve a hidden goal to monetize trillion of Debt?








Gerald Sullivan

Portfolio Manager



Past performance does not guarantee future results.



Opinions expressed are those of USA Mutuals Advisors, Inc. and are subject to change, are not guaranteed, and should not be considered a recommendation to buy or sell any security.



This report is authorized for use when preceded or accompanied by a prospectus. Read it carefully before investing or sending money.



Fund holdings and sector allocations are subject to change and are not a recommendation to buy or sell any security. For a complete list of Fund holdings, please see the Portfolio of Investments and Schedule of Options Written in this report.



Mutual fund investing involves risk; principal loss is possible. The Barrier Fund will concentrate its net assets in industries that have significant barriers to entry including the alcoholic beverages, tobacco, gaming and defense/aerospace industries. The Fund may be subject to risks affecting those industries, including the risk that the securities of companies within those industries will underperform due to adverse economic conditions, regulatory or legislative changes or increased competition affecting those industries, more than would a fund that invests in a wide variety of industries. The Fund invests in foreign securities which involve greater volatility and political, economic and currency risks and differences in accounting methods. The Fund invests in smaller companies, which involve additional risk such as limited liquidity and greater volatility.



The Generation Wave Growth Fund invests in foreign securities which involve greater volatility and political, economic and currency risks and differences in accounting methods. The Fund invests in smaller companies, which involve additional risk such as limited liquidity and greater volatility. Because the Generation Wave Growth Fund may invest in third-party investment companies, including exchange-traded funds (ETFs), open-end mutual funds and other investment companies, your cost of investing in the Fund will generally be higher than the cost of investing directly in shares of mutual funds in which it invests. By investing in the Fund, you will indirectly bear your share of any fees and expenses charged by the underlying funds, in addition to indirectly bearing the principal risks of those funds. Please refer to the prospectuses for more information about the Fund, including risks, fees and expenses. Because the Fund invests in ETFs, it is subject to additional risks that do not apply to conventional mutual funds, including the risks that the market price of an ETFs shares may trade at a discount to its net asset value (NAV), an active secondary trading market may not develop or be maintained, or trading may be halted by the exchange in which they trade, which may impact the Funds ability to sells it shares. Derivatives may involve certain costs and risks such as liquidity, interest rate, market, credit, management and the risk that a position could not be closed when most advantageous. Investing in derivatives could result in losing more than the amount invested.



The S&amp;P Index is a broad based unmanaged index of stocks, which is widely recognized as representative of the equity market in general. You cannot invest directly in an index.



The IQ Hedge Multi-Strategy Index seeks to replicate the risk-adjusted return characteristics of the collective hedge funds using various hedge fund investment styles, including long/short equity, global macro, market neutral, event-driven, fixed income arbitrage and emerging markets.You cannot invest directly in an index.



The USA Mutuals Funds are distributed by Quasar Distributors, LLC.





























EXPENSE EXAMPLE (Unaudited)
