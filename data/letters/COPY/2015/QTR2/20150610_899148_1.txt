DEAR SHAREHOLDER








ECONOMIC AND MARKET OVERVIEW








ABOUT SHAREHOLDERS FUND EXPENSES








BANKING FUND








BASIC MATERIALS FUND








BIOTECHNOLOGY FUND








CONSUMER PRODUCTS FUND








ELECTRONICS FUND








ENERGY FUND








ENERGY SERVICES FUND








FINANCIAL SERVICES FUND








HEALTH CARE FUND








INTERNET FUND








LEISURE FUND








PRECIOUS METALS FUND








REAL ESTATE FUND








RETAILING FUND








TECHNOLOGY FUND








TELECOMMUNICATIONS FUND








TRANSPORTATION FUND








UTILITIES FUND








NOTES TO FINANCIAL STATEMENTS








REPORT OF INDEPENDENT REGISTERED PUBLIC ACCOUNTING FIRM








OTHER INFORMATION








INFORMATION ON BOARD OF TRUSTEES AND OFFICERS








GUGGENHEIM INVESTMENTS PRIVACY POLICIES
















THE RYDEX FUNDS ANNUAL REPORT |











March







Dear Shareholder:



Security Investors, LLC (the Investment Adviser) is pleased to present the annual shareholder report for of our sector funds (the Funds) for the annual period ended March



The Investment Adviser is part of Guggenheim Investments, which represents the investment management businesses of Guggenheim Partners, LLC (Guggenheim), a global, diversified financial services firm.




Guggenheim Funds Distributors, LLC is the distributor of the Funds. Guggenheim Funds Distributors, LLC is affiliated with Guggenheim and the Investment Adviser.




We encourage you to read the Economic and Market Overview section of the report, which follows this letter, and then the Performance Report and Fund Profile for each Fund.




We are committed to providing innovative investment solutions and appreciate the trust you place in us.




Sincerely,

DEAR SHAREHOLDER








ECONOMIC AND MARKET OVERVIEW








A BRIEF NOTE ON THE COMPOUNDING OF RETURNS








ABOUT SHAREHOLDERS FUND EXPENSES








NOVA FUND








S&amp;P FUND








INVERSE S&amp;P STRATEGY FUND








MONTHLY REBALANCE STRATEGY FUND








INVERSE STRATEGY FUND








MID-CAP STRATEGY FUND








INVERSE MID-CAP STRATEGY FUND








RUSSELL STRATEGY FUND








RUSSELL FUND








INVERSE RUSSELL STRATEGY FUND








GOVERNMENT LONG BOND STRATEGY FUND








INVERSE GOVERNMENT LONG BOND STRATEGY FUND








HIGH YIELD STRATEGY FUND








INVERSE HIGH YIELD STRATEGY FUND








U.S. GOVERNMENT MONEY MARKET FUND








NOTES TO FINANCIAL STATEMENTS








REPORT OF INDEPENDENT REGISTERED PUBLIC ACCOUNTING FIRM








OTHER INFORMATION








INFORMATION ON BOARD OF TRUSTEES AND OFFICERS








GUGGENHEIM INVESTMENTS PRIVACY POLICIES

















THE RYDEX FUNDS ANNUAL REPORT |











March







Dear Shareholder:



Security Investors, LLC (the Investment Adviser) is pleased to present the annual shareholder report for a selection of our Funds (the Funds) for the one-year period ended March




The Investment Adviser is part of Guggenheim Investments, which represents the investment management businesses of Guggenheim Partners, LLC (Guggenheim), a global, diversified financial services firm.




Guggenheim Funds Distributors, LLC is the distributor of the Funds. Guggenheim Funds Distributors, LLC is affiliated with Guggenheim Partners, LLC, and Security Investors, LLC.




We encourage you to read the Economic and Market Overview section of the report, which follows this letter, and then the Performance Report and Fund Profile for each Fund.




We are committed to providing innovative investment solutions and appreciate the trust you place in us.




Sincerely,

DEAR SHAREHOLDER








ECONOMIC AND MARKET OVERVIEW








A BRIEF NOTE ON THE COMPOUNDING OF RETURNS








ABOUT SHAREHOLDERS FUND EXPENSES








S&amp;P PURE GROWTH FUND








S&amp;P PURE VALUE FUND








S&amp;P MIDCAP PURE GROWTH FUND








S&amp;P MIDCAP PURE VALUE FUND








S&amp;P SMALLCAP PURE GROWTH FUND








S&amp;P SMALLCAP PURE VALUE FUND








EUROPE STRATEGY FUND








JAPAN STRATEGY FUND








STRENGTHENING DOLLAR STRATEGY FUND








WEAKENING DOLLAR STRATEGY FUND








NOTES TO FINANCIAL STATEMENTS








REPORT OF INDEPENDENT REGISTERED PUBLIC ACCOUNTING FIRM








OTHER INFORMATION








INFORMATION ON BOARD OF TRUSTEES AND OFFICERS








GUGGENHEIM INVESTMENTS PRIVACY POLICIES

















THE RYDEX FUNDS ANNUAL REPORT |















March









Dear Shareholder:



Security Investors, LLC (the Investment Adviser) is pleased to present the annual shareholder report for a selection of our Funds (the Funds) for the one-year period ended March




The Investment Adviser is part of Guggenheim Investments, which represents the investment management businesses of Guggenheim Partners, LLC (Guggenheim), a global, diversified financial services firm.




Guggenheim Funds Distributors, LLC is the distributor of the Funds. Guggenheim Funds Distributors, LLC is affiliated with Guggenheim and the Investment Adviser.




We encourage you to read the Economic and Market Overview section of the report, which follows this letter, and then the Performance Report and Fund Profile for each Fund.




We are committed to providing innovative investment solutions and appreciate the trust you place in us.




Sincerely,

DEAR SHAREHOLDER








ECONOMIC AND MARKET OVERVIEW








ABOUT SHAREHOLDERS FUND EXPENSES








FUND








NOTES TO FINANCIAL STATEMENTS








REPORT OF INDEPENDENT REGISTERED PUBLIC ACCOUNTING FIRM








OTHER INFORMATION








INFORMATION ON BOARD OF TRUSTEES AND OFFICERS








GUGGENHEIM INVESTMENTS PRIVACY POLICIES
















THE RYDEX FUNDS ANNUAL REPORT |















March









Dear Shareholder:



Security Investors, LLC (the Investment Adviser) is pleased to present the annual shareholder report for one of our Rydex Funds (the Fund). This report covers performance for the one-year period ended March




The Investment Adviser is part of Guggenheim Investments, which represents the investment management businesses of Guggenheim Partners, LLC, (Guggenheim) a global, diversified financial services firm.




Guggenheim Funds Distributors, LLC is the distributor of the Funds. Guggenheim Funds Distributors, LLC is affiliated with Guggenheim and the Investment Adviser.




We encourage you to read the Economic and Market Overview section of the report, which follows this letter, and then the Performance Report and Fund Profile forthe Fund.




We are committed to providing innovative investment solutions and appreciate the trust you place in us.




Sincerely,

DEAR SHAREHOLDER








ECONOMIC AND MARKET OVERVIEW








A BRIEF NOTE ON THE COMPOUNDING OF RETURNS








ABOUT SHAREHOLDERS FUND EXPENSES








LONG SHORT EQUITY FUND








EVENT DRIVEN AND DISTRESSED STRATEGIES FUND








EMERGING MARKETS STRATEGY FUND








INVERSE EMERGING MARKETS STRATEGY FUND








EMERGING MARKETS BOND STRATEGY FUND








NOTES TO FINANCIAL STATEMENTS








REPORT OF INDEPENDENT REGISTERED PUBLIC ACCOUNTING FIRM








OTHER INFORMATION








INFORMATION ON BOARD OF TRUSTEES AND OFFICERS








GUGGENHEIM INVESTMENTS PRIVACY POLICIES
















THE RYDEX FUNDS ANNUAL REPORT |











March







Dear Shareholder:



Security Investors, LLC (the Investment Adviser) is pleased to present the annual shareholder report for a selection of our alternative strategy Funds (the Funds) for the one-year period ended March




The Investment Adviser is part of Guggenheim Investments, which represents the investment management businesses of Guggenheim Partners, LLC (Guggenheim), a global, diversified financial services firm.




Guggenheim Funds Distributors, LLC is the distributor of the Funds. Guggenheim Funds Distributors, LLC is affiliated with Guggenheim and the Investment Adviser.




We encourage you to read the Economic and Market Overview section of the report, which follows this letter, and then the Performance Report and Fund Profile for each Fund.




We are committed to providing innovative investment solutions and appreciate the trust you place in us.




Sincerely,
