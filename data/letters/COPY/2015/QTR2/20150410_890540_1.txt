Dear Shareholder, 
-3.3%,
Reflecting back on 2014, the United States distanced itself from the rest of the world not only in terms of its stock market performance but in its ability to continue to grow economically. Many developed
countries around the globe spent the year teetering on the edge of recession. The U.S., however, after a shaky first quarter, produced two very solid quarters with the 4th quarter expected to qualify as above the long-term average. 
Looking forward to 2015, as is usually the case, there are many issues that warrant vigilance. The market hates uncertainty, and there are several unknown
outcomes that loom. For starters, the Federal Reserve (Fed) has publicly stated their intention to raise short term rates in 2015. The other looming never seems to go away issue is the Eurozone. Grexit is the catchy


term used for Greece exiting the Euro and as of this writing; a concern of the market is that Greece will Grexit thus defaulting on its debt. Another issue of concern is the price of
oil. While hovering in the $90-$100 per barrel price for much of the summer, oil plunged dramatically. The good news of this decline is that we have some very happy consumers at the gas pumps. The concern we have is the pace of the decline. Supply
has been increasing, but this is not new news. Our concern is that the market is trying to tell us something about demand which would be a signal of slowing growth for economies across the globe. 
Our outlook for the stock market in 2015 is cautious. Valuations, while not in alarming territory, seem a bit stretched. With that said, stocks remain an
important part of a portfolio for our clients. For investors with a long time horizon, we believe the returns of stocks will outperform bonds for longer time horizons (5-10 years), however, 2015 may be a bit bumpier than what investors have
experienced over the last several years. No matter what the upcoming year brings, we look forward to helping you meet the challenges on your horizon by continuing to apply our core investment principles of experience, strength, stability and
integrity. 
Asset Growth 
The
Hancock Horizon Diversified International Fund and the Hancock Horizon Quantitative Long/Short Fund both saw growth of over 30% in assets under management over the last year. As of January 31, 2015, the International Fund had over $480 million
in assets while the Long/Short Fund increased to over $100 million in assets. The Hancock Horizon Burkenroad Small Cap Fund also saw growth in assets of around $70 million over the year and as of January 31, 2015 had over $740 million is assets
under management. 
 
2 



