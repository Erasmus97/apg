Dear Valued Shareholder: 
Wells Fargo Advantage Dow Jones Target Date Funds 
The U.S. Federal Reserve (Fed) held short-term interest rates near zero,
which supported equity investing, although falling energy commodity prices caused volatility. Many domestic longer-term fixed-income market segments recorded positive returns, which was counter to investor expectations that bonds would suffer when
the Fed ended its quantitative easing-related bond-buying program in October 2014. The Feds low-interest-rate policy stifled short-term debt returns. 
Strengthening U.S. economic recovery benefited investors. 
1
2
Europe and Asia focus on economic stimulus. 
4
 
 
 
 
 
Table of Contents
 
5
Target date funds are playing a central role in Americans retirement savings. 
6
Dont let short-term uncertainty derail long-term investment goals. 
Wells Fargo Advantage Funds 
Wells Fargo Advantage Funds
Sincerely, 


