Dear Shareholder, 
In a world that is changing faster than ever before, investors want asset managers who offer a global perspective while generating
strong and sustainable returns. To that end, Columbia Management, in conjunction with its U.K.-based affiliate, Threadneedle Investments, has rebranded to Columbia Threadneedle Investments. The new global brand represents the combined capabilities,
resources and reach of the global group, offering investors access to the best of both firms. 
With a presence in 18
countries and more than 450 investment professionals*, our collective perspective and world view as Columbia Threadneedle Investments gives us deeper insight into what might affect the real-life financial outcomes clients are seeking. Putting our
views into a global context enables us to build richer perspectives and create the right solutions, and provides us with enhanced capabilities to deliver consistent investment performance, which may ultimately lead to better investor outcomes.

As a result of the rebrand, you will begin to see our new logo and colors reflected in printed materials, such as this shareholder
report, as well as on our new website  columbiathreadneedle.com/us. We encourage you to visit us online and view a new video on the About Us tab that speaks to the strength of the firm. 
While we are introducing a new brand, in many ways, the investment company you know well has not changed. The following remain in effect:

 
Fund and strategy names 
 
Established investment teams, philosophies and processes 
 
Account services, features, servicing phone numbers and mailing addresses 
 
Columbia Management Investment Distributors as distributor and Columbia Management Investment Advisers as investment adviser

We recognize that the money we manage represents the hard work and savings of people like you, and that everyone has
different ambitions and different definitions of success. Investors have varying goals  funding their childrens education, enjoying their retirement, putting money aside for unexpected events, and more. Whatever your ambitions, we
believe our wide range of investment products and solutions can help give you confidence that you will reach your goals. 
The world is
constantly changing, but our priority remains the same: to help you secure your finances, meet your goals and achieve success. Thank you for your continued investment with us. Our service representatives are available at 800.345.6611 to help with
any questions. 
Sincerely, 


