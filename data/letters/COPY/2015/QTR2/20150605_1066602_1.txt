Dear Shareholder,
 
Geopolitical strategy and diplomacy — not
to mention the global economy — are deeply driven by questions of access to resources. Who has what you need?
How much do you have to pay to get it? How friendly do buyers and sellers need to be?
 
Today we are confronted with a variety of
such issues that may profoundly affect the world for decades to come. For example, the renewed prominence of the United States
as an energy producer likely will impact the relationships between consumers, like Europe, and producers, such as Russia and OPEC.
While the surge of people fleeing wars in the Middle East and Africa may present an integration challenge to Europe, it also may
represent an infusion of youth and energy that the aging continent needs. And as traditional supplies of water come under stress,
the need for alternative water sources will likely spur the creation of new technologies and new investment opportunities.
 
Of course, there are more immediate economic
concerns, notably whether central bank support — waning in the U.S., expanding pretty much everywhere else — can
sustain global economic growth. Add in elements of political uncertainty, and bouts of market volatility should not be unexpected.
 
What lessons lie here for your investment
program? The most important: Don’t try to “game” diversification. No one can be certain how the global economy
will turn, or know where the best investment opportunities will arise. Rather than groping for the next hot investment, you’re
well advised to remain broadly diversified, retaining the potential to benefit from whatever the next generation of opportunities
has to offer. Periodically review your portfolio with your financial advisor to make sure it remains focused on your investing
goals. And as always, do not make changes to your portfolio without first discussing them with your financial advisor.
 
We appreciate your continued confidence in us, and we look forward
to serving your investment needs in the future.
 
Sincerely,


Dear Shareholder,
Geopolitical strategy and diplomacynot to mention the
global economyare deeply driven by questions of access to resources. Who has what you need? How much do you have to pay to get it? How friendly
do buyers and sellers need to be?
Today we are confronted with a variety of such issues that
may profoundly affect the world for decades to come. For example, the renewed prominence of the United States as an energy producer likely will impact
the relationships between consumers, like Europe, and producers, such as Russia and OPEC. While the surge of people fleeing wars in the Middle East and
Africa may present an integration challenge to Europe, it also may represent an infusion of youth and energy that the aging continent needs. And as
traditional supplies of water come under stress, the need for alternative water sources will likely spur the creation of new technologies and new
investment opportunities.
Of course, there are more immediate economic concerns,
notably whether central bank supportwaning in the U.S., expanding pretty much everywhere elsecan sustain global economic growth. Add in
elements of political uncertainty, and bouts of market volatility should not be unexpected.
What lessons lie here for your investment program? The most
important: Dont try to game diversification. No one can be certain how the global economy will turn, or know where the best
investment opportunities will arise. Rather than groping for the next hot investment, youre well advised to remain broadly diversified, retaining
the potential to benefit from whatever the next generation of opportunities has to offer. Periodically review your portfolio with your financial
advisor to make sure it remains focused on your investing goals. And as always, do not make changes to your portfolio without first discussing them
with your financial advisor.
We appreciate your continued confidence in us, and we look
forward to serving your investment needs in the future.

