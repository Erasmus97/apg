Dear Shareholder:
The global economy appears to be on track for continued,
albeit modest, growth over the next year, with the U.S. leading Europe and Japan. Here at home, employment growth continues, although
the pace has slowed in recent months. Housing data is positive and household finances are benefitting from lower levels of debt
and debt service, gains in real income and lower energy prices.
Growth overseas, particularly in emerging economies,
is a lingering concern. The stronger dollar and sluggish growth abroad have had a negative impact on U.S. exporters and manufacturers,
and lower global energy prices have taken a toll on the domestic energy sector. Nevertheless, our economists see sufficient reason
to expect the U.S. economy overall to maintain its moderate expansionary path.
For months, the most persistent question has been when
the Federal Reserve Board would begin to tighten its monetary policy. That question was answered on December 16, when the Fed bumped
short-term rates up by 0.25%. Based on financial data and guidance from the Fed itself, analysts agree that the tightening process
is likely to be "low and slow."
As always, we encourage you to visit deutschefunds.com
for timely information and insights about economic developments and your Deutsche fund investment. With frequent updates from our
CIO Office and economists, we want to ensure that you are equipped to make informed decisions.
Thank you for your continued investment. We appreciate
the opportunity to serve your investment needs.
Best regards,


