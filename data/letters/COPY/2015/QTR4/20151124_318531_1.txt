Dear Shareholder:

 
We hope you find the annual report for the Prudential Jennison Small
Company Fund, Inc., informative and useful. The report covers performance for the 12-month period that ended September 30, 2015. 
 
Since market conditions change over time, we believe it is important to maintain a diversified portfolio of funds consistent with your tolerance for risk, time
horizon, and financial goals. 
 
Your financial advisor can help you create a
diversified investment plan that may include funds covering all the basic asset classes and that reflects your personal investor profile and risk tolerance. Keep in mind, however, that diversification and asset allocation strategies do not assure a
profit or protect against loss in declining markets. 
 
®
 
Thank you for choosing the
Prudential Investments family of funds. 
 
Sincerely, 


