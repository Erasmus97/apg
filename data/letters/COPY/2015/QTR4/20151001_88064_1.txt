Dear Shareholder:



Despite slow growth during the first half of and disturbing market volatility in late summer, the U.S. economys underlying fundamentals remain sound. Employment growth has been solid, the housing market continues to improve and households have strengthened their finances. Real income is firming and, while consumers remain cautious, theyre likely to loosen their purse strings over time.



In short, our economists see an environment that should support modestly above-trend domestic growth. The strong U.S. dollar continues to act as a headwind to exports and (for those whose positions are not hedged) a detractor from foreign equity returns.



The Federal Reserve Board is likely to start raising short-term interest rates in the U.S. later this year. However, the specific timing remains unclear. In any case, analysts expect the process to be gradual. Meanwhile, Europes slow but steady recovery remains on track, and central bank easing should continue to stimulate growth, albeit at the cost of increased volatility.



This view may seem overly optimistic in light of recent developments in China. Indeed, our Chief Investment Officer, Asoka Wohrmann, wrote in his September update: "The risks from Chinas transformation should not be underestimated. The country has a one-sixth share of global trade, and its growth looks set to be more unpredictable in the future." Nevertheless, he adds "We are still optimistic about the global economy and about most stock exchanges. Our scenario is based on the assumption that the upswings in Europe and the United States are sufficiently self-supporting not to be derailed by developments in China."



We encourage you to visit us at deutschefunds.com for timely information and insights about economic developments and your Deutsche fund investment. With frequent updates from Mr. Wohrmann and our economists, we want to ensure that you have the resources you need to make informed decisions.



Thank you for your continued investment. We appreciate the opportunity to serve your investment needs.



Best regards,
