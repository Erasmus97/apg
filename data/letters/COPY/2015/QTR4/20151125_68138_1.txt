Dear Shareholder,
For the 12 months ended September 30, 2015, Investor Shares returned 4.76% and the lower-cost Admiral Shares returned 4.88%. These returns compare with the 1.40% average return of peer funds and the 3.21% return of the fund’s benchmark index, the Russell 3000 Growth Index.
Despite market turbulence in August and September, the growth stocks that your fund specializes in generally found favor with investors during the period, and they outpaced their value counterparts by a sizable margin.
If you hold shares in a taxable account, you may wish to review the information about after-tax returns, based on the highest federal income tax bracket, that appears later in the report. Please note that as of September 30, 2015, the Morgan Growth Fund realized fractional short-term capital gains equal to less than 1% of fund assets and long-term capital gains equal to about 7%. Gains will be distributed in December.
2
China’s economic woes weighed on U.S. stocks
The broad U.S. stock market returned –0.49% for the 12 months. The final two months were especially rocky as investors worried in particular about the global ripple effects of slower economic growth in China.
For much of the fiscal year, investors were preoccupied with the possibility of an increase in short-term U.S. interest rates. On September 17, the Federal Reserve announced that it would hold rates steady for the time being, a decision that to some investors indicated that the Fed was concerned about the fragility of global markets.
International stocks returned about –11% as the dollar’s strength against many foreign currencies hindered results. Returns for emerging markets, which were especially hard hit by concerns about China, trailed those of the developed markets of the Pacific region and Europe.
Taxable bonds recorded gains as investors searched for safety
The broad U.S. taxable bond market returned 2.94% as investors gravitated toward safe-haven assets amid global stock market volatility. Stimulative monetary policies from the world’s central banks, declining inflation expectations, and global investors’ search for higher yields also helped lift U.S. bonds.
 
3
The yield of the 10-year Treasury note ended September at 2.05%, down from 2.48% a year earlier. (Bond prices and yields move in opposite directions.)
International bond markets (as measured by the Barclays Global Aggregate Index ex USD) returned –7.67%, hurt by the dollar’s strength against many foreign currencies. Without this currency effect, international bonds advanced modestly.
The Fed’s 0%–0.25% target for short-term interest rates continued to limit returns for money market funds and savings accounts.
In a tale of two halves, your fund held up well
As I mentioned earlier in this letter, the Morgan Growth Fund opened the fiscal year with an impressive first-half advance that was fueled by robust returns for health care stocks. That advance faltered in the late summer as market gyrations returned following an extended period of relative calm.
Biotechnology and pharmaceutical shares were especially affected by the second-half retreat. After surging amid merger-and-acquisition activity and optimism about new therapies, these
4
stocks struggled toward the end of the period, roiled by controversy over drug prices and concerns about lofty valuations.
Although the Morgan Growth Fund has substantial holdings in health care, the sector is by no means the only one in which the fund invests. The fund recorded double-digit returns for consumer discretionary stocks, which were buoyed by hopes that lower gas prices would prompt consumers to open their wallets for other purchases. Success in this broad category, which includes everything from home builders to internet retailers, helped blunt the effect of the second-half falloff in health care.
The fund recorded its worst returns among energy stocks, which plummeted after a sharp drop in the price of oil; however, its small allocation to this sector helped limit the damage. (The Fund Profile page later in this report shows the fund’s exact weighting in energy and other sectors as of September 30.)
The Morgan Growth Fund owns more than 300 stocks across several industries, with information technology, consumer discretionary, and health care representing the largest allocations. It also benefits from a variety of perspectives: Each of the fund’s five advisors employs a distinct investment strategy. This approach provides diversification, which can mute some of the volatility associated with the stocks of fast-growing companies.
5
 
