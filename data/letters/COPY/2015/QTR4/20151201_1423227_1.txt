Dear Shareholder, 
The US economy
expanded at an uneven pace during the 12-month period ended September 30, 2015. A sell-off late in the period caused stocks to generate a negative return, whereas higher quality bonds posted a positive return. 
12 Months in Review 
dollar-denominated
Turning to the US economy, gross domestic product (GDP), the value of goods and services produced in the
country, the broadest measure of economic activity and the principal indicator of economic performance, expanded at an annual pace of 2.1% during the fourth quarter of 2014. The US economy then moderated during the first quarter of 2015, as the US
Commerce Department reported that GDP grew at an annual pace of 0.6%. However, US economic activity then improved, as it grew at an annual pace of 3.9% for the second quarter of 2015. Finally, the Commerce Departments initial estimate showed
that third quarter 2015 GDP  released after the reporting period had ended  grew at an annual pace of 1.5%. 
The Federal Reserve (the Fed)
maintained an accommodative monetary policy during the 12 month reporting period. At its June 2015 meeting, the Fed said that it currently anticipates that, even after employment and inflation are near mandate-consistent levels, economic
conditions may, for some time, warrant keeping the target federal funds rate below levels the Committee views as normal in the longer run. At its meeting in July 2015, the Fed provided some clues that it remains on target to institute its
first rate hike by the end of 2015. In particular, the Fed cited solid job gains and noted that underutilization of labor resources has diminished since early this year. However, in September 2015, the Fed kept rates on hold
between 0% and 0.25%. 
 
Table of Contents
Outlook 
Macroeconomic data remain consistent with a moderate pace of growth in the second half of 2015. In terms of the Fed raising interest rates, the weak September 2015
unemployment report may cause the Fed to delay its plans to increase the federal funds rate prior to the end of 2015. In addition, inflation may not reach the Feds 2% target in the near future. Regardless of when the Fed pulls the trigger, we
believe the path of future rate hikes is far more important than the date of lift-off. 
In conclusion, we should expect that the normalization of monetary policy
after such a prolonged and extraordinary accommodation is not going to be easy, and will involve many moving parts. For investors with longer-term time horizons, we believe its important to remain well-diversified while maintaining exposure to
risk assets. 
 
On behalf of Allianz Global Investors Fund Management and our Sub-Advisers,
thank you for investing with us. We encourage you to consult with your financial advisor and to visit our website, us.allianzgi.com, for additional information. We remain dedicated to serving your investment needs. 
Sincerely, 


