Dear shareholder,
U.S. stocks experienced a spike in volatility in recent months. The
pullback we had anticipated for some months took place in August, and
stocks experienced their first official correctiona decline of more
than 10% in the stock marketin more than four years. There were
several headwinds keeping stock prices from moving higher all year,
but the headline for this summer's correction was the news of slowing
economic growth in China and the effect that might have on global
growth. While the market subsequently rebounded, for the time being,
global economic data continues to be a leading driver of investor
sentiment.
Market volatility is naturally unnerving, which is why we recommend
that investors maintain a regular dialogue with their financial
advisors. Your advisor can help put market events into context and
determine whether your portfolio is sufficiently diversified and
continues to match your long-term financial goals.
Introducing John Hancock Multifactor Exchange-Traded Funds (ETFs)
We believe investors benefit from a combination of active and passive
strategies in their portfolios. That's why, for years, we've offered
actively managed funds to our shareholders, alongside asset
allocation portfolios that employ a mix of active and passive
strategies. That same thinking is what led us to team up with
Dimensional Fund Advisors LPa company regarded as one of the
pioneers in strategic beta investing*for the launch of the passively
managed John Hancock Multifactor ETFs. Each ETF seeks to track a
custom index built upon decades of academic research into the factors
that drive higher expected returns: smaller capitalizations, lower
valuations, and higher profitability. For nearly 30 years, it's just
the kind of time-tested approach we have looked for as a manager of
managers. For more information, visit our website at
jhinvestments.com/etf.
On behalf of everyone at John Hancock Investments, I'd like to take
this opportunity to welcome new shareholders and thank existing
shareholders for the continued trust you've placed in us.
Sincerely,


Dear shareholder,
U.S. stocks experienced a spike in volatility in recent months. The
pullback we had anticipated for some months took place in August, and
stocks experienced their first official correctiona decline of more
than 10% in the stock marketin more than four years. There were
several headwinds keeping stock prices from moving higher all year,
but the headline for this summer's correction was the news of slowing
economic growth in China and the effect that might have on global
growth. While the market subsequently rebounded, for the time being,
global economic data continues to be a leading driver of investor
sentiment.
Market volatility is naturally unnerving, which is why we recommend
that investors maintain a regular dialogue with their financial
advisors. Your advisor can help put market events into context and
determine whether your portfolio is sufficiently diversified and
continues to match your long-term financial goals.
Introducing John Hancock Multifactor Exchange-Traded Funds (ETFs)
We believe investors benefit from a combination of active and passive
strategies in their portfolios. That's why, for years, we've offered
actively managed funds to our shareholders, alongside asset
allocation portfolios that employ a mix of active and passive
strategies. That same thinking is what led us to team up with
Dimensional Fund Advisors LPa company regarded as one of the
pioneers in strategic beta investing*for the launch of the passively
managed John Hancock Multifactor ETFs. Each ETF seeks to track a
custom index built upon decades of academic research into the factors
that drive higher expected returns: smaller capitalizations, lower
valuations, and higher profitability. For nearly 30 years, it's just
the kind of time-tested approach we have looked for as a manager of
managers. For more information, visit our website at
jhinvestments.com/etf.
On behalf of everyone at John Hancock Investments, I'd like to take
this opportunity to welcome new shareholders and thank existing
shareholders for the continued trust you've placed in us.
Sincerely,


Dear shareholder,
U.S. stocks experienced a spike in volatility in recent months. The
pullback we had anticipated for some months took place in August, and
stocks experienced their first official correctiona decline of more
than 10% in the stock marketin more than four years. There were
several headwinds keeping stock prices from moving higher all year,
but the headline for this summer's correction was the news of slowing
economic growth in China and the effect that might have on global
growth. While the market subsequently rebounded, for the time being,
global economic data continues to be a leading driver of investor
sentiment.
Market volatility is naturally unnerving, which is why we recommend
that investors maintain a regular dialogue with their financial
advisors. Your advisor can help put market events into context and
determine whether your portfolio is sufficiently diversified and
continues to match your long-term financial goals.
Introducing John Hancock Multifactor Exchange-Traded Funds (ETFs)
We believe investors benefit from a combination of active and passive
strategies in their portfolios. That's why, for years, we've offered
actively managed funds to our shareholders, alongside asset
allocation portfolios that employ a mix of active and passive
strategies. That same thinking is what led us to team up with
Dimensional Fund Advisors LPa company regarded as one of the
pioneers in strategic beta investing*for the launch of the passively
managed John Hancock Multifactor ETFs. Each ETF seeks to track a
custom index built upon decades of academic research into the factors
that drive higher expected returns: smaller capitalizations, lower
valuations, and higher profitability. For nearly 30 years, it's just
the kind of time-tested approach we have looked for as a manager of
managers. For more information, visit our website at
jhinvestments.com/etf.
On behalf of everyone at John Hancock Investments, I'd like to take
this opportunity to welcome new shareholders and thank existing
shareholders for the continued trust you've placed in us.
Sincerely,


