Dear Valued Shareholder: 
Wells Fargo International Bond Fund 
The Fed showed signs of
raising its key rate, but other central banks continued to ease. 
Economic data continued to indicate that the U.S. economy was in a sluggish but
consistent recovery. For example, the U.S. unemployment rate eased from 5.8% at the beginning of the reporting period (November 2014) to 5.0% at the end (October 2015). Throughout the reporting period, the Federal Open Market Committee (FOMC), which
is the Feds monetary policymaking body, kept its key interest rate effectively at zero. 
As the period progressed, various comments by Fed Chair Janet Yellen
led investors to believe that the FOMC would soon raise its key federal funds rate. The FOMC remained on hold at its September 2015 meeting, however, citing concerns about a weaker global economy and subdued U.S. inflation. The FOMCs decision
caused some uncertainty, but by the end of the period, most investors expected a modest Fed rate hike in late 2015 or early 2016. 
In contrast, the European Central
Bank (ECB) showed no signs of raising rates in the near future, as the eurozone continued to grapple with sluggish growth. The eurozone reported annualized gross domestic product growth of 1.6% in the second quarter of 2015, which was the highest
annualized growth for the past eight quarters. Yet while the unemployment rate improved, it remained stubbornly above 10%. The ECB thus maintained a variety of measures aimed at encouraging lending, including making funds available to banks at low
interest rates and imposing a negative interest rate on bank deposits held at the central bank. 
Other major central banks, such as the Peoples Bank of
China and the Bank of Japan, also remained in easing mode in response to sluggish growth in their respective economies. The difference in policy between the Fed and other central banks led the U.S. dollar to appreciate against most major currencies.

Slow but persistent international growth supported risk-based assets, but the stronger dollar depressed the returns of international
investments. 
1
 
 
Table of Contents
 
2
3
Dont let short-term uncertainty derail long-term investment goals. 
Wells Fargo Funds 
Sincerely, 


Dear Valued Shareholder: 
 Wells Fargo Strategic Income Fund
The Fed showed signs
of raising its key rate, but other central banks continued to ease. 
Economic data continued to indicate that the U.S. economy was in a sluggish but
consistent recovery. For example, the U.S. unemployment rate eased from 5.8% at the beginning of the reporting period (November 2014) to 5.0% at the end (October 2015). Throughout the reporting period, the Federal Open Market Committee (FOMC), which
is the Feds monetary policymaking body, kept its key interest rate effectively at zero. 
As the period progressed, various comments by Fed Chair Janet Yellen
led investors to believe that the FOMC would soon raise its key federal funds rate. The FOMC remained on hold at its September 2015 meeting, however, citing concerns about a weaker global economy and subdued U.S. inflation. The FOMCs decision
caused some uncertainty, but by the end of the period, most investors expected a modest Fed rate hike in late 2015 or early 2016. 
In contrast, the European Central
Bank (ECB) showed no signs of raising rates in the near future, as the eurozone continued to grapple with sluggish growth. The eurozone reported annualized gross domestic product growth of 1.6% in the second quarter of 2015, which was the highest
annualized growth for the past eight quarters. Yet while the unemployment rate improved, it remained stubbornly above 10%. The ECB thus maintained a variety of measures aimed at encouraging lending, including making funds available to banks at low
interest rates and imposing a negative interest rate on bank deposits held at the central bank. 
Other major central banks, such as the Peoples Bank of China
and the Bank of Japan, also remained in easing mode in response to sluggish growth in their respective economies. The difference in policy between the Fed and other central banks led the U.S. dollar to appreciate against most major currencies. 
Slow but persistent international growth supported risk-based assets, but the stronger dollar depressed the returns of international
investments. 
1
 
 
Table of Contents
 
2
3
Dont let short-term uncertainty derail long-term investment goals. 
Wells Fargo Funds 
Wells Fargo Funds
Sincerely, 


Dear Valued Shareholder: 
Wells Fargo Asia Pacific Fund
The Fed showed signs of
raising its key rate, but other central banks continued to maintain an easy monetary policy. 
Economic data continued to indicate that the U.S. economy
was in a sluggish but consistent recovery. For example, the U.S. unemployment rate eased from 5.8% at the beginning of the reporting period (November 2014) to 5.0% at the end (October 2015). Throughout the reporting period, the Federal Open Market
Committee (FOMC), which is the Feds monetary policymaking body, kept its key interest rate effectively at zero. 
As the period progressed, various comments by
Fed Chair Janet Yellen led investors to believe that the FOMC would soon raise its key federal funds rate. The FOMC remained on hold at its September 2015 meeting, however, citing concerns about a weaker global economy and subdued U.S. inflation.
The FOMCs decision caused some uncertainty, but by the end of the period, most investors expected a modest Fed rate hike in late 2015 or early 2016. 
In
contrast, the Peoples Bank of China (PBOC) continued to take several steps to support the slowing Chinese economy. In October 2015, the PBOC cut its benchmark one-year lending and deposit rates for the sixth time since November 2014 and
trimmed the percentage of deposits that large banks need to hold as reserves for the fourth time. The moves were aimed at supporting the economy by encouraging lending. The PBOCs actions tended to put pressure on the yuan versus foreign
currencies, pressure that was only reinforced when the PBOC officially devalued the currency in August. 
Other major central banks, including the European Central
Bank and the Bank of Japan, also maintained easy monetary policy in order to support their respective economies. The difference in policy between the Fed and other central banks led the U.S. dollar to appreciate against most major currencies,
including most emerging markets currencies. 
The year was a difficult one for emerging markets investing, marked by slower growth in
China and a stronger dollar that depressed the returns of international assets. 
Emerging markets faced a number of internal and external headwinds during
the past 12 months. Internal factors included not only an economic slowdown in Chinawith the countrys annual growth rate dipping below 7% for the first time since 2009but also persistent low prices for oil, natural gas, and
industrial metals such as copper. Low oil and commodity prices weakened the economies of commodity producers, including Brazil, which also grappled with a political scandal involving corruption at its state-owned oil firm. Among external factors,
global investors continued to worry about the timing of the first rate hike in the U.S. since 2006. The prospect for a U.S. rate hike led to a stronger U.S. dollar; given that many emerging markets have issued U.S. dollar-denominated debt, dollar
strength could make it more difficult for emerging countries to repay their creditors. 
Table of Contents
 
1
Dont let
short-term uncertainty derail long-term investment goals. 
Wells Fargo Funds 
Wells Fargo 
Sincerely, 


Dear Valued Shareholder: 
Wells Fargo Diversified International Fund 
Low commodity prices and slower global growth overshadowed improved economic data. 
In Europe, the eurozones largest economiesFrance, Germany, Italy, and Spainreported gross domestic product (GDP) growth on an annualized basis for the
second quarter of 2015, although results were uneven for smaller, developing economies in the region. While annualized GDP growth slowed in the third quarter in the eurozone overall, it remained positive. Signs of encouragement in the eurozone were
more than offset by slowing economic growth in China. China posted third-quarter GDP growth of 6.9%. While that pace might be envied by other developed economies, it is below levels many investors have come to expect from China. Furthermore, the
government indicated the full-year growth target of 7% may not be met. 
Stocks generally moved higher in April and early May 2015 before events served to discourage
equity investors as summer advanced. The decline of oil and natural gas prices that began during the summer of 2014 continued into 2015. At the same time, the value of the U.S. dollar increased relative to other currencies, particularly those in
emerging markets. Both conditions concerned investors. Sustained low oil, natural gas, and other commodity prices hampered economies in several emerging markets. Slowed commodity consumption among developed markets also suggested a deceleration of
global growth. 
Global central bank policies diverge. 
After ending its quantitative easing-related bond-buying program toward the end of 2014, the U.S. Federal Reserve (Fed) signaled its intention to increase the federal
funds rate. However, concerns about slowing growth, particularly in emerging markets, prompted the Fed to defer an interest-rate increase. Investors globally fell into a holding pattern at times as they tried to anticipate the Feds next move.
Meanwhile, the Bank of Japan announced in October 2014 significant expansion of its economic stimulus programs to include additional injections of liquidity into the economy through bond purchases. In addition, the Government Pension Investment Fund
announced plans to reduce government bond investments and direct those assets to investments in the Japanese stock market. The Peoples Bank of China also took initiatives to spur economic growth by cutting interest rates for the sixth time in
less than a year in October 2015; lowering bank reserve requirements to encourage lending; and allowing the renminbi to depreciate to support exports and to advance government efforts to transition to a more market-driven economy. 
1
 
 
Table of Contents
 
2
3
Dont let short-term uncertainty derail
long-term investment goals. 
Wells Fargo Funds 
Wells Fargo Funds
Sincerely, 


Dear Valued Shareholder: 
Wells Fargo Emerging Markets Equity Fund
The Fed
showed signs of raising its key rate, but other central banks continued to ease. 
Economic data continued to indicate that the U.S. economy was in a
sluggish but consistent recovery. For example, the U.S. unemployment rate eased from 5.8% at the beginning of the reporting period (November 2014) to 5.0% at the end (October 2015). Throughout the reporting period, the Federal Open Market Committee
(FOMC), which is the Feds monetary policymaking body, kept its key interest rate effectively at zero. 
As the period progressed, various comments by Fed Chair
Janet Yellen led investors to believe that the FOMC would soon raise its key federal funds rate. The FOMC remained on hold at its September 2015 meeting, however, citing concerns about a weaker global economy and subdued U.S. inflation. The
FOMCs decision caused some uncertainty, but by the end of the period, most investors expected a modest Fed rate hike in late 2015 or early 2016. 
In contrast,
the Peoples Bank of China (PBOC) continued to take several steps to support the slowing Chinese economy. In October 2015, the PBOC cut its benchmark one-year lending and deposit rates for the sixth time since November 2014 and trimmed the
percentage of deposits that large banks need to hold as reserves for the fourth time. The moves were aimed at supporting the economy by encouraging lending. The PBOCs actions tended to put pressure on the yuan versus foreign currencies,
pressure that was only reinforced when the PBOC officially devalued the currency in August. 
Other major central banks, including the European Central Bank and the
Bank of Japan, also continued to ease in order to support their respective economies. The difference in policy between the Fed and other central banks led the U.S. dollar to appreciate against most major currencies, including most emerging markets
currencies. 
The year was a difficult one for emerging markets investing, marked by slower growth in China and a stronger dollar that
depressed the returns of international assets. 
Emerging markets faced a number of internal and external headwinds during the past 12 months. Internal
factors included not only an economic slowdown in Chinawith the countrys annual growth rate dipping below 7% for the first time since 2009but also persistent low prices for oil, natural gas, and industrial metals such as copper.
Low oil and commodity prices weakened the economies of commodity producers, including Brazil, which also grappled with a political scandal involving corruption at its state-owned oil firm. Among external factors, global investors continued to worry
about the timing of the first rate hike in the U.S. since 2006. The prospect for a U.S. rate hike led to a stronger U.S. dollar; given that many emerging markets have issued U.S. dollar-denominated debt, dollar strength could make it more difficult
for emerging countries to repay their creditors. 
Table of Contents
 
1
Dont let
short-term uncertainty derail long-term investment goals. 
Wells Fargo Funds 
Wells Fargo Funds
Sincerely,



Dear Valued Shareholder: 
Wells Fargo Emerging Markets Fund Equity Income 
The Fed
showed signs of raising its key rate, but other central banks continued to ease. 
Economic data continued to indicate that the U.S. economy was in a
sluggish but consistent recovery. For example, the U.S. unemployment rate eased from 5.8% at the beginning of the reporting period (November 2014) to 5.0% at the end (October 2015). Throughout the reporting period, the Federal Open Market Committee
(FOMC), which is the Feds monetary policymaking body, kept its key interest rate effectively at zero. 
As the period progressed, various comments by Fed Chair
Janet Yellen led investors to believe that the FOMC would soon raise its key federal funds rate. The FOMC remained on hold at its September 2015 meeting, however, citing concerns about a weaker global economy and subdued U.S. inflation. The
FOMCs decision caused some uncertainty, but by the end of the period, most investors expected a modest Fed rate hike in late 2015 or early 2016. 
In contrast,
the Peoples Bank of China (PBOC) continued to take several steps to support the slowing Chinese economy. In October 2015, the PBOC cut its benchmark one-year lending and deposit rates for the sixth time since November 2014 and trimmed the
percentage of deposits that large banks need to hold as reserves for the fourth time. The moves were aimed at supporting the economy by encouraging lending. The PBOCs actions tended to put pressure on the yuan versus foreign currencies,
pressure that was only reinforced when the PBOC officially devalued the currency in August. 
Other major central banks, including the European Central Bank and the
Bank of Japan, also continued to ease in order to support their respective economies. The difference in policy between the Fed and other central banks led the U.S. dollar to appreciate against most major currencies, including most emerging markets
currencies. 
The year was a difficult one for emerging markets investing, marked by slower growth in China and a stronger dollar that
depressed the returns of international assets. 
Emerging markets faced a number of internal and external headwinds during the past 12 months. Internal
factors included not only an economic slowdown in Chinawith the countrys annual growth rate dipping below 7% for the first time since 2009but also persistent low prices for oil, natural gas, and industrial metals such as copper.
Low oil and commodity prices weakened the economies of commodity producers, including Brazil, which also grappled with a political scandal involving corruption at its state-owned oil firm. Among external factors, global investors continued to worry
about the timing of the first rate hike in the U.S. since 2006. The prospect for a U.S. rate hike led to a stronger U.S. dollar; given that many emerging markets have issued U.S. dollardenominated debt, dollar strength could make it more
difficult for emerging countries to repay their creditors. 
Table of Contents
 
1
Dont let short-term uncertainty derail long-term investment goals. 
Wells Fargo Funds 
Wells Fargo Funds
Sincerely, 


Dear Valued Shareholder: 
Wells Fargo Global Opportunities Fund 
Low commodity prices and slower global growth overshadowed improved economic data. 
In Europe, the eurozones largest economiesFrance, Germany, Italy, and Spainreported gross domestic product (GDP) growth on an annualized basis for the
second quarter of 2015, although results were uneven for smaller, developing economies in the region. While annualized GDP growth slowed in the third quarter in the eurozone overall, it remained positive. Signs of encouragement in the eurozone were
more than offset by slowing economic growth in China. China posted third-quarter GDP growth of 6.9%. While that pace might be envied by other developed economies, it is below levels many investors have come to expect from China. Furthermore, the
government indicated the full-year growth target of 7% may not be met. 
Stocks generally moved higher in April and early May 2015 before events served to discourage
equity investors as summer advanced. The decline of oil and natural gas prices that began during the summer of 2014 continued into 2015. At the same time, the value of the U.S. dollar increased relative to other currencies, particularly those in
emerging markets. Both conditions concerned investors. Sustained low oil, natural gas, and other commodity prices hampered economies in several emerging markets. Slowed commodity consumption among developed markets also suggested a deceleration of
global growth. 
Global central bank policies diverge. 
After ending its quantitative easing-related bond-buying program toward the end of 2014, the U.S. Federal Reserve (Fed) signaled its intention to increase the federal
funds rate. However, concerns about slowing growth, particularly in emerging markets, prompted the Fed to defer an interest-rate increase. Investors globally fell into a holding pattern at times as they tried to anticipate the Feds next move.
Meanwhile, the Bank of Japan announced in October 2014 significant expansion of its economic stimulus programs to include additional injections of liquidity into the economy through bond purchases. In addition, the Government Pension Investment Fund
announced plans to reduce government bond investments and direct those assets to investments in the Japanese stock market. The Peoples Bank of China also took initiatives to spur economic growth by cutting interest rates for the sixth time in
less than a year in October 2015; lowering bank reserve requirements to encourage lending; and allowing the renminbi to depreciate to support exports and to advance government efforts to transition to a more market-driven economy. 
1
 
 
Table of Contents
 
2
3
Dont let short-term uncertainty derail long-term investment goals. 
Wells Fargo Funds 
Wells Fargo Funds
Sincerely, 


Dear Valued Shareholder: 
Wells Fargo International Equity Fund
Low commodity prices and slower global growth overshadowed improved economic data. 
In Europe, the eurozones largest economiesFrance, Germany, Italy, and Spainreported gross domestic product (GDP) growth on an annualized basis for the
second quarter of 2015, although results were uneven for smaller, developing economies in the region. While annualized GDP growth slowed in the third quarter in the eurozone overall, it remained positive. Signs of encouragement in the eurozone were
more than offset by slowing economic growth in China. China posted third-quarter GDP growth of 6.9%. While that pace might be envied by other developed economies, it is below levels many investors have come to expect from China. Furthermore, the
government indicated the full-year growth target of 7% may not be met. 
Stocks generally moved higher in April and early May 2015 before events served to discourage
equity investors as summer advanced. The decline of oil and natural gas prices that began during the summer of 2014 continued into 2015. At the same time, the value of the U.S. dollar increased relative to other currencies, particularly those in
emerging markets. Both conditions concerned investors. Sustained low oil, natural gas, and other commodity prices hampered economies in several emerging markets. Slowed commodity consumption among developed markets also suggested a deceleration of
global growth. 
Global central bank policies diverge. 
After ending its quantitative easing-related bond-buying program toward the end of 2014, the U.S. Federal Reserve (Fed) signaled its intention to increase the federal
funds rate. However, concerns about slowing growth, particularly in emerging markets, prompted the Fed to defer an interest-rate increase. Investors globally fell into a holding pattern at times as they tried to anticipate the Feds next move.
Meanwhile, the Bank of Japan announced in October 2014 significant expansion of its economic stimulus programs to include additional injections of liquidity into the economy through bond purchases. In addition, the Government Pension Investment Fund
announced plans to reduce government bond investments and direct those assets to investments in the Japanese stock market. The Peoples Bank of China also took initiatives to spur economic growth by cutting interest rates for the sixth time in
less than a year in October 2015; lowering bank reserve requirements to encourage lending; and allowing the renminbi to depreciate to support exports and to advance government efforts to transition to a more market-driven economy. 
1
 
 
Table of Contents
 
2
3
Dont let short-term uncertainty derail long-term investment goals. 
Wells Fargo Funds
Wells Fargo Funds
Sincerely, 


Dear Valued Shareholder: 
Wells Fargo Intrinsic World Equity Fund
Low commodity prices and slower global growth overshadowed improved economic data. 
In Europe, the eurozones largest economiesFrance, Germany, Italy, and Spainreported gross domestic product (GDP) growth on an annualized basis for the
second quarter of 2015, although results were uneven for smaller, developing economies in the region. While annualized GDP growth slowed in the third quarter in the eurozone overall, it remained positive. Signs of encouragement in the eurozone were
more than offset by slowing economic growth in China. China posted third-quarter GDP growth of 6.9%. While that pace might be envied by other developed economies, it is below levels many investors have come to expect from China. Furthermore, the
government indicated the full-year growth target of 7% may not be met. 
Stocks generally moved higher in April and early May 2015 before events served to discourage
equity investors as summer advanced. The decline of oil and natural gas prices that began during the summer of 2014 continued into 2015. At the same time, the value of the U.S. dollar increased relative to other currencies, particularly those in
emerging markets. Both conditions concerned investors. Sustained low oil, natural gas, and other commodity prices hampered economies in several emerging markets. Slowed commodity consumption among developed markets also suggested a deceleration of
global growth. 
Global central bank policies diverge. 
After ending its quantitative easing-related bond-buying program toward the end of 2014, the U.S. Federal Reserve (Fed) signaled its intention to increase the federal
funds rate. However, concerns about slowing growth, particularly in emerging markets, prompted the Fed to defer an interest-rate increase. Investors globally fell into a holding pattern at times as they tried to anticipate the Feds next move.
Meanwhile, the Bank of Japan announced in October 2014 significant expansion of its economic stimulus programs to include additional injections of liquidity into the economy through bond purchases. In addition, the Government Pension Investment Fund
announced plans to reduce government bond investments and direct those assets to investments in the Japanese stock market. The Peoples Bank of China also took initiatives to spur economic growth by cutting interest rates for the sixth time in
less than a year in October 2015; lowering bank reserve requirements to encourage lending; and allowing the renminbi to depreciate to support exports and to advance government efforts to transition to a more market-driven economy. 
1
 
 
Table of Contents
 
2
3
Dont let short-term uncertainty derail
long-term investment goals. 
Wells Fargo Funds 
Wells Fargo Funds
Sincerely, 


