Dear Shareholder,
Vanguard Growth and Income Fund returned less than 1% for the fiscal year, besting its benchmark, the Standard & Poor’s 500 Index, which returned –0.61%. The average return of its large-capitalization core fund peers was even lower, at –2.30%.
Of the fund’s ten industry sectors, six recorded gains. Consumer-related and health care companies fared well; energy and materials companies were notable detractors.
If you own shares of the fund in a taxable account, you may wish to review information on the fund’s after-tax returns that appears later in this report. Please note that as of September 30, 2015, the Growth and Income Fund had realized short-term capital gains equal to about 1% of fund assets and long-term capital gains equal to about 4% of fund assets. Gains will be distributed in December.
China’s economic woes weighed on U.S. stocks
The broad U.S. stock market returned –0.49% for the 12 months. The final two months were especially rocky as investors
2
worried in particular about the global ripple effects of slower economic growth in China.
For much of the fiscal year, investors were preoccupied with the possibility of an increase in short-term interest rates. On September 17, the Federal Reserve announced that it would hold rates steady for the time being, a decision that to some investors indicated that the Fed was concerned about the fragility of global markets.
Taxable bonds recorded gains as investors searched for safety
The broad U.S. taxable bond market returned 2.94%, as investors gravitated toward safe-haven assets amid global stock market turmoil. Stimulative monetary policies from the world’s central banks, declining inflation expectations, and global investors’ search for higher yields also helped lift U.S. bonds.
The yield of the 10-year Treasury note ended September at 2.05%, down from 2.48% a year earlier. (Bond prices and yields move in opposite directions.)
 
3
International bond markets (as measured by the Barclays Global Aggregate Index ex USD) returned –7.67%, hurt by the dollar’s strength. Without this currency effect, international bonds advanced modestly.
The Fed’s 0%–0.25% target for short-term interest rates continued to limit returns for money market funds and savings accounts.
Consumer stocks did well, but energy weighed heavily
The Growth and Income Fund invests in a diversified portfolio of hundreds of large-cap stocks that, while similar in composition to the S&P 500 Index, has the potential to outperform it. The fund’s three advisors rely on sophisticated computer models to identify the stocks that offer the best prospects.
The two consumer-oriented sectors, consumer discretionary (+15%) and consumer staples (+7%), advanced notably as retail spending rose. Strong auto sales were reflected in auto component stocks, and returns from internet retailers were particularly robust. Gains also came from the subsectors associated with the housing market’s rebound.
Health care stocks also contributed, generating returns of about 7%. The industry has benefited from several trends, including an aging U.S. population that requires more health care and the broader availability of insurance coverage through the Affordable Care Act. Health care services and medical equipment companies stood out. The picture was far different for the pharmaceutical and biotechnology
4
subsectors: They were strong performers in the first half of the period, but their 12-month return was near zero.
Information technology, the largest sector, posted a tepid 2% return. Internet-based services enjoyed strong returns as investors were optimistic about the subsector’s growth prospects.
Financial stocks, which made up the third-largest portion of the fund on average, returned –2%. Although health care real estate investment trusts benefited from the trends helping the health care industry, banks, asset managers, and consumer credit stocks retreated in the face of continuing low interest rates, crimping profit margins from loans.
The two sectors that weighed most heavily on the fund were energy (–24%) and materials (–14%). Both were hit hard by the big decline in commodity prices. Oil prices plunged about 50% over the 12-month period, and natural gas fell about 40% as a global glut of supply and slowing demand from developing nations cut profits dramatically.
For more about the advisors’ strategy and the fund’s positioning during the 12 months, please see the Advisors’ Report that follows this letter.
The fund’s three advisors have seen success over time
Over the past ten years, Investor Shares of the Growth and Income Fund returned an annual average of 6.16%, close to that
5
Staying the course can help you stay closer to your fund’s return
When stock markets are highly volatile, as in recent months, it’s tempting to run for cover. But the price of panic can be high.
A rough measure of what can be lost from attempts to time the market is the difference between the returns produced by a fund and the returns earned by the fund’s investors.
Many sensible investment behaviors can contribute to the difference in returns, but industry cash flow data suggest that one important factor is the generally counterproductive effort to buy and sell at the “right” time. Keeping your emotions in check can help narrow the gap.
Mutual fund returns and investor returns over the last decade
Dear Shareholder,
For the 12-month period, Vanguard Structured Large-Cap Equity Fund returned 1.03% for Institutional Shares and a little more, 1.05%, for Institutional Plus Shares, which have a lower expense ratio. It surpassed the slightly negative result of its benchmark, the S&P 500 Index, by more than 1 percentage point and the average return of its peers by more than 3 percentage points.
Mid- and small-capitalization stocks performed a little better than large-caps. Institutional Shares of Vanguard Structured Broad Market Fund returned 2.80% and Institutional Plus Shares 2.84%. This fund outpaced its comparative standards by wider margins than its large-cap counterpart. It beat its benchmark, the Russell 3000 Index, by more than 3 percentage points and the average return of its peers by more than 5 percentage points.
2
Results by sector varied widely. For both funds, however, consumer discretionary and energy were among the biggest relative contributors, helping to offset subpar performances in other sectors including telecommunications and financials.
Please note: At the end of the reporting period, we estimated that the Structured Broad Market Fund would distribute capital gains equal to 7.6% of net assets in mid-December. The distribution will be almost all long-term gains.
China’s economic woes weighed on global stocks
The broad U.S. stock market returned –0.49% for the 12 months. The final two months were rocky as investors worried in particular about the global ripple effects of slower economic growth in China.
For much of the fiscal year, investors were preoccupied with the possibility of an increase in short-term interest rates. On September 17, the Federal Reserve announced that it would hold rates steady for the time being, a decision that to some investors indicated the Fed’s concern about the fragility of global markets.
International stocks returned about –11% as the dollar’s strength against many foreign currencies weighed on results. Returns for emerging markets, which were especially hard hit by concerns about China, trailed those of the developed markets of the Pacific region and Europe.
Taxable bonds recorded gains as investors searched for safety
The broad U.S. taxable bond market returned 2.94% as investors gravitated toward safe-haven assets amid global
 
3
stock market turmoil. Stimulative monetary policies from the world’s central banks, declining inflation expectations, and global investors’ search for higher yields also helped lift U.S. bonds.
The yield of the 10-year Treasury note ended September at 2.05%, down from 2.48% a year earlier. (Bond prices and yields move in opposite directions.)
International bond markets (as measured by the Barclays Global Aggregate Index ex USD) returned –7.67%; the dollar’s strength was a significant factor here as well. Without this currency effect, international bonds advanced modestly.
The Fed’s 0%–0.25% target for short-term interest rates continued to limit returns for money market funds and savings accounts.
The funds’ quantitative screening produced benchmark­beating results
The funds’ advisor, Vanguard Quantitative Equity Group, seeks to construct portfolios whose characteristics—stock market capitalization, sector allocation, and risk profile—closely match those of each fund’s benchmark. The advisor selects a subset of the stocks in the benchmarks using computer-driven quantitative models that consider criteria such as valuations, earnings growth potential, and market sentiment.
The energy sector continued to suffer from the sharp drop in the price of oil and returned roughly –30% in each of the benchmarks. The funds were able to produce better returns, however, by holding oil and gas stocks that held up better than the sector as a whole. The broad market fund also benefited from underweighting the oil and gas equipment and services segment.
Another area of strength for the broad market fund was the information technology sector, where its holdings in software stocks produced double-digit returns.
The funds underperformed their benchmarks in a few sectors. In financials, outsized holdings in consumer finance hurt both funds. Regional banks were a weak spot for the broad market fund. And both funds disappointed in telecommunication services.
For more information about the advisor’s approach and the funds’ positioning during the year, please see the Advisor’s Report that follows this letter.
The funds have weathered ups and downs in the market
The Institutional Shares of both funds have been around for close to a decade, so their advisor has navigated them through the 2008–2009 financial crisis as well as some
4
heady times in the markets before and after that. Although the funds haven’t bested their benchmarks every fiscal year, they have outperformed over the longer term through good markets and bad. This is a testament to the good stewardship of the funds’ advisor as well as the merit of our low-cost structure.
The large-cap fund’s Institutional Shares have produced an average annual return of 6.87% since their launch in May 2006, while the benchmark returned 6.56%. The Broad Market Fund’s Institutional Shares have returned a little less than that, 6.49%, since their launch in November 2006, compared with 6.00% for the benchmark.
Both funds outpaced the average return of their peers by even wider margins over the same periods.
A dose of discipline is crucial when markets become volatile
Although the broad U.S. stock market has posted gains for six straight calendar years—from 2009 to 2014—that streak may not last a seventh. Stocks tumbled in August and swung up and down in September.
5
Staying the course can help you stay closer to your fund’s return
When stock markets are highly volatile, as in recent months, it’s tempting to run for cover. But the price of panic can be high.
A rough measure of what can be lost from attempts to time the market is the difference between the returns produced by a fund and the returns earned by the fund’s investors.
Many sensible investment behaviors can contribute to the difference in returns, but industry cash flow data suggest that one important factor is the generally counterproductive effort to buy and sell at the “right” time. Keeping your emotions in check can help narrow the gap.
Mutual fund returns and investor returns over the last decade
