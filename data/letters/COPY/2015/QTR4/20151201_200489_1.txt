Dear Shareholders: 
Despite weak global growth, the U.S. economy continues to expand modestly, buoyed by firming labor and housing markets and resilient consumer spending. However, the


U.S. Federal Reserve continues to take a cautious approach to raising interest rates. 
In Asia,
Chinas slowing economic growth has raised concerns and fed global market volatility. Commodity exporters have been hurt by weaker Chinese demand. Global oil markets remain oversupplied, further depressing crude oil and gasoline prices. Japan
continues to struggle in its attempt to stimulate its economy through monetary and fiscal policy initiatives. 
While the eurozone economy is expanding,
growth has been mild and inconsistent, while deflation remains a risk. Hopes for a more 

robust recovery rest on the European Central Banks quantitative easing program. 
The worlds financial markets have become increasingly complex in recent years. Now, more than ever, it is important to understand companies on a global basis. At MFS®, we believe our integrated research
platform, collaborative culture, active risk management process and long-term focus give us a research advantage. 
As investors, we aim to add long-term
value. We believe this approach will serve you well as you work with your financial advisor to reach your investment objectives. 
Respectfully,



Dear Shareholders: 
Despite weak global growth, the U.S. economy continues to expand modestly, buoyed by firming labor and housing markets and resilient consumer spending. However, the


U.S. Federal Reserve continues to take a cautious approach to raising interest rates. 
In Asia,
Chinas slowing economic growth has raised concerns and fed global market volatility. Commodity exporters have been hurt by weaker Chinese demand. Global oil markets remain oversupplied, further depressing crude oil and gasoline prices. Japan
continues to struggle in its attempt to stimulate its economy through monetary and fiscal policy initiatives. 
While the eurozone economy is expanding,
growth has been mild and inconsistent, while deflation remains a risk. Hopes for a more 

robust recovery rest on the European Central Banks quantitative easing program. 
The worlds financial markets have become increasingly complex in recent years. Now, more than ever, it is important to understand companies on a global basis. At MFS®, we believe our integrated research
platform, collaborative culture, active risk management process and long-term focus give us a research advantage. 
As investors, we aim to add long-term
value. We believe this approach will serve you well as you work with your financial advisor to reach your investment objectives. 
Respectfully,



Dear Shareholders: 
Despite weak global growth, the U.S. economy continues to expand modestly, buoyed by firming labor and housing markets and resilient consumer spending. However, the


U.S. Federal Reserve continues to take a cautious approach to raising interest rates. 
In Asia,
Chinas slowing economic growth has raised concerns and fed global market volatility. Commodity exporters have been hurt by weaker Chinese demand. Global oil markets remain oversupplied, further depressing crude oil and gasoline prices. Japan
continues to struggle in its attempt to stimulate its economy through monetary and fiscal policy initiatives. 
While the eurozone economy is expanding,
growth has been mild and inconsistent, while deflation remains a risk. Hopes for a more 

robust recovery rest on the European Central Banks quantitative easing program. 
The worlds financial markets have become increasingly complex in recent years. Now, more than ever, it is important to understand companies on a global basis. At MFS®, we believe our integrated research
platform, collaborative culture, active risk management process and long-term focus give us a research advantage. 
As investors, we aim to add long-term
value. We believe this approach will serve you well as you work with your financial advisor to reach your investment objectives. 
Respectfully,



