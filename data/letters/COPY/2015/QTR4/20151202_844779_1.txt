Dear Shareholder, 
Diverging monetary policies and shifting economic outlooks across regions were the overarching themes driving financial markets during the 12-month period
ended September 30, 2015. U.S. economic growth was picking up considerably in the fourth quarter of 2014, while the broader global economy showed signs of slowing. Investors favored the stability of U.S. assets despite expectations that the
Federal Reserve (the Fed) would eventually be inclined to raise short-term interest rates. International markets struggled even as the European Central Bank and the Bank of Japan eased monetary policy. Oil prices plummeted in late 2014
due to a global supply-and-demand imbalance, fueling a sell-off in energy-related assets and emerging markets. Investors piled into U.S. Treasury bonds as their persistently low yields had become attractive as compared to the even lower yields on
international sovereign debt. 
Equity markets reversed in early 2015, with international markets outperforming the United States as global risks
temporarily abated and the U.S. economy hit a soft patch amid a harsh winter and a west coast port strike. High valuations took their toll on U.S. stocks, while bond yields fell to extreme lows. (Bond prices rise as yields fall.) In contrast,
economic reports in Europe and Asia began to improve, and accommodative policies from central banks in those regions helped international equities rebound. Oil prices stabilized, providing some relief for emerging market stocks, although a stronger
U.S. dollar posed another significant headwind for the asset class. 
U.S. economic growth regained momentum in the second quarter, helping U.S.
stocks resume an upward path; however, the improving data underscored the likelihood that the Fed would raise short-term rates before the end of 2015 and bond yields moved swiftly higher. The month of June brought a sharp, but temporary, sell-off
across most asset classes as Greeces long-brewing debt troubles came to an impasse. These concerns abated when the Greek parliament passed a series of austerity and reform measures in July. But the markets calm was short-lived. Signs of
weakness in Chinas economy sparked extreme levels of volatility in Chinese equities despite policymakers attempts to stabilize the market. 
Higher volatility spread through markets globally in the third quarter as further evidence of deceleration in China stoked worries about overall global growth. Weakening demand caused oil prices to slide
once again, igniting another steep sell-off in emerging markets. Global volatility spiked higher as investors speculated whether the Fed would raise rates at its September meeting. News that the rate hike had been postponed brought little relief in
the markets as the central banks decision reinforced investors concerns about the state of the global economy. Global equities and high yield bonds broadly declined, while higher quality assets, including U.S. Treasury bonds, municipal
bonds and investment grade credit benefited from investors seeking shelter amid global uncertainty. 
blackrock.com
Sincerely, 


Dear Shareholder, 
Diverging monetary policies and shifting economic outlooks across regions were the overarching themes driving financial markets during the 12-month period
ended September 30, 2015. U.S. economic growth was picking up considerably in the fourth quarter of 2014, while the broader global economy showed signs of slowing. Investors favored the stability of U.S. assets despite expectations that the
Federal Reserve (the Fed) would eventually be inclined to raise short-term interest rates. International markets struggled even as the European Central Bank and the Bank of Japan eased monetary policy. Oil prices plummeted in late 2014
due to a global supply-and-demand imbalance, fueling a sell-off in energy-related assets and emerging markets. Investors piled into U.S. Treasury bonds as their persistently low yields had become attractive as compared to the even lower yields on
international sovereign debt. 
Equity markets reversed in early 2015, with international markets outperforming the United States as global risks
temporarily abated and the U.S. economy hit a soft patch amid a harsh winter and a west coast port strike. High valuations took their toll on U.S. stocks, while bond yields fell to extreme lows. (Bond prices rise as yields fall.) In contrast,
economic reports in Europe and Asia began to improve, and accommodative policies from central banks in those regions helped international equities rebound. Oil prices stabilized, providing some relief for emerging market stocks, although a stronger
U.S. dollar posed another significant headwind for the asset class. 
U.S. economic growth regained momentum in the second quarter, helping U.S.
stocks resume an upward path; however, the improving data underscored the likelihood that the Fed would raise short-term rates before the end of 2015 and bond yields moved swiftly higher. The month of June brought a sharp, but temporary, sell-off
across most asset classes as Greeces long-brewing debt troubles came to an impasse. These concerns abated when the Greek parliament passed a series of austerity and reform measures in July. But the markets calm was short-lived. Signs of
weakness in Chinas economy sparked extreme levels of volatility in Chinese equities despite policymakers attempts to stabilize the market. 
Higher volatility spread through markets globally in the third quarter as further evidence of deceleration in China stoked worries about overall global growth. Weakening demand caused oil prices to slide
once again, igniting another steep sell-off in emerging markets. Global volatility spiked higher as investors speculated whether the Fed would raise rates at its September meeting. News that the rate hike had been postponed brought little relief in
the markets as the central banks decision reinforced investors concerns about the state of the global economy. Global equities and high yield bonds broadly declined, while higher quality assets, including U.S. Treasury bonds, municipal
bonds and investment grade credit benefited from investors seeking shelter amid global uncertainty. 
blackrock.com
Sincerely, 


Dear Shareholder, 
Diverging monetary policies and shifting economic outlooks across regions were the overarching themes driving financial markets during the 12-month period
ended September 30, 2015. U.S. economic growth was picking up considerably in the fourth quarter of 2014, while the broader global economy showed signs of slowing. Investors favored the stability of U.S. assets despite expectations that the
Federal Reserve (the Fed) would eventually be inclined to raise short-term interest rates. International markets struggled even as the European Central Bank and the Bank of Japan eased monetary policy. Oil prices plummeted in late 2014
due to a global supply-and-demand imbalance, fueling a sell-off in energy-related assets and emerging markets. Investors piled into U.S. Treasury bonds as their persistently low yields had become attractive as compared to the even lower yields on
international sovereign debt. 
Equity markets reversed in early 2015, with international markets outperforming the United States as global risks
temporarily abated and the U.S. economy hit a soft patch amid a harsh winter and a west coast port strike. High valuations took their toll on U.S. stocks, while bond yields fell to extreme lows. (Bond prices rise as yields fall.) In contrast,
economic reports in Europe and Asia began to improve, and accommodative policies from central banks in those regions helped international equities rebound. Oil prices stabilized, providing some relief for emerging market stocks, although a stronger
U.S. dollar posed another significant headwind for the asset class. 
U.S. economic growth regained momentum in the second quarter, helping U.S.
stocks resume an upward path; however, the improving data underscored the likelihood that the Fed would raise short-term rates before the end of 2015 and bond yields moved swiftly higher. The month of June brought a sharp, but temporary, sell-off
across most asset classes as Greeces long-brewing debt troubles came to an impasse. These concerns abated when the Greek parliament passed a series of austerity and reform measures in July. But the markets calm was short-lived. Signs of
weakness in Chinas economy sparked extreme levels of volatility in Chinese equities despite policymakers attempts to stabilize the market. 
Higher volatility spread through markets globally in the third quarter as further evidence of deceleration in China stoked worries about overall global growth. Weakening demand caused oil prices to slide
once again, igniting another steep sell-off in emerging markets. Global volatility spiked higher as investors speculated whether the Fed would raise rates at its September meeting. News that the rate hike had been postponed brought little relief in
the markets as the central banks decision reinforced investors concerns about the state of the global economy. Global equities and high yield bonds broadly declined, while higher quality assets, including U.S. Treasury bonds, municipal
bonds and investment grade credit benefited from investors seeking shelter amid global uncertainty. 
At BlackRock, we believe investors need to
think globally, extend their scope across a broad array of asset classes and be prepared to move freely as market conditions change over time. We encourage you to talk with your financial advisor and visit blackrock.com for further insight about
investing in todays markets. 
Sincerely, 


Dear Shareholder, 
Diverging monetary policies and shifting economic outlooks across regions were the overarching themes driving financial markets during the 12-month period
ended September 30, 2015. U.S. economic growth was picking up considerably in the fourth quarter of 2014, while the broader global economy showed signs of slowing. Investors favored the stability of U.S. assets despite expectations that the
Federal Reserve (the Fed) would eventually be inclined to raise short-term interest rates. International markets struggled even as the European Central Bank and the Bank of Japan eased monetary policy. Oil prices plummeted in late 2014
due to a global supply-and-demand imbalance, fueling a sell-off in energy-related assets and emerging markets. Investors piled into U.S. Treasury bonds as their persistently low yields had become attractive as compared to the even lower yields on
international sovereign debt. 
Equity markets reversed in early 2015, with international markets outperforming the United States as global risks
temporarily abated and the U.S. economy hit a soft patch amid a harsh winter and a west coast port strike. High valuations took their toll on U.S. stocks, while bond yields fell to extreme lows. (Bond prices rise as yields fall.) In contrast,
economic reports in Europe and Asia began to improve, and accommodative policies from central banks in those regions helped international equities rebound. Oil prices stabilized, providing some relief for emerging market stocks, although a stronger
U.S. dollar posed another significant headwind for the asset class. 
U.S. economic growth regained momentum in the second quarter, helping U.S.
stocks resume an upward path; however, the improving data underscored the likelihood that the Fed would raise short-term rates before the end of 2015 and bond yields moved swiftly higher. The month of June brought a sharp, but temporary, sell-off
across most asset classes as Greeces long-brewing debt troubles came to an impasse. These concerns abated when the Greek parliament passed a series of austerity and reform measures in July. But the markets calm was short-lived. Signs of
weakness in Chinas economy sparked extreme levels of volatility in Chinese equities despite policymakers attempts to stabilize the market. 
Higher volatility spread through markets globally in the third quarter as further evidence of deceleration in China stoked worries about overall global growth. Weakening demand caused oil prices to slide
once again, igniting another steep sell-off in emerging markets. Global volatility spiked higher as investors speculated whether the Fed would raise rates at its September meeting. News that the rate hike had been postponed brought little relief in
the markets as the central banks decision reinforced investors concerns about the state of the global economy. Global equities and high yield bonds broadly declined, while higher quality assets, including U.S. Treasury bonds, municipal
bonds and investment grade credit benefited from investors seeking shelter amid global uncertainty. 
blackrock.com
Sincerely, 


