Dear Shareholders:
 
A few of the watchwords for financial markets over the past year
included excess liquidity, unicorn valuations, risk-on/risk-off, deflation, income inequality and, unfortunately, terrorism. Perhaps
there is another way to describe the overarching themes driving the markets as we enter the eighth year of recovery since the Great
Financial Recession. The markets are still captive to the complexity and contradiction of fundamental data. Alpine believes that
this condition stems from the unfocused, weak economic expansion. Atypically, the markets are dependent not upon the leadership
of governments or industry but from central banks. Historically, economic leadership has not been the role of central banks. However,
corporate spending has been limited and government stimulus has been constrained by either ideology (austerity) or weak tax revenues.
This has placed an unusual burden on central banks, worldwide, which are designed and staffed to balance or neutralize unfavorable
economic forces. Perhaps that is why both consumer and business confidence has been weak during this cycle as central banks have,
by default, become the primary source of both economic stimulus and direction during the past six years, yet by their nature, they
can only provide monetary leadership.
 
As we look back on fiscal year 2015, and prepare for 2016, the
slow recovery of both global economies and evolving capital markets is very clear. Nonetheless, this has been an eventful year
filled with notable shifts and even dramatic reversals leading to periods of increased volatility. The dominant factors have been
the anticipated emergence of divergent interest rate trends between cyclically advancing countries and those which lag behind.
At the start of 2015, it was clear to the markets that the Federal Reserve was done with quantitative easing (QE), — after
first signaling this intention back in May 2013 — to the point where the Fed was about to raise interest rates. Also, at
the beginning of the year it was clear that the European Central Bank (ECB) was about to embark upon a program of quantitative
easing trailing the Fed’s lead by over five years.
 
As we embark upon 2016, the Fed seems likely to finally raise
interest rates, if only by a quarter percent, while the ECB has announced an extension of QE into 2017. Long-term trends remain
in place, so we believe that this divergence between slowly strengthening economies and still weak, lagging countries, may continue
for another two to three years, taking us into 2018. It is conceivable that even more time will be required before another global
super cycle of demand exceeding supply emerges. At minimum, any
major global upswing of demand for goods and services may take three to five years to emerge. This suggests that inflation may
not become a significant threat for such a period of time. The upshot is that we may see U.S. interest rates gradually increase
over several years by 100 to 250 basis points towards normalized historical levels. We believe the ECB is at least three years
behind the Fed. This also suggests that we may be experiencing an historically extended period of economic growth without recession
that could last into the next decade.
 
Members of our Investment team, with respect to their areas of
expertise, have made the following observations covering both the past year and the new year. Bruce Ebnother, a member of our real
estate investment team noted that “Unlike typical recovery cycles ... only a moderate amount of new supply has come online.”
Mark Austin, who covers global housing stocks, commented that “wages (in the U.S.) have grown faster over the past year than
at any other point during this recovery since 2008 and unemployment has dropped to its lowest level since then. Despite this, industry
fundamentals continue to point to merely another year of slow but steady growth”. He is, however, more optimistic because
in 2018 “40 million millennials will turn 25, an age when historically increased numbers begin looking for new homes. Already,
we are experiencing a pick-up in household formation.” So, it appears that both commercial and residential real estate in
the U.S., among other countries, is undersupplied from a cyclical perspective and demand is underpinned by solid fundamentals.
 
In contrast, the energy and mining sectors have only recently
experienced a significant downdraft in demand at a time where productive capacity hit a new peak. Thus, over the past year, we
have only begun to see a modest reduction of output despite a significant collapse in prices due to the excesses of the commodity
sector. Sarah Hunt, who follows industrial companies and the oil sector, in particular, as part of our commodities team, observed
that the old oil industry adage that “low prices cure low prices” (where supplies typically reduce production in the
face of falling prices only when the cost of extraction is higher than the sales price) did not hold true as “large fiscal
inventories ... and current future strip pricing is not predicting a quick fix for low oil prices”. Thus, we believe consumers
and certain industries should continue to benefit from the lowest oil prices since 2004, roughly 60% below peak levels. This could
last well into 2016, before gradually moving higher.
1
 
 
Supply-side issues have generally not been such a factor in the
healthcare sector as demand growth continues. Jonathan Gelb, who follows the healthcare sector for us, notes that “companies
that produce clinically different drugs that create cost savings will remain insulated from pricing pressure, however, companies
whose models depend on aggressive pricing actions without significant innovation are likely to see growth impacted as soon as 2016.”
No doubt his comments apply to both Turing Pharmaceutical and Valent Pharmaceuticals, which relied upon repackaging acquired medication
at higher prices for a captive marketplace. With limited innovation, he believes that consolidation “is a theme that will
likely continue into 2016, since nearly $265 billion worth of mergers or acquisitions (M&amp;As) were announced during 2015.
 
Another area where acquisitions have been on the rise, creating
revenue and hopefully, earnings growth, has been the financial sector. Peter Kovalski, who follows regional banks and financial
services for us, noted that M&amp;A activity in the U.S. for 2015 to date has totaled 243 announced deals. What’s notable
is that this volume is only slightly behind last year’s level of 284 deals, the most since 2006 when 299 transactions took
place. The difference is one of scale as the aggregate deal value in 2006 was $109 billion compared to only $19 billion in 2014.
Thus, the typical transaction was less than a fifth of the size of previous transactions in 2006, suggesting that “the main
driver behind these small bank deals is the rising cost of regulatory compliance ... a burden which is not going away.” The
implication of this trend is that small local banks may not be as competitive in their local markets as larger, regional players.
This may accelerate the underlying consolidation trend in which the number of banks in the U.S. has fallen from 14,907 in 1984
to 5,410 in 2015.
 
In a world where the pie is only growing slowly or for just a
few, where their industry or market share is shrinking due to regulations, technological change or better-capitalized competition,
the best business alternative may be a merger or acquisition. Indeed, Brian Hennessey, who follows commodity-related businesses
in the mining and industrial materials companies for us, described 2015 as “the year of the mega mergers.” He expects
this to continue in 2016 as “many forces driving consolidation across the developed world with low interest rates and high
uncertainty with respect to the regulatory and macro-economic backdrop.” Naturally such consolidation can create opportunities
for both astute and opportunistic acquirers as well as for companies whose shares have been undervalued by the market place. The
broad economic implications of strategic or complementary consolidation is that it encourages buyers to shut the least effective
operators or least efficient
producers, which curtails excess production, permitting supply
and demand to find equilibrium levels faster than they might otherwise. Companies structured merely to grow through acquisitions,
however, may lose their luster if they cannot innovate or continue to find M&amp;A opportunities for expansion. Such risks have
driven the market to place of particularly high earnings multiples on technological leaders including Facebook, Amazon, Netflix
and Google (now known as Alphabet), among others which are expanding geographically, across platforms and market segments, while
both making acquisitions and pushing innovation in ways that portend an extended period of revenue expansion.
 
As we look forward to 2016 and beyond, we should note that a key
fundamental driver of growth is in fact demographic. That is, continual global population growth and the trending evolution of
agrarian populations moving to urban industrial lifestyles, transitioning from subsistence to higher levels of productivity. This
ongoing shift has not only been true of emerging countries but even here at home where many recent college graduates have chosen
to live in urban settings, leading to a rebirth of a number of downtowns throughout the U.S. This has put extra demands upon infrastructure
while creating new nodes for consumer activity, which, of course, has led to rising real estate values in many places. The rapidly
expanding capacity to make the world our “oyster,” no longer depending upon local merchants or service providers to
enhance our lifestyle, may prove to be an affirmation of our technological prowess even though there might be a range of social
consequences. That said, the increased access to information, as well as goods and services, is opening up new avenues for creativity
on both personal and societal levels. Those companies that can benefit from these different trends — old and new, small and
large-should be able to generate attractive growth. As the global economy gradually returns to normalized levels of expansion,
we would anticipate that the period of corporate consolidation through M&amp;A will evolve towards a new period of capital investment
and innovation. Even though this recent period of economic recovery has been quite extended, we are optimistic that the longer-term
dynamic for continued expansion will become more apparent in the months and quarters ahead of us.
 
Thank you for your interest and support.
 
Sincerely,


Dear Shareholders:
 
We present below the annual results for the Alpine
International Real Estate Equity Fund. For the period ended October 31, 2015, the closing NAV was $21.92 per share, representing
a total return of -8.05%. The Fund’s benchmark index, the FTSE EPRA/NAREIT Global Ex-U.S. Index returned -2.07% over the
same period. In the context of broader equity markets, the MSCI EAFE Index produced a total return of -0.07%. All references in
this letter to the Fund’s performance relate to the performance of the Fund’s Institutional Class.
 
Performance Drivers
 
Broadly speaking, international equity performance
for the year was a story of contrasts with strength in the first half of the year giving way to uncertainty in the second half.
The broad themes driving the momentum of returns were the slow-motion global recovery, expectations for the timing and pace of
Fed policy, and questions surrounding China and its ability to manage its economy through a pronounced cyclical downturn. During
the first half of the period markets took a constructive view of these variables and as such the risk on trade was firmly established.
Adding to this was the Chinese government’s use of moral suasion to encourage equity market speculation seemingly as a means
for promoting mainland IPO activity and acting as a pressure valve for over-indebted corporates. The bubble in the China A-share
market that followed shortly thereafter was the inflection point for the risk trade in global markets. Downward pressure on equities
gathered further momentum in August when China embarked on a managed depreciation of its currency, which was communicated poorly
and subsequently sent markets into a tailspin. Concerns were amplified by foreign exchange (FX) volatility (USD strength), persistent
commodity weakness and a seemingly endless list of geopolitical flare ups ranging from Greece to Russia to the constantly evolving
situation in the Middle East.
 
Nevertheless, the risk off environment in the
second half did nothing to dampen transaction volumes in the listed and physical property markets. According to Jones Lang LaSalle
(JLL), transaction volumes through Q3 2015 were $497bn, up 13% from 2014 in local currency terms, while UBS calculates $154bn in
listed real estate mergers and acquisitions (M&amp;A) during the period which lags only 2007 values in aggregate. The Fund’s
largest position was in the UK whose economy continued to muddle along and real estate equities handily outperformed despite
the lingering threat of a rate hike as rental
growth in London offices continued to surge higher. In Japan both the developers and the Japanese REITs (JREITs) lagged the broader
market despite robust evidence of a recovery in operational markets, especially in Tokyo. Market focus has shifted toward expectations
for further Bank of Japan (BoJ) easing as the macroeconomic data follows a prolonged downtrend. European real estate outperformed
by a wide margin due largely to the European Central Bank (ECB) launching its quantitative easing (QE) program. Smaller markets
including Ireland and Spain saw strong returns. The data in Europe have been lackluster and there remain downside risks to growth
and inflation which could prompt the ECB to consider further measures to support the recovery. The deterioration in the macroeconomic
and currency outlook in Australia continued to weigh on the Australian REIT (AREIT) market on an absolute basis but it nevertheless
outperformed on a relative basis due to reasonable valuations, stable fundamentals and yield support. Emerging markets (EM) generally
tracked the momentum in China which ultimately was a significant drag on returns as EMs took the brunt of the impact from the Renminbi
devaluation, via FX channels as well as the equities.
 
Portfolio Analysis
 
At the headline level, the relative underperformance
of the Fund during the period was driven primarily by stock selection, however this aggregate perspective masks the wide variance
of contributions across geographies as economic and policy divergence became increasingly more pronounced for international investors.
For example, the Fund saw relative outperformance across the majority of developed markets due to both stock selection and allocation
(i.e., the UK, Japan and many European countries including Ireland, France and Spain) or due to allocation decisions entirely (i.e.,
Canada and Australia). By contrast the primary detractors to relative performance during the past year could largely be attributed
to select emerging markets countries, specifically the overweight allocations to India, Brazil and Mexico or stock selection in
Indonesia weighing on performance.
 
At quarter end the top 10 positions accounted
for 33.84% of the portfolio versus 37.30% twelve months ago and the composition shifted considerably due to M&amp;A and corporate
actions. There were six companies falling out of the top 10: three of them including Songbird Estates in the UK, Unitech Corporate
Parks in India and BHG in
8
Dear Shareholders:
 
® 
 
At October 31, 2015, the Fund’s net asset
value had increased to $22.00 from $21.29 twelve months prior. During this timeframe, the Fund paid four quarterly distributions
of $0.1875 per share totaling $0.75 per share for the fiscal year. Since its inception at $10.00 per share on December 29, 1998
through October 31, 2015, the Fund has delivered an annualized total return to shareholders of 11.39% which includes cumulative
distributions of $16.98. The performance chart on page 13 presents the Fund’s returns for the latest one-year, three-year,
five year, ten-year, and since inception periods.
 
In our view, the most impactful factors influencing
returns for REIT securities during the fiscal year, consistent with the last few years, were (i) the level and direction of long
term interest rates, specifically that of the Ten-Year US Treasury obligation; (ii) continued improvement in real estate property
fundamentals nationally with particular strength in those coastal markets with notably higher than average job growth trends; and
(iii) steady demand by both domestic and foreign capital for direct property investment in the private market and for debt investments
in both the private and public markets.
 
The Ten-Year Treasury rate remained historically
low during the fiscal year, even decreasing 0.20% over the twelve-month period to 2.14% as of October 31, 2015, and providing,
in our view, a positive tailwind for public real estate securities valuations. Yet fluctuations in that rate – a fiscal year
low of 1.64% on January 30th to a high of 2.49% on June 10th – caused above average volatility in REIT pricing, in our opinion,
as investors remained concerned that significant rate increases would negatively impact underlying real estate valuations. In contrast
to public market participants, private capital remained seemingly more confident in the continuance of economic growth and the
real estate cycle; in the relative stability of US real estate cash flow prospects; and
in the ability to generate attractive returns
on equity relative to other alternative investments. That conviction in ongoing improvement in real estate operating fundamentals
has, in our opinion, increased transactional and merger and acquisition activity including privatizations of REIT entities perceived
to be trading at discounts to their underlying net asset value. We believe this trend could continue and even accelerate in the
upcoming year if US economic growth continues at a slow and steady pace, long term interest rates remain historically subdued,
and discounts to private market valuations continue in REIT securities pricing.
 
For the Fund, the top contributors to its positive
performance during the latest fiscal year included companies within the self-storage and apartment sectors as well as those within
the segment of the regional mall category with the highest tenant sales productivity. Self-storage companies, including the Fund’s
third largest holding, Public Storage, a national owner and developer of storage facilities, produced strong yearly growth in earnings
from occupancy and rental increases and were rewarded with some of the top total returns within the REIT group. Apartment companies,
including Essex Properties Trust, Equity Residential, and AvalonBay Communities, all top ten holdings for the Fund, demonstrated
continued strength in achieving high occupancies and rates of rental increases, particularly in the western coastal markets from
southern California to Seattle, Washington. Within the regional mall sector, there was a definitive bifurcation of fundamental
operating performance and stock price appreciation between Class A malls achieving higher rents, sales productivity, and occupancies
and their Class B brethren who struggled to maintain stable tenancy levels in the face of higher than average retailer bankruptcies
and store closures during the period. Both retail landlords, Simon Property Group and The Macerich Company, whom Simon unsuccessfully
targeted as an acquisition earlier in the year, produced above average returns for the Fund. Relative to the overall return of
the RMS Index, some of the greatest positive attribution was produced by our overweight positions in CoreSite Realty, a data center
owner and developer who experienced strong demand from network service, cloud, and information technology providers; Home Properties,
an apartment properties owner that was acquired by a private equity entity; the aforementioned Macerich; and by our underweight
position in the healthcare REIT, HCP, Inc.
15
Dear Shareholders:
 
We present below the annual results for the Alpine
Emerging Markets Real Estate Fund. For the period ended October 31, 2015, the closing NAV was $14.54 per share, representing a
total return of -12.59% for the period. The Fund’s benchmark index, the FTSE EPRA/NAREIT Emerging Index returned -8.83% adjusted
in U.S. dollar terms over the same period. Over that same time frame the MSCI Emerging Markets Index finished with a total return
of -14.53% and the S&amp;P Developed Ex-U.S. Property Index returned 1.01%. All references in this letter to the Fund’s performance
relate to the performance of the Fund’s Institutional Class.
 
Performance Drivers
 
Broadly speaking, Emerging Market (EM) real estate
equity performance for the year was a story of contrasts with strength in the first half of the year giving way to heightened volatility
in the second half. The broad themes driving the momentum of returns were shifting expectations for the timing and pace of Fed
policy, questions surrounding China and its ability to manage its economy through a pronounced cyclical downturn were at the epicenter
of concerns, as well as the overall slow-motion global recovery. Indeed, the International Monetary Fund (IMF) once again revised
its growth outlook for the global economy from 3.8% to 3.1% and for EMs from 5.0% to 4.0%, though its projection for 2016 currently
stands at 4.5%. Macro market gyrations seized many of the headlines over the past twelve months – whether it was sharp commodity
price fluctuations, currency market agitation, or global bond markets and the signaling of negative yields – and exacerbated
volatility across assets. During the first half of the period markets took a constructive view of these variables and as such the
risk on trade was firmly established. Adding to this was the Chinese government’s use of moral suasion to encourage equity
market speculation seemingly as a means for promoting mainland initial public offering (IPO) activity and acting as a pressure
valve for over-indebted corporates. The bubble in the China A-share market that followed shortly thereafter was the inflection
point for the risk trade across EMs. Downward pressure on developing markets evolved into indiscriminate selling in August as China
embarked on a managed depreciation of its currency, which was communicated poorly and subsequently sent EMs into a tailspin over
fears of deflationary feedback loops. Concerns were amplified by foreign exchange (FX) volatility (USD strength), persistent commodity
weakness
and a seemingly endless list of geopolitical
flare ups including Greece, Turkey and Russia as well as the constantly evolving situation in the Middle East. As a result, EM
mutual funds experienced combined portfolio outflows of $45bn in August and September, which greatly exceeded the amount seen post
the 2013 taper tantrum.
 
Portfolio Analysis
 
Stock selection was the dominant factor in the
overall performance of the Fund during the period. Stock picking in China and Hong Kong along with the underweight allocation to
South Africa and the overweight in Egypt and Mexico were the most significant detractors from relative performance. The underperformance
in China was due largely to the strong performance of Evergrande Real Estate, which the Fund was underweight. The underweight allocation
to Brazil and Malaysia as well as the weighting in Argentina and Spain provided the largest positive contributions to relative
performance.
 
At year end the top 10 positions accounted for
42.34% of the portfolio versus 42.42% a year ago. The composition shifted slightly with Dalian Wanda and Evergrande replacing BR
Malls and Shimao. During the twelve-month period under review, the aggregate weightings in Brazil and Thailand were pared back
significantly due to a deteriorating macroeconomic position in the former and political unrest and weakening domestic consumption
trends in the latter. The exposure to China/HK was increased due to growing focus on policy tools to reinvigorate the economy and
clear evidence of a volume recovery in Tier 1 residential markets. The large underweight in South Africa was reduced, yet the Fund
maintained a significant underweight to the market due to a weak macro outlook and instability in the currency as well as the underweight
to Malaysia due to geopolitical risk and deteriorating macro on the back of export weakness and oil prices. Additionally, the Fund
hedged its currency exposure to the Euro in the first half of the fiscal year. The currency hedging mitigated some of the negative
impact of currency in the portfolio.
 
The top five contributors to the Fund’s
absolute performance over the period under discussion based on contribution to total return were China Overseas Land, China Vanke,
China Resources Land, SM Prime, and Evergrande.
 
21
Dear Shareholders:
 
For the fiscal year ended October 31, 2015, the
Alpine Global Infrastructure Fund reported a -7.90% total return versus the S&amp;P Global Infrastructure Index, which had a total
return of -5.54%. All references in this letter to the Fund’s performance relate to the performance of the Fund’s Institutional
Class.
 
Performance Drivers
 
® 
 
On a relative basis, the Fund outperformed in
the energy sector due to its underweighting in the sector and the stocks within the sector outperforming the S&amp;P Global Infrastructure
Index. The energy sector was the worst performing sector in the index as MLPs declined. This was due to the drop in oil and natural
gas prices and the market’s anticipation of an increase in interest rates by the Federal Reserve Bank. The Alerian MLP Index
declined 30.08% over the fiscal year. The Fund underperformed in the transportation sector due to stock selection. The overweight
in railroads hurt the performance as rail volumes declined during 2015.
 
We believe that the urbanization of emerging
market countries will be an important driver of infrastructure development and spending. During the period, the Fund’s exposure
to emerging markets continues to be
overweight versus its benchmark but we have reduced
the position to below 20% of our total assets.
 
The Fund also participated in a number of initial
public offerings (IPOs) both inside and outside of the infrastructure sector, that have contributed to the Fund’s total return.
We cannot predict how long, if at all, these opportunities will continue to exist, but to the extent we consider IPOs to be attractively
priced and available, the Fund will continue to participate in them.
 
Portfolio Analysis
 
The top five stocks contributing to the Fund’s
absolute performance for the 12-month period ended October 31, 2015, based on contribution to total return were CRRC Corp. (“CRRC”),
China Railway Construction Corp. (“CRCC”), Ferrovial (“FER”), Veolia Environment (“VIE”) and
Vinci (“DG”).
 
28
