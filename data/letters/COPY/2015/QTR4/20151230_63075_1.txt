Dear Shareholders: 
The global economy remains vulnerable as central banks continue to play a major role in supporting growth. 
Even the resilient U.S. economy has had slower growth, as a strong U.S. dollar and weak overseas markets have hurt exporters. However, robust consumer demand and a recovering housing market have aided the domestic
economy, fueled by cheap gasoline and an improving labor market. 
Chinas transition to a consumer-based, slower-growth economy has weighed on many
commodity-exporting nations. And concerns about weakness in China have eroded investor and business confidence around the world. Meanwhile, rising geopolitical concerns will weigh on the eurozone, which is still reliant on the European Central
Banks support. 
As markets have become more focused on short-term trends in recent years, we believe its important for
investors to lengthen their investment time horizon. At MFS®, we dont trade on headlines or trends; we invest for the long term. 
We believe
that this approach, coupled with the professional guidance of a financial advisor, will help you reach your investment goals. 
Respectfully, 


