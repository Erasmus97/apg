RECESSION ALTHOUGH UNLIKELY GIVEN STRONG EXPORT MARKETS RELATIVELY LOW INVENTORY LEVELS ECONOMY STILL POSSIBILITY GIVEN WEAKENED FINANCIAL STATE CONSUMER DIFFICULTY OBTAINING CREDIT THROUGHOUT UNITED STATES
SECOND QUARTER OIL PRICES INCREASED ROUGHLY NEARLY COMMODITIES NEARLY PUSHED REAL ESTATE SECOND PLACE WALL WORRY CONFRONTING INVESTORS
PARTICULARLY USEFUL GUIDES KEEP MIND TODAY TIME MARKETS AROUND GLOBE BUFFETED PROBLEMS FINANCIAL REAL ESTATE INDUSTRIES CONCERNS SLOWING ECONOMY
RECESSION TALK WIDESPREAD CONCERN GREW FALLING HOME PRICES RISING UNEMPLOYMENT DISRUPTIONS FINANCIAL MARKETS POSED SIGNIFICANT THREAT ECONOMIC GROWTH
TREASURY BOND PRICES ROSE MARKET UNDERWENT CLASSIC FLIGHT QUALITY PERIOD LEADING NEAR FAILURE BEAR STEARNS ALMOST RETURNED START YEAR LEVELS
MARKETS REMAIN VOLATILE FALLING RISK TOLERANCES DELEVERAGING MAY DEPRESS ASSET PRICES SHORT TERM EQUITY CORPORATE BOND VALUATIONS LOOK ATTRACTIVE LONGER TIME HORIZON
CONDITIONS WORSENED FIRST QUARTER FALLING PRICES MARGIN CALLS DELEVERAGING CONTINUED AUCTION RATE PREFERRED MARKET SEIZED
EXTENDED PERIOD STEADY ECONOMIC GROWTH SUSTAINED LOW UNEMPLOYMENT LOW INFLATION ECONOMY RAN DIFFICULTY DREW CLOSE
FIXED INCOME CREDIT SPREADS DIFFERENCE RATES CORPORATE GOVERNMENT BONDS WIDENED DRAMATICALLY STOCK MARKETS FELL
PIONEER IBBOTSON ASSET ALLOCATION SERIES ANNUAL REPORT MARKETS VOLATILE ECONOMIC FUNDAMENTALS MAY TURNED CORNER
