Dear
Shareholders:
 
We
are pleased to present to you the Morgan Stanley Institutional Fund Trust’s
(the “Fund”) Annual Report for the Advisory Portfolios for the year ended September 30,
2008. Our Fund currently consists of 18 portfolios. The Fund’s portfolios,
together with the portfolios of the Morgan Stanley Institutional Fund, Inc.,
provide investors with a means to help them meet specific investment needs and
to allocate their investments among equities (e.g., value and growth; small,
medium, and large capitalization) and fixed income (e.g., short, medium, and
long duration; investment grade and high yield).
 
 
Sincerely,


Dear
Shareholders:
 
We
are pleased to present to you the Morgan Stanley Institutional Fund Trust’s
(the “Fund”) Annual Report for the year ended September 30, 2008. Our Fund
currently consists of 18 portfolios. The Fund’s portfolios, together with the
portfolios of the Morgan Stanley Institutional Fund, Inc., provide
investors with a means to help them meet specific investment needs and to
allocate their investments among equities (e.g., value and growth; small,
medium, and large capitalization) and fixed income (e.g., short, medium, and
long duration; investment grade and high yield).
 
 
Sincerely,


