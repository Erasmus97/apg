Dear Shareholders: We are pleased to provide you with this overview of the performance of the Lord Abbett Emerging Markets Currency Fund and Lord Abbett Global Allocation Fund for the fiscal year ended
On this page and the following pages, we discuss the major factors that influenced performance. For detailed and more timely information about the Funds, please visit our Website at www.lordabbett.com, where you also can
access the quarterly commentaries by the Funds portfolio managers. Thank you for investing in Lord Abbett mutual funds. We value
the trust that you place in us and look forward to serving your investment needs in the years to come. Best regards,
