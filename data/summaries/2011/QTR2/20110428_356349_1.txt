CEO Dear Shareholders:
After an extended rebound in the financial markets, uncertainty returned in as investors began to question the durability of the recovery for global
economies and markets. That uncertainty led to increased risk aversion, especially as investors saw the eurozone struggle with the debt woes of many of its members. Last September, the U.S. Federal Reserve Boards promises to further loosen
monetary policy helped assuage market fears and drive asset prices off their recent lows. A combination of solid earnings and improving economic data gave an additional boost to investor sentiment. For we are cautiously optimistic that
economic growth will continue to improve and that the global economies will recover from the shocks of the past few years. We expect the pace
of recovery worldwide will be uneven and volatile and acknowledge theelevated uncertainty created by events in Japan, Europe, and the Middle East.
As always, we continue to be mindful of the many economic challenges faced at the local, national, and international levels. It is in times such as these
that we want to remind investors of the merits of maintaining a long-term view, adhering to basic investing principles such as asset allocation and diversification, and working closely with their advisors to research and identify appropriate
investment opportunities. Respectfully,

CEO Dear Shareholders:
After an extended rebound in the financial markets, uncertainty returned in as investors began to question the durability of the recovery for global
economies and markets. That uncertainty led to increased risk aversion, especially as investors saw the eurozone struggle with the debt woes of many of its members. Last September, the U.S. Federal Reserve Boards promises to further loosen
monetary policy helped assuage market fears and drive asset prices off their recent lows. A combination of solid earnings and improving economic data gave an additional boost to investor sentiment. For we are cautiously optimistic that
economic growth will continue to improve and that the global economies will recover from the shocks of the past few years. We expect the pace of recovery worldwide will be uneven and volatile and acknowledge the elevated uncertainty created by
events in Japan, Europe, and the Middle East. As always, we continue to be mindful of the many economic challenges faced at the local,
national, and international levels. It is in times such as these that we want to remind investors of the merits of maintaining a long-term view, adhering to basic investing principles such as asset allocation and diversification, and working closely
with their advisors to research and identify appropriate investment opportunities. Respectfully,

CEO Dear Shareholders:
After an extended rebound in the financial markets, uncertainty returned in as investors began to question the durability of the recovery for global
economies and markets. That uncertainty led to increased risk aversion, especially as investors saw the eurozone struggle with the debt woes of many of its members. Last September, the U.S. Federal Reserve Boards promises to further loosen
monetary policy helped assuage market fears and drive asset prices off their recent lows. A combination of solid earnings and improving economic data gave an additional boost to investor sentiment. For we are cautiously optimistic that
economic growth will continue to improve and that the global economies will recover from the shocks of the past few years. We expect the pace of recovery worldwide will be uneven and volatile and acknowledge the elevated uncertainty created by
events in Japan, Europe, and the Middle East. As always, we continue to be mindful of the many economic challenges faced at the local,
national, and international levels. It is in times such as these that we want to remind investors of the merits of maintaining a long-term view, adhering to basic investing principles such as asset allocation and diversification, and working closely
with their advisors to research and identify appropriate investment opportunities. Respectfully,
