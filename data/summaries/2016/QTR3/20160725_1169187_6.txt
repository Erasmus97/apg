Dear Investor
Strong performance in recent months helped
fixed income markets overcome weakness in the first half of the fund’s fiscal
year. The factors behind the improved performance were mixed, however, as
disparate economic signals drove aggressive monetary stimulus and falling
yields, on the one hand, and a renewed risk appetite, on the other. We are
pleased to report that our fund navigated these crosscurrents relatively well in
the six months since our last report, outperforming its benchmark and peer
group. We suspect more volatility lies ahead in 2016, however, making individual
security selection especially important for bond investors.
