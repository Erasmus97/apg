INTERNATIONAL ASSET CLASSES MIXED RESULTS YEAR EMERGING MARKET STOCKS BONDS EXPERIENCING BIGGEST DRAWDOWNS
LOOKING AHEAD MEDIUM TERM TRAJECTORY INTEREST RATES LIKELY HIGHER PATH GET WILL LIKELY LONG ONE FEDERAL RESERVE INDICATED FUTURE FEDERAL FUNDS RATE WILL 375% DECEMBER 2016 LIKELY OPTIMISTIC
MARKET STRENGTH FOURTH QUARTER HELPED AMP 500 FINISH YEAR SMALL POSITIVE TOTAL RETURN
SINCE ECONOMIC DATA MIXED BAG STRONG JOB GROWTH LOW UNEMPLOYMENT LOW INFLATION MODERATE ECONOMIC GROWTH
CALM SHATTERED LAST TWO WEEKS AUGUST AMP 500 DECLINED 11% AMID UNCERTAINTY REGARDING TIMING FEDERAL RESERVE’S FED NEXT MOVE RUMORS ECONOMIC SLOWDOWN CHINA SURPRISING DEVALUATION YUAN
INCREASES INTEREST RATES ACROSS MATURITIES CULMINATED DECEMBER FED RAISING TARGETED FED FUNDS RATE 25% FIRST RATE CHANGE SINCE 2008 FIRST RATE INCREASE SINCE 2006
BACKDROP LOW INTEREST RATES LOW INFLATION MODEST ECONOMIC GROWTH
CONDITIONS DOLLAR CONTINUED HOLD VALUE VERSUS MAJOR WORLD CURRENCIES
FIRST HALF 2015 RELATIVELY UNEVENTFUL EQUITY MARKETS TRUDGED MODESTLY HIGHER DESPITE ECONOMIC INDICATORS SIGNALING MODERATE EXPANSION
COMMODITIES WORST PERFORMING ASSET CLASS 2015 DECLINING ALMOST 25% OIL DROPPING 28 5%
