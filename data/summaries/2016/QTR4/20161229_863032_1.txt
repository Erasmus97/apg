Dear Shareholders: 
Despite June’s unexpected vote by the United Kingdom to leave the European Union and the surprising result in the U.S. presidential election, most markets
proved resilient 
post-Brexit
Emblematic of the sluggish global growth environment is a pronounced slowdown in the growth of global trade. Despite the slowdown, emerging market equities have
held up well, withstanding geopolitical shocks such as an attempted coup in Turkey and the impeachment and removal of the president of Brazil. The U.S. Federal Reserve’s go-slow approach to rate hikes and economic stimulus abroad have helped
support markets. 
At MFS®, we believe in a patient, long-term approach to investing. Viewing investments with a long lens makes
it possible to filter out short-term market noise and focus on achieving solid risk-adjusted returns over a full market cycle. 
In our view, such an
approach, along with the professional guidance of a financial advisor, will help you reach your investment objectives. 
Respectfully, 


Dear Shareholders: 
Despite June’s unexpected vote by the United Kingdom to leave the European Union and the surprising result in the U.S. presidential election, most markets
proved resilient 
post-Brexit
Emblematic of the sluggish global growth environment is a pronounced slowdown in the growth of global trade. Despite the slowdown, emerging market equities have
held up well, withstanding geopolitical shocks such as an attempted coup in Turkey and the impeachment and removal of the president of Brazil. The U.S. Federal Reserve’s go-slow approach to rate hikes and economic stimulus abroad have helped
support markets. 
At MFS®, we believe in a patient, long-term approach to investing. Viewing investments with a long lens makes
it possible to filter out short-term market noise and focus on achieving solid risk-adjusted returns over a full market cycle. 
In our view, such an
approach, along with the professional guidance of a financial advisor, will help you reach your investment objectives. 
Respectfully, 


Dear Shareholders: 
Despite June’s unexpected vote by the United Kingdom to leave the European Union and the surprising result in the U.S. presidential election, most markets
proved resilient 
post-Brexit
Emblematic of the sluggish global growth environment is a pronounced slowdown in the growth of global trade. Despite the slowdown, emerging market equities have
held up well, withstanding geopolitical shocks such as an attempted coup in Turkey and the impeachment and removal of the president of Brazil. The U.S. Federal Reserve’s go-slow approach to rate hikes and economic stimulus abroad have helped
support markets. 
At MFS®, we believe in a patient, long-term approach to investing. Viewing investments with a long lens makes
it possible to filter out short-term market noise and focus on achieving solid risk-adjusted returns over a full market cycle. 
In our view, such an
approach, along with the professional guidance of a financial advisor, will help you reach your investment objectives. 
Respectfully, 


