Dear Fellow Shareholder,




As stewards of our customers' savings, the management team and Directors of Davis New York Venture Fund recognize the importance of candid, thorough, and regular communication with our shareholders. In our Annual and Semi-Annual Reports we include all of the required quantitative information such as audited financial statements, detailed footnotes, performance reports, fund holdings, and performance attribution. Also included is a list of positions opened and closed.




In addition, we produce a Manager Commentary. In this commentary, we give a more qualitative perspective on fund performance, discuss our thoughts on individual holdings, and share our investment outlook. You may obtain a copy of the current Manager Commentary either on the Fund's website at www.davisfunds.com or by calling




We thank you for your continued trust. We will do our best to earn it in the years ahead.







Sincerely,
