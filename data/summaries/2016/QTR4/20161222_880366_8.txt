Dear Shareholder,

We are pleased to provide the annual report of QS Global Equity Fund for the twelve-month reporting period ended October 31, 2016. Please read
on for a detailed look at prevailing economic and market conditions during the Fund’s reporting period and to learn how those conditions have affected Fund performance. 
As always, we remain committed to providing you with excellent service and a full spectrum of investment choices. We also remain committed to supplementing the support you receive from your financial advisor. One
way we accomplish this is through our website, www.leggmason.com. Here you can gain immediate access to market and investment information, including: 
 
Fund prices and performance, 
 
Market insights and commentaries from our portfolio managers, and 
 
A host of educational resources. 
We look
forward to helping you meet your financial goals. 
Sincerely, 


