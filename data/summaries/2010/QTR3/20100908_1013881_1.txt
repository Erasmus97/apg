POTENTIAL IMPACT STEPS CONSIDERED MANY GOVERNMENTS COUNTERACT EXTRAORDINARY GOVERNMENTAL SPENDING CREDIT EXPANSION DEAL RECENT FINANCIAL ECONOMIC CRISIS INJECTING UNCERTAINTY GLOBAL FINANCIAL MARKETS
IMPLICATIONS FUTURE TAX RATES GOVERNMENT SPENDING INTEREST RATES PACE ECONOMIC RECOVERY LEADING ECONOMIES EXTREMELY DIFFICULT PREDICT PRESENT TIME
LONG TERM HEALTH GLOBAL ECONOMY DEPENDS RESTORING MEASURE FISCAL DISCIPLINE AROUND WORLD SINCE CORRECTIVE STEPS REQUIRE ECONOMIC PAIN SURPRISING GOVERNMENTS RELUCTANT UNDERTAKE
REASON NUVEEN’S INVESTMENT MANAGEMENT TEAMS WORKING HARD BALANCE RETURN RISK BUILDING WELL DIVERSIFIED PORTFOLIOS AMONG STRATEGIES
ECONOMIC ENVIRONMENT FUND OPERATES REFLECTS CONTINUING UNEVEN ECONOMIC RECOVERY
NEAR TERM GOVERNMENTS REMAIN COMMITTED FURTHERING ECONOMIC RECOVERY REALIZING MEANINGFUL REDUCTION NATIONAL UNEMPLOYMENT RATES
MAJOR INDUSTRIAL COUNTRIES EXPERIENCING STEADY COMPARATIVELY LOW LEVELS ECONOMIC GROWTH EMERGING MARKET COUNTRIES SEEING RESUMPTION RELATIVELY STRONG ECONOMIC EXPANSION
ENVIRONMENT PRODUCE CONTINUED ECONOMIC GROWTH CONSEQUENTLY ATTRACTIVE INVESTMENT OPPORTUNITIES
ALWAYS ALSO ENCOURAGE CONTACT FINANCIAL CONSULTANT QUESTIONS NUVEEN FUND INVESTMENT
LONGER TERM LARGER UNCERTAINTY MENTIONED EARLIER CARRIES RISK UNEXPECTED POTHOLES ROAD SUSTAINED RECOVERY
