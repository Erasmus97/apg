Dear Shareholders:





We invite you to take a few minutes to review the results of the
fiscal year ended





This report includes comparative performance graphs and tables,
portfolio commentaries, complete listings of portfolio holdings,
and additional fund information. We hope you will find this
helpful in monitoring your investment portfolio.





Also, through our website, FirstAmericanFunds.com, we provide
quarterly performance fact sheets on all First American Funds,
the economic outlook as viewed by our senior investment
officers, and other information about fund investments and
portfolio strategies.





As announced on U.S.Bancorp has
entered into an agreement to sell a portion of the asset
management business of FAF Advisors, the funds advisor, to
Nuveen Investments. Included in the sale will be that part of
FAF Advisors business that advises the funds. Subject to
the approval of the funds board of directors and
shareholders, along with other conditions related to the closing
of the sale, Nuveen Asset Management, a subsidiary of Nuveen
Investments, will become the advisor to the funds. The sale is
currently expected to close by the end of There will be no
change in the funds investment objectives or policies as a
result of the transaction.





Please contact your financial professional if you have questions
about First American Funds or contact First American Investor
Services at





We appreciate your investment with First American Funds and look
forward to serving your financial needs in the future.





Sincerely,

Dear Shareholders:





We invite you to take a few minutes to review the results of the
fiscal year ended





This report includes portfolio commentaries, comparative
performance graphs and tables, complete listings of portfolio
holdings, and additional fund information. We hope you will find
this helpful in monitoring your investment portfolio.





Also, through our website, FirstAmericanFunds.com, we provide
quarterly performance fact sheets on all First American Funds,
the economic outlook as viewed by our senior investment
officers, and other information about fund investments and
portfolio strategies.





As announced on U.S. Bancorp has entered
into an agreement to sell a portion of the asset management
business of FAF Advisors, the funds advisor, to Nuveen
Investments. Included in the sale will be that part of FAF
Advisors business that advises the funds. Subject to the
approval of the funds board of directors and shareholders,
along with other conditions related to the closing of the sale,
Nuveen Asset Management, a subsidiary of Nuveen Investments,
will become the advisor to the funds. The sale is currently
expected to close by the end of There will be no change in
the funds investment objectives or policies as a result of
the transaction.





Please contact your financial professional if you have questions
about FirstAmerican Funds or contact First American
Investor Services at





We appreciate your investment with First American Funds and look
forward to serving your financial needs in the future.





Sincerely,
