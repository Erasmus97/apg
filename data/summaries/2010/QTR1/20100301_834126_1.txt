Dear Shareholder: 
 
Recently we announced the renaming of JennisonDryden, Prudential Financial’s mutual fund family, to Prudential Investments. As a result of this change, each of our funds has been renamed to feature
“Prudential” as part of its new name. The name of your fund has changed from the Dryden Short-Term Corporate Bond Fund to the Prudential Short-Term Corporate Bond Fund, Inc. 
 
While the name of your fund has changed, its investment objectives and portfolio management team remain the same. No action is required
on your part. If you participate in an automatic investment plan, your account continues to be invested in the Fund under its new name. 
 
Featuring the Prudential name in our funds creates an immediate connection to the experience and heritage of Prudential, a name recognized by millions for helping
people grow and protect their wealth. 
 
On the following pages, you will find
your fund’s annual report, including an analysis of its performance over its fiscal year in addition to other data. If you have questions about your fund or the renaming of our mutual fund family, please contact your financial professional or
visit our website at www.prudentialfunds.com. 
 
Sincerely, 


