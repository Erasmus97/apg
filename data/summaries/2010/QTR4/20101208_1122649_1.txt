Dear Shareholder,




We are pleased to provide the annual report for the Hillman Funds for the fiscal year ended




Navigating todays investment waters remains challenging for many investors as they seek to preserve capital and earn returns sufficient to meet their long-term objectives.Those who follow the financial media have been bombarded with rhetoric surrounding the relative merits of differing asset classes and investment approaches.We have enclosed the attached commentary to remind our shareholders of Hillman Capital Managements mission and to provide perspective on current market conditions.




On behalf of the team at Hillman Capital Management, I thank you for your ongoing confidence.It is our hope that we may continue to serve you throughout the years to come.




Sincerely,
