TOTAL INVESTMENT RETURNS FISCAL YEAR ENDED MARCH BELIEVE CONTINUED UPWARD MOVEMENT MARKETS ECONOMIES WORLDWIDE HIGHLY DEPENDENT GOVERNMENTS GETTING FINANCIAL BALANCE SHEETS ORDER
MARCH NET ASSETS GOVERNMENT STREET MID CAP FUND NET ASSET VALUE PER SHARE TURNOVER RATE PREVIOUS TWELVE MONTHS TOTAL NUMBER HOLDINGS MARCH NET EXPENSE RATIO FUND ALABAMA TAX FREE BOND FUND FIXED INCOME MARKETS GENERALLY RETURNED SOUND FOOTING PAST TWELVE MONTHS INVESTORS GAINED CONFIDENCE WORST CREDIT CRISIS PASSED
FURTHERMORE ENCOURAGED PERFORMANCE FAVORABLE PAST THREE YEARS EXTENT REFLECTED MINI MARKET CYCLE MODEST GAINS COLLAPSE SURGE INFORMATION TECHNOLOGY SECTOR MAJOR CONTRIBUTOR MARKET GAINS YEAR
FACT MANY STOCKS APPEAR OFFER BETTER DEAL FIXED INCOME INVESTMENTS DUE APPEALING FREE CASH FLOWS DIVIDENDS
HEADING NEXT FISCAL YEAR REMAIN MODESTLY OVERWEIGHT INFORMATION TECHNOLOGY GIVEN GOOD GROWTH PROSPECTS SOLID BALANCE SHEETS INTERNATIONAL EXPOSURE REASONABLE VALUATIONS
BIGGEST CONTRIBUTORS FUNDS PERFORMANCE PAST YEAR PIONEER NATURAL RESOURCES CREE INC COGNIZANT TECHNOLOGY SOLUTIONS LONGER TERM PERFORMANCE FUND RELATIVE AMP GOOD FUND THREE YEAR ANNUALIZED RETURN COMPARED AMP FIVE YEAR ANNUALIZED RETURN GOVERNMENT STREET MID CAP FUND VERSUS AMP LARGE CAP STOCKS MEASURED AMP INDEX RETURNED PAST THREE YEARS PAST FIVE YEARS
INVESTORS CURRENTLY CONTENT FINANCE DEBT LOW RATES REMAINS SEEN INVESTOR APPETITE WILL GOING FORWARD WILL MEAN FUTURE RATES INFLATION
MARCH FUND CASH BALANCE EXCESS HELPED MARKET CORRECTION HURT STOCKS TOOK CASH INVESTED THROUGHOUT CO URSE YEAR DRAG PERFORMANCE
FUNDS ALLOCATION CONSUMER DISCRETIONARY STOCKS MARCH WELL ALLOCATION AMP DISCREPANCY NARROWED THROUGHOUT SINCE CONSUMER DISCRETIONARY THIRD BEST PERFORMING SECTOR AMP YEAR ENDED MARCH GOVERNMENT STREET MID CAP FUNDS UNDERWEIGHTING HURT RELATIVE PERFORMANCE
BELIEVE COMPANY GROW EARNINGS PER SHARE EPS LOW MID DOUBLE DIGIT PACE USING STRONG FREE CASH FLOW FCF PAY DEBT MAKE ACQUISITIONS BUY BACK STOCK
