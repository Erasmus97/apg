Dear Shareholder: 
 
On the following pages, youll find your annual report for the Jennison Blend Fund, which includes
performance data, an analysis of Fund performance, and a listing of its holdings at period-end. The Funds fiscal year has changed from a reporting period that ends December 31 to one that ends August 31. This change should have no
impact on the way the Fund is managed. Shareholders will receive future annual and semiannual reports on the new fiscal year-end schedule. 
 
Mutual fund prices and returns will rise or fall over time, and asset managers tend to have periods when they perform better or worse than their long-term average. The best
measures of a mutual funds quality are its return compared to that of similar investments and the variability of its return over the long term. We recommend that you review your portfolio regularly with your financial adviser. 
 
Sincerely, 


