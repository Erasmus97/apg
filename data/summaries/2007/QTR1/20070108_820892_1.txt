Dear Shareholders:









We invite you to take a few minutes to review the results of the
fiscal year ended






This report includes comparative performance graphs and tables,
portfolio commentaries, complete listings of portfolio holdings,
and additional fund information. We hope you will find this
helpful in monitoring your investment portfolio.



Also, through our website, firstamericanfunds.com, we provide
quarterly performance fact sheets on all First American Funds,
the economic outlook as viewed by our senior investment
officers, and other information about fund investments and
portfolio strategies.



Please contact your financial professional if you have questions
about First American Funds or contact First American Investor
Services at



We appreciate your investment with First American Funds and look
forward to serving your financial needs in the future.



Sincerely,

Dear Shareholders:









We invite you to take a few minutes to review the results of the
fiscal year ended






This report includes comparative performance graphs and tables,
portfolio commentaries, complete listings of portfolio holdings,
and additional fund information. We hope you will find this
helpful in monitoring your investment portfolio.



Also, through our website, firstamericanfunds.com, we provide
quarterly performance fact sheets on all First American Funds,
the economic outlook as viewed by our senior investment
officers, and other information about fund investments and
portfolio strategies.



Please contact your financial professional if you have questions
about First American Funds or contact First American Investor
Services at



We appreciate your investment with First American Funds and look
forward to serving your financial needs in the future.



Sincerely,
