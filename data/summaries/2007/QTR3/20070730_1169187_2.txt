Dear Investor


• The Institutional Core Plus Fund outperformed its Lipper peer group average and the Lehman Brothers U.S. Aggregate Index for the past six and 12 months. 
• The funds emphasis on higher-yielding, lower-quality issues and foreign bonds boosted returns over the period, while our underweight in mortgages detracted. 
• We intend to maintain our focus on lower-quality issues, but alongside an emphasis on diversification, which should minimize our exposure to company-specific problems.
Market Environment
While economic growth was generally subdued over the past six months, the negative reaction of the bond marketand the enthusiastic response of the equity marketsuggested that most investors believed the slowdown was only temporary. Bond prices fell and yields rose substantially around the turn of the year, when several pieces of data indicated job growth was strong, consumers were buoyant, and even the troubled housing sector might be on the verge of righting itself, as suggested by an unanticipated rise in home sales. 
Bonds rallied in the first quarter of 2007 when optimism about the economy began to appear misplaced, however. Further signals from the housing sector revealed that the housing crunch was far from over. Starts of new homes declined sharply, home prices began to fall on a nationwide basis, and, in March, existing home sales saw their biggest monthly drop in nearly two decades. Indeed, a new set of housing worries emerged when default rates on subprime loans began to climb dramatically, which 
