Dear Green Century Funds Shareholder: 
Whether you are a new investor with Green Centuryor one who has been with us over the decadeswe appreciate the trust that you have placed in us. 
Since our last update, Green Century has reached some important milestones that we want to share. 
In the year ended July 31, 2015, the Green Century Funds have gathered more net new assets than at any time in our history, reaching over $315
million in assets under management. Thank you for investing with Green Century, referring friends and sharing our online updates. (Please sign up for our e-newsletter at www.greencentury.com if you do not yet receive it). 
In addition to seeking competitive returns, Green Century provides you three ways to make an impact, getting more from your investments: 
Investment Strategy 
Green Century has long followed an investment strategy rooted in
the belief that companies that are environmentally and socially responsible could avoid potential business risks and costly litigation while enjoying competitive advantages such as access to expanding markets. In the last several years, a growing
body of evidence has provided additional support for this approach. 
Employing this strategy results in Green Century
Funds shareholders supporting companies like Johnson Controls,
www.greencentury.com/fossil-fuel-free
Non-Profit Ownership 
 100% of the net profits that Green Century earns
 
 
Green Century is the

 
 
 belong to its non-profit founders
 belong to its non-profit founders
 


