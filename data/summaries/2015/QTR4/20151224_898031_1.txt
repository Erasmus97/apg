Dear Shareholders: We
are pleased to provide you with this overview of the performance of the Funds for the fiscal year ended October On this
page and the following pages, we discuss the major factors that influenced fiscal year performance. For detailed and more timely
information about the Funds, please visit our website at www.lordabbett.com, where you also can access the quarterly commentaries
that provide updates on each Fund&rsquo;s performance and other portfolio related updates.



Thank
you for investing in Lord Abbett mutual funds. We value the trust that you place in us and look forward to serving your investment
needs in the years to come.



Best regards,

Dear Shareholders: We are pleased to provide you with
this overview of the performance of the Funds for the fiscal year ended October On this page and the following pages,
we discuss the major factors that influenced fiscal year performance. For detailed and more timely information about the Funds,
please visit our website at www.lordabbett.com, where you also can access the quarterly commentaries that provide updates on each
Fund&rsquo;s performance and other portfolio related updates.

Thank you for investing in Lord Abbett mutual
funds. We value the trust that you place in us and look forward to serving your investment needs in the years to come.



Best regards,
