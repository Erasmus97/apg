Dear Shareholder: 
2014 GuideStone Funds Annual Report
In 2014 GuideStone Funds was
recognized, for the third consecutive year, by the Lipper Fund Awards. Ranking No. 1 out of 57 eligible funds, the Extended-Duration Bond Fund was recognized as the Best Fund Over 3 Years, and ranking No. 1 out of 51 eligible funds, as the
Best Fund Over 5 Years in the Corporate Debt A-Rated Funds category for the funds risk-adjusted total return ending November 30, 2013. 
GuideStone Funds were made available to an expanded audience in 2014. 
www.GuideStoneFunds.com/Disclosures 
 
