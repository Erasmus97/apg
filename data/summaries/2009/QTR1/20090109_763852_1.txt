Dear Shareholder:
The storm that overcame the global financial markets a few months
ago has continued unabated. The fallout from this economic crisis has left few stones unturned: bank lending has virtually stalled and extreme market
volatility has weakened all of the major indexes as well as the balance sheets of financial services firms.
