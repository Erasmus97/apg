Dear Shareholder:

 
On December 11, 2009, the Board of Directors approved the renaming of
JennisonDryden, Prudential Financial’s mutual fund family, to Prudential Investments, effective on or about February 16, 2010. As a result of this change, each of our Funds will be renamed to feature “Prudential” as part of its new
name. The name of your Fund will change from Dryden Large-Cap Core Equity Fund to Prudential Large-Cap Core Equity Fund. 
 
While the name of your Fund will change, its investment objectives and portfolio management team will remain the same. No action is required on your part. If you
participate in an automatic investment plan, your account will continue to be invested in the Fund under its new name. 
 
Featuring the Prudential name in our Funds will create an immediate connection to the experience and heritage of Prudential, a name recognized by millions for helping
people grow and protect their wealth. 
 
On the following pages, you will find
your Fund’s annual report, including an analysis of its performance over the fiscal year in addition to other data. If you have questions about this information or the renaming of our mutual fund family, please contact your financial
professional or visit our website at www.jennisondryden.com. 
 
Sincerely,



