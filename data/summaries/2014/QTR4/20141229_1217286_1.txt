Dear Shareholder, 
What began as a period of moderate economic growth supported by the concerted efforts of central banks to provide stimulus, ended with a clear divergence between the U.S. and the rest of the world in terms
of both policy and growth. During the twelve months ended October 31, 2014, the U.S. Federal Reserve responded to improving employment and consumer spending by steadily winding down and ending its $85 billion-a-month program of bond purchases,
and signaled it may start raising interest rates in mid-2015. In June, the European Central Bank (ECB) took unprecedented steps to prevent a deflationary spiral and in October, the Bank of Japan shocked financial markets by unveiling a massive
stimulus program to restart economic growth. 
 
Dear Shareholder, 
What began as a period of moderate economic growth supported by the concerted efforts of central banks to provide stimulus, ended with a clear divergence between the U.S. and the rest of the world in terms
of both policy and growth. During the twelve months ended October 31, 2014, the U.S. Federal Reserve responded to improving employment and consumer spending by steadily winding down and ending its $85 billion-a-month program of bond purchases, and
signaled it may start raising interest rates in mid-2015. In June, the European Central Bank (ECB) took unprecedented steps to prevent a deflationary spiral and in October, the Bank of Japan shocked financial markets by unveiling a massive stimulus
program to restart economic growth. 
 
Dear Shareholder, 
What began as a period of moderate economic growth supported by the concerted efforts of central banks to provide stimulus, ended with a clear divergence between the U.S. and the rest of the world in terms
of both policy and growth. During the twelve months ended October 31, 2014, the U.S. Federal Reserve responded to improving employment and consumer spending by steadily winding down and ending its $85 billion-a-month program of bond purchases,
and signaled it may start raising interest rates in mid-2015. In June, the European Central Bank (ECB) took unprecedented steps to prevent a deflationary spiral and in October, the Bank of Japan shocked financial markets by unveiling a massive
stimulus program to restart economic growth. 
 
Dear Shareholder, 
What began as a period of moderate economic growth supported by the concerted efforts of central banks to provide stimulus, ended with a clear divergence between the U.S. and the rest of the world in terms
of both policy and growth. During the twelve months ended October 31, 2014, the U.S. Federal Reserve responded to improving employment and consumer spending by steadily winding down and ending its $85 billion-a-month program of bond purchases,
and signaled it may start raising interest rates in mid-2015. In June, the European Central Bank (ECB) took unprecedented steps to prevent a deflationary spiral and in October, the Bank of Japan shocked financial markets by unveiling a massive
stimulus program to restart economic growth. 
 


Dear Shareholder, 
What began as a period of moderate economic growth supported by the concerted efforts of central banks to provide stimulus, ended with a clear divergence between the U.S. and the rest of the world in terms
of both policy and growth. During the twelve months ended October 31, 2014, the U.S. Federal Reserve responded to improving employment and consumer spending by steadily winding down and ending its $85 billion-a-month program of bond purchases,
and signaled it may start raising interest rates in mid-2015. In June, the European Central Bank (ECB) took unprecedented steps to prevent a deflationary spiral and in October, the Bank of Japan shocked financial markets by unveiling a massive
stimulus program to restart economic growth. 
 
Dear Shareholder, 
What began as a period of moderate economic growth supported by the concerted efforts of central banks to provide stimulus, ended with a clear divergence between the U.S. and the rest of the world in terms
of both policy and growth. During the twelve months ended October 31, 2014, the U.S. Federal Reserve responded to improving employment and consumer spending by steadily winding down and ending its $85 billion-a-month program of bond purchases, and
signaled it may start raising interest rates in mid-2015. In June, the European Central Bank (ECB) took unprecedented steps to prevent a deflationary spiral and in October, the Bank of Japan shocked financial markets by unveiling a massive stimulus
program to restart economic growth. 
 
Dear Shareholder, 
What began as a period of moderate economic growth supported by the concerted efforts of central banks to provide stimulus, ended with a clear divergence between the U.S. and the rest of the world in terms
of both policy and growth. During the twelve months ended October 31, 2014, the U.S. Federal Reserve responded to improving employment and consumer spending by steadily winding down and ending its $85 billion-a-month program of bond purchases,
and signaled it may start raising interest rates in mid-2015. In June, the European Central Bank (ECB) took unprecedented steps to prevent a deflationary spiral and in October, the Bank of Japan shocked financial markets by unveiling a massive
stimulus program to restart economic growth. 
 
Dear Shareholder, 
What began as a period of moderate economic growth supported by the concerted efforts of central banks to provide stimulus, ended with a clear divergence between the U.S. and the rest of the world in terms
of both policy and growth. During the twelve months ended October 31, 2014, the U.S. Federal Reserve responded to improving employment and consumer spending by steadily winding down and ending its $85 billion-a-month program of bond purchases, and
signaled it may start raising interest rates in mid-2015. In June, the European Central Bank (ECB) took unprecedented steps to prevent a deflationary spiral and in October, the Bank of Japan shocked financial markets by unveiling a massive stimulus
program to restart economic growth. 
 
Dear Shareholder, 
What began as a period of moderate economic growth supported by the concerted efforts of central banks to provide stimulus, ended with a clear divergence between the
U.S. and the rest of the world in terms of both policy and growth. During the twelve months ended October 31, 2014, the U.S. Federal Reserve responded to improving employment and consumer spending by steadily winding down and ending its $85
billion-a-month program of bond purchases, and signaled it may start raising interest rates in mid-2015. In June, the European Central Bank (ECB) took unprecedented steps to prevent a deflationary spiral and in October, the Bank of Japan shocked
financial markets by unveiling a massive stimulus program to restart economic growth. 
During the latter part of the twelve month period, U.S. markets were
supported by a 25% drop in global oil prices from June to October, 3.9% growth in third quarter U.S. gross domestic product (GDP), the strongest employment gains since 1999, and rising corporate earnings. From May through June, equity prices touched
record highs and volatility in U.S. markets retreated to lows not consistently seen since 2007. Notably, U.S. equities fell to six-month lows in mid October only to rebound 8.4%, driving the Standard &amp; Poor’s 500 Index to a new closing
high of 2,018.05 points on the final day of the reporting period. 
Following a brief sell-off in emerging markets in late 2013, much of the reporting period was
marked by low growth and low inflation globally, and low volatility in financial markets. In the European Union (EU), positive economic growth returned, and Ireland became the first nation to exit its EU bailout program. However, unemployment in the
EU remained exceptionally high throughout the reporting period, and manufacturing activity across the euro zone slowed in the latter part of the reporting period. 
In June, the ECB cut the EU deposit rate to negative 0.1% from 0.0% in a bid to push banks to extend lending by effectively charging them for parking excess cash with
the central bank. In August, ECB President Mario Draghi acknowledged the need to spur job creation and signaled his commitment to support growth and head off a deflationary spiral. In October, the EU inflation rate stood at 0.4%, well below the ECB
target rate of just below 2%. Growth in GDP across the EU slowed to 0.2% in the third quarter of 2014 from 0.3% in the final quarter of 2013. While European corporate earnings continued to grow during 2014, the overall trend in profit growth was
deceleration. 
Equity markets in Japan ended 2013 at record highs and outperformed other developed markets for the calendar year. However, investor enthusiasm for
the policies of Prime Minister Shinzo Abe began to recede in 2014 and was further diminished by an April 1 increase in the national consumption tax. During the second quarter of the year, the Japanese economy had suffered its biggest slowdown
since the global financial crisis of 2007-08. While Japan’s third quarter showed improvement, it was insufficient to dispel investor concerns about Japan’s economic trajectory. On October 31, the Bank of 
 
1 
Table of Contents

Japan surprised global markets by unveiling plans to buy 8 trillion to 12 trillion yen ($72 billion to $107 billion) of Japanese government bonds per month, while also tripling its purchases of
exchange-traded funds. The announcement drove Tokyo stocks to seven-year highs. For the 12 month reporting period, the MSCI Europe, Australasia and Far East Index (net of withholding taxes) returned -0.2%. 
While emerging markets overall rebounded strongly from the late-2013 sell-off, slowing growth in China and political unrest in select nations weighed on emerging market
equities as an asset class. At 7.3%, China’s third quarter 2014 GDP growth was the slowest in five years, partly due to a slowdown in the property sector. A pro-democracy movement in Hong Kong added to investor concerns. In Brazil, the
re-election of President Dilma Rousseff in October was followed by a greater than 5.0% intra-day drop in the Bovespa Index amid investor concern over her administration’s interventionist private-sector policies. For the twelve month reporting
period, the MSCI Emerging Markets Index (net of withholding taxes) returned 1.0%. 
While relatively low global energy prices provided a welcome tailwind for both
developed and emerging market economies toward the end of the reporting period, investors were squarely focused on the central banks’ policies, which remained accommodative to growth. Amid this backdrop, a sharp pullback in global equities in
mid-October revealed the fragility of gains made during the previous months. However, over the twelve month reporting period, select equities and bond markets provided solid returns for investors who maintained a diversified portfolio and a
long-term view. 
On behalf of everyone at J.P. Morgan Asset Management, thank you for your continued support. We look forward to managing your investment needs for
years to come. Should you have any questions, please visit www.jpmorganfunds.com or contact the J.P. Morgan Funds Service Center at 1-800-480-4111. 
Sincerely
yours, 


Dear Shareholder, 
What began as a period of moderate economic growth supported by the concerted efforts of central banks to provide stimulus, ended with a clear divergence
between the U.S. and the rest of the world in terms of both policy and growth. During the twelve months ended October 31, 2014, the U.S. Federal Reserve responded to improving employment and consumer spending by steadily winding down and ending
its $85 billion-a-month program of bond purchases, and signaled it may start raising interest rates in mid-2015. In June, the European Central Bank (ECB) took unprecedented steps to prevent a deflationary spiral and in October, the Bank of Japan
shocked financial markets by unveiling a massive stimulus program to restart economic growth. 
During the latter part of the twelve month period, U.S.
markets were supported by a 25% drop in global oil prices from June to October, 3.9% growth in third quarter U.S. gross domestic product (GDP), the strongest employment gains since 1999, and rising corporate earnings. From May through June, equity
prices touched record highs and volatility in U.S. markets retreated to lows not consistently seen since 2007. Notably, U.S. equities fell to six-month lows in mid October only to rebound 8.4%, driving the Standard &amp; Poor’s 500 Index
to a new closing high of 2,018.05 points on the final day of the reporting period. 
Following a brief sell-off in emerging markets in late 2013, much of
the reporting period was marked by low growth and low inflation globally, and low volatility in financial markets. In the European Union (EU), positive economic growth returned, and Ireland became the first nation to exit its EU bailout program.
However, unemployment in the EU remained exceptionally high throughout the reporting period, and manufacturing activity across the euro zone slowed in the latter part of the reporting period. 
In June, the ECB cut the EU deposit rate to negative 0.1% from 0.0% in a bid to push banks to extend lending by effectively charging them for parking excess
cash with the central bank. In August, ECB President Mario Draghi acknowledged the need to spur job creation and signaled his commitment to support growth and head off a deflationary spiral. In October, the EU inflation rate stood at 0.4%, well
below the ECB target rate of just below 2%. Growth in GDP across the EU slowed to 0.2% in the third quarter of 2014 from 0.3% in the final quarter of 2013. While European corporate earnings continued to grow during 2014, the overall trend in profit
growth was deceleration. 
Equity markets in Japan ended 2013 at record highs and outperformed other developed markets for the calendar year. However,
investor enthusiasm for the policies of Prime Minister Shinzo Abe began to recede in 2014 and was further diminished by an April 1 increase in the national consumption tax. During the second quarter of the year, the Japanese economy had
suffered its biggest slowdown since the global financial crisis of 2007-08. While Japan’s third quarter showed improvement, it was insufficient to dispel investor concerns about Japan’s economic trajectory. On October 31, the Bank of
Japan surprised global markets by unveiling plans to buy 8 trillion to 12 trillion yen ($72 billion to $107 billion) of Japanese government bonds per month, while also tripling its purchases of exchange-traded funds. The announcement drove Tokyo
stocks to seven-year highs. For the 12 month reporting period, the MSCI Europe, Australasia and Far East Index (net of withholding taxes) returned -0.2%. 
While emerging markets overall rebounded strongly from the late-2013 sell-off, slowing growth in China and political unrest in select nations weighed on
emerging market equities as an asset class. At 7.3%, China’s third quarter 2014 GDP growth was the slowest in five years, partly due to a slowdown in the property sector. A pro-democracy movement in Hong Kong added to investor concerns. In
Brazil, the re-election of President Dilma Rousseff in October was followed by a greater than 5.0% intra-day drop in the Bovespa Index amid investor concern over her administration’s interventionist private-sector policies. For the twelve month
reporting period, the MSCI Emerging Markets Index (net of withholding taxes) returned 1.0%. 
 
1 
Table of Contents
While relatively low global energy prices provided a welcome tailwind for both developed and emerging market
economies toward the end of the reporting period, investors were squarely focused on the central banks’ policies, which remained accommodative to growth. Amid this backdrop, a sharp pullback in global equities in mid-October revealed the
fragility of gains made during the previous months. However, over the twelve month reporting period, select equities and bond markets provided solid returns for investors who maintained a diversified portfolio and a long-term view. 
On behalf of everyone at J.P. Morgan Asset Management, thank you for your continued support. We look forward to managing your investment needs for years to
come. Should you have any questions, please visit www.jpmorganfunds.com or contact the J.P. Morgan Funds Service Center at 1-800-480-4111. 
Sincerely
yours, 


Dear Shareholder, 
What began as a period of moderate economic growth supported by the concerted efforts of central banks to provide stimulus, ended with a clear divergence between the U.S. and the rest of the world in terms
of both policy and growth. During the twelve months ended October 31, 2014, the U.S. Federal Reserve responded to improving employment and consumer spending by steadily winding down and ending its $85 billion-a-month program of bond purchases, and
signaled it may start raising interest rates in mid-2015. In June, the European Central Bank (ECB) took unprecedented steps to prevent a deflationary spiral and in October, the Bank of Japan shocked financial markets by unveiling a massive stimulus
program to restart economic growth. 
 
Dear Shareholder, 
mid-2015.
 
Dear Shareholder, 
What began as a period of moderate economic growth supported by the concerted efforts of central banks to provide stimulus, ended with a clear divergence between the U.S. and the rest of the world in terms
of both policy and growth. During the twelve months ended October 31, 2014, the U.S. Federal Reserve responded to improving employment and consumer spending by steadily winding down and ending its $85 billion-a-month program of bond purchases, and
signaled it may start raising interest rates in mid-2015. In June, the European Central Bank (ECB) took unprecedented steps to prevent a deflationary spiral and in October, the Bank of Japan shocked financial markets by unveiling a massive stimulus
program to restart economic growth. 
 
Dear Shareholder, 
What began as a period of moderate economic growth supported by the concerted efforts of central banks to provide stimulus, ended with a clear divergence between the U.S. and the rest of the world in terms
of both policy and growth. During the twelve months ended October 31, 2014, the U.S. Federal Reserve responded to improving employment and consumer spending by steadily winding down and ending its $85 billion-a-month program of bond purchases,
and signaled it may start raising interest rates in mid-2015. In June, the European Central Bank (ECB) took unprecedented steps to prevent a deflationary spiral and in October, the Bank of Japan shocked financial markets by unveiling a massive
stimulus program to restart economic growth. 
 
Dear Shareholder, 
What began as a period of moderate economic growth supported by the concerted efforts of central banks to provide stimulus, ended with a clear divergence between the U.S. and the rest of the world in terms
of both policy and growth. During the twelve months ended October 31, 2014, the U.S. Federal Reserve responded to improving employment and consumer spending by steadily winding down and ending its $85 billion-a-month program of bond purchases, and
signaled it may start raising interest rates in mid-2015. In June, the European Central Bank (ECB) took unprecedented steps to prevent a deflationary spiral and in October, the Bank of Japan shocked financial markets by unveiling a massive stimulus
program to restart economic growth. 
 
Dear Shareholder, 
What began
as a period of moderate economic growth supported by the concerted efforts of central banks to provide stimulus, ended with a clear divergence between the U.S. and the rest of the world in terms of both policy and growth. During the twelve months
ended October 31, 2014, the U.S. Federal Reserve responded to improving employment and consumer spending by steadily winding down and ending its $85 billion-a-month program of bond purchases, and signaled it may start raising interest rates in
mid-2015. In June, the European Central Bank (ECB) took unprecedented steps to prevent a deflationary spiral and in October, the Bank of Japan shocked financial markets by unveiling a massive stimulus program to restart economic growth. 
During the latter part of the twelve month period, U.S. markets were supported by a 25% drop in global oil prices from June to October, 3.9%
growth in third quarter U.S. gross domestic product (GDP), the strongest employment gains since 1999, and rising corporate earnings. From May through June, equity prices touched record highs and volatility in U.S. markets retreated to lows not
consistently seen since 2007. Notably, U.S. equities fell to six-month lows in mid October only to rebound 8.4%, driving the Standard &amp; Poor’s 500 Index to a new closing high of 2,018.05 points on the final day of the reporting period.

Following a brief sell-off in emerging markets in late 2013, much of the reporting period was marked by low growth and low inflation
globally, and low volatility in financial markets. In the European Union (EU), positive economic growth returned, and Ireland became the first nation to exit its EU bailout program. However, unemployment in the EU remained exceptionally high
throughout the reporting period, and manufacturing activity across the euro zone slowed in the latter part of the reporting period. 
In
June, the ECB cut the EU deposit rate to negative 0.1% from 0.0% in a bid to push banks to extend lending by effectively charging them for parking excess cash with the central bank. In August, ECB President Mario Draghi acknowledged the need to spur
job creation and signaled his commitment to support growth and head off a deflationary spiral. In October, the EU inflation rate stood at 0.4%, well below the ECB target rate of just below 2%. Growth in GDP across the EU slowed to 0.2% in the third
quarter of 2014 from 0.3% in the final quarter of 2013. While European corporate earnings continued to grow during 2014, the overall trend in profit growth was deceleration. 
Equity markets in Japan ended 2013 at record highs and outperformed other developed markets for the calendar year. However, investor
enthusiasm for the policies of Prime Minister Shinzo Abe began to recede in 2014 and was further diminished by an April 1 increase in the national consumption tax. During the second quarter of the year, the Japanese economy had suffered its
biggest slowdown since the global financial crisis of 2007-08. While Japan’s third quarter showed improvement, it was insufficient to dispel investor concerns about Japan’s economic trajectory. On October 31, the Bank of Japan
surprised global markets by unveiling plans to buy 8 trillion to 12 trillion yen ($72 billion to $107 billion) of Japanese government bonds per month, while also tripling its purchases of exchange-traded funds. The announcement drove Tokyo stocks to
seven-year highs. For the 12 month reporting period, the MSCI Europe, Australasia and Far East Index (net of withholding taxes) returned -0.2%. 
While emerging markets overall rebounded strongly from the late-2013 sell-off, slowing growth in China and political unrest in select nations
weighed on emerging market equities as an asset class. At 7.3%, China’s third quarter 2014 GDP growth was the slowest in five years, partly due to a slowdown in the property sector. A pro-democracy movement in Hong Kong added to investor
concerns. In Brazil, the re-election of President Dilma Rousseff in October was followed by a greater than 5.0% intra-day drop in the Bovespa Index amid investor concern over her administration’s interventionist private-sector policies. For the
twelve month reporting period, the MSCI Emerging Markets Index (net of withholding taxes) returned 1.0%. 
While relatively low global
energy prices provided a welcome tailwind for both developed and emerging market economies toward the end of the reporting period, investors were squarely focused on the central banks’ policies, which remained accommodative to growth. Amid this
backdrop, a sharp pullback in global equities in mid-October revealed the fragility of gains made during the previous months. However, over the twelve month reporting period, select equities and bond markets provided solid returns for investors who
maintained a diversified portfolio and a long-term view. 
 
1 
Table of Contents
On behalf of everyone at J.P. Morgan Asset Management, thank you for your continued support. We
look forward to managing your investment needs for years to come. Should you have any questions, please visit www.jpmorganfunds.com or contact the J.P. Morgan Funds Service Center at 1-800-480-4111. 
Sincerely yours, 


