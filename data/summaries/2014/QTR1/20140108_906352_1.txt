Dear Fellow Shareholders,
At First Eagle, we remain committed to attempting to protect and preserve our shareholders' purchasing power over time. Irrespective of short-term market events, swings in investor confidence or the pressure to follow a new market paradigm, our first priority is unwavering — to provide prudent stewardship of the assets you've placed in our care.
Before I provide a market overview and each portfolio management team's views, I wanted to pause and thank you for your continued confidence and support.
Market Overview
Since I last wrote to you in April, equity markets have generally continued their steady march higher and, while there have been a few pockets of uncertainty, by and large investors seem unruffled by continued unprecedented central bank intervention, budget impasses that cause government shut downs, stubbornly high global unemployment, large municipal bankruptcies and debt ceiling impasses — just to name a few. In contrast, talk of "tapering" sent fixed income markets into a swoon through May and June. Nonetheless, prices seem to have stabilized — at least for now.
With the fifth anniversary of the financial crisis upon us, we find it disquieting that investors appear so comfortable taking on more risk while these unresolved global, monetary and fiscal issues continue to fester in the background.
Letter from the President (continued)


