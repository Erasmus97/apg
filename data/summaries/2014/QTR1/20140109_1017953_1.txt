Dear Hennessy Funds Shareholder:






Looking back at it was a year of many highs and lows economically, politically, and socially here in the U.S. and around the globe.The year was marked with some fond memories: we survived two fiscal cliffs, anointed a new Pope, watched the Red Sox don facial hair and keep their town Boston Strong, and home prices began to rebound around the country.But the year was also scarred by worsening political partisanship, civil unrest in many countries, flooding in Colorado, wildfires in the West, and the tragedy of the Boston Marathon.



We understand that the economic landscape is still rife with problems.Economic progress and growth are uneven, at best, and unemployment remains high.The hurdles that, in my opinion, are stalling a more robust recovery here in the U.S. are the same ones weve faced for several years: we are no closer to receiving the clarity we need from our leaders in Washington on taxes, regulation, and healthcare.Companies are still sitting on record amounts of cash, but they appear reluctant to hire in earnest until they feel they have clear guidelines on these major issues.Instead, these cash-rich companies continue to increase and initiate dividends, buy back stock and participate in accretive mergers and acquisitions.We are seeing these activities spread beyond just large, value-oriented companies to more small and mid-cap and growth-oriented firms.While these activities should benefit the shareholders of these companies, these initiatives are not creating jobs, which I believe is the critical missing piece to a thriving and growing economy.



Even through this slow-growth economy, the U.S. stock market has found ways to carve out healthy returns.For the twelve months ended October the Dow Jones Industrial Average (DJIA) returned while the S&amp;P Index returned comprising the DJIA and the S&amp;P continue to have strong balance sheets and are generating respectable profits.There are many attractive stocks with strong and improving fundamentals that I believe present compelling investment opportunities.In fact, through the mutual fund portfolios that we manage, we are seeing improvement across asset classes and in many sectors, including financials, natural gas, housing, and consumer discretionary.I believe that innovative business leaders in this country will continue to find ways to make money for their shareholders.



Japan Market



We have witnessed a strong resurgence in the Japanese market over the past year.While some may have been surprised by strength of this recovery, we believe that we are finally seeing the result of the structural and political reforms that have been taking place in Japan for a number of years.The Japan market was among the top performing markets worldwide over the past year, with the Tokyo Price Index (TOPIX) returning over (in USD terms) for the twelve month period ended October and Japan was by far the highest performing Asian market.



Japans prime minister, Shinzo Abe, and his bold monetary, stimulus and growth policies, termed the Three Arrows of Abenomics, appear to be reversing almost two decades of slow growth and deflation and moving the country into a new cycle of recovery that we believe will be sustainable.The Bank of Japan has undertaken an aggressive Quantitative Easing program, similar to what the Federal Reserve did here in the U.S., causing the Yen to weaken, which in turn has increased the demand for high quality Japanese products throughout the world.However, while the weakening of the Yen may be a catalyst to attract investors to Japan, we have never believed that the Japanese equity market is simply a Yen play.



With anticipated political stability for the next few years and with a strengthening relationship between government and Japanese businesses, we believe that many of the economic growth strategies proposed by Prime Minister Abe will have the support needed to succeed.Despite strong returns over the past year, we do not believe that the Japanese market is currently overvalued, but rather we continue to believe that there remain strong values in high-quality, Japanese companies today and for the long-term.








I am very confident that we are in a long-term, secular bull market fueled by solid economic fundamentals.I am encouraged by the strong returns for the major U.S. and global financial market indices and also by the positive performance of each of the Hennessy Funds over the past twelve months.However, while we have seen consumer and investor confidence on the rise, we still dont believe that investors have fully returned to investing in equities, and particularly in U.S. equities.When investors do return to equities in earnest, it should bode well for the economy.



At Hennessy we remain focused on investing with fundamentals and committed to our proven investment strategies.Treating clients honestly and ethically, building strong partnerships and managing money for the sole benefit of shareholders are the principles that our business was founded on, and those same principles guide us today, nearly years later.If you have any questions or want to speak with us directly, please dont hesitate to call us at



Best regards,
