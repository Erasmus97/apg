Dear Shareholder,
We are pleased to send to you the
HSBC Funds annual report, covering the fiscal year ended October 31, 2013. This
report contains detailed information about your Funds’ portfolio of investments
and operating results. We encourage you to review it carefully.
This report includes commentary from the Funds’
portfolio managers in which they discuss the investment markets and their
respective Fund’s performance. The portfolio manager commentary is accompanied
by the Fund’s return for the period, listed alongside the returns of its
benchmark index and peer group average for comparative purposes.
In closing, we would like to thank
you for investing in the HSBC Funds. We continue to focus the HSBC Fund Family
on investment solutions to assist our shareholders in reaching their financial
goals. We appreciate the trust you place in us, and will continue working to
earn it. Please contact us at any time with questions or concerns.

