Dear Shareholder,
At a meeting held in May the Funds Board of Directors approved a recommendation from Legg Mason Partners Fund Advisor, LLC, the
Funds investment manager, to change the fiscal year-end of the Fund from to As a result of this change, shareholders are being provided with a short-period annual report for the five-month period from
through Please read on for a more detailed look at the prevailing economic and market conditions during the Funds abbreviated reporting period and to learn how those conditions have affected Fund
performance. As always, we remain committed to providing you with excellent service and a full spectrum of investment choices. We also remain committed
to supplementing the support you receive from your financial advisor. One way we accomplish this is through our website, www.leggmason.com/individualinvestors. Here you can gain immediate access to market and investment information, including:






Fund prices and performance,





Market insights and commentaries from our portfolio managers, and






A host of educational resources. We look
forward to helping you meet your financial goals. Sincerely,
