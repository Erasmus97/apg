Dear Shareholder:



At the beginning of the year, the government reported that the U.S. economy had slowed and the market, fearing a recession, pulled back from a strong gain in the second half of my last letter, I reported to you that I shifted the Masters Fund to a more fully-invested and therefore more aggressive posture to take advantage of the lower prices that we saw after the market pullback.



As things turned out, the reduced economic activity in the quarter of was more the result of an exceptionally cold winter than a sign of a weakening economy.However, as the U.S. economy continues to gain strength, the markets optimism has been reigned in by global instability highlighted by the deteriorating situation in Iraq, Syria, Israel, and the Ukraine.As a result, the market has rewarded primarily large-cap companies which operate on a global scale, and many of the innovative, small-cap companies that we invested in the first quarter have suffered.



When performance lags, portfolio adjustments can be made at two levels.At the first level, the individual members of the team are making changes to their portions of the portfolio. I have advised each of them to rank their positions from most to least confident in the next months, then to sell the bottom of the list to invest in the stocks at the top of their list.This should have the effect of recognizing some losses, while at the same time putting the fund in a better position to recover.



The second level of adjustment that I can make is to change the team.When the members of the team have finished their portfolio reviews, I will compare what I hear from them with what I am hearing from the members I have on the bench. I have just completed a review of year track records.There are just under Marketcracy model portfolios that have outperformed Morningstars mutual fund for the last years, and over of these model portfolios outperformed by an average of



The most convincing evidence of investment skill is the demonstration of a superior long-term track record.This greatly reduces the risk of selecting a manager who has simply been lucky a few times.It is hard to be lucky consistently over years.At the same time, I have not yet found any manager whose investment style can perform consistently well in all market conditions.Therefore, as I review the managers on team and on the bench, I will be looking for exceptional recent performance to reduce the risk of selecting a manager whose investment style has run its course and is now out of favor.



Sincerely,
