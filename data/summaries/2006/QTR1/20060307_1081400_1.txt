DEAR VALUED SHAREHOLDER,

Enclosed is your WELLS FARGO ADVANTAGE LIFE STAGE PORTFOLIOS(SM) annual
report for the year ended December On the following pages, you will
find a discussion of each Portfolio, including the Portfolio managers'
performance highlights, information about the holdings in each Portfolio, and
the Portfolio managers' strategic outlook.
