Dear Shareholders,

We would like to thank you for investing in the Baird Funds. We appreciate the
opportunity to help our investors navigate the transitions and challenges
presented by the financial markets in The Baird Funds continued to grow
during the year with total net assets exceeding million as of December
In addition, three of our Bond Funds and two of our Equity Funds
celebrated their anniversaries during the year.

In this Annual Report we review the bond market in and the performance and
composition of each of the Baird Bond Funds. We hope you find this report both
informative and helpful in achieving your investment goals. Thank you again for
choosing Baird Funds.

Sincerely,

Dear Shareholders,

We would like to thank you for investing in the Baird Funds. We appreciate the
opportunity to help our investors navigate the transitions and challenges
presented by the financial markets in The Baird Funds continued to grow
during the year with total net assets exceeding million as of December
In addition, two of our Equity Funds and three of our Bond Funds
celebrated their anniversaries during the year.

In this Annual Report we review the equity market in and the performance
and composition of each of the Baird Equity Funds. We hope you find this report
both informative and helpful in achieving your investment goals. Thank you
again for choosing Baird Funds.

Sincerely,
