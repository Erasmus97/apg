Dear Shareholder,

Detailed information on your Fund's performance can be found in the Portfolio
Managers' Comments and Fund Spotlight sections of this report. The funds
feature portfolio management by Institutional Capital Corporation (ICAP), with
Nuveen Asset Management managing the municipal portion of the Nuveen Balanced
Municipal and Stock Fund. I urge you to take the time to read the portfolio
managers' comments.

With the recent volatility in the market, many have begun to wonder which way
the market is headed, and whether they need to adjust their holdings of
investments. No one knows what the future will bring, which is why we think a
well-balanced portfolio that is structured and carefully monitored with the
help of an investment professional is an important component in achieving your
long-term financial goals. A well-diversified portfolio may actually help to
reduce your overall investment risk, and we believe that investments like your
Nuveen Investments Fund can be important building blocks in a portfolio crafted
to perform well through a variety of market conditions.

Since Nuveen Investments has offered financial products and solutions
that incorporate careful research, diversification, and the application of
conservative risk-management principles. We are grateful that you have chosen
us as a partner as you pursue your financial goals. We look forward to
continuing to earn your trust in the months and years ahead.

Sincerely,

Dear Shareholder,

Detailed information on your Fund's performance can be found in the Portfolio
Managers' Comments and Fund Spotlight sections of this report. The value funds
feature portfolio management by NWQ Investment Management Company, LLC (NWQ)
and Tradewinds NWQ Global Investors, LLC (Tradewinds). I urge you to take the
time to read the portfolio managers' comments.

With the recent volatility in the market, many have begun to wonder which way
the market is headed, and whether they need to adjust their holdings of
investments. No one knows what the future will bring, which is why we think a
well-balanced portfolio that is structured and carefully monitored with the
help of an investment professional is an important component in achieving your
long-term financial goals. A well-diversified portfolio may actually help to
reduce your overall investment risk, and we believe that investments like your
Nuveen Investments Fund can be important building blocks in a portfolio crafted
to perform well through a variety of market conditions.

Since Nuveen Investments has offered financial products and solutions
that incorporate careful research, diversification, and the application of
conservative risk-management principles. We are grateful that you have chosen
us as a partner as you pursue your financial goals. We look forward to
continuing to earn your trust in the months and years ahead.

Sincerely,
