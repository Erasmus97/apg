DEAR SHAREHOLDER,

We present you with the annual report for UBS S&P Index Fund (the "Fund")
for the year ended May

PERFORMANCE

The Fund's investment goal is to replicate the total return of the S&P Index
(the "Index"), before fees and expenses. It seeks to accomplish this goal by
buying and holding substantially all of the common stocks comprising the Index.

Over the fiscal year ended May the Fund's Class A shares returned
net of expenses, before deducting any sales charges (after deducting
maximum sales charges, the return was During the same period, the
median return for the Fund's peer group, the Lipper S&P Index Objective
Funds, was The Fund's performance was in line with the return of
the Index given that the Fund incurs management fees and expenses that the Index
does not. Please keep in mind it is not possible to invest directly in an index.
(Returns for all share classes over various time periods are shown in
"Performance at a Glance" on pages and please note the returns shown do not
reflect the deduction of taxes that a shareholder would pay on Fund
distributions or the redemption of Fund shares.)

UBS S&P INDEX FUND

INVESTMENT GOAL:

Replicate the total return of the S&P Index, before fees and expenses.

PORTFOLIO MANAGERS:
