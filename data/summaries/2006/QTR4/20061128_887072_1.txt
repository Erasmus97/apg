Dear Shareholder:


We are pleased to present this semiannual report for Dreyfus Massachusetts
Intermediate Municipal Bond Fund, covering the six-month period from April through September


After more than two years of steady and gradual increases, the Federal Reserve
Board (the Fed) held short-term interest rates unchanged at its meetings in August and
September.The Fed has indicated that the U.S.
economy has moved to a slower-growth phase of its cycle, as evidenced by softening housing markets in
many regions of the United States.Yet, energy prices have moderated from record highs, calming fears
that the economy may fall into a full-blown
recession.

Most sectors of the U.S. fixed-income market rallied in anticipation of and in
response to the pause in the Feds tightening campaign, including municipal bonds. Investors
apparently are optimistic that higher
borrowing costs and moderating home values may wring current inflationary pressures from the economy. In
addition, most states and municipalities have continued to report higher-than-expected tax receipts as a
result of the recovering economy,
helping to support the credit quality of many municipal bond issuers. As always, we encourage you to
talk with your financial advisor about these and other developments to help ensure that your portfolio
remains aligned with your current tax-managed
needs and future investment goals.

For information about how the fund performed during the reporting period, as
well as market perspectives, we have provided a Discussion of Fund Performance given by the funds
portfolio manager.
