Dear Shareholder: 
 
We hope you find the annual report for the Florida Series informative and useful. 
 
As part of our continuing effort to improve, JennisonDryden has conducted a review of our fund family.
One result of this initiative was the recommendation that the Dryden National Municipals Fund acquire the assets of your fund. This merger was approved by shareholders on October 13, 2006, and it is expected to be completed by the end of this year.

 
We have created a family of funds that you and your professional financial advisor can
comfortably rely upon in investment programs suited to your personal goals and tolerance for risk. We appreciate your confidence in JennisonDryden Mutual Funds, and will continue to work to earn it. 
 
Sincerely, 


Dear Shareholder: 
 
We hope you find the annual report for the New Jersey Series informative and useful. 
 
As part of our continuing effort to improve, JennisonDryden has conducted a review of our fund family.
One result of this initiative was the recommendation that the Dryden National Municipals Fund acquire the assets of your fund. This merger was approved by shareholders on October 13, 2006, and it is expected to be completed by the end of this year.

 
We have created a family of funds that you and your professional financial advisor can
comfortably rely upon in investment programs suited to your personal goals and tolerance for risk. We appreciate your confidence in JennisonDryden Mutual Funds, and will continue to work to earn it. 
 
Sincerely, 


Dear Shareholder: 
 
We hope you find the annual report for the New York Series informative and useful. 
 
As part of our continuing effort to improve, JennisonDryden has conducted a review of our fund family.
One result of this initiative was the recommendation that the Dryden National Municipals Fund acquire the assets of your fund. This merger was approved by shareholders on October 13, 2006, and it is expected to be completed by the end of this
year. 
 
We have created a family of funds that you and your professional financial advisor
can comfortably rely upon in investment programs suited to your personal goals and tolerance for risk. We appreciate your confidence in JennisonDryden Mutual Funds, and will continue to work to earn it. 
 
Sincerely, 


Dear Shareholder: 
 
We hope you find the annual report for the Pennsylvania Series informative and useful. 
 
As part of our continuing effort to improve, JennisonDryden has conducted a review of our fund family.
One result of this initiative was the recommendation that the Dryden National Municipals Fund acquire the assets of your fund. This merger was approved by shareholders on October 13, 2006, and it is expected to be completed by the end of this year.

 
We have created a family of funds that you and your professional financial advisor can
comfortably rely upon in investment programs suited to your personal goals and tolerance for risk. We appreciate your confidence in JennisonDryden Mutual Funds, and will continue to work to earn it. 
 
Sincerely, 


