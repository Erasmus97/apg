Dear Shareholder,

We are pleased to provide the annual report of Legg Mason Batterymarch International Equity Trust for the twelve-month reporting period
ended December 31, 2012. Please read on for a detailed look at prevailing economic and market conditions during the Fund’s reporting period and to learn how those conditions have affected Fund performance. 
Special shareholder notice 
At the
Board of Trustees of Legg Mason Global Asset Management Trust’s regular meeting held in February 2013, Western Asset Management Company was appointed as a subadviser of the Fund solely for the management of cash and short-term instruments.

As always, we remain committed to providing you with excellent service and a full spectrum of investment choices. We also remain
committed to supplementing the support you receive from your financial advisor. One way we accomplish this is through our website, www.leggmason.com/individualinvestors. Here you can gain immediate access to market and investment information,
including: 
 
Fund prices and performance, 
 
Market insights and commentaries from our portfolio managers, and 
 
A host of educational resources. 
We look forward to helping you meet your financial goals. 
Sincerely, 


