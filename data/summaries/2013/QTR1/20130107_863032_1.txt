Dear Shareholders: 
As 2012 winds down, economic uncertainty continues to dominate world financial markets. In the United States, all eyes are riveted to the ongoing budget deal

negotiations and the specter of a “fiscal cliff.” Overseas, we see growth slowing in China and Japan, and the eurozone has entered its second
recession in four years against a backdrop of double-digit unemployment and the continuing sovereign debt crisis. 
Amidst the instability, there are
silver linings — especially in the U.S. where the labor and housing markets have picked up, consumer confidence has risen and industrial output has increased. Additionally, a U.S. budgetary compromise could propel markets, unleashing pent-up
spending and investments, which would help to revive both the U.S. and global economies. 
As always, managing risk in the face of
uncertainty remains a top priority for investors. At MFS®, an emphasis on global research and our disciplined risk management approach anchor our uniquely collaborative investment process. Our global team of more than 200 investment
professionals shares ideas and evaluates opportunities across continents, investment disciplines, and asset classes — all with a goal of building better insights, and ultimately better results — for our clients. 
We are mindful of the many economic challenges we face locally, nationally and globally. It is more important than ever to maintain a long-term view, employ
time-tested principles, such as asset allocation and diversification, and work closely with investment advisors to identify and pursue the most suitable opportunities. 
Respectfully, 


Dear Shareholders: 
As 2012 winds down, economic uncertainty continues to dominate world financial markets. In the United States, all eyes are riveted to the ongoing budget deal

negotiations and the specter of a “fiscal cliff.” Overseas, we see growth slowing in China and Japan, and the eurozone has entered its second
recession in four years against a backdrop of double-digit unemployment and the continuing sovereign debt crisis. 
Amidst the instability, there are
silver linings — especially in the U.S. where the labor and housing markets have picked up, consumer confidence has risen and industrial output has increased. Additionally, a U.S. budgetary compromise could propel markets, unleashing pent-up
spending and investments, which would help to revive both the U.S. and global economies. 
As always, managing risk in the face of
uncertainty remains a top priority for investors. At MFS®, an emphasis on global research and our disciplined risk management approach anchor our uniquely collaborative investment process. Our global team of more than 200 investment
professionals shares ideas and evaluates opportunities across continents, investment disciplines, and asset classes — all with a goal of building better insights, and ultimately better results — for our clients. 
We are mindful of the many economic challenges we face locally, nationally and globally. It is more important than ever to maintain a long-term view, employ
time-tested principles, such as asset allocation and diversification, and work closely with investment advisors to identify and pursue the most suitable opportunities. 
Respectfully, 


Dear Shareholders: 
As 2012 winds down, economic uncertainty continues to dominate world financial markets. In the United States, all eyes are riveted to the ongoing budget deal

negotiations and the specter of a “fiscal cliff.” Overseas, we see growth slowing in China and Japan, and the eurozone has entered its second
recession in four years against a backdrop of double-digit unemployment and the continuing sovereign debt crisis. 
Amidst the instability, there are
silver linings — especially in the U.S. where the labor and housing markets have picked up, consumer confidence has risen and industrial output has increased. Additionally, a U.S. budgetary compromise could propel markets, unleashing pent-up
spending and investments, which would help to revive both the U.S. and global economies. 
As always, managing risk in the face of
uncertainty remains a top priority for investors. At MFS®, an emphasis on global research and our disciplined risk management approach anchor our uniquely collaborative investment process. Our global team of more than 200 investment
professionals shares ideas and evaluates opportunities across continents, investment disciplines, and asset classes — all with a goal of building better insights, and ultimately better results — for our clients. 
We are mindful of the many economic challenges we face locally, nationally and globally. It is more important than ever to maintain a long-term view, employ
time-tested principles, such as asset allocation and diversification, and work closely with investment advisors to identify and pursue the most suitable opportunities. 
Respectfully, 


