Dear Fellow Shareholder,




As stewards of our customers savings, the management team and Directors/Trustees of the Selected Funds recognize the importance of candid, thorough, and regular communication with our shareholders. In our Annual and Semi-Annual Reports, we include all of the required quantitative information, such as audited financial statements, detailed footnotes, performance reports, fund holdings, and performance attribution.Also included is a list of positions opened and closed.




In addition, we produce a Manager Commentary for certain funds, which is published semi-annually. In this commentary, we give a more qualitative perspective on fund performance, discuss our thoughts on individual holdings, and share our investment outlook. You may obtain a copy of the current Manager Commentary either on the Funds website at www.selectedfunds.com or by calling




It is with regret that we inform you that Jerome (Jerry) Hass passed away on January For more than years, Jerry has zealously and effectively served the Funds and their shareholders as an independent director, for many years serving as Chair of the Trading and Brokerage Committee and as an active member of the Investment Committee. He will be missed.




We thank you for your continued trust. We will do our best to earn it in the years ahead.







Sincerely,
