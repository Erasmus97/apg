Dear Shareholder,
We are pleased to provide the annual report of ClearBridge Small Cap Value Fund for the twelve-month reporting period ended
Please read on for a detailed look at prevailing economic and market conditions during the Funds reporting period and to learn how those conditions have affected Fund performance.
I am pleased to introduce myself as the new President and Chief Executive Officer of the Fund, succeeding R. Jay Gerken, as he embarks upon his retirement. Jay has most recently served as Chairman of the Board,
President and Chief Executive Officer of the Fund and other funds in the Legg Mason complex. On behalf of all our shareholders and the Funds Board of Trustees, I would like to thank Jay for his vision and guidance, and wish him all the best.
