Dear Shareholder:
We are pleased to present this annual report for Dreyfus AMT-Free Municipal Bond Fund, covering the 12-month period from September 1, 2012, through August 31, 2013. For information about how the fund performed during the reporting period, as well as general market perspectives, we provide a Discussion of Fund Performance on the pages that follow.
U.S. fixed-income markets, including municipal bonds, encountered heightened volatility over the past year as long-term interest rates climbed in response to accelerating economic growth and expectations that the Federal Reserve Board will begin to back away from the open-ended quantitative easing program it launched last fall. Indeed, the U.S. economy has responded positively to low interest rates amid muted inflationary pressures, helping to drive the unemployment rate lower and housing markets higher.
In our analysis, the U.S. economy is nearing an inflection point to a somewhat faster growth rate.We expect a reduced fiscal drag in 2014 and beyond, and the stimulative monetary policy of the past five years should continue to support economic expansion, particularly in interest rate-sensitive industry groups. For information on how these developments may affect your fixed-income investments, we urge you to discuss these matters with your financial advisor.
Thank you for your continued confidence and support.
Sincerely,


Dear Shareholder:
We are pleased to present this annual report for Dreyfus BASIC Municipal Money Market Fund, covering the 12-month period from September 1, 2012, through August 31, 2013. For information about how the fund performed during the reporting period, as well as general market perspectives, we provide a Discussion of Fund Performance on the pages that follow.
U.S. fixed-income markets weathered bouts of heightened volatility over the past year as long-term interest rates climbed in response to accelerating economic growth and expectations that the Federal Reserve Board (the “Fed”) will begin to back away from the open-ended quantitative easing program it launched last fall. Indeed, the U.S. economy has responded positively to low interest rates amid muted inflationary pressures, helping to drive the unemployment rate lower and housing markets higher. A tax hike on upper-income earners, higher Social Security taxes, and sequester-related spending cuts were not enough to fully offset the stimulative effects of the Fed’s accommodative monetary policy. While these forces drove long-term interest rates higher, money market yields remained anchored near historical lows by the Fed’s unchanged target for short-term rates.
In our analysis, the U.S. economy is nearing an inflection point to a somewhat faster growth rate.We expect a reduced fiscal drag in 2014 and beyond, and the stimulative monetary policy of the past five years should continue to support economic expansion. Nonetheless, the Fed has shown no signs that it intends to raise short-term interest rates anytime soon. For information on how these developments may affect your investments, we urge you to discuss these matters with your financial advisor.
Thank you for your continued confidence and support.
Sincerely,


Dear Shareholder:
We are pleased to present this annual report for Dreyfus HighYield Municipal Bond Fund, covering the 12-month period from September 1, 2012, through August 31, 2013. For information about how the fund performed during the reporting period, as well as general market perspectives, we provide a Discussion of Fund Performance on the pages that follow.
U.S. fixed-income markets, including municipal bonds, encountered heightened volatility over the past year as long-term interest rates climbed in response to accelerating economic growth and expectations that the Federal Reserve Board will begin to back away from the open-ended quantitative easing program it launched last fall. Indeed, the U.S. economy has responded positively to low interest rates amid muted inflationary pressures, helping to drive the unemployment rate lower and housing markets higher.
In our analysis, the U.S. economy is nearing an inflection point to a somewhat faster growth rate.We expect a reduced fiscal drag in 2014 and beyond, and the stimulative monetary policy of the past five years should continue to support economic expansion, particularly in interest rate-sensitive industry groups. For information on how these developments may affect your fixed-income investments, we urge you to discuss these matters with your financial advisor.
Thank you for your continued confidence and support.
Sincerely,


