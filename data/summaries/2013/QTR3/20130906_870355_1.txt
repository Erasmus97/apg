Dear Fellow
Shareholder,
 
The premise on which Quaker Funds, Inc. was founded was the desire to afford everyday investors access to the same
tactical allocation used by professional money managers to augment traditional investing strategies within a holistic asset allocation
mix. Our commitment to this principle is still as strong today as it was the day we opened our doors.
 
Our management team
continually strives to provide our shareholders with innovative investment alternatives and advisers that constantly seek
superior returns. Thank you for your trust and investment in the Quaker Funds.
 
Sincerely,


