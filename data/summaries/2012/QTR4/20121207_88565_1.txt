DEAR SHAREHOLDER





ECONOMIC AND MARKET OVERVIEW





ABOUT SHAREHOLDERS&rsquo; FUND EXPENSES





LARGE CAP VALUE FUND





LARGE CAP VALUE INSTITUTIONAL FUND





NOTES TO FINANCIAL STATEMENTS





REPORT OF INDEPENDENT REGISTERED PUBLIC ACCOUNTING FIRM





OTHER INFORMATION





INFORMATION ON BOARD OF DIRECTORS AND OFFICERS





GUGGENHEIM INVESTMENTS PRIVACY POLICIES







THE GUGGENHEIM FUNDS ANNUAL










September



Dear Shareholder:



Security Investors, LLC (the &ldquo;Investment Adviser&rdquo;)
is pleased to present the annual shareholder report for two of our mutual funds (the &ldquo;Funds&rdquo;).



The Investment Adviser is a part of Guggenheim Investments,
which represents the investment management businesses of Guggenheim Partners, LLC, a global, diversified financial services firm.



This report covers performance of the following Funds for the
annual period ended September with the name of each Fund followed by its ticker symbol:



-Large Cap Value Fund (SECIX)*



-Large Cap Value Institutional Fund (SLCIX)



Rydex Distributors, LLC, the distributor of the Funds, is committed
to providing investors with innovative investment solutions; as of the date of this report, we offer a wide range of domestic and
global themes in our funds and a distinctive ETF line-up.



To learn more about economic and market conditions over the
months ended September and the objective and performance of each Fund, we encourage you to read the Economic and Market
Overview section of the report, which follows this letter, and the Manager&rsquo;s Commentary for each Fund.



Sincerely,
