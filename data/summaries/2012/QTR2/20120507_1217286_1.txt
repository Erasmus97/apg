Dear Shareholder: 
“Risk on, risk off” was the mantra for 2011. Assets perceived as risky moved en masse as investors adjusted their portfolios in response to unexpected sources of distress and updates on known
problems — the European debt crisis, political strife in the Middle East, the earthquake and subsequent tsunami in Japan and the credit rating of debt issued by the United States. 
 
Dear Shareholder: 
“Risk on, risk off” was the mantra for 2011. Assets perceived as risky moved en masse as investors adjusted their portfolios in response to unexpected sources of distress and updates on known
problems — the European debt crisis, political strife in the Middle East, the earthquake and subsequent tsunami in Japan and the credit rating of debt issued by the United States. 
 
Dear Shareholder: 
“Risk on, risk off” was the mantra for 2011. Assets perceived as risky moved en masse as investors adjusted their portfolios in response to unexpected sources of distress and updates on known
problems — the European debt crisis, political strife in the Middle East, the earthquake and subsequent tsunami in Japan and the credit rating of debt issued by the United States. 
 
Dear Shareholder: 
“Risk on, risk off” was the mantra for 2011. Assets perceived as risky moved en masse as investors adjusted their portfolios in response to unexpected sources of distress and updates on known
problems — the European debt crisis, political strife in the Middle East, the earthquake and subsequent tsunami in Japan and the credit rating of debt issued by the United States. 
 
Dear Shareholder: 
“Risk on, risk off” was the mantra for 2011. Assets perceived as risky moved en masse as investors adjusted their portfolios in response to unexpected sources of distress and updates on known
problems — the European debt crisis, political strife in the Middle East, the earthquake and subsequent tsunami in Japan and the credit rating of debt issued by the United States. 
 
Dear Shareholder: 
“Risk on, risk off” was the mantra for 2011. Assets perceived as risky moved en masse as investors adjusted their portfolios in response to unexpected sources of distress and updates on known
problems—the European debt crisis, political strife in the Middle East, the earthquake and subsequent tsunami in Japan and the credit rating of debt issued by the United States. 
 
Dear Shareholder: 
“Risk on, risk off” was the mantra for 2011. Assets perceived as risky moved en masse as investors adjusted their portfolios in response to unexpected sources of distress and updates on known
problems—the European debt crisis, political strife in the Middle East, the earthquake and subsequent tsunami in Japan and the credit rating of debt issued by the United States. 
 
Dear Shareholder: 
“Risk on, risk off” was the mantra for 2011. Assets perceived as risky moved en masse as investors adjusted their portfolios in response to unexpected sources of distress and updates on known
problems — the European debt crisis, political strife in the Middle East, the earthquake and subsequent tsunami in Japan and the credit rating of debt issued by the United States. 
 
Dear Shareholder: 
“Risk on,
risk off” was the mantra for 2011. Assets perceived as risky moved en masse as investors adjusted their portfolios in response to unexpected sources of distress and updates on known problems—the European debt crisis, political strife in
the Middle East, the earthquake and subsequent tsunami in Japan and the credit rating of debt issued by the United States. 
So far in 2012,
improving U.S. economic data and aggressive measures taken by the European Central Bank to curb the region’s debt crisis have sustained investors’ appetite for risk. Since January 1, 2012, equities have rallied strongly and
outperformed fixed income securities. 
U.S. Stocks Outperform 
While the “risk on, risk off” market environment caused elevated correlations within individual asset classes, stock returns across market capitalizations, countries and investment styles varied
during the six months ended February 29, 2012. Buoyed by stronger-than-expected economic data, U.S. stocks finished the six-months ended February 29, 2012 ahead of international and emerging markets stocks. 
During the six months ended February 29, 2012, the S&amp;P 500 Index gained 13.3% versus the 9.4% and 9.1% returns for the MSCI EAFE (Europe,
Australasia, and the Far East) Index (gross of foreign withholding taxes) and the MSCI EM (Emerging Markets) Index (gross of foreign withholding taxes), respectively. Among U.S. stocks, large-cap stocks outperformed small- and mid-cap stocks.

Yields for U.S. Treasuries Remain Near Historical Lows 
Despite historically low yields and a credit downgrade, investors continued to invest in U.S. Treasury securities. The yield for l0-year U.S. Treasury securities ended February 2012 below 2% and reached
its lowest level since 1950 in September 2011. The yield on the 2-year U.S. Treasury note finished February 2012 at 0.3%, while the yield on the 30-year U.S. Treasury bond finished at 3.1%. 
Interest rates have increased in March 2012 but yields remain at historically low levels. Nonetheless, many investors have renewed their focus on the longer-term impact of rising interest rates on their
fixed income investments. In addition, many investors view the unprecedented quantitative easing by the U.S. Federal Reserve as kindling for inflation if the U.S. economic recovery continues to take hold. 
Maintain a Diversified Portfolio 
Several indicators suggest that a U.S. economic recovery is gaining traction—the labor market has shown signs of healing, while recent retail and
auto sales have been strong. Meanwhile, soaring year-to-date returns for emerging markets stocks reflect investors’ optimism surrounding the prospects for growth in these countries. Of course, risks remain. Many investors worry that the pace of
China’s robust economic growth could decelerate, potentially resulting in a “hard landing” from measures aimed to control inflation and cool its surging property market. In addition, political turmoil in the Middle East persists,
while Europe faces pockets of recession in the midst of its ongoing debt crisis. 
 
1 
Table of Contents
Indeed, despite the recent rally in stock markets uncertainty in the current environment is widespread,
making diversification even more critical to long-term investment success. Exposure to a variety of asset classes within fixed income, equity and alternative investments can help safeguard portfolios from the current risks in the marketplace and any
unexpected shocks along the way. 
On behalf of everyone at J.P. Morgan Asset Management, thank you for your continued support. We look forward
to managing your investment needs for years to come. Should you have any questions, please visit www.jpmorganfunds.com or contact the J.P. Morgan Funds Service Center at 1-800-480-4111. 
Sincerely yours, 


