import general.utils as utils
import numpy as np
import networkx as nx


class File:

    MIN_SUMMARY = 10

    def __init__(self, path, summary=False):
        self.__path = path
        self.__content = []
        self.__n_words = None
        self.__cleaned = False
        self.__processed = False
        self.__is_summary = summary
        self.glove_embeddings = {}

        # Save text results as attributes
        self.words = {}
        self.fogs = {}
        self.results = {}

        # Save previous filing of fund as attribute
        self.link = None
        self.similarity = None

        self.__create()

        # If it is a summary, then it is cleaned and processed
        if summary:
            self.__cleaned = True
            self.__processed = True
            self.set_word_count()
            self.get_glove_embeddings()

    @property
    def path(self):
        return self.__path

    @property
    def content(self):
        return self.__content

    @property
    def n_words(self):
        return self.__n_words

    @property
    def is_summary(self):
        return self.__is_summary

    def size(self):
        return len(self.__content)

    def set_word_count(self):
        """ Sets the word count attribute of the file """
        self.__n_words = 0
        for line in self.__content:
            self.__n_words += len(line.split())

    def __create(self):
        """ Sets the content of the object as the file's content """
        if self.__is_summary:
            self.__content = utils.io.open(self.__path, 'r', encoding='utf-8').readlines()
        else:
            self.__content = utils.io.open(self.__path, 'r', encoding='utf-8').read()

    def is_clean(self):
        return self.__cleaned

    def is_processed(self):
        return self.__processed

    def clean(self, inplace=True):
        """
        This method cleans the content of the file from punctuation and turns everything to uppercase
        :param inplace: if True, all content is replaced by the `cleaned` content
        :type inplace: bool
        :return: the cleaned shareholder letter
        :rtype: list
        """
        data = utils.tokenize.sent_tokenize(self.__content)

        # Remove new lines from raw version
        file = [f.replace('\n', ' ') for f in data]

        # Remove duplicate letters in a file
        file = utils.remove_duplicate(file)

        # Fixed: abbreviations with two dots
        new_file = []
        for i in range(len(file)):
            line = file[i]

            if 'DEAR' in line.upper() and any(ext in line.upper() for ext in utils.STARTERS):
                starter = [ext for ext in utils.STARTERS if ext in line.upper()][0]
                splitted = utils.re.split(starter, line, flags = utils.re.IGNORECASE)
                if len(splitted) > 1:
                    line = splitted[0] + ' ' + starter
                    new_file.append(line)
                    new_file.append(splitted[1])
                    continue

            # Don't forget to add the first line
            if i == 0:
                new_file.append(line)
                continue

            # Check if first letter is lower case
            if line[0] == line[0].lower():
                line = file[i - 1] + ' ' + file[i]
                new_file.pop()

            new_file.append(line)

        # Make uppercase of every word
        file = [f.upper() for f in new_file]

        # Replace punctuation by white space
        file = [utils.re.sub(r"""[-",|.;@=#?!()+&$*']+ \ * """, " ", line, flags=utils.re.VERBOSE) for line in file]
        file = [f.replace('®', '') for f in file]
        file = [f.replace('©', '') for f in file]
        file = [f.replace('™', '') for f in file]
        file = [f.replace('--', ' ') for f in file]

        if inplace:
            # Update file object
            self.__content = file
            self.__cleaned = True

            # Update number of words in file
            self.set_word_count()
        else:
            return file

    def remove_words(self, line_idx=None):
        """
        This method removes stopwords and personal words from the file.
        :param line_idx: The index of the line that needs to be transformed (see attributes).
            If None, the transformation will take place in every line of the given
            file index.
            Note that `file_idx` should be given for this variable to work.
        :type line_idx: int (default None)
        :return: the cleaned shareholder letter
        :rtype: list
        """

        def __clean_line(line):
            # Split the line
            splitted = line.split()

            # Get list of words that need to be removed
            for word in utils.TOTAL_WORDS:
                # Word can occur multiple times, so remove recursively
                while word in splitted:
                    splitted.remove(word)

            # Remove words with single character
            splitted = [s for s in splitted if len(s) > 1]

            return ' '.join(splitted)

        # File must be cleaned!
        if not self.__cleaned:
            self.clean(inplace=True)

        # Clean specific line, place it back in file and return line
        if line_idx is not None:
            self.__content[line_idx] = __clean_line(self.__content[line_idx])

            # Update that the whole file is preprocessed
            if line_idx == self.size() - 1:
                self.__processed = True
                self.set_word_count()

            # We need a copy of this line when calculating results
            return utils.copy.deepcopy(self.__content[line_idx])
        # Iterate over each line
        else:
            # Iterate over every line to clean it
            for i in range(self.size()):
                self.__content[i] = __clean_line(self.__content[i])

            # Update that the whole file is preprocessed
            self.__processed = True
            self.set_word_count()

    def get_glove_embeddings(self):
        """Import GloVe Word List and Store Weights"""
        print('Loading GloVe....')
        glove_embeddings = {}
        f = open('../data/glove/glove.6B.300d.txt', encoding='utf-8')
        for line in f:
            values = line.split()
            word = values[0]
            coefs = np.asarray(values[1:], dtype='float32')
            size = len(coefs)
            glove_embeddings[word.upper()] = coefs
        f.close()
        print('Loaded GloVe!\n')
        self.glove_embeddings = glove_embeddings

    def summarize(self, to_file=False):
        """
        This method creates a summary of the content
        :param to_file: if True, the summary is written to a file with the same title
        :type to_file: bool
        :return: the summarized content
        :rtype: list
        """
        if len(self.__content) < File.MIN_SUMMARY:
            if to_file:
                self.summary_to_file(utils.copy.deepcopy(self.__content))
            return utils.copy.deepcopy(self.__content)

        if not self.__processed:
            self.remove_words()

        content = utils.copy.deepcopy(self.__content)

        # Remove query sentences such that they do not bias our weights
        content = [c for c in content if not ('DEAR' in c and any(ext in c for ext in utils.STARTERS))]
        content = [c for c in content if not (len(c)<5 and (any(ext in c for ext in utils.TITLES_SHORT) or
                                                            any(ext in c for ext in utils.TITLES_FULL)))]
        content = [c for c in content if c not in utils.ENDINGS]
        content = [c for c in content if c not in utils.DISCLAIMERS]
        content = [c for c in content if c not in ["", " "]]

        # Check again if the file is long enough
        if len(content) < File.MIN_SUMMARY:
            if to_file:
                self.summary_to_file(content)
            return content

        # Create weights per sentence based on GloVe
        sentence_vectors = []
        for sentence in content:
            words = sentence.split()
            if len(words) > 0:
                # For each word in the sentence, find the value in the word_embeddings dictionary
                sentence_weight = sum([self.glove_embeddings.get(w, np.zeros((300,))) for w in words]) / (len(words))
            else:
                sentence_weight = np.zeros((300,))
            sentence_vectors.append(sentence_weight)

        # Determine negative distance between sentences
        neg_distance = np.zeros([len(sentence_vectors), len(sentence_vectors)])
        for i in range(len(sentence_vectors)):
            for j in range(len(sentence_vectors)):
                if i != j:
                    # Compute negative cosine distance
                    denominator = 1
                    for vec in [sentence_vectors[i].reshape(1, 300), sentence_vectors[j].reshape(1, 300)]:
                        l2 = 0
                        for element in vec[0]:
                            l2 += element**2
                        denominator *= np.sqrt(l2)
                    numerator = np.inner(sentence_vectors[i].reshape(1, 300), sentence_vectors[j].reshape(1, 300))
                    # Check if numerator is defined
                    if numerator>0 and denominator>0:
                        neg_distance[i][j] = numerator/denominator+1
                    else:
                        neg_distance[i][j] = 1

        # Construct graph and find ranks
        nx_graph = nx.from_numpy_array(neg_distance)
        scores = nx.pagerank_numpy(nx_graph)

        # Rank sentences
        ranked_sentences = sorted(((scores[i], s) for i, s in enumerate(content)), reverse=True)

        # Get unique ranked sentences
        i = 0
        break_point = min(File.MIN_SUMMARY, len(sentence_vectors))
        summary = []
        while True:
            # Check if we still can get an element from the list
            if i < len(ranked_sentences):
                ranked_sentence = ranked_sentences[i][1]
            else:
                break

            # Only add sentences to the summary that are not yet in there
            if ranked_sentence not in summary:
                summary.append(ranked_sentence)
            else:
                # By updating the end point, we can take extra sentences
                break_point += 1

            # If we reach the end, we stop
            if i == break_point - 1:
                break

            # Update counter
            i += 1

        if to_file:
            self.summary_to_file(summary)

        return summary

    def summary_to_file(self, summary):
        """
        This method writes the summary to a file
        :param summary: content of the summary
        :type summary: list
        """
        # Make a proper path name and create path
        path = utils.DATA_PATH + 'summaries'
        if not utils.os.path.isdir(path):
            utils.os.mkdir(path)

        for add in self.__path.split('/')[-3:-1]:
            path = path + '/' + add
            if not utils.os.path.isdir(path):
                utils.os.mkdir(path)

        with open(path + f'/{self.__path.split("/")[-1]}', 'w+') as f:
            for line in summary:
                f.write(line + '\n')

    def __str__(self):
        return utils.copy.deepcopy(self.__content)

    def __len__(self):
        return len(self.__content)

    def __iter__(self):
        self.__n = -1
        return self

    def __next__(self):
        if self.__n < self.size() - 1:
            self.__n += 1
            return utils.copy.deepcopy(self.__content[self.__n])
        else:
            raise StopIteration

    def __getitem__(self, idx):
        return utils.copy.deepcopy(self.__content[idx])


class FileList:

    def __init__(self):
        self.__files = {}
        self.__size = 0

    @staticmethod
    def __is_file(file):
        if isinstance(file, File):
            return True
        raise TypeError('file is not of object File.')

    def append(self, file, clean=True):
        """
        This method adds a File object to the list
        :type file: File object
        :param clean: if True, then the file is cleaned (see method clean in File)
        :type clean: bool
        """
        self.__is_file(file)
        if file.path not in self.__files:
            if clean and (not file.is_clean()):
                file.clean()
            self.__files[file.path] = file
            self.__size += 1
        else:
            raise KeyError('file already exists is list.')

    def delete(self, file_name):
        """
        This method removes a File object from the list
        :param file_name: path of the file
        :type file_name: str
        """
        try:
            del self.__files[file_name]
            self.__size -= 1
        except KeyError:
            raise KeyError('file does not exist in list.')

    def winsorize(self, perc=0.05):
        """
        This method trims the file list based on file size
        :param perc: percentage that needs to be removed in total
        :type perc: float
        """
        # Sort dictionary based on number of words
        # If reverse is False, then it is sorted from small to big
        for reverse in [False, True]:
            size_dict = {key: value for key, value in
                         sorted(utils.copy.deepcopy(self.__files).items(),
                                key=lambda item: item[1].n_words, reverse=reverse)}

            count = 0
            bound = int(len(size_dict)*perc) // 2
            for key, value in size_dict.items():
                # This value is the smallest one in the current list
                self.delete(key)

                count += 1
                if count == bound:
                    break

    def find_trim(self, perc=0.05):
        """
        This method sorts a dictionary on number of words and finds items to remove
        :param perc: percentage that needs to be removed
        :type perc: float
        :return: trimmed set
        :rtype: set
        """
        # If reverse is False, then it is sorted from small to big
        trim_set = set()
        for reverse in [False, True]:
            size_dict = {key: value for key, value in
                         sorted(utils.copy.deepcopy(self.__files).items(),
                                key=lambda item: item[1].n_words, reverse=reverse)}

            count = 0
            bound = int(len(size_dict) * perc) // 2
            for key, value in size_dict.items():
                # This value is the smallest one in the current list
                file_name = key.split('/')[-1]
                trim_set.add(file_name)
                count += 1
                if count == bound:
                    break
        return trim_set

    def trim(self, set):
        """ This method removes files in a set """
        for key, value in utils.copy.deepcopy(self.__files).items():
            for file_name in set:
                if file_name in key:
                    self.delete(key)

    def get_file(self, file_name):
        try:
            return self.__files[file_name]
        except KeyError:
            return None

    def get_files(self):
        return utils.copy.deepcopy(list(self.__files.values()))

    def file_names(self):
        return utils.copy.deepcopy(list(self.__files.keys()))

    def __str__(self):
        return f'The file list contains {len(self.__files)} files.'

    def __len__(self):
        return self.__size

    def __iter__(self):
        self.__n = -1
        return self

    def __next__(self):
        if self.__n < len(self.__files) - 1:
            self.__n += 1
            # This is NOT a deepcopy
            return list(self.__files.values())[self.__n]
        else:
            raise StopIteration

    def __getitem__(self, idx):
        # Even though we have a dictionary, we allow to get a file by
        # index because it could be convenient
        return utils.copy.deepcopy(list(self.__files.values())[idx])