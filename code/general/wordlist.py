#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 17 13:09:44 2020

@author: W.F. van Driel
"""

import pickle

from os.path import exists

import pandas as pd


class WordLists:
    """
    The class WordLists represents the word lists of Loughran and McDonald
    which is retrieved from: https://sraf.nd.edu
    """
    
    PATH_LM = "../data/LoughranMcDonald_SentimentWordLists_2018.xlsx"
    PATH_DICT = "../data/LoughranMcDonald_SentimentWordLists_2018.pickle"
    PATH_FOG = "../data/wordlist_merged_2028.txt"
    
    def __init__(self):
        self.__sentiments = []

        # Check if dictionary of L&M already exists
        if exists(WordLists.PATH_DICT):
            with open(WordLists.PATH_DICT, 'rb') as handle:
                self.__dictionary = pickle.load(handle)
        else:
            # Create dictionary of L&M
            file = self._open_file(WordLists.PATH_LM)
            self.__dictionary = self.create_word_list(file)
        
        # Create file for the modified fog index
        self.__fog_list = self._open_file(WordLists.PATH_FOG)
        
    @property
    def dictionary(self):
        return self.__dictionary
    
    @property
    def fog_list(self):
        return self.__fog_list
    
    @property
    def sentiments(self):
        return self.__sentiments
    
    @sentiments.setter
    def sentiments(self, value):
        self.__sentiments = value
        
    def create_word_list(self, df):
        """ This method creates the word list from a pandas.Dataframe
        
        Parameters
        ----------
        df : pandas.DataFrame
            The dataframe that contains the words per sentiment (separated per sheet)
        
        Returns
        -------
        dir_ : dictionary
            Every word is a key with the corresponding sentiment(s) as value(s)
        """
        # Create dictionary: word --> sentiment(s)
        dir_ = {}
        for sentiment, words in df.items():
            # First sheet only provides info of the .xlsx file
            if sentiment == 'Documentation':
                continue
            
            # Add sentiment to list
            self.__sentiments.append(sentiment)
            
            # The variable `words` is a pandas.Dataframe where the header is 
            # one of the words that we need
            # Also, each element is a list in a list, so we need to grab the 
            # only value out of this outer list
            all_words = [word[0] for word in words.values.tolist() + words.columns.tolist()]
            
            # Add every word to dictionary
            for word in all_words:
                if word not in dir_:
                    dir_[word] = sentiment
                else:
                    # Check if we already had multiple sentiments
                    if isinstance(dir_[word], list):
                        dir_[word].append(sentiment)
                    else:
                        dir_[word] = [dir_[word], sentiment]

        # Save dictionary as pickle such that it can be used again later
        with open(WordLists.PATH_DICT, 'wb') as handle:
            pickle.dump(dir_, handle, protocol=pickle.HIGHEST_PROTOCOL)

        return dir_
        
    
    @staticmethod
    def _open_file(path):
        """ This method opens the file to the given path and converts it into
        a pandas.DataFrame
        
        Parameters
        ----------
        path : str
            The relative path to the file
        
        Returns
        -------
        df : pandas.DataFrame
            The dataframe which contains all words and sentiments
        
        """
        if ".xlsx" in path or ".xls" in path:
            try:
                file = pd.read_excel(path, sheet_name=None)
            except:
                raise ValueError(f'{WordLists.PATH_LM} does not exist.')
        elif ".txt" in path:
            try: 
                file = open(path, 'r').readlines()
                file = [f.replace('\n', '').upper() for f in file]
            except:
                raise ValueError(f'{WordLists.PATH_FOG} does not exist.')
        return file
    