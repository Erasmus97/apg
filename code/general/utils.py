#!/usr/bin/python3

import time
import sys
import os
import io
import copy
import re

from nltk import tokenize
from urllib.request import urlretrieve
from urllib.request import urlopen
from linearmodels import PanelOLS
from datetime import datetime

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.style as style
import statsmodels.api as sm2

from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.decomposition import NMF, LatentDirichletAllocation

import warnings

from pandas.core.common import SettingWithCopyWarning
from linearmodels.shared.exceptions import MissingValueWarning

warnings.simplefilter(action='ignore', category=SettingWithCopyWarning)
warnings.simplefilter(action='ignore', category=MissingValueWarning)


################################################
# PATHS
################################################

DATA_PATH = '../data/'
LETTERS_PATH = DATA_PATH + 'letters/'
DATABASE_PATH = LETTERS_PATH + 'CRSP_CIK.csv'

SUMMARIES_PATH = DATA_PATH + 'summaries/'
RESULTS_PATH = '../results/'

TRADING_PATH = RESULTS_PATH + 'TF_IDF/'

CRSP_PATH = DATA_PATH + 'CRSP/'
CRSP_DATA_PATH = CRSP_PATH + 'CIK_merged.csv'

LOG_PATH = DATA_PATH + '.log'

################################################
# WORD LISTS
################################################

PERSONAL_WORDS = ['I', 'ME', 'MYSELF', 'MY', 'MINE', 'WE', 'OUR', 'OURS', 'OURSELVES']

STOPWORDS = ['ME', 'MY', 'MYSELF', 'WE', 'OUR', 'OURS', 'OURSELVES', 'YOU',
             'YOUR', 'YOURS', 'YOURSELF', 'YOURSELVES', 'HE', 'HIM', 'HIS',
             'HIMSELF', 'SHE', 'HER', 'HERS', 'HERSELF','IT', 'ITS', 'ITSELF',
             'THEY', 'THEM', 'THEIR', 'THEIRS', 'THEMSELVES', 'WHAT', 'WHICH',
             'WHO', 'WHOM', 'THIS', 'THAT', 'THESE', 'THOSE', 'AM', 'IS', 'ARE',
             'WAS', 'WERE', 'BE', 'BEEN', 'BEING', 'HAVE', 'HAS', 'HAD', 'HAVING',
             'DO', 'DOES', 'DID', 'DOING', 'AN', 'THE', 'AND', 'BUT', 'IF',
             'OR', 'BECAUSE', 'AS', 'UNTIL', 'WHILE', 'OF', 'AT', 'BY',
             'FOR', 'WITH', 'ABOUT', 'BETWEEN', 'INTO', 'THROUGH', 'DURING',
             'BEFORE', 'AFTER', 'ABOVE', 'BELOW', 'TO', 'FROM', 'UP', 'DOWN',
             'IN', 'OUT', 'ON', 'OFF', 'OVER', 'UNDER', 'AGAIN', 'FURTHER',
             'THEN', 'ONCE', 'HERE', 'THERE', 'WHEN', 'WHERE', 'WHY', 'HOW',
             'ALL', 'ANY', 'BOTH', 'EACH', 'FEW', 'MORE', 'MOST', 'OTHER',
             'SOME', 'SUCH', 'NO', 'NOR', 'NOT', 'ONLY', 'OWN', 'SAME', 'SO',
             'THAN', 'TOO', 'VERY', 'CAN', 'JUST', 'SHOULD', 'NOW', 'THE', 'A']

STARTERS = ['SHAREHOLDER', 'INVESTOR', 'SHAREOWNER']
ENDINGS = ['SINCERELY', 'YOURS TRULY', 'GOOD FORTUNE', 'RESPECTFULLY YOURS', 'CORDIALLY',
           'YOURS,', '/s/', '<SIGNED>', 'RESPECTFULLY', 'FAITHFULLY', 'BEST REGARDS', 'KIND REGARDS',
           'HIGHEST REGARDS', 'WARMEST REGARDS', 'YOURS IN CHRIST', 'ALL THE BEST']
TITLES_SHORT = ['CEO', 'CFO', 'CTO', 'CIO', 'CFA', 'PRESIDENT', 'CHAIRMAN', 'LLC']
TITLES_SHORT = TITLES_SHORT + [title + ',' for title in TITLES_SHORT]
TITLES_FULL = ['PORTFOLIO MANAGER', 'VICE PRESIDENT', 'FUND MANAGER', 'CHIEF INVESTMENT OFFICER',
               'CHIEF INVESTMENT STRATEGIST', 'MANAGING DIRECTOR', 'MANAGEMENT TEAM', 'PORTFOLIOMANAGER']
DISCLAIMERS = ['WE THANK YOU FOR YOUR INVESTMENT AND LOOK FORWARD TO YOUR CONTINUED CONFIDENCE',
               'IN CLOSING, WE THANK YOU FOR YOUR CONTINUED SUPPORT',
               'THIS SECTION SHOWS YOUR FUNDS PERFORMANCE',
               'TOTAL RETURN INCLUDES REINVESTMENT OF DIVIDENDS AND ANY CAPITAL GAINS PAID',
               'WHAT IS THE FUNDS CURRENT STRATEGY?', "FUND'S EXPENSES",
               'FUND S EXPENSES', 'EXPENSE EXAMPLE', 'EXPENSES EXAMPLE', 'FUNDS EXPENSES', '(UNAUDITED)',
               'FUND FEES', 'HYPOTHETICAL EXAMPLE', 'HYPOTHETICAL INVESTMENT',
               'THIS SECTION SHOWS YOUR FUNDS PERFORMANCE']

TOTAL_WORDS = STOPWORDS + PERSONAL_WORDS

################################################
# METHODS THAT DOWNLOAD FILES
################################################


def download_to_file(url, file_name):
    """
    Downloads a file from 'url' and write to '_fname'
    :param url: hyperlink where content is downloaded from
    :type url: str
    :param file_name: path where content will be written to
    :type file_name: str
    """
    number_of_tries = 10
    sleep_time = 10  # Note sleep time accumulates according to err

    for i in range(1, number_of_tries + 1):
        try:
            urlretrieve(url, file_name)
            return
        except Exception as exc:
            if i == 1:
                print('\n==>urlretrieve error in download_to_file.py')
            print('  {0}. _url:  {1}'.format(i, url))
            print('     _fname:  {0}'.format(file_name))
            print('     Warning: {0}  [{1}]'.format(str(exc), time.strftime('%c')))
            if '404' in str(exc):
                break
            print('     Retry in {0} seconds'.format(sleep_time))
            time.sleep(sleep_time)
            sleep_time += sleep_time

    print('\n  ERROR:  Download failed for')
    print('          url:  {0}'.format(url))
    print('          _fname:  {0}'.format(file_name))


def download_to_doc(url, _f_log=None):
    """
    Downloads url content to string doc
    :param url: hyperlink where content is downloaded from
    :type url: str
    :param _f_log: path to log file
    :type _f_log: str
    """

    number_of_tries = 10
    sleep_time = 5  # Note sleep time is cumulative over loop

    for i in range(1, number_of_tries + 1):
        try:
            doc = urlopen(url).read().decode('utf-8', errors='ignore')
            return
        except Exception as exc:
            if i == 1:
                print('\n==>urlopen error in download_to_doc.py')
            print('  {0}. _url:  {1}'.format(i, url))
            print('     Warning: {0}  [{1}]'.format(str(exc), time.strftime('%c')))
            if '404' in str(exc):
                break
            print('     Retry in {0} seconds'.format(sleep_time))
            time.sleep(sleep_time)
            sleep_time += sleep_time

    print('\n  ERROR:  Download failed for url: {0}'.format(url))
    if _f_log:
        _f_log.write('ERROR:  Download failed=>  _url: {0:75}'.format(url))
        _f_log.write('  |  {0}\n'.format(time.strftime('%c')))


################################################
# METHODS THAT FIND STUFF INSIDE DOCUMENTS
################################################


def tag_search(tags, file):
    """
    Finds the data belonging to a tag
    :param tags: list of tags from CSV file
    :type tags: list
    :param file: path to file that contains possibly a shareholder letter
    :type file: str
    :return: dictionary of tags
    :rtype: dict
    """

    dict_tags = {
        'ticker': '<CLASS-CONTRACT-TICKER-SYMBOL>',
        'series-id': '<SERIES-ID>',
        'name': '<CLASS-CONTRACT-NAME>',
        'contract-id': '<CLASS-CONTRACT-ID>',
        'cik': '<OWNER-CIK>',
        'report-date': 'CONFORMED PERIOD OF REPORT:',
        'file-date': 'FILED AS OF DATE:'
    }

    # Get correct tag
    dict_ = {tag: [] for tag in tags}

    # Reset reading file
    file = open(file, 'r').readlines()

    # Find lines with a tag
    for line in file:
        for tag in dict_:
            if dict_tags[tag] in line.upper():
                line = line.replace(dict_tags[tag], '')  # remove tag
                line = line.replace('\n', '') # remove line ends
                line = line.replace('\t', '')  # remove line tabs
                try:
                    dict_[tag].append(line)
                except:
                    pass

    return dict_


################################################
# METHODS THAT PRINT STUFF
################################################


def update_progress(progress):
    """
    Creates a progress bar in the console
    :param progress: progress in percentages
    :type progress: float
    """
    bar_length = 20 # Modify this to change the length of the progress bar
    status = ""
    if isinstance(progress, int):
        progress = float(progress)
    if not isinstance(progress, float):
        progress = 0
        status = "error: progress var must be float\r\n"
    if progress < 0:
        progress = 0
        status = "Halt...\r\n"
    if progress >= 1:
        progress = 1
        status = "Done...\r\n"
    block = int(round(bar_length*progress))
    text = "\rPercent: [{0}] {1}% {2}".format( "#"*block + "-"*(bar_length-block), round(progress*100,3), status)
    sys.stdout.write(text)
    sys.stdout.flush()


################################################
# METHODS THAT WORK WITH DIRECTORIES
################################################


def get_files(path, ext=None):
    """
    This method returns all files in a directory that is given by the path.

    :param path: the relative path to a directory (default None)
    :type path: str
    :param ext: extension of the file (e.g. `.txt`, `.pdf`, `.xlsx`)
    :type ext: str
    :return: a list of all files in the directory
    :rtype: list
    """
    if ext is not None and '.' in ext:
        files = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
        return [f for f in files if ext in f]
    return [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]


def get_directories(path):
    """
    This method gets all directories in a given path
    :param path: the relative path to a directory (default None)
    :type path: str
    :return: a list of directories in the current directory
    :rtype: list
    """
    dirs = [name + '/' for name in os.listdir(path) if os.path.isdir(os.path.join(path, name))]

    # We are not interested in certain files --> remove that from possible paths
    try:
        dirs.remove('Log_Files')
    except ValueError:
        pass
    return dirs


def get_path_files(our_root, ext=None):
    """
    This method returns all files from the current directory onwards.

    :param our_root: the path from which we start to go downwards
    :type our_root: str
    :param ext: extension of the file (e.g. `.txt`, `.pdf`, `.xlsx`)
    :type ext: str
    :return: a list with all relative paths that contains a file
    :rtype: list
    """
    lst = []
    for root, dirs, files in os.walk(our_root):
        if ext is None:
            lst += [f'{root}/{file}' for file in files if not file.endswith('.DS_Store')]
        else:
            lst += [f'{root}/{file}' for file in files if file.endswith(ext)]

    return lst


def merge_csv(original_path, ext_root, ext):
    """
    This method merges all CSV files in a given directory
    :param original_path: path to original CSV file that must not be merged
    :type original_path: str
    :param ext_root: path where all CSV files must be merged
    :type ext_root: str
    :param ext: ending of path of CSV file
    :type ext: str
    """
    # Get all relative paths to the CSV files
    ext_paths = get_path_files(ext_root, ext)

    # Remove the paths from the list that are the same as the original CSV file
    ext_paths = [path for path in ext_paths if path.split('/')[-1] != original_path.split('/')[-1]]
    ext_paths = [path for path in ext_paths if '_v2' not in path]
    ext_paths.sort()

    # Stack our CSV files to empty CSV file
    df = None
    for path_idx in range(len(ext_paths)):
        path = ext_paths[path_idx]
        print(f'Adding file: {path}   |   {round(100*path_idx/len(ext_paths), 3)}%')

        # Open our updated CSV file
        df_temp = pd.read_csv(path)
        df_temp = df_temp[~pd.isnull(df_temp['file-name'])]
        if df is None:
            df = df_temp
        else:
            df = pd.concat([df, df_temp])
        df.reset_index(inplace=True, drop=True)

    # Drop useless column and write to CSV file
    try:
        df.drop(['Unnamed: 0'], axis=1, inplace=True)
    except:
        pass
    df.to_csv(original_path.replace('.csv', '_v3.csv'), index=False)


def write_line_to_text(file, message):
    """
    Adds a line to a text file
    :param file: path to text file
    :type file: str
    :param message: line that needs to be appended
    :type message: str
    """
    file_object = open(file, 'a')
    file_object.writelines(message +'\n')
    file_object.close()


def set_file_size(path, size=65000, ext='.txt'):
    """
    This method reduces the size of a given file type
    :param path: path to the files
    :type path: str
    :param size: size (in bytes) to which the file must be reduced
    :type size: int (optional: float)
    :param ext: file extension/type
    :type ext: str
    """
    if path.split('.')[-1] != ext[1:]:
        raise ValueError('extension of file is not correct.')

    if os.path.getsize(path) > size:
        # Open file --> could not work if there are unknown characters
        try:
            file = io.open(path, 'r', encoding='utf-8').readlines()
        except:
            return

        while True:
            # Remove last line
            file.pop()

            # Overwrite file
            with open(path, 'w') as f:
                for line in file:
                    try:
                        f.write(f"{line}\n")
                    except:
                        pass
            print(f'\rProcessing file: {path}     |       {os.path.getsize(path)}', end='')

            # Check if we are done
            if os.path.getsize(path) <= size:
                break
            elif os.path.getsize(path) >= 10*size:
                file = file[:int(len(file)*0.3)]
            elif os.path.getsize(path) >= 5*size:
                file = file[:int(len(file)*0.4)]
            elif os.path.getsize(path) >= 1.5*size:
                file = file[:int(len(file)*0.5)]
        print()
        print(f'New file size of file: {path}     |       {os.path.getsize(path)}\n')


def remove_duplicate(file):
    """
    Removes duplicate shareholder letters from file.
    :param file: text file with a letter
    :type file: File
    :return: cleaned file
    """
    dear = []
    new_file = None
    lines=file
    # Loop over lines in file
    i = 0
    while i < len(lines):
        line = lines[i]
        # Find start of shareholder letter
        if ('DEAR' in line.upper() and any(ext in line.upper() for ext in STARTERS)) or ('DEAR FELLOW' in line.upper()):
            dear.append(i)
            # Split starting sentence of letter
            sentence = re.split('DEAR|Dear|dear',line)
            if sentence[0] != '':
                lines[i] = sentence[0]
                lines.insert(i+1,'Dear' + sentence[1])
                dear[-1] = dear[-1]+1
                i+=1
        i+=1

    # Clean file if more than one letter
    if len(dear)>1:
        # Select first letter
        letter = lines[:dear[1]]
        # Remove empty lines
        letter[:] = (line for line in letter if line !='\n')
        new_file = letter
        # Find words in main letter
        all_letters = []
        letter_words = set()
        for line in letter:
            letter_words = set(letter_words).union(set(line.split()))
        all_letters.append(letter_words)
        # Loop over other letters
        for j in range(len(dear)-1):
            if j != len(dear)-2:
                next_letter = lines[dear[j+1]:dear[j+2]]
            else:
                next_letter = lines[dear[j+1]:]
            # Remove empty lines
            next_letter[:] = (line for line in next_letter if line !='\n')

            # Find words in other letter
            next_letter_words = set()
            for line in next_letter:
                next_letter_words = set(next_letter_words).union(set(line.split()))
            # Find common words
            ratio = []
            for i in range(len(all_letters)):
                intersection = all_letters[i].intersection(next_letter_words)
                ratio.append(len(intersection)/len(all_letters[i]))
            # Check similarity of files
            if max(ratio) < 0.9:
                 new_file = new_file + next_letter
                 all_letters.append(next_letter_words)
    else:
        new_file = file
    return new_file

