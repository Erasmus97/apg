import pandas as pd
import numpy as np
from copulas.bivariate import Clayton, Frank
from scipy.stats import t, uniform
import scipy.stats as stats
from sklearn.neighbors import KernelDensity
from datetime import datetime as dt

import data_analysis.baseline_results as base_res

# -----------------------  Plotting stuff-----------------------
import matplotlib.pyplot as plt
plt.style.use('seaborn-deep')  # PLOT STYLE

# -----------------------
# User defined parameters
# -----------------------

# Select type of weights
SCORES_TF_IDF = True            # Boolean: if TRUE, scores are computed using TF-IDF

# Select robustness check
SINGLE_FUND_ONLY = False        # Boolean: if TRUE, only consider fund-specific shareholder letters
SUMMARIZATION_SCORES = False    # Boolean: if TRUE, consider summarized letter scores
CRISIS_SAMPLE = False           # Boolean: if TRUE, consider crisis subsample

# Save files to bootstrap folder
bSaveCopulaTest = False

# Crisis start and end date
crisis_start_date = dt.strptime(str(int(float('20070101'))), '%Y%m%d').date()
crisis_end_date = dt.strptime(str(int(float('20081231'))), '%Y%m%d').date()

# Print settings
print('TF IDF: ' + str(SCORES_TF_IDF))
print('Single Fund: ' + str(SINGLE_FUND_ONLY))
print('Summarization: ' + str(SUMMARIZATION_SCORES))
print('Crisis Sample: ' + str(CRISIS_SAMPLE))

# -----------------------Paths-----------------------
if not SCORES_TF_IDF:
    PARENT_PATH = '../../results/proportional/'
    RESULTS_FILENAME = 'scores_proportional.csv'
    RESULTS_MERGED_FILENAME ='scores_proportional_merged.csv'
else:
    PARENT_PATH = '../../results/TF_IDF/'
    RESULTS_FILENAME = 'scores_tf-idf.csv'
    RESULTS_MERGED_FILENAME = 'scores_tf-idf_merged.csv'

if SUMMARIZATION_SCORES:
    PARENT_PATH = PARENT_PATH + 'summarization/'
    RESULTS_FILENAME = 'summary_tf-idf.csv'
    RESULTS_MERGED_FILENAME = 'summary_tf-idf_merged.csv'

RESULTS_FILENAME = PARENT_PATH + RESULTS_FILENAME
RESULTS_MERGED_FILENAME = PARENT_PATH + RESULTS_MERGED_FILENAME

if SINGLE_FUND_ONLY:
    PATH_RESULTS = PARENT_PATH + 'single_funds/'
elif CRISIS_SAMPLE:
    PATH_RESULTS = PARENT_PATH + 'crisis_subsample/'
else:
    PATH_RESULTS = PARENT_PATH + 'baseline/'

PATH_DATA = '../../data/'
PATH_CRSP = '../../data/CRSP/'          # Set relative paths for CRSP data files
PATH_EDGAR = '../../data/EDGAR/'
PATH_LETTERS = '../../data/letters/'    # Set relative paths for letter files
PATH_FIGURES = PATH_RESULTS + 'Figures/'
PATH_COPULA = PATH_RESULTS + 'Copula/'

# -----------------------CRSP processed filenames-----------------------
CRSP_CIK_FILENAME = PATH_CRSP + 'CIK_merged.csv'
TICKER_FILENAME = PATH_CRSP + 'tickers.txt'
COMBINED_FILENAME = PATH_CRSP + 'returns_factors_combined.csv'
FUND_PERF_MEASURES_FILENAME = PATH_CRSP + 'fund_performance_measures.csv'

# -----------------------Results filenames-----------------------
RESULTS_WINSORIZED_FILENAME = PATH_RESULTS + 'results_winsorized.csv'
RESULTS_COMPLETE_FILENAME = PATH_RESULTS + 'results_complete.csv'

# -----------------------Output filenames-----------------------
DESCRIPTIVES_TAIL_DEP_FILENAME = PATH_RESULTS + 'descriptives_tail_dep.xlsx'
COPULA_OUTPUT_FILENAME = PATH_RESULTS + 'copula_output.csv'

if bSaveCopulaTest:
    PATH_SAVE_RESULTS = PATH_RESULTS.replace('results', 'copula_test')

#
# * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * +

def get_distributions(save_directory=PATH_FIGURES):
    """
    Analyzes distributions of textual scores and performance measures
    :param save_directory: path where results need to be saved
    :type save_directory: str
    """
    # Read csv file with fund characteristics, performance measures and textual scores
    df_results = pd.read_csv(RESULTS_COMPLETE_FILENAME)

    # Variables of interest (different for summarized letters)
    dep_vars = ['Future Return', 'Future Fund Flow', 'Alpha', 'Volatility']
    if SUMMARIZATION_SCORES:
        indep_abs_vars = ['Negativity', 'Confidence', 'Similarity']
        indep_surprise_vars = ['Negativity Surprise', 'Confidence Surprise']
    else:
        indep_abs_vars = ['Negativity', 'Confidence', 'Personalness', 'Complexity', 'Similarity']
        indep_surprise_vars = ['Negativity Surprise', 'Confidence Surprise', 'Complexity Surprise', 'Personalness Surprise']
    cols = [val for sublist in [dep_vars, indep_abs_vars, indep_surprise_vars] for val in sublist]

    # Create histograms for marginal distributions of all measures
    for measure in cols:
        data = df_results[measure].dropna()
        data = data[(data > np.percentile(data, 0.5)) & (data < np.percentile(data, 99.5))]

        fig = plt.figure()
        plt.hist(data, bins=40, density=False, alpha=0.6, color='g') # Plot the histogram
        title = measure
        plt.title(title)

        plotName = save_directory + measure + '_histogram' + '.eps'
        plt.savefig(plotName, format='eps')


def get_tail_dependence(input=None, alpha=0.10):
    """
    Estimates empirical upper and lower tail dependence matrices for dataframe
    :param input: if given, the data set that is used
    :type input: pandas.DataFrame
    :param alpha: probability bound for the tail dependence
    :type alpha: float
    :return: lower and upper tail dependence
    :rtype: tuple
    """
    if input is None:
        # Read csv file with fund characteristics, performance measures and textual scores
        df_results = pd.read_csv(RESULTS_COMPLETE_FILENAME)

        # Variables of interest (different for summarized letters)
        dep_vars = ['Future Return', 'Future Fund Flow', 'Alpha', 'Volatility']
        if SUMMARIZATION_SCORES:
            indep_abs_vars = ['Negativity', 'Confidence', 'Similarity']
        else:
            indep_abs_vars = ['Negativity', 'Confidence', 'Personalness', 'Complexity', 'Similarity']
        cols = [val for sublist in [dep_vars, indep_abs_vars] for val in sublist]
        df_series = df_results[cols]
    else:
        df_series = input

    # Estimate lower tail dependence matrix
    in_lower_tail = df_series < df_series.quantile(q=alpha)
    probs_lower = pd.concat([in_lower_tail[in_lower_tail[col] == True].mean()
                       for col in in_lower_tail], axis=1)
    probs_lower.columns = probs_lower.index

    # Estimate upper tail dependence matrix
    in_upper_tail = df_series > df_series.quantile(q=(1-alpha))
    probs_upper = pd.concat([in_upper_tail[in_upper_tail[col] == True].mean()
                       for col in in_upper_tail], axis=1)
    probs_upper.columns = probs_upper.index

    if input is None:
        '''Write descriptives to .xlsx'''
        with pd.ExcelWriter(DESCRIPTIVES_TAIL_DEP_FILENAME) as writer:
            probs_lower.to_excel(writer, sheet_name='Lower tail dependence')
            probs_upper.to_excel(writer, sheet_name='Upper tail dependence')
    else:
        if probs_upper.size == 4:
            probs_upper = (probs_upper.iloc[0,1] + probs_upper.iloc[1,0])/2
            probs_lower = (probs_lower.iloc[0,1] + probs_lower.iloc[1,0])/2
        return probs_lower, probs_upper


def fit_copula(data, dist_marginal='Empirical', kernel=None, bandwidth=0.2, copula_type=None, bForecast=False,
               x_out_of_sample=None, bSimulate=False, nSim=1000, indep_var=None, dep_var=None, bSaveCopulaTest=False):
    """
    This method fits the data to a copula
    """

    # Extract variables from dataframe
    x = data.iloc[:, 0] # independent variable (textual score)
    y = data.iloc[:, 1] # dependent variable (performance measure)
    df = pd.DataFrame({'x': x, 'y': y})
    df_unique = df.drop_duplicates(subset=['x'])
    x_unique = df_unique['x']  # Consider unique observations only
    y_unique = df_unique['y']  # Consider unique observations only

    if bSaveCopulaTest:
        print(str(len(x) - len(x_unique)) + ' observations deleted.')

    # Fit marginal distributions to data observations and transform data
    if dist_marginal == 'Empirical':
        # Fit empirical distribution function to x and y
        if kernel is not None:
            kde = KernelDensity(bandwidth=bandwidth, kernel=kernel).fit(x[:, np.newaxis])
            xi = np.linspace(x.min(), x.max(), np.array(x).shape[0])
            log_pdf = kde.score_samples(xi[:, np.newaxis])
            x = np.exp(log_pdf)

        quantiles_x, cumprob_x = base_res.convert_to_uniform(x, dropna=True, drop_duplicates=False)
        quantiles_y, cumprob_y = base_res.convert_to_uniform(y, dropna=True, drop_duplicates=False)
        df_temp = pd.DataFrame({'x': quantiles_x, 'cumprob_x': cumprob_x})
        df = pd.merge(df, df_temp, on='x')
        df_temp = pd.DataFrame({'y': quantiles_y, 'cumprob_y': cumprob_y})
        df = pd.merge(df, df_temp, on='y')

        # Transform variables to U(0, 1)
        x_unif = df['cumprob_x']
        y_unif = df['cumprob_y']

        # If forecasting, repeat for out-of-sample (use in-sample EDF)
        if bForecast:
            # x_out_of_sample = np.expand_dims(x_out_of_sample, axis=1)
            x_unif_out_of_sample = np.apply_along_axis(get_edf, 0, x_out_of_sample, np.array([quantiles_x, cumprob_x]).T)
    elif dist_marginal == 'Student-t':
        # Fit Student's t-distribution to data observations
        t_params_1 = t.fit(x)
        t_params_2 = t.fit(y)

        # Standardize variables
        x_std = (x - t_params_1[1]) / t_params_1[2]
        y_std = (y - t_params_2[1]) / t_params_2[2]

        # Transform variables to U(0, 1)
        x_unif = t.cdf(x_std, t_params_1[0])
        y_unif = t.cdf(y_std, t_params_2[0])

        # If forecasting, repeat for out-of-sample (use in-sample parameter estimates)
        if bForecast:
            x_std_out_of_sample = (x_out_of_sample - t_params_1[1]) / t_params_1[2]
            x_unif_out_of_sample = t.cdf(x_std_out_of_sample, t_params_1[0])

    # Collect transformed data
    data_transformed = pd.DataFrame({'u1': x_unif, 'u2': y_unif})
    data_transformed_unique = data_transformed.drop_duplicates(subset=['u1'])
    x_unif_unique = data_transformed_unique['u1']  # Consider unique observations only
    y_unif_unique = data_transformed_unique['u2']  # Consider unique observations only

    # Write uniform marginals to CSV file
    if bSaveCopulaTest:
        TRANSFORMED_DATA_FILENAME = PATH_SAVE_RESULTS + indep_var + '_' + dep_var + '.csv'
        data_transformed.to_csv(TRANSFORMED_DATA_FILENAME, index=False)

    # Create copula objects and fit to data
    if bSimulate == False:
        # Only fit copula and return theta and Kendall's tau
        if copula_type == 'Clayton':
            cop_obj = Clayton()
        elif copula_type == 'Frank':
            cop_obj = Frank()

        cop_obj.fit(data_transformed.to_numpy())
        theta = cop_obj.theta
        tau_implied = cop_obj.tau
        tau, pval = stats.kendalltau(x_unif, y_unif)

        # Return theta and implied Kendall's tau
        output = {'theta': theta, 'Kendall': tau_implied, 'p-value': pval}
    else:
        # Select best copula based on AIC fit
        cop_obj_cl = Clayton()                          # Clayton
        cop_obj_cl.fit(data_transformed.to_numpy())
        cop_obj_fr = Frank()                            # Frank
        cop_obj_fr.fit(data_transformed.to_numpy())

        # Extract estimated theta parameter
        theta_cl = cop_obj_cl.theta
        theta_fr = cop_obj_fr.theta

        # Simulate standard uniform observations
        if bForecast:
            # For forecast, simulate out-of-sample observations for out-of-sample x-values
            nObs = len(x_unif_out_of_sample)
            u1 = np.expand_dims(x_unif_out_of_sample, axis=1)
        else:
            # For normal fit, simulate in-sample observations for all unique x-values
            nObs = len(x_unif_unique)
            u1 = np.expand_dims(x_unif_unique, axis=1)

        v1 = u1 * np.ones((1, nSim))
        v2 = uniform.rvs(size=(nObs, nSim))

        # Simulate observations from conditional copula
        u2_cl = compute_cond_copula_fcst(v1, v2, theta_cl, 'Clayton')
        u2_fr = compute_cond_copula_fcst(v1, v2, theta_fr, 'Frank')

        # Compute AICs for all copulas and choose best
        AIC_cl_normal = compute_copula_aic(cop_obj_cl, u1, u2_cl)
        AIC_cl_reverse = compute_copula_aic(cop_obj_cl, 1 - u1, 1 - u2_cl)
        AIC_fr = compute_copula_aic(cop_obj_fr, u1, u2_fr)

        # Choose copula with lowest AIC
        if AIC_cl_normal < AIC_cl_reverse and AIC_cl_normal < AIC_fr:
            copula_choice = 'Clayton'
            theta = theta_cl
            u2 = u2_cl
        elif AIC_cl_reverse < AIC_cl_normal and AIC_cl_reverse < AIC_fr:
            copula_choice = 'Reverse Clayton'
            theta = theta_cl
            u1 = 1 - u1
            u2 = 1 - u2_cl
        else:
            copula_choice = 'Frank'
            theta = theta_fr
            u2 = u2_fr

        # Estimate Kendall's tau with p-value
        tau, pval = stats.kendalltau(x_unif, y_unif)

        # Transform simulated observations to back to original marginal distributions
        if dist_marginal == 'Empirical':
            x_out = np.apply_along_axis(inverse_edf, 0, u1, np.array([quantiles_x, cumprob_x]).T)
            y_out_mean = np.apply_along_axis(inverse_edf, 0, np.mean(u2, axis=1), np.array([quantiles_y, cumprob_y]).T)
            y_out_std = np.apply_along_axis(inverse_edf, 0, np.std(u2, axis=1), np.array([quantiles_y, cumprob_y]).T)
            y_out_lower = np.apply_along_axis(inverse_edf, 0, np.quantile(u2, 0.025, axis=1), np.array([quantiles_y, cumprob_y]).T)
            y_out_upper = np.apply_along_axis(inverse_edf, 0, np.quantile(u2, 0.975, axis=1), np.array([quantiles_y, cumprob_y]).T)
        elif dist_marginal == 'Student-t':
            x_out = t.ppf(u1, t_params_1[0], t_params_1[1], t_params_1[2])
            y_out_mean = t.ppf(np.mean(u2, axis=1), t_params_2[0], t_params_2[1], t_params_2[2])
            y_out_std = t.ppf(np.std(u2, axis=1), t_params_2[0], t_params_2[1], t_params_2[2])
            y_out_lower = t.ppf(np.quantile(u2, 0.025, axis=1), t_params_2[0], t_params_2[1], t_params_2[2])
            y_out_upper = t.ppf(np.quantile(u2, 0.975, axis=1), t_params_2[0], t_params_2[1], t_params_2[2])

        # Collect and return output
        if bForecast:
            output = y_out_mean
        else:
            df_results = pd.DataFrame({'x': x_unique, 'y': y_unique, 'y_out_mean': y_out_mean, 'y_out_std': y_out_std, 'y_out_lower': y_out_lower, 'y_out_upper': y_out_upper})
            output = {'copula_choice': copula_choice, 'theta': theta, 'Kendall': tau, 'p-value': pval, 'fitted_values': df_results}

    return output


def inverse_edf(u_value, EDF_table):
    """
    Returns inverse from empirical distribution function
    """
    index = np.searchsorted(EDF_table[:, 1], u_value)

    # Determine inverse using linear interpolation
    index[index > EDF_table.shape[0] - 1] = EDF_table.shape[0] - 1  # Set values larger than in EDF_table equal to max
    index[index < 1] = 1                                            # Set values smaller than in EDF_table equal to min
    range_y = EDF_table[index, 0] - EDF_table[index - 1, 0]
    range_x = EDF_table[index, 1] - EDF_table[index - 1, 1]
    inverse_value = EDF_table[index - 1, 0] + (u_value - EDF_table[index - 1, 1]) / range_x * range_y

    return inverse_value


def get_edf(x_value, EDF_table):
    """
    Returns empirical distribution function value
    """
    ''''''
    index = np.searchsorted(EDF_table[:, 0], x_value)

    # Determine inverse using linear interpolation
    index[index>EDF_table.shape[0]-1] = EDF_table.shape[0]-1  # Set values larger than in EDF_table equal to max
    index[index<1] = 1                                        # Set values smaller than in EDF_table equal to min
    range_y = EDF_table[index, 1] - EDF_table[index - 1, 1]
    range_x = EDF_table[index, 0] - EDF_table[index - 1, 0]
    edf_value = EDF_table[index - 1, 1] + (x_value - EDF_table[index - 1, 0]) / range_x * range_y

    return edf_value


def compute_cond_copula_fcst(v1, v2, theta, type):
    """
    Computes forecast depending on copula type
    """
    ''''''
    if type == 'Clayton':
        u2 = (v1 ** (-theta) * (v2 ** ((-theta) / (theta + 1)) - 1) + 1) ** (-1 / theta)
    elif type == 'Frank':
        u2 = -1 / theta * np.log( 1 + (v2 * (1 - np.exp(-theta))) / (v2 * (np.exp(-theta*v1) - 1) - np.exp(-theta * v1)) )
    else:
        u2 = None

    return u2


def compute_copula_aic(copula_obj, u1, u2, k=1):
    """
    Computes AIC for copula fitted to data
    """
    # Replace zeros by very small number and set u2 equal to mean across simulations
    u1[u1 == 0] = 10**(-6)
    u2_mean = np.expand_dims(np.mean(u2, axis=1), axis=1)
    u2_mean[u2_mean == 0] = 10 ** (-6)

    # Compute copula pdf based on data
    pdf = copula_obj.probability_density(X=np.concatenate((u1, u2_mean), axis=1))

    # Compute AIC
    log_pdf = np.log(pdf)
    aic = 2*k - 2*np.sum(log_pdf)

    return aic


def run_copula(dist_marginal='Empirical', kernel=None, bandwidth=0.2, bSimulate=True, nSim=1000, bSaveCopulaTest=False):
    """ Runs copula analysis for various combinations of performance measures and textual scores """
    # Read results (different for summarized letters)
    df_results = pd.read_csv(RESULTS_COMPLETE_FILENAME)
    if SUMMARIZATION_SCORES:
        indep_abs_vars = ['Negativity', 'Confidence', 'Similarity']
        indep_rel_vars = ['Negativity Surprise', 'Confidence Surprise']
    else:
        indep_abs_vars = ['Negativity', 'Confidence', 'Personalness', 'Complexity', 'Similarity']
        indep_rel_vars = ['Negativity Surprise', 'Confidence Surprise', 'Complexity Surprise', 'Personalness Surprise']
    dep_vars = ['Future Return', 'Future Fund Flow', 'Alpha', 'Volatility']
    cols = ['Performance Measure', 'Textual Score', 'Copula Type', 'Theta', 'Kendall', 'p-value', 'Kendall fit']
    df_summary_results = pd.DataFrame(columns=cols)

    for dep_var in dep_vars:
        print(dep_var)
        COPULA_FILENAME = PATH_COPULA + dep_var + '.xlsx'

        # Perform copula analysis and write to file
        with pd.ExcelWriter(COPULA_FILENAME) as writer:
            for indep_var in (indep_abs_vars+indep_rel_vars):
                print(indep_var)
                df_input = df_results[[indep_var, dep_var]].dropna(inplace=False)
                df_input = df_input.reset_index(drop=True)

                res_copula = fit_copula(df_input, dist_marginal=dist_marginal, kernel=kernel, bandwidth=bandwidth,
                                        bSimulate=bSimulate, nSim=nSim, indep_var=indep_var, dep_var=dep_var, bSaveCopulaTest=bSaveCopulaTest)
                df_output = res_copula['fitted_values']
                df_output.to_excel(writer, sheet_name=indep_var, index=False)

                # Compute Kendall's tau between fitted and observed values
                kendalls_tau = df_output[['y', 'y_out_mean']].corr(method='kendall').iloc[0,1]

                # Append copula estimation output to dataframe
                summary_output = pd.Series({'Performance Measure': dep_var, 'Textual Score': indep_var,
                                            'Copula Type': res_copula['copula_choice'], 'Theta': res_copula['theta'],
                                            'Kendall': res_copula['Kendall'], 'p-value': res_copula['p-value'],
                                            'Kendall fit': kendalls_tau})
                df_summary_results = df_summary_results.append(summary_output, ignore_index=True)

    df_summary_results.to_csv(COPULA_OUTPUT_FILENAME, index=False)


def copula_over_time():
    """ Returns copula-implied Kendall's tau over time """
    # Read results (different for summarized letters)
    df_results = pd.read_csv(RESULTS_COMPLETE_FILENAME)
    if SUMMARIZATION_SCORES:
            indep_abs_vars = ['Negativity', 'Confidence', 'Similarity']
            indep_surprise_vars = ['Negativity Surprise', 'Confidence Surprise']
            indep_vars = indep_abs_vars + indep_surprise_vars
    else:
            indep_abs_vars = ['Negativity', 'Confidence', 'Personalness', 'Complexity', 'Similarity']
            indep_surprise_vars = ['Negativity Surprise', 'Confidence Surprise', 'Complexity Surprise', 'Personalness Surprise']
            indep_vars = indep_abs_vars + indep_surprise_vars
    dep_vars = ['Future Return', 'Future Fund Flow', 'Alpha', 'Volatility']

    # Convert date column to datetime format
    # df_results['Date'] = pd.to_datetime(df_results['Date'], format='%d/%m/%Y')
    df_results['Date'] = pd.to_datetime(df_results['Date'], format='%Y-%m-%d')
    date_list = df_results['Date'].unique()

    COPULA_OVER_TIME_FILENAME = PATH_COPULA + 'copula_over_time.xlsx'

    with pd.ExcelWriter(COPULA_OVER_TIME_FILENAME) as writer:
        for dep_var in dep_vars:
            df_copula_results = pd.DataFrame()
            df_copula_results['Date'] = date_list

            # Estimate copula on rolling window basis for all independent variables
            for indep_var in indep_vars:
                # Take subset of dataframe
                df_temp_results = pd.DataFrame(columns=['Date', indep_var])

                # Loop over dates and estimate copula on cross-section
                for date in date_list:
                    # Estimate copula for given date
                    df_temp = df_results.loc[df_results['Date'] == date]
                    df_temp = df_temp[[indep_var, dep_var]].dropna(inplace=False)
                    df_temp = df_temp.reset_index(drop=True)
                    if len(df_temp) > 0:
                        res_copula = fit_copula(df_temp, dist_marginal='Empirical', copula_type='Clayton', bForecast=False,
                                            x_out_of_sample=None, bSimulate=False, nSim=1000, indep_var=indep_var, dep_var=dep_var)
                        summary_output = pd.Series({'Date': date, indep_var: res_copula['Kendall']})
                    else:
                        summary_output = pd.Series({'Date': date, indep_var: None})

                    # Append copula estimation output to dataframe
                    df_temp_results = df_temp_results.append(summary_output, ignore_index=True)

                df_copula_results = pd.merge(df_copula_results, df_temp_results, on='Date')

            # Write copula estimation output to file
            df_copula_results = df_copula_results.sort_values(by='Date', ascending=True)
            df_copula_results.to_excel(writer, sheet_name=dep_var, index=False)


if __name__ == '__main__':
    get_tail_dependence()

