#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 20 10:22:54 2020

@author: W.F. van Driel
"""

import copy

import numpy as np
import pandas as pd


class TextAnalysis:

    def __init__(self, utils, word_list, file_list):
        self._link = []
        self._not_found_links = 0

        # Create additional attributes
        self._utils = utils
        self._word_list = word_list
        self._file_list = file_list
        self._df = None

        self.remove_invalid_funds()

    @property
    def df(self):
        return copy.deepcopy(self._df)

    @df.setter
    def df(self, value):
        self._df = value

    def remove_invalid_funds(self):
        """ This method removes invalid funds from the file list """
        # Our plots show that there are two letters that are outliers (wrongly scraped)
        # That is why we remove them from the file list
        paths = ['2009/QTR4/20091207_844779_1.txt', '2011/QTR1/20110304_893818_1.txt',]
        file_paths = self._file_list.file_names()
        invalid_paths = [path for path in file_paths if any(ext in path for ext in paths)]
        for path in invalid_paths:
            self._file_list.delete(path)

    @staticmethod
    def n_syllables(word):
        """
        This method calculates the number of syllables in a word using the method of Talburt (1986).
        :param word: A given word
        :type word: str
        :return: The number of syllables
        :rtype: int
        """

        word = word.upper()
        sylls = 0
        vowels = "AEIOUY"

        # If first letter is a vowel, we already count it
        if word[0] in vowels:
            sylls += 1

        for i in range(1, len(word)):
            if word[i] in vowels and word[i - 1] not in vowels:
                sylls += 1

            if word[i - 1:i + 1] == "IO":
                sylls += 1

        # John Talburt, 1986
        if (word.endswith("E") or word.endswith("ES") or word.endswith("ED")) and \
                ((not word.endswith("LE")) and (not word.endswith("TED") and (not word.endswith("DED")))):
            sylls -= 1

        if len(word) == 3:
            sylls = 1

        return max(1, sylls)

    @staticmethod
    def fog_index(n_words, n_complex, n_sent):
        """
        This method calculates the Fog index for a file.
        :param n_words: Number of words in the (cleaned) file.
        :type n_words: int
        :param n_complex: Number of complex words in the (cleaned) file.
            This integer depends on the fact if we are looking for the modified Fog index, or not.
        :type n_complex: int
        :param n_sent: Number of sentences in the (cleaned) file.
        :type n_sent: int
        :return: The Fog index.
        :rtype: float
        """
        return 0.4 * ((n_words / n_sent) + (n_complex / n_words))

    @staticmethod
    def cosine_similarity(weight1, weight2):
        """
        This method computes the cosine similarity of two weighting vectors.
        :param weight1: dictionary containing weighting scheme of a document
        :type dict
        :param weight2: dictionary containing weighting scheme of another document
        :type dict
        :return: cosine similarity between the two documents
        :rtype float
        """

        # Find shortest word list to efficiently loop through text
        w1 = weight1 if len(weight1) < len(weight2) else weight2
        w2 = weight2 if len(weight1) < len(weight2) else weight1

        # Compute denominator as product of L2-norms of weightings
        l1 = sum([i ** 2 for i in w1.values()]) ** (1 / 2.0)
        l2 = sum([i ** 2 for i in w2.values()]) ** (1 / 2.0)

        denominator = l1 * l2

        # Compute nominator as product of weightings over all similar words
        numerator = 0
        for word in w1:
            if word in w2:
                numerator += w1[word] * w2[word]
        return numerator / denominator

    def personalness(self, splitted, dict_pers):
        """
        Determines the number of occurences per personal word in the file.
        :param splitted: Each element in the list is a word in a sentence.
        :type splitted: list
        :param dict_pers: Old counter of personal words
        :type dict_pers: dict
        :return: Every key is a personal word with the number of occurrences as value
        :rtype: dict
        """

        for word in self._utils.PERSONAL_WORDS:
            count = splitted.count(word)

            if word not in dict_pers:
                dict_pers[word] = count
            else:
                dict_pers[word] += count

        return dict_pers

    def n_complex(self, word):
        """
        This method decides if a word is complex. A word is complex if it has more than two syllables.
        :param word: A given word
        :type word: str
        :return: True if a word is complex; else False
        :rtype: bool
        """
        return self.n_syllables(word) > 2

    def get_weights(self, option):
        """
        This method creates the weighting per word.
        :param option: option which type of weights we want to calculate
        :type option: str
        :return: weights per word in a specific text
        :rtype: list
        """

        if option == 'tf-idf':
            n_files = len(self._file_list)

            # Make a list of all words in the documents
            all_dicts = [list(file.words.keys()) for file in self._file_list]
            flat_list = [item for keys in all_dicts for item in keys]

            # Get the count of every word across documents
            values, counts = np.unique(flat_list, return_counts=True)
            dict_df = {value: count for value, count in zip(values, counts)}

        # Save weights in list to return
        weights = {}

        # Iterate over each file
        for file in self._file_list:
            # Dictionary of weights per file
            dict_weights = {}

            # Iterate over every word and its count in that file
            for word_in_file, count_in_file in file.words.items():
                if option == 'tf-idf':
                    # Calculate numerator separately
                    weight = (1 + np.log(count_in_file)) / (1 + np.log(file.n_words))

                    # Check that weight is not negative
                    assert (weight >= 0)

                    weight = weight * np.log(len(self._file_list) / dict_df[word_in_file])
                elif option == 'proportional':
                    weight = file.words[word_in_file] / file.n_words
                else:
                    raise Exception('option does not exist!')

                # Add weight to dictionary of current file
                dict_weights[word_in_file] = weight

            # Save weights
            weights[file.path] = dict_weights

        return weights

    def get_sentiment(self, word, dict_sentiments):
        """
        This method finds the sentiment of a given word and adds the count
        to the dictionary of previous results.
        :param word: The word for which we need to find its sentiment.
        :type word: str
        :param dict_sentiments: A dictionary that keeps per file track of the occurence of each
            sentiment.
        :type dict_sentiments: dict
        :return: Dictionary with sentiments
        :rtype: dict
        """
        try:
            sentiment = self._word_list.dictionary[word]
            dict_sentiments[sentiment] += 1
        except:
            pass
        return dict_sentiments

    def get_similarity(self, weights):
        """
        Compute cosine similarity of files.
        :param weights: list with dictionaries of weightings per file
        :type list
        """
        # Check if files are linked to each other
        self.find_link()

        similarity = []
        for file in self._file_list:
            # Compute cosine similarity if linked
            if file.link is not None:
                # Calculate similarity
                sim = self.cosine_similarity(weights[file.path], weights[file.link])

                # Update similarity in file
                file.similarity = sim
            else:
                sim = None
            similarity.append(sim)
        return similarity

    def find_link(self):
        """ Links document to previously filed document of the same type. """

        # Loop over all items
        not_found_links = 0
        total_size = len(self._file_list)
        for file in self._file_list:
            # Get series-id column that corresponds to file name
            file_name = file.path.split('/')[-1]
            file_name = '_'.join(file_name.split('_')[:2])
            file_data = self._df.loc[self._df['file-name'].str.contains(file_name), ['series-id', 'report-date']]

            # We did not find the file in the dataframe
            if len(file_data.index) > 0:
                # Get series ID(s) (string with 1 or more series ID) and report date
                series = file_data['series-id'].values[0]
                date = file_data['report-date'].values[0]
                series_list = series.replace('"','').split(',')

                # Get previous filings with same series ID
                temp = self._df[(self._df['report-date'] < date) &
                                (self._df['series-id'].str.contains('|'.join(series_list))) &
                                (~pd.isna(self._df['file-name']))]

                # Check if we found something
                if len(temp.index) > 0:
                    i = 1
                    while i < len(temp.index):
                        # Sort dates from oldest to newest and get LATEST file name
                        temp.sort_values('report-date', inplace=True)
                        file_link = temp['file-name'].values[-i]

                        # If the file is a summary, then the path should be renamed
                        # Replace `letters/COPY` by 'summaries'
                        if file.is_summary:
                            file_link = file_link.replace('letters/COPY', 'summaries')

                        # Check if file is not part of winsorization
                        if self._file_list.get_file(file_link) is not None:
                            # Find position of the latest filing in our list
                            file.link = file_link
                            break

                        i += 1
                else:
                    not_found_links += 1
            else:
                self._file_list.delete(file.path)
                not_found_links += 1

        print(f'There are {not_found_links} ({round(100*not_found_links/total_size, 3)}%) links not found!')


class LetterAnalysis(TextAnalysis):

    def __init__(self, utils, word_list, file_list):
        super().__init__(utils, word_list, file_list)

    def get_results(self, weight_scheme):
        """ This method calculates the (modified) Fog index score and the weights. """

        # Lists which will contain the results over all documents/files
        # Variable `all_words`: keep track of words over ALL documents/files
        pers, sent, fogs, all_words = [], [], [], []

        # Iterate over every file
        for file in self._file_list:
            print(f'\rGetting results for: {file.path}', end='')

            # Keep track of variables
            dict_pers, dict_words = {}, {}
            dict_sent = {key: 0 for key in self._word_list.sentiments}
            dict_fogs = {'Modified': 0, 'Unmodified': 0}
            n_words = 0

            # Variable for Fog index (modified and unmodified)
            n_complex_mod, n_complex_nmod = 0, 0

            # Iterate over each sentence in the file
            for line_idx in range(len(file)):
                # Get deepcopy of line and split per word
                line = file[line_idx]
                splitted = line.split()

                # Update dictionary of personalness
                dict_pers = self.personalness(splitted, dict_pers)

                # Clean sentence (inplace is always True)
                line = file.remove_words(line_idx)

                # Go over each word
                for word in line.split():
                    # Update word counter
                    n_words += 1

                    # Save word and its count
                    if word not in dict_words:
                        dict_words[word] = 1
                    else:
                        dict_words[word] += 1

                    # Update `complex` counter for Fog index
                    if self.n_complex(word):
                        if word not in self._word_list.fog_list:
                            n_complex_mod += 1
                        n_complex_nmod += 1

                    # Calculate sentiments
                    dict_sent = self.get_sentiment(word, dict_sent)

            # Calculate Fog index over whole file
            dict_fogs['Modified'] = self.fog_index(n_words, n_complex_mod, len(file))
            dict_fogs['Unmodified'] = self.fog_index(n_words, n_complex_nmod, len(file))

            # Add results to the file object
            file.words = dict_words
            file.fogs = dict_fogs

            pers.append(dict_pers)
            sent.append(dict_sent)
            fogs.append(dict_fogs)
            all_words.append([dict_words, n_words])

        print('\nCompleted obtaining the results!')
        print(f'Making {weight_scheme} weights...')

        # Calculate TF-IDF weights
        weights = self.get_weights(option=weight_scheme)

        print('Completed making the weights!')

        return pers, sent, fogs, weights, all_words

    def get_scores(self, weight_scheme, return_sentiment=True, to_dataframe=True, path=None):
        """
        This method determines the textual scores in all files.
        :param to_dataframe: If True, the results are appended to our dataframe
        :type to_dataframe: bool
        :param path: relative path (including file name) where the dataframe should be saved
        :type path: str
        :return: a dataframe with all results or five lists containing personalness, negativity, confidence, complexity, similarity
        :rtype: pandas.DataFrame or list
        """
        print('Getting results...')
        pers, _, _, weights, _ = self.get_results(weight_scheme=weight_scheme)

        if return_sentiment:
            personalness = []
            negativity = []
            confidence = []
            complexity = []

        # Get similarity scores of files
        print('Getting similarity...')
        similarity = self.get_similarity(weights)

        # Scale sensitivity
        min_neg, max_neg = self._utils.np.inf, -self._utils.np.inf
        min_conf, max_conf = self._utils.np.inf, -self._utils.np.inf
        min_comp, max_comp = self._utils.np.inf, -self._utils.np.inf
        min_pers, max_pers = self._utils.np.inf, -self._utils.np.inf

        # Loop over each file
        print('Getting scores...')
        for i in range(len(self._file_list)):
            # Get file object
            file = self._file_list[i]

            # Set counters
            personal, negative, confident = 0, 0, 0
            complex = file.fogs['Modified']

            # Iterate over every personal word in file
            personal = sum(pers[i].values())

            # Make it to proportional weights
            personal = personal / file.n_words  # PROPORTIONAL WEIGHTS BY DEFAULT

            if len(file.words) < len(self._word_list.dictionary):
                for word in file.words:
                    # Check if word is in our sentiment dictionary
                    if word in self._word_list.dictionary:
                        if 'Negative' in self._word_list.dictionary[word]:
                            negative += weights[file.path][word]
                        if 'StrongModal' in self._word_list.dictionary[word]:
                            confident += weights[file.path][word]
                        if 'WeakModal' in self._word_list.dictionary[word]:
                            confident -= weights[file.path][word]
            else:
                for word in self._word_list.dictionary:
                    # Check if word is in our sentiment dictionary
                    if word in file.words:
                        if 'Negative' in self._word_list.dictionary[word]:
                            negative += weights[file.path][word]
                        if 'StrongModal' in self._word_list.dictionary[word]:
                            confident += weights[file.path][word]
                        if 'WeakModal' in self._word_list.dictionary[word]:
                            confident -= weights[file.path][word]

            # Update minimum and maximum for scaling
            max_neg = negative if negative > max_neg else max_neg
            min_neg = negative if negative < min_neg else min_neg

            max_conf = confident if confident > max_conf else max_conf
            min_conf = confident if confident < min_conf else min_conf

            max_comp = complex if complex > max_comp else max_comp
            min_comp = complex if complex < min_comp else min_comp

            max_pers = personal if personal > max_pers else max_pers
            min_pers = personal if personal < min_pers else min_pers

            if return_sentiment:
                personalness.append(personal)
                negativity.append(negative)
                confidence.append(confident)
                complexity.append(complex)

            self._file_list.get_file(file.path).results['personalness'] = personal
            self._file_list.get_file(file.path).results['negativity'] = negative
            self._file_list.get_file(file.path).results['confidence'] = confident
            self._file_list.get_file(file.path).results['complexity'] = complex

        # Scale every sentiment
        negativity = [(neg - min_neg) / (max_neg - min_neg) for neg in negativity]  # between 0 and 1
        confidence = [2*((conf - min_conf) / (max_conf - min_conf)) - 1 for conf in confidence]  # between -1 and 1
        complexity = [(comp - min_comp) / (max_comp - min_comp) for comp in complexity]  # between 0 and 1
        personalness = [(p - min_pers) / (max_pers - min_pers) for p in personalness]

        if to_dataframe:
            print('Writing to dataframe...')
            df = copy.deepcopy(self._df)
            df['number-words'] = np.nan
            for idx in range(len(self._file_list)):
                file = self._file_list[idx]
                df.loc[df['file-name'].str.contains(file.path), 'personalness'] = personalness[idx]
                df.loc[df['file-name'].str.contains(file.path), 'negativity'] = negativity[idx]
                df.loc[df['file-name'].str.contains(file.path), 'confidence'] = confidence[idx]
                df.loc[df['file-name'].str.contains(file.path), 'complexity'] = complexity[idx]
                df.loc[df['file-name'].str.contains(file.path), 'similarity'] = similarity[idx]
                df.loc[df['file-name'].str.contains(file.path), 'number-words'] = file.n_words
            if path is not None:
                df.to_csv(path)
            return df

        if return_sentiment:
            return personalness, negativity, confidence, complexity, similarity


class SummaryAnalysis(TextAnalysis):

    def __init__(self, utils, word_list, file_list):
        super().__init__(utils, word_list, file_list)

    def get_results(self, weight_scheme):
        """ This method calculates the (modified) Fog index score and the weights. """

        # Lists which will contain the results over all documents/files
        # Variable `all_words`: keep track of words over ALL documents/files
        sent, fogs, all_words = [], [], []

        # Iterate over every file
        for file in self._file_list:
            print(f'\rGetting results for: {file.path}', end='')

            # Keep track of variables
            dict_pers, dict_words = {}, {}
            dict_sent = {key: 0 for key in self._word_list.sentiments}
            dict_fogs = {'Modified': 0, 'Unmodified': 0}
            n_words = 0

            # Variable for Fog index (modified and unmodified)
            n_complex_mod, n_complex_nmod = 0, 0

            # Iterate over each sentence in the file
            for line_idx in range(len(file)):
                # Get deepcopy of line and split per word
                line = file[line_idx]

                # Go over each word
                for word in line.split():
                    # Update word counter
                    n_words += 1

                    # Save word and its count
                    if word not in dict_words:
                        dict_words[word] = 1
                    else:
                        dict_words[word] += 1

                    # Update `complex` counter for Fog index
                    if self.n_complex(word):
                        if word not in self._word_list.fog_list:
                            n_complex_mod += 1
                        n_complex_nmod += 1

                    # Calculate sentiments
                    dict_sent = self.get_sentiment(word, dict_sent)

            # Calculate Fog index over whole file
            dict_fogs['Modified'] = self.fog_index(n_words, n_complex_mod, len(file))
            dict_fogs['Unmodified'] = self.fog_index(n_words, n_complex_nmod, len(file))

            # Add results to the file object
            file.words = dict_words
            file.fogs = dict_fogs

            sent.append(dict_sent)
            fogs.append(dict_fogs)
            all_words.append([dict_words, n_words])

        print('\nCompleted obtaining the results!')
        print(f'Making {weight_scheme} weights...')

        # Calculate TF-IDF weights
        weights = self.get_weights(option=weight_scheme)

        print('Completed making the weights!')

        return sent, fogs, weights, all_words

    def get_scores(self, weight_scheme, return_sentiment=True, to_dataframe=True, path=None):
        """
        This method determines the textual scores in all files.
        :param to_dataframe: If True, the results are appended to our dataframe
        :type to_dataframe: bool
        :param path: relative path (including file name) where the dataframe should be saved
        :type path: str
        :return: a dataframe with all results or five lists containing personalness, negativity, confidence, complexity, similarity
        :rtype: pandas.DataFrame or list
        """
        print('Getting results...')
        _, _, weights, _ = self.get_results(weight_scheme=weight_scheme)

        if return_sentiment:
            negativity = []
            confidence = []

        # Get similarity scores of files
        print('Getting similarity...')
        similarity = self.get_similarity(weights)

        # Scale sensitivity
        min_neg, max_neg = self._utils.np.inf, -self._utils.np.inf
        min_conf, max_conf = self._utils.np.inf, -self._utils.np.inf

        # Loop over each file
        print('Getting scores...')
        for i in range(len(self._file_list)):
            print(f'\rProgress: {round(i*100/len(self._file_list), 3)}%', end='')
            # Get file object
            file = self._file_list[i]

            # Set counters
            personal, negative, confident = 0, 0, 0

            if len(file.words) < len(self._word_list.dictionary):
                for word in file.words:
                    # Check if word is in our sentiment dictionary
                    if word in self._word_list.dictionary:
                        if 'Negative' in self._word_list.dictionary[word]:
                            negative += weights[file.path][word]
                        if 'StrongModal' in self._word_list.dictionary[word]:
                            confident += weights[file.path][word]
                        if 'WeakModal' in self._word_list.dictionary[word]:
                            confident -= weights[file.path][word]
            else:
                for word in self._word_list.dictionary:
                    # Check if word is in our sentiment dictionary
                    if word in file.words:
                        if 'Negative' in self._word_list.dictionary[word]:
                            negative += weights[file.path][word]
                        if 'StrongModal' in self._word_list.dictionary[word]:
                            confident += weights[file.path][word]
                        if 'WeakModal' in self._word_list.dictionary[word]:
                            confident -= weights[file.path][word]

            # Update minimum and maximum for scaling
            max_neg = negative if negative > max_neg else max_neg
            min_neg = negative if negative < min_neg else min_neg

            max_conf = confident if confident > max_conf else max_conf
            min_conf = confident if confident < min_conf else min_conf

            if return_sentiment:
                negativity.append(negative)
                confidence.append(confident)

            self._file_list.get_file(file.path).results['negativity'] = negative
            self._file_list.get_file(file.path).results['confidence'] = confident
        print()

        # Scale every sentiment
        negativity = [(neg - min_neg) / (max_neg - min_neg) for neg in negativity]  # between 0 and 1
        confidence = [2*((conf - min_conf) / (max_conf - min_conf)) - 1 for conf in confidence]  # between -1 and 1

        if to_dataframe:
            print('Writing to dataframe...')
            df = copy.deepcopy(self._df)
            df['number-words'] = np.nan
            for idx in range(len(self._file_list)):
                file = self._file_list[idx]
                file_name = file.path.split('/')[-3:]
                df.loc[df['file-name'].str.contains('/'.join(file_name)), 'negativity'] = negativity[idx]
                df.loc[df['file-name'].str.contains('/'.join(file_name)), 'confidence'] = confidence[idx]
                df.loc[df['file-name'].str.contains('/'.join(file_name)), 'similarity'] = similarity[idx]
                df.loc[df['file-name'].str.contains('/'.join(file_name)), 'number-words'] = file.n_words
            if path is not None:
                df.to_csv(path)
            return df

        if return_sentiment:
            return negativity, confidence, similarity

