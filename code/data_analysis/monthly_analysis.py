import pandas as pd
import numpy as np
import sys
from datetime import datetime as dt

import general.utils as utils
from data_analysis.out_of_sample import fama_macbeth, portfolio_sort

# ----------------------- Plotting stuff -----------------------
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

# PLOT STYLE
plt.style.use('seaborn-deep')

# DATE STYLE
years = mdates.YearLocator()   # every year
months = mdates.MonthLocator()  # every month
yearsFmt = mdates.DateFormatter('%Y')

# FONT SIZE
plt.rcParams.update({'font.size': 16})
sys.path.append('../../code/general/')  # Add directory of folder to path

# -----------------------
# User defined parameters
# -----------------------

# Select type of weights
SCORES_TF_IDF = True            # Boolean: if TRUE, scores are computed using TF-IDF

# Select robustness check
SINGLE_FUND_ONLY = False        # Boolean: if TRUE, only consider fund-specific shareholder letters
CRISIS_SAMPLE = False           # Boolean: if TRUE, consider crisis subsample
SUMMARIZATION_SCORES = False    # Boolean: if TRUE, consider summarized letter scores

# Select bootstrap type
bBootstrap = False              # Boolean: if TRUE, run iid bootstrap procedure in regression analysis
bPanelBootstrap = False         # Boolean: if TRUE, run panel bootstrap procedure in regression analysis
write_funds_counter = False     # Boolean: if TRUE, keep track of number of funds in sample by writing this to a text file

# Save files to bootstrap folder
bSaveBootstrap = False           # Boolean: if TRUE, save output to bootstrap_regression folder

# Crisis start and end date
full_start_date = dt.strptime(str(int(float('20060601'))), '%Y%m%d').date()
full_end_date = dt.strptime(str(int(float('20170131'))), '%Y%m%d').date()

crisis_start_date = dt.strptime(str(int(float('20070101'))), '%Y%m%d').date()
crisis_end_date = dt.strptime(str(int(float('20081231'))), '%Y%m%d').date()

# # Print settings
# print('TF IDF: ' + str(SCORES_TF_IDF))
# print('Single Fund: ' + str(SINGLE_FUND_ONLY))
# print('Summarization: ' + str(SUMMARIZATION_SCORES))
# print('Crisis Sample: ' + str(CRISIS_SAMPLE))
# print('Panel Bootstrap: ' + str(bPanelBootstrap))
# print('Bootstrap: ' + str(bBootstrap) + '\n')

# -----------------------Paths-----------------------
PATH_DATA = '../../data/'
PATH_CRSP = '../../data/CRSP/'          # Set relative paths for CRSP data files
PATH_FRENCH = '../../data/French/'      # Set relative paths for Kenneth French data files
PATH_EDGAR = '../../data/EDGAR/'
PATH_LETTERS = '../../data/letters/'    # Set relative paths for letter files
PATH_SIMULATION = '../simulation/'

if SCORES_TF_IDF:
    PATH_RESULTS = '../../results/TF_IDF/'
    if SUMMARIZATION_SCORES:
        PATH_RESULTS += 'summarization/'
        RESULTS_FILENAME = PATH_RESULTS + 'summary_tf-idf.csv'
        RESULTS_MERGED_FILENAME = PATH_RESULTS + 'summary_tf-idf_merged.csv'
    else:
        RESULTS_FILENAME = PATH_RESULTS + 'scores_tf-idf.csv'
        RESULTS_MERGED_FILENAME = PATH_RESULTS + 'scores_tf-idf_merged.csv'
else:
    PATH_RESULTS = '../../results/proportional/'
    if SUMMARIZATION_SCORES:
        PATH_RESULTS += 'summarization/'
        RESULTS_FILENAME = PATH_RESULTS + 'scores_proportional.csv'
        RESULTS_MERGED_FILENAME = PATH_RESULTS + 'summary_proportional_merged.csv'
    else:
        RESULTS_FILENAME = PATH_RESULTS + 'scores_proportional.csv'
        RESULTS_MERGED_FILENAME = PATH_RESULTS + 'scores_proportional_merged.csv'

if SINGLE_FUND_ONLY:
    PATH_RESULTS += 'single_funds/'
elif CRISIS_SAMPLE:
    PATH_RESULTS += 'crisis_subsample/'
else:
    PATH_RESULTS += 'baseline/'

PATH_FIGURES = PATH_RESULTS + 'Figures/'

# -----------------------CRSP processed filenames-----------------------
CRSP_CIK_FILENAME = PATH_CRSP + 'CIK_merged_monthly.csv'
FUND_PERF_MEASURES_FILENAME = PATH_CRSP + 'fund_performance_measures.csv'
FUND_PERF_MEASURES_AUGMENTED_FILENAME = PATH_CRSP + 'fund_performance_measures_augmented.csv'

# -----------------------Results filenames-----------------------
RESULTS_COMPLETE = PATH_RESULTS + 'results_complete_monthly.csv'

# -----------------------Output filenames-----------------------

RESULTS_REGRESSION_ABS_FILENAME = PATH_RESULTS + 'absolute_regression_output.csv'
RESULTS_REGRESSION_REL_FILENAME = PATH_RESULTS + 'relative_regression_output.csv'

RESULTS_REGRESSION_ABS_BOOTSTRAP_FILENAME = PATH_RESULTS + 'absolute_regression_bootstrap_output.csv'
RESULTS_REGRESSION_REL_BOOTSTRAP_FILENAME = PATH_RESULTS + 'relative_regression_bootstrap_output.csv'

RESULTS_INDIVID_REGRESSION_FILENAME = PATH_RESULTS + 'individ_regression_output.xlsx'
RESULTS_ADD_REGRESSION_FILENAME = PATH_RESULTS + 'add_regression_output.xlsx'

if bSaveBootstrap:
    PATH_SAVE_RESULTS = PATH_RESULTS.replace('results', 'code/bootstrap_regression')
    RESULTS_WINSORIZED_BOOTSTRAP_FILENAME =  PATH_SAVE_RESULTS + 'results_winsorized.csv'

#
# * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * +

def merge_CRSP_letters():
    """
    Merges CRSP dataframe with FF performance measures, and finally with textual scores
    :param write_funds_counter: if True, writes to a file how many unique funds there are
    :type write_funds_counter: bool
    """
    # CRSP dataframe with fund characteristics
    df1 = pd.read_csv(CRSP_CIK_FILENAME)
    df1 = process_fund_data(df1)

    # Dataframe with textual scores
    df2 = pd.read_csv(RESULTS_FILENAME)
    df2 = process_scores(df2)

    # # Dataframe with FF performance measures
    # # TODO get these monthly
    # df_fund_perf = pd.read_csv(FUND_PERF_MEASURES_AUGMENTED_FILENAME)
    # df_fund_perf['file-date'] = pd.to_datetime(df_fund_perf['file-date'], format='%Y-%m-%d')
    # df2 = pd.merge(df2, df_fund_perf, on=['FundNo', 'file-date'])

    # Merge dataframes
    cols = list(df2)[4:]
    df = pd.merge(df1, df2, on=['period', 'CIK', 'CIK', 'Class ID', 'Series ID'], how="left")
    df = df.drop(columns=["period", "Class ID", 'Series ID', "CIK"])

    # Remove Funds with no letters
    n_words = df.groupby("FundNo")["number-words"].count()
    fund_list = n_words[n_words > 0].index
    df = df.loc[df["FundNo"].isin(fund_list)]

    # Forward fill letter data up to 1 year
    df = df.sort_values(by=["FundNo", "Date", "file-date"])
    df[cols] = df.groupby("FundNo")[cols].ffill(limit=11)
    df = apply_subsample(df)

    df.to_csv(RESULTS_COMPLETE, index=False)


def process_scores(df):
    # Rename
    df = df.rename(columns={'negativity': 'Negativity', 'complexity': 'Complexity',
                            'confidence': 'Confidence', 'personalness': 'Personalness', 'similarity': 'Similarity'})
    df['Date'] = pd.to_datetime(df["Date"])

    # Compute surprise measures as first difference of absolute scores
    df = df.sort_values(by=['FundNo', 'Date'], ascending=True)
    df['Negativity Surprise'] = df.groupby('FundNo')['Negativity'].diff()
    df['Confidence Surprise'] = df.groupby('FundNo')['Confidence'].diff()

    if not SUMMARIZATION_SCORES:
        df['Complexity Surprise'] = df.groupby('FundNo')['Complexity'].diff()
        df['Personalness Surprise'] = df.groupby('FundNo')['Personalness'].diff()

    # Compute number of funds in letter
    df['No. funds'] = df['series-id'].apply(lambda x: len(x.split(',')))
    df['LN(No. funds)'] = np.log(df['No. funds'])

    # Get date-month
    df["file-date"] = pd.to_datetime(df["file-date"], format="%Y%m%d")
    df['period'] = df["file-date"].dt.to_period("M")

    # Select columns
    cols = ['CIK', 'Series ID', 'Class ID', 'period', 'file-date', 'number-words', 'Negativity', 'Confidence', 'Similarity', "Negativity Surprise", "Confidence Surprise"]
    if not SUMMARIZATION_SCORES:
        cols += ["Personalness", "Complexity", "Personalness Surprise", "Complexity Surprise"]
    return df[cols]


def process_fund_data(df):
    """Creates the dataframe used in the regressions of fund performance measures on textual features + controls."""

    # Rename columns (different for summarized letters)
    df['Date'] = pd.to_datetime(df['Date'], format='%Y-%m-%d')
    df = df.rename(columns={"mtna": "Fund Size", "mret": "Return", "mnav": "Net Asset Value",
                            "exp_ratio": "Expense Ratio", "mgmt_fee": "Management Fee"})

    # Convert performance measures to percentages
    pct_cols = ['Return', 'Fund Flow', "Expense Ratio", "Management Fee"]
    df[pct_cols] *= 100

    # # Clip data TODO Does not affect portfolio sort nor FM
    # cols = ['Return', 'Fund Flow', "Management Fee", "Expense Ratio"]
    # for col in cols:
    #     upper = df[col].quantile(0.99)
    #     lower = df[col].quantile(0.01)
    #     if col == "Return":
    #         df[col] = df[col].clip(lower=-100, upper=upper)
    #     else:
    #         df[col] = df[col].clip(lower=lower, upper=upper)

    # Compute log-transformed variables
    ln_cols = ["Fund Size", "Fund Age"]
    for col in ln_cols:
        df[f'LN({col})'] = np.log(1 + df[col])
    df = df.drop(columns=ln_cols)
    df['period'] = df["Date"].dt.to_period("M")

    return df


def apply_subsample(df):
    # TODO
    # Reduce dataset to fund-specific letters if desired
    if SINGLE_FUND_ONLY:
        # Drop fund observations if there are multiple series IDs in its filing
        df = df[df['LN(No. funds)'] == 0]

    if CRISIS_SAMPLE:
        # Select subsample corresponding to crisis
        mask_crisis = (df['Date'] >= pd.Timestamp(crisis_start_date)) & (df['Date'] <= pd.Timestamp(crisis_end_date))
        df = df.loc[mask_crisis]

    else:
        # Restrict sample since we do not have sufficient coverage otherwise
        mask_crisis = (df['Date'] >= pd.Timestamp(full_start_date)) & (df['Date'] <= pd.Timestamp(full_end_date))
        df = df.loc[mask_crisis]

    return df


if __name__ == '__main__':
    merge_CRSP_letters()