import pandas as pd
import numpy as np
from scipy import stats
import sys
import statsmodels.api as sm
from statsmodels.stats.outliers_influence import variance_inflation_factor

from linearmodels import PanelOLS, PooledOLS
import seaborn as sns
import math
import os
from scipy.stats import norm
from scipy.stats.distributions import chi2
from datetime import datetime as dt

import data_analysis.bootstrap_regressions as bs
import general.utils as utils

# ----------------------- Plotting stuff -----------------------
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

# PLOT STYLE
plt.style.use('seaborn-deep')

# DATE STYLE
years = mdates.YearLocator()   # every year
months = mdates.MonthLocator()  # every month
yearsFmt = mdates.DateFormatter('%Y')

# FONT SIZE
plt.rcParams.update({'font.size': 16})
sys.path.append('../../code/general/')  # Add directory of folder to path

# -----------------------
# User defined parameters
# -----------------------

# Select type of weights
SCORES_TF_IDF = True            # Boolean: if TRUE, scores are computed using TF-IDF

# Select robustness check
SINGLE_FUND_ONLY = True        # Boolean: if TRUE, only consider fund-specific shareholder letters
CRISIS_SAMPLE = False           # Boolean: if TRUE, consider crisis subsample
SUMMARIZATION_SCORES = True    # Boolean: if TRUE, consider summarized letter scores

# Select bootstrap type
bBootstrap = False              # Boolean: if TRUE, run iid bootstrap procedure in regression analysis
bPanelBootstrap = False         # Boolean: if TRUE, run panel bootstrap procedure in regression analysis
write_funds_counter = False     # Boolean: if TRUE, keep track of number of funds in sample by writing this to a text file

# Save files to bootstrap folder
bSaveBootstrap = False           # Boolean: if TRUE, save output to bootstrap_regression folder

# Crisis start and end date
crisis_start_date = dt.strptime(str(int(float('20070101'))), '%Y%m%d').date()
crisis_end_date = dt.strptime(str(int(float('20081231'))), '%Y%m%d').date()

# Print settings
print('TF IDF: ' + str(SCORES_TF_IDF))
print('Single Fund: ' + str(SINGLE_FUND_ONLY))
print('Summarization: ' + str(SUMMARIZATION_SCORES))
print('Crisis Sample: ' + str(CRISIS_SAMPLE))
print('Panel Bootstrap: ' + str(bPanelBootstrap))
print('Bootstrap: ' + str(bBootstrap) + '\n')

# -----------------------Paths-----------------------
if not SCORES_TF_IDF:
    PARENT_PATH = '../../results/proportional/'
    RESULTS_FILENAME = 'scores_proportional.csv'
    RESULTS_MERGED_FILENAME ='scores_proportional_merged.csv'
    if SUMMARIZATION_SCORES:
        PARENT_PATH = PARENT_PATH + 'summarization/'
        RESULTS_FILENAME = 'summary_proportional.csv'
        RESULTS_MERGED_FILENAME = 'scores_proportional_merged.csv'
else:
    PARENT_PATH = '../../results/TF_IDF/'
    RESULTS_FILENAME = 'scores_tf-idf.csv'
    RESULTS_MERGED_FILENAME = 'scores_tf-idf_merged.csv'
    if SUMMARIZATION_SCORES:
        PARENT_PATH = PARENT_PATH + 'summarization/'
        RESULTS_FILENAME = 'summary_tf-idf.csv'
        RESULTS_MERGED_FILENAME = 'summary_tf-idf_merged.csv'

RESULTS_FILENAME = PARENT_PATH + RESULTS_FILENAME
RESULTS_MERGED_FILENAME = PARENT_PATH + RESULTS_MERGED_FILENAME

if SINGLE_FUND_ONLY:
    PATH_RESULTS = PARENT_PATH + 'single_funds/'
elif CRISIS_SAMPLE:
    PATH_RESULTS = PARENT_PATH + 'crisis_subsample/'
else:
    PATH_RESULTS = PARENT_PATH + 'baseline/'

PATH_DATA = '../../data/'
PATH_CRSP = '../../data/CRSP/'          # Set relative paths for CRSP data files
PATH_EDGAR = '../../data/EDGAR/'
PATH_LETTERS = '../../data/letters/'    # Set relative paths for letter files
PATH_FIGURES = PATH_RESULTS + 'Figures/'

# -----------------------CRSP processed filenames-----------------------
CRSP_CIK_FILENAME = PATH_CRSP + 'CIK_merged.csv'
TICKER_FILENAME = PATH_CRSP + 'tickers.txt'
COMBINED_FILENAME = PATH_CRSP + 'returns_factors_combined.csv'
FUND_PERF_MEASURES_FILENAME = PATH_CRSP + 'fund_performance_measures.csv'
FUND_PERF_MEASURES_AUGMENTED_FILENAME = PATH_CRSP + 'fund_performance_measures_augmented.csv'

# -----------------------Results filenames-----------------------
RESULTS_WINSORIZED_FILENAME = PATH_RESULTS + 'results_winsorized.csv'
RESULTS_COMPLETE_FILENAME = PATH_RESULTS + 'results_complete.csv'
MEASURES_TRANSFORMED_FILENAME = PATH_RESULTS + 'measures_transformed.csv'

# -----------------------Output filenames-----------------------
FUNDS_COUNTER_FILENAME = PARENT_PATH + 'funds_counter.txt'
DESCRIPTIVES_FILENAME = PATH_RESULTS + 'descriptives.xlsx'
TIMESERIES_DATA_FILENAME = PATH_FIGURES + 'timeseries.xlsx'

RESULTS_REGRESSION_ABS_FILENAME = PATH_RESULTS + 'absolute_regression_output.csv'
RESULTS_REGRESSION_REL_FILENAME = PATH_RESULTS + 'relative_regression_output.csv'

RESULTS_RESIDUALS_ABS_FILENAME = PATH_RESULTS + 'absolute_regression_residuals.csv'
RESULTS_RESIDUALS_REL_FILENAME = PATH_RESULTS + 'relative_regression_residuals.csv'

RESULTS_REGRESSION_ABS_BOOTSTRAP_FILENAME = PATH_RESULTS + 'absolute_regression_bootstrap_output.csv'
RESULTS_REGRESSION_REL_BOOTSTRAP_FILENAME = PATH_RESULTS + 'relative_regression_bootstrap_output.csv'

RESULTS_RESIDUALS_ABS_BOOTSTRAP_FILENAME = PATH_RESULTS + 'absolute_regression_bootstrap_residuals.csv'
RESULTS_RESIDUALS_REL_BOOTSTRAP_FILENAME = PATH_RESULTS + 'relative_regression_bootstrap_residuals.csv'

RESULTS_INDIVID_REGRESSION_FILENAME = PATH_RESULTS + 'individ_regression_output.xlsx'
RESULTS_ADD_REGRESSION_FILENAME = PATH_RESULTS + 'add_regression_output.xlsx'

if bSaveBootstrap:
    PATH_SAVE_RESULTS = PATH_RESULTS.replace('results', 'code/bootstrap_regression')
    RESULTS_WINSORIZED_BOOTSTRAP_FILENAME =  PATH_SAVE_RESULTS + 'results_winsorized.csv'

#
# * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * +

def merge_CRSP_letters(write_funds_counter=False):
    """
    Merges CRSP dataframe with FF performance measures, and finally with textual scores
    :param write_funds_counter: if True, writes to a file how many unique funds there are
    :type write_funds_counter: bool
    """
    # CRSP dataframe with fund characteristics
    df1 = pd.read_csv(CRSP_CIK_FILENAME)
    df1['Date'] = pd.to_datetime(df1['Date'], format='%Y-%m-%d')
    df1.drop(columns=['Lagged Fund Flow', 'Future Fund Flow'], inplace=True)

    # Dataframe with textual scores
    df2 = pd.read_csv(RESULTS_FILENAME)
    df2['file-date'] = pd.to_datetime(df2['file-date'], format='%Y%m%d')
    df2['Date'] = pd.to_datetime(df2['Date'], format='%d/%m/%y')
    df2.drop(columns=['Lagged Return', 'Future Return', 'Lagged Fund Flow', 'Future Fund Flow'], inplace=True)

    # Dataframe with FF performance measures
    df_fund_perf = pd.read_csv(FUND_PERF_MEASURES_AUGMENTED_FILENAME)
    df_fund_perf['file-date'] = pd.to_datetime(df_fund_perf['file-date'], format='%Y-%m-%d')
    df2 = pd.merge(df2, df_fund_perf, on=['FundNo', 'file-date'])

    # Select columns (different for summarized letters)
    if SUMMARIZATION_SCORES:
        cols = ['Date', 'CIK', 'Series ID', 'Class ID', 'series-id', 'contract-id', 'file-name', 'report-date', 'file-date',
                'number-words', 'negativity', 'confidence', 'similarity',
                'SD(dret)', 'Alpha CAPM', 'Alpha FF3', 'Alpha CH4', 'Rm-Rf', 'SMB', 'HML',
                'SD(resid)', 'Lagged Volatility', 'Lagged Return', 'Future Return', 'Lagged Fund Flow', 'Future Fund Flow']
    else:
        cols = ['Date', 'CIK', 'Series ID', 'Class ID', 'series-id', 'contract-id', 'file-name', 'report-date', 'file-date',
                'number-words', 'negativity', 'complexity', 'confidence', 'personalness', 'similarity', 'SD(dret)', 'Alpha CAPM', 'Alpha FF3', 'Alpha CH4',
                'Rm-Rf', 'SMB', 'HML', 'SD(resid)', 'Lagged Return', 'Future Return', 'Lagged Volatility', 'Lagged Fund Flow', 'Future Fund Flow']
    df2 = df2[cols]

    # Drop rows not matched to letters
    df2 = df2.dropna(subset=['number-words'])

    # Merge dataframes
    df_merged = pd.merge(df1, df2, on=['Date', 'CIK', 'Class ID', 'Series ID'])
    df_merged.to_csv(RESULTS_MERGED_FILENAME, index=False)

    if write_funds_counter:
        nFunds = df_merged['FundNo'].unique().size
        message = 'Match funds with SEC N-CSR filings,' + str(nFunds)
        utils.write_line_to_text(FUNDS_COUNTER_FILENAME, message)


def winsorize(write_funds_counter=False, bSaveBootstrap=False):
    """
    Winsorizes certain columns in dataset, renames columns, and applies some final transformations
    :param write_funds_counter: if True, writes to a file how many unique funds there are
    :type write_funds_counter: bool
    :param bSaveBootstrap: if True, it will save the bootstrapped values
    :type bSaveBootstrap: bool
    """
    df_results = pd.read_csv(RESULTS_MERGED_FILENAME)

    # Reduce dataset to fund-specific letters if desired
    if SINGLE_FUND_ONLY:
        # Drop fund observations if there are multiple series IDs in its filing
        df_results = df_results[df_results['series-id'].apply(lambda x: len(x.split(','))) == 1]

        # Drop fund observationns if there are other funds with identical textual scores
        if SUMMARIZATION_SCORES:
            cols = ['number-words', 'negativity', 'confidence', 'similarity', 'mgmt_name', 'CIK']
        else:
            cols = ['number-words', 'negativity', 'confidence', 'similarity', 'mgmt_name', 'CIK']
        df_results = df_results.reset_index(drop=True)
        df_results = df_results.drop_duplicates(subset=cols, keep=False)
    elif CRISIS_SAMPLE:
        df_results['Date'] = pd.to_datetime(df_results['Date'], format='%Y-%m-%d')

        # Select subsample corresponding to crisis
        mask_crisis = (df_results['Date'] >= pd.Timestamp(crisis_start_date)) & (df_results['Date'] <= pd.Timestamp(crisis_end_date))
        df_results = df_results.loc[mask_crisis]
        df_results = df_results.reset_index(drop=True)

    # Rename columns (different for summarized letters)
    if SUMMARIZATION_SCORES:
        df_results = df_results.rename(columns={'SD(dret)': 'Volatility', 'SD(resid)': 'Idiosyncratic Risk', 'Rm-Rf': 'Systematic Risk',
            'TNA': 'Fund Size', 'exp_ratio': 'Expense Ratio', 'mgmt_fee': 'Management Fee', 'turn_ratio': 'Turnover Ratio',
            'negativity': 'Negativity', 'confidence': 'Confidence', 'similarity': 'Similarity'})
    else:
        df_results = df_results.rename(columns={'SD(dret)': 'Volatility', 'SD(resid)': 'Idiosyncratic Risk', 'Rm-Rf': 'Systematic Risk',
            'TNA': 'Fund Size', 'exp_ratio': 'Expense Ratio', 'mgmt_fee': 'Management Fee', 'turn_ratio': 'Turnover Ratio',
            'negativity': 'Negativity', 'complexity': 'Complexity', 'confidence': 'Confidence', 'personalness': 'Personalness', 'similarity': 'Similarity'})

    # Convert performance measures to percentages
    cols_to_decimal = ['Return']
    cols_to_percentage = ['Future Return', 'Lagged Return', 'Fund Flow', 'Expense Ratio', 'Turnover Ratio', 'Lagged Fund Flow',
                           'Future Fund Flow', 'Alpha CAPM', 'Alpha FF3', 'Alpha CH4', 'Volatility', 'Idiosyncratic Risk', 'Systematic Risk', 'Lagged Volatility']

    df_results[cols_to_decimal] = df_results[cols_to_decimal] - 1
    df_results[cols_to_decimal + cols_to_percentage] = df_results[cols_to_decimal + cols_to_percentage] * 100

    # Annualize performance measures
    cols_semi_annual = ['Future Return', 'Lagged Return', 'Future Fund Flow', 'Lagged Fund Flow']
    cols_quarterly = ['Return', 'Fund Flow']
    cols_daily_sqrt = ['Volatility', 'Lagged Volatility', 'Idiosyncratic Risk', 'Systematic Risk']
    cols_daily = ['Alpha CAPM', 'Alpha FF3', 'Alpha CH4']

    df_results[cols_semi_annual] = df_results[cols_semi_annual] * 2
    df_results[cols_quarterly] = df_results[cols_quarterly] * 4
    df_results[cols_daily] = df_results[cols_daily] * 252
    df_results[cols_daily_sqrt] = df_results[cols_daily_sqrt] * math.sqrt(252)

    '''Clean data'''
    # Remove future returns outlier of 800%
    df_results = df_results[df_results['Future Return'] < 800]

    # Remove select observations with magnitude larger than 99.5% quantile
    cols_to_trim = ['Fund Flow', 'Lagged Fund Flow', 'Future Fund Flow', 'Volatility', 'Turnover Ratio', 'Alpha CAPM', 'Alpha FF3', 'Alpha CH4']
    abs_quantiles = df_results[cols_to_trim].abs().quantile(0.995)

    for col, i in zip(cols_to_trim, range(len(abs_quantiles))):
        df_results = df_results[df_results[col].abs() < abs_quantiles[i]]

    if write_funds_counter:
        nFunds = df_results['FundNo'].unique().size
        message = 'Remove outliers,' + str(nFunds)
        utils.write_line_to_text(FUNDS_COUNTER_FILENAME, message)

    # Write winsorized dataframe to csv file
    if bSaveBootstrap:
        filename = RESULTS_WINSORIZED_BOOTSTRAP_FILENAME
    else:
        filename = RESULTS_WINSORIZED_FILENAME

    df_results.to_csv(filename, index=False)


def summary_statistics():
    """
    Computes summary statistics and writes them to an Excel file
    """
    df_results = pd.read_csv(RESULTS_WINSORIZED_FILENAME)
    df_results = df_results.dropna(subset=['file-name'])

    # Compute number of funds mentioned in a letter
    df_results['No. funds'] = df_results['series-id'].apply(lambda x: len(x.split(',')))

    '''Distribution of letters over time'''
    df_results['report-date'] = pd.to_datetime(df_results['report-date'], format='%Y%m%d')
    df_results['file-date'] = pd.to_datetime(df_results['file-date'], format='%Y-%m-%d')

    # Get months
    df_results['report-month'] = df_results['report-date'].dt.month
    df_results['file-month'] = df_results['file-date'].dt.month

    # Create new dataframe
    df_months = df_results[['file-name', 'report-month', 'file-month']]
    df_months = df_months.drop_duplicates(inplace=False).reset_index(drop=True)

    # Count letters per month
    df_report_month = df_months[['file-name', 'report-month']].groupby(['report-month']).agg('count')
    df_report_month = df_report_month.rename(columns={'file-name': 'Report Date'})
    df_report_month['Report Date %'] = (df_report_month['Report Date'] / df_report_month['Report Date'].sum() )*100

    df_file_month = df_months[['file-name', 'file-month']].groupby(['file-month']).agg('count')
    df_file_month = df_file_month.rename(columns={'file-name': 'Filing Date'})
    df_file_month['Filing Date %'] = (df_file_month['Filing Date'] / df_file_month['Filing Date'].sum() )*100

    df_month_descriptives = pd.concat([df_file_month, df_report_month], axis=1)
    df_month_descriptives = df_month_descriptives.append(df_month_descriptives.sum().rename('Total'))

    '''Descriptives of textual properties'''
    # Select columns (different for summarized letters)
    if SUMMARIZATION_SCORES:
        df_letters = df_results[['number-words', 'Negativity', 'Confidence', 'Similarity', 'No. funds']]
    else:
        df_letters = df_results[['number-words', 'Negativity', 'Confidence', 'Personalness', 'Complexity', 'Similarity', 'No. funds']]
    df_letters = df_letters.describe()
    df_letters = df_letters.transpose()

    '''Fund characteristics descriptives'''
    df_funds = df_results[['Future Return', 'Alpha', 'Volatility', 'Fund Size', 'Future Fund Flow', 'Fund Age', 'Management Fee', 'Expense Ratio', 'Turnover Ratio']]
    df_funds = df_funds.describe()
    df_funds = df_funds.transpose()

    '''Compute correlations'''
    # Select columns (different for summarized letters)
    if SUMMARIZATION_SCORES:
        cols = ['Date', 'Future Return', 'Future Fund Flow', 'Alpha', 'Volatility', 'Negativity', 'Confidence', 'Similarity']
        # cols = ['Date', 'Return', 'Fund Flow', 'Alpha', 'Total Risk', 'Idiosyncratic Risk', 'Systematic Risk', \
        #         'Negativity', 'Confidence', 'Similarity']
    else:
        cols = ['Date', 'Future Return', 'Future Fund Flow', 'Alpha', 'Volatility', 'Negativity', 'Confidence', 'Personalness', 'Complexity', 'Similarity']
        # cols = ['Date', 'Return', 'Fund Flow', 'Alpha', 'Total Risk', 'Idiosyncratic Risk', 'Systematic Risk', \
        #         'Negativity', 'Confidence', 'Complexity', 'Personalness', 'Similarity']
    df = df_results[cols]
    df_corr = df.corr(method='pearson')
    df_kendall = df.corr(method='kendall')

    '''Write descriptives to .xlsx'''
    with pd.ExcelWriter(DESCRIPTIVES_FILENAME) as writer:
        df_month_descriptives.to_excel(writer, sheet_name='Months')
        df_letters.to_excel(writer, sheet_name='Letters')
        df_funds.to_excel(writer, sheet_name='Funds')
        df_corr.to_excel(writer, sheet_name='Correlation')
        df_kendall.to_excel(writer, sheet_name='Kendall')


def get_regression_df():
    """Creates the dataframe used in the regressions of fund performance measures on textual features + controls."""

    # Read csv file with fund characteristics, performance measures and textual scores
    df_results = pd.read_csv(RESULTS_WINSORIZED_FILENAME)
    df_results['Date'] = pd.to_datetime(df_results['Date'], format='%Y/%m/%d')

    # Sort dataframe by FundNo and Date
    df_results = df_results.sort_values(by=['FundNo', 'Date'], ascending=[True, True])

    # Compute surprise measures as first difference of absolute scores (different for summarized letters)
    df_results['Negativity Surprise'] = df_results.groupby('FundNo')['Negativity'].diff()
    df_results['Confidence Surprise'] = df_results.groupby('FundNo')['Confidence'].diff()

    if not SUMMARIZATION_SCORES:
        df_results['Complexity Surprise'] = df_results.groupby('FundNo')['Complexity'].diff()
        df_results['Personalness Surprise'] = df_results.groupby('FundNo')['Personalness'].diff()

    # Compute log-transformed variables
    df_results['LN(Fund Size)'] = np.log(df_results['Fund Size'])
    df_results['LN(Fund Age)'] = np.log(df_results['Fund Age'])

    # Compute number of funds in letter
    if SINGLE_FUND_ONLY == False:
        df_results['No. funds'] = df_results['series-id'].apply(lambda x: len(x.split(',')))
        df_results['LN(No. funds)'] = np.log(df_results['No. funds'])

    # Create dummy variable indicating whether or not fund is team-managed
    df_results['Team Managed'] = 0
    df_results.loc[df_results['mgr_name'].str.contains('Team Managed'), 'Team Managed'] = 1

    # Remove rows with missing values
    df_results = df_results.dropna(subset=['Lagged Return', 'Return', 'Lagged Fund Flow', 'Fund Flow', 'Fund Age'])
    df_results = df_results.reset_index(drop=True)

    # Compute return and fund flow quintiles
    df_results['Return Quintile'] = df_results.groupby(['Quarter Date'])['Lagged Return'].transform(
        lambda x: pd.qcut(x, 5, duplicates='drop', labels=False))
    df_results['Flow Quintile'] = df_results.groupby(['Quarter Date'])['Lagged Fund Flow'].transform(
        lambda x: pd.qcut(x, 5, duplicates='drop', labels=False))

    # Create dummy variables for return and flow quintiles (leave out bottom quintiles due to multicollinearity)
    df_results['Return Middle Quintiles'] = 0
    df_results['Return Upper Quintile'] = 0
    df_results.loc[df_results['Return Quintile'].isin([1, 2, 3]), 'Return Middle Quintiles'] = 1
    df_results.loc[df_results['Return Quintile'] == 4, 'Return Upper Quintile'] = 1

    df_results['Flow Middle Quintiles'] = 0
    df_results['Flow Upper Quintile'] = 0
    df_results.loc[df_results['Flow Quintile'].isin([1, 2, 3]), 'Flow Middle Quintiles'] = 1
    df_results.loc[df_results['Flow Quintile'] == 4, 'Flow Upper Quintile'] = 1

    # Write complete dataframe to csv file
    df_results.to_csv(RESULTS_COMPLETE_FILENAME, index=False)


def fit_regression(df, dep_var, regressors):
    """Fits fixed effect regression and calculates LR statistics."""
    Y = df[dep_var]
    X = sm.add_constant(df[regressors])

    FE_model = PanelOLS(Y, X, entity_effects=True, time_effects=True, drop_absorbed=True)  # Two-way fixed effects
    res = FE_model.fit(cov_type='clustered', cluster_entity=True, cluster_time=True)

    # Run pooled OLS (H0 of no fixed effects and H0 of only constant)
    pooled = PooledOLS(Y, X)
    res_pooled = pooled.fit(cov_type='clustered', cluster_entity=True, cluster_time=True)

    pooled_constant = PooledOLS(Y, X.iloc[:, 0])
    res_pooled_constant = pooled_constant.fit(cov_type='clustered', cluster_entity=True, cluster_time=True)

    # Compute LR statistics
    LR_stat = -2 * (res_pooled.loglik - res.loglik)
    df_LR = res.df_model - res_pooled.df_model
    LR_p_value = chi2.sf(LR_stat, df_LR)

    LR_stat_constant = -2 * (res_pooled_constant.loglik - res.loglik)
    df_LR_constant = res.df_model - res_pooled_constant.df_model
    LR_constant_p_value = chi2.sf(LR_stat_constant, df_LR_constant)

    return res, LR_p_value, LR_constant_p_value

def run_baseline_regressions(calc_regresion_df=False, bBootstrap=False, bPanelBootstrap=False, nBootstrap=100):
    """
    Runs baseline regressions a la Hillert et al. (2016)
    :param bBootstrap: if True, it calculates bootstrap standard errors
    :type bBootstrap: bool
    :param bPanelBootstrap: if True, it performs bootstrapped Panel OLS
    :type bPanelBootstrap: bool
    :param nBootstrap: number of iterations in the bootstrap algorithm
    :type nBootstrap: int
    """
    # Define dependent, independent and control variables (different for summarized letters)
    dep_vars = ['Future Return', 'Future Fund Flow', 'Alpha FF3', 'Volatility']

    if SUMMARIZATION_SCORES:
        indep_abs_vars = ['Negativity', 'Confidence', 'Similarity']
        indep_surprise_vars = ['Negativity Surprise', 'Confidence Surprise', 'Similarity']
    else:
        indep_abs_vars = ['Negativity', 'Complexity', 'Confidence', 'Personalness', 'Similarity']
        indep_surprise_vars = ['Negativity Surprise', 'Complexity Surprise', 'Confidence Surprise', 'Personalness Surprise', 'Similarity']

    if SINGLE_FUND_ONLY:
        # Do not include LN(No. funds) as control variable
        control_vars = ['LN(Fund Size)', 'LN(Fund Age)', 'Expense Ratio', 'Management Fee', 'Turnover Ratio',
                        'Team Managed', 'Lagged Return', 'Lagged Fund Flow', 'Lagged Volatility', 'Return Middle Quintiles',
                        'Return Upper Quintile', 'Flow Middle Quintiles', 'Flow Upper Quintile']
    else:
        control_vars = ['LN(Fund Size)', 'LN(Fund Age)', 'Expense Ratio', 'Management Fee', 'Turnover Ratio', 'Team Managed',
                        'LN(No. funds)', 'Lagged Return', 'Lagged Fund Flow', 'Lagged Volatility', 'Return Middle Quintiles', 'Return Upper Quintile',
                        'Flow Middle Quintiles', 'Flow Upper Quintile']

    if calc_regresion_df or (not os.path.exists(RESULTS_COMPLETE_FILENAME)):
        df_results = get_regression_df()
    else:
        df_results = pd.read_csv(RESULTS_COMPLETE_FILENAME)
        df_results['Date'] = pd.to_datetime(df_results['Date'], format='%Y/%m/%d')

    # For bootstrapped standard errors, create copy of dataframe
    if bBootstrap or bPanelBootstrap:
        df_input = df_results.copy()

    # Construct bivariate index (needed for fixed effects regressions)
    df_results.set_index(['FundNo', 'Date'], inplace=True)

    '''Run regressions on absolute and surprise textual scores'''
    bAbs = True

    # Loop over both sets of independent variables (absolute and surprise)
    for indep_vars in [indep_abs_vars, indep_surprise_vars]:
        # Create empty dataframe and collect regressors
        df_regression_output = pd.DataFrame(columns=dep_vars)
        df_residuals = pd.DataFrame(columns=dep_vars)
        regressors = [val for sublist in [indep_vars, control_vars] for val in sublist]

        # Loop over all dependent variables
        for dep_var in dep_vars:
            # Run two-way fixed effects regression
            res, LR_p_value, LR_constant_p_value = fit_regression(df_results, dep_var, regressors)
            params = res.params.rename(index={'const': 'Constant'})
            f_stat_p_value = res.f_statistic_robust.pval

            # Obtain t-statistics using desired procedure
            if bBootstrap:
                # Obtain bootstrap standard errors and compute t-statistics
                cols = [val for sublist in [['FundNo', 'Date'], [dep_var], regressors] for val in sublist]
                boot_se = bs.get_bootstrap_se(df_input[cols], num_iter=nBootstrap)
                tstats = params / boot_se
            elif bPanelBootstrap:
                tstats = res.tstats.rename(index={'const': 'Constant'})
                cols = [val for sublist in [['FundNo', 'Date'], [dep_var], regressors] for val in sublist]
                p_values = bs.panel_bootstrap(df_input[cols], tstats, num_iter=nBootstrap)
                p_values = p_values.fillna(99)
            else:
                # Obtain t-statistics based on double-clustered standard errors
                tstats = res.tstats.rename(index={'const': 'Constant'})

            # Compute Kendall's tau between fitted and observed values
            df_compare = pd.merge(res.model.dependent.dataframe, res.fitted_values, left_index=True, right_index=True)
            df_compare = df_compare.rename(columns={dep_var:'y_obs', 'fitted_values': 'y_fit'})
            kendalls_tau = df_compare[['y_obs', 'y_fit']].corr(method='kendall').iloc[0,1]

            # Combine output and append to dataframe
            tstats = tstats.fillna(0)
            if bPanelBootstrap:
                output_merged = pd.concat([params, p_values], axis=1).stack().reset_index(1, drop=True).to_frame(dep_var)
            else:
                output_merged = pd.concat([params, tstats], axis=1).stack().reset_index(1, drop=True).to_frame(dep_var)
            output_merged = pd.concat([output_merged[dep_var], pd.Series([res.rsquared_within, res.nobs, kendalls_tau,
                                                                        LR_p_value, LR_constant_p_value, f_stat_p_value])], axis=0)
            output_merged = output_merged.rename(index={0: 'R^2', 1: 'Obs.', 2: 'Kendalls Tau',
                                                        3: 'LR Pooled', 4: 'LR Constant', 5: 'F-statistic'})
            # output_merged.columns = [dep_var]
            df_regression_output[dep_var] = output_merged

            # Obtain residuals and append to dataframe
            resids = res.resids.to_frame(dep_var)
            df_residuals[dep_var] = resids

        # Write regression output to csv file
        if bAbs:
            if bBootstrap or bPanelBootstrap:
                df_regression_output.to_csv(RESULTS_REGRESSION_ABS_BOOTSTRAP_FILENAME, index=True)
                df_residuals.to_csv(RESULTS_RESIDUALS_ABS_BOOTSTRAP_FILENAME, index=True)
            else:
                df_regression_output.to_csv(RESULTS_REGRESSION_ABS_FILENAME, index=True)
                df_residuals.to_csv(RESULTS_RESIDUALS_ABS_FILENAME, index=True)
            bAbs = False
        else:
            if bBootstrap or bPanelBootstrap:
                df_regression_output.to_csv(RESULTS_REGRESSION_REL_BOOTSTRAP_FILENAME, index=True)
                df_residuals.to_csv(RESULTS_RESIDUALS_REL_BOOTSTRAP_FILENAME, index=True)
            else:
                df_regression_output.to_csv(RESULTS_REGRESSION_REL_FILENAME, index=True)
                df_residuals.to_csv(RESULTS_RESIDUALS_REL_FILENAME, index=True)


def run_additional_regressions(calc_regresion_df=False):
    """
    1) Runs regressions on individual X's
    2) Calculates variance inflation factor
    3) Runs alpha regressions
    """
    # Define dependent, independent and control variables (different for summarized letters)
    dep_vars = ['Future Return', 'Future Fund Flow', 'Alpha CAPM', 'Alpha FF3', 'Alpha CH4', 'Volatility']

    if SUMMARIZATION_SCORES:
        indep_abs_vars = ['Negativity', 'Confidence', 'Similarity']
        indep_surprise_vars = ['Negativity Surprise', 'Confidence Surprise']
    else:
        indep_abs_vars = ['Negativity', 'Complexity', 'Confidence', 'Personalness', 'Similarity']
        indep_surprise_vars = ['Negativity Surprise', 'Complexity Surprise', 'Confidence Surprise', 'Personalness Surprise']

    if SINGLE_FUND_ONLY:
        # Do not include LN(No. funds) as control variable
        control_vars = ['LN(Fund Size)', 'LN(Fund Age)', 'Expense Ratio', 'Management Fee', 'Turnover Ratio',
                        'Team Managed', 'Lagged Return', 'Lagged Fund Flow', 'Lagged Volatility', 'Return Middle Quintiles',
                        'Return Upper Quintile', 'Flow Middle Quintiles', 'Flow Upper Quintile']
    else:
        control_vars = ['LN(Fund Size)', 'LN(Fund Age)', 'Expense Ratio', 'Management Fee', 'Turnover Ratio', 'Team Managed',
                        'LN(No. funds)', 'Lagged Return', 'Lagged Fund Flow', 'Lagged Volatility', 'Return Middle Quintiles', 'Return Upper Quintile',
                        'Flow Middle Quintiles', 'Flow Upper Quintile']

    if calc_regresion_df or (not os.path.exists(RESULTS_COMPLETE_FILENAME)):
        get_regression_df()
    df_results = pd.read_csv(RESULTS_COMPLETE_FILENAME)
    df_results['Date'] = pd.to_datetime(df_results['Date'], format='%Y/%m/%d')

    # For bootstrapped standard errors, create copy of dataframe
    if bBootstrap or bPanelBootstrap:
        df_input = df_results.copy()

    # Construct bivariate index (needed for fixed effects regressions)
    df_results.set_index(['FundNo', 'Date'], inplace=True)

    '''Run regressions on absolute and surprise textual scores'''
    # 1) Run regressions on individual X's
    # Loop over both sets of independent variables (absolute and surprise)
    for indep_var in indep_abs_vars + indep_surprise_vars:
        # Create empty dataframe and collect regressors
        df_regression_output = pd.DataFrame(columns=dep_vars)
        regressors = [indep_var] + control_vars

        # Loop over all dependent variables
        for dep_var in dep_vars:
            # Run two-way fixed effects regression
            res, LR_p_value, LR_constant_p_value = fit_regression(df_results, dep_var, regressors)
            params = res.params.rename(index={'const': 'Constant'})
            tstats = res.tstats.rename(index={'const': 'Constant'})
            f_stat_p_value = res.f_statistic_robust.pval

            # # Obtain t-statistics using desired procedure
            # if bBootstrap:
            #     # Obtain bootstrap standard errors and compute t-statistics
            #     cols = [val for sublist in [['FundNo', 'Date'], [dep_var], regressors] for val in sublist]
            #     boot_se = bs.get_bootstrap_se(df_input[cols], num_iter=nBootstrap)
            #     tstats = params / boot_se
            # elif bPanelBootstrap:
            #     tstats = res.tstats.rename(index={'const': 'Constant'})
            #     cols = [val for sublist in [['FundNo', 'Date'], [dep_var], regressors] for val in sublist]
            #     p_values = bs.panel_bootstrap(df_input[cols], tstats, num_iter=nBootstrap)
            #     p_values = p_values.fillna(99)
            # else:
            #     # Obtain t-statistics based on double-clustered standard errors
            #     tstats = res.tstats.rename(index={'const': 'Constant'})

            # Combine output and append to dataframe
            tstats = tstats.fillna(0)
            # if bPanelBootstrap:
            #     output_merged = pd.concat([params, p_values], axis=1).stack().reset_index(1, drop=True).to_frame(dep_var)
            # else:
            output_merged = pd.concat([params, tstats], axis=1).stack().reset_index(1, drop=True).to_frame(dep_var)
            output_merged = pd.concat([output_merged[dep_var], pd.Series([res.rsquared_within, res.nobs,
                                                                        LR_p_value, LR_constant_p_value, f_stat_p_value])], axis=0)
            output_merged = output_merged.rename(index={0: 'R^2', 1: 'Obs.',
                                                        2: 'LR Pooled', 3: 'LR Constant', 4: 'F-statistic'})
            df_regression_output[dep_var] = output_merged

        # Save output
        if os.path.exists(RESULTS_INDIVID_REGRESSION_FILENAME):
            with pd.ExcelWriter(RESULTS_INDIVID_REGRESSION_FILENAME, engine='openpyxl', mode='a') as writer:
                df_regression_output.to_excel(writer, sheet_name=indep_var)
        else:
            df_regression_output.to_excel(RESULTS_INDIVID_REGRESSION_FILENAME, sheet_name=indep_var)

    # 2) Calculate variance inflation factor
    # Loop over both sets of independent variables (absolute and surprise)
    df_vif = pd.DataFrame(columns=indep_abs_vars + indep_surprise_vars, index=['all', 'textual_features'])
    for indep_vars in [indep_abs_vars, indep_surprise_vars]:
        regressors = [val for sublist in [indep_vars, control_vars] for val in sublist]

        # Calculate VIF for (i) all regressors and (ii) only textual features
        df_regressors = df_results[regressors].dropna()
        df_indep_vars = df_results[indep_vars].dropna()
        for i, indep_var in enumerate(indep_vars):
            df_vif.loc['all', indep_var] = variance_inflation_factor(df_regressors, i)
            df_vif.loc['textual_features', indep_var] = variance_inflation_factor(df_indep_vars, i)

    if os.path.exists(RESULTS_ADD_REGRESSION_FILENAME):
        with pd.ExcelWriter(RESULTS_ADD_REGRESSION_FILENAME, engine='openpyxl', mode='a') as writer:
            df_vif.to_excel(writer, sheet_name='VIF')
    else:
        df_vif.to_excel(RESULTS_ADD_REGRESSION_FILENAME, sheet_name='VIF')

    # 3) Run alpha regressions
    # Loop over both sets of independent variables (absolute and surprise)
    alpha_vars = ['Alpha CAPM', 'Alpha FF3', 'Alpha CH4']
    sheet_names = ['absolute', 'surprise']
    for i, indep_vars in enumerate([indep_abs_vars, indep_surprise_vars]):
        # Create empty dataframe and collect regressors
        df_regression_output = pd.DataFrame(columns=alpha_vars)
        regressors = indep_vars + control_vars

        # Loop over all dependent variables
        for dep_var in alpha_vars:
            # Run two-way fixed effects regression
            res, LR_p_value, LR_constant_p_value = fit_regression(df_results, dep_var, regressors)
            params = res.params.rename(index={'const': 'Constant'})
            tstats = res.tstats.rename(index={'const': 'Constant'})
            f_stat_p_value = res.f_statistic_robust.pval

            # # Obtain t-statistics using desired procedure
            # if bBootstrap:
            #     # Obtain bootstrap standard errors and compute t-statistics
            #     cols = [val for sublist in [['FundNo', 'Date'], [dep_var], regressors] for val in sublist]
            #     boot_se = bs.get_bootstrap_se(df_input[cols], num_iter=nBootstrap)
            #     tstats = params / boot_se
            # elif bPanelBootstrap:
            #     tstats = res.tstats.rename(index={'const': 'Constant'})
            #     cols = [val for sublist in [['FundNo', 'Date'], [dep_var], regressors] for val in sublist]
            #     p_values = bs.panel_bootstrap(df_input[cols], tstats, num_iter=nBootstrap)
            #     p_values = p_values.fillna(99)
            # else:
            #     # Obtain t-statistics based on double-clustered standard errors
            #     tstats = res.tstats.rename(index={'const': 'Constant'})

            # Combine output and append to dataframe
            tstats = tstats.fillna(0)
            # if bPanelBootstrap:
            #     output_merged = pd.concat([params, p_values], axis=1).stack().reset_index(1, drop=True).to_frame(dep_var)
            # else:
            output_merged = pd.concat([params, tstats], axis=1).stack().reset_index(1, drop=True).to_frame(dep_var)
            output_merged = pd.concat([output_merged[dep_var], pd.Series([res.rsquared_within, res.nobs,
                                                                        LR_p_value, LR_constant_p_value, f_stat_p_value])], axis=0)
            output_merged = output_merged.rename(index={0: 'R^2', 1: 'Obs.',
                                                        2: 'LR Pooled', 3: 'LR Constant', 4: 'F-statistic'})
            df_regression_output[dep_var] = output_merged

        # Save output
        with pd.ExcelWriter(RESULTS_ADD_REGRESSION_FILENAME, engine='openpyxl', mode='a') as writer:
            df_regression_output.to_excel(writer, sheet_name=sheet_names[i])


def plot_timeseries(save_directory=PATH_FIGURES, plot=False):
    """
    Converts measures to U(0,1) and plots both original measure and transformed over time
    :param save_directory: path to which the results should be written
    :type save_directory: str
    :param plot: if True, a plot of the results is created
    :type plot: bool
    """
    # Read CSV and format
    df = pd.read_csv(RESULTS_WINSORIZED_FILENAME)
    df['Date'] = pd.to_datetime(df['Date'], format='%Y/%m/%d')

    # Select columns (different for summarized letters)
    if SUMMARIZATION_SCORES:
        cols = ['Date', 'Return', 'Negativity', 'Confidence', 'Similarity']
    else:
        cols = ['Date', 'Return', 'Negativity', 'Complexity', 'Confidence', 'Personalness', 'Similarity']
    df = df[cols]

    # Seperate df for similarity since it has missing values
    df_similarity = df[['Date', 'Similarity']]
    df_similarity = df_similarity.dropna()

    # Dataframe storing dates, returns, and measures over time
    df_over_time = df[['Date', 'Return']].groupby(['Date'], as_index=False).agg('mean')
    quantiles, cumprob = convert_to_uniform(df_over_time['Return'])
    df_returns = pd.DataFrame({'Dist return': cumprob, 'Return': quantiles})
    df_over_time = df_over_time.merge(df_returns, on='Return')

    # Create separate dataframe for similarity since it starts one quarter later
    df_over_time_similarity = df_over_time.iloc[1:, :].reset_index(inplace=False, drop=True)

    measures = cols[2:]
    cols_to_select = [1, 4, 5, 6, 9, 12, 13, 14]  # Select mean, 25%, 50%, 75% for both measure and its U(0,1)-transformation
    for measure in measures:
        # Calculate empirical distribution
        if measure == 'Similarity':
            quantiles, cumprob = convert_to_uniform(df_similarity[measure])
            df_measure = pd.DataFrame({'Dist ' + measure: cumprob, measure: quantiles})
            df_temp = df_similarity[['Date', measure]]
        else:
            quantiles, cumprob = convert_to_uniform(df[measure])
            df_measure = pd.DataFrame({'Dist ' + measure: cumprob, measure: quantiles})
            df_temp = df[['Date', measure]]

        # Concatenate to dataframe
        df_temp = df_measure.merge(df_temp, on=measure)
        df_temp = df_temp.groupby(['Date'], as_index=False).agg('describe')

        df_temp = df_temp.iloc[:, cols_to_select]

        if measure == 'Similarity':
            df_over_time_similarity = pd.concat([df_over_time_similarity, df_temp], axis=1, sort=False)
        else:
            df_over_time = pd.concat([df_over_time, df_temp], axis=1, sort=False)

        # Plot or save data
        if plot:
            df_plot = pd.concat([df_over_time['Date'], df_temp], axis=1, sort=False)
            fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(15, 6))

            df_plot.plot(x='Date', title=measure, y=list(df_plot)[4:], ax=ax[0], legend=False)
            df_plot.plot(x='Date', title=measure + ' transformed', y=list(df_plot)[1:4], ax=ax[1], legend=False)

            plotName = save_directory + measure + '.eps'
            plt.savefig(plotName, format='eps')

    with pd.ExcelWriter(TIMESERIES_DATA_FILENAME) as writer:
        df_over_time.to_excel(writer, sheet_name='Timeseries', index=False)
        df_over_time_similarity.to_excel(writer, sheet_name='Similarity', index=False)


def convert_to_uniform(data, dropna=False, drop_duplicates=False):
    """
    Calculate empirical distribution for a given array
    :param data: data
    :type data: np.darray
    :param dropna: if True, NaN values are dropped
    :type dropna: bool
    :param drop_duplicates: if True, duplicates values are removed
    :type drop_duplicates: bool
    :return: quantiles and cumulative probabilities of empirical distribution
    :rtype: tuple
    """
    data = np.atleast_1d(data)
    if dropna:
        data = data[~np.isnan(data)]
    if drop_duplicates:
        data = np.unique(data)
    quantiles, counts = np.unique(data, return_counts=True)

    cumprob = np.cumsum(counts).astype(np.double) / data.size

    return quantiles, cumprob


def measures_to_uniform():
    """ Transforms performance measures to U(0,1) variables """
    df = pd.read_csv(RESULTS_WINSORIZED_FILENAME)

    df = df.rename(columns={'Total Risk': 'Volatility'})
    perf_measures = ['Future Return', 'Future Fund Flow', 'Alpha', 'Volatility']

    # Select text measures (different for summarized letters)
    if SUMMARIZATION_SCORES:
        text_measures = ['Negativity', 'Confidence', 'Similarity']
    else:
        text_measures = ['Negativity', 'Complexity', 'Confidence', 'Personalness', 'Similarity']
    measures = perf_measures + text_measures
    df = df[measures]
    df.drop_duplicates(subset=measures, inplace=True)

    for measure in measures:
        quantiles, cumprob = convert_to_uniform(df[measure], dropna=True, drop_duplicates=True)
        df_temp = pd.DataFrame({measure: quantiles, measure + ' transformed': cumprob})
        df = pd.merge(df, df_temp, on=measure)

    df.to_csv(MEASURES_TRANSFORMED_FILENAME, index=False)


def residual_distributions():
    """
    Plots histograms for the distributions of regression residuals
    :return: histogram of residuals
    :rtype: pd.DataFrame.Plot
    """
    # Read csv file with regression residuals
    df_residuals = pd.read_csv(RESULTS_RESIDUALS_ABS_FILENAME)

    # Variables of interest
    dep_vars = ['Future Return', 'Future Fund Flow', 'Alpha', 'Volatility']
    # dep_vars = ['Future Return', 'Future Fund Flow', 'Alpha', 'Total Risk', 'Idiosyncratic Risk', 'Systematic Risk']

    # Remove residuals extremely close to zero
    tolerance = 10**(-10)

    df_residuals[abs(df_residuals[dep_vars]) < tolerance] = np.nan

    # Compute JB-statistic
    df_jb_test_res = pd.DataFrame()

    # Loop over columns with residuals
    for dep_var in dep_vars:
        # Winsorize data and compute JB-statistics
        cleaned_res_series = stats.mstats.winsorize(df_residuals[dep_var].dropna(), limits=0.025)
        jb_test_res = pd.Series(sm.stats.stattools.jarque_bera(cleaned_res_series, axis=0)).to_frame(dep_var)
        df_jb_test_res = pd.concat([df_jb_test_res, jb_test_res], axis=1)
        ax = sns.distplot(cleaned_res_series, fit=norm, kde=False)

    df_jb_test_res = df_jb_test_res.rename(index={0: 'JB-stat', 1: 'p-val', 2: 'Skewness', 3: 'Kurtosis'})

    # Create histograms for marginal distributions
    hist = df_residuals.hist(column=dep_vars, bins=100)

    return hist


def get_intersection(filename1='../../regression/TF_IDF/single_funds/results_winsorized.csv',
                     filename2='../../regression/TF_IDF/summarization/single_funds/results_winsorized.csv'):
    """ Takes intersection of existing file names"""

    filename1 = '/Users/maarten/apg/regression/TF_IDF/single_funds/results_winsorized.csv'
    filename2 = '/Users/maarten/apg/regression/proportional/single_funds/results_winsorized.csv'

    df1 = pd.read_csv(filename1)
    df2 = pd.read_csv(filename2)

    list1 = df1['file-name'].unique()
    list2 = df2['file-name'].unique()

    diff = list(np.setdiff1d(list1, list2))
    diff2 = list(np.setdiff1d(list2, list1))
    list1 = list(set(list1) - set(diff) - set(diff2))
    list2 = list(set(list2) - set(diff) - set(diff2))

    df1 = df1.loc[df1['file-name'].isin(list1)]
    df2 = df2.loc[df2['file-name'].isin(list2)]

    df1.to_csv(filename1)
    df2.to_csv(filename2)


if __name__ == '__main__':
    ...
    merge_CRSP_letters(write_funds_counter=write_funds_counter)
    print('Merging completed')
    winsorize(write_funds_counter=write_funds_counter, bSaveBootstrap=bSaveBootstrap)
    print('Winsorization completed')
    run_baseline_regressions(bBootstrap=bBootstrap, bPanelBootstrap=bPanelBootstrap)
    print('Regression analysis completed')
    # run_additional_regressions(calc_regresion_df=True)
