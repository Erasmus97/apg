import pandas as pd
import numpy as np
import sys
import statsmodels.api as sm2
from linearmodels import PanelOLS
from resample.bootstrap import bootstrap

sys.path.append('../../code/general/')  # Add directory of folder to path

import general.utils as utils

SINGLE_FUND_ONLY = False        # Boolean: if TRUE, only consider fund-specific shareholder letters
SCORES_TF_IDF = True            # Boolean: if TRUE, scores are computed using TF-IDF
CRISIS_SAMPLE = False           # Boolean: if TRUE, consider crisis subsample
SUMMARIZATION_SCORES = False    # Boolean: if TRUE, consider summarized letter scores

# -----------------------Paths-----------------------
if SINGLE_FUND_ONLY:
    PATH_RESULTS = '../../results/single_funds/'
    RESULTS_FILENAME = PATH_RESULTS + 'scores_tf-idf.csv'
    RESULTS_MERGED_FILENAME = PATH_RESULTS + 'scores_tf-idf_merged.csv'
elif SCORES_TF_IDF:
    PATH_RESULTS = '../../results/TF_IDF/'
    RESULTS_FILENAME = PATH_RESULTS + 'scores_tf-idf.csv'
    RESULTS_MERGED_FILENAME = PATH_RESULTS + 'scores_tf-idf_merged.csv'
elif CRISIS_SAMPLE:
    PATH_RESULTS = '../../results/crisis_subsample/'
    RESULTS_FILENAME = PATH_RESULTS + 'scores_tf-idf.csv'
    RESULTS_MERGED_FILENAME = PATH_RESULTS + 'scores_tf-idf_merged.csv'
elif SUMMARIZATION_SCORES:
    PATH_RESULTS = '../../results/summarization/'
    RESULTS_FILENAME = PATH_RESULTS + 'summary_tf-idf.csv'
    RESULTS_MERGED_FILENAME = PATH_RESULTS + 'summary_tf-idf_merged.csv'
else:
    PATH_RESULTS = '../../results/proportional/'
    RESULTS_FILENAME = PATH_RESULTS + 'scores_proportional.csv'
    RESULTS_MERGED_FILENAME = PATH_RESULTS + 'scores_proportional_merged.csv'

PATH_DATA = '../../data/'
PATH_CRSP = '../../data/CRSP/'          # Set relative paths for CRSP data files
PATH_EDGAR = '../../data/EDGAR/'
PATH_LETTERS = '../../data/letters/'    # Set relative paths for letter files
PATH_FIGURES = PATH_RESULTS + 'Figures/'

RESULTS_COMPLETE_FILENAME = PATH_RESULTS + 'results_complete.csv'


#
# * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * +

def fit_fe_regression(df_input):
    """
    Performs two-way fixed effects regression on dataframe and returns coefficient estimates
    :param df_input: data set that is used to fit fixed effects regression
    :type df_input: pandas.DataFrame
    :return: parameters of the regression
    :rtype: linearmodels.PanelOLS.Results
    """
    df_input = pd.DataFrame(df_input)

    # Construct bivariate index (needed for fixed effects regressions)
    df_input.set_index([0, 1], inplace=True)

    # Extract dependent and independent variables and perform fixed effects regression
    Y = df_input.iloc[:, 0]
    X = sm2.add_constant(df_input.iloc[:, 1:])
    # model_temp = PanelOLS(Y, X, entity_effects=False, time_effects=False, drop_absorbed=True) # Panel OLS
    model_temp = PanelOLS(Y, X, entity_effects=True, time_effects=True, drop_absorbed=True)  # Two-way fixed effects
    res_temp = model_temp.fit(cov_type='clustered', cluster_entity=True, cluster_time=True)

    # Obtain and return parameters
    params = res_temp.params.rename(index={'const': 'Constant'})

    return params


def get_bootstrap_se(df_input, num_iter=1000):
    """
    Computes bootstrapped standard errors for two-way fixed effects regression coefficient estimates
    :param df_input: data set that is used to fit fixed effects regression
    :type df_input: pandas.DataFrame
    :param num_iter: number of iterations in the bootstrap algorithm
    :type num_iter: int
    :return: bootstrapped standard errors
    :rtype: numpy.array
    """

    # Estimate regression coefficients for bootstrap samples
    boot_coef = bootstrap(a=df_input, f=fit_fe_regression, b=num_iter)

    # Estimate bootstrapped standard errors
    boot_se = boot_coef.std(axis=0)

    return boot_se


def sample_timeseries(fund, boot_sample):
    """
    Gets a unique sample for time series analysis given the fund name
    :param fund: fund name
    :type fund: str
    :param boot_sample: sample that is used in bootstrap analysis
    :type boot_sample: pandas.DataFrame
    :return: sample for time series analysis given the fund
    :rtype: pandas.DataFrame
    """
    # Extract relevant dates
    dates = boot_sample.loc[boot_sample['index'] == fund, ['Date']]
    dates = dates['Date'].tolist()

    # Sample from dates
    rows = boot_sample.loc[boot_sample['Date'].isin(dates)].sample(len(dates))
    rows = rows.sort_values(by=['Date'])
    rows['index'] = fund

    return rows


def panel_bootstrap(df_input, obs_t_stats, num_iter=100):
    """
    Performs Panel OLS in combination with bootstrap
    :param df_input: data set that is used to fit fixed effects regression
    :type df_input: pandas.DataFrame
    :param obs_t_stats: observed t-statistics
    :type obs_t_stats: numpy.array
    :param num_iter: number of itations for bootstrap algorithm
    :type num_iter: int
    :return: p-values of the bootstrapped Panel OLS
    :rtype: pandas.DataFrame
    """

    # Sample funds from sample
    start = utils.time.time()  # Keep track of time
    funds_list = df_input['FundNo'].unique()
    boot_funds = bootstrap(a=funds_list, b=num_iter)

    t_stats_array = np.zeros((len(df_input.columns)-2, num_iter))

    for i in range(num_iter):
        # Print progress
        if (i+1) % 10 == 0:
            current_time = round(utils.time.time() - start)
            print('Bootstrap iteration ' + str(i+1) + '. Time elapsed ' + str(current_time))

        # Select subsample
        boot_sample = df_input[df_input['FundNo'].isin(boot_funds[i])]

        # Create duplicate funds to retain correct number of clusters
        funds_sample = pd.DataFrame(boot_funds[i], columns=['FundNo'])
        funds_sample.reset_index(inplace=True)
        boot_sample = funds_sample.merge(boot_sample, how='left')

        # Sample from timeseries for each fund
        funds = boot_sample['index'].unique()

        df_boot_sample = [sample_timeseries(fund, boot_sample) for fund in funds]
        df_boot_sample = pd.concat(df_boot_sample, axis=0)

        # Set double index
        df_boot_sample.set_index(['index', 'Date'], inplace=True)
        df_boot_sample.drop(columns=['FundNo'], inplace=True)

        # Run FE regressions
        Y = df_boot_sample.iloc[:, 0]
        X = sm2.add_constant(df_boot_sample.iloc[:, 1:])
        est = PanelOLS(Y, X, entity_effects=True, time_effects=True, drop_absorbed=True)  # Two-way fixed effects
        res = est.fit(cov_type='clustered', cluster_entity=True, cluster_time=True)

        # Extract t-statistics
        t_stats = res.tstats.rename(index={'const': 'Constant'})
        t_stats_array[:, i] = res.tstats

    # Compute (finite-sample adjusted) empirical p-values
    num_greater = np.abs(t_stats_array) >= np.expand_dims(np.abs(obs_t_stats), axis=1)
    p_values = (np.sum(num_greater, axis=1) + 1) / (num_iter + 1)

    # Convert numpy array to dataframe with correct index
    df_p_values = t_stats.copy()
    df_p_values[:] = pd.Series(p_values)

    return df_p_values

