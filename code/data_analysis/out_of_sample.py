import pandas as pd
import numpy as np
import bottleneck as bn
import statsmodels.api as sm2
from linearmodels import PanelOLS
from datetime import datetime as dt
import data_analysis.copula_analysis as cop
from scipy.stats import rankdata

# -----------------------
# User defined parameters
# -----------------------

SINGLE_FUND_ONLY = False        # Boolean: if TRUE, only consider fund-specific shareholder letters
SCORES_TF_IDF = True            # Boolean: if TRUE, scores are computed using TF-IDF
CRISIS_SAMPLE = False           # Boolean: if TRUE, consider crisis subsample
SUMMARIZATION_SCORES = True    # Boolean: if TRUE, consider summarized letter scores

# -----------------------Paths-----------------------
if SCORES_TF_IDF:
    PATH_RESULTS = '../../results/TF_IDF/'
    if SUMMARIZATION_SCORES:
        PATH_RESULTS += 'summarization/'
        RESULTS_FILENAME = PATH_RESULTS + 'summary_tf-idf.csv'
        RESULTS_MERGED_FILENAME = PATH_RESULTS + 'summary_tf-idf_merged.csv'
    else:
        RESULTS_FILENAME = PATH_RESULTS + 'scores_tf-idf.csv'
        RESULTS_MERGED_FILENAME = PATH_RESULTS + 'scores_tf-idf_merged.csv'
else:
    PATH_RESULTS = '../../results/proportional/'
    if SUMMARIZATION_SCORES:
        PATH_RESULTS += 'summarization/'
        RESULTS_FILENAME = PATH_RESULTS + 'scores_proportional.csv'
        RESULTS_MERGED_FILENAME = PATH_RESULTS + 'summary_proportional_merged.csv'
    else:
        RESULTS_FILENAME = PATH_RESULTS + 'scores_proportional.csv'
        RESULTS_MERGED_FILENAME = PATH_RESULTS + 'scores_proportional_merged.csv'

if SINGLE_FUND_ONLY:
    PATH_RESULTS += 'single_funds/'
elif CRISIS_SAMPLE:
    PATH_RESULTS += 'crisis_subsample/'
else:
    PATH_RESULTS += 'baseline/'

PATH_DATA = '../../data/'
PATH_CRSP = '../../data/CRSP/'          # Set relative paths for CRSP data files
PATH_FRENCH = '../../data/French/'      # Set relative paths for Kenneth French data files
PATH_EDGAR = '../../data/EDGAR/'
PATH_LETTERS = '../../data/letters/'    # Set relative paths for letter files
PATH_FIGURES = PATH_RESULTS + 'Figures/'
PATH_SIMULATION = '../simulation/'

FF_FACTORS_FILENAME = PATH_FRENCH + 'F-F_Q.csv'
FF_MOM_FILENAME = PATH_FRENCH + 'F-F_Momentum_Factor.csv'
MERGED_DATA_FILENAME = PATH_CRSP + "CIK_merged.csv"
# -----------------------Results filenames-----------------------
RESULTS_COMPLETE_FILENAME = PATH_RESULTS + 'results_complete.csv'
RESULTS_WINSORIZED_FILENAME = PATH_RESULTS + 'results_winsorized.csv'

# -----------------------Output filenames-----------------------
FORECAST_TEST_OUTPUT_FILENAME = PATH_RESULTS + 'forecast_test.csv'

#
# * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * +


def produce_fcst(loss_type='squared', measure_type='DM'):
    """
    Produces out-of-sample forecasts
    :param loss_type: type of loss function
    :type loss_type: str
    :param measure_type: test for difference in forecasting accuracy (Diebold-Mariano)
    :type measure_type: str
    """

    # Read csv file with regression input
    df_results = pd.read_csv(RESULTS_COMPLETE_FILENAME)
    df_results['Date'] = pd.to_datetime(df_results['Date'], format='%Y-%m-%d')

    # Define dependent, independent and control variables (different for summarized letters)
    dep_vars = ['Return', 'Fund Flow', 'Alpha', 'Volatility']
    # dep_vars = ['Future Return', 'Future Fund Flow', 'Alpha', 'Total Risk', 'Idiosyncratic Risk', 'Systematic Risk']
    if SUMMARIZATION_SCORES:
        indep_abs_vars = ['Negativity', 'Confidence', 'Similarity']
        indep_surprise_vars = ['Negativity Surprise', 'Confidence Surprise', 'Similarity']
    else:
        indep_abs_vars = ['Negativity', 'Complexity', 'Confidence', 'Personalness', 'Similarity']
        indep_surprise_vars = ['Negativity Surprise', 'Complexity Surprise', 'Confidence Surprise', 'Personalness Surprise',
                               'Similarity']

    if SINGLE_FUND_ONLY:
        # Do not include LN(No. funds) as control variable
        control_vars = ['LN(Fund Size)', 'LN(Fund Age)', 'Expense Ratio', 'Management Fee', 'Turnover Ratio',
                        'Team Managed', 'Lagged Return', 'Lagged Fund Flow', 'Return Middle Quintiles',
                        'Return Upper Quintile', 'Flow Middle Quintiles', 'Flow Upper Quintile']
    else:
        control_vars = ['LN(Fund Size)', 'LN(Fund Age)', 'Expense Ratio', 'Management Fee', 'Turnover Ratio',
                        'Team Managed', 'LN(No. funds)', 'Lagged Return', 'Lagged Fund Flow', 'Return Middle Quintiles',
                        'Return Upper Quintile', 'Flow Middle Quintiles', 'Flow Upper Quintile']

    # Set dates corresponding to in-sample
    in_sample_start = dt.strptime(str(int(float('20060101'))), '%Y%m%d').date()
    out_of_sample_start = dt.strptime(str(int(float('20101231'))), '%Y%m%d').date()

    # Select subsample corresponding to in-sample
    mask_in_sample = (df_results['Date'] >= in_sample_start) & (df_results['Date'] <= out_of_sample_start)
    df_in_sample = df_results.loc[mask_in_sample]           # In-sample
    df_in_sample = df_in_sample.reset_index(drop=True)
    df_out_of_sample = df_results.loc[-mask_in_sample]      # Out-of-sample
    df_out_of_sample = df_out_of_sample.reset_index(drop=True)

    # Construct bivariate index (needed for fixed effects regressions)
    df_in_sample.set_index(['FundNo', 'Date'], inplace=True)
    df_out_of_sample.set_index(['FundNo', 'Date'], inplace=True)

    cols = ['Dependent variable', 'Independent vars.', 'Loss Type', 'Test', 'FE-Copula', 'FE-Random', 'Copula-Random']
    df_test_results = pd.DataFrame(columns=cols)
    bAbs = 'Absolute'

    # Loop over both sets of independent variables (absolute and surprise)
    for indep_vars in [indep_abs_vars, indep_surprise_vars]:
        # Create empty dataframe and collect regressors
        df_fcst_errors = pd.DataFrame()
        regressors = [val for sublist in [indep_vars, control_vars] for val in sublist]

        # Loop over all dependent variables
        for dep_var in dep_vars:
            # Produce forecasts using two-way fixed effects regression
            fcst_fe_reg = forecast_fe_regression(df_in_sample, df_out_of_sample, dep_var, regressors)

            # Produce forecasts using copula
            df_copula_fcsts = pd.DataFrame()

            # Loop over independent variables
            for indep_var in indep_vars:
                fcst_copula = cop.fit_copula(data=df_in_sample[[dep_var, indep_var]], dist_marginal='Empirical', nSim=1000, bForecast=True, x_out_of_sample=df_out_of_sample[dep_var])
                df_copula_fcsts = pd.concat([df_copula_fcsts, pd.DataFrame(fcst_copula)], axis=1)

            # Final copula forecast is average
            fcst_copula = df_copula_fcsts.mean(axis=1)

            # Random walk forecast (unconditional mean)
            fcst_rw = pd.Series(df_in_sample[dep_var].mean()).repeat(len(df_out_of_sample))

            # Compute losses
            df_fe_reg = pd.concat([pd.DataFrame(df_out_of_sample[dep_var]).reset_index(), pd.DataFrame(fcst_fe_reg).reset_index()], axis=1)
            df_fe_reg = df_fe_reg.loc[:, ~df_fe_reg.columns.duplicated()]
            df_fe_reg.set_index(['FundNo', 'Date'], inplace=True)
            df_fe_reg = df_fe_reg.rename(columns={dep_var: 'observations'})

            df_copula = pd.concat([pd.DataFrame(df_out_of_sample[dep_var]).reset_index(), pd.DataFrame(fcst_copula).reset_index()], axis=1)
            df_copula = df_copula.drop(columns=['index'])
            df_copula.set_index(['FundNo', 'Date'], inplace=True)
            df_copula = df_copula.rename(columns={dep_var: 'observations', 0: 'predictions'})

            df_rw = pd.concat([pd.DataFrame(df_out_of_sample[dep_var]).reset_index(), pd.DataFrame(fcst_rw).reset_index()], axis=1)
            df_rw = df_rw.drop(columns=['index'])
            df_rw.set_index(['FundNo', 'Date'], inplace=True)
            df_rw = df_rw.rename(columns={dep_var: 'observations', 0: 'predictions'})

            # df_complete = pd.merge(df_fe_reg, df_copula['predictions'], left_index=True, right_index=True)

            loss_fe_reg = compute_loss_function(df_fe_reg, loss_type=loss_type)
            loss_copula = compute_loss_function(df_copula, loss_type=loss_type)
            loss_rw = compute_loss_function(df_rw, loss_type=loss_type)

            # Test for difference in forecasting accuracy
            test_stat_fe_cop = compute_fcst_measure(pd.concat([pd.DataFrame(loss_fe_reg), pd.DataFrame(loss_copula)], axis=1), measure_type=measure_type)
            test_stat_fe_rw = compute_fcst_measure(pd.concat([pd.DataFrame(loss_fe_reg), pd.DataFrame(loss_rw)], axis=1), measure_type=measure_type)
            test_stat_cop_rw = compute_fcst_measure(pd.concat([pd.DataFrame(loss_copula), pd.DataFrame(loss_rw)], axis=1), measure_type=measure_type)

            # Collect results
            test_results = pd.Series({'Dependent variable': dep_var, 'Independent vars.': bAbs, 'Loss Type': loss_type,
                                      'Test': measure_type, 'FE-Copula': test_stat_fe_cop, 'FE-Random': test_stat_fe_rw, 'Copula-Random': test_stat_cop_rw})
            df_test_results = df_test_results.append(test_results, ignore_index=True)

        bAbs = 'Surprise'

    # Write test output to file
    df_test_results.to_csv(FORECAST_TEST_OUTPUT_FILENAME, index=False)


def forecast_fe_regression(df_in_sample, df_out_of_sample, dep_var, regressors):
    """
    Produces out-of-sample forecasts using two-way fixed effects regression
    :param df_in_sample: in-sample data set
    :type df_in_sample: pandas.DataFrame
    :param df_out_of_sample: out-of-sample data set
    :type df_out_of_sample: pandas.DataFrame
    :param dep_var: name of the dependent variable
    :type dep_var: str
    :param regressors: names of the regressors
    :type regressors: str
    :return: the forecasted values
    :rtype: pandas.DataFrame
    """
    # Run two-way fixed effects regression
    Y_in_sample = df_in_sample[dep_var]
    X_in_sample = sm2.add_constant(df_in_sample[regressors])
    model = PanelOLS(Y_in_sample, X_in_sample, entity_effects=True, time_effects=True, drop_absorbed=True)
    res = model.fit(cov_type='clustered', cluster_entity=True, cluster_time=True)

    # Obtain parameters
    params = res.params.rename(index={'const': 'Constant'})

    # Produce out-of-sample forecasts
    X_out_of_sample = sm2.add_constant(df_out_of_sample[regressors])
    fcst = model.predict(params=params, exog=X_out_of_sample)

    return fcst


def compute_loss_function(df_fcst_results, loss_type='squared'):
    """
    Transforms forecast error series using loss function
    :param df_fcst_results: forecasted values
    :type df_fcst_results: pandas.DataFrame
    :param loss_type: type of loss function
    :type loss_type: str
    :return: loss values of the forecasts
    :rtype: numpy.darray
    """
    # Read input data
    observations = np.array(df_fcst_results['observations'])
    predictions = np.array(df_fcst_results['predictions'])
    errors = observations - predictions

    # Determine loss depending on loss function
    if loss_type == 'squared':
        loss = errors ** 2
    elif loss_type == 'absolute':
        loss = np.abs(errors)
    elif loss_type == 'relative':
        loss = np.abs(errors / observations)

    return loss


def compute_fcst_measure(df_loss_results, measure_type='DM'):
    """
    Computes forecasting accuracy measure
    :param df_loss_results: loss values of the forecasted results
    :type df_loss_results: pandas.DataFrame
    :param measure_type: test for difference in forecasting accuracy (Diebold-Mariano)
    :type measure_type: str
    """

    # Read input data
    loss_1 = np.array(df_loss_results.iloc[:, 0])
    loss_2 = np.array(df_loss_results.iloc[:, 1])

    # Determine forecasting accuracy measure depending on type
    if measure_type == 'DM':
        differential  = loss_1 - loss_2
        mean_diff = np.nanmean(differential)
        se_diff = np.nanstd(differential) / np.count_nonzero(~np.isnan(differential))
        statistic = mean_diff / se_diff
    else:
        raise NotImplementedError()

    return statistic


def calc_spanning_alpha():
    """Calculates spanning alpha for LS returns for various factor models."""

    # Load returns
    df_ret = pd.read_csv(f'{PATH_SIMULATION}future return/trading_returns.csv')
    df_vol = pd.read_csv(f'{PATH_SIMULATION}volatility/trading_returns.csv')
    df_ret_notext = pd.read_csv(f'{PATH_SIMULATION}future return/notext/trading_returns.csv')
    df_vol_notext = pd.read_csv(f'{PATH_SIMULATION}volatility/notext/trading_returns.csv')
    df_ff = pd.read_csv(FF_FACTORS_FILENAME).iloc[176:214].reset_index(drop=True)

    # Convert mom to quarterly
    df_mom = pd.read_csv(FF_MOM_FILENAME)
    df_mom.columns = ["Date", 'MOM']
    df_mom['Date'] = pd.to_datetime(df_mom['Date'], format="%Y%m")
    df_mom['Date'] = df_mom['Date'].dt.to_period('Q')
    df_mom = df_mom.groupby('Date')['MOM'].apply(lambda x: np.prod(x/100 + 1) -1 ) * 100
    df_mom = df_mom.iloc[322: 360]
    df_mom = df_mom.reset_index()

    # Concat data
    strategies = ['LetterLSRet', 'SummaryLSRet', 'ControlLSRet', 'LetterLSVol', 'SummaryLSVol', 'ControlLSVol']
    factors = ['Mkt-RF', 'SMB', 'HML', 'RMW', 'CMA', "MOM"]
    cols = factors + strategies
    df = pd.concat([df_ret, df_vol, df_ret_notext, df_vol_notext, df_ff, df_mom], axis=1)
    df = df[cols]

    # Spanning regressions and write output to excel
    for i, strategy in enumerate(strategies):
        X = df[factors]
        X = sm2.add_constant(X)
        Y = df[strategy]
        maxlags = int(np.floor(np.power(len(Y), 1 / 4)))  # Max lags for Newey-West SEs

        # 1) CAPM alpha
        X_cols = ['const', 'Mkt-RF']
        mod = sm2.OLS(Y, X[X_cols], missing='drop').fit(cov_type='HAC', cov_kwds={'maxlags': maxlags})
        capm = pd.concat([mod.params, mod.pvalues], axis=1).set_axis(labels=['coef', 'p-value'], axis=1)

        # 2) FF3 alpha
        X_cols += ['SMB', 'HML']
        mod = sm2.OLS(Y, X[X_cols], missing='drop').fit(cov_type='HAC', cov_kwds={'maxlags': maxlags})
        ff3 = pd.concat([mod.params, mod.pvalues], axis=1).set_axis(labels=['coef', 'p-value'], axis=1)

        # 3) CH4 alpha
        X_cols += ['MOM']
        mod = sm2.OLS(Y, X[X_cols], missing='drop').fit(cov_type='HAC', cov_kwds={'maxlags': maxlags})
        ch4 = pd.concat([mod.params, mod.pvalues], axis=1).set_axis(labels=['coef', 'p-value'], axis=1)

        df_reg = pd.concat([capm, ff3, ch4], axis=1)
        df_reg.loc['const', 'coef'] *= 4  # Annualize alpha

        filename = PATH_SIMULATION + "trading_spanning_alpha.xlsx"
        if i == 0:
            df_reg.to_excel(filename, sheet_name=strategy)
        else:
            with pd.ExcelWriter(filename, mode='a') as writer:
                df_reg.to_excel(writer, sheet_name=strategy)


def portfolio_sort(quantiles=5, monthly=False, min_obs=200):
    """Conduct univariate portfolio sorts based on fund characteristics."""
    input_name = RESULTS_COMPLETE_FILENAME
    if monthly:
        input_name = input_name.replace(".csv", "_monthly.csv")
    df_results = pd.read_csv(input_name)
    df_results['Date'] = pd.to_datetime(df_results['Date'], format='%Y/%m/%d')

    if SUMMARIZATION_SCORES:
        filename = PATH_SIMULATION + "portfolio_sort_summary.xlsx"
        scores = ['Negativity', 'Confidence', 'Similarity']
    else:
        filename = PATH_SIMULATION + "portfolio_sort.xlsx"
        scores = ['Negativity', 'Complexity', 'Confidence', 'Personalness', 'Similarity']
    if monthly:
        filename = filename.replace(".xlsx", "_monthly.xlsx")
    scores += [f"{score} Surprise" for score in scores[:-1]]
    control_vars = ['LN(Fund Size)', 'LN(Fund Age)', 'Expense Ratio', 'Management Fee', 'Turnover Ratio',
                    'LN(No. funds)', 'Lagged Return', 'Lagged Fund Flow', 'Lagged Volatility', "number-words"]
    cols = scores + control_vars
    cols = [c for c in cols if c in list(df_results)]

    # Only keep latest letter per date
    df_results = df_results.sort_values(by=["FundNo", "Date", "file-date"])
    df_results = df_results.drop_duplicates(subset=["FundNo", "Date"], keep="last")
    funds = df_results['FundNo'].unique()
    dates = df_results["Date"].unique()
    min_date, max_date = np.min(dates), np.max(dates)

    # Load returns
    ret_filename = MERGED_DATA_FILENAME
    if monthly:
        ret_filename = ret_filename.replace(".csv", "_monthly.csv")
        usecols = ['FundNo', 'Date', 'mret']
    else:
        usecols = ['FundNo', 'Date', 'Return']
    df_rets = pd.read_csv(ret_filename, usecols=usecols).rename(columns={"mret": "Return"}).drop_duplicates()
    df_rets['Date'] = pd.to_datetime(df_rets['Date'], infer_datetime_format=True)
    df_rets = df_rets.sort_values(by=["FundNo", "Date"])

    # End return dates one period later
    dates_rets = df_rets['Date'].unique()
    dates_rets.sort()
    last_date = dates_rets[np.argmax(dates_rets > max_date)]
    dates_rets = dates_rets[(dates_rets >= min_date) & (dates_rets <= last_date)]

    # Pivot and select relevant funds + dates
    rets = df_rets.pivot(index="FundNo", columns="Date", values="Return")
    rets = rets.loc[funds, dates_rets]  # Select funds
    np_rets = rets.to_numpy()[:, None, :]
    if not monthly:
        np_rets -= 1
    np_rets *= 100

    multi_cols = pd.MultiIndex.from_product([cols, range(quantiles + 1)], names=["feature", "portfolio"])
    df_port_ret = pd.DataFrame(index=dates_rets[1:], columns=multi_cols)
    for col in cols:
        print("Running", col)
        sel = ["Date", "FundNo", col]
        df_sel = df_results[sel].copy().dropna(subset=[col])
        min_coverage = df_sel.groupby('Date')[col].count() > min_obs
        subsample = min_coverage.loc[min_coverage].index
        df_sel = df_sel.loc[df_sel["Date"].isin(subsample)]
        df_sel['rank'] = df_sel.groupby("Date")[col].transform(lambda x: rankdata(x, method="ordinal"))  # create unique ranks
        df_sel['quantile'] = df_sel.groupby("Date")["rank"].transform(lambda x: pd.qcut(x, quantiles, labels=False, duplicates="drop"))

        # Create numpy array with dimensions (fund x quantile x date)
        portfolios = df_sel.pivot(index="FundNo", columns="Date", values="quantile")
        portfolios = portfolios.reindex_like(rets)  # Align shape with returns
        portfolios = portfolios.shift(1, axis=1)  # Shift portfolios forward
        portfolios = np.stack([portfolios] * quantiles, axis=1)  # Create new dimension for every quantile

        # Calculate equal weights by first creating a boolean array
        weights = np.zeros(portfolios.shape)
        for port in range(quantiles):
            weights[:, port] = np.where(portfolios[:, port] == port, 1.0, np.nan)
        weights = weights / bn.nansum(weights, axis=0)[None, :, :]

        # Calculate returns
        port_ret = bn.nansum(weights * np_rets, axis=0)
        sum_port = bn.nansum(weights * np.where(np.isnan(np_rets), 0, 1), axis=0)  # Rescale portfolio to account for missing returns
        sum_port = bn.replace(sum_port, 0, np.nan)
        port_ret = port_ret[:, 1:] / sum_port[:, 1:]  # Skip first date

        # Add return spread between top and bottom portfolios
        ret_spread = port_ret[-1] - port_ret[0]
        port_ret = np.concatenate([port_ret, ret_spread[None, :]], axis=0)

        # Append
        df_port_ret[col] = port_ret.T

    # Calculate summary statistics
    stats = df_port_ret.apply(lambda x: calc_nw_tstat(x), axis=0)
    stats.index = ["mean", "tstat"]

    # Write output to Excel
    df_port_ret.to_excel(filename, sheet_name="returns")
    with pd.ExcelWriter(filename, mode='a') as writer:
        stats.to_excel(writer, sheet_name="stats")


def fama_macbeth(monthly=False, min_obs=50):
    """Conduct 1-step ahead Fama Macbeth regressions based on fund characteristics."""
    input_name = RESULTS_COMPLETE_FILENAME
    if monthly:
        input_name = input_name.replace(".csv", "_monthly.csv")
    df_results = pd.read_csv(input_name)
    df_results['Date'] = pd.to_datetime(df_results['Date'], format='%Y/%m/%d')

    if SUMMARIZATION_SCORES:
        filename = PATH_SIMULATION + "fama_macbeth_summary.xlsx"
        scores = ['Negativity', 'Confidence', 'Similarity']
    else:
        filename = PATH_SIMULATION + "fama_macbeth.xlsx"
        scores = ['Negativity', 'Complexity', 'Confidence', 'Personalness', 'Similarity']
    if monthly:
        filename = filename.replace(".xlsx", "_monthly.xlsx")
    scores += [f"{score} Surprise" for score in scores[:-1]]
    control_vars = ['LN(Fund Size)', 'LN(Fund Age)', 'Expense Ratio', 'Management Fee', 'Turnover Ratio',
                    'LN(No. funds)', 'Lagged Return', 'Lagged Fund Flow', 'Lagged Volatility', "number-words"]
    cols = scores + control_vars
    cols = [c for c in cols if c in list(df_results)]

    # Only keep latest letter per quarter
    df_results = df_results.sort_values(by=["FundNo", "Date", "file-date"])
    df_results = df_results.drop_duplicates(subset=["FundNo", "Date"], keep="last")

    # Dates mapping
    dates = df_results["Date"].unique()
    dates.sort()
    df_dates = pd.DataFrame(dates, columns=["Date"])
    df_dates["lagged_date"] = df_dates["Date"].shift(1)

    # Load returns
    usecols = ['FundNo', 'Date', 'Return']
    ret_filename = MERGED_DATA_FILENAME
    if monthly:
        ret_filename = ret_filename.replace(".csv", "_monthly.csv")
        usecols = ['FundNo', 'Date', 'mret']
    df_rets = pd.read_csv(ret_filename, usecols=usecols).drop_duplicates().rename(columns={"mret": "Return"})

    # Lag dates because we want to regress t+1 returns on t variables
    df_rets['Date'] = pd.to_datetime(df_rets['Date'], infer_datetime_format=True)
    df_rets = df_rets.merge(df_dates, on=["Date"])
    df_rets = df_rets.drop(columns=["Date"]).dropna(axis=0).rename(columns={"lagged_date": "Date"})
    if not monthly:
        df_rets["Return"] -= 1
    df_rets["Return"] *= 100

    # Merge with features
    df = df_results.drop(columns=["Return"]).merge(df_rets, on=["FundNo", "Date"])
    df_out = pd.DataFrame(index=cols, columns=['mean', "t_stat", "rsquared"])
    for col in cols:
        print("Running", col)
        sel = ["Date", "FundNo", "Return", col]
        df_sel = df[sel].copy().dropna(subset=[col, "Return"])
        min_coverage = df_sel.groupby('Date')[col].count() > min_obs
        subsample = min_coverage.loc[min_coverage].index
        df_sel = df_sel.loc[df_sel["Date"].isin(subsample)]
        df_sel["z_score"] = df_sel.groupby('Date')[col].apply(lambda x: (x - x.mean()) / x.std())
        df_sel["z_score"] = np.clip(df_sel["z_score"], -3, 3)  # Winsorize data at 3 standard deviations

        def _xs_regression(df):
            df = sm2.add_constant(df).dropna()
            mod = sm2.OLS(endog=df["Return"], exog=df[["const", "z_score"]]).fit()
            out = mod.params
            out.at["rsquared"] = mod.rsquared
            return out

        # Run FM regressions
        df_reg = df_sel.groupby("Date").apply(lambda x: _xs_regression(x))  # Step 1
        fm_values = calc_nw_tstat(df_reg["z_score"]).set_axis(["mean", "t_stat"])  # Step 2
        fm_values.at["rsquared"] = df_reg["rsquared"].mean()
        df_out.loc[col] = fm_values

    # Write output to Excel
    df_out.to_excel(filename, sheet_name="returns")


def calc_nw_tstat(series):
    """OLS regression to estimate sample mean and Newey-West t-statistic."""
    df = sm2.add_constant(series.to_frame("y")).dropna()
    lags = int(np.floor(len(series) ** 0.25))
    mod = sm2.OLS(endog=df["y"], exog=df["const"]).fit(cov_type='HAC', cov_kwds={'maxlags': lags})
    mean = mod.params
    tstat = mod.tvalues
    return pd.concat([mean, tstat])


if __name__ == '__main__':
    # produce_fcst()
    # calc_spanning_alpha()
    portfolio_sort(monthly=False)
    fama_macbeth(monthly=False)