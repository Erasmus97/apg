%% Description
% This script plots all of the figures reported in the paper.
% It relies on results generated from various Python scripts. 

%% Settings
clear; clc;
format shortG

% Configurations
tf_idf = true;
crisis_period = false;
single_fund = false;
summarization = false;

plot_returns = true;
transformed_scatter = true;
standardized = false;
plot_surprise_measures = false;
drop_duplicates = true;

% Graph options
linewidth = 1.5;
linewidth_thick = 2.5;
markersize = 15;
dotsize = 100;
dotsize_small = 50;
markersize_small = 2;
alpha = 0.1;

fontsize = 24;
title_font = 24;
axis_font = 20;
bins = 40;
ub = 1.05;
yfmt = '%.1f';
yfmt_short = '%.0f';

% Colors
blue = [0, 114, 189]/255;
red = [196 78 82]/255;
green = [85 168 104]/255;
purple = [124 114 171]/255;
cyan = [119 180 202]/255;
yellow = [202 185 125]/255;

%% Paths and directories
parent_path = '../../';
trading_path = strcat(parent_path, 'results/trading/');
french_path = strcat(parent_path, 'data/French/');
dir = strcat(parent_path, '/code/Matlab');
cd(dir);

if tf_idf == 1
    parent_path = strcat(parent_path, 'results/TF_IDF/');
else
    parent_path = strcat(parent_path, 'results/proportional/');
end

if summarization == 1
    parent_path = strcat(parent_path, 'summarization/');
end

if crisis_period == 1
    results_path = strcat(parent_path, 'crisis_subsample/');
elseif single_fund == 1
    results_path = strcat(parent_path, 'single_funds/');
else
    results_path = strcat(parent_path, 'baseline/');
end

copula_path = strcat(results_path, 'Copula/');
save_path = strcat(results_path, 'Figures/');
save_path_timeseries = strcat(save_path, 'Timeseries/');
save_path_histograms = strcat(save_path, 'Histograms/');
save_path_scatters = strcat(save_path, 'Scatters/');
save_path_copula = strcat(save_path, 'Copula/');

% Filenames
ff_filename = strcat(french_path, 'F-F_Q.csv');
timeseries_filename = strcat(save_path, 'timeseries.xlsx');
results_filename = strcat(results_path, 'results_winsorized.csv');
results_transformed_filename = strcat(results_path, 'measures_transformed.csv');
results_complete_filename = strcat(results_path, 'results_complete.csv');
copula_over_time_filename = strcat(copula_path, 'copula_over_time.xlsx');
trading_returns_filename = strcat(trading_path, 'future return/trading_returns.csv');
trading_volatility_filename = strcat(trading_path, 'volatility/trading_returns.csv');
trading_returns_copula_filename = strcat(trading_path, 'future return/copula/trading_returns.csv');
trading_volatility_copula_filename = strcat(trading_path, 'volatility/copula/trading_returns.csv');
trading_returns_notext_filename = strcat(trading_path, 'future return/notext/trading_returns.csv');
trading_volatility_notext_filename = strcat(trading_path, 'volatility/notext/trading_returns.csv');

%% Names and indices
if summarization == 1
    text_measure_names = {'Negativity', 'Confidence', 'Similarity'};
    surprise_measure_names = {'Negativity Surp', 'Confidence Surp.'};
    text_measures_indices = [12, 13, 14];
    surprise_measure_indices = [24, 25]; 
    perf_measure_indices = [23, 1, 16, 15];
else
    text_measure_names = {'Negativity', 'Confidence', 'Personalness', 'Complexity', 'Similarity'};
    surprise_measure_names = {'Negativity Surp', 'Confidence Surp', 'Personalness Surp',  'Complexity Surp'};
    text_measures_indices = [13, 14, 12, 15, 16];
    surprise_measure_indices = [26, 27, 28, 29]; 
    perf_measure_indices = [24, 1, 18, 17];
end

perf_measure_names = {'Return', 'Fund Flow', 'Alpha', 'Volatility'};
strategy_names2 = {'Pos Ret Letter', 'Pos Ret Summary', 'Low Vol Letter', 'Low Vol Summary', '1$/N$', 'MSCI World Index'};
strategy_names_notext = {'Letter', 'Summary', 'Control', '1$/N$'};
strategy_names = {'Pos Ret Reg. Sum.', 'Pos Ret Cop. Sum.', 'Low Vol Reg. Sum.', 'Low Vol Cop. Let.', '1/$N$', 'MSCI World Index'};
strategy_indices = [1, 3, 2, 4];
vol_indices = [1, 3];

nMeasures = length(text_measure_names);
nPerf = length(perf_measure_names);

xtick_values = [2006, 2008, 2010, 2012, 2014, 2016];
xtick_copula = [2006, 2009, 2012, 2015];
xtick_values_similarity = [2006, 2008, 2010, 2012, 2014, 2017];
xtick_copula = datetime(string(xtick_copula), 'Format', 'yyyy');
xtick_values = datetime(string(xtick_values), 'Format', 'yyyy');
xtick_values_similarity = datetime(string(xtick_values_similarity), 'Format', 'yyyy');

%% Histograms
% Read data and select relevant columns
results_table = readtable(results_complete_filename);
results_table = table2array(results_table(:, 32:end));

text_measures_indices = [1:5];
perf_measure_indices = [13, 16, 7, 6];
surprise_measure_indices = [17, 19, 18, 20];

agg_indices = [perf_measure_indices, text_measures_indices, surprise_measure_indices];
names = [perf_measure_names, text_measure_names, surprise_measure_names];

nCols = [length(perf_measure_indices), length(text_measures_indices), length(surprise_measure_indices)];
rowNames = {'perf', 'text_abs', 'text_rel'};
counter = 1;
widths = [4, 5, 4];

results_table = results_table(:, agg_indices);

for i = 1:length(nCols)
    cols = nCols(i);
    for j = 1:cols
        subplot(1,cols,j);  
        % Select data and filter outliers
        data = results_table(:, counter);
        mask = data > quantile(data, 0.005) & data < quantile(data, 0.995);
        data = data(mask);

        % Make histogram
        [nn,x]= hist(data, bins);
        ytickformat(yfmt)
        delta_x = x(2)-x(1);
        bar(x,nn./sum(nn)/delta_x,1); % <- percentage cum dist

        % Figure settings
        findobj(gca,'Type','patch');
        set(gca, 'ylim', ylim*ub);
        set(gca,'FontSize', axis_font)
        set(gca, 'TickLabelInterpreter', 'latex')
        title(names(counter), 'interpreter', 'latex', 'FontSize', title_font)
        counter = counter + 1;
    end
    fig = gcf;
    pos_fig = [0 1000 1600/(5/widths(i)) 250]; % Sets size of figure equal to top half of screen
    set(fig,'Position',pos_fig)
    filename = char(strcat('hist_', rowNames(i)));
    saveas(gca, fullfile(save_path, filename), 'epsc');
end

%% Scatter plots
if transformed_scatter == 1
    results_table = readtable(results_transformed_filename, 'PreserveVariableNames', true);
    if summarization == 1
        results_table = results_table(:, 8:end);
        text_measures_indices = 5:7;
        perf_measure_indices = 1:4;
    else
        results_table = results_table(:, 10:end);
        text_measures_indices = 5:9;
        perf_measure_indices = 1:4;
    end
else
    results_table = readtable(results_complete_filename, 'PreserveVariableNames', true);
    results_table = results_table(:, 23:end);
end

% Specify relevant indices of variables
perf_measures = table2array(results_table(:, perf_measure_indices));

if plot_surprise_measures == 1 && transformed_scatter == 0
    nMeasures = length(surprise_measure_indices);
    text_measures = table2array(results_table(:, surprise_measure_indices));
    % Remove NaNs from surprise measures
    mask = ~-isnan(text_measures(:, 1));
    text_measures = text_measures(mask, :);
    perf_measures = perf_measures(mask, :);
else
    text_measures = table2array(results_table(:, text_measures_indices));
end

% Loop over performance measures
for i = 1:nPerf
    % Loop over textual measures
    for j = 1:nMeasures
        % Drop NaNs
        perf_measure = perf_measures(:, i);
        text_measure = text_measures(:, j);
        
        if standardized == 1
            mask = ~-isnan(perf_measure);
            perf_measure = perf_measure(mask);
            text_measure = text_measure(mask);
            
            perf_measure = (perf_measure - mean(perf_measure)) / std(perf_measure);
        elseif drop_duplicates == 1
            matrix = unique([perf_measure, text_measure], 'rows');
            perf_measure = matrix(:, 1);
            text_measure = matrix(:, 2);
        end
        
        % Plot
        subplot(1, nMeasures, j)
        if transformed_scatter == 1
            ytickformat(yfmt)
            scatter(text_measure, perf_measure, dotsize_small, '.', 'MarkerEdgeAlpha', alpha);

        else
            ytickformat(yfmt_short)
            scatter(text_measure, perf_measure, dotsize, '.', 'MarkerEdgeAlpha', alpha);
        end
        set(gca, 'ZColor', blue);
        set(gca,'FontSize', axis_font)
        set(gca, 'TickLabelInterpreter', 'latex')
        title(text_measure_names(j), 'interpreter', 'latex', 'FontSize', title_font)
        set(findall(gcf, 'Type', 'Text'),'FontWeight', 'Normal')
    end
    fig = gcf;
    pos_fig = [0 1000 1600 250]; % Sets size of figure equal to top half of screen
    set(fig,'Position',pos_fig)
    
    % Specify filename and save
    filename = char(perf_measure_names(i));
    if transformed_scatter == 1
        filename = strcat('Transformed/', filename, '_transformed');
    elseif standardized == 1
        filename = strcat('Standardized/', filename, '_standardized');
    elseif plot_surprise_measures == 1
        filename = strcat('Surprise/', filename, '_surprise');
    end
    % Remove spaces
    filename = strrep(filename, ' ', '_');
    saveas(gca, fullfile(save_path_scatters, filename), 'epsc');
end

%% Copula plots
perf_measure_names = {'Future Return', 'Future Fund Flow', 'Alpha', 'Volatility'};
for i=1:nPerf
    %     perf_measure_names(i)
    for j = 1:nMeasures
        % Read data
        filename = char(strcat(copula_path, perf_measure_names(i), '.xlsx'));
        data = readmatrix(filename, 'Sheet', j);
        
        % Select data
        x = data(:, 1);
        y = data(:, 2);
        y_pred = data(:, 3);    
        y_lower = data(:, 5);
        y_upper = data(:, 6);
        
        % Plot scatter
        sub = subplot(1, nMeasures, j);
        hold on
        h1 = scatter(x, y, dotsize, '.', 'MarkerEdgeAlpha', alpha);
        set(h1, 'CData', blue);
        h2 = scatter(x, y_pred, dotsize, '.');
        set(h2, 'CData', green);
        
        % Fit polynomials through CI
        poly = 'poly4';
        lower = fit(x, y_lower, poly);
        upper = fit(x, y_upper, poly);
        h3 = plot(lower, '--');
        h4 = plot(upper, '--');
        set(h3, 'color', red, 'LineWidth', linewidth_thick);
        set(h4, 'color', red, 'LineWidth', linewidth_thick);
        legend('off')
        hold off
        
        % Set ylim relative to upper and lower quantiles
        if i == 3
            ylim = [min(y_lower), max(y_upper)*1.1];
        else
            ylim = [min(y_lower), max(y_upper)*1.1];
        end
        
        % Settings
        set(gca, 'ylim', ylim);
        set(gca, 'FontSize', fontsize)
        set(gca,'FontSize', axis_font)
        set(gca, 'TickLabelInterpreter', 'latex')
        set(get(sub, 'XLabel'), 'String', {})
        set(get(sub, 'YLabel'), 'String', {})
        title(text_measure_names(j), 'interpreter', 'latex', 'FontSize', title_font)
    end
    fig = gcf;
    pos_fig = [0 1000 1600/(5/nMeasures) 250]; % Sets size of figure equal to top half of screen
    set(fig,'Position',pos_fig)
    filename = char(perf_measure_names(i));
    filename = strrep(filename, ' ', '_');
    saveas(gca, fullfile(save_path_copula, filename), 'epsc');
    delete(gcf)
end

%% Timeseries plots
timeseries_table = readtable(timeseries_filename, 'Sheet', 1, 'PreserveVariableNames', true);
dates = table2array(timeseries_table(:, 1));
returns = table2array(timeseries_table(:, 3));
scores = table2array(timeseries_table(:, 4:end));

% Specify relevant indices of variables
start_indices = [1, 9, 17, 25];
xrange = [xtick_values_similarity(1), xtick_values_similarity(end)];

for i = 1:nMeasures-1
    % Plot U(0,1) first
    set(0,'DefaultLineLineWidth',linewidth)
    
    if plot_returns == 1
        fig1 = plot(dates, scores(:, start_indices(i)+2), dates, scores(:, start_indices(i)+1), ':', ...
            dates, returns, '--', dates, scores(:, start_indices(i)+3), ':');    
    else
        fig1 = plot(dates, scores(:, start_indices(i)), dates, scores(:, start_indices(i)+1), ':', ...
            dates, scores(:, start_indices(i)+2), '--', dates, scores(:, start_indices(i)+3), ':');
    end
    
    % Figure settings
    set(fig1(1), 'color', blue, 'LineWidth', linewidth_thick);
    set(fig1(3), 'color', green, 'LineWidth', linewidth_thick);
    set(fig1([2,4]), 'color', red);
    set(gca, 'xlim', xrange);
    xticks(xtick_values);
    set(gca, 'TickLabelInterpreter', 'latex')
    set(gca,'FontSize', axis_font)
    title(strcat(text_measure_names(i)), 'interpreter', 'latex', 'FontSize', title_font)
    
    % Save
    filename1 = char(strcat(text_measure_names(i), '_transformed'));
    filename1 = strrep(filename1, ' ', '_');
    saveas(gca, fullfile(save_path_timeseries, filename1), 'epsc');
    
    % Plot raw measure
    set(0,'DefaultLineLineWidth',1)
    fig2 = plot(dates, scores(:, start_indices(i)+4), dates, scores(:, start_indices(i)+5), ':', ...
        dates, scores(:, start_indices(i)+6), '--', dates, scores(:, start_indices(i)+7), ':');
    
    % Settings
    set(fig2(1), 'color', blue, 'LineWidth', linewidth_thick);
    set(fig2(3), 'color', green, 'LineWidth', linewidth_thick);
    set(fig2([2,4]), 'color', red);
    set(gca, 'xlim', xrange);
    xticks(xtick_values);
    set(gca, 'TickLabelInterpreter', 'latex')
    set(gca,'FontSize', axis_font)
    title(text_measure_names(i), 'interpreter', 'latex', 'FontSize', title_font)
    
    % Save
    filename2 = char(text_measure_names(i));
    filename2 = strrep(filename2, ' ', '_');
    saveas(gca, fullfile(save_path_timeseries, filename2), 'epsc');
end

% Repeat above for similarity
similarity_table = readtable(timeseries_filename, 'Sheet', 2, 'PreserveVariableNames', true);
dates = table2array(similarity_table(2:end, 1));
returns = table2array(similarity_table(2:end, 3));
scores = table2array(similarity_table(2:end, 4:end));

i=nMeasures;
% Plot U(0,1) first
set(0,'DefaultLineLineWidth',linewidth)
if plot_returns == 1
fig1 = plot(dates, scores(:, 3), dates, scores(:, 2), ':', ...
    dates, returns, '--', dates, scores(:, 4), ':');
else
    fig1 = plot(dates, scores(:, 1), dates, scores(:, 2), ':', ...
    dates, scores(:, 3), '--', dates, scores(:, 4), ':');
end
    
% Settings
set(fig1(1), 'color', blue, 'LineWidth', linewidth_thick);
set(fig1(3), 'color', green, 'LineWidth', linewidth_thick);
set(fig1([2,4]), 'color', red);
set(gca, 'TickLabelInterpreter', 'latex')
set(gca,'FontSize', axis_font)
set(gca, 'xlim', xrange);
xticks(xtick_values);
title(strcat(text_measure_names(i)), 'interpreter', 'latex', 'FontSize', title_font)

% Save
filename1 = char(strcat(text_measure_names(i), '_transformed'));
filename1 = strrep(filename1, ' ', '_');
saveas(gca, fullfile(save_path_timeseries, filename1), 'epsc');

% Plot raw measure
set(0,'DefaultLineLineWidth',1)
fig2 = plot(dates, scores(:, 5), dates, scores(:, 6), ':', ...
    dates, scores(:, 7), '--', dates, scores(:, 8), ':');

% Settings
set(fig2(1), 'color', blue, 'LineWidth', linewidth_thick);
set(fig2(3), 'color', green, 'LineWidth', linewidth_thick);
set(fig2([2,4]), 'color', red);
set(gca, 'TickLabelInterpreter', 'latex')
set(gca,'FontSize', axis_font)
set(gca, 'xlim', xrange);
xticks(xtick_values);
title(text_measure_names(i), 'interpreter', 'latex', 'FontSize', title_font)

% Save
filename2 = char(text_measure_names(i));
filename2 = strrep(filename2, ' ', '_');
saveas(gca, fullfile(save_path_timeseries, filename2), 'epsc');

%% Copula over time
if plot_surprise_measures == 1
    text_measures_complete = [text_measure_names, surprise_measure_names];
else
    text_measures_complete = text_measure_names;
end

len = length(text_measures_complete);
xrange = [xtick_values_similarity(1), xtick_values_similarity(end)];

for i=1:nPerf
    timeseries_table = readtable(copula_over_time_filename, 'Sheet', i, 'PreserveVariableNames', true);
    dates = table2array(timeseries_table(:, 1));
    data = table2array(timeseries_table(:, 2:end));
    
    for j=1:len
        
        sub = subplot(1, len, j);
        set(0,'DefaultLineLineWidth',linewidth)
        fig = plot(dates, data(:, j));
        
        % Settings
        set(fig(1), 'color', blue);
        set(gca, 'TickLabelInterpreter', 'latex')
        set(gca,'FontSize', axis_font)
        set(gca, 'xlim', xrange);
        xticks(xtick_copula);
        title(text_measures_complete(j), 'interpreter', 'latex', 'FontSize', title_font)
    end
    fig = gcf;
    pos_fig = [0 1000 1600/(5/len) 250]; % Sets size of figure equal to top half of screen
    set(fig,'Position',pos_fig)
    filename = char(perf_measure_names(i));
    filename = strrep(filename, ' ', '_');
    save_path = strcat(save_path_copula, 'Over Time/');
    saveas(gca, fullfile(save_path, filename), 'epsc');
end

%% Trading results
% Regressions
returns1 = readmatrix(trading_returns_filename);
returns1 = returns1(:, 3:end);
returns1 = returns1(:, strategy_indices);

returns2 = readmatrix(trading_volatility_filename);
returns2 = returns2(:, 3:end);
returns2 = returns2(:, vol_indices);

% Copula
returns3 = readmatrix(trading_returns_copula_filename);
returns3 = returns3(:, 3:end);
returns3 = returns3(:, strategy_indices);

returns4 = readmatrix(trading_volatility_copula_filename);
returns4 = returns4(:, 3:end);
returns4 = returns4(:, vol_indices);

returns_reg = [returns1(:, 1), returns1(:, 2), returns2(:, 1), returns2(:, 2), returns1(:, 3:end)];
returns_cop = [returns3(:, 1), returns3(:, 2), returns4(:, 1), returns4(:, 2), returns3(:, 3:end)];
returns_best = [returns1(:, 2), returns3(:, 2), returns2(:, 2), returns4(:, 1), returns1(:, 3:end)];

for i=2:3
    if i == 1
        returns = returns_best;
        str = '';
        strats = strategy_names;
    elseif i == 2
        returns = returns_reg;
        str = '_reg';
        strats = strategy_names2;
    elseif i == 3
        returns = returns_cop;
        str = '_cop';
        strats = strategy_names2;
    end
% Compute cumulative returns
returns_cum = returns/100 + 1;
returns_cum = (cumprod(returns_cum) - 1)*100;

ytick_cum = [-50, -25, 0, 25, 50, 75, 100];
ytick_ret = [-30, -20, -10, 0, 10, 20, 30]; 

start_date = datetime(2007,6,1);
end_date = datetime(2016,12,1);
dates = linspace(start_date, end_date, 39);

set(0,'DefaultLineLineWidth',linewidth_thick)
fig = plot(dates, returns(:, 1), '-', dates, returns(:, 2), '-',...
    dates, returns(:, 3), '-.', dates, returns(:, 4), '-.', ...
    dates, returns(:, 5), '--', dates, returns(:, 6), ':');
    
% Settings
set(fig(1), 'color', blue);
set(fig(2), 'color', cyan),
set(fig(3), 'color', red);
set(fig(4), 'color', yellow);
set(fig(5), 'color', green);
set(fig(6), 'color', purple);
set(gca, 'TickLabelInterpreter', 'latex')
set(gca,'FontSize', axis_font)
yticks(ytick_ret);
ylim([-25 25])
xticks(xtick_values);
if i==2
legend(fig(1:3), strats(1:3), ...
    'Interpreter', 'latex', 'Location', 'SouthOutside', 'NumColumns', 3, 'FontSize', axis_font-4);
legend('boxoff')
end
if i==3
legend(fig(4:6), strats(4:6), ...
    'Interpreter', 'latex', 'Location', 'SouthOutside', 'NumColumns', 3,  'FontSize', axis_font-4);
legend('boxoff')
end
fig = gcf;
pos_fig = [0 1000 1600/3 380]; % Sets size of figure equal to top half of screen
set(fig,'Position',pos_fig)

% Save
filename = strcat('trading_simulation_returns', str);
saveas(gca, fullfile(trading_path, filename), 'epsc');

set(0,'DefaultLineLineWidth',linewidth_thick)
fig = plot(dates, returns_cum(:, 1), '-', dates, returns_cum(:, 2), '-',...
    dates, returns_cum(:, 3), '-.', dates, returns_cum(:, 4), '-.', ...
    dates, returns_cum(:, 5), '--', dates, returns_cum(:, 6), ':');
    
% Settings
set(fig(1), 'color', blue);
set(fig(2), 'color', cyan),
set(fig(3), 'color', red);
set(fig(4), 'color', yellow);
set(fig(5), 'color', green);
set(fig(6), 'color', purple);
set(gca, 'TickLabelInterpreter', 'latex')
set(gca,'FontSize', axis_font)
xticks(xtick_values);
yticks(ytick_cum);
ylim([-50 80])
if i==2
legend(fig(1:3), strats(1:3), ...
    'Interpreter', 'latex', 'Location', 'SouthOutside', 'NumColumns', 3, 'FontSize', axis_font-4);
legend('boxoff')
end
if i==3
legend(fig(4:6), strats(4:6), ...
    'Interpreter', 'latex', 'Location', 'SouthOutside', 'NumColumns', 3,  'FontSize', axis_font-4);
legend('boxoff')
end

fig = gcf;
pos_fig = [0 1000 1600/3 380]; % Sets size of figure equal to top half of screen
set(fig,'Position',pos_fig)

% Save
filename = strcat('trading_simulation_returns_cumulative', str);
saveas(gca, fullfile(trading_path, filename), 'epsc');
end
%% Trading results NEW CONTROL
% FF
returns_ff = readtable(ff_filename, VariableNamingRule='preserve');
rf = returns_ff.RF(177:214);

% Future return
returns1 = readtable(trading_returns_filename, VariableNamingRule='preserve');
returns1 = table2array(returns1(:, ["LetterRet_Q5", "SummaryRet_Q5", "1/N"]));

returns2 = readtable(trading_returns_notext_filename);
returns2 = returns2.ControlRet_Q5;

% Future volatility
returns3 = readtable(trading_volatility_filename, VariableNamingRule='preserve');
returns3 = table2array(returns3(:, ["LetterVol_Q5", "SummaryVol_Q5", "1/N"]));

returns4 = readtable(trading_volatility_notext_filename);
returns4 = returns4.ControlVol_Q5;

% Letter | Summary | Control |  1/N
ret = [returns1(:, 1:2), returns2, returns1(:, 3)];
vol = [returns3(:, 1:2), returns4, returns3(:, 3)];
ret_excess = ret - rf;
vol_excess = vol - rf;

% Rolling Sharpe ratio
ret_mov_mean = movmean(ret_excess, 12, 'Endpoints', 'discard');
ret_mov_std = movstd(ret, 12, 'Endpoints', 'discard');
ret_sr = ret_mov_mean./ret_std * sqrt(4);

vol_movmean = movmean(vol_excess, 12, 'Endpoints', 'discard');
vol_mov_std = movstd(vol, 12, 'Endpoints', 'discard');
vol_sr = vol_movmean./vol_mov_std * sqrt(4);

% Compute cumulative returns
ret_cum = ret/100 + 1;
ret_cum = (cumprod(ret_cum) - 1)*100;
vol_cum = vol/100 + 1;
vol_cum = (cumprod(vol_cum) - 1)*100;

ytick_cum = [-50, -25, 0, 25, 50, 75, 100];
ytick_ret = [-30, -20, -10, 0, 10, 20, 30]; 

start_date = datetime(2007,9,1);
end_date = datetime(2016,12,1);
dates = linspace(start_date, end_date, 38);
dates_mov = dates(12:end);

% Plot future return
set(0,'DefaultLineLineWidth',linewidth_thick)
fig = plot(dates, ret_cum(:, 1), '-', dates, ret_cum(:, 2), ':',...
    dates, ret_cum(:, 3), '-.', dates, ret_cum(:, 4), '--');
    
% Settings
set(fig(1), 'color', red);
set(fig(2), 'color', blue),
set(fig(3), 'color', green);
set(fig(4), 'color', purple);
set(gca, 'TickLabelInterpreter', 'latex')
set(gca,'FontSize', axis_font)
xticks(xtick_values);
yticks(ytick_cum);
ylim([-50 100])
legend(fig, strategy_names_notext, ...
    'Interpreter', 'latex', 'Location', 'SouthOutside', 'NumColumns', 4, 'FontSize', axis_font-4);
legend('boxoff')

fig = gcf;
pos_fig = [0 1000 1600/3 380]; % Sets size of figure equal to top half of screen
set(fig,'Position',pos_fig)

% Save
filename = 'cumret_return';
saveas(gca, fullfile(trading_path, filename), 'epsc');

% Plot future volatility
set(0,'DefaultLineLineWidth',linewidth_thick)
fig = plot(dates, vol_cum(:, 1), '-', dates, vol_cum(:, 2), ':',...
    dates, vol_cum(:, 3), '-.', dates, vol_cum(:, 4), '--');
    
% Settings
set(fig(1), 'color', red);
set(fig(2), 'color', blue),
set(fig(3), 'color', green);
set(fig(4), 'color', purple);
set(gca, 'TickLabelInterpreter', 'latex')
set(gca,'FontSize', axis_font)
xticks(xtick_values);
yticks(ytick_cum);
ylim([-50 60])
legend(fig, strategy_names_notext, ...
    'Interpreter', 'latex', 'Location', 'SouthOutside', 'NumColumns', 4, 'FontSize', axis_font-4);
legend('boxoff')

fig = gcf;
pos_fig = [0 1000 1600/3 380]; % Sets size of figure equal to top half of screen
set(fig,'Position',pos_fig)

% Save
filename = 'cumret_vol';
saveas(gca, fullfile(trading_path, filename), 'epsc');

%% Calculate ensemble and levered returns
ret_std = std(ret);
ret_vol = std(vol);

lev_ratio_ret = ret_std(end) ./ ret_std;
lev_ratio_vol = ret_vol(end) ./ ret_vol;
ret_lev = ret .* lev_ratio_ret;
vol_lev = vol .* lev_ratio_vol;

comb = (ret + vol)/2;
sr_comb = mean(comb - rf) ./ std(comb) * 2;

% Compute cumulative returns
ret_lev_cum = ret_lev/100 + 1;
ret_lev_cum = (cumprod(ret_lev_cum) - 1)*100;
vol_lev_cum = vol_lev/100 + 1;
vol_lev_cum = (cumprod(vol_lev_cum) - 1)*100;
comb_cum = comb/100 + 1;
comb_cum = (cumprod(comb_cum) - 1)*100;

% Plot future return
fig = plot(dates, comb_cum(:, 1), '-', dates, comb_cum(:, 2), ':',...
    dates, comb_cum(:, 3), '-.', dates, comb_cum(:, 4), '--');
    
% Settings
set(fig(1), 'color', red);
set(fig(2), 'color', blue),
set(fig(3), 'color', green);
set(fig(4), 'color', purple);
set(gca, 'TickLabelInterpreter', 'latex')
set(gca,'FontSize', axis_font)
xticks(xtick_values);
yticks(ytick_cum);
ylim([-50 100])
legend(fig, strategy_names_notext, ...
    'Interpreter', 'latex', 'Location', 'SouthOutside', 'NumColumns', 4, 'FontSize', axis_font-4);
legend('boxoff')

fig = gcf;
pos_fig = [0 1000 1600/3 380]; % Sets size of figure equal to top half of screen
set(fig,'Position',pos_fig)

% Save
filename = 'cumret_comb';
saveas(gca, fullfile(trading_path, filename), 'epsc');