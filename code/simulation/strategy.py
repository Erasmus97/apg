import pandas as pd

class Strategy:

    def __init__(self, name, dependent_var='Future Return'):
        self.name = name
        self.__name__ = 'Strategy'
        self.portfolio = {}
        self.returns = {}
        self.turnover = {}
        self.mgt_fee = {}
        self.dependent_var = dependent_var

        # Plot attributes
        self.color = 'r'
        self.linestyle = 'solid'
        self.factor_vals = []
        self.perc_vals = []

    def update_weights(self, d_sub):
        """
        This method determines and updates the weights for the portfolio based on a strategy
        :param df_sub: subsample for a specific date
        :type df_sub: pandas.DataFrame

        Update strategy:
        * If mutual fund has not published in the rolling window --> hold
        * If factor returns is less than 1 --> sell
        * If factor returns is bigger than 1 --> buy 1/N
        """
        # Note: column `PredFactorReturns` contains NaN values when:
        #   * measures are NaN (such as similarity)
        #   * old data that is used to calculate predictions (rolling window data)
        pass


class BuyHoldSell(Strategy):

    def __init__(self, name):
        super().__init__(name)

        self.__name__ = 'BuyHoldSell'
        self.color = 'r'
        self.linestyle = 'solid'

    def update_weights(self, df_sub):
        df_sub.dropna(inplace=True)
        
        # Get investments on hold
        old_ids = [fundno for fundno, weight in self.portfolio.items() if weight > 0]
        old_ids = [fundno for fundno in old_ids if fundno not in list(df_sub['FundNo'].values)]

        # Set weights to 1/N if returns are positive or are on hold
        fund_nos = list(df_sub[df_sub['PredFactorReturns'] > 1]['FundNo'].values) + old_ids
        fund_nos = [fundno for fundno in fund_nos if fundno in self.portfolio.keys()]
        for fund_no in self.portfolio.keys():
            self.portfolio[fund_no] = 1 / len(fund_nos) if fund_no in fund_nos else 0


class BuyHoldSellVol(Strategy):

    def __init__(self, name, perc=0.2, color='orange'):
        """
        Represents the `buy, hold and sell` trading strategy, where the dependent variable is 'Future Volatility`
        :param name: name of the strategy that will be printed
        :type name: str
        :param perc: percentage of lowest volatilities in which will be invested
        :type perc: float
        :param color: color of line in plot
        :type color: str
        """
        super().__init__(name)

        self.__name__ = 'BuyHoldSellVol'
        self.color = color
        self.linestyle = 'dashed'
        self.perc = perc

    def update_weights(self, df_sub):
        df_sub.dropna(inplace=True)
        
        # Get investments on hold
        old_ids = [fundno for fundno, weight in self.portfolio.items() if weight > 0]
        old_ids = [fundno for fundno in old_ids if fundno not in list(df_sub['FundNo'].values)]

        # Rank funds on predicted returns and from small to large
        df_sub = df_sub.sort_values(by='PredVolatility', ascending=True)

        # Set weights to 1/N if returns are positive or are on hold
        fund_nos = list(df_sub['FundNo'].iloc[:int(len(df_sub)*self.perc)].values) + old_ids
        fund_nos = [fundno for fundno in fund_nos if fundno in self.portfolio.keys()]
        for fund_no in self.portfolio.keys():
            self.portfolio[fund_no] = 1 / len(fund_nos) if fund_no in fund_nos else 0


class OneOverN(Strategy):

    def __init__(self, name):
        super().__init__(name)

        self.__name__ = 'OneOverN'
        self.color = 'green'

    def update_weights(self, df_sub):
        # Create equally weighted portfolio
        # Note that we only have to do this once (when all values are equal to zero)
        for fund_no in self.portfolio.keys():
            self.portfolio[fund_no] = 1 / len(self.portfolio)
            
class Quintile(Strategy):            

    def __init__(self, name, quintile = 5, dependent_var = 'Future Return', color = 'blue'):
        super().__init__(name, dependent_var)
        
        self.__name__ = 'Quintile'
        self.color = color
        self.linestyle = 'dashed'
        self.quintile = quintile
    
    def update_weights(self, df_sub):
        df_sub.dropna(inplace=True)
        df_sub = df_sub[df_sub['FundNo'].isin(list(self.portfolio.keys()))]
    
        # Rank funds by return ascending or volatility descending
        if self.dependent_var == 'Future Return':
            df_sub['quintile'] = pd.qcut(df_sub['PredFactorReturns'], 5, labels = False)
        elif self.dependent_var == 'Volatility':
            df_sub['quintile'] = pd.qcut(-df_sub['PredVolatility'], 5, labels = False)
        else:
            raise KeyError('This dependent variable is invalid')
    
        # Find funds within quintile
        fund_nos = list(df_sub.loc[df_sub['quintile'] == self.quintile-1]['FundNo'])
        
        # Set weights to 1/N if fund in quintile
        for fund_no in self.portfolio.keys():
            self.portfolio[fund_no] = 1 / len(fund_nos) if fund_no in fund_nos else 0
            
class QuintileLongShort(Strategy):            

    def __init__(self, name, dependent_var = 'Future Return', color = 'blue'):
        super().__init__(name, dependent_var)
        
        self.__name__ = 'QuintileLongShort'
        self.color = color
        self.linestyle = 'dashed'
    
    def update_weights(self, df_sub):
        df_sub.dropna(inplace=True)
        df_sub = df_sub[df_sub['FundNo'].isin(list(self.portfolio.keys()))]
    
        # Rank funds by return ascending or volatility descending
        if self.dependent_var == 'Future Return':
            df_sub['quintile'] = pd.qcut(df_sub['PredFactorReturns'], 5, labels = False)
        elif self.dependent_var == 'Volatility':
            df_sub['quintile'] = pd.qcut(-df_sub['PredVolatility'], 5, labels = False)
        else:
            raise KeyError('This dependent variable is invalid')
    
        # Find funds with highest and lowest quintile
        fund_nos_long = list(df_sub.loc[df_sub['quintile'] == 4]['FundNo'])
        fund_nos_short = list(df_sub.loc[df_sub['quintile'] == 0]['FundNo'])
        
        # Set weights to 1/N if fund in highest or lowest quantile
        for fund_no in self.portfolio.keys():
            self.portfolio[fund_no] = 0
            self.portfolio[fund_no] +=  1 / len(fund_nos_long) if fund_no in fund_nos_long else 0 
            self.portfolio[fund_no] -=  1 / len(fund_nos_short) if fund_no in fund_nos_short else 0            