%% Import data
clc; clear;

% Select predictor variable ('future return' or 'volatility') and copula
% ('True' or 'False') and number of strategies including MSCI
copula = false;
no_strategies = 13;
no_notext_strategies = 6;
msci = false;
msci_notext = msci;
r_w = 0.5;

% Adjust path
c = '';
if copula == true
    c = '/copula/'; 
end
r = 'future return';
v = 'Volatility';
r_path = strcat('./', r, c);
v_path = strcat('./', v, c);

% Adjust string formatting
str_f_strategies = '';
str_f_strategies_csv = '';
str_f_strategies_min = '';
str_s_strategies = '';
str_s_strategies_csv = '';
str_f_strategies_notext = '';
str_f_strategies_notext_csv = '';
str_f_strategies_notext_min = '';
str_s_strategies_notext = '';
str_f_used_notext = '';
str_s_used_notext = '';
str_s_strategies_notext_csv = '';

for i=1:no_strategies
  str_f_strategies = strcat(str_f_strategies, ' %f');
  str_f_strategies_csv = strcat(str_f_strategies_csv, ',%f');
  str_s_strategies = strcat(str_s_strategies, ' %s');
  str_s_strategies_csv = strcat(str_s_strategies_csv, ',%s');
end  

for i=1:no_notext_strategies
  str_f_strategies_notext = strcat(str_f_strategies_notext, ' %f');
  str_s_strategies_notext = strcat(str_s_strategies_notext, ' %s');
  str_f_strategies_notext_csv = strcat(str_f_strategies_notext_csv, ',%f');
  str_s_strategies_notext_csv = strcat(str_s_strategies_notext_csv, ',%s');
end

if msci
  str_f_strategies_min = str_f_strategies(1:numel(str_f_strategies) - 3);
else  
  str_f_strategies_min = str_f_strategies;
end

if msci_notext
  str_f_strategies_notext_min = str_f_strategies_notext(1:numel(str_f_strategies_notext) - 3);
else
  str_f_strategies_notext_min = str_f_strategies_notext;
end

% Import data
strategies = textscan(fopen(strcat(r_path,'/trading_returns.csv')), strcat('%s %s', str_s_strategies), 1, 'Delimiter', ',');
strategies_notext = textscan(fopen(strcat('./',r,'/notext/trading_returns.csv')), strcat('%s %s', str_s_strategies_notext), 1, 'Delimiter', ',');
strategies = cell2mat([strategies(1:end), strategies_notext(3:end)]);

idx = textscan(fopen(strcat(r_path,'/trading_returns.csv')), strcat('%d %s', str_f_strategies), 'Headerlines', 1, 'Delimiter', ',');
row_idx = idx{1, 1};
date_idx = cell2mat(idx{1, 2});

r_returns = textscan(fopen(strcat(r_path,'/trading_returns.csv')), strcat('%d %s', str_f_strategies), 'Headerlines', 1, 'Delimiter', ',');
r_returns_notext = textscan(fopen(strcat('./',r,'/notext/trading_returns.csv')), strcat('%d %s', str_f_strategies_notext), 'Headerlines', 1, 'Delimiter', ',');
r_returns = [r_returns{1,3:end} r_returns_notext{1,3:end}];
v_returns = textscan(fopen(strcat(v_path,'/trading_returns.csv')), strcat('%d %s', str_f_strategies), 'Headerlines', 1, 'Delimiter', ',');
v_returns_notext = textscan(fopen(strcat('./',v,'/notext/trading_returns.csv')), strcat('%d %s', str_f_strategies_notext), 'Headerlines', 1, 'Delimiter', ',');
v_returns = [v_returns{1,3:end} v_returns_notext{1,3:end}];

r_fee = textscan(fopen(strcat(r_path,'/trading_mgt_fee.csv')), strcat('%d %s', str_f_strategies_min), 'Headerlines', 1, 'Delimiter', ',');
r_fee_notext = textscan(fopen(strcat('./',r,'/notext/trading_mgt_fee.csv')), strcat('%d %s', str_f_strategies_notext_min), 'Headerlines', 1, 'Delimiter', ',');
r_fee = [r_fee{1,3:end} r_fee_notext{1,3:end}];
v_fee = textscan(fopen(strcat(v_path,'/trading_mgt_fee.csv')), strcat('%d %s', str_f_strategies_min), 'Headerlines', 1, 'Delimiter', ',');
v_fee_notext = textscan(fopen(strcat('./',v,'/notext/trading_mgt_fee.csv')), strcat('%d %s', str_f_strategies_notext_min), 'Headerlines', 1, 'Delimiter', ',');
v_fee = [v_fee{1,3:end} v_fee_notext{1,3:end}];

r_turnover = textscan(fopen(strcat(r_path,'/trading_turnover.csv')), strcat('%d %s', str_f_strategies_min), 'Headerlines', 1, 'Delimiter', ',');
r_turnover_notext = textscan(fopen(strcat('./',r,'/notext/trading_turnover.csv')), strcat('%d %s', str_f_strategies_notext_min), 'Headerlines', 1, 'Delimiter', ',');
r_turnover = [r_turnover{1,3:end} r_turnover_notext{1,3:end}];
v_turnover = textscan(fopen(strcat(v_path,'/trading_turnover.csv')), strcat('%d %s', str_f_strategies_min), 'Headerlines', 1, 'Delimiter', ',');
v_turnover_notext = textscan(fopen(strcat('./',v,'/notext/trading_turnover.csv')), strcat('%d %s', str_f_strategies_notext_min), 'Headerlines', 1, 'Delimiter', ',');
v_turnover = [v_turnover{1,3:end} v_turnover_notext{1,3:end}];

% Create ensemble
returns = r_w * r_returns + (1-r_w) * v_returns;
fee = r_w * r_fee + (1-r_w) * v_fee;
turnover = r_w * r_turnover + (1-r_w) * v_turnover;

for i=1:numel(strategies)
  strategies(1, i) = strrep(strategies(1, i), 'Ret', 'Ens');
end

% Export results
fid = fopen(strcat('../../results/trading/', 'ensemble', c, '/trading_returns.csv'), 'w') ;
fprintf(fid, strcat('%s,%s', str_s_strategies_csv, str_s_strategies_notext_csv, '\n'), strategies{:});
for i=1:size(row_idx, 1)
  fprintf(fid, '%d,', row_idx(i))
  fprintf(fid, '%s', date_idx(i, :))
  fprintf(fid, strcat(str_f_strategies_csv, str_f_strategies_notext_csv, '\n'), returns(i, :))
end
fclose(fid);

fid = fopen(strcat('../../results/trading/', 'ensemble', c, '/trading_mgt_fee.csv'), 'w') ;
fprintf(fid, strcat('%s,%s', str_s_strategies_csv, str_s_strategies_notext_csv, '\n'), strategies{:});
for i=1:size(row_idx, 1)
  fprintf(fid, '%d,', row_idx(i))
  fprintf(fid, '%s', date_idx(i, :))
  fprintf(fid, strcat(str_f_strategies_csv, str_f_strategies_notext_csv, '\n'), fee(i, :))
end
fclose(fid);

fid = fopen(strcat('../../results/trading/', 'ensemble', c, '/trading_turnover.csv'), 'w') ;
fprintf(fid, strcat('%s,%s', str_s_strategies_csv, str_s_strategies_notext_csv, '\n'), strategies{:});
for i=1:size(row_idx, 1)
  fprintf(fid, '%d,', row_idx(i))
  fprintf(fid, '%s', date_idx(i, :))
  fprintf(fid, strcat(str_f_strategies_csv, str_f_strategies_notext_csv, '\n'), turnover(i, :))
end
fclose(fid);