%% Import data
clc; clear;

% Import Sharpe ratio, variance, return and t-test
pkg load statistics
addpath('./RobustSharpeTest')
addpath('./RobustVarianceTest')
addpath('./RobustMeanTest')

% Select predictor variable ('future return' or 'volatility') and copula
% ('True' or 'False') and number of strategies including MSCI
pred = 'ensemble';
copula = false;
no_strategies = 13;
no_notext_strategies = 6;
bm = 19; % 2 = Letter_Q1, 9 = Summary_Q1, 15 = Control_Q1, 7 = 1/N, 14 = ControlLS, 19 = Control_Q5
msci = false;
msci_notext = msci;

% Adjust path
c = '';
if copula == true
    c = '/copula/'; 
end
path = strcat('./', pred, c);

% Adjust string formatting
str_f_strategies = '';
str_f_strategies_csv = '';
str_f_strategies_min = '';
str_s_strategies = '';
str_s_strategies_csv = '';
str_f_strategies_notext = '';
str_f_strategies_notext_csv = '';
str_f_strategies_notext_min = '';
str_s_strategies_notext = '';
str_f_used_notext = '';
str_s_used_notext = '';
str_s_strategies_notext_csv = '';

for i=1:no_strategies
  str_f_strategies = strcat(str_f_strategies, ' %f');
  str_f_strategies_csv = strcat(str_f_strategies_csv, ',%f');
  str_s_strategies = strcat(str_s_strategies, ' %s');
  str_s_strategies_csv = strcat(str_s_strategies_csv, ',%s');
end  

for i=1:no_notext_strategies
  str_f_strategies_notext = strcat(str_f_strategies_notext, ' %f');
  str_s_strategies_notext = strcat(str_s_strategies_notext, ' %s');
  str_f_strategies_notext_csv = strcat(str_f_strategies_notext_csv, ',%f');
  str_s_strategies_notext_csv = strcat(str_s_strategies_notext_csv, ',%s');
end

if msci
  str_f_strategies_min = str_f_strategies(1:numel(str_f_strategies) - 3);
else  
  str_f_strategies_min = str_f_strategies;
end

if msci_notext
  str_f_strategies_notext_min = str_f_strategies_notext(1:numel(str_f_strategies_notext) - 3);
else
  str_f_strategies_notext_min = str_f_strategies_notext;
end

% Import data
rf = textscan(fopen('../../data/French/F-F_Q.csv'), '%s %f %f %f %f %f %f', 'Headerlines', 1, 'Delimiter', ',');
rf = [rf{1, 7}](177:214);

#returns = textscan(fopen(strcat(path,'/trading_returns.csv')), strcat('%d %s', str_f_strategies), 'Headerlines', 1, 'Delimiter', ',');
#returns_notext = textscan(fopen(strcat('./',pred,'/notext/trading_returns.csv')), strcat('%d %s', str_f_strategies_notext), 'Headerlines', 1, 'Delimiter', ',');
returns_ens = textscan(fopen('../../results/trading/ensemble/trading_returns.csv'), strcat('%d %s', str_f_strategies, str_f_strategies_notext), 'Headerlines', 1, 'Delimiter', ',');
#returns = [returns{1,3:end} returns_notext{1,3:end} returns_ens{1,3:end}];
returns = [returns_ens{1,3:end}];

#strategies = textscan(fopen(strcat(path,'/trading_returns.csv')), strcat('%s %s', str_s_strategies), 1, 'Delimiter', ',');
#strategies_notext = textscan(fopen(strcat('./',pred,'/notext/trading_returns.csv')), strcat('%s %s', str_s_strategies_notext), 1, 'Delimiter', ',');
strategies_ens = textscan(fopen('../../results/trading/ensemble/trading_returns.csv'), strcat('%s %s', str_s_strategies, str_s_strategies_notext), 1, 'Delimiter', ',');
#strategies = cell2mat([strategies(3:end), strategies_notext(3:end), strategies_ens(3:end)]);
strategies = cell2mat([strategies_ens(3:end)]);

#fee = textscan(fopen(strcat(path,'/trading_mgt_fee.csv')), strcat('%d %s', str_f_strategies_min), 'Headerlines', 1, 'Delimiter', ',');
#fee_notext = textscan(fopen(strcat('./',pred,'/notext/trading_mgt_fee.csv')), strcat('%d %s', str_f_strategies_notext_min), 'Headerlines', 1, 'Delimiter', ',');
fee_ens = textscan(fopen('../../results/trading/ensemble/trading_mgt_fee.csv'), strcat('%d %s', str_f_strategies_min, str_f_strategies_notext_min), 'Headerlines', 1, 'Delimiter', ',');
#if msci && msci_notext
#  fee = [fee{1,3:end} ones(size(fee{1,3}),1)*0.24/4/100 fee_notext{1,3:end} ones(size(fee{1,3}),1)*0.24/4/100 fee_notext{1,3:end} fee_ens{1,3:3+no_strategies-1} ones(size(fee{1,3}),1)*0.24/4/100 fee_ens{1,3+no_strategies:end} ones(size(fee{1,3}),1)*0.24/4/100];
#elseif msci && ~msci_notext
#  fee = [fee{1,3:end} ones(size(fee{1,3}),1)*0.24/4/100 fee_notext{1,3:end} fee_ens{1,3:3+no_strategies-1} ones(size(fee{1,3}),1)*0.24/4/100 fee_ens{1,3+no_strategies:end}];
#elseif ~msci && msci_notext
#  fee = [fee{1,3:end} fee_notext{1,3:end} ones(size(fee{1,3}),1)*0.24/4/100 fee_ens{1,3:end} ones(size(fee{1,3}),1)*0.24/4/100];
#else
#  fee = [fee{1,3:end} fee_notext{1,3:end} fee_ens{1,3:end}];
#end  

if msci && msci_notext
  fee = [fee_ens{1,3:3+no_strategies-1} ones(size(fee{1,3}),1)*0.24/4/100 fee_ens{1,3+no_strategies:end} ones(size(fee{1,3}),1)*0.24/4/100];
elseif msci && ~msci_notext
  fee = [fee_ens{1,3:3+no_strategies-1} ones(size(fee{1,3}),1)*0.24/4/100 fee_ens{1,3+no_strategies:end}];
elseif ~msci && msci_notext
  fee = [fee_ens{1,3:end} ones(size(fee{1,3}),1)*0.24/4/100];
else
  fee = [fee_ens{1,3:end}];
end  

#turnover = textscan(fopen(strcat(path,'/trading_turnover.csv')), strcat('%d %s', str_f_strategies_min), 'Headerlines', 1, 'Delimiter', ',');
#turnover_notext = textscan(fopen(strcat('./',pred,'/notext/trading_turnover.csv')), strcat('%d %s', str_f_strategies_notext_min), 'Headerlines', 1, 'Delimiter', ',');
turnover_ens = textscan(fopen('../../results/trading/ensemble/trading_turnover.csv'), strcat('%d %s', str_f_strategies_min, str_f_strategies_notext_min), 'Headerlines', 1, 'Delimiter', ',');
#if msci && msci_notext
#  turnover = [turnover{1,3:end} zeros(size(turnover{1,3}),1) turnover_notext{1,3:end} zeros(size(turnover{1,3}),1) turnover_ens{1,3:3+no_strategies-1} zeros(size(turnover{1,3}),1) turnover_ens{1,3+no_strategies:end} zeros(size(turnover{1,3}),1)];
#elseif msci && ~msci_notext
#  turnover = [turnover{1,3:end} zeros(size(turnover{1,3}),1) turnover_notext{1,3:end} turnover_ens{1,3:3+no_strategies-1} zeros(size(turnover{1,3}),1) turnover_ens{1,3+no_strategies:end}];
#elseif ~msci && msci_notext
#  turnover = [turnover{1,3:end} turnover_notext{1,3:end} zeros(size(turnover{1,3}),1) turnover_ens{1,3:end} zeros(size(turnover{1,3}),1)];
#else
#  turnover = [turnover{1,3:end} turnover_notext{1,3:end} turnover_ens{1,3:end}];
#end
if msci && msci_notext
  turnover = [turnover_ens{1,3:3+no_strategies-1} zeros(size(turnover{1,3}),1) turnover_ens{1,3+no_strategies:end} zeros(size(turnover{1,3}),1)];
elseif msci && ~msci_notext
  turnover = [turnover_ens{1,3:3+no_strategies-1} zeros(size(turnover{1,3}),1) turnover_ens{1,3+no_strategies:end}];
elseif ~msci && msci_notext
  turnover = [turnover_ens{1,3:end} zeros(size(turnover{1,3}),1)];
else
  turnover = [turnover_ens{1,3:end}];
end

%% Compute performance measures
returns_rf = returns - rf;
comp_ret = (prod(1+returns/100).^(4/size(returns,1))-1)*100;
vol = std(returns)*sqrt(4);
sharpe = mean(returns_rf)./std(returns_rf)*sqrt(4);
avg_fee = [mean(fee)*4];
avg_turnover = [mean(turnover*100)*4];

% Set benchmark asset (1/N)
bm_strategy = strategies{bm};
fprintf('Benchmark asset: %s \n', bm_strategy);

% Initialise variables
diff_ret = zeros(1,numel(strategies));
se_ret = zeros(1,numel(strategies));
p_ret = ones(1,numel(strategies));
avg_ret = zeros(1,numel(strategies));

se_diff_ret = zeros(1,numel(strategies));
p_diff_ret = ones(1,numel(strategies));
avg_diff_ret = zeros(1,numel(strategies));

diff_sharpe = zeros(1,numel(strategies));
se_sharpe = zeros(1,numel(strategies));
p_sharpe = ones(1,numel(strategies));
diff_sharpe_test = zeros(1,numel(strategies));

se_diff_var = zeros(1,numel(strategies));
p_diff_var = ones(1,numel(strategies));
diff_var = zeros(1,numel(strategies));
diff_vol = zeros(1,numel(strategies));

% Perform tests
for i=1:numel(strategies)
    [avg_ret(i), se_ret(i), p_ret(i)] = robustMean([returns(:,i) zeros(size(returns));], 0, 1, 1, 'G');
    diff_ret(i) = (mean(returns(:, i)) - mean(returns(:, bm))) * 4;
    diff_vol(i) = vol(i) - vol(bm);
    diff_sharpe(i) = sharpe(i) - sharpe(bm);
    
    if i ~= bm
      try
        [se_sharpe(i), ~, ~, p_sharpe(i), diff_sharpe_test(i), ~] = sharpeHACnoOut([returns_rf(:,i) returns_rf(:,bm)]);
      catch
        disp('Sharpe test failed')
      end
      try
        [avg_diff_ret(i), se_diff_ret(i), p_diff_ret(i)] = robustMean([returns(:,i) returns(:,bm)], 0, 1, 1, 'G');
      catch
        disp('Mean test failed')
      end
      try
        [diff_var(i), ~, se_diff_var(i), p_diff_var(i)] = robustVariance([returns(:,i) returns(:,bm)], 0, 1, 1, 'G');
      catch
        disp('Variance test failed')
      end 
    end
end

%% Export results
x_axis = cellstr(["Return"; "P-value"; "Return difference"; "P-value difference"; "Volatility"; "Volatility difference"; "P-value difference"; "Sharpe ratio"; "Sharpe ratio difference"; "P-value difference"; "Management fee"; "Turnover"]);
data = [comp_ret; p_ret; diff_ret; p_diff_ret; vol; diff_vol; p_diff_var; sharpe; diff_sharpe; p_sharpe; avg_fee; avg_turnover];

fid = fopen(strcat('../../results/trading/', pred, c, '/trading_performance_', num2str(bm), '.csv'), 'w') ;
fprintf(fid, strcat(' ', str_s_strategies_csv, str_s_strategies_notext_csv, '\n'), strategies{:});
for i=1:size(data, 1)
  fprintf(fid, '%s', x_axis{i})
  fprintf(fid, strcat(str_f_strategies_csv, str_f_strategies_notext_csv, '\n'), data(i, :))
end
fclose(fid);

x_axis = cellstr(["Difference in Quarterly Sharpe Ratio"; "HAC Standard Error"; "P-Value"; "Quarterly Return"; "Standard Error"; "P-value"; "Difference in Quarterly Return"; "Standard Error"; "P-value"; "Difference in Variance"; "Standard Error"; "P-value"]);
data = [diff_sharpe_test; se_sharpe; p_sharpe; avg_ret; se_ret; p_ret; avg_diff_ret; se_diff_ret; p_diff_ret; diff_var; se_diff_var; p_diff_var];
fid = fopen(strcat('../../results/trading/', pred, c, '/trading_test_', num2str(bm), '.csv'), 'w') ;
fprintf(fid, strcat(' ', str_s_strategies_csv, str_s_strategies_notext_csv, '\n'), strategies{:});
for i=1:size(data, 1)
  fprintf(fid, '%s', x_axis{i})
  fprintf(fid, strcat(str_f_strategies_csv, str_f_strategies_notext_csv, '\n'), data(i, :))
end
fclose(fid);