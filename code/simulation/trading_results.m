%% Import data
clc; clear;

% Import Sharpe ratio test
addpath('./RobustSharpeTest')

% Select predictor variable ('future return' or 'volatiltiy') and copula
% ('True' or 'False')
pred = 'volatility';
copula = false;

% Adjust path
c = '';
if copula == true
    c = '/copula/';
end
path = strcat('./', pred, c);

% Import data
returns = readtable(strcat(path,'/trading_returns.csv'), 'ReadVariableNames', false);
strategies = returns{1,3:end};
returns = str2double(returns{2:end,3:end});

fee = readtable(strcat(path,'/trading_mgt_fee.csv'), 'ReadVariableNames', false);
fee = str2double(fee{2:end,3:end});

turnover = readtable(strcat(path,'/trading_turnover.csv'), 'ReadVariableNames', false);
turnover = str2double(turnover{2:end,3:end});

%% Compute performance measures
comp_ret = (prod(1+returns/100).^(4/size(returns,1))-1)*100;
vol = std(returns)*sqrt(4);
sharpe = mean(returns)./std(returns)*sqrt(4);
avg_fee = [mean(fee*100)*4, 0.24];
avg_turnover = [mean(turnover*100)*4, 0];

%% Perform Sharpe ratio test
% Set benchmark asset (1/N)
bm = 2;

% Initialise variables
diff = zeros(1,numel(strategies));
se = zeros(1,numel(strategies));
p = ones(1,numel(strategies));

for i=1:numel(strategies)
    if i ~= bm
        [se(i), p(i), diff(i), ~] = sharpeHACnoOut([returns(:,i) returns(:,bm)]);
    end
end

%% Export results
x_axis = ["Return", "Volatility", "Sharpe ratio", "" "Management fee", "Turnover"];
data = [comp_ret; vol; sharpe; p; avg_fee; avg_turnover];
mat = [["", strategies]; [x_axis' data]];
writematrix(mat, strcat('../../results/trading/', pred, c, '/trading_performance.xlsx'));

x_axis = ["Difference in Quarterly Sharpe Ratio", "HAC Standard Error", "P-Value"];
data = [diff; se; p];
mat = [["", strategies]; [x_axis' data]];
writematrix(mat, strcat('../../results/trading/', pred, c, '/trading_sharpe_test.xlsx'));

