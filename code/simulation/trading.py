from data_analysis.copula_analysis import fit_copula


class Trading:

    DROP_COLUMNS = [
         'Quarter', 'CIK', 'Series ID', 'series-id',
         'contract-id', 'file-name', 'ncusip', 'mgmt_name'
    ]

    LETTER_VARS = ['Negativity', 'Complexity', 'Confidence', 'Personalness', 'Similarity']
    
    SUMMARY_VARS = ['Negativity', 'Confidence', 'Similarity']

    CONTROL_VARS = [
        'LN(Fund Size)', 'LN(Fund Age)', 'Expense Ratio', 'Management Fee', 'Turnover Ratio', 'Team Managed',
        'LN(No. funds)', 'Lagged Return', 'Lagged Fund Flow', 'Return Middle Quintiles', 'Return Upper Quintile',
        'Flow Middle Quintiles', 'Flow Upper Quintile'
    ]

    DEP_VARS = {
        'Future Return': 'PredReturns',
        'Volatility': 'PredVolatility'
    }

    def __init__(self, utils, strategies=[], dependent_var='Future Return', summary=[], copula=False, notext=False):
        self.__utils = utils
        self.__strategies = strategies
        self.__summary = summary
        self.__dead_funds = []
        self.__dead_date = {}
        self.__dependent_var = dependent_var
        self.__copula = copula
        self.__load_data()
        self.__notext = notext

    def __reset(self):
        """ This method resets the value of some attributes for summary trading """
        self.__dead_funds = []
        self.__dead_date = {}

        #self.__df_returns = self.__utils.pd.read_csv(
        #    self.__utils.CRSP_PATH + 'merged.csv',
        #    usecols=['FundNo', 'Quarter Date', 'Return', 'mgmt_fee']
        #)
        #self.__df_returns['Return'] = self.__df_returns['Return'].map(lambda x: (x - 1) * 100)
        
        # Forward fill management fee
        #self.__df_returns['mgmt_fee'] = self.__df_returns.groupby('FundNo')['mgmt_fee'].transform(lambda v: v.ffill())
        self.__load_data()

    def __load_data(self):
        """ This method sets the data attributes in this class """
        try:
            self.__df_letters = self.__utils.pd.read_csv(
                self.__utils.TRADING_PATH + '/baseline/results_complete.csv'
            )
            self.__df_summaries = None
            if self.__summary:
                self.__df_summaries = self.__utils.pd.read_csv(
                    self.__utils.TRADING_PATH + 'summarization/baseline/results_complete.csv'
                )  # based on TF-IDF
            self.__df_msci = self.__utils.pd.read_excel(
                self.__utils.DATA_PATH + 'msci_world.xls',
                header=6,
                usecols=['Quarter', 'Compounded Quarterly Return', 'Compounded Quarterly Return (%)']
            )
            self.__df_returns = self.__utils.pd.read_csv(
                self.__utils.CRSP_PATH + 'merged.csv',
                usecols=['FundNo', 'Quarter Date', 'Return', 'mgmt_fee']
            )
        except FileNotFoundError:
            raise FileNotFoundError('Path to trading data does not exist')

        # Make percentages of MSCI world
        self.__df_msci.rename(
            columns={
                'Compounded Quarterly Return (%)': 'Returns',
                'Compounded Quarterly Return': 'FactorReturns'
            },
            inplace=True
        )
        self.__df_msci['Returns'] = self.__df_msci['Returns'].map(lambda x: x*100)
        self.__df_returns['Return'] = self.__df_returns['Return'].map(lambda x: (x-1)*100)
        
        # Forward fill management fee
        self.__df_returns['mgmt_fee'] = self.__df_returns.groupby('FundNo')['mgmt_fee'].transform(lambda v: v.ffill())

        # Set to datetime and sort
        self.__df_letters['Date'] = self.__utils.pd.to_datetime(self.__df_letters['Date'], format='%Y-%m-%d')
        self.__df_letters = self.__df_letters.sort_values(by='Date')
        self.__df_letters['FactorReturn'] = self.__df_letters['Return'].map(lambda r: (r / 100) + 1)
        self.__df_letters['Management Fee'] = self.__df_letters['Management Fee'].map(lambda r: (r / 100))

        if self.__summary:
            self.__df_summaries['Date'] = self.__utils.pd.to_datetime(self.__df_summaries['Date'], format='%Y-%m-%d')
            self.__df_summaries = self.__df_summaries.sort_values(by='Date')
            self.__df_summaries['FactorReturn'] = self.__df_summaries['Return'].map(lambda r: (r / 100) + 1)
            self.__df_summaries['Management Fee'] = self.__df_summaries['Management Fee'].map(lambda r: (r / 100))

        self.__start_date = self.__df_letters['Quarter Date'].iloc[0]
        self.__end_date = self.__df_letters['Quarter Date'].iloc[-1]

        # Drop columns that are not relevant
        self.__df_letters.drop(columns=Trading.DROP_COLUMNS, inplace=True)
        if self.__summary:
            self.__df_summaries.drop(columns=Trading.DROP_COLUMNS, inplace=True)

    @staticmethod
    def intersection(lst1, lst2):
        """ This method returns the intersection of values in two lists """
        return [value for value in list(lst1) if value in list(lst2)]

    def find_dead_funds(self):
        """ This method finds per quarter in the sample which funds are dead """
        self.__df_returns = self.__df_returns.sort_values(by='Quarter Date').reset_index(drop=True)
        fund_vals = self.__df_returns.groupby(['FundNo']).groups
        for fund, idx in fund_vals.items():
            last_date = self.__df_returns.loc[idx[-1]]['Quarter Date']
            if last_date in self.__dead_date:
                self.__dead_date[last_date].append(fund)
            else:
                self.__dead_date[last_date] = [fund]

    def start(self, **kwargs):
        """ This method starts the simulation """
        # Set start and end date of trading simulation
        qtr_dates = list(self.__df_letters['Quarter Date'].unique())
        start_date, end_date = qtr_dates[0], qtr_dates[-1]
        window = 4  # one-year rolling window as default
        save = True
        for key, value in kwargs.items():
            if key == 'start_date':
                start_date = value
            elif key == 'end_date':
                end_date = value
            elif key == 'window':
                window = value if value > 0 else window
            elif key == 'save':
                save = value
            else:
                raise KeyError('Invalid argument is given')

        # Run general simulation on letters and 1/N (if given)
        if self.__strategies:
            print('Simulation data: shareholder letters')
            self.general_simulation(start_date, end_date, qtr_dates, window)

        # Run simulation on summary (if given)
        if self.__summary:
            print('Simulation data: summary letters')
            self.summary_simulation(start_date, end_date, qtr_dates, window)

        # Show results
        self.make_plot(start_date, end_date, save=save)
        self.print_sharpes()
        self.print_turnovers()
        self.print_fees()
        if save:
            self.to_csv()

    def general_simulation(self, start_date, end_date, qtr_dates, window):
        """
        This method runs the simulation for every strategy except those based on summarized letters
        :param start_date: first date of running the simulation e.g. `2007Q1`
        :type start_date: str
        :param end_date: last date of running the simulation e.g. `2016Q4`
        :type end_date: str
        :param qtr_dates: all unique quarters in the data set
        :type qtr_dates: list
        :param window: size of the rolling window
        :type window: int
        """
        if self.__notext:
            # Remove all textual explanatory variables
            Trading.INDEP_VARS = []
        else:
            # Choose all textual explanatory variables
            Trading.INDEP_VARS = Trading.LETTER_VARS
        
        # Set regressors
        Trading.REGRESSORS = Trading.CONTROL_VARS + Trading.INDEP_VARS
          

        # Find index of start_date
        idx_start_date = [idx for idx, date in enumerate(qtr_dates) if date == start_date][0]

        # Update start date if we do not have enough data for the window
        if idx_start_date - window - 1 < 0:
            print(f'Error: start date {start_date} is invalid for {window}-quarter rolling window')
            start_date = qtr_dates[window + 1]
            begin_window = qtr_dates[0]
            print(f'Auto-correct: new start date is {start_date}')
        else:
            begin_window = qtr_dates[idx_start_date - window - 1]

        # Select subsample that belongs to time horizon
        df = self.__utils.copy.deepcopy(self.__df_letters)
        df = df[(df['Quarter Date'] >= begin_window) & (df['Quarter Date'] <= end_date)]

        # Set initial weights for the portfolio
        ids = df['FundNo'].unique()
        for strategy in self.__strategies:
            strategy.portfolio = {id_: 0 for id_ in ids}

        # Only select subsample of returns that have the same fund numbers
        self.__df_returns = self.__df_returns[self.__df_returns['FundNo'].isin(ids)]

        # Create dictionary: date -> dead funds
        self.find_dead_funds()
        prev_date = None
        for idx, date in enumerate(qtr_dates):
            # Weights are not yet known at this date
            if int(date[:4] + date[-1]) < int(start_date[:4] + start_date[-1]):
                # Get funds that will be dead next quarter
                try:
                    dead_funds = self.__dead_date[date]
                except KeyError:
                    continue

                for strategy in self.__strategies:
                    # Remove dead funds from portfolio such that will not influence new weights
                    for dead_fund in dead_funds:
                        try:
                            del strategy.portfolio[dead_fund]
                        except KeyError:
                            pass  # in this case, the dead fund is already deleted
                continue

            # Set start date of rolling window
            begin_window = qtr_dates[idx - window - 1]

            # Determine start weights
            if date == start_date:
                print(f'\rSimulating for {date}', end='')
                # We use `< date` because we will use the "previous" quarter to predict
                df_period = df[df['Quarter Date'].isin(qtr_dates[idx-window-1:idx])]
                df_history = df[df['Quarter Date'].isin(qtr_dates[:idx])]
                
                # Get latest data from history
                df_history = df_history[df_history['Quarter Date'] == df_history.groupby('FundNo')['Quarter Date'].transform(max)]
                df_history['Quarter Date'] == df_period['Quarter Date'].unique()[-1]

                # Make data frame unique for ``FundNo - Date`` combination
                # because sometimes this combination is NOT unique
                df_period = self.get_unique_frame(df_period)
                df_history = self.get_unique_frame(df_history)
                
                # Predict future returns based on regression
                df_period[Trading.DEP_VARS[self.__dependent_var]] = self.predict_dependent_var(df_period, df_period[df_period['Quarter Date'] == df_period['Quarter Date'].unique()[-1]])
                df_history[Trading.DEP_VARS[self.__dependent_var]] = self.predict_dependent_var(df_period, df_history)
                if self.__dependent_var == 'Future Return':
                    df_period['PredFactorReturns'] = df_period['PredReturns'].map(lambda r: (r / 100) + 1)
                    df_history['PredFactorReturns'] = df_history['PredReturns'].map(lambda r: (r / 100) + 1)

                # Get funds that will be dead next quarter
                dead_funds = self.__dead_date[date]
                
                # Remove dead funds from subset, s.t. we do not invest in them
                df_period = df_period[~df_period['FundNo'].isin(dead_funds)]
                df_history = df_history[~df_history['FundNo'].isin(dead_funds)]
                
                # Determine weights for future portfolio based on every strategy
                for strategy in self.__strategies:
                    # Remove dead funds from portfolio such that will not influence new weights
                    for dead_fund in dead_funds:
                        try:
                            del strategy.portfolio[dead_fund]
                        except KeyError:
                            pass  # in this case, the dead fund is already deleted

                    if 'BuyHoldSell' in strategy.__name__:
                        strategy.update_weights(df_period)
                    else:
                        strategy.update_weights(df_history)

                # Update previous date
                prev_date = date

                # Go to next quarter, so that we can start evaluating performances
                continue

            # Check performance based on weights
            print(f'\rSimulating for {date}', end='')
            df_returns_id = self.__df_returns[self.__df_returns['Quarter Date'] == date][['Return', 'FundNo']].set_index(['FundNo'])
            df_mgmt_fee_id = self.__df_returns[self.__df_returns['Quarter Date'] == date][['mgmt_fee', 'FundNo']].set_index(['FundNo'])
                                                 
            # Get subsample that belongs to the current date
            df_period = df[df['Quarter Date'].isin(qtr_dates[idx - window - 1:idx])]
            df_history = df[df['Quarter Date'].isin(qtr_dates[:idx])]
 
            
            # Get latest data from history
            df_history = df_history[df_history['Quarter Date'] == df_history.groupby('FundNo')['Quarter Date'].transform(max)]
            df_history['Quarter Date'] == df_period['Quarter Date'].unique()[-1]

            # Make data frame unique for ``FundNo - Date`` combination
            # because sometimes this combination is NOT unique
            df_period = self.get_unique_frame(df_period)
            df_history = self.get_unique_frame(df_history)
            
            # Predict future returns based on regression
            df_period[Trading.DEP_VARS[self.__dependent_var]] = self.predict_dependent_var(df_period, df_period[df_period['Quarter Date'] == df_period['Quarter Date'].unique()[-1]])
            df_history[Trading.DEP_VARS[self.__dependent_var]] = self.predict_dependent_var(df_period, df_history)
            if self.__dependent_var == 'Future Return':
                df_period['PredFactorReturns'] = df_period['PredReturns'].map(lambda r: (r / 100) + 1)
                df_history['PredFactorReturns'] = df_history['PredReturns'].map(lambda r: (r / 100) + 1)

            # Get funds that are dead (last returns were previous quarter)
            dead_funds = self.__dead_date[date]
            
            # Remove dead funds from subset, such that you cannot invest in them anymore
            df_period = df_period[~df_period['FundNo'].isin(dead_funds)]
            df_history = df_history[~df_history['FundNo'].isin(dead_funds)]
            
            returns = {}
            mgmt_fees = {}
            prev_weights = {}
            for strategy in self.__strategies:
                # Determine returns and management fee based on asset allocation
                for fund_no, weight in strategy.portfolio.items():
                    try:
                        returns[fund_no] = df_returns_id.loc[fund_no].values[0] * weight
                        mgmt_fees[fund_no] = df_mgmt_fee_id.loc[fund_no].values[0] * weight
                        prev_weights[fund_no] = weight * (1 + df_returns_id.loc[fund_no].values[0]/100)
                    except:
                        raise KeyError('this should not be possible')

                # Remove dead funds from portfolio such that will not influence new weights
                for dead_fund in dead_funds:
                    try:
                        del strategy.portfolio[dead_fund]
                    except KeyError:
                        pass  # in this case, the dead fund is already deleted

                # Update weights
                if 'BuyHoldSell' in strategy.__name__:
                    strategy.update_weights(df_period)
                else:
                    strategy.update_weights(df_history)

                # Normalise previous weights
                if 'LongShort' in strategy.__name__:
                    norm_prev_weights = 2 / sum([abs(w) for w in prev_weights.values()])
                else:
                    norm_prev_weights = sum(prev_weights.values())
                prev_weights = {k: v / norm_prev_weights for k, v in prev_weights.items()}
                
                # Determine turnover
                turnover = 0
                for fund_no in list(set(list(prev_weights.keys()) + list(strategy.portfolio.keys()))):
                    if fund_no in prev_weights.keys() and fund_no in strategy.portfolio.keys():
                        turnover += abs(prev_weights[fund_no] - strategy.portfolio[fund_no])
                    elif fund_no in prev_weights.keys():
                        turnover += abs(prev_weights[fund_no] - 0)
                    else:
                        turnover += abs(0 - strategy.portfolio[fund_no])

                # Update counters
                strategy.returns[date] = sum(returns.values())
                strategy.turnover[date] = turnover
                strategy.mgt_fee[date] = sum(mgmt_fees.values())

            # Update previous date
            prev_date = date

            assert sorted(self.__strategies[0].portfolio.keys()) == sorted(self.__strategies[1].portfolio.keys())
        print()

    def summary_simulation(self, start_date, end_date, qtr_dates, window):
        """
        Runs the simulation for the strategy based on summarized letters
        :param start_date: first date of running the simulation e.g. `2007Q1`
        :type start_date: str
        :param end_date: last date of running the simulation e.g. `2016Q4`
        :type end_date: str
        :param qtr_dates: all unique quarters in the data set
        :type qtr_dates: list
        :param window: size of the rolling window
        :type window: int
        """
        # Reset values
        self.__reset()
        
        if self.__notext:
            # Remove all textual explanatory variables
            Trading.INDEP_VARS = []
        else:
            # Choose all textual explanatory variables
            Trading.INDEP_VARS = Trading.SUMMARY_VARS
        
        # Set regressors
        Trading.REGRESSORS = Trading.CONTROL_VARS + Trading.INDEP_VARS

        # Find index of start_date
        idx_start_date = [idx for idx, date in enumerate(qtr_dates) if date == start_date][0]

        # Update start date if we do not have enough data for the window
        if idx_start_date - window - 1< 0:
            start_date = qtr_dates[window + 1]
            begin_window = qtr_dates[0]
        else:
            begin_window = qtr_dates[idx_start_date - window - 1]

        # Select subsample that belongs to time horizon
        df = self.__utils.copy.deepcopy(self.__df_summaries)
        df = df[(df['Quarter Date'] >= begin_window) & (df['Quarter Date'] <= end_date)]

        # Set initial weights for the portfolio
        ids = df['FundNo'].unique()
        for strategy in self.__summary:
            strategy.portfolio = {id_: 0 for id_ in ids}

        # Only select subsample of returns that have the same fund numbers
        self.__df_returns = self.__df_returns[self.__df_returns['FundNo'].isin(ids)]

        # Create dictionary: date -> dead funds
        self.find_dead_funds()

        prev_date = None
        for idx, date in enumerate(qtr_dates):
            # Weights are not yet known at this date
            if int(date[:4] + date[-1]) < int(start_date[:4] + start_date[-1]):
                # Get funds that will be dead next quarter
                try:
                    dead_funds = self.__dead_date[date]
                except KeyError:
                    continue

                for strategy in self.__summary:
                    # Remove dead funds from portfolio such that will not influence new weights
                    for dead_fund in dead_funds:
                        try:
                            del strategy.portfolio[dead_fund]
                        except KeyError:
                            pass  # in this case, the dead fund is already deleted
                continue

            # Determine start weights
            if date == start_date:
                print(f'\rSimulating for {date}', end='')
                df_returns_id = self.__df_returns[self.__df_returns['Quarter Date'] == date][['Return', 'FundNo']].set_index(['FundNo'])
                df_mgmt_fee_id = self.__df_returns[self.__df_returns['Quarter Date'] == date][['mgmt_fee', 'FundNo']].set_index(['FundNo'])
                                                     
                # Get subsample that belongs to the current date
                df_period = df[df['Quarter Date'].isin(qtr_dates[idx - window - 1:idx])]
                df_history = df[df['Quarter Date'].isin(qtr_dates[:idx])]
                
                # Get latest data from history
                df_history = df_history[df_history['Quarter Date'] == df_history.groupby('FundNo')['Quarter Date'].transform(max)]
                df_history['Quarter Date'] == df_period['Quarter Date'].unique()[-1]

                # Make data frame unique for ``FundNo - Date`` combination
                # because sometimes this combination is NOT unique
                df_period = self.get_unique_frame(df_period)
                df_history = self.get_unique_frame(df_history)
                
                # Predict future returns based on regression
                df_period[Trading.DEP_VARS[self.__dependent_var]] = self.predict_dependent_var(df_period, df_period[df_period['Quarter Date'] == df_period['Quarter Date'].unique()[-1]])
                df_history[Trading.DEP_VARS[self.__dependent_var]] = self.predict_dependent_var(df_period, df_history)
                if self.__dependent_var == 'Future Return':
                    df_period['PredFactorReturns'] = df_period['PredReturns'].map(lambda r: (r / 100) + 1)
                    df_history['PredFactorReturns'] = df_history['PredReturns'].map(lambda r: (r / 100) + 1)

                # Get funds that are dead (last returns were previous quarter)
                dead_funds = self.__dead_date[date]
            
                # Remove dead funds from subset, such that you cannot invest in them anymore
                df_period = df_period[~df_period['FundNo'].isin(dead_funds)]
                df_history = df_history[~df_history['FundNo'].isin(dead_funds)]
                
                # Determine weights for future portfolio based on every strategy
                for strategy in self.__summary:
                    # Remove dead funds from portfolio such that will not influence new weights
                    for dead_fund in dead_funds:
                        try:
                            del strategy.portfolio[dead_fund]
                        except KeyError:
                            pass  # in this case, the dead fund is already deleted

                    if 'BuyHoldSell' in strategy.__name__:
                        strategy.update_weights(df_period)
                    else:
                        strategy.update_weights(df_history)

                # Update previous date
                prev_date = date

                # Go to next quarter, so that we can start evaluating performances
                continue

            # Check performance based on weights
            print(f'\rSimulating for {date}', end='')
            df_returns_id = self.__df_returns[self.__df_returns['Quarter Date'] == date][['Return', 'FundNo']].set_index(['FundNo'])
            df_mgmt_fee_id = self.__df_returns[self.__df_returns['Quarter Date'] == date][['mgmt_fee', 'FundNo']].set_index(['FundNo'])

            # Get subsample that belongs to the current date
            df_period = df[df['Quarter Date'].isin(qtr_dates[idx - window - 1:idx])]
            df_history = df[df['Quarter Date'].isin(qtr_dates[:idx])] 
            
            # Get latest data from history
            df_history = df_history[df_history['Quarter Date'] == df_history.groupby('FundNo')['Quarter Date'].transform(max)]
            df_history['Quarter Date'] == df_period['Quarter Date'].unique()[-1]

            # Make data frame unique for ``FundNo - Date`` combination
            # because sometimes this combination is NOT unique
            df_period = self.get_unique_frame(df_period)
            df_history = self.get_unique_frame(df_history)
            
            # Predict future returns based on regression
            df_period[Trading.DEP_VARS[self.__dependent_var]] = self.predict_dependent_var(df_period, df_period[df_period['Quarter Date'] == df_period['Quarter Date'].unique()[-1]])
            df_history[Trading.DEP_VARS[self.__dependent_var]] = self.predict_dependent_var(df_period, df_history)
            if self.__dependent_var == 'Future Return':
                df_period['PredFactorReturns'] = df_period['PredReturns'].map(lambda r: (r / 100) + 1)
                df_history['PredFactorReturns'] = df_history['PredReturns'].map(lambda r: (r / 100) + 1)
            
            # Get funds that are dead (last returns were previous quarter)
            dead_funds = self.__dead_date[date]

            # Remove dead funds from subset, such that you cannot invest in them anymore
            df_period = df_period[~df_period['FundNo'].isin(dead_funds)]
            df_history = df_history[~df_history['FundNo'].isin(dead_funds)]
            
            returns = {}
            mgmt_fees = {}
            prev_weights = {}
            for strategy in self.__summary:
                # Determine returns and management fee based on asset allocation
                for fund_no, weight in strategy.portfolio.items():
                    try:
                        returns[fund_no] = df_returns_id.loc[fund_no].values[0] * weight
                        mgmt_fees[fund_no] = df_mgmt_fee_id.loc[fund_no].values[0] * weight
                        prev_weights[fund_no] = weight * (1 + df_returns_id.loc[fund_no].values[0]/100)
                    except:
                        raise KeyError('this should not be possible')

                # Remove dead funds from portfolio such that will not influence new weights
                for dead_fund in dead_funds:
                    try:
                        del strategy.portfolio[dead_fund]
                    except KeyError:
                        pass  # in this case, the dead fund is already deleted

                # Update weights
                if 'BuyHoldSell' in strategy.__name__:
                    strategy.update_weights(df_period)
                else:
                    strategy.update_weights(df_history)

                # Normalise previous weights
                if 'LongShort' in strategy.__name__:
                    norm_prev_weights = 2 / sum([abs(w) for w in prev_weights.values()])
                else:
                    norm_prev_weights = sum(prev_weights.values())
                prev_weights = {k: v / norm_prev_weights for k, v in prev_weights.items()}
                
                # Determine turnover
                turnover = 0
                for fund_no in list(set(list(prev_weights.keys()) + list(strategy.portfolio.keys()))):
                    if fund_no in prev_weights.keys() and fund_no in strategy.portfolio.keys():
                        turnover += abs(prev_weights[fund_no] - strategy.portfolio[fund_no])
                    elif fund_no in prev_weights.keys():
                        turnover += abs(prev_weights[fund_no] - 0)
                    else:
                        turnover += abs(0 - strategy.portfolio[fund_no])

                # Update counters
                strategy.returns[date] = sum(returns.values())
                strategy.turnover[date] = turnover
                strategy.mgt_fee[date] = sum(mgmt_fees.values())

            # Update previous date
            prev_date = date
            
            assert sorted(self.__summary[0].portfolio.keys()) == sorted(self.__summary[1].portfolio.keys())
        print()

    @staticmethod
    def get_unique_frame(df_sub):
        """
        This method returns unique values in a pandas.DataFrame based on the `FundNo-Date` combination
        :param df_sub:
        :type df_sub:
        :return:
        :rtype:
        """
        # Get two columns that we want to filter on
        fund_date = df_sub[['FundNo', 'Date']].drop_duplicates()

        # If length stays the same, we are done
        if len(fund_date) == len(df_sub):
            return df_sub

        return df_sub.loc[fund_date.index]

    def predict_dependent_var(self, df_sub, df_predict):
        """
        This method determines the future returns based on the fitted coefficients
        :param df_sub: subsample for a specific date
        :type df_sub: pandas.DataFrame
        :return: predicted returns per class ID
        :rtype: pandas.Series
        """
        if not self.__copula:
            # Construct bivariate index (needed for fixed effects regressions)
            df_sub.dropna(subset=['Return'], inplace=True)
            df_sub.reset_index(drop=True)
            df_predict.dropna(subset=['Return'], inplace=True)
            df_predict.reset_index(drop=True)

            # Set multi-index for PanelOLS
            df_sub.set_index(['FundNo', 'Date'], inplace=True, drop=False)
            df_predict.set_index(['FundNo', 'Date'], inplace=True, drop=False)

            # Run regression and fit model
            df_sub1 = df_sub[df_sub['Quarter Date'] != df_sub['Quarter Date'].unique()[-1]]
            Y = df_sub1[self.__dependent_var]
            X = self.__utils.sm2.add_constant(df_sub1[Trading.REGRESSORS])

            mdl = self.__utils.PanelOLS(dependent=Y, exog=X, entity_effects=False, time_effects=False, drop_absorbed=True)

            # Fit model
            res = mdl.fit(cov_type='clustered', cluster_entity=True, cluster_time=True)

            # Obtain parameters
            params = res.params.rename(index={'const': 'Constant'})

            # Only select last quarter to predict
            #df = df_sub[df_sub['Quarter Date'] == df_sub['Quarter Date'].unique()[-1]]
            df = df_predict
            X = self.__utils.sm2.add_constant(df[Trading.REGRESSORS])
            df[Trading.DEP_VARS[self.__dependent_var]] = res.predict(exog=X, fitted=False, effects=True)
            return df[Trading.DEP_VARS[self.__dependent_var]]
        else:
            all_vars = Trading.INDEP_VARS + [self.__dependent_var]
            df_sub.dropna(subset=all_vars, inplace=True)
            df_predict.dropna(subset=all_vars, inplace=True)
            df_ins = df_sub[df_sub['Quarter Date'] != df_sub['Quarter Date'].unique()[-1]]
            df_out = df_predict 
            # df_out = df_sub[df_sub['Quarter Date'] == df_sub['Quarter Date'].unique()[-1]]
            index = df_out.index
            pred_list = []

            for indep_var in Trading.INDEP_VARS:
                res_copula = fit_copula(data=df_ins[[indep_var, self.__dependent_var]], dist_marginal='Empirical',
                                        bForecast=True, x_out_of_sample=df_out[indep_var], bSimulate=True, nSim=1000,
                                        indep_var=indep_var, dep_var=self.__dependent_var, bSaveUniform=False)
                pred_list.append(res_copula)

            # Forecast averaging
            average = self.__utils.np.average(self.__utils.np.array([pred_list]), axis = 0)
            average = self.__utils.np.transpose(average)
            df = self.__utils.pd.DataFrame(data = average[:,0], index = index, columns = [Trading.DEP_VARS[self.__dependent_var]])
            return df

    def make_plot(self, start_qtr, end_qtr, save=True):
        """
        This method creates a plot based on the trading simulation
        :param start_qtr: first date for which we have simulated values
        :type start_qtr: str
        :param end_qtr: last date for which we have simulated values
        :type end_qtr: str
        """
        self.__utils.style.use('fivethirtyeight')

        # Make figure object
        fig, axes = self.__utils.plt.subplots(nrows=2, ncols=1, figsize=(10, 8), sharex=True)

        diff_qtr = (int(end_qtr.split('Q')[0]) - int(start_qtr.split('Q')[0]))*4 + \
                   (int(end_qtr.split('Q')[1]) - int(start_qtr.split('Q')[1])) + 1

        # Set axes
        set_legend = {strategy.name: False for strategy in self.__strategies + self.__summary}
        axes[1].set_xlabel('Time')
        axes[0].set_xlim(0, diff_qtr - 1)
        axes[1].set_xlim(0, diff_qtr - 1)
        axes[0].set_ylabel('Return (in percentages)')
        axes[1].set_ylabel('Cumulative Return (in factors)')

        dates = []
        qtr = int(start_qtr.split('Q')[1])
        year = int(start_qtr.split('Q')[0])
        for i in range(diff_qtr):
            # Update quarter and year if we go to the next year
            if qtr == 5:
                year += 1
                qtr = 1

            dates.append(f'{year}Q{qtr}')

            # Update quarter
            qtr += 1

        self.__utils.plt.xticks(range(diff_qtr), dates)
        self.__utils.plt.xticks(rotation=45)
        self.__utils.plt.tight_layout()

        idxs = []
        self.__msci_vals = []
        msci_factors = []
        for idx, date in enumerate(dates):
            found = True
            for strategy in self.__strategies + self.__summary:
                try:
                    x = strategy.returns[date]
                    if isinstance(x, self.__utils.np.ndarray):
                        x = x[0]
                except KeyError:
                    found = False
                    continue

                # Add date for x-axis
                if idx not in idxs:
                    idxs.append(idx)

                # Get cumulative returns
                factor_val = (x / 100) + 1
                if strategy.factor_vals:
                    factor_val *= strategy.factor_vals[-1]
                strategy.factor_vals.append(factor_val)

                strategy.perc_vals.append(x)

                axes[0].plot(idxs, strategy.perc_vals, strategy.color,
                             linestyle=strategy.linestyle, label=strategy.name, linewidth=1.5)
                axes[1].plot(idxs, strategy.factor_vals, strategy.color,
                             linestyle=strategy.linestyle, label=strategy.name, linewidth=1.5)
                axes[0].axhline(y=0, xmin=0, xmax=diff_qtr - 1, color='black', linewidth=1.5)
                axes[1].axhline(y=1, xmin=0, xmax=diff_qtr - 1, color='black', linewidth=1.5)

                if not set_legend[strategy.name]:
                    axes[0].legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), ncol=len(self.__strategies + self.__summary))
                    set_legend[strategy.name] = True

            if found:
                # Get MSCI world value for this quarter and save it
                date_msci = date[:4] + ' ' + date[4:]
                msci_val = self.__df_msci[self.__df_msci['Quarter'] == date_msci]['Returns']
                self.__msci_vals.append(msci_val.values[0])

                factor_val = self.__df_msci[self.__df_msci['Quarter'] == date_msci]['FactorReturns'].values[0]
                if msci_factors:
                    factor_val *= msci_factors[-1]
                msci_factors.append(factor_val)

                axes[0].plot(idxs, self.__msci_vals, 'purple', label='MSCI World', linewidth=1.5)
                axes[1].plot(idxs, msci_factors, 'purple', label='MSCI World', linewidth=1.5)

                self.__utils.plt.show()
            self.__utils.plt.pause(0.5)

        if save:
            dir = self.__dependent_var.lower()
            if self.__copula:
                fig.savefig(f'./simulation/{dir}/copula/trading_simulation.png')
            elif self.__notext:
                fig.savefig(f'./simulation/{dir}/notext/trading_simulation.png')
            else:
                fig.savefig(f'./simulation/{dir}/trading_simulation.png')

    def print_sharpes(self):
        """ Prints statistics of the simulated sharpe ratios """
        print('\nSHARPE RATIOS')
        print('-' * 80)
        print("{:<15} {:<19} {:<20} {:<20}".format('Strategy', 'Mean Qtr. Return', 'Std Qtr. Return', 'Sharpe'))
        strategies = self.__strategies + self.__summary
        for strategy in strategies:
            mean_ = self.__utils.np.mean(strategy.perc_vals)
            std_ = self.__utils.np.std(strategy.perc_vals)
            sharpe = round(mean_ / std_, 3)
            print("{:<15} {:<19} {:<20} {:<20}".format(strategy.name, round(mean_, 3), round(std_, 3), sharpe))
        mean_ = self.__utils.np.mean(self.__msci_vals)
        std_ = self.__utils.np.std(self.__msci_vals)
        sharpe = round(mean_ / std_, 3)
        print("{:<15} {:<19} {:<20} {:<20}".format('MSCI', round(mean_, 3), round(std_, 3), sharpe))

    def print_turnovers(self):
        """ Prints statistics of the simulated turnover ratios """
        print('\nTURNOVER')
        print('-' * 80)
        print("{:<15} {:<15} {:<10}".format('Strategy', 'Mean', 'Std'))
        strategies = self.__strategies + self.__summary
        for strategy in strategies:
            vals = list(strategy.turnover.values())
            new_vals = []
            for val in vals:
                if isinstance(val, self.__utils.np.ndarray):
                    new_vals.append(val[0])
                else:
                    new_vals.append(val)

            mean_ = self.__utils.np.mean(new_vals)
            std_ = self.__utils.np.std(new_vals)
            print("{:<15} {:<15} {:<10}".format(strategy.name, round(mean_, 3), round(std_, 3)))

    def print_fees(self):
        """ Prints statistics of the simulated management fees """
        print('\nMANAGEMENT FEES')
        print('-' * 80)
        print("{:<15} {:<15} {:<10}".format('Strategy', 'Mean', 'Std'))
        strategies = self.__strategies + self.__summary
        for strategy in strategies:
            vals = list(strategy.mgt_fee.values())
            new_vals = []
            for val in vals:
                if isinstance(val, self.__utils.np.ndarray):
                    new_vals.append(val[0])
                else:
                    new_vals.append(val)

            mean_ = self.__utils.np.mean(new_vals)
            std_ = self.__utils.np.std(new_vals)
            print("{:<15} {:<15} {:<10}".format(strategy.name, round(mean_, 3), round(std_, 3)))

    def to_csv(self, export_msci = False):
        """ This method writes the statistical output from the simulation to a CSV file """
        dict_returns = {}
        dict_turnover = {}
        dict_mgt_fee = {}
        strategies = self.__strategies + self.__summary

        # Make dataframe for returns
        dict_returns['Date'] = list(self.__strategies[0].returns.keys())
        dict_turnover['Date'] = list(self.__strategies[0].returns.keys())
        dict_mgt_fee['Date'] = list(self.__strategies[0].returns.keys())
        for strategy in strategies:
            dict_returns[strategy.name] = list(strategy.returns.values())
            dict_turnover[strategy.name] = list(strategy.turnover.values())
            dict_mgt_fee[strategy.name] = list(strategy.mgt_fee.values())
        if export_msci:    
            dict_returns['MSCI'] = self.__msci_vals

        # Write dataframes to CSV
        dir = self.__dependent_var.lower()
        for name, dict_ in zip(['returns', 'turnover', 'mgt_fee'], [dict_returns, dict_turnover, dict_mgt_fee]):
            df = self.__utils.pd.DataFrame.from_dict(dict_)
            if self.__copula:
                df.to_csv(f'./simulation/{dir}/copula/trading_{name}.csv')
            elif self.__notext:
                df.to_csv(f'./simulation/{dir}/notext/trading_{name}.csv')
            else:
                df.to_csv(f'./simulation/{dir}/trading_{name}.csv')

