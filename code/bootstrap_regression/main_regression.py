import pandas as pd
import numpy as np
import time
import statsmodels.api as sm2
from linearmodels import PanelOLS
from resample.bootstrap import bootstrap

'''
This script serves as the main file to efficiently run the double resampling 
bootstrap procedure which generates regression output based on bootstrapped p-values.
'''

# -----------------------
# User defined parameters
# -----------------------

# -----------------------DESCRIPTIONS-----------------------
# SCORES_TF_IDF: if TRUE, scores are computed using TF-IDF
# SURPRISE_REGRESSION: if TRUE, regress on surprise measures
# SINGLE_FUND_ONLY: if TRUE, only consider fund-specific shareholder letters
# CRISIS_SAMPLE: if TRUE, consider crisis subsample
# SUMMARIZATION_SCORES: if TRUE, consider summarized letter scores

# TF_IDF + summarization + relative
run1 = {'SCORES_TF_IDF': False, \
         'SURPRISE_REGRESSION': False, \
         'SINGLE_FUND_ONLY': False, \
         'CRISIS_SAMPLE': False, \
         'SUMMARIZATION_SCORES': True}

# prop + baseline + absolute
run2 = {'SCORES_TF_IDF': True, \
         'SURPRISE_REGRESSION': False, \
         'SINGLE_FUND_ONLY': True, \
         'CRISIS_SAMPLE': False, \
         'SUMMARIZATION_SCORES': True}

# prop + single_fund
run3 = {'SCORES_TF_IDF': False, \
         'SURPRISE_REGRESSION': False, \
         'SINGLE_FUND_ONLY': True, \
         'CRISIS_SAMPLE': False, \
         'SUMMARIZATION_SCORES': False}

runs = [run1, run2, run3]

# Bootstrap settings
bBootstrap = True
nBootstrap = 100

#
# * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * +

def sample_timeseries(fund, boot_sample):
    # Extract relevant dates
    dates = boot_sample.loc[boot_sample['index'] == fund, ['Date']]
    dates = dates['Date'].tolist()

    # Sample from dates
    rows = boot_sample.loc[boot_sample['Date'].isin(dates)].sample(len(dates))
    rows = rows.sort_values(by=['Date'])
    rows['index'] = fund

    return rows


def panel_bootstrap(df_input, obs_t_stats, num_iter=100):

    # Sample funds from sample
    start = time.time()  # Keep track of time
    funds_list = df_input['FundNo'].unique()
    boot_funds = bootstrap(a=funds_list, b=num_iter)

    t_stats_array = np.zeros((len(df_input.columns)-2, num_iter))

    for i in range(num_iter):
        # Print progress
        if (i+1) % 10 == 0:
            current_time = round(time.time() - start)
            print('Bootstrap iteration ' + str(i+1) + '. Time elapsed ' + str(current_time))

        # Select subsample
        boot_sample = df_input[df_input['FundNo'].isin(boot_funds[i])]

        # Create duplicate funds to retain correct number of clusters
        funds_sample = pd.DataFrame(boot_funds[i], columns=['FundNo'])
        funds_sample.reset_index(inplace=True)
        boot_sample = funds_sample.merge(boot_sample, how='left')

        # Sample from timeseries for each fund
        funds = boot_sample['index'].unique()

        df_boot_sample = [sample_timeseries(fund, boot_sample) for fund in funds]
        df_boot_sample = pd.concat(df_boot_sample, axis=0)

        # Set double index
        df_boot_sample.set_index(['index', 'Date'], inplace=True)
        df_boot_sample.drop(columns=['FundNo'], inplace=True)

        # Run FE regressions
        Y = df_boot_sample.iloc[:, 0]
        X = sm2.add_constant(df_boot_sample.iloc[:, 1:])
        est = PanelOLS(Y, X, entity_effects=True, time_effects=True, drop_absorbed=True)  # Two-way fixed effects
        res = est.fit(cov_type='clustered', cluster_entity=True, cluster_time=True)

        # Extract t-statistics
        t_stats = res.tstats.rename(index={'const': 'Constant'})
        t_stats_array[:, i] = res.tstats

    # Compute (finite-sample adjusted) empirical p-values
    num_greater = np.abs(t_stats_array) >= np.expand_dims(np.abs(obs_t_stats), axis=1)
    p_values = (np.sum(num_greater, axis=1) + 1) / (num_iter + 1)

    # Convert numpy array to dataframe with correct index
    df_p_values = t_stats.copy()
    df_p_values[:] = pd.Series(p_values)

    return df_p_values


def run_baseline_regressions(input_filename, bBootstrap=False, bRelative=False, nBootstrap=100):
    '''Runs baseline regressions a la Hillert et al. (2016)'''

    # Read csv file with fund characteristics, performance measures and textual scores
    df_results = pd.read_csv(input_filename)
    df_results['Date'] = pd.to_datetime(df_results['Date'], format='%Y/%m/%d')

    # Define dependent, independent and control variables (different for summarized letters)
    dep_vars = ['Future Return', 'Future Fund Flow', 'Alpha', 'Volatility']
    if SUMMARIZATION_SCORES:
        indep_abs_vars = ['Negativity', 'Confidence', 'Similarity']
        indep_surprise_vars = ['Negativity Surprise', 'Confidence Surprise', 'Similarity']
    else:
        indep_abs_vars = ['Negativity', 'Complexity', 'Confidence', 'Personalness', 'Similarity']
        indep_surprise_vars = ['Negativity Surprise', 'Complexity Surprise', 'Confidence Surprise', 'Personalness Surprise', 'Similarity']

    if SINGLE_FUND_ONLY:
        # Do not include LN(No. funds) as control variable
        control_vars = ['LN(Fund Size)', 'LN(Fund Age)', 'Expense Ratio', 'Management Fee', 'Turnover Ratio',
                        'Team Managed', 'Lagged Return', 'Lagged Fund Flow', 'Lagged Volatility', 'Return Middle Quintiles',
                        'Return Upper Quintile', 'Flow Middle Quintiles', 'Flow Upper Quintile']
    else:
        control_vars = ['LN(Fund Size)', 'LN(Fund Age)', 'Expense Ratio', 'Management Fee', 'Turnover Ratio', 'Team Managed',
                        'LN(No. funds)', 'Lagged Return', 'Lagged Fund Flow', 'Lagged Volatility', 'Return Middle Quintiles', 'Return Upper Quintile',
                        'Flow Middle Quintiles', 'Flow Upper Quintile']

    '''Perform data transformations'''
    # Sort dataframe by FundNo and Date
    df_results = df_results.sort_values(by=['FundNo', 'Date'], ascending=[True, True])

    # Compute surprise measures as first difference of absolute scores (different for summarized letters)
    df_results['Negativity Surprise'] = df_results.groupby('FundNo')['Negativity'].diff()
    df_results['Confidence Surprise'] = df_results.groupby('FundNo')['Confidence'].diff()

    if not SUMMARIZATION_SCORES:
        df_results['Complexity Surprise'] = df_results.groupby('FundNo')['Complexity'].diff()
        df_results['Personalness Surprise'] = df_results.groupby('FundNo')['Personalness'].diff()

    # Compute log-transformed variables
    df_results['LN(Fund Size)'] = np.log(df_results['Fund Size'])
    df_results['LN(Fund Age)'] = np.log(df_results['Fund Age'])

    # Compute number of funds in letter
    if SINGLE_FUND_ONLY == False:
        df_results['No. funds'] = df_results['series-id'].apply(lambda x: len(x.split(',')))
        df_results['LN(No. funds)'] = np.log(df_results['No. funds'])

    # Create dummy variable indicating whether or not fund is team-managed
    df_results['Team Managed'] = 0
    df_results.loc[df_results['mgr_name'].str.contains('Team Managed'), 'Team Managed'] = 1

    # Remove rows with missing values
    df_results = df_results.dropna(subset=['Lagged Return', 'Return', 'Lagged Fund Flow', 'Fund Flow', 'Fund Age'])
    df_results = df_results.reset_index(drop=True)

    # Compute return and fund flow quintiles
    df_results['Return Quintile'] = df_results.groupby(['Quarter Date'])['Lagged Return'].transform(
        lambda x: pd.qcut(x, 5, duplicates='drop', labels=False))
    df_results['Flow Quintile'] = df_results.groupby(['Quarter Date'])['Lagged Fund Flow'].transform(
        lambda x: pd.qcut(x, 5, duplicates='drop', labels=False))

    # Create dummy variables for return and flow quintiles (leave out bottom quintiles due to multicollinearity)
    df_results['Return Middle Quintiles'] = 0
    df_results['Return Upper Quintile'] = 0
    df_results.loc[df_results['Return Quintile'].isin([1, 2, 3]), 'Return Middle Quintiles'] = 1
    df_results.loc[df_results['Return Quintile'] == 4, 'Return Upper Quintile'] = 1

    df_results['Flow Middle Quintiles'] = 0
    df_results['Flow Upper Quintile'] = 0
    df_results.loc[df_results['Flow Quintile'].isin([1, 2, 3]), 'Flow Middle Quintiles'] = 1
    df_results.loc[df_results['Flow Quintile'] == 4, 'Flow Upper Quintile'] = 1

    # Write complete dataframe to csv file
    # df_results.to_csv(RESULTS_COMPLETE_FILENAME, index=False)

    # For bootstrapped standard errors, create copy of dataframe
    if bBootstrap:
        df_input = df_results.copy()

    # Construct bivariate index (needed for fixed effects regressions)
    df_results.set_index(['FundNo', 'Date'], inplace=True)

    '''Run regressions on absolute and surprise textual scores'''
    # Loop over both sets of independent variables (absolute and surprise)
    if not bRelative:
        indep_vars = indep_abs_vars
        bAbs = True
    else:
        indep_vars = indep_surprise_vars
        bAbs = False

    # Create empty dataframe and collect regressors
    df_regression_output = pd.DataFrame()
    df_residuals = pd.DataFrame()
    regressors = [val for sublist in [indep_vars, control_vars] for val in sublist]

    # Loop over all dependent variables
    for dep_var in dep_vars:
        print('Regressing: ' + dep_var)
        # Run two-way fixed effects regression
        Y = df_results[dep_var]
        X = sm2.add_constant(df_results[regressors])
        model_temp = PanelOLS(Y, X, entity_effects=True, time_effects=True, drop_absorbed=True) # Two-way fixed effects
        res_temp = model_temp.fit(cov_type='clustered', cluster_entity=True, cluster_time=True)

        # Obtain parameters
        params = res_temp.params.rename(index={'const': 'Constant'})

        # Obtain t-statistics using desired procedure
        if bBootstrap:
            # Obtain bootstrap standard errors and compute t-statistics
            tstats = res_temp.tstats.rename(index={'const': 'Constant'})
            cols = [val for sublist in [['FundNo', 'Date'], [dep_var], regressors] for val in sublist]
            p_values = panel_bootstrap(df_input[cols], tstats, num_iter=nBootstrap)
            p_values = p_values.fillna(99)
        else:
            # Obtain t-statistics based on double-clustered standard errors
            tstats = res_temp.tstats.rename(index={'const': 'Constant'})

        # Compute Kendall's tau between fitted and observed values
        df_compare = pd.merge(res_temp.model.dependent.dataframe, res_temp.fitted_values, left_index=True, right_index=True)
        df_compare = df_compare.rename(columns={dep_var:'y_obs', 'fitted_values': 'y_fit'})
        kendalls_tau = df_compare[['y_obs', 'y_fit']].corr(method='kendall').iloc[0,1]

        # Combine output and append to dataframe
        tstats = tstats.fillna(0)
        if bBootstrap:
            output_merged = pd.concat([params, p_values], axis=1).stack().reset_index(1, drop=True).to_frame(dep_var)
        else:
            output_merged = pd.concat([params, tstats], axis=1).stack().reset_index(1, drop=True).to_frame(dep_var)

        output_merged = output_merged[dep_var].append(pd.DataFrame([res_temp.rsquared, res_temp.nobs, kendalls_tau]))
        output_merged = output_merged.rename(index={0: 'R^2', 1: 'Obs.', 2: 'Kendalls Tau'})
        output_merged = output_merged.rename(columns={list(output_merged)[0]: dep_var})
        df_regression_output = pd.concat([df_regression_output, output_merged], axis=1)

        # Obtain residuals and append to dataframe
        resids = res_temp.resids.to_frame(dep_var)
        df_residuals = pd.concat([df_residuals, resids], axis=1)

    # Write regression output to csv file
    if bAbs:
        if bBootstrap:
            df_regression_output.to_csv(RESULTS_REGRESSION_ABS_BOOTSTRAP_FILENAME, index=True)
            df_residuals.to_csv(RESULTS_RESIDUALS_ABS_BOOTSTRAP_FILENAME, index=True)
        else:
            df_regression_output.to_csv(RESULTS_REGRESSION_ABS_FILENAME, index=True)
            df_residuals.to_csv(RESULTS_RESIDUALS_ABS_FILENAME, index=True)
    else:
        if bBootstrap:
            df_regression_output.to_csv(RESULTS_REGRESSION_REL_BOOTSTRAP_FILENAME, index=True)
            df_residuals.to_csv(RESULTS_RESIDUALS_REL_BOOTSTRAP_FILENAME, index=True)
        else:
            df_regression_output.to_csv(RESULTS_REGRESSION_REL_FILENAME, index=True)
            df_residuals.to_csv(RESULTS_RESIDUALS_REL_FILENAME, index=True)


if __name__ == '__main__':
    print('Start time: ' + time.strftime('%H:%M:%S'))
    start = time.time()
    for run in runs:
        # Load settings
        SCORES_TF_IDF = run['SCORES_TF_IDF']
        SURPRISE_REGRESSION = run['SURPRISE_REGRESSION']
        SINGLE_FUND_ONLY = run['SINGLE_FUND_ONLY']
        CRISIS_SAMPLE = run['CRISIS_SAMPLE']
        SUMMARIZATION_SCORES = run['SUMMARIZATION_SCORES']

        # Print settings
        print('TF IDF: ' + str(SCORES_TF_IDF))
        print('Relative regression: ' + str(SURPRISE_REGRESSION))
        print('Single Fund: ' + str(SINGLE_FUND_ONLY))
        print('Summarization: ' + str(SUMMARIZATION_SCORES))
        print('Crisis Sample: ' + str(CRISIS_SAMPLE))

        # Set paths
        if not SCORES_TF_IDF:
            PARENT_PATH = 'proportional/'
        else:
            PARENT_PATH = 'TF_IDF/'

        if SUMMARIZATION_SCORES:
            PARENT_PATH = PARENT_PATH + 'summarization/'

        if SINGLE_FUND_ONLY:
            PATH_RESULTS = PARENT_PATH + 'single_funds/'
        elif CRISIS_SAMPLE:
            PATH_RESULTS = PARENT_PATH + 'crisis_subsample/'
        else:
            PATH_RESULTS = PARENT_PATH + 'baseline/'

        # Results filenames
        RESULTS_WINSORIZED_FILENAME = PATH_RESULTS + 'results_winsorized.csv'
        RESULTS_COMPLETE_FILENAME = PATH_RESULTS + 'results_complete.csv'

        # Output filenames
        RESULTS_REGRESSION_ABS_FILENAME = PATH_RESULTS + 'absolute_regression_output.csv'
        RESULTS_REGRESSION_REL_FILENAME = PATH_RESULTS + 'relative_regression_output.csv'

        RESULTS_RESIDUALS_ABS_FILENAME = PATH_RESULTS + 'absolute_regression_residuals.csv'
        RESULTS_RESIDUALS_REL_FILENAME = PATH_RESULTS + 'relative_regression_residuals.csv'

        RESULTS_REGRESSION_ABS_BOOTSTRAP_FILENAME = PATH_RESULTS + 'absolute_regression_bootstrap_output.csv'
        RESULTS_REGRESSION_REL_BOOTSTRAP_FILENAME = PATH_RESULTS + 'relative_regression_bootstrap_output.csv'

        RESULTS_RESIDUALS_ABS_BOOTSTRAP_FILENAME = PATH_RESULTS + 'absolute_regression_bootstrap_residuals.csv'
        RESULTS_RESIDUALS_REL_BOOTSTRAP_FILENAME = PATH_RESULTS + 'relative_regression_bootstrap_residuals.csv'

        print('Using the following input file: ' + RESULTS_WINSORIZED_FILENAME + '\n')

        run_baseline_regressions(input_filename=RESULTS_WINSORIZED_FILENAME, bBootstrap=bBootstrap, \
                                 bRelative=SURPRISE_REGRESSION, nBootstrap=nBootstrap)

    print('Total running time: ' + str(round(time.time() - start)))