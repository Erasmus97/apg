#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 20 10:22:54 2020

@author: W.F. van Driel, F.C. Neefjes
"""

from data_extraction.edgar_scraping import ShareholderLetter
from general.file import File, FileList
from general.wordlist import WordLists
from simulation.trading import Trading

import data_analysis.text_analysis as text_analysis
import simulation.strategy as strategy

import data_extraction.download as download
import general.utils as utils

##############################################################################
# Message from the authors:
#
# For information about our code and how to set the right settings in main.py
# please read the README in the GitLab directory.
#
# The main provides a pipeline for obtaining the shareholder letters/summaries,
# textual scores and the portfolio allocation strategy.
#
# For the regression analysis, see the package data_analysis
##############################################################################

# Create constants
DOWNLOAD_LETTERS = False
MAKE_SUMMARIES = False
MERGE_CSV = False
GET_SCORES_LETTERS = False
GET_SCORES_SUMMARIES = False
PROPORTIONAL = False
CROSS_TRIMMING = False
TRADING = [
    {'dep_var': 'Volatility', 'notext': False, 'copula': False, 'save': True},
    {'dep_var': 'Future Return', 'notext': False, 'copula': False, 'save': True},
    {'dep_var': 'Volatility', 'notext': True, 'copula': False, 'save': True},
    {'dep_var': 'Future Return', 'notext': True, 'copula': False, 'save': True},
]

# Create basic instances
letter = ShareholderLetter(utils)
words = WordLists()

# USER INPUT
START_YEAR = 2006
END_YEAR = 2016
BEGIN_QUARTER = 1
END_QUARTER = 4


# Pipeline 1.1: Download letters from the SEC
if DOWNLOAD_LETTERS:
    year = START_YEAR if START_YEAR == END_YEAR else str(START_YEAR) + '-' + str(END_YEAR)[-2:]
    qtr = BEGIN_QUARTER if BEGIN_QUARTER == END_QUARTER else str(BEGIN_QUARTER) + '-' + str(END_QUARTER)
    file_name = f'{year}Q{qtr}'
    download.download_forms(letter, csv_id=file_name, begin_year=START_YEAR, end_year=END_YEAR,
                            begin_quarter=BEGIN_QUARTER, end_quarter=END_QUARTER)


# Pipeline 1.2: Create summaries based on letters
if MAKE_SUMMARIES:
    # Get paths to shareholder letters
    print('Obtaining paths to shareholder letters...')
    files = utils.get_path_files(utils.LETTERS_PATH, '.txt')
    files = [f for f in files]
    files.sort()

    files = [f for f in files]

    # Create file objects and summaries
    print('Creating summaries...')
    for path in files:
        file = File(path)

        file.remove_words()
        file.summarize(to_file=True)

        print('\rSummarized: {}'.format(path), end='')

        # Save memory
        del file
    print('Summaries are created!\n')


# Pipeline 2: Merge all CSV files
if MERGE_CSV:
    print('Merging CSV files...')
    utils.merge_csv(utils.LETTERS_PATH + 'CRSP_CIK.csv', utils.LETTERS_PATH, ext='.csv')
    print('Merge completed!\n')


# Pipeline 3: Trim outliers
if CROSS_TRIMMING:
    # Create objects for letters and add them to a list
    files = utils.get_path_files(utils.LETTERS_PATH + 'COPY', '.txt')
    files.sort()

    letter_list = FileList()

    start = utils.time.time()
    for path in files:
        # Make NEW instance
        file = File(path)
        letter_list.append(file)
    print(f'Created {len(letter_list)} file objects in {round(utils.time.time() - start, 3)} seconds')

    # Make instance for the analysis
    letter_analysis = text_analysis.LetterAnalysis(utils, words, letter_list)

    # Find letters to trim
    trim_letter = letter_list.find_trim(0.1)

    # Create objects for summaries and add them to a list
    files = utils.get_path_files(utils.SUMMARIES_PATH, '.txt')
    files.sort()

    # Create new file list instance
    summary_list = FileList()

    start = utils.time.time()
    for path in files:
        file = File(path, summary=True)
        summary_list.append(file)
    print(f'Created {len(summary_list)} file objects in {round(utils.time.time() - start, 3)} seconds')

    # Make object for analysis
    summary_analysis = text_analysis.SummaryAnalysis(utils, words, summary_list)

    # Find summaries to trim
    trim_summary = summary_list.find_trim(0.1)

    # Concatenate lists of letters and summaries to trim
    trim_set = trim_letter.union(trim_summary)

    # Pipeline 4: Get textual scores of the letters
    if GET_SCORES_LETTERS:
        # Trim files
        letter_list.trim(trim_set)

        letter_analysis.df = utils.pd.read_csv('../data/letters/CRSP_CIK_v3.csv')

        start = utils.time.time()
        if PROPORTIONAL:
            letter_analysis.get_scores(
                weight_scheme='proportional',
                to_dataframe=True,
                path='../data/letters/scores_prop_full_data.csv'
            )
            print(f'Calculated scores in  {round(utils.time.time() - start, 3)} seconds')
        else:
            letter_analysis.get_scores(
                weight_scheme='tf-idf',
                to_dataframe=True,
                path='../data/letters/scores_tf-idf_full_data.csv'
            )
            print(f'Calculated scores in  {round(utils.time.time() - start, 3)} seconds')

    # Pipeline 5: Get textual scores of the summaries
    if GET_SCORES_SUMMARIES:
        # Trim files
        summary_list.trim(trim_set)

        summary_analysis.df = utils.pd.read_csv(utils.LETTERS_PATH + 'CRSP_CIK_v3.csv')

        if PROPORTIONAL:
            start = utils.time.time()
            summary_analysis.get_scores(
                weight_scheme='proportional',
                to_dataframe=True,
                path='../data/summaries/summary_prop_full_data.csv'
            )
            print(f'Calculated scores in  {round(utils.time.time() - start, 3)} seconds')
        else:
            start = utils.time.time()
            summary_analysis.get_scores(
                weight_scheme='tf-idf',
                to_dataframe=True,
                path='../data/summaries/summary_tf-idf_full_data-idf.csv'
            )
            print(f'Calculated scores in  {round(utils.time.time() - start, 3)} seconds')

if not CROSS_TRIMMING:
    # Pipeline 3: Get textual scores of the letters
    if GET_SCORES_LETTERS:
        # Create objects for letters and add them to a list
        files = utils.get_path_files(utils.LETTERS_PATH + 'COPY', '.txt')
        files = [f for f in files]
        files.sort()

        file_list = FileList()

        start = utils.time.time()
        for path in files:
            # Make NEW instance
            file = File(path)
            file_list.append(file)
        print(f'Created file objects in {round(utils.time.time() - start, 3)} seconds')

        # Make instance for the analysis
        letter_analysis = text_analysis.LetterAnalysis(utils, words, file_list)

        # Winsorize files
        file_list.winsorize(perc=0.1)

        letter_analysis.df = utils.pd.read_csv('../data/letters/CRSP_CIK_v3.csv')

        if PROPORTIONAL:
            start = utils.time.time()
            letter_analysis.get_scores(
                weight_scheme='proportional',
                to_dataframe=True,
                path='../data/letters/scores_prop_full_data.csv'
            )
            print(f'Calculated scores in  {round(utils.time.time() - start, 3)} seconds')
        else:
            start = utils.time.time()
            letter_analysis.get_scores(
                weight_scheme='tf-idf',
                to_dataframe=True,
                path='../data/letters/scores_tf-idf_full_data.csv'
            )
            print(f'Calculated scores in  {round(utils.time.time() - start, 3)} seconds')

    # Pipeline 4: Get textual scores of the summaries
    if GET_SCORES_SUMMARIES:
        # Get summary files
        files = utils.get_path_files(utils.SUMMARIES_PATH, '.txt')
        files = [f for f in files]
        files.sort()

        # Create new file list instance
        file_list = FileList()

        start = utils.time.time()
        for path in files:
            file = File(path, summary=True)
            file_list.append(file)
        print(f'Created {len(file_list)} file objects in {round(utils.time.time() - start, 3)} seconds')
        print('Get scores of summaries...')

        # Make object for analysis
        summary_analysis = text_analysis.SummaryAnalysis(utils, words, file_list)

        # Winsorize files
        file_list.winsorize(perc=0.1)

        summary_analysis.df = utils.pd.read_csv(utils.LETTERS_PATH + 'CRSP_CIK_v3.csv')

        if PROPORTIONAL:
            start = utils.time.time()
            summary_analysis.get_scores(
                weight_scheme='proportional',
                to_dataframe=True,
                path='../data/summaries/summary_prop_full_data.csv'
            )
            print(f'Calculated scores in  {round(utils.time.time() - start, 3)} seconds')
        else:
            start = utils.time.time()
            summary_analysis.get_scores(
                weight_scheme='tf-idf',
                to_dataframe=True,
                path='../data/summaries/summary_tf-idf_full_data-idf.csv'
            )
            print(f'Calculated scores in  {round(utils.time.time() - start, 3)} seconds')

# Simulate trading strategies
for TRADE in TRADING:
        print(f"Starting simulation for {TRADE['dep_var']} strategies. Copula is {TRADE['copula']} and notext is {TRADE['notext']}...")
        strategies = []
        summary = []
        if TRADE['dep_var'] == 'Volatility':
            abr = 'Vol'
            if TRADE['notext']:
                #strategies.append(strategy.BuyHoldSellVol('Control' + abr))
                strategies.append(strategy.QuintileLongShort('ControlLS' + abr, dependent_var = TRADE['dep_var']))
            else:    
                #strategies.append(strategy.BuyHoldSellVol('Letter' + abr))
                strategies.append(strategy.QuintileLongShort('LetterLS' + abr, dependent_var = TRADE['dep_var']))
                #summary.append(strategy.BuyHoldSellVol('Summary' + abr))
                summary.append(strategy.QuintileLongShort('SummaryLS' + abr, dependent_var = TRADE['dep_var']))
        elif TRADE['dep_var'] == 'Future Return':
            abr = 'Ret'
            if TRADE['notext']:
                #strategies.append(strategy.BuyHoldSell('Control' + abr))
                strategies.append(strategy.QuintileLongShort('ControlLS' + abr, dependent_var = TRADE['dep_var']))
            else:    
                #strategies.append(strategy.BuyHoldSell('Letter' + abr))
                strategies.append(strategy.QuintileLongShort('LetterLS' + abr, dependent_var = TRADE['dep_var']))
                #summary.append(strategy.BuyHoldSell('Summary' + abr)) 
                summary.append(strategy.QuintileLongShort('SummaryLS' + abr, dependent_var = TRADE['dep_var']))  
        for q in range(1, 6):
            if TRADE['notext']:
                strategies.append(strategy.Quintile('Control' + abr + '_Q' + str(q), quintile = q, dependent_var = TRADE['dep_var']))
            else:    
                strategies.append(strategy.Quintile('Letter' + abr + '_Q' + str(q), quintile = q, dependent_var = TRADE['dep_var']))
                summary.append(strategy.Quintile('Summary' + abr + '_Q' + str(q), quintile = q, dependent_var = TRADE['dep_var']))
        if not TRADE['notext']:
            strategies.append(strategy.OneOverN('1/N'))
        
        trading = Trading(utils, dependent_var=TRADE['dep_var'], strategies=strategies, summary=summary, copula=TRADE['copula'], notext=TRADE['notext'])
        trading.start(save=TRADE['save'])
