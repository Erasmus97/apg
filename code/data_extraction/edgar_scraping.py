#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 22 10:22:03 2020

@author: W.F. van Driel
"""

import re
import os

from bs4 import BeautifulSoup
from datetime import datetime
from os.path import isfile


class ShareholderLetter:

    STATUS_SSOUP = 'Soup successfully extracted letter in file.'
    STATUS_FSOUP = 'Soup did not find letter in file.'
    STATUS_SFILE = 'Letter has been extracted successfully using Python file scraping.'
    STATUS_FFILE = 'Letter has not been found in file with Python scraping.'

    def __init__(self, utils, arg=None):
        """
        The constructor of this class takes the following arguments:
        :param arg: a relative path that contains the shareholders letters (default None)
        :type arg: str
        """
        self.__utils = utils

        if isinstance(arg, str):
            self.__path = arg

        self.__to_log(datetime.now().strftime("%d/%m/%Y %H:%M:%S"), None)

    @property
    def path(self):
        return self.__path

    @path.setter
    def path(self, value):
        self.__path = value

    def __update(self, letter, to_file, file_name):
        """
        This method sends an update to the log file and returns the list.
        :param letter: The shareholder letter (could be empty if not found)
        :type letter: list
        :param to_file: If True, the letter is written to a file
        :type to_file: bool
        :param file_name: The path in which the current shareholder letter is looked for
        :type file_name: str
        :return: The shareholder letter (could be empty if not found)
        :rtype: list
        """
        if letter:
            self.__to_log(file_name, ShareholderLetter.STATUS_SSOUP)
            if to_file:
                self.letter_to_file(letter, file_name)
        else:
            self.__to_log(file_name, ShareholderLetter.STATUS_FSOUP)
        return letter

    def __scrape_txt(self, file_name):
        """
        This method scrapes a file, looking for the shareholder letter.

        :param file_name: path to the file in which we are interested
        :type file_name: str
        """
        # Re-do shit
        if isinstance(file_name, str):
            file = open(file_name, 'r')
            file = file.readlines()
        else:
            file = file_name

        # Get letter out of 10-K using strings in lists
        start = False
        strings = []
        for line in file:
            # Look for starting words of the letter
            if ('DEAR' in line.upper() and any(ext in line.upper() for ext in self.__utils.STARTERS)) or (
                    'DEAR FELLOW' in line.upper()):
                start = True

            if start:
                strings.append(line)

            # Look for closing words in the letter
            if start:
                if any(ext in line.upper() for ext in self.__utils.ENDINGS):
                    start = False
                    strings.append('\n')

                elif any(ext in line.upper() for ext in self.__utils.DISCLAIMERS):
                    start = False
                    strings.append('\n')

                # A bit risky, but if a line has at most 5 words AND someone's title is in there
                # then we assume it is the end of the letter
                elif len(line.split()) <= 5:
                    if any(ext in line.upper().split() for ext in self.__utils.TITLES_SHORT) or \
                            (any(ext in line.upper() for ext in self.__utils.TITLES_FULL)):
                        start = False
                        strings.append('\n')

        # Clean HTML tags from text if we have found any text
        if strings:
            # Keep track of updated strings
            copy_strings = []
            tab = False

            for i in range(len(strings) - 1):
                string = strings[i]

                # Remove table
                if '<TABLE>' in string.upper():
                    tab = True
                if not tab:
                    # Remove opening tags (with and without attributes) and closing tags
                    string = re.sub('<[^>]+>', '', string)
                    string = re.sub('&#[^;]+;', '', string)
                    string = string.replace('&nbsp;', '')

                    # Remove remaining tags that do not follow structure
                    if '<' in string:
                        string = ' '.join(string.split('<')[:-1])
                    if '>' in string:
                        string = ' '.join(string.split('>')[1:])

                    # Remove continued, photos or any number
                    string = string.replace('(CONTINUED)', '')
                    string = re.sub('\[PHOTO[^\]]*\]', '', string)
                    string = ' '.join(s for s in string.split() if not any(c.isdigit() for c in s))
                    copy_strings.append(string)
                if '</TABLE>' in string.upper():
                    tab = False

            # Write letter to a file if it has content
            return copy_strings

    def __scrape_html(self, soup):
        """
        This method scrapes a BeautifulSoup object and searches for the shareholder letter.
        :param soup: the BeautifulSoup object that contains a 10-K filing
        :type soup: BeautifulSoup
        """
        start = False
        strings = []

        # Get letter out of 10-K using HTML scraping
        for tag in soup.find_all(['p', 'img']):
            string = tag.string

            if start:
                if '\n' in str(tag):
                    string = str(tag)
                    # Split tag on closing char of opening part
                    string = string.split('>')

                    # Get all elements that has closing part of tag
                    string = [i for i in string if '</' in i]
                    string = ' '.join(string)

                    # Split tag on opening char of closing part
                    string = string.split('</')[0]

            # We want to stop the letter when we reach an image
            try:
                src = tag['src']
                start = False
            except KeyError:
                pass

            # Valid paragraph tag
            if string is not None:
                # Look for starting words of the letter
                if ('DEAR' in string.upper() and any(ext in string.upper() for ext in self.__utils.STARTERS)) or (
                        'DEAR FELLOW' in string.upper()):
                    start = True

                if start:
                    strings.append(string)

                # Look for closing words in the letter
                if start:
                    if any(ext in string.upper() for ext in self.__utils.ENDINGS):
                        start = False
                        strings.append('\n')

                    elif any(ext in string.upper() for ext in self.__utils.DISCLAIMERS):
                        start = False
                        strings.append('\n')

                    # A bit risky, but if a line has at most 5 words AND someone's title is in there
                    # then we assume it is the end of the letter
                    elif len(string.split()) <= 5:
                        if any(ext in string.upper().split() for ext in self.__utils.TITLES_SHORT) or \
                                (any(ext in string.upper() for ext in self.__utils.TITLES_FULL)):
                            start = False
                            strings.append('\n')

        # Write letters to file if results are found
        return strings

    def letter_to_file(self, letters, path=None):
        """
        A list which contains every sentence in the cleaned shareholder letter

        :param letters: a list of lines in the letter
        :type letters: list
        :param path: the path to the original document which contained the letter
        :type path: str
        """
        with self.__utils.io.open(path, 'w') as f:
            for letter in letters:
                try:
                    f.write(f"{letter}\n")  # Windows cannot handle all characters
                except:
                    pass

    def __to_log(self, file_name, status):
        try:
            # Find if log_file exists
            attr = 'a' if isfile(self.__utils.LOG_PATH) else 'w'

            # Write or append to file
            with self.__utils.io.open(self.__utils.LOG_PATH, attr) as f:
                # Only write date
                if status is None:
                    f.write('-'*50 + '\n')
                    f.write(f'Date: {file_name}\n')
                else:
                    f.write(f'File: {file_name}\n')
                    f.write(f'Status: {status}\n\n')
        except AttributeError:
            pass

    def _remove_log(self):
        """ This method deletes the log file, which keeps track of our scraping activities. """
        if self.__utils.os.path.isfile(self.__utils.LOG_PATH):
            os.remove(self.__utils.LOG_PATH)

    def _read_log(self):
        try:
            file = open(self.__utils.LOG_PATH, 'r')
            for line in file:
                print(line)
        except:
            pass

    def get_letters(self, file=None, to_file=False):
        """
        This method obtains all letters from the path that has been given as attribute, or from a file that is given as
        argument.
        :param file: the file that needs to be scraped. If it is not given, then all files from the current path onwards
        will be scraped (default None)
        :type file: list
        :param to_file: if True, the letters are written to a file with the same name; else, the letters will be returned
        :type to_file: list
        """
        # NOTE: WE ALWAYS RETURN THE LETTERS, WHICH FUNCTIONS AS A BOOLEAN FOR OTHER METHODS (see download.py)
        # File is given either by path or as a list
        if file is not None:
            # Check if file is path
            if isinstance(file, str) and isfile(file):
                file_obj = open(file, 'r')
                try:
                    soup = BeautifulSoup(file_obj.read(), 'html.parser')
                    letter = self.__scrape_html(soup)
                    if not letter:
                        letter = self.__scrape_txt(file)
                except:
                    letter = self.__scrape_txt(file)

                # Write letter to file if needed and update log
                return self.__update(letter, to_file, file)

            # Check if file is a list
            elif isinstance(file, list):
                try:
                    soup = BeautifulSoup(file, 'html.parser')
                    letter = self.__scrape_html(soup)
                    if not letter:
                        letter = self.__scrape_txt(file)
                except:
                    letter = self.__scrape_txt(file)

                # Write letter to file if needed and update log
                return self.__update(letter, to_file, 'UNKNOWN')

            # Invalid argument
            else:
                raise ValueError('file must either be of type `str` or `list`')

        # We are going to look for all files from current directory onwards
        else:
            files = self.__utils.get_path_files(self.__path, ext='.txt')
            letters = []

            # Loop over each file
            for file_name in files:
                # Remove our old-made letters and shitty files
                if 'letter' in file_name or '.DS_Store' in file_name:
                    files.remove(file_name)
                    os.remove(file_name)
                    continue

                # Open file
                file = open(file_name, 'r')

                print(f'Parsing file: {file_name}')
    
                # If soup fails, then it is not a HTML file that we can read
                # so we need to open the file manually
                try:
                    soup = BeautifulSoup(file.read(), 'html.parser')
                    letter = self.__scrape_html(soup)
                except:
                    letter = self.__scrape_txt(file_name)

                if not to_file:
                    letters.append(letter)

                # Write letter to file if needed and update log
                self.__update(letter, to_file, file_name)

            if not to_file:
                return letters




