import os
import time

import data_extraction.EDGAR_Forms as EDGAR_Forms
import data_extraction.EDGAR_Pac as EDGAR_Pac
import general.utils as utils
import pandas as pd
import numpy as np

################################################
# CONSTANTS
################################################

PARM_FORMS = EDGAR_Forms.N_CSR  # or, for example, PARM_FORMS = ['8-K', '8-K/A']

# Change the file pointer below to reflect your location for the log file
#    (directory must already exist)
PARM_LOGFILE = (r'../data/EDGAR/Test/Log_Files/' +
                r'EDGAR_Download_FORM-X_LogFile_' +
                str(2005) + '-' + str(2014) + '.txt')

# EDGAR parameter
PARM_EDGARPREFIX = 'https://www.sec.gov/Archives/'

CIK_downloaded = []  # To keep track of the CIKs for which a form is downloaded

################################################
# LAMBDA FUNCTIONS
################################################

quarter = lambda m: m // 4 + 1


################################################
# METHODS
################################################


def in_database(df, cik, date, contract_id=None):
    """ Check if cik and date are in our database """
    date = str(date)
    year_item, qtr_item = int(date[:4]), quarter(int(date[4:6]))

    # Get subset of dataframe
    if contract_id is None:
        idx = df[(df['CIK'] == cik) & (df['Quarter Date'] == f'{year_item}Q{qtr_item}')].index
    else:
        idx = df[(df['CIK'] == cik) & (df['Quarter Date'] == f'{year_item}Q{qtr_item}') & (df['Class ID'] == contract_id)].index

    # Check if we have any hits or not
    return idx


def download_forms(shareholder, csv_id, begin_year=2006, end_year=2007, begin_quarter=1, end_quarter=4, progress_bar=True):
    """
    This method downloads each year/quarter master.idx and saves the record for requested forms
    :param shareholder: object that can identify the shareholder letter in a document
    :type shareholder: Shareholder
    :param csv_id: version of the CSV file
    :type csv_id: str
    :param begin_year: year from when onwards we start scraping
    :type begin_year: int
    :param end_year: last year of scraping
    :type end_year: int
    :param begin_quarter: first quarter of the year that we take into account for scraping
    :type begin_quarter: int
    :param end_quarter: last quarter of the year that we take into account for scraping
    :type end_quarter: int
    :param progress_bar: if True, a progress bar from Utils will be displayed
    :type progress_bar: bool
    """

    # Open CSV file
    df_crsp = pd.read_csv(utils.CRSP_DATA_PATH)
    csv_path = utils.LETTERS_PATH + f'CRSP_CIK_{csv_id}.csv'
    letters_path = utils.LETTERS_PATH + 'Test/'
    print('Letters will be saved at: {:>20}'.format(letters_path))
    print('Copy of CSV file will be saved as: {:>20}'.format(csv_path))

    # Add new columns if they are not there yet
    for col in ['series-id', 'contract-id', 'file-name', 'report-date', 'file-date']:
        if col not in df_crsp.columns:
            df_crsp[col] = np.nan

    n_tot, n_errs = 0, 0
    for year in range(begin_year, end_year + 1):
        for qtr in range(begin_quarter, end_quarter + 1):
            startloop = time.process_time()
            n_qtr = 0
            file_count = {}

            # Setup output path
            path = '{0}{1}/QTR{2}/'.format(letters_path, str(year), str(qtr))
            if not os.path.exists(path):
                os.makedirs(path)
                print('Path: {0} created'.format(path))

            # Get the master index
            masterindex = EDGAR_Pac.download_masterindex(year, qtr, True)

            # Check if master index is not empty
            if masterindex:
                len_m = len(masterindex)
                for m in range(len_m):
                    item = masterindex[m]

                    if np.mod(m, 100000) == 0:
                        # Save CSV file --> as a copy!!
                        df_crsp.to_csv(csv_path)
                        print('Updated CSV file!')

                    # Display progress bar
                    if progress_bar and (np.mod(m, 1000) == 0 or m == 0):
                        utils.update_progress(m / len_m)

                    # Check if CIK is in our sample of mutual funds
                    # and check if it corresponds with the date
                    idx = in_database(df_crsp, item.cik, item.filingdate)

                    if not idx.empty:
                        # Kill time when server not available
                        while EDGAR_Pac.edgar_server_not_available(True):
                            pass

                        # Check if form of interest is available
                        if item.form in PARM_FORMS:
                            if item.cik not in CIK_downloaded:
                                CIK_downloaded.append(item.cik)
                            n_qtr += 1

                            # Keep track of filings and identify duplicates
                            fid = str(item.cik) + str(item.filingdate) + item.form
                            if fid in file_count:
                                file_count[fid] += 1
                            else:
                                file_count[fid] = 1

                            # Setup EDGAR URL and output file name
                            url = PARM_EDGARPREFIX + item.path
                            file_name = (path + str(item.filingdate) + '_' + str(item.cik) + '_' +
                                         str(file_count[fid]) + '.txt')

                            # Save file
                            return_url = utils.download_to_file(url, file_name)

                            # Filter out an anomalous file that raises a 404 Error
                            if file_name == '../data/letters/COPY/2014/QTR3/20140811_778202_1.txt':
                                continue

                            # Calls our method and immediately checks if a letter is found
                            letter = shareholder.get_letters(file=file_name, to_file=False)
                            if letter:
                                print('Found shareholder letter and is written to file')

                                # Save tags to CSV data file
                                tags = ['contract-id', 'series-id', 'report-date', 'file-date']
                                dict_tags = utils.tag_search(tags, file_name)

                                # Write letter to original file
                                shareholder.letter_to_file(letter, file_name)

                                # Check what contract-ID is in CSV
                                matched_contract_id = False
                                for id_ in dict_tags['contract-id']:
                                    # Find all rows that match with our contract-id
                                    contract_idx = in_database(df_crsp, item.cik, item.filingdate, id_)

                                    # If we at least matched one contract-id, then we save the file
                                    if not contract_idx.empty:
                                        matched_contract_id = True

                                    # Loop over the rows that correspond to the contract-id
                                    for i in contract_idx:
                                        for tag in tags:
                                            # Set the contract-id to the one that we now have fixed
                                            if tag == 'contract-id':
                                                df_crsp.loc[i, tag] = id_
                                                continue

                                            # Take the values that we have found in the letter
                                            value = dict_tags[tag]
                                            if not value:
                                                continue
                                            elif len(value) == 1:
                                                value = value[0]
                                            else:
                                                value = ','.join(value)
                                            df_crsp.loc[i, tag] = value
                                        df_crsp.loc[i, 'file-name'] = file_name

                                # Remove file if not contract-id is matched
                                if not matched_contract_id:
                                    os.remove(file_name)

                            # Remove file if the shareholder letter is not found
                            else:
                                os.remove(file_name)

                            if return_url:
                                n_errs += 1
                            n_tot += 1
                            # time.sleep(1)  # Space out requests
                        else:
                            continue
            print(str(year) + ':' + str(qtr) + ' -> {0:,}'.format(n_qtr) + ' downloads completed.  Time = ' +
                  time.strftime('%H:%M:%S', time.gmtime(time.process_time() - startloop)) +
                  ' | ' + time.strftime('%c'))

        # Save CSV file --> as a copy!!
        df_crsp.to_csv(csv_path)
        print('Updated CSV file!')

    print('{0:,} total forms downloaded.'.format(n_tot))
