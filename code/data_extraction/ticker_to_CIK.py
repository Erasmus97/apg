'''
Code to get CIK code using ticker symbol
'''
import re
from requests import get
from time import time

# -----------------------
# Preliminaries
# -----------------------

URL = 'http://www.sec.gov/cgi-bin/browse-edgar?CIK={}&Find=Search&owner=exclude&action=getcompany'
CIK_RE = re.compile(r'.*CIK=(\d{10}).*')
CONTRACT_ID_RE = re.compile(r'.*C(\d{9}).*')

# Example tickers
TICKERS = ['CABNX', 'APITX', 'GFIZX']

#
# * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * +

def get_CIK_txt(tickers):
    '''Writes dictionary linking SEC CIK to fund ticker to .txt file'''

    cik_dict = {}
    for ticker in tickers:
        results = CIK_RE.findall(get(URL.format(ticker)).content.decode('utf-8'))
        if len(results):
            cik_dict[str(ticker).lower()] = str(results[0])
    with open('tickers.txt', 'w') as file:
        for ticker, cik in cik_dict.items():
            print(f'{ticker}: {cik.lstrip("0")}', file=file)
    file.close()


def get_CIK(ticker):
    '''Returns SEC CIK given a single fund ticker'''

    CIK = CIK_RE.findall(get(URL.format(ticker)).content.decode('utf-8'))
    if len(CIK):
        return CIK
    else:
        return float('NaN')


def get_CIK_multiple(tickers):
    '''Returns dictionary linking SEC CIK to fund ticker'''

    t0 = time()  # Starting time
    cik_dict = {}
    for ticker, counter in zip(tickers, range(1, len(tickers)+1)):
        results = CIK_RE.findall(get(URL.format(ticker)).content.decode('utf-8'))  # Obtain CIK from website
        if len(results):
            cik_dict[str(ticker)] = str(results[0].lstrip("0"))  # Convert to string and remove leading zeros
        else:
            cik_dict[str(ticker)] = float('NaN')  # Set equal to NaN if no hit
        if counter % 100 == 0:
            t_now = time() - t0
            print(str(t_now) + '    ' + str(counter) + '\n')  # Timer
    return cik_dict

def get_CONTRACT_ID(tickers):
    '''Returns dictionary linking SEC CONTRACT_ID to fund ticker'''

    t0 = time()  # Starting time
    contract_id_dict = {}
    for ticker, counter in zip(tickers, range(1, len(tickers) + 1)):
        results = CONTRACT_ID_RE.findall(get(URL.format(ticker)).content.decode('utf-8'))  # Obtain contract-ID from website
        if len(results):
            contract_id_dict[str(ticker)] = "C" + str(results[0])
        else:
            contract_id_dict[str(ticker)] = float('NaN')  # Set equal to NaN if no hit
        if counter % 100 == 0:
            t_now = time() - t0
            print(str(t_now) + '    ' + str(counter) + '\n')  # Timer
    return contract_id_dict

def get_CIK_AND_CONTRACT_ID(tickers):
    '''Returns dictionary linking SEC CIK and CONTRACT_ID to fund ticker'''

    t0 = time()  # Starting time
    dict = {}
    for ticker, counter in zip(tickers, range(1, len(tickers) + 1)):
        cik = CIK_RE.findall(
            get(URL.format(ticker)).content.decode('utf-8'))  # Obtain contract-ID from website
        if len(cik):
            cik = str(cik[0].lstrip("0"))  # Convert to string and remove leading zeros
        else:
            cik = float('NaN')  # Set equal to NaN if no hit
        contract_id = CONTRACT_ID_RE.findall(
            get(URL.format(ticker)).content.decode('utf-8'))  # Obtain contract-ID from website
        if len(contract_id):
            contract_id = "C" + str(contract_id[0])
        else:
            contract_id = float('NaN')  # Set equal to NaN if no hit
        dict[str(ticker)] = [cik,contract_id] # make list with [CIK, contract-ID]
        if counter % 50 == 0:
            t_now = time() - t0
            print(str(t_now) + '    ' + str(counter) + '\n')  # Timer
    return dict

if __name__ == '__main__':
    get_CIK('AAPL')
