import sys
import pandas as pd
import numpy as np
import ticker_to_CIK
from datetime import timedelta
import os

sys.path.append('../../code/general/')  # Add directory of folder to path

# -----------------------
# User defined parameters
# -----------------------

# -----------------------Settings-----------------------
START_DATE_DAILY = '2005-07-01'  # Y-m-d
END_DATE_DAILY = '2017-07-31'  # Y-m-d

SINGLE_FUND_ONLY = False     # Boolean: if TRUE, only consider fund-specific shareholder letters
SCORES_TF_IDF = True         # Boolean: if TRUE, scores are computed using TF-IDF
CRISIS_SAMPLE = False        # Boolean: if TRUE, consider crisis subsample

# -----------------------Paths-----------------------
if SINGLE_FUND_ONLY:
    PATH_RESULTS = '../../results/single_funds/'
    RESULTS_FILENAME = PATH_RESULTS + 'scores_tf-idf.csv'
    RESULTS_MERGED_FILENAME = PATH_RESULTS + 'scores_tf-idf_merged.csv'
elif SCORES_TF_IDF:
    PATH_RESULTS = '../../results/TF_IDF/'
    RESULTS_FILENAME = PATH_RESULTS + 'scores_tf-idf.csv'
    RESULTS_MERGED_FILENAME = PATH_RESULTS + 'scores_tf-idf_merged.csv'
elif CRISIS_SAMPLE:
    PATH_RESULTS = '../../results/crisis_subsample/'
    RESULTS_FILENAME = PATH_RESULTS + 'scores_tf-idf.csv'
    RESULTS_MERGED_FILENAME = PATH_RESULTS + 'scores_tf-idf_merged.csv'
else:
    PATH_RESULTS = '../../results/proportional/'
    RESULTS_FILENAME = PATH_RESULTS + 'scores_proportional.csv'
    RESULTS_MERGED_FILENAME = PATH_RESULTS + 'scores_proportional_merged.csv'

PATH_DATA = '../../data/'
PATH_CRSP = '../../data/CRSP/'          # Set relative paths for CRSP data files
PATH_FRENCH = '../../data/French/'      # Set relative paths for Kenneth French data files
PATH_EDGAR = '../../data/EDGAR/'

# -----------------------CRSP raw filenames-----------------------
CRSP_RETURNS_FILENAME = PATH_CRSP + 'Raw/' + 'CRSP_returns_monthly.csv'
CRSM_SUMMARY_FILENAME = PATH_CRSP + 'Raw/' + 'CRSP_summary.csv'
CRSP_RETURNS_DAILY_FILENAME = PATH_CRSP + 'Raw/' + 'CRSP_returns_daily.csv'

# -----------------------CRSP processed filenames-----------------------
CRSP_MERGED_FILENAME = PATH_CRSP + 'merged.csv'
CRSP_CLEANED_FILENAME = PATH_CRSP + 'cleaned.csv'
CRSP_CIK_FILENAME = PATH_CRSP + 'CIK_merged_monthly.csv'
TICKER_FILENAME = PATH_CRSP + 'tickers.txt'
COMBINED_FILENAME = PATH_CRSP + 'returns_factors_combined.csv'
FUND_PERF_MEASURES_FILENAME = PATH_CRSP + 'fund_performance_measures.csv'
FUND_PERF_MEASURES_AUGMENTED_FILENAME = PATH_CRSP + 'fund_performance_measures_augmented.csv'

# -----------------------EDGAR filenames and paths-----------------------
SCRAPED_TICKERS_FILENAME = PATH_EDGAR + 'scraped_tickers.csv'
PATH_SEC_LINKING_TABLES = '../../data/EDGAR/Linking_Tables/' # Set relative paths for EDGAR linking tables files

# -----------------------Fama-French filenames-----------------------
FF_FACTORS_DAILY_FILENAME = PATH_FRENCH + 'F-F_Research_Data_Factors_daily.csv'

# -----------------------Ouput filenames-----------------------
FUNDS_COUNTER_FILENAME = PATH_RESULTS + 'funds_counter.txt'

#
# * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * +

def load_monthly_returns():
    """Converts monthly CRSP fund returns data."""
    df_returns = pd.read_csv(CRSP_RETURNS_FILENAME)
    df_returns = df_returns.rename(columns={'caldt': 'Date', 'crsp_fundno': 'FundNo'})
    df_returns['Date'] = pd.to_datetime(df_returns['Date'], format='%Y%m%d')

    # Process returns data
    # Convert TNA column to type numeric
    for col in ["mtna", "mret", "mnav"]:
        df_returns[col] = pd.to_numeric(df_returns[col], errors='coerce')

    # Replace missing data, denoted by -99
    df_returns = df_returns.replace(-99, np.nan)
    df_returns = df_returns.dropna(subset=["mtna", "mret", "mnav"], how="all")

    # Drop Funds where returns are all returns are missing or all values the same
    df_count = df_returns.groupby("FundNo")[["mtna", "mret", "mnav"]].count()
    all_missing_funds = df_count.query("mret == 0").index
    n_unique = df_returns.groupby("FundNo")[["mtna", "mret", "mnav"]].nunique()
    all_equal = (n_unique.max(axis=1) == 1) & (df_count.max(axis=1) > 1)
    all_equal_funds = all_equal[all_equal].index
    funds_to_drop = all_missing_funds.union(all_equal_funds)
    df_returns = df_returns.loc[~df_returns['FundNo'].isin(funds_to_drop)]

    # Calculate Fund Flow
    df_returns = df_returns.sort_values(by=["FundNo", "Date"])
    df_returns[["mtna_lagged", "mret_lagged"]] = df_returns.groupby("FundNo").shift(1)[["mtna", "mret"]]
    df_returns["Fund Flow"] = (df_returns["mtna"] / df_returns["mtna_lagged"] - 1) - df_returns["mret_lagged"]
    df_returns["Fund Flow"] = np.where(~np.isfinite(df_returns["Fund Flow"]), np.nan, df_returns["Fund Flow"])
    df_returns = df_returns.drop(columns=["mtna_lagged", "mret_lagged"])
    return df_returns


def load_monthly_chars():
    """Converts monthly CRSP fund characteristics data."""
    usecols = ["crsp_fundno", "caldt", "ticker", "mgr_name", "first_offer_dt", "mgmt_fee", "exp_ratio", "lipper_obj_name"]
    df = pd.read_csv(CRSM_SUMMARY_FILENAME, usecols=usecols)
    df = df.rename(columns={'caldt': 'Date', 'crsp_fundno': 'FundNo'})
    df['Date'] = pd.to_datetime(df['Date'], format='%Y%m%d')
    df = df.sort_values(by=["FundNo", "Date"])

    # Replace values
    df = df.replace(-99, np.nan)
    cols = ["mgmt_fee", "exp_ratio"]
    for col in cols:
        # Replace negative numbers with nan, will be filled later on
        df.loc[df[col] < 0] = np.nan

    # Fill missing observations
    ffill_cols = ["ticker", "mgr_name", "first_offer_dt", "mgmt_fee", "exp_ratio", "lipper_obj_name"]
    df[ffill_cols] = df.groupby('FundNo')[ffill_cols].ffill()
    bfill_cols = ["first_offer_dt", "lipper_obj_name"]
    df[bfill_cols] = df.groupby('FundNo')[bfill_cols].bfill()

    # Drop rows without ticker
    df = df.dropna(subset=["ticker"])

    # Team managed dummy
    team_list = ["Team Managed", "Team Manged", "Team Manager"]
    df["Team Managed"] = np.where(df["mgr_name"].isin(team_list), 1, 0)
    df = df.drop(columns=["mgr_name"])

    # Fix some anomalous first offer dates (correct data obtained from Yahoo Finance)
    df.loc[df['ticker'] == 'DVSVX', ['first_offer_dt']] = 20020711
    df.loc[df['ticker'] == 'DSKIX', ['first_offer_dt']] = 19990301
    df.loc[df['ticker'] == 'DVIGX', ['first_offer_dt']] = 19960221
    df.loc[df['ticker'] == 'SCIGX', ['first_offer_dt']] = 19950602

    return df


def get_CIK(df_CRSP, scrape=False):
    '''Adds column with SEC CIK, Contract ID and Series ID to CRSP data'''

    # Read linking table files from SEC
    SEC_linking_tables = os.listdir(PATH_SEC_LINKING_TABLES)
    sec_list = []
    for file in SEC_linking_tables:
        sec_list.append(pd.read_csv(PATH_SEC_LINKING_TABLES + file))
    df_SEC = pd.concat(sec_list)

    # Specify columns to keep and drop duplicates
    cols = ['CIK Number', 'Series ID', 'Class ID', 'Class Ticker']
    df_SEC = df_SEC[cols].drop_duplicates().dropna(subset=["Class Ticker"])

    # Merge CRSP and SEC dataframes
    tickers_CRSP = df_CRSP['ticker'].dropna().unique()
    df_CRSP = df_CRSP.rename(columns={'ticker': 'Class Ticker'})
    df = pd.merge(df_CRSP, df_SEC, on=['Class Ticker'])

    # Find lists of matched tickers and tickers not matched with SEC database
    tickers_SEC = df['Class Ticker'].unique()
    tickers_CRSP = np.setdiff1d(tickers_CRSP, tickers_SEC)
    tickers_CRSP = tickers_CRSP.tolist()

    # CRSP dataframe that is not matched
    df_CRSP = df_CRSP.loc[df_CRSP['Class Ticker'].isin(tickers_CRSP)]

    '''Obtain scraped tickers'''
    if os.path.exists(SCRAPED_TICKERS_FILENAME) and not scrape:
        # If already scraped, read stored csv file
        df_scraped = pd.read_csv(SCRAPED_TICKERS_FILENAME)
        df_scraped = df_scraped[['Class Ticker', 'CIK Number', 'Class ID']].drop_duplicates()
        df_CRSP = df_CRSP.merge(df_scraped, on=['Class Ticker'])

    elif os.path.exists(SCRAPED_TICKERS_FILENAME) and scrape:
        df_scraped = pd.read_csv(SCRAPED_TICKERS_FILENAME)
        df_scraped = df_scraped[['Class Ticker', 'CIK Number', 'Class ID']].drop_duplicates(inplace=True)

        # Scrape additional tickers from SEC
        tickers_scraped = df_scraped['Class Ticker'].unique()
        tickers_to_scrape = np.setdiff1d(tickers_CRSP, tickers_scraped)

        CIK_dict = ticker_to_CIK.get_CIK_AND_CONTRACT_ID(tickers_to_scrape)
        df_CIK = pd.DataFrame.from_dict(CIK_dict, orient='index', columns=['CIK Number', 'Class ID'])

        # Convert index to column
        df_CIK = df_CIK.reset_index()
        df_CIK = df_CIK.rename(columns={'index': 'Class Ticker'})
        df_CIK.dropna(subset=['CIK Number'], inplace=True)

        # Merge with existing dataframe
        df_CIK = pd.concat([df_scraped, df_CIK])

        # Merge df with CIK and Contract ID with full CRSP df
        df_CRSP = pd.merge(df_CRSP, df_CIK, on=['Class Ticker'])

        # Drop rows with missing observations and add empty row with Series ID
        df_CRSP = df_CRSP.dropna(subset=['CIK Number', 'Class Ticker'])
        df_CRSP['Series ID'] = np.nan

        df_CRSP.to_csv(SCRAPED_TICKERS_FILENAME, index=False)
    else:
        CIK_dict = ticker_to_CIK.get_CIK_AND_CONTRACT_ID(tickers_CRSP)
        df_CIK = pd.DataFrame.from_dict(CIK_dict, orient='index', columns=['CIK Number', 'Class ID'])

        # Convert index to column
        df_CIK = df_CIK.reset_index()
        df_CIK = df_CIK.rename(columns={'index': 'Class Ticker'})

        # Merge df with CIK and Contract ID with full CRSP df
        df_CRSP = pd.merge(df_CRSP, df_CIK, on=['Class Ticker'])

        # Drop rows with missing observations and add empty row with Series ID
        df_CRSP = df_CRSP.dropna(subset=['CIK Number', 'Class Ticker'])
        df_CRSP['Series ID'] = np.nan

        df_CRSP.to_csv(SCRAPED_TICKERS_FILENAME, index=False)

    # Concat
    df = pd.concat([df, df_CRSP], sort=False)
    df = df.reset_index(drop=True)
    df = df.rename(columns={'CIK Number': 'CIK'})
    df = df.dropna(subset={'Class ID'})
    return df


def get_monthly_data():
    """Loads monthly CRSP data."""
    df_rets = load_monthly_returns()
    df_chars = load_monthly_chars()
    df = df_rets.merge(df_chars, on=["FundNo", "Date"], how="left")

    # Fill quarterly data 2 months
    quarterly_cols = ['ticker', 'first_offer_dt', 'mgmt_fee', 'exp_ratio', 'Team Managed', "lipper_obj_name"]
    df[quarterly_cols] = df.groupby("FundNo")[quarterly_cols].ffill(limit=2)
    df[quarterly_cols] = df.groupby("FundNo")[quarterly_cols].bfill(limit=2)

    # Fill missing data
    df['first_offer_dt'] = pd.to_datetime(df['first_offer_dt'], format='%Y%m%d')
    df["first_date"] = df.groupby("FundNo")["Date"].transform("first")
    df["first_offer_dt"] = df["first_offer_dt"].fillna(df["first_date"])
    df = df.loc[df["first_offer_dt"] <= df["Date"]]  # Remove observations where first offer date is after date

    fill_median_cols = ['mgmt_fee', 'exp_ratio']
    group_date_median = df.groupby(["Date", "lipper_obj_name"])[fill_median_cols].transform("median")
    for col in fill_median_cols:
        group_date_median[col] = group_date_median[col].fillna(group_date_median[col].median())
        df[col] = df[col].fillna(group_date_median[col])

    # Calculate fund age
    df['Fund Age'] = (df['Date'] - df['first_offer_dt']) / timedelta(days=1)  # Convert time delta to float
    df['Fund Age'] = df['Fund Age'] / 365  # Convert to years

    # Drop columns
    cols_to_drop = ["first_date", "first_offer_dt", "lipper_obj_name"]
    df = df.drop(columns=cols_to_drop)

    # Match with CIK
    # WHY DID WE NOT USE CRSP_CIK_MAP? https://www.crsp.org/files/MFDB_Guide.pdf
    df = get_CIK(df)
    df = df.drop_duplicates()
    df.to_csv(CRSP_CIK_FILENAME, index=False)


if __name__ == '__main__':
    get_monthly_data()
