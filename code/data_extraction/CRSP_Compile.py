import sys
import pandas as pd
import numpy as np
import math
import datetime
import ticker_to_CIK
import statsmodels.api as sm
import statsmodels.api as sm2
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
import os

sys.path.append('../../code/general/')  # Add directory of folder to path
from utils import write_line_to_text

# -----------------------
# User defined parameters
# -----------------------

# -----------------------Settings-----------------------
START_DATE_DAILY = '2005-07-01'  # Y-m-d
END_DATE_DAILY = '2017-07-31'  # Y-m-d

SINGLE_FUND_ONLY = False     # Boolean: if TRUE, only consider fund-specific shareholder letters
SCORES_TF_IDF = True         # Boolean: if TRUE, scores are computed using TF-IDF
CRISIS_SAMPLE = False        # Boolean: if TRUE, consider crisis subsample

# -----------------------Paths-----------------------
if SINGLE_FUND_ONLY:
    PATH_RESULTS = '../../results/single_funds/'
    RESULTS_FILENAME = PATH_RESULTS + 'scores_tf-idf.csv'
    RESULTS_MERGED_FILENAME = PATH_RESULTS + 'scores_tf-idf_merged.csv'
elif SCORES_TF_IDF:
    PATH_RESULTS = '../../results/TF_IDF/'
    RESULTS_FILENAME = PATH_RESULTS + 'scores_tf-idf.csv'
    RESULTS_MERGED_FILENAME = PATH_RESULTS + 'scores_tf-idf_merged.csv'
elif CRISIS_SAMPLE:
    PATH_RESULTS = '../../results/crisis_subsample/'
    RESULTS_FILENAME = PATH_RESULTS + 'scores_tf-idf.csv'
    RESULTS_MERGED_FILENAME = PATH_RESULTS + 'scores_tf-idf_merged.csv'
else:
    PATH_RESULTS = '../../results/proportional/'
    RESULTS_FILENAME = PATH_RESULTS + 'scores_proportional.csv'
    RESULTS_MERGED_FILENAME = PATH_RESULTS + 'scores_proportional_merged.csv'

PATH_DATA = '../../data/'
PATH_CRSP = '../../data/CRSP/'          # Set relative paths for CRSP data files
PATH_FRENCH = '../../data/French/'      # Set relative paths for Kenneth French data files
PATH_EDGAR = '../../data/EDGAR/'

# -----------------------CRSP raw filenames-----------------------
CRSP_RETURNS_FILENAME = PATH_CRSP + 'Raw/' + 'CRSP_returns_monthly.csv'
CRSM_SUMMARY_FILENAME = PATH_CRSP + 'Raw/' + 'CRSP_summary.csv'
CRSP_RETURNS_DAILY_FILENAME = PATH_CRSP + 'Raw/' + 'CRSP_returns_daily.csv'

# -----------------------CRSP processed filenames-----------------------
CRSP_MERGED_FILENAME = PATH_CRSP + 'merged.csv'
CRSP_CLEANED_FILENAME = PATH_CRSP + 'cleaned.csv'
CRSP_CIK_FILENAME = PATH_CRSP + 'CIK_merged.csv'
TICKER_FILENAME = PATH_CRSP + 'tickers.txt'
COMBINED_FILENAME = PATH_CRSP + 'returns_factors_combined.csv'
FUND_PERF_MEASURES_FILENAME = PATH_CRSP + 'fund_performance_measures.csv'
FUND_PERF_MEASURES_AUGMENTED_FILENAME = PATH_CRSP + 'fund_performance_measures_augmented.csv'

# -----------------------EDGAR filenames and paths-----------------------
SCRAPED_TICKERS_FILENAME = PATH_EDGAR + 'scraped_tickers.csv'
PATH_SEC_LINKING_TABLES = '../../data/EDGAR/Linking_Tables/' # Set relative paths for EDGAR linking tables files

# -----------------------Fama-French filenames-----------------------
FF_FACTORS_DAILY_FILENAME = PATH_FRENCH + 'F-F_Research_Data_Factors_daily.csv'

# -----------------------Ouput filenames-----------------------
FUNDS_COUNTER_FILENAME = PATH_RESULTS + 'funds_counter.txt'

#
# * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * +

def merge_CRSP_data(calculate_lagged=False):
    '''Converts monthly CRSP fund returns data to quarterly frequency and merges with CRSP quarterly fund summary data'''

    # Read csv files
    df_returns = pd.read_csv(CRSP_RETURNS_FILENAME)
    df_chars = pd.read_csv(CRSM_SUMMARY_FILENAME)

    # Count number of funds
    nFunds = df_returns['crsp_fundno'].unique().size
    message = 'Full CRSP mutual fund database of all open-ended mutual funds,' + str(nFunds)
    write_line_to_text(FUNDS_COUNTER_FILENAME, message)

    # Rename some columns
    df_returns = df_returns.rename(columns={'caldt': 'Date', 'crsp_fundno': 'FundNo'})
    df_chars = df_chars.rename(columns={'caldt': 'Date', 'crsp_fundno': 'FundNo'})

    # Set data format and add date quarter column
    for df in [df_returns, df_chars]:
        df['Date'] = pd.to_datetime(df['Date'], format='%Y%m%d')

        df['Quarter'] = df['Date'].dt.quarter
        df['Quarter Date'] = df['Date'].dt.year.map(str) + 'Q' + df['Quarter'].map(str)

    # Convert TNA column to type numeric
    df_returns['mtna'] = pd.to_numeric(df_returns['mtna'], errors='coerce')

    # Convert decimal df_returns to factor df_returns
    df_returns['mret'] = df_returns['mret'].replace('R', np.NaN)
    df_returns['mret'] = pd.to_numeric(df_returns['mret'])
    df_returns['mret'] = df_returns['mret'] + 1

    ''' Aggregate monthly data to quarterly frequency '''
    fund_list = df_returns['FundNo'].unique()
    cols = ['FundNo', 'Quarter Date', 'Return', 'TNA', 'NAV', 'Fund Flow']

    df_quarterly_returns = pd.DataFrame(columns=cols)  # Create empty dataframe for quarterly df_returns
    df_quarterly_returns = []  # Create empty list to store dataframes

    for fund in fund_list[:]:
        df_temp = df_returns.loc[df_returns['FundNo'] == fund].reset_index(drop=True)
        if df_temp['mret'].isnull().values.any():  # Skip fund if one of its monthly return values is a NaN
            continue
        elif df_temp['mret'].nunique() == 1:  # Skip fund if all return values are the same
            continue
        elif df_temp['mtna'].isnull().values.any():  # Skip fund if one of its TNA values is a NaN
            continue
        elif df_temp['mtna'].min() <= 1:  # Skip fund if one of its TNA values is smaller than or equal to one
            continue
        else:
            df_quarterly_returns_temp = pd.DataFrame(columns=cols)  # Empty dataframe

            quarters = df_temp['Quarter Date'].unique()
            prev_quarter = quarters[0]
            agg_return = 1
            row_counter = 0
            quarter_counter = 0

            for quarter in df_temp['Quarter Date']:
                if quarter == prev_quarter:
                    agg_return = agg_return*df_temp.loc[row_counter, 'mret']  # Calculate compounded quarter return
                else:
                    df_quarterly_returns_temp.loc[quarter_counter, 'Quarter Date'] = df_temp.loc[row_counter-1, 'Quarter Date']
                    df_quarterly_returns_temp.loc[quarter_counter, 'NAV'] = df_temp.loc[row_counter-1, 'mnav']
                    df_quarterly_returns_temp.loc[quarter_counter, 'Return'] = agg_return

                    # Update TNA and calculate fund flow
                    current_TNA = df_temp.loc[row_counter - 1, 'mtna']
                    df_quarterly_returns_temp.loc[quarter_counter, 'TNA'] = current_TNA

                    if quarter != quarters[1]:  # Only start calculating from second quarter onwards
                        prev_TNA = df_quarterly_returns_temp.loc[quarter_counter - 1, 'TNA']
                        df_quarterly_returns_temp.loc[quarter_counter, 'Fund Flow'] = (current_TNA / prev_TNA - 1) - (agg_return -1)

                    # Update return
                    agg_return = df_temp.loc[row_counter, 'mret']
                    quarter_counter += 1

                # Update row counter and quarter
                row_counter += 1
                prev_quarter = quarter

                # Add last full quarter of dataset
                if row_counter == len(df_temp) and quarter_counter > 0:  # and len(df_temp) % 3 == 0
                    # row_to_update = row_counter / 3 - 1

                    # Update quarter date and NAV
                    df_quarterly_returns_temp.loc[quarter_counter, 'Quarter Date'] = df_temp.loc[row_counter - 1, 'Quarter Date']
                    df_quarterly_returns_temp.loc[quarter_counter, 'NAV'] = df_temp.loc[row_counter - 1, 'mnav']

                    # Calculate return
                    current_return = agg_return * df_temp.loc[row_counter - 1, 'mret']
                    df_quarterly_returns_temp.loc[quarter_counter, 'Return'] = current_return

                    # Update TNA and calculate fund flow
                    prev_TNA = df_quarterly_returns_temp.loc[quarter_counter - 1, 'TNA']
                    current_TNA = df_temp.loc[row_counter - 1, 'mtna']
                    df_quarterly_returns_temp.loc[quarter_counter, 'TNA'] = current_TNA

                    df_quarterly_returns_temp.loc[quarter_counter, 'Fund Flow'] = (current_TNA / prev_TNA - 1) - (current_return -1)

            df_quarterly_returns_temp['FundNo'] = df_temp['FundNo']  # Add fund number
            df_quarterly_returns.append(df_quarterly_returns_temp)  # Concatenate temporary df to full quarterly df

    # Concatenate list into dataframe at the end
    df_quarterly_returns = pd.concat(df_quarterly_returns, axis=0)

    # Count number of funds
    nFunds = df_quarterly_returns['FundNo'].unique().size
    message = 'Initial filtering on missing returns, TNA and on funds with TNA smaller than 1 mln,' + str(nFunds)
    write_line_to_text(FUNDS_COUNTER_FILENAME, message)

    ''' Merge quarterly df_returns with fund df_chars '''
    df_CRSP = pd.merge(df_quarterly_returns, df_chars, on=['FundNo', 'Quarter Date'])
    df_CRSP = df_CRSP.sort_values(by=['FundNo', 'Date'], ascending=[True, True])

    # Compute lagged and future fund return and fund flow
    if calculate_lagged:
        df_lags = pd.DataFrame(df_CRSP['Quarter Date'].copy())

        df_lags['Lagged Fund Flow 3m'] = 1 + df_CRSP.groupby(['FundNo'])['Fund Flow'].shift(1)
        df_lags['Lagged Fund Flow 6m'] = 1 + df_CRSP.groupby(['FundNo'])['Fund Flow'].shift(2)

        df_lags['Future Fund Flow 3m'] = 1 + df_CRSP.groupby(['FundNo'])['Fund Flow'].shift(-1)
        df_lags['Future Fund Flow 6m'] = 1 + df_CRSP.groupby(['FundNo'])['Fund Flow'].shift(-2)

        # Add to complete dataframe
        df_CRSP['Lagged Fund Flow'] = df_lags['Lagged Fund Flow 3m'].multiply(df_lags['Lagged Fund Flow 6m'], axis='index') - 1
        df_CRSP['Future Fund Flow'] = df_lags['Future Fund Flow 3m'].multiply(df_lags['Future Fund Flow 6m'], axis='index') - 1

    # Count number of funds
    nFunds = df_CRSP['FundNo'].unique().size
    message = 'Merge fund characteristics with returns (retaining only intersection),' + str(nFunds)
    write_line_to_text(FUNDS_COUNTER_FILENAME, message)

    # Write merged data to csv
    df_CRSP.to_csv(CRSP_MERGED_FILENAME, index=False)
    return df_CRSP


def clean_CRSP_data():
    '''Cleans merged CRSP data'''

    df_CRSP = pd.read_csv(CRSP_MERGED_FILENAME)

    # Merge TNA and remove duplicate column 'tna_latest'
    df_CRSP['TNA'] = np.where(df_CRSP['TNA'].isna == False, df_CRSP['TNA'], df_CRSP['tna_latest'])
    df_CRSP = df_CRSP.drop('tna_latest', 1)

    # Merge NAV and remove duplicate column 'nav_latest'
    df_CRSP['NAV'] = np.where(df_CRSP['NAV'].isna == False, df_CRSP['NAV'], df_CRSP['nav_latest'])
    df_CRSP = df_CRSP.drop('nav_latest', 1)

    # Calculate fund age
    # First fix some anomalous first offer dates (correct data obtained from Yahoo Finance)
    df_CRSP.loc[df_CRSP['ticker'] == 'DVSVX', ['first_offer_dt']] = 20020711
    df_CRSP.loc[df_CRSP['ticker'] == 'DSKIX', ['first_offer_dt']] = 19990301
    df_CRSP.loc[df_CRSP['ticker'] == 'DVIGX', ['first_offer_dt']] = 19960221
    df_CRSP.loc[df_CRSP['ticker'] == 'SCIGX', ['first_offer_dt']] = 19950602

    df_CRSP['first_offer_dt'] = pd.to_datetime(df_CRSP['first_offer_dt'], format='%Y%m%d')
    df_CRSP['Date'] = pd.to_datetime(df_CRSP['Date'], format='%Y-%m-%d')
    df_CRSP['Fund Age'] = (df_CRSP['Date'] - df_CRSP['first_offer_dt']) / timedelta(days=1)  # Convert time delta to float
    df_CRSP['Fund Age'] = df_CRSP['Fund Age'] / 365  # Convert to years

    # Remove unwanted columns and rows
    df_CRSP = df_CRSP.drop(['summary_period2', 'nav_latest_dt', 'tna_latest_dt'], 1)
    df_CRSP = df_CRSP.drop(['lipper_obj_name', 'delist_cd', 'maturity', 'cusip8', 'crsp_portno'], 1)

    df_CRSP = df_CRSP[df_CRSP['NAV'] != 1]
    df_CRSP = df_CRSP.loc[df_CRSP['turn_ratio'] != -99]
    df_CRSP = df_CRSP.loc[df_CRSP['exp_ratio'] != -99]

    # Count number of funds
    nFunds = df_CRSP['FundNo'].unique().size
    message = 'Clean turnover ratio (!= -99) and NAV (!= 1),' + str(nFunds)
    write_line_to_text(FUNDS_COUNTER_FILENAME, message)

    # Remove rows without ticker, TNA, manager name, company name, first offer date
    df_CRSP = df_CRSP.dropna(subset=['ticker'])
    df_CRSP = df_CRSP.dropna(subset=['TNA'])
    df_CRSP = df_CRSP.dropna(subset=['mgr_name'])
    df_CRSP = df_CRSP.dropna(subset=['adv_name'])
    df_CRSP = df_CRSP.dropna(subset=['first_offer_dt'])

    # Replace missing dividend yield with 0
    df_CRSP['div_ytd'] = df_CRSP['div_ytd'].fillna(0)

    '''Fill in missing data with previous or next valid observation'''
    df_CRSP = df_CRSP.reset_index(drop=True)
    rows_to_delete = []

    for row in range(0, len(df_CRSP)):
        if math.isnan(df_CRSP.loc[row, 'mgmt_fee']):  # Check if missing management fee value
            current_fund = df_CRSP.loc[row, 'FundNo']
            next_row = df_CRSP.loc[row:, 'mgmt_fee'].first_valid_index()  # Index of next non-empty row

            if pd.notnull(next_row) and df_CRSP.loc[next_row, 'FundNo'] == current_fund and row < len(df_CRSP)-1:
                df_CRSP.loc[row, ['mgmt_fee', 'exp_ratio', 'turn_ratio']] = df_CRSP.loc[next_row, ['mgmt_fee', 'exp_ratio', 'turn_ratio']]
            elif df_CRSP.loc[row-1, 'FundNo'] == current_fund and row != 0 and ((row-1) not in rows_to_delete):
                df_CRSP.loc[row, ['mgmt_fee', 'exp_ratio', 'turn_ratio']] = df_CRSP.loc[row-1, ['mgmt_fee', 'exp_ratio', 'turn_ratio']]
            else:
                rows_to_delete.append(row)

    df_CRSP = df_CRSP.drop(rows_to_delete)
    df_CRSP = df_CRSP.reset_index(drop=True)

    # Count number of funds
    nFunds = df_CRSP['FundNo'].unique().size
    message = 'Remove funds without ticker/TNA manager name/company name,' + str(nFunds)
    write_line_to_text(FUNDS_COUNTER_FILENAME, message)

    # Write cleaned data to csv
    df_CRSP.to_csv(CRSP_CLEANED_FILENAME, index=False)
    return df_CRSP


def get_CIK(scrape=False):
    '''Adds column with SEC CIK, Contract ID and Series ID to CRSP data'''

    # Read linking table files from SEC
    SEC_linking_tables = os.listdir(PATH_SEC_LINKING_TABLES)
    sec_list = []
    for file in SEC_linking_tables:
        sec_list.append(pd.read_csv(PATH_SEC_LINKING_TABLES + file))
    df_SEC = pd.concat(sec_list)

    # Specify columns to keep and drop duplicates
    cols = ['CIK Number', 'Series ID', 'Class ID', 'Class Ticker']
    df_SEC = df_SEC[cols].drop_duplicates()

    # Merge CRSP and SEC dataframes
    df_CRSP = pd.read_csv(CRSP_CLEANED_FILENAME)
    df_CRSP = df_CRSP.rename(columns={'ticker': 'Class Ticker'})
    df_merged = pd.merge(df_CRSP, df_SEC, on=['Class Ticker'])

    # Find lists of matched tickers and tickers not matched with SEC database
    funds_SEC = df_merged['FundNo'].unique()
    tickers_SEC = df_merged['Class Ticker'].unique()
    tickers_CRSP = df_CRSP['Class Ticker'].unique()

    tickers_CRSP = np.setdiff1d(tickers_CRSP, tickers_SEC)
    tickers_CRSP = tickers_CRSP.tolist()

    # CRSP dataframe that is not matched
    df_CRSP = df_CRSP.loc[df_CRSP['Class Ticker'].isin(tickers_CRSP)]

    '''Obtain scraped tickers'''
    if os.path.exists(SCRAPED_TICKERS_FILENAME) and not scrape:
        # If already scraped, read stored csv file
        df_scraped = pd.read_csv(SCRAPED_TICKERS_FILENAME)
        df_scraped = df_scraped[['Quarter Date', 'Class Ticker', 'CIK Number', 'Class ID', 'Series ID']]
        df_CRSP = pd.merge(df_CRSP, df_scraped, on=['Quarter Date', 'Class Ticker'])

    elif os.path.exists(SCRAPED_TICKERS_FILENAME) and scrape:
        df_scraped = pd.read_csv(SCRAPED_TICKERS_FILENAME)
        df_scraped = df_scraped[['Class Ticker', 'CIK Number', 'Class ID']]
        df_scraped.drop_duplicates(inplace=True)

        # Scrape additional tickers from SEC
        tickers_scraped = df_scraped['Class Ticker'].unique()
        tickers_to_scrape = np.setdiff1d(tickers_CRSP, tickers_scraped)

        CIK_dict = ticker_to_CIK.get_CIK_AND_CONTRACT_ID(tickers_to_scrape)
        df_CIK = pd.DataFrame.from_dict(CIK_dict, orient='index', columns=['CIK Number', 'Class ID'])

        # Convert index to column
        df_CIK = df_CIK.reset_index()
        df_CIK = df_CIK.rename(columns={'index': 'Class Ticker'})
        df_CIK.dropna(subset=['CIK Number'], inplace=True)

        # Merge with existing dataframe
        df_CIK = pd.concat([df_scraped, df_CIK])

        # Merge df with CIK and Contract ID with full CRSP df
        df_CRSP = pd.merge(df_CRSP, df_CIK, on=['Class Ticker'])

        # Drop rows with missing observations and add empty row with Series ID
        df_CRSP = df_CRSP.dropna(subset=['CIK Number', 'Class Ticker'])
        df_CRSP['Series ID'] = np.nan

        df_CRSP.to_csv(SCRAPED_TICKERS_FILENAME, index=False)
    else:
        CIK_dict = ticker_to_CIK.get_CIK_AND_CONTRACT_ID(tickers_CRSP)
        df_CIK = pd.DataFrame.from_dict(CIK_dict, orient='index', columns=['CIK Number', 'Class ID'])

        # Convert index to column
        df_CIK = df_CIK.reset_index()
        df_CIK = df_CIK.rename(columns={'index': 'Class Ticker'})

        # Merge df with CIK and Contract ID with full CRSP df
        df_CRSP = pd.merge(df_CRSP, df_CIK, on=['Class Ticker'])

        # Drop rows with missing observations and add empty row with Series ID
        df_CRSP = df_CRSP.dropna(subset=['CIK Number', 'Class Ticker'])
        df_CRSP['Series ID'] = np.nan

        df_CRSP.to_csv(SCRAPED_TICKERS_FILENAME, index=False)

    '''Concat dataframes and write to csv'''
    df_CRSP_SEC = pd.concat([df_merged, df_CRSP], sort=False)
    df_CRSP_SEC = df_CRSP_SEC.reset_index(drop=True)
    df_CRSP_SEC = df_CRSP_SEC.rename(columns={'CIK Number': 'CIK'})
    df_CRSP_SEC = df_CRSP_SEC.dropna(subset={'Class ID'})

    df_CRSP_SEC.to_csv(CRSP_CIK_FILENAME, index=False)

    # Compile list of tickers and write to txt file
    tickers = df_CRSP_SEC['Class Ticker'].unique()

    with open(TICKER_FILENAME, 'w') as filehandle:
        for item in tickers:
            filehandle.write(item + '\n')

    # Count number of funds
    funds = df_CRSP_SEC['FundNo'].unique()
    nFunds = len(funds)
    message = 'Merge CRSP and SEC on fund tickers,' + str(nFunds)
    write_line_to_text(FUNDS_COUNTER_FILENAME, message)

    nFunds = len(funds_SEC)
    message = 'Scrape SEC for remaining tickers,' + str(nFunds)
    write_line_to_text(FUNDS_COUNTER_FILENAME, message)


def get_daily_returns_and_factors():
    '''Reads daily return and Fama French data and computes fund performance measures'''

    # Read base dataset
    if os.path.exists(RESULTS_FILENAME):
        df_base = pd.read_csv(RESULTS_FILENAME)

        # Combine CRSP data with FF factors
        if os.path.exists(COMBINED_FILENAME):
            # Read csv file if already exists
            df_combined = pd.read_csv(COMBINED_FILENAME)
            df_combined['Date'] = pd.to_datetime(df_combined['Date'], format='%Y-%m-%d')
        else:
            # Read FF and daily returns datasets
            df_returns_daily = pd.read_csv(CRSP_RETURNS_DAILY_FILENAME)
            df_FF_factors_daily = pd.read_csv(FF_FACTORS_DAILY_FILENAME)

            # Rename some columns
            df_returns_daily = df_returns_daily.rename(columns={'caldt': 'Date', 'crsp_fundno': 'FundNo'})
            df_returns_daily['dret'] = pd.to_numeric(df_returns_daily['dret'], errors='coerce')

            # Convert date column to datetime format
            df_returns_daily['Date'] = pd.to_datetime(df_returns_daily['Date'], format='%Y%m%d')
            df_FF_factors_daily['Date'] = pd.to_datetime(df_FF_factors_daily['Date'], format='%Y%m%d')

            # Merge datasets
            df_combined = pd.merge(df_returns_daily, df_FF_factors_daily, on=['Date'])

            # Write to csv file
            df_combined.to_csv(COMBINED_FILENAME, index=False)
            del df_returns_daily, df_FF_factors_daily
            print('Combined file written.')

        # Select subset of dates
        mask = (df_combined['Date'] >= START_DATE_DAILY) & (df_combined['Date'] <= END_DATE_DAILY)
        df_combined = df_combined.loc[mask]
        df_combined = df_combined.reset_index(drop=True)
        del mask

        # Create column with excess returns
        df_combined['dret_exc'] = df_combined['dret'] - df_combined['RF']/252  # Divide annual Rf by 252

        # Sort dataframes
        df_combined.sort_values(by=['FundNo', 'Date'], inplace=True)
        df_base.sort_values(by=['FundNo', 'file-date'], inplace=True)
        df_base = df_base[['FundNo', 'file-date']]

        df_base.reset_index(drop=True, inplace=True)
        df_combined.reset_index(drop=True, inplace=True)

        fund_list = df_base['FundNo'].unique()

        cols = ['FundNo', 'file-date', 'Lagged Return', 'Future Return', 'SD(dret)',
                'Alpha CAPM', 'Alpha FF3', 'Alpha CH4', 'Rm-Rf', 'SMB', 'HML', 'Mom',
                'SD(resid)', 'Lagged Volatility']
        df_output = []

        # Compute performance measures based on months t+1 through t+6
        for fund, i in zip(fund_list, range(len(fund_list))):
            if (i+1) % 100 == 0:
                print(str(i+1) + '/' + str(len(fund_list)) + ' funds processed. ')

            date_list = df_base.loc[df_base['FundNo'] == fund].reset_index(drop=True)['file-date'].unique()

            for date in date_list:
                df_temp = df_combined.loc[df_combined['FundNo'] == fund].reset_index(drop=True)
                if df_temp.size == 0:  # Skip fund if it has no observations
                    continue
                else:
                    # Determine dates corresponding to months t-6, t+1, and t+6
                    current_date = datetime.strptime(str(int(float(date))), '%Y%m%d').date()
                    date_t_plus_1 = str(current_date + relativedelta(months=0))
                    date_t_plus_6 = str(current_date + relativedelta(months=+6))
                    date_t_minus_6 = str(current_date - relativedelta(months=+6))

                    # Select subset of daily returns corresponding to months t+1 and t+6
                    mask_future = (df_temp['Date'] >= pd.Timestamp(date_t_plus_1)) & (df_temp['Date'] <= pd.Timestamp(date_t_plus_6))
                    mask_lag = (df_temp['Date'] >= pd.Timestamp(date_t_minus_6)) & (df_temp['Date'] < pd.Timestamp(current_date))

                    df_future = df_temp.loc[mask_future]
                    df_future = df_future.reset_index(drop=True)

                    # Estimate Fama-French three-factor model for months t+1 through t+6
                    Y = df_future['dret_exc']
                    if Y.count() < 30:  # Skip fund if it does not have sufficient excess return observations
                         continue
                    else:
                        # Results dataframe
                        df_fund_perf = pd.DataFrame(columns=cols)

                        X = df_future[['Mkt-RF', 'SMB', 'HML', 'Mom']]/100
                        X = sm2.add_constant(X)

                        # 1) CAPM alpha
                        X_cols = ['const', 'Mkt-RF']
                        mod = sm.OLS(Y, X[X_cols], missing='drop').fit()
                        alpha_capm = mod.params['const']

                        # 2) FF3 alpha
                        X_cols += ['SMB', 'HML']
                        mod = sm.OLS(Y, X[X_cols], missing='drop').fit()
                        alpha_ff3 = mod.params['const']

                        # 3) CH4 alpha
                        X_cols += ['Mom']
                        mod = sm.OLS(Y, X[X_cols], missing='drop').fit()

                        # Store alphas and CH4 factor loadings
                        params = mod.params.rename(index={'const': 'Alpha CH4', 'Mkt-RF': 'Rm-Rf'})
                        params = [alpha_capm, alpha_ff3] + params.to_list()
                        col_names = ['Alpha CAPM', 'Alpha FF3', 'Alpha CH4', 'Rm-Rf', 'SMB', 'HML', 'Mom']
                        df_fund_perf.loc[0, col_names] = params

                        # Estimate standard deviation of residuals and append to new dataframe
                        resids = mod.resid
                        sd_resids = resids.std()
                        df_fund_perf.loc[0, 'SD(resid)'] = sd_resids

                        # Estimate standard deviation of daily returns and append to new dataframe
                        df_fund_perf.loc[0, 'SD(dret)'] = df_future['dret'].std()

                        # Add fund number and date
                        df_fund_perf.loc[0, 'FundNo'] = fund
                        df_fund_perf.loc[0, 'file-date'] = date

                        # Add lagged and future returns
                        df_lagged = df_temp.loc[mask_lag]
                        df_lagged = df_lagged.reset_index(drop=True)

                        df_fund_perf.loc[0, 'Future Return'] = (df_future['dret']+1).product() - 1
                        df_fund_perf.loc[0, 'Lagged Return'] = (df_lagged['dret']+1).product() - 1
                        df_fund_perf.loc[0, 'Lagged Volatility'] = df_lagged['dret'].std()

                        # Set equal to NaN if no lagged returns
                        df_fund_perf.loc[df_fund_perf['Lagged Return'] == 0, 'Lagged Return'] = np.NaN

                        # Update row counter
                        df_output.append(df_fund_perf)

        # Write to csv file
        df_output = pd.concat(df_output, axis=0)
        df_output.to_csv(FUND_PERF_MEASURES_FILENAME, index=False)
    else:
        raise ValueError('Error: The following file does not exist: ' + RESULTS_FILENAME)


def compute_fund_flow():
    '''Computes future and lagged fund flow relative to the letter filing date'''
    # Read inputs
    df_returns = pd.read_csv(CRSP_RETURNS_FILENAME)
    df_results = pd.read_csv(FUND_PERF_MEASURES_FILENAME)

    # Format and select subset of funds
    df_returns.rename(columns={'caldt': 'Date', 'crsp_fundno': 'FundNo'}, inplace=True)
    fund_list = df_results['FundNo'].unique()
    df_returns = df_returns[df_returns['FundNo'].isin(fund_list)]

    df_results['file-date'] = pd.to_datetime(df_results['file-date'], format='%Y%m%d')
    df_returns['Date'] = pd.to_datetime(df_returns['Date'], format='%Y%m%d')

    # Round dates to the next month
    df_returns['Date'] = df_returns['Date'] + pd.offsets.MonthBegin(0)
    df_results['Date'] = df_results['file-date'] + pd.offsets.MonthBegin(0)

    # Create lag and future columns
    cols = ['mtna', 'mret']
    df_returns['mret'] = df_returns['mret'].replace('R', np.NaN)

    for col in cols:
        df_returns[col] = pd.to_numeric(df_returns[col], errors='coerce')
        if col == 'mtna':
            df_returns[col + '-1'] = df_returns.groupby('FundNo')[col].shift(1)   # For month t-1
            df_returns[col + '-6'] = df_returns.groupby('FundNo')[col].shift(6)   # For month t-6
            df_returns[col + '+6'] = df_returns.groupby('FundNo')[col].shift(-5)  # For month t+6

    # Compute compounded six months returns using a rolling window
    df_returns = (df_returns.set_index('FundNo', append=True)
              .assign(cumret_lagged=df_returns.groupby('FundNo')['mret'].rolling(6, min_periods=6)
              .apply(lambda x: np.prod(1 + x) - 1, raw=True)
              .swaplevel(0, 1)).reset_index(1))

    df_returns['cumret_lagged'] = df_returns.groupby('FundNo')['cumret_lagged'].shift(1)  # Return over t-6 to t-1
    df_returns['cumret_future'] = df_returns.groupby('FundNo')['cumret_lagged'].shift(-5)  # Return over t+1 to t+6

    df_returns.dropna(inplace=True)
    df_returns = df_returns.reset_index(drop=True)

    # Compute TNA deltas
    df_returns['mtna delta lagged'] = df_returns['mtna-1'].div(df_returns['mtna-6']) - 1
    df_returns['mtna delta'] = df_returns['mtna+6'].div(df_returns['mtna']) - 1

    # Compute fund flows
    df_returns['Lagged Fund Flow'] = df_returns['mtna delta lagged'] - df_returns['cumret_lagged']
    df_returns['Future Fund Flow'] = df_returns['mtna delta'] - df_returns['cumret_future']

    # Merge with performance measures dataframe
    cols = ['FundNo', 'Date', 'Future Fund Flow', 'Lagged Fund Flow']
    df_results = df_results.merge(df_returns[cols], on=['FundNo', 'Date'])

    # Write to csv file
    df_results.drop(columns=['Date'], inplace=True)
    df_results.to_csv(FUND_PERF_MEASURES_AUGMENTED_FILENAME, index=False)


if __name__ == '__main__':
    ...
    # merge_CRSP_data()
    # print('Datasets merged.')
    # clean_CRSP_data()
    # print('Dataset cleaned.')
    # get_CIK()
    # print('CIKs collected.')
    # get_daily_returns_and_factors()
    # print('FF regressions performed')
    # compute_fund_flow()
    # print('Fund flow computed')


