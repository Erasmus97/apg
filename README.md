 ![something](./images/erasmus_logo.png)

# Background

This research project is a core component of the seminar Seminar Financial Case Studies from the master's program
Quantitative Finance (Econometrics and Management Science) at the Erasmus School of Economics, Rotterdam, the Netherlands. 
We investigate how textual analysis of mutual funds’ shareholder letters in N-CSR filings can be used to 
predict risk-adjusted mutual fund returns. 

The students who worked on this project are:
* Christiaan van den Berg (432934)
* Wesley van Driel (425342)
* Maarten Jansen (422196)
* Fabian Neefjes (429638)

# Code

### Library dependencies
Library dependencies for the Python code. You need to install these with
`pip install -r requirements.txt` before you can run this.

### Prior to running
Before running the pipeline, which scrapes the web for files and does the textual 
analysis, a CSV file should be constructed with fund data from the CRSP database 
like `CIK_merged.csv`. [This file](data/CRSP/CIK_merged.csv) can be created by downloading
data from [CRSP](https://wrds-web.wharton.upenn.edu/wrds/) and running part of the code in the 
[data_extraction](code/data_extraction) folder. Specifically, the functions 
`merge_CRSP_data()`, `merge_CRSP_data()`, and `get_CIK()` should be run using
the data from CRSP to generate this file. Note that the remaining functions, 
`get_daily_returns_and_factors()` and `compute_fund_flow()`, can only be runned after the pipeline.

Performing textual analysis requires [Loughran and McDonald Word Lists](data), as well as data 
from [GloVe](data/glove). Additionally, running the trading strategy (i.e., the portfolio allocation strategy)
requires returns data from the [MSCI World Index](data/msci_world.xls). The files that are 
currently used can be found in the [data](data) folder.

### Starting the pipeline
The pipeline is among other things responsible for downloading N-CSR filings from the EDGAR database, 
cleaning them, computing textual scores, summarizing shareholder letters, and running the textual
analysis-based 'trading strategy'.

The pipeline is started by running `main.py`. The code starts by listing several
settings:

```
DOWNLOAD_LETTERS = True
MAKE_SUMMARIES = True
MERGE_CSV = True
GET_SCORES_LETTERS = True
GET_SCORES_SUMMARIES = True
PROPORTIONAL = True
CROSS_TRIMMING = True
TRADING = {'Volatility': True, 'Future Return': True}
COPULA_TRADE = False

```

The function of each setting is:
* DOWNLOAD_LETTERS: start downloading the shareholder letters for a certain time period. 
This period can be set later in the code by the user input:
```
START_YEAR = 2006
END_YEAR = 2016
BEGIN_QUARTER = 1
END_QUARTER = 4
```
* MAKE_SUMMARIES: creates a summary of every shareholder letter for later analysis
* MERGE_CSV: merges the separate CSV files that are created when scraping the 
EDGAR database for obtaining shareholder letters
* GET_SCORES_LETTERS: performes textual analysis on the shareholder letters and 
saves the textual scores in a CSV file
* GET_SCORES_SUMMARIES: performs textual analysis on the shareholder letters'
summaries and saves the textual scores in a CSV file
* PROPORTIONAL: if True, the textual scores are based on proportional weights; if 
Else, the weights of the words in the text are created based on the TF-IDF weighting
scheme
* CROSS_TRIMMING: if True, the trimming of shareholder letters and summarized letters are the same
* TRADING: set True or False, to choose whether to run volatility or return strategies
* COPULA_TRADE: if True, copulas are used in the trading strategy, else regressions

### After running
After running the pipeline, the results can be analyzed using copulas or 
regression analysis, which can be performed using the files in the [data_analysis](code/data_analysis) folder. 
To perform the regression analysis, data from [Kenneth French's Data Library](data/French) is also required, 
which can be found in the [data](data) folder. Note that this data has to be processed using the corresponding
function in [CRSP_Compile](code/data_extraction/CRSP_Compile.py).

### Package descriptions

* bootstrap_regression: contains a script to efficiently conduct the double resampling bootstrap procedure
* copula_test: contains an R script that performs the independence copula test by Genest and Rémillard (2004) 
* data_analysis: contains all scripts to run regressions, fit copulas, and perform textual analysis
* data_extraction: contains the scripts that are needed to scrape the shareholder letters from the internet
* general: contains general scripts that are necessary in multiple dependencies, such as 
creating file objects and the word lists of Loughran & McDonald
* matlab: contains a MATLAB script which is used to generate all figures reported in the paper
* simulation: contains all scripts (and corresponding results) of the trading strategy

A description of the scripts inside these packages is provided [here](code/Documentation.pdf).

### Code from Loughran & McDonald

A part of our code is based on the code used in several papers by Loughran & McDonald, 
which revolves around accessing the EDGAR database and obtaining filings.
The code of Loughran & McDonald is obtained from the 
[Software Repository for Accounting and Finance](https://sraf.nd.edu) and contains the following:

* `Class_LM10XSummaries.py`: module that provides a class for the the LM_10X_Summary files

* `EDGAR_DownloadForms.py`: module used to download EDGAR files from the SEC website by form type

* `EDGAR_Forms.py`: module that can be imported to provide convenient lists of form variants

* `EDGAR_Pac.py`: module containing utility subroutines to facilitate downloading the EDGAR master index files

* `General_Utilities.py`: module with generic utilities (in this case used for downloading files to a string or to a file)

* `Generic_Parser.py`: module to generate sentiment counts for all files contained within a specified folder

* `Load_MasterDictionary.py`: module to load Loughran & McDonald master dictionary

# Data
This folder contains the following files:
* [CRSP](data/CRSP)
    * [Raw](data/CRSP/Raw): unprocessed data downloaded from the CRSP Mutual Fund Database 
        * CRSP_returns_daily.csv: daily mutual fund return data
        * CRSP_returns_monthly.csv: monthly mutual fund return data
        * CRSP_summary.csv: quarterly mutual fund characteristics data
    * CIK_merged.csv: cleaned.csv merged with SEC identifiers 
    * cleaned.csv: cleaned fund return and characteristics data (merged.csv)
    * fund_performance_measures.csv: CIK_merged.csv augmented with additional fund performance measure columns
    * fund_performance_measures_augmented.csv: fund_performance_measures.csv augmented with fund flow columns
    * merged.csv: merge of CRSP_returns_monthly.csv and CRSP_summary.csv
    * return_factors_combined.csv: merge of CRSP_returns_daily.csv and F-F_Research_Data_Factors_daily.csv (from [French](data/French))
    * tickers.txt: list of fund tickers (used when downloading CRSP_returns_daily.csv)
* [EDGAR](data/EDGAR) 
    * Linking_Tables: folder containing all the [SEC Series and Class Reports](https://www.sec.gov/open/datasets-investment_company.html)
    * scraped_tickers.csv: mutual fund data corresponding to funds scraped for additional data on the SEC website
* [French](data/French)
    * F-F_Research_Data_Factors_daily.csv: daily Fama-French three-factor return data
* [glove](data/glove)
    * glove.6B.300d.txt: 300-dimensional GloVe word representation vectors constructed using Wikipedia and GigaWord 5
* [letters](data/letters)
    * [COPY](data/letters/COPY): scraped shareholder letters sorted by year and quarter
    * CRSP_CIK_XXXXQX_TEST.csv: fund characteristics and their linked shareholder letters per quarter
    * CRSP_CIK_v3.csv: most recent merger of all CRSP_CIK_XXXXQX_TEST.csv
* [summaries](data/summaries): generated shareholder letter summaries sorted by year and quarter
* Benchmark_data.xls: monthly MSCI World and S&P500 Index return data
* LoughranMcDonald_MasterDictionary_2018.csv: master dictionary with the frequency, sentiment measures, syllables of words by Loughran and McDonald
* LoughranMcDonald_SentimentWordLists_2018.xlsx: word lists per sentiment type by Loughran and McDonald
* msci_world.xls: monthly MSCI World Index return data used as a benchmark in the trading strategy
* wordlist_merged_2028.txt: list of common multi-syllable words in finance by Kim, Wang & Zhang used for the Modified Fog Index

# Results
This folder contains output from the code, consisting of three parts:
1. [TF_IDF](results/TF_IDF): results obtained using the TF-IDF weighting scheme (main results)
2. [proportional](results/proportional): results obtained using the proportional weighting scheme (robustness check)
3. [trading](results/trading): results from the empirical application (i.e., the portfolio allocation strategy)

Output includes, for example, dataframes with fund characteristics 
and the corresponding computed textual scores, descriptive statistics, regression output, and figures. 

[TF_IDF](results/TF_IDF) and [proportional](results/proportional) are further organized depending on whether the
output files correspond to summarized letters and/or whether they correspond to the full sample (baseline) 
or a particular sub-sample (crisis period sub-sample or fund-specific letters sub-sample).